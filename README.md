VisNow
==================================

VisNow is a generic visualization framework in Java technology, developed by visnow.org community since 2020 (2006-2019 designed and developed by the Interdisciplinary Centre for Mathematical and Computational Modelling 
at University of Warsaw) See VisNow web page (https://www.visnow.org/) for details.
It is a modular data flow driven platform enabling users to create schemes for data visualization, visual analysis, data processing and simple simulations. 
Motivated by 'Read and Watch' idea, VisNow shows the data as soon and fast as possible giving further opportunity for processing and more in-depth visualization. 
In a few steps it can create professional images and movies as well as discover unknown information hidden in datasets.

VisNow, not trying to be yet another visualization tool, is targeted at overcoming the drawbacks of other similar software platforms. It is subject to the following keynotes:
* Clear and legible desktop
* Network creation support
* Multifunctional modules
* Module-Object-Interface connection


VisNow Maven repository 
==================================
If you wish to download JARs built on `develop` branch or use VisNow as a dependecy for your own project or VisNow plugins, use our GitLab maven repository and follow the instructions provided [here](https://gitlab.com/visnow.org/VisNow/-/packages/3995974).


Building VisNow from sources 
==================================

Prerequisites:
- JDK 1.8
- Apache Maven version >=3.6
- cloned VisNow repository

To build VisNow from the command line enter the cloned folder and execute:

`mvn clean install`


To run VisNow from the command line after a successful buid execute: 

`mvn exec:exec` 

or 

`java -jar target/VisNow-<version>-SNAPSHOT.jar` replacing <version> with current version build.


Migrating VisNow from Ant to Maven
==================================

Please note that some older VisNow commits and branches are Ant projects as VisNow migrated to Maven in December 2021. If you're using ant-based project sources of VisNow and wish to migrate to new maven-based project sources we recommend to follow the below points:
- It is advised not to mix ant-based and maven-based branches/versions in a single project as some branch/version switching issues might appear. We recommend to checkout a new clean repository clone into a new folder.
- If you're using IDE (significantly NetBeans IDE) for VisNow development do not switch branches between ant-based and maven-based in NetBeans as it will corrupt project settings. Close the project in IDE, use git command line for branch switching, and reopen the project in IDE (Some manual files/folders cleanup might be required).
- If you have some changes in ant-based branches and wish to merge with maven-based ones you should do it manually via diff files or use branch merging with proper conflicts resolving. Note that new files shall be moved from `src` to `src/main/java` folder.


