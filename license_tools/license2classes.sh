#!/bin/bash

PROJECT_DIR=/Users/babor/Work/Java/projects/tmp/license/VisNow
LICENSE_PLATE=./LICENSE_PLATE.txt
SRC_DIR=$PROJECT_DIR/src/main/java
FILES=`find $SRC_DIR -name "*.java"`

for f in $FILES; do
  cat $LICENSE_PLATE > $f.new
  cat $f | sed -n -e '/package/,$p' >> $f.new
  mv $f.new $f
done


