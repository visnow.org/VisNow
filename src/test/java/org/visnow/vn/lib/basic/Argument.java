/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic;

import org.visnow.jscic.FieldType;
import org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.DevelopmentTestRegularField;
import org.visnow.vn.lib.basic.testdata.TestCells.TestCells;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;

 public class Argument
    {

        private final Class moduleClass;
        private final String inputPortName;
        private FieldType inputFieldType;
        private final int dimension;
        private final String testFieldModuleName;
        private final String viewerModuleName;
        public static final String TEST_REGULAR_FIELD_MODULE_NAME = DevelopmentTestRegularField.class.getName();
        public static final String TEST_IRREGULAR_FIELD_MODULE_NAME = TestCells.class.getName();
        public static final String VIEWER_MODULE_NAME = Viewer3D.class.getName();

        public Argument(Class moduleClass, String inputPortName, FieldType inputFieldType) {
            this(moduleClass, inputPortName, inputFieldType, 3);
        }
        
        public Argument(Class moduleClass, String inputPortName, FieldType inputFieldType, int dimension)
        {
            this(moduleClass, inputPortName, inputFieldType, dimension, inputFieldType == FieldType.FIELD_REGULAR ? TEST_REGULAR_FIELD_MODULE_NAME : TEST_IRREGULAR_FIELD_MODULE_NAME, VIEWER_MODULE_NAME);
        }
       
        public Argument(Class moduleClass, String inputPortName, FieldType inputFieldType, int dimension, String testFieldModuleName) {
            this(moduleClass, inputPortName, inputFieldType, dimension, testFieldModuleName, VIEWER_MODULE_NAME);
        }
        
        public Argument(Class moduleClass, String inputPortName, FieldType inputFieldType, int dimension, String testFieldModuleName, String viewerModuleName)
        {
            this.moduleClass = moduleClass;
            this.inputPortName = inputPortName;
            this.inputFieldType = inputFieldType;
            this.dimension = dimension;
            this.testFieldModuleName = testFieldModuleName;
            this.viewerModuleName = viewerModuleName;            
        }

        public String getModuleName()
        {
            return moduleClass.getName();
        }

        public String getModuleSimpleName()
        {
            return moduleClass.getSimpleName();
        }

        public String getInputPortName()
        {
            return inputPortName;
        }

        public FieldType getFieldType()
        {
            return inputFieldType;
        }

        public int getDimension()
        {
            return dimension;
        }
        
        public String getTestFieldModuleName()
        {
            return testFieldModuleName;
        }
        
        public String getViewerModuleName()
        {
            return viewerModuleName;
        }
    }
