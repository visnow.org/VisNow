/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.AnisotropicDenoiser;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.SwingUtilities;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperations;
import org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared;
import org.visnow.vn.lib.basic.testdata.TestField.TestField;
import org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.tests.application.SimpleApplication;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import org.visnow.vn.system.utils.usermessage.UserMessageListener;

/**
 * Tests of AnisotropicDenoiser
 *
 * @author Piotr Regulski (regulski@icm.edu.pl)
 * <p>
 * Interdisciplinary Centre for Mathematical and Computational Modelling,University of Warsaw
 * <p>
 * Dental & Maxillofacial Radiology Dept. Medical University of Warsaw
 */
@RunWith(Parameterized.class)
public class AnisotropicDenoiserTest
{
    private static UserMessage currentErrorMessage = null;
    private static final int SLEEP_TIME = 100;
    private static int N_DIMS;
    private static int ACTION;
    private static AnisotropicDenoiserShared.Resource RESOURCE;
    private static boolean BYSLICE;
    private static AnisotropicDenoiserShared.Method METHOD;
    private static AnisotropicDenoiserShared.Method PREMETHOD;
    private static int ITERATIONS;
    private static int N_COMPONENTS_INFIELD;
    private static int N_COMPONENTS_SELECTED;

    /**
     * Creates new instance of AnisotropicDenoiserTest
     *
     * @param met                 method
     * @param res                 resource
     * @param dim                 dimmensions
     * @param act                 action (in ComponentsOperations)
     * @param byslice             by slice
     * @param pre                 presmooth method
     * @param iters               number of iterations
     * @param nComponentsInField  number of components selected in TestRegularField
     * @param nComponentsSelected number of components selected in AnisotropicDenoiser
     */
    public AnisotropicDenoiserTest(Object met, Object res, Object dim, Object act, Object byslice, Object pre, Object iters, Object nComponentsInField, Object nComponentsSelected)
    {
        METHOD = (AnisotropicDenoiserShared.Method) met;
        RESOURCE = (AnisotropicDenoiserShared.Resource) res;
        N_DIMS = (Integer) dim;//(Integer) arg[0];
        ACTION = (Integer) act;//(0Integer) arg[1];
        BYSLICE = (Boolean) byslice;
        PREMETHOD = (AnisotropicDenoiserShared.Method) pre;
        ITERATIONS = (Integer) iters;
        N_COMPONENTS_INFIELD = (Integer) nComponentsInField;
        N_COMPONENTS_SELECTED = (Integer) nComponentsSelected;
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Parameterized.Parameters
    public static Collection<Object[]> paramts()
    {
        Object[][] list = new Object[][]{
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.NOOP, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.BYTE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.BYTE_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 2},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.ATAN, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.BYTE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.BYTE_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.BYTE_NORMALIZED, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT_NORMALIZED, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.INT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.DOUBLE, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.BYTE_NORMALIZED, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT_NORMALIZED, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.INT, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.DOUBLE, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 2, ComponentOperationsShared.LOG, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.ATAN, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.BYTE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.BYTE_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.SHORT, true, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.MEDIAN, AnisotropicDenoiserShared.Resource.CPU, 3, ComponentOperationsShared.FLOAT, true, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.BYTE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.BYTE_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.LOG, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.ATAN, false, AnisotropicDenoiserShared.Method.AVERAGE, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.AVERAGE, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.MEDIAN, 1, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.SHORT_NORMALIZED, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.INT, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.FLOAT, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 2, 1},
            {AnisotropicDenoiserShared.Method.AVERAGE, AnisotropicDenoiserShared.Resource.GPU_CUDA, 3, ComponentOperationsShared.DOUBLE, false, AnisotropicDenoiserShared.Method.MEDIAN, 2, 1, 1},};
        return Arrays.asList(list);

    }

    /**
     * Test of onActive method, of class AnisotropicDenoiser.
     *
     * @throws java.lang.InterruptedException
     * @throws java.lang.reflect.InvocationTargetException
     */
    @Test
    public void tester() throws InterruptedException, InvocationTargetException
    {
        String outS = "";
        if (METHOD == AnisotropicDenoiserShared.Method.AVERAGE)
            outS += "avg";
        else
            outS += "med";
        if (RESOURCE == AnisotropicDenoiserShared.Resource.CPU)
            outS += "_cpu";
        else
            outS += "_gpu";
        outS += "_" + N_DIMS + "D";
        if (ACTION == ComponentOperationsShared.NOOP)
            outS += "_noop";
        else if (ACTION == ComponentOperationsShared.BYTE)
            outS += "_byte";
        else if (ACTION == ComponentOperationsShared.BYTE_NORMALIZED)
            outS += "_byte_normalized";
        else if (ACTION == ComponentOperationsShared.SHORT)
            outS += "_short";
        else if (ACTION == ComponentOperationsShared.SHORT_NORMALIZED)
            outS += "_short_norm";
        else if (ACTION == ComponentOperationsShared.INT)
            outS += "_int";
        else if (ACTION == ComponentOperationsShared.FLOAT)
            outS += "_float";
        else if (ACTION == ComponentOperationsShared.DOUBLE)
            outS += "_double";
        else if (ACTION == ComponentOperationsShared.LOG)
            outS += "_log";
        else if (ACTION == ComponentOperationsShared.ATAN)
            outS += "_atan";
        if (BYSLICE)
            outS += "_by_slice";
        else
            outS += "_no_by_slice";
        if (PREMETHOD == AnisotropicDenoiserShared.Method.AVERAGE)
            outS += "_presmooth_avg";
        else
            outS += "_presmooth_med";
        if (ITERATIONS == 1)
            outS += "_1_iter";
        else
            outS += "_2_iter";
        System.out.println(outS);
        try {
            try {
                VisNow.mainBlocking(new String[]{}, true);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
            VisNow.initLogging(true);
            VisNow.get().addUserMessageListener(new UserMessageListener()
            {
                @Override
                public void newMessage(UserMessage message)
                {
                    if (message.getLevel() == Level.ERROR)
                        currentErrorMessage = message;
                }
            });
            Application application = new Application("Test", true);
            VisNow.get().getMainWindow().getApplicationsPanel().addApplication(application);
            String testModuleVNName = SimpleApplication.addModule(application, TestField.class.getName(), 10);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);

            }
            String modifyModuleVNName = SimpleApplication.addModule(application, ComponentOperations.class.getName(), 60);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);

            }
            String currentModuleVNName = SimpleApplication.addModule(application, AnisotropicDenoiser.class.getName(), 110);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);

            }
            String viewerModuleVNName = SimpleApplication.addModule(application, Viewer3D.class.getName(), 160);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                ex.printStackTrace();
            }
            final org.visnow.vn.engine.core.Parameters pTest = application.getEngine().getModule(testModuleVNName).getCore().getParameters();
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    try {
                        pTest.setValue(TestFieldShared.NUMBER_OF_DIMENSIONS.getName(), N_DIMS);
                        pTest.setValue(TestFieldShared.DIMENSION_LENGTH.getName(), 20);
                        if (N_COMPONENTS_INFIELD == 1)
                            pTest.setValue(TestFieldShared.SELECTED_COMPONENTS.getName(), new int[]{0});
                        else
                            pTest.setValue(TestFieldShared.SELECTED_COMPONENTS.getName(), new int[]{0, 1});
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });
            currentErrorMessage = null;
            pTest.fireStateChanged();
            Thread.sleep(SLEEP_TIME);
            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", modifyModuleVNName, "inField"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", modifyModuleVNName, "inField"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                ex.printStackTrace();
            }

            final org.visnow.vn.engine.core.Parameters pModify = application.getEngine().getModule(modifyModuleVNName).getCore().getParameters();
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    try {
                        if (N_COMPONENTS_INFIELD == 1) {
                            pModify.setValue("Actions", new int[]{ACTION});
                            pModify.setValue("Retain", new boolean[]{false});
                        } else {
                            pModify.setValue("Actions", new int[]{ACTION, ACTION});
                            pModify.setValue("Retain", new boolean[]{false, false});
                        }
                        pModify.setValue("Running message", RunButton.RunState.RUN_DYNAMICALLY);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fail("AD: " + ex.toString());
                    }
                }
            });
            currentErrorMessage = null;
            pTest.fireStateChanged();
            Thread.sleep(SLEEP_TIME);
            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(modifyModuleVNName, "regularOutField", currentModuleVNName, "inField"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(modifyModuleVNName, "regularOutField", currentModuleVNName, "inField"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                ex.printStackTrace();
            }

            final org.visnow.vn.engine.core.Parameters pModule = application.getEngine().getModule(currentModuleVNName).getCore().getParameters();
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    pModule.setValue(AnisotropicDenoiserShared.RESOURCE.getName(), RESOURCE);
                    pModule.setValue(AnisotropicDenoiserShared.PRESMOOTH.getName(), true);
                    pModule.setValue(AnisotropicDenoiserShared.BY_SLICE.getName(), BYSLICE);
                    pModule.setValue(AnisotropicDenoiserShared.METHOD.getName(), METHOD);
                    pModule.setValue(AnisotropicDenoiserShared.PRESMOOTH_METHOD.getName(), PREMETHOD);
                    pModule.setValue(AnisotropicDenoiserShared.ITERATIONS.getName(), ITERATIONS);
                    if (N_COMPONENTS_SELECTED != 1)
                        pModule.setValue(AnisotropicDenoiserShared.COMPONENTS.getName(), new int[][]{{0, 0}, {1, 1}});

                    pModule.setValue(AnisotropicDenoiserShared.RUNNING_MESSAGE.getName(), RunButton.RunState.RUN_DYNAMICALLY);

                }
            });
            application.getEngine().getModule(currentModuleVNName).getCore().startAction();

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            Thread.sleep(SLEEP_TIME * 50);
            VNRegularField outputDenoised = (VNRegularField) application.getEngine().getModule(currentModuleVNName).getCore().getOutputs().getOutput("outField").getData().getValue();
            String error = "";
            if (outputDenoised == null && ACTION != ComponentOperationsShared.NOOP) {
                error = "no_output_";
                error += outS;
            }
            VisNow.get().getMainWindow().getApplicationsPanel().removeApplication(application, true);
            VisNow.get().getMainWindow().dispose();
            if (!error.equalsIgnoreCase("") || currentErrorMessage != null)
                fail(error + currentErrorMessage);
        } catch (InterruptedException | InvocationTargetException ex) {
            String error = "exception_" + outS;
            ex.printStackTrace();
            fail(error + ex.toString());
        }

    }
}
