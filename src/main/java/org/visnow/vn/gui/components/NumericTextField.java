/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.components;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import org.visnow.vn.gui.swingwrappers.TextField;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import static org.visnow.vn.gui.components.NumericTextField.FieldType.*;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import org.visnow.jscic.utils.NumberUtils;
import static org.visnow.jscic.utils.NumberUtils.*;

/**
 * Adds parsing input text to TextField, that means: tests numeric format of input value against X.parseX (X = Byte, Short, Integer, Long, Float or Double).
 * <p>
 * Main assumption here is that this field always stores and shows correct number (for current selected type). So input is tested in user action
 * (ValueChangedAction) in setText and in both setValue.
 * But not always getValue == parse(getText) - this is caused by formatting (but it's fine).
 * <p>
 * Internal model here is type + double/long number + double/long min, max + textual value.
 * <p>
 * On user action (input text) value is reverted if value is out of number range.
 * If value is out of [min, max] range than it's reverted (if revertIfOutOfBounds == true) or is limited to min/max (if revertIfOutOfBounds == false).
 * <p>
 * setText throws exception if value is out of range (this is feature of parseX methods).
 * setValue throws exception if value is out of range (to be consistent with setText).
 * setType always casts to new type with rounding to range (according to new type) - in the same time this avoids casting large double to infinity float.
 * <p>
 Internal formatter is based on EngineeringFormattingUtils and is used every time is needed, on:
 setType, setValue, if rounding to range on user action
 <p>
 * This component starts in type == Double. If you use it as a JavaBean (in NetBeans GUI designer) then setType and setText/setValue can be called in "random"
 * order and rounding error may appear (for large Long numbers, larger then Long.MAX_VALUE/1023).
 * In such case it's better to use custom generation code (new NumericTextField(Long))
 * <p>
 * Additionally valueChangeAction is performed only when numeric value has changed (not only text content - try "0 and "00")
 * <p>
 * NaN and Infinity are not allowed here (even if they are properly parsed in parseFloat/Double).
 * <p>
 * getText return different value then internal text (only while editing). In contrary getValue always return internally stored value.
 * <p>
 * [Note: Because of the flow at the beginning this field cannot be easily extended to other types (wider then double, or completely different like Big*,
 * complex, etc)]
 * <p>
 * [Note: It might be better to have NumericField that does not extend TextField (so one cannot call setText but only setValue), and it can be done
 * like it's done in JSpinner (by JPanel-based editor) but in such case all TextField functionality (like setBackground, addFocusListener, etc)
 * would be lost and would need implementation.]
 * <p>
 * @author szpak
 */
public class NumericTextField extends TextField implements Comparable
{

    //model (only one from doubleValue and longValue handles correct value (depends on field type))
    //this could be changed to double/float/long/int/short/byteValue but that would be probably even more messy (in casting/parsing)
    //assume: never NaN, never Infinity
    private double doubleValue; //for float/double types
    private double doubleMin;
    private double doubleMax; // assume: always doubleMin<=doubleValue<=doubleMax

    private long longValue; // for byte/short/int/long types
    private long longMin;
    private long longMax; // assume: always  longMin<=longValue<=longMax

    private String text; //assume: never null
    //
    //assume: type should be never null
    private FieldType type;

    private Formatter formatter;

    private boolean revertIfOutOfBounds = false;
    private boolean autoToolTip = true;

    public NumericTextField()
    {
        this(DOUBLE); //Double by default (wider class) (although may cause problems with large long (read javadoc))
    }

    public NumericTextField(FieldType formatType)
    {
        this(formatType, 0);
    }

    private NumericTextField(FieldType formatType, Object defaultValue) //String defaultText)
    {
        type = formatType;

//        text = defaultText;
//        super.setText(text);
        if (isRealType()) doubleValue = convertToDouble(type, (Number) defaultValue);
//                (Double) parseText(text);
        else longValue = createLongFromObject(type, (Number) defaultValue);
//                (Long) parseText(text);

        formatter = new Formatter()
        {
            @Override
            public String format(double number, boolean fullPrecision)
            {
                if (!fullPrecision) return EngineeringFormattingUtils.format(number);
                else {
                    if (type == DOUBLE) return EngineeringFormattingUtils.formatToPrecision(new BigDecimal(Double.toString(number)), 18, true, false, -4, 6);
                    else return EngineeringFormattingUtils.formatToPrecision(new BigDecimal(Float.toString((float) number)), 18, true, false, -4, 6);
                }
            }

            @Override
            public String format(long number, boolean fullPrecision)
            {
                return Long.toString(number);
            }
        };

        updateInternalTextTooltipAndTextField();

        initializeInternalMinMax();

        super.addUserActionListener(new UserActionListener()
        {
            @Override
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent event)
            {
                try { //fire value change if correct                    
                    if (isRealType()) { //real number type
                        String newText = NumericTextField.super.getText();
                        double newDoubleValue = (Double) parseText(newText);
                        if (revertIfOutOfBounds && (newDoubleValue < doubleMin || newDoubleValue > doubleMax)) NumericTextField.super.setText(text);
                        else {
                            if (newDoubleValue < doubleMin || newDoubleValue > doubleMax) {
                                if (newDoubleValue < doubleMin) newDoubleValue = doubleMin;
                                if (newDoubleValue > doubleMax) newDoubleValue = doubleMax;
                                if (newDoubleValue != doubleValue) {
                                    doubleValue = newDoubleValue;
                                    updateInternalTextTooltipAndTextField();
                                    fireValueChanged();
                                } else NumericTextField.super.setText(text);
                            } else {
                                //no exception -> update model
                                text = newText;
                                if (newDoubleValue != doubleValue) {
                                    doubleValue = newDoubleValue; //update model
                                    updateToolTip();
                                    fireValueChanged(); //numeric value changed -> fire event
                                }
                            }
                        }
                    } else { //int number type
                        String newText = NumericTextField.super.getText();
                        long newLongValue = (Long) parseText(newText);
                        if (revertIfOutOfBounds && (newLongValue < longMin || newLongValue > longMax)) NumericTextField.super.setText(text);
                        else {
                            if (newLongValue < longMin || newLongValue > longMax) {
                                if (newLongValue < longMin) newLongValue = longMin;
                                if (newLongValue > longMax) newLongValue = longMax;
                                if (newLongValue != longValue) {
                                    longValue = newLongValue;
                                    updateInternalTextTooltipAndTextField();
                                    fireValueChanged();
                                } else NumericTextField.super.setText(text);
                            } else {
                                //no exception -> update model
                                text = newText;
                                if (newLongValue != longValue) {
                                    longValue = newLongValue; //update model
                                    updateToolTip();
                                    fireValueChanged(); //numeric value changed -> fire event
                                }
                            }
                        }
                    }
                } catch (NumberFormatException e) { //revert text on failure
                    NumericTextField.super.setText(text);
                }
            }

            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
    }

    public enum FieldType
    {
        BYTE, SHORT, INT, LONG, FLOAT, DOUBLE;

        public boolean isReal()
        {
            return equals(FLOAT) || equals(DOUBLE);
        }
    }

    /**
     * Sets/changes format type of this field to
     * <code>formatType</code> and do necessary casting.
     *
     * This property is called FieldType and not just Type to be alphabetically before "Text" in NetBeans GUI designer (sic!).
     * (anyway BeanInfo has to be added and FieldType marked as preferred)
     */
    public void setFieldType(FieldType newType)
    {
        if (newType == null)
            throw new NullPointerException("Format type cannot be null");
        if (type != newType) {
            if (isRealType()) { //cast from real type
                if (newType == DOUBLE)
                    ; // do nothing (from double to double)
                else if (newType == FLOAT) {
                    doubleValue = Math.max(-Float.MAX_VALUE, Math.min(Float.MAX_VALUE, doubleValue)); //round to range + avoid casting to infinity!
                    doubleMax = Math.max(-Float.MAX_VALUE, Math.min(Float.MAX_VALUE, doubleMax));
                    doubleMin = Math.max(-Float.MAX_VALUE, Math.min(Float.MAX_VALUE, doubleMin));
                } else if (newType == LONG) {
                    longValue = (long) doubleValue; //auto round to range
                    longMax = (long) doubleMax;
                    longMin = (long) doubleMin;
                } else if (newType == INT) {
                    longValue = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, (long) doubleValue)); //round to range
                    longMax = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, (long) doubleMax)); //round to range
                    longMin = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, (long) doubleMin)); //round to range
                } else if (newType == SHORT) {
                    longValue = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, (long) doubleValue)); //round to range
                    longMax = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, (long) doubleMax)); //round to range
                    longMin = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, (long) doubleMin)); //round to range
                } else if (newType == BYTE) {
                    longValue = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, (long) doubleValue)); //round to range
                    longMax = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, (long) doubleMax)); //round to range
                    longMin = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, (long) doubleMin)); //round to range
                } else
                    throw new RuntimeException("Incorrect format");
            } else { //cast from integer type
                if (newType == DOUBLE) {
                    doubleValue = (double) longValue; // double range larger than long
                    doubleMax = (double) longMax;
                    doubleMin = (double) longMin;
                } else if (newType == FLOAT) {
                    doubleValue = (float) longValue; // float range larger than long
                    doubleMax = (float) longMax;
                    doubleMin = (float) longMin;
                } else if (newType == LONG)
                    ; // do nothing (from integer to integer)
                else if (newType == INT) {
                    longValue = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, longValue)); //round to range
                    longMax = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, longMax)); //round to range
                    longMin = Math.max(Integer.MIN_VALUE, Math.min(Integer.MAX_VALUE, longMin)); //round to range
                } else if (newType == SHORT) {
                    longValue = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, longValue)); //round to range
                    longMax = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, longMax)); //round to range
                    longMin = Math.max(Short.MIN_VALUE, Math.min(Short.MAX_VALUE, longMin)); //round to range
                } else if (newType == BYTE) {
                    longValue = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, longValue)); //round to range
                    longMax = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, longMax)); //round to range
                    longMin = Math.max(Byte.MIN_VALUE, Math.min(Byte.MAX_VALUE, longMin)); //round to range
                } else
                    throw new RuntimeException("Incorrect format");
            }

            //update type
            type = newType;

            updateInternalTextTooltipAndTextField();
        }
    }

    public FieldType getFieldType()
    {
        return type;
    }

    /**
     * Tries to parse and updates model on success.
     * This method changes min/max if necessary.
     *
     * @throws NumberFormatException if <code>t</code> cannot be parsed in current type.
     * @deprecated use setValue instead (formatter will be used to format text field)
     */
    @Deprecated
    @Override
    public void setText(String t)
    {
        if (isRealType()) {
            doubleValue = (Double) parseText(t);
            if (doubleMin > doubleValue) doubleMin = doubleValue;
            if (doubleMax < doubleValue) doubleMax = doubleValue;
        } else {
            longValue = (Long) parseText(t);
            if (longMin > longValue) longMin = longValue;
            if (longMax < longValue) longMax = longValue;
        }
        text = t;
        super.setText(t);
    }

    /**
     * This method changes min/max if necessary. Converts value to proper type and sets internal model and view.
     * Conversion is necessary for simple access from GUIBuilder.
     * <p>
     * @throws IllegalArgumentException if value is not of correct type (real number for integer field)
     */
    public void setValue(Number value)
    {
        if (!isCorrectType(value)) throw new IllegalArgumentException("Passed value is incorrect for this field type");

        if (isRealType()) {
            doubleValue = convertToDouble(type, (Number) value);
            if (doubleMin > doubleValue) doubleMin = doubleValue;
            if (doubleMax < doubleValue) doubleMax = doubleValue;
        } else {
            longValue = createLongFromObject(type, (Number) value);
            if (longMin > longValue) longMin = longValue;
            if (longMax < longValue) longMax = longValue;
        }
        updateInternalTextTooltipAndTextField();
    }

    /**
     * This method changes min/max if necessary.
     * <p>
     * @throws IllegalArgumentException if value is not of correct type (real number for integer field)
     * @throws IllegalFormatException   If format is incorrect
     * @throws NullPointerException     if format is null
     * @deprecated use setValue instead (formatter will be used to format text field)
     */
    @Deprecated
    public void setValue(Object value, String format)
    {
        if (!isCorrectType(value)) throw new IllegalArgumentException("Passed value is incorrect for this field type");

        if (isRealType()) {
            doubleValue = convertToDouble(type, (Number) value);
            if (doubleMin > doubleValue) doubleMin = doubleValue;
            if (doubleMax < doubleValue) doubleMax = doubleValue;
            text = String.format(format, doubleValue);
            super.setText(text);
        } else {
            longValue = createLongFromObject(type, (Number) value);
            if (longMin > longValue) longMin = longValue;
            if (longMax < longValue) longMax = longValue;
            text = String.format(format, longValue);
            super.setText(text);
        }
    }

    public static double createDoubleFromObject(Number number)
    {
        return createDoubleFromObject(getType(number), number);
    }

    public static double createDoubleFromObject(FieldType returnType, Number number)
    {
        Double doubleObject;
        if (number instanceof Double || number instanceof Float) doubleObject = NumberUtils.convertToDouble(number);
        else throw new IllegalArgumentException("Incorrect number type");
//        if (number instanceof Double) doubleObject = (Double) number;
//        else doubleObject = new Double((Float) number);
//        else if (object instanceof Long) doubleObject = new Double((Long) object);
//        else if (object instanceof Integer) doubleObject = new Double((Integer) object);
//        else if (object instanceof Short) doubleObject = new Double((Short) object);
//        else doubleObject = new Double((Byte) object);

        //test if correct
        if (isNaNorInfinite(doubleObject))
            //                doubleObject.isNaN() || doubleObject.isInfinite())
            throw new NumberFormatException("NaN and Infinity are not supported: " + number);

        if (returnType == DOUBLE) ; //do nothing (range correct)
        else if (returnType == FLOAT) {
            if ((float) (double) doubleObject > Float.MAX_VALUE || (float) (double) doubleObject < -Float.MAX_VALUE)
                throw new IllegalArgumentException("Float value out of range " + number);
        } else throw new RuntimeException("Incorrect format");

        return doubleObject;
    }

    public static double convertToDouble(FieldType returnType, Number object)
    {
        Double doubleObject = NumberUtils.convertToDouble(object);
//        if (object instanceof Double) doubleObject = (Double) object;
//        else if (object instanceof Float) doubleObject = new Double((Float) object);
//        else if (object instanceof Long) doubleObject = new Double((Long) object);
//        else if (object instanceof Integer) doubleObject = new Double((Integer) object);
//        else if (object instanceof Short) doubleObject = new Double((Short) object);
//        else doubleObject = new Double((Byte) object);

        //test if correct
        if (isNaNorInfinite(doubleObject))
            throw new NumberFormatException("NaN and Infinity are not supported: " + object);

        if (returnType == DOUBLE) ; //do nothing (range correct)
        else if (returnType == FLOAT) {
            if ((float) (double) doubleObject > Float.MAX_VALUE || (float) (double) doubleObject < -Float.MAX_VALUE)
                throw new IllegalArgumentException("Float value out of range " + object);
        } else throw new RuntimeException("Incorrect format");

        return doubleObject;
    }

    public static long createLongFromObject(Number number)
    {
        return createLongFromObject(getType(number), number);
    }

    /**
     * @return long value with respect to current field type and passed Object class.
     */
    public static long createLongFromObject(FieldType returnType, Number number)
    {
        //get value from Object
        Long longObject;
        if (number instanceof Long) longObject = (Long) number;
        else if (number instanceof Integer) longObject = new Long((Integer) number);
        else if (number instanceof Short) longObject = new Long((Short) number);
        else longObject = new Long((Byte) number);

        //test if correct
        if (returnType == LONG) ; //do nothing (range correct)
        else if (returnType == INT) {
            if (longObject > Integer.MAX_VALUE || longObject < Integer.MIN_VALUE) throw new IllegalArgumentException("Integer value out of range " + number);
        } else if (returnType == SHORT) {
            if (longObject > Short.MAX_VALUE || longObject < Short.MIN_VALUE) throw new IllegalArgumentException("Short value out of range " + number);
        } else if (returnType == BYTE) {
            if (longObject > Byte.MAX_VALUE || longObject < Byte.MIN_VALUE) throw new IllegalArgumentException("Byte value out of range " + number);
        } else throw new RuntimeException("Incorrect format");

        return longObject;
    }

//    public static boolean isReal(Number number)
//    {
//        return number instanceof Float || number instanceof Double;
//    }
    public static FieldType getType(Number number)
    {
        if (number instanceof Double) return DOUBLE;
        else if (number instanceof Float) return FLOAT;
        else if (number instanceof Long) return LONG;
        else if (number instanceof Integer) return INT;
        else if (number instanceof Short) return SHORT;
        else if (number instanceof Byte) return BYTE;
        else throw new RuntimeException("Incorrect format");
    }

    public static int compareStrict(Number o1, Number o2)
    {
        if (!isSameTypeStrict((Number) o1, (Number) o2)) throw new IllegalArgumentException("Incompatible types");

        if (isReal(o1))
            return new Double(createDoubleFromObject(o1)).compareTo(new Double(createDoubleFromObject(o2)));
        else
            return new Long(createLongFromObject(o1)).compareTo(new Long(createLongFromObject(o2)));
    }

    public static int compare(Number o1, Number o2)
    {
        if (getType((Number) o1).isReal() != getType((Number) o2).isReal()) throw new IllegalArgumentException("Incompatible types");

        if (isReal(o1))
            //XXX: double check if it's proper conversion from Float to Double (see NumbericUtils.convertToDouble(Number))
            return new Double(createDoubleFromObject(o1)).compareTo(new Double(createDoubleFromObject(o2)));
        else
            return new Long(createLongFromObject(o1)).compareTo(new Long(createLongFromObject(o2)));
    }

    private void initializeInternalMinMax()
    {
        if (type == DOUBLE) {
            doubleMin = -Double.MAX_VALUE;
            doubleMax = Double.MAX_VALUE;
        } else if (type == FLOAT) {
            doubleMin = -Float.MAX_VALUE;
            doubleMax = Float.MAX_VALUE;
        } else if (type == LONG) {
            longMin = Long.MIN_VALUE;
            longMax = Long.MAX_VALUE;
        } else if (type == INT) {
            longMin = Integer.MIN_VALUE;
            longMax = Integer.MAX_VALUE;
        } else if (type == SHORT) {
            longMin = Short.MIN_VALUE;
            longMax = Short.MAX_VALUE;
        } else if (type == BYTE) {
            longMin = Byte.MIN_VALUE;
            longMax = Byte.MAX_VALUE;
        } else throw new RuntimeException("Incorrect format");
    }

    public static Number[] getMinMax(FieldType type)
    {
        if (type == DOUBLE) return new Number[]{-Double.MAX_VALUE, Double.MAX_VALUE};
        else if (type == FLOAT) return new Number[]{-Float.MAX_VALUE, Float.MAX_VALUE};
        else if (type == LONG) return new Number[]{Long.MIN_VALUE, Long.MAX_VALUE};
        else if (type == INT) return new Number[]{Integer.MIN_VALUE, Integer.MAX_VALUE};
        else if (type == SHORT) return new Number[]{Short.MIN_VALUE, Short.MAX_VALUE};
        else if (type == BYTE) return new Number[]{Byte.MIN_VALUE, Byte.MAX_VALUE};
        else throw new RuntimeException("Incorrect format");
    }

    public Number getMax()
    {
        if (isRealType()) return doubleMax;
        else return longMax;
    }

    public Number getMin()
    {
        if (isRealType()) return doubleMin;
        else return longMin;
    }

    /**
     * This method changes min and value if necessary. Converts value to proper type and sets internal model and view.
     * Conversion is necessary for simple access from GUIBuilder.
     */
    public void setMax(Number max)
    {
        if (!isCorrectType(max)) throw new IllegalArgumentException("Passed value is incorrect for this field type");

        if (isRealType()) {
            doubleMax = convertToDouble(type, max);
            if (doubleMin > doubleMax) doubleMin = doubleMax;
            if (doubleValue > doubleMax) {
                doubleValue = doubleMax;
                updateInternalTextTooltipAndTextField();
            }
        } else {
            longMax = createLongFromObject(type, max);
            if (longMin > longMax) longMin = longMax;
            if (longValue > longMax) {
                longValue = longMax;
                updateInternalTextTooltipAndTextField();
            }
        }
    }

    /**
     * This method changes max and value if necessary. Converts value to proper type and sets internal model and view.
     * Conversion is necessary for simple access from GUIBuilder.
     */
    public void setMin(Number min)
    {
        if (!isCorrectType(min)) throw new IllegalArgumentException("Passed value is incorrect for this field type");

        if (isRealType()) {
            doubleMin = convertToDouble(type, min);
            if (doubleMin > doubleMax) doubleMax = doubleMin;
            if (doubleValue < doubleMin) {
                doubleValue = doubleMin;
                updateInternalTextTooltipAndTextField();
            }
        } else {
            longMin = createLongFromObject(type, min);
            if (longMin > longMax) longMax = longMin;
            if (longValue < longMin) {
                longValue = longMin;
                updateInternalTextTooltipAndTextField();
            }
        }
    }

    /**
     * All numeric types are correct if field is of real type; for integer type field only integer types are correct.
     */
    private boolean isCorrectType(Object o)
    {
        if (isRealType())
            return o instanceof Double || o instanceof Float || o instanceof Long || o instanceof Integer || o instanceof Short || o instanceof Byte;
        else
            return o instanceof Long || o instanceof Integer || o instanceof Short || o instanceof Byte;
    }

    private static boolean isSameTypeStrict(Number o, FieldType type)
    {
        return o instanceof Double && type == DOUBLE ||
                o instanceof Float && type == FLOAT ||
                o instanceof Long && type == LONG ||
                o instanceof Integer && type == INT ||
                o instanceof Short && type == SHORT ||
                o instanceof Byte && type == BYTE;
    }

    private static boolean isSameTypeStrict(Number o1, Number o2)
    {
        return o1.getClass().equals(o2.getClass());
    }

    /**
     * Returns numeric value of this text field. Result object class reflects fieldType and is Double, Float, Long, Integer, Short or Byte.
     */
    public Number getValue()
    {
        if (type == DOUBLE) return new Double(doubleValue);
        else if (type == FLOAT) return new Float(doubleValue); //assume: cannot be infinite
        else if (type == LONG) return new Long(longValue);
        else if (type == INT) return new Integer((int) longValue);
        else if (type == SHORT) return new Short((short) longValue);
        else if (type == BYTE) return new Byte((byte) longValue);
        else throw new RuntimeException("Incorrect format");
    }

    private void updateInternalTextTooltipAndTextField()
    {
        if (isRealType())
            text = formatter.format(doubleValue, false);
        else
            text = formatter.format(longValue, false);
        super.setText(text);
        updateToolTip();
    }

    /**
     * Parses
     * <code>t</code> and returns Double or Long (depends on
     * <code>type</code>).
     *
     * @throws NumberFormatException if cannot parse or floating point number is NaN or Infinity.
     */
    private Object parseText(String t) throws NumberFormatException
    {
        if (type == DOUBLE) {
            Double d = Double.parseDouble(t);
            if (d.isNaN() || d.isInfinite())
                throw new NumberFormatException("NaN and Infinity are not supported");
            return d;
        } else if (type == FLOAT) {
            Float f = Float.parseFloat(t);
            if (f.isNaN() || f.isInfinite())
                throw new NumberFormatException("NaN and Infinity are not supported");
            return NumberUtils.convertToDouble(f);//new Double(f);
        } else if (type == LONG)
            return Long.parseLong(t);
        else if (type == INT)
            return new Long(Integer.parseInt(t));
        else if (type == SHORT)
            return new Long(Short.parseShort(t));
        else if (type == BYTE)
            return new Long(Byte.parseByte(t));

        throw new RuntimeException("Incorrect format");
    }

    /**
     * Is float or double.
     */
    private boolean isRealType()
    {
        return (type == DOUBLE || type == FLOAT);
    }

    public void setFormatter(Formatter formatter)
    {
        this.formatter = formatter;
        updateInternalTextTooltipAndTextField();
    }

    private void updateToolTip()
    {
        if (autoToolTip) {
            if (isRealType()) super.setToolTipText(formatter.format(doubleValue, true));
            else super.setToolTipText(formatter.format(longValue, true));
        }
    }

    public boolean isAutoToolTip()
    {
        return autoToolTip;
    }

    public void setAutoToolTip(boolean autoToolTip)
    {
        this.autoToolTip = autoToolTip;
        updateToolTip();
    }

    public boolean isRevertIfOutOfBounds()
    {
        return revertIfOutOfBounds;
    }

    /**
     * @param revertIfOutOfBounds false: if value input by user is out of bound then it's limited to min/max and exception is thrown (if new number is
     *                            different)
     *                            true: if value input by user is out of bound then it is reverted (value and text) to previous state
     */
    public void setRevertIfOutOfBounds(boolean revertIfOutOfBounds)
    {
        this.revertIfOutOfBounds = revertIfOutOfBounds;
    }

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof NumericTextField) {
            NumericTextField tf = (NumericTextField) o;
            if (!tf.type.equals(type)) throw new IllegalArgumentException("Incompatible types");
            else o = tf.getValue();
        }

        if (!isSameTypeStrict((Number) o, type)) throw new IllegalArgumentException("Incompatible with internal type");

        if (isRealType()) {
            double d = createDoubleFromObject(type, (Number) o);
            return doubleValue == d ? 0 : doubleValue < d ? -1 : 1;
        } else {
            long l = createLongFromObject(type, (Number) o);
            return longValue == l ? 0 : longValue < l ? -1 : 1;
        }
    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }
}
