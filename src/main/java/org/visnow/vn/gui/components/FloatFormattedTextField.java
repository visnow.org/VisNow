/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.components;

import java.text.NumberFormat;
import java.text.ParseException;

//import org.apache.log4j.Logger;
/**
 * Text field which tests input format against NumberFormat.getInstance().parse() method.
 * If maxPrecisionDigits is provided then decimal formatting is used.
 * Infinite and NaN are not valid values.
 * <p>
 * @author szpak
 */
public class FloatFormattedTextField extends FormattedTextField
{
    //    private static final Logger LOGGER = Logger.getLogger(FloatFormattedTextField.class);

    private int maxPrecisionDigits = 0;
    private boolean preserveUserFormat = true;

    @Override
    protected boolean isValidValue(String text)
    {
        try {
            text = preformat(text);
            Float f = NumberFormat.getInstance().parse(text).floatValue();
            if (Float.isInfinite(f) || Float.isNaN(f))
                return false;
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    @Override
    protected String getDefaultValue()
    {
        return NumberFormat.getInstance().format(1.0);
    }

    /**
     * Sets maximum number of digits to show. Set to 0 for no formatting (only trim).
     * <p>
     */
    public void setMaxPrecisionDigits(int maxPrecisionDigits)
    {
        this.maxPrecisionDigits = maxPrecisionDigits;
    }

    /**
     * This flag indicates that field is not formatted after user input.
     * <p>
     * @param preserveUserFormat
     */
    public void setPreserveUserFormat(boolean preserveUserFormat)
    {
        this.preserveUserFormat = preserveUserFormat;
    }

    /**
     * Formats text to be formatted on some particular base level - this formatting is applied even if "preserveUserFormat" is set.
     */
    private String preformat(String text)
    {
        //trim + lame hack to replace comma with dot (polish numeric keyboard)
        text = text.trim().replaceAll("\\,", ".");
        //another lame hack to add "0" at the beginning if not present (numbers in format ".001")
        return (text.length() == 0 || text.charAt(0) != '.') ? text : "0" + text;
    }

    @Override
    /**
     * Formats number to have only maxFormatDigits displayed.
     */
    protected String format(String text, boolean userModified)
    {
        //just trim spaces if user modified field
        // or if no formatting (just preformatting)
        if ((preserveUserFormat && userModified) || maxPrecisionDigits == 0)
            return preformat(text);
        //otherwise reformat using default formatter
        //by definition text is valid value - no need to catch exception
        else {
            float f = 0;
            try {
                text = preformat(text);
                f = NumberFormat.getInstance().parse(text.trim()).floatValue();
            } catch (ParseException e) {
                //                LOGGER.error("ParseException - This exception should not occur here. We assume that text is already in correct format!");
                throw new NumberFormatException(e.getMessage());
            }

            return String.format("%1$." + maxPrecisionDigits + "f", f);
        }
    }

    public float getValue()
    {
        try {
            return Float.parseFloat(getText());
        } catch (Exception e) {
            return 0.f;
        }
    }

    private static String formatString(float v)
    {
        int n = (int)Math.log10(v);
        if (n <= -3)
            return "%8.3e";
        if (n < 0)
            return "%"+(4-n)+".4f";
        else if (n < 3)
            return "%"+(6+n)+"." + (4-n) + "f";
        else if (n < 7)
            return "%"+(2+n)+"." + Math.max(0, Math.min(4, n-4))+ "f";
        else
            return "%8.3e";
    }

    private static String format(float v)
    {
        return String.format(formatString(v), v);
    }

    public void setValue(float v)
    {
        setText(format(v));
    }

}
