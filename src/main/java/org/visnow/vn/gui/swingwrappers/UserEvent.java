/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.util.EventObject;

/**
 * Event which:
 * 1) is fired on user action only; and
 * 2) is somehow related to value which is changed or otherwise submitted by user.
 * <p>
 * @author szpak
 */
public class UserEvent extends EventObject
{
    private final int eventType;
    private final Object eventData;
    
    
    //this event is equal to userChangeAction
    public final static int VALUE_CHANGED = 0x01;
    //this event is fired when user "confirms" value without changing it (presses enter in TextField for the second time, clicks already selected RadioButton)
    public final static int VALUE_CONFIRMED = 0x02;
    //this event is fired when user "adjusts" value which means that it keeps focus on the control, drags the slider or sth like that...
    public final static int ADJUSTING = 0x04;
    //XXX: add STOP_ADJUSTING ??

    private final int flags;

    public UserEvent(Object source)
    {
        super(source);
        flags = 0;
        eventType = 0;
        eventData = null;
    }

    public UserEvent(Object source, int eventType)
    {
        super(source);
        flags = 0;
        this.eventType = eventType;
        eventData = null;
    }

    public UserEvent(Object source, Object eventData)
    {
        super(source);
        flags = 0;
        eventType = 0;
        this.eventData = eventData;
    }

    public UserEvent(int flags, Object source)
    {
        super(source);
        this.flags = flags;
        eventType = 0;
        eventData = null;
    }

    public boolean isValueChanged()
    {
        return 0 != (flags & VALUE_CHANGED);
    }

    public boolean isValueConfirmed()
    {
        return 0 != (flags & VALUE_CONFIRMED);
    }

    public boolean isAdjusting()
    {
        return 0 != (flags & ADJUSTING);
    }

    public int getEventType()
    {
        return eventType;
    }

    public Object getEventData()
    {
        return eventData;
    }       
}
