/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;

/**
 * JTextField wrapper: adds ValueChanged Action to this component (with two alternative behaviors: submit on lost focus (default) or revert on lost focus)
 * <p>
 * ValueChange is fired only on user action (no firing event on setter).
 *
 * @author szpak
 */
public class TextField extends JTextField
{
    public static final int EVENT_CHANGE_VALUE = 1;
    public static final int EVENT_NO_CHANGE_ENTER_KEY = 1;
    public static final int EVENT_NO_CHANGE_LOST_FOCUS = 2;

    
    private String storedText = "";
    private boolean submitOnLostFocus = true;

    public TextField()
    {
        initEvents();
    }

    private void initEvents()
    {

        this.addFocusListener(new FocusAdapter()
        {
            //submit or revert value on lost focus
            @Override
            public void focusLost(FocusEvent e)
            {
                if (submitOnLostFocus) {
                    processUserEvent(true);
                } else
                    restoreText();
            }
        });

        this.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                processUserEvent(false);
            }
        });
    }

    private void processUserEvent(boolean lostFocus)
    {
        String newText = TextField.super.getText();
        //fire event if value changed
        boolean textChanged = !newText.equals(storedText);
        if (textChanged) {
            storeText(newText);
            fireValueChanged();
        }
        //fire event on user action
        fireUserAction(textChanged ? EVENT_CHANGE_VALUE : lostFocus ? EVENT_NO_CHANGE_LOST_FOCUS : EVENT_NO_CHANGE_ENTER_KEY);
    }

    @Override
    public void setText(String t)
    {
        super.setText(t);
        storedText = super.getText();
    }

    /**
     * In general it should be called before fireValueChange - because text/value are good.
     */
    private void storeText(String newText)
    {
        storedText = newText;
    }

    private void restoreText()
    {
        super.setText(storedText);
    }

    private void fireUserAction(int eventType)
    {
        for (UserActionListener listener : userActionListeners)
            listener.userAction(new UserEvent(this, eventType));
    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

    public void setSubmitOnLostFocus(boolean submitOnLostFocus)
    {
        this.submitOnLostFocus = submitOnLostFocus;
    }

    public boolean isSubmitOnLostFocus()
    {
        return submitOnLostFocus;
    }
}
