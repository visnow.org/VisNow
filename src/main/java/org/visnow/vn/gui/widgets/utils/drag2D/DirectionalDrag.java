/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.utils.drag2D;

/**
 * Drag along specified axis.
 * <p>
 * One can specify two axes. They are considered to be orthogonal. If second axis is not specified then standard perpendicular axis is taken.
 * <p>
 * To work properly axes need to be linearly independent. Although it's not tested in constructor.
 * <p>
 * @author szpak
 */
public class DirectionalDrag
{
    private final double dragStartX;
    private final double dragStartY;
    //a11, a12, a21, a22
    private final double[] dragBasisMatrix;

    /**
     * Create new directional drag along specified axis1 and axis2 (which is considered orthogonal).
     * <p>
     * Axes are normalized so their length is not taken into consideration.
     */
    public DirectionalDrag(double dragStartX, double dragStartY, double axis1X, double axis1Y, double axis2X, double axis2Y)
    {
        this.dragStartX = dragStartX;
        this.dragStartY = dragStartY;
        double length1 = Math.sqrt(axis1X * axis1X + axis1Y * axis1Y);
        axis1X /= length1;
        axis1Y /= length1;
        double length2 = Math.sqrt(axis2X * axis2X + axis2Y * axis2Y);
        axis2X /= length1;
        axis2Y /= length1;

        double det = axis1X * axis2Y - axis2X * axis1Y;
        //matrix which converts to drag base
        dragBasisMatrix = new double[]{axis2Y / det, -axis2X / det,
                                       -axis1Y / det, axis1X / det};
    }

    /**
     * Create new directional drag along specified axis1 and axis perpendicular to axis1.
     * <p>
     * Invocation of this method is equal to
     * new DirectionalDrag(dragStartX, dragStartY, axis1X, axis1Y, -axis1Y, axis1X);
     */
    public DirectionalDrag(double dragStartX, double dragStartY, double axis1X, double axis1Y)
    {
        this(dragStartX, dragStartY, axis1X, axis1Y, -axis1Y, axis1X);
    }

    public double calculateDrag(double x, double y)
    {
        x -= dragStartX;
        y -= dragStartY;

        double dX = dragBasisMatrix[0] * x + dragBasisMatrix[1] * y;

        return dX;
    }

    public double[] calculateDoubleDrag(double x, double y)
    {
        x -= dragStartX;
        y -= dragStartY;

        double dX = dragBasisMatrix[0] * x + dragBasisMatrix[1] * y;
        double dY = dragBasisMatrix[2] * x + dragBasisMatrix[3] * y;

        return new double[]{dX, dY};
    }
}
