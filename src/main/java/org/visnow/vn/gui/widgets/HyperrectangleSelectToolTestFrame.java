/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import org.apache.log4j.Logger;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 * @author szpak
 */
public class HyperrectangleSelectToolTestFrame extends javax.swing.JFrame
{

    private static final Logger LOGGER = Logger.getLogger(HyperrectangleSelectToolTestFrame.class);

    /**
     * Creates new form RectangleSelectToolTestFrame
     */
    public HyperrectangleSelectToolTestFrame()
    {
        boolean a;

        initComponents();
        updateTextFields();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        uIStyleFakeInitializer1 = new org.visnow.vn.system.swing.UIStyleFakeInitializer();
        marginPanel = new javax.swing.JPanel();
        rectangleSelectTool = new org.visnow.vn.gui.widgets.HyperrectangleSelectTool();
        controPanel = new javax.swing.JPanel();
        allowOverflow = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        noOfDimensionsTF = new org.visnow.vn.gui.components.NumericTextField();
        jButton1 = new javax.swing.JButton();
        baseSizeLabel = new javax.swing.JLabel();
        base1TF = new org.visnow.vn.gui.components.NumericTextField();
        base2TF = new org.visnow.vn.gui.components.NumericTextField();
        base3TF = new org.visnow.vn.gui.components.NumericTextField();
        setBaseButton = new javax.swing.JButton();
        baseSizeLabel1 = new javax.swing.JLabel();
        selectionSize1TF = new org.visnow.vn.gui.components.NumericTextField();
        selectionSize2TF = new org.visnow.vn.gui.components.NumericTextField();
        selectionSize3TF = new org.visnow.vn.gui.components.NumericTextField();
        setSelectionSizeButton = new javax.swing.JButton();
        baseSizeLabel2 = new javax.swing.JLabel();
        selectionOffset1TF = new org.visnow.vn.gui.components.NumericTextField();
        selectionOffset2TF = new org.visnow.vn.gui.components.NumericTextField();
        selectionOffset3TF = new org.visnow.vn.gui.components.NumericTextField();
        setSelectionSizeButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());
        getContentPane().add(uIStyleFakeInitializer1, new java.awt.GridBagConstraints());

        marginPanel.setBackground(new java.awt.Color(214, 225, 238));
        marginPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        marginPanel.setLayout(new java.awt.BorderLayout());

        rectangleSelectTool.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        rectangleSelectTool.setPreferredSize(new java.awt.Dimension(80, 250));
        rectangleSelectTool.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rectangleSelectToolUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });

        javax.swing.GroupLayout rectangleSelectToolLayout = new javax.swing.GroupLayout(rectangleSelectTool);
        rectangleSelectTool.setLayout(rectangleSelectToolLayout);
        rectangleSelectToolLayout.setHorizontalGroup(
            rectangleSelectToolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 382, Short.MAX_VALUE)
        );
        rectangleSelectToolLayout.setVerticalGroup(
            rectangleSelectToolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 68, Short.MAX_VALUE)
        );

        marginPanel.add(rectangleSelectTool, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(marginPanel, gridBagConstraints);

        controPanel.setLayout(new java.awt.GridBagLayout());

        allowOverflow.setSelected(true);
        allowOverflow.setText("Allow overflow");
        allowOverflow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                allowOverflowActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        controPanel.add(allowOverflow, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Number of Dimensions:");
        jPanel1.add(jLabel1, new java.awt.GridBagConstraints());

        noOfDimensionsTF.setText("2");
        noOfDimensionsTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        noOfDimensionsTF.setMinimumSize(new java.awt.Dimension(40, 20));
        noOfDimensionsTF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel1.add(noOfDimensionsTF, gridBagConstraints);

        jButton1.setText("Set");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel1.add(jButton1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        controPanel.add(jPanel1, gridBagConstraints);

        baseSizeLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        baseSizeLabel.setText("Base size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(baseSizeLabel, gridBagConstraints);

        base1TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        base1TF.setMinimumSize(new java.awt.Dimension(40, 20));
        base1TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(base1TF, gridBagConstraints);

        base2TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        base2TF.setMinimumSize(new java.awt.Dimension(40, 20));
        base2TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(base2TF, gridBagConstraints);

        base3TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        base3TF.setMinimumSize(new java.awt.Dimension(40, 20));
        base3TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(base3TF, gridBagConstraints);

        setBaseButton.setText("Set base");
        setBaseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setBaseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 0);
        controPanel.add(setBaseButton, gridBagConstraints);

        baseSizeLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        baseSizeLabel1.setText("Selection size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(baseSizeLabel1, gridBagConstraints);

        selectionSize1TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionSize1TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionSize1TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionSize1TF, gridBagConstraints);

        selectionSize2TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionSize2TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionSize2TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionSize2TF, gridBagConstraints);

        selectionSize3TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionSize3TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionSize3TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionSize3TF, gridBagConstraints);

        setSelectionSizeButton.setText("Set selection size");
        setSelectionSizeButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setSelectionSizeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 0);
        controPanel.add(setSelectionSizeButton, gridBagConstraints);

        baseSizeLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        baseSizeLabel2.setText("Selection offset:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(baseSizeLabel2, gridBagConstraints);

        selectionOffset1TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionOffset1TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionOffset1TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionOffset1TF, gridBagConstraints);

        selectionOffset2TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionOffset2TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionOffset2TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionOffset2TF, gridBagConstraints);

        selectionOffset3TF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        selectionOffset3TF.setMinimumSize(new java.awt.Dimension(40, 20));
        selectionOffset3TF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        controPanel.add(selectionOffset3TF, gridBagConstraints);

        setSelectionSizeButton1.setText("Set selection offset");
        setSelectionSizeButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setSelectionSizeButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        controPanel.add(setSelectionSizeButton1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(controPanel, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void setBaseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_setBaseButtonActionPerformed
    {//GEN-HEADEREND:event_setBaseButtonActionPerformed
        rectangleSelectTool.setBaseSize(new double[]{(double) base1TF.getValue(), (double) base2TF.getValue(), (double) base3TF.getValue()});
        updateTextFields();
    }//GEN-LAST:event_setBaseButtonActionPerformed

    private void allowOverflowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_allowOverflowActionPerformed
    {//GEN-HEADEREND:event_allowOverflowActionPerformed
        rectangleSelectTool.setAllowOverflow(allowOverflow.isSelected());
        updateTextFields();
    }//GEN-LAST:event_allowOverflowActionPerformed

    private void setSelectionSizeButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_setSelectionSizeButtonActionPerformed
    {//GEN-HEADEREND:event_setSelectionSizeButtonActionPerformed
        rectangleSelectTool.setSelectionSize(new double[]{(double) selectionSize1TF.getValue(), (double) selectionSize2TF.getValue(), (double) selectionSize3TF.getValue()});
        updateTextFields();
    }//GEN-LAST:event_setSelectionSizeButtonActionPerformed

    private void setSelectionSizeButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_setSelectionSizeButton1ActionPerformed
    {//GEN-HEADEREND:event_setSelectionSizeButton1ActionPerformed
        rectangleSelectTool.setSelectionOffset(new double[]{(double) selectionOffset1TF.getValue(), (double) selectionOffset2TF.getValue(), (double) selectionOffset3TF.getValue()});
        updateTextFields();
    }//GEN-LAST:event_setSelectionSizeButton1ActionPerformed

    private void rectangleSelectToolUserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_rectangleSelectToolUserChangeAction
    {//GEN-HEADEREND:event_rectangleSelectToolUserChangeAction
        updateTextFields();
    }//GEN-LAST:event_rectangleSelectToolUserChangeAction

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        rectangleSelectTool.setNumberOfDimensions((int) noOfDimensionsTF.getValue());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void updateTextFields()
    {
        double[] base = rectangleSelectTool.getBaseSize();
        base1TF.setValue(base[0], "%4.0f");
        base2TF.setValue(base[1], "%4.0f");
        base3TF.setValue(base[2], "%4.0f");
        double[] selection = rectangleSelectTool.getSelectionSize();
        selectionSize1TF.setValue(selection[0], "%4.0f");
        selectionSize2TF.setValue(selection[1], "%4.0f");
        selectionSize3TF.setValue(selection[2], "%4.0f");
        double[] selectionOffset = rectangleSelectTool.getSelectionOffset();
        selectionOffset1TF.setValue(selectionOffset[0], "%4.0f");
        selectionOffset2TF.setValue(selectionOffset[1], "%4.0f");
        selectionOffset3TF.setValue(selectionOffset[2], "%4.0f");
        noOfDimensionsTF.setValue(rectangleSelectTool.getNumberOfDimensions());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();

        /* Set the Nimbus look and feel */
        //        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        //        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        //         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
        //         */
        //        try {
        //            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        //                if ("Nimbus".equals(info.getName())) {
        //                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
        //                    break;
        //                }
        //            }
        //        } catch (ClassNotFoundException ex) {
        //            java.util.logging.Logger.getLogger(RectangleSelectToolTestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (InstantiationException ex) {
        //            java.util.logging.Logger.getLogger(RectangleSelectToolTestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (IllegalAccessException ex) {
        //            java.util.logging.Logger.getLogger(RectangleSelectToolTestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
        //            java.util.logging.Logger.getLogger(RectangleSelectToolTestFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        }
        //        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                HyperrectangleSelectToolTestFrame rectangleSelectToolTestFrame = new HyperrectangleSelectToolTestFrame();
                rectangleSelectToolTestFrame.setLocation(500, 300);
                rectangleSelectToolTestFrame.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton allowOverflow;
    private org.visnow.vn.gui.components.NumericTextField base1TF;
    private org.visnow.vn.gui.components.NumericTextField base2TF;
    private org.visnow.vn.gui.components.NumericTextField base3TF;
    private javax.swing.JLabel baseSizeLabel;
    private javax.swing.JLabel baseSizeLabel1;
    private javax.swing.JLabel baseSizeLabel2;
    private javax.swing.JPanel controPanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel marginPanel;
    private org.visnow.vn.gui.components.NumericTextField noOfDimensionsTF;
    private org.visnow.vn.gui.widgets.HyperrectangleSelectTool rectangleSelectTool;
    private org.visnow.vn.gui.components.NumericTextField selectionOffset1TF;
    private org.visnow.vn.gui.components.NumericTextField selectionOffset2TF;
    private org.visnow.vn.gui.components.NumericTextField selectionOffset3TF;
    private org.visnow.vn.gui.components.NumericTextField selectionSize1TF;
    private org.visnow.vn.gui.components.NumericTextField selectionSize2TF;
    private org.visnow.vn.gui.components.NumericTextField selectionSize3TF;
    private javax.swing.JButton setBaseButton;
    private javax.swing.JButton setSelectionSizeButton;
    private javax.swing.JButton setSelectionSizeButton1;
    private org.visnow.vn.system.swing.UIStyleFakeInitializer uIStyleFakeInitializer1;
    // End of variables declaration//GEN-END:variables
}
