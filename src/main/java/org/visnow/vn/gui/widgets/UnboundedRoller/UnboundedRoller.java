/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.UnboundedRoller;

/*
 * SubRangeSlider.java
 *
 * Created on April 13, 2004, 3:11 PM
 */
import java.awt.Dimension;
import java.awt.Insets;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class UnboundedRoller extends JComponent implements ChangeListener, Serializable
{

    private UnboundedValueModel model;
    private Insets insets = new Insets(3, 3, 3, 3);
    private boolean active = true;

    /**
     * Creates a new instance of SubRangeSlider
     */
    public UnboundedRoller()
    {
        init(new DefaultUnboundedValueModel());
        addMouseWheelListener(new java.awt.event.MouseWheelListener()
            {
                @Override
                public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt)
                {
                    UnboundedValueModel model = getModel();
                    setValue(model.getValue() + model.getSensitivity() * evt.getWheelRotation());
                }
            });
    }

    protected final void init(UnboundedValueModel m)
    {
        setModel(m);
        setMinimumSize(new Dimension(60, 45));
        setPreferredSize(new Dimension(200, 55));
        UIManager.put(UnboundedRollerUI.UI_CLASS_ID, "BasicUnboundedRollerUI");
        updateUI();
    }

    public void setUI(UnboundedRollerUI ui)
    {
        super.setUI(ui);
    }

    @Override
    public void updateUI()
    {
        setUI(new BasicUnboundedRollerUI());
        invalidate();
    }

    @Override
    public String getUIClassID()
    {
        return UnboundedRollerUI.UI_CLASS_ID;
    }

    public void setModel(UnboundedValueModel m)
    {
        UnboundedValueModel old = model;
        if (old != null)
            old.removeChangeListener(this);

        if (m == null)
            model = new DefaultUnboundedValueModel();
        else
            model = m;
        model.addChangeListener(this);

        firePropertyChange("model", old, model);
    }

    public UnboundedValueModel getModel()
    {
        return model;
    }

    public void reset()
    {
        model.setValue(0);
        model.setSensitivity(1.f);
    }

    public void stateChanged(ChangeEvent e)
    {
        repaint();
    }

    public float getValue()
    {
        return model.getValue();
    }

    public void setValue(float v)
    {
        float old = getValue();
        if (v != old) {
            model.setValue(v);
            if (active)
                fireStateChanged();
        }
    }

    public void setOutValue(float v)
    {
        float old = getValue();
        if (v != old)
            model.setValue(v);
    }

    public boolean isAdjusting()
    {
        return model.isAdjusting();
    }

    public void setAdjusting(boolean b)
    {
        boolean old = isAdjusting();
        if (b != old) {
            model.setAdjusting(b);
            fireStateChanged();
        }
    }

    public float getSensitivity()
    {
        return model.getSensitivity();
    }
    
    public float getLargeIncrement()
    {
        return model.getLargeIncrement();
    }
    
    public void setLargeIncrement(float increment)
    {
        model. setLargeIncrement(increment);
    }

    public void setSensitivity(float m)
    {
        float old = getSensitivity();
        if (m != old)
            model.setSensitivity(m);
    }

    public void setInsets(Insets i)
    {
        insets = i;
    }

    public void setInsets(int top, int left, int bottom, int right)
    {
        insets = new Insets(top, left, bottom, right);
    }

    @Override
    public Insets getInsets()
    {
        return insets;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<FloatValueModificationListener> floatValueModificationListenerList = new ArrayList<FloatValueModificationListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        floatValueModificationListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(FloatValueModificationListener listener)
    {
        floatValueModificationListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, model.getValue(), model.isAdjusting());
        for (FloatValueModificationListener listener : floatValueModificationListenerList) {
            listener.floatValueChanged(e);
        }
    }

    private boolean enabled = true;

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        repaint();
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
