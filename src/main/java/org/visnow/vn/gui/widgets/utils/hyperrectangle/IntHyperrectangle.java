/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.utils.hyperrectangle;

/**
 *  Hyperrectangle with values rounded to nearest integer.
 * 
 * @author szpak
 */
public class IntHyperrectangle extends Hyperrectangle
{
    public IntHyperrectangle(int numberOfDimensions, double[] size, double[] offset)
    {
        super(numberOfDimensions, round(size), round(offset));
    }

    public IntHyperrectangle(int numberOfDimensions, double[] size)
    {
        super(numberOfDimensions, round(size), new double[]{0, 0, 0});
    }

    public IntHyperrectangle(Hyperrectangle h)
    {
        this(h.numberOfDimensions, h.size, h.offset);
    }

    @Override
    public void cropTo(Hyperrectangle bounds, double minimumDimensionLength)
    {
        super.cropTo(new IntHyperrectangle(bounds), Math.round(minimumDimensionLength));
    }

    @Override
    public IntHyperrectangle clone()
    {
        return new IntHyperrectangle(numberOfDimensions, size, offset);
    }

    @Override
    public void intersectWith(Hyperrectangle h)
    {
        super.intersectWith(new IntHyperrectangle(h));
    }

    @Override
    public void packInside(Hyperrectangle bounds)
    {
        super.packInside(new IntHyperrectangle(bounds)); 
    }

    @Override
    public void translate(Endpoint[] endpoints, double[] distance, double minimumDimensionLength)
    {
        super.translate(endpoints, round(distance), Math.round(minimumDimensionLength)); 
    }
    
    private static double[] round(double[] values)
    {
        double[] roundedValues = new double[values.length];
        for (int i = 0; i < values.length; i++)
            roundedValues[i] = Math.round(values[i]);
        return roundedValues;
    }
}
