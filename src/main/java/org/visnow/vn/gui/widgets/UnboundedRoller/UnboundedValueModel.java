/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.UnboundedRoller;

import javax.swing.event.*;

/**
 * Defines the data model used by UnboundedRoller component.
 * Defines two interrelated float properties: value and sensitivity.
 *
 *
 * @version 1.26 01/23/03
 * @author Krzysztof S. Nowinski
 * @see DefaultBoundedRangeModel
 */
public interface UnboundedValueModel
{

    /**
     * Returns the current value.
     *
     * @return the value of the value property
     * <p>
     * @see #setValue
     */
    float getValue();

    /**
     * Sets the model's current value to <I>newValue</I>.
     * Notifies any listeners if the model changes.
     *
     * @param newMinimum the model's new minimum
     * <p>
     * @see #getValue
     * @see #addChangeListener
     */
    void setValue(float newValue);

    /**
     * Returns the model's sensitivity.
     * <p>
     * @see #setSensitivity
     */
    float getSensitivity();

    /**
     * Sets the model's sensitivity to <I>newSensitivity</I>.
     *
     * @param newSensitivity the model's new sensitivity
     * <p>
     * @see #getSensitivity
     * @see #addChangeListener
     */
    void setSensitivity(float newSensitivity);

    /**
     * Returns true if a series of changes of
     * BottomValue or TopValue settings is in progress.
     * Returns false when both TopValue and BottomValue are finally set.
     */
    boolean isAdjusting();

    /**
     * Set true if a series of changes of
     * Value settings is in progress.
     * Set false when both Value are finally set.
     */
    void setAdjusting(boolean newAdjusting);
    
    /**
     * largeIncrement will be added to /subtracted from the current value on shift-click
     * @param increment 
     */
    float getLargeIncrement();
    void setLargeIncrement(float increment);

    /**
     * Adds a ChangeListener to the model's listener list.
     *
     * @param x the ChangeListener to add
     * <p>
     * @see #removeChangeListener
     */
    void addChangeListener(ChangeListener x);

    /**
     * Removes a ChangeListener from the model's listener list.
     *
     * @param x the ChangeListener to remove
     * <p>
     * @see #addChangeListener
     */
    void removeChangeListener(ChangeListener x);

}
