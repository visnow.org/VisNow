/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GraphFloatSlider extends javax.swing.JPanel implements Serializable
{

    protected float min = 0.f;
    protected float max = 1.f;
    protected float val = .5f;
    protected float rMin = 0.f;
    protected float rMax = 1.f;
    protected float d = .001f;
    protected int l = 0;
    protected int u = 1000;
    protected int ld = 100;
    protected int dec = 4;
    protected int ddec = 3;
    protected String form = "%" + dec + "." + ddec + "f";
    protected String lform = "%" + (dec - 1) + "." + (ddec - 1) + "f";
    protected Font textFont = new java.awt.Font("Dialog", 0, 10);
    protected boolean active = true;
    protected boolean adjusting = true;
    protected boolean showingFields = true;
    protected float[] graph = null;
    private Hashtable<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();

    class ImagePanel extends JPanel
    {

        public ImagePanel()
        {
        }

        public void paintComponent(Graphics g)
        {
            int w = getWidth();
            int h = getHeight();
            g.setColor(new Color(238, 238, 238));
            g.fillRect(0, 0, w, h);
            if (graph == null || graph.length < 2)
                return;
            g.setColor(Color.BLUE);
            int n = graph.length;
            int[] xPoints = new int[n];
            int[] yPoints = new int[n];
            float s = 0;
            for (int i = 0; i < n; i++)
                if (s < graph[i])
                    s = graph[i];
            s = (h - 4) / s;
            for (int i = 0; i < n; i++) {
                xPoints[i] = (int) (i * (w - 16) / (n - 1.)) + 7;
                yPoints[i] = (int) (s * graph[i]) + 2;
            }
            g.drawPolyline(xPoints, yPoints, n);
        }
    }

    /**
     * Creates new form FloatSlider
     */
    public GraphFloatSlider()
    {
        initComponents();
        for (int i = 0; i + l <= u; i += ld) {
            JLabel lbl = new JLabel(String.format(lform, rMin + i * d));
            lbl.setFont(getFont());
            labels.put(new Integer(i + l), lbl);
        }
        slider = new JSlider();
        slider.setLabelTable(labels);
        slider.setFont(new java.awt.Font("Dialog", 0, 8));
        slider.setMajorTickSpacing(100);
        slider.setMaximum(1000);
        slider.setMinorTickSpacing(20);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setMinimumSize(new java.awt.Dimension(36, 40));
        slider.setOpaque(false);
        slider.setPreferredSize(new java.awt.Dimension(200, 45));
        slider.setRequestFocusEnabled(false);
        slider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                sliderStateChanged(evt);
            }
        });

        sliderPanel = new ImagePanel();
        sliderPanel.setLayout(new java.awt.BorderLayout());
        sliderPanel.add(slider, java.awt.BorderLayout.CENTER);
        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(sliderPanel, gridBagConstraints);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        maxField = new javax.swing.JTextField();
        valField = new javax.swing.JTextField();
        minField = new javax.swing.JTextField();
        showFieldsBox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();

        setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        setMinimumSize(new java.awt.Dimension(90, 48));
        setPreferredSize(new java.awt.Dimension(200, 51));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        maxField.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        maxField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        maxField.setText("1");
        maxField.setMaximumSize(new java.awt.Dimension(60, 15));
        maxField.setMinimumSize(new java.awt.Dimension(35, 12));
        maxField.setPreferredSize(new java.awt.Dimension(35, 13));
        maxField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(maxField, gridBagConstraints);

        valField.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        valField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        valField.setText(".5");
        valField.setMaximumSize(new java.awt.Dimension(60, 15));
        valField.setMinimumSize(new java.awt.Dimension(50, 12));
        valField.setPreferredSize(new java.awt.Dimension(60, 13));
        valField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                valFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(valField, gridBagConstraints);

        minField.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        minField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        minField.setText("0");
        minField.setMaximumSize(new java.awt.Dimension(60, 15));
        minField.setMinimumSize(new java.awt.Dimension(4, 12));
        minField.setPreferredSize(new java.awt.Dimension(60, 13));
        minField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(minField, gridBagConstraints);

        showFieldsBox.setBackground(new java.awt.Color(153, 153, 153));
        showFieldsBox.setSelected(true);
        showFieldsBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showFieldsBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        showFieldsBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showFieldsBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        showFieldsBox.setMinimumSize(new java.awt.Dimension(12, 10));
        showFieldsBox.setPreferredSize(new java.awt.Dimension(10, 10));
        showFieldsBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showFieldsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        add(showFieldsBox, gridBagConstraints);

        jPanel1.setMinimumSize(new java.awt.Dimension(10, 4));
        jPanel1.setPreferredSize(new java.awt.Dimension(100, 200));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void showFieldsBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showFieldsBoxActionPerformed
        showingFields = showFieldsBox.isSelected();
        showFields();
    }//GEN-LAST:event_showFieldsBoxActionPerformed

    private void minFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minFieldActionPerformed
        min = Float.parseFloat(minField.getText());//GEN-LAST:event_minFieldActionPerformed
        roundMinMax();
    }

    private void maxFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxFieldActionPerformed
        active = false;
        float v = val;
        max = Float.parseFloat(maxField.getText());
        roundMinMax();
        val = v;
        slider.setValue((int) ((val - min + d / 2) / d));
        active = true;
    }//GEN-LAST:event_maxFieldActionPerformed

    private void valFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_valFieldActionPerformed
        try {
            val = Float.parseFloat(valField.getText());
        } catch (Exception ex) {
            return;
        }
        if (val < min || val > max)
            return;
        slider.setValue((int) ((val - min + d / 2) / d));
        fireStateChanged();
    }//GEN-LAST:event_valFieldActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        active = false;
        float v = val;
        roundMinMax();
        val = v;
        slider.setValue((int) ((val - min + d / 2) / d));
        active = true;
    }//GEN-LAST:event_formComponentResized

    private void sliderStateChanged(javax.swing.event.ChangeEvent evt)
    {
        val = rMin + slider.getValue() * d;
        valField.setText(String.format(form, rMin + slider.getValue() * d));
        fireStateChanged();
    }

    private void roundMinMax()
    {
        double r = max - min;
        if (r <= 0)
            r = 1;
        double logr = log10(r);
        int iLogr = (int) (logr + 100) - 100;
        double mr = r / pow(10., 1. * iLogr);
        int space = (slider.getWidth() - 30) / 20;
        if (space < 1)
            space = 1;
        if (space > mr) {
            mr *= 10;
            iLogr -= 1;
        }
        mr /= space;
        if (mr < 2)
            mr = 2;
        else if (mr < 5)
            mr = 5;
        else
            mr = 10;
        d = (float) mr;
        if (iLogr > 0)
            for (int i = 0; i < iLogr; i++)
                d *= 10;
        if (iLogr < 0)
            for (int i = 0; i > iLogr; i--)
                d /= 10;
        d /= ld;
        rMin = ld * d * ((int) (min / (ld * d)));
        while (rMin < min)
            rMin += ld * d;
        rMax = ld * d * ((int) (max / (ld * d)));
        while (rMax > max)
            rMax -= ld * d;
        labels.clear();
        l = (int) (rMin / d) - (int) (min / d);
        u = (int) (rMax / d) - (int) (min / d);
        ddec = 100 - (int) (log10(d) + 100);
        if (ddec < 0)
            ddec = 0;
        if (iLogr > 0)
            dec = iLogr + ddec + 2;
        else
            dec = ddec + 1;
        form = "%" + dec + "." + ddec + "f";
        int k = ddec - 2;
        if (k < 0)
            k = 0;
        lform = "%" + (dec - 1) + "." + k + "f";
        for (int i = 0; i + l <= u; i += ld) {
            JLabel lbl = new JLabel(String.format(lform, rMin + i * d));
            lbl.setFont(this.getFont());
            labels.put(new Integer(i + l), lbl);
        }
        slider.setMaximum(u);
        slider.setLabelTable(labels);
        slider.repaint();
    }

    public float getMin()
    {
        return min;
    }

    public void setMin(float min)
    {
        this.min = min;
        minField.setText("" + min);
        roundMinMax();
    }

    public float getMax()
    {
        return max;
    }

    public void setMax(float max)
    {
        this.max = max;
        maxField.setText("" + max);
        roundMinMax();
    }

    public void setMinMax(float min, float max)
    {
        this.min = min;
        minField.setText("" + min);
        this.max = max;
        maxField.setText("" + max);
        roundMinMax();
    }

    public float getVal()
    {
        return val;
    }

    public void setVal(float val)
    {
        this.val = val;
    }

    public int getDec()
    {
        return dec;
    }

    public void setDec(int dec)
    {
        this.dec = dec;
    }

    public int getDdec()
    {
        return ddec;
    }

    public void setDdec(int ddec)
    {
        this.ddec = ddec;
    }

    public void setTextFont(Font textFont)
    {
        this.textFont = textFont;
        maxField.setFont(textFont);
        valField.setFont(textFont);
        minField.setFont(textFont);
    }

    public boolean isAdjusting()
    {
        return slider.getValueIsAdjusting();
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
        slider.setValueIsAdjusting(adjusting);
    }

    public boolean isShowingFields()
    {
        return showingFields;
    }

    public void setShowingFields(boolean showingFields)
    {
        this.showingFields = showingFields;
        showFieldsBox.setSelected(showingFields);
        showFields();
    }

    public void showFields()
    {
        minField.setVisible(showingFields);
        maxField.setVisible(showingFields);
        valField.setVisible(showingFields);
        this.validate();
    }

    public void setGraph(float[] graph)
    {
        this.graph = graph;
        sliderPanel.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField maxField;
    private javax.swing.JTextField minField;
    private javax.swing.JCheckBox showFieldsBox;
    private javax.swing.JTextField valField;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JSlider slider;
    private ImagePanel sliderPanel;
    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        if (changeListenerList != null) {
            changeListenerList.remove(listener);
        }
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        if ((slider.getValueIsAdjusting() && !adjusting) || !active)
            return;
        java.util.ArrayList list;
        ChangeEvent e = new ChangeEvent(this);
        synchronized (this) {
            if (changeListenerList == null)
                return;
            list = new ArrayList<ChangeListener>(changeListenerList);
            //       list = (ArrayList<ChangeListener>)changeListenerList.clone ();
        }
        for (int i = 0; i < list.size(); i++) {
            ((ChangeListener) list.get(i)).stateChanged(e);
        }
    }
}
