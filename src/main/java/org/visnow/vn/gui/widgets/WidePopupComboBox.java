/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

/**
 *
 * @author szpak (based on SteppedComboBox by Nobuo Tamemasa)
 */
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import org.visnow.vn.gui.swingwrappers.ComboBox;
import org.visnow.vn.lib.utils.SwingInstancer;

public class WidePopupComboBox extends ComboBox
{
    final private WidePopupComboBoxUI steppedComboBoxNewUI;

    public WidePopupComboBox()
    {
        steppedComboBoxNewUI = new WidePopupComboBoxUI();
        WidePopupComboBox.super.getInnerComboBox().setUI(steppedComboBoxNewUI);

        addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                steppedComboBoxNewUI.updatePopupWidth();
            }
        });
    }

    public void componentResized(ComponentEvent e)
    {
                steppedComboBoxNewUI.updatePopupWidth();
    }
}

class WidePopupComboBoxUI extends MetalComboBoxUI
{
    private int popupWidth;
    protected void updatePopupWidth()
    {

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {                                    
                if (comboBox.getRenderer() != null &&
                        comboBox.getModel() != null &&
                        (comboBox.getUI() instanceof WidePopupComboBoxUI)) {

                    JList list = ((WidePopupComboBoxUI) comboBox.getUI()).createPopup().getList();

                    popupWidth = 10;
                    for (int i = 0; i < comboBox.getModel().getSize(); i++) {
                        int w = comboBox.getRenderer().getListCellRendererComponent(
                                list, comboBox.getModel().getElementAt(i),
                                i, true, true).getPreferredSize().width;
                        if (w > popupWidth) popupWidth = w;
                    }
                    popupWidth += 25;

                } else {
                    if (comboBox.getFont() != null) {
                        FontMetrics fm = comboBox.getFontMetrics(comboBox.getFont());
                        if (fm == null) {
                            return;
                        }
                        popupWidth = 10;
                        for (int i = 0; i < comboBox.getModel().getSize(); i++)
                            if (comboBox.getModel() != null && comboBox.getModel().getElementAt(i) != null) {
                                int w = fm.stringWidth(comboBox.getModel().getElementAt(i).toString());
                                if (w > popupWidth) popupWidth = w;
                            }

                        popupWidth += 80;
                    }
                }

                popupWidth = Math.max(popupWidth, comboBox.getWidth());
            }
        });

    }

    public Dimension getPopupSize()
    {
        Dimension size = comboBox.getSize();
        if (popupWidth < 1) {
            popupWidth = size.width;
        }
        return new Dimension(popupWidth, size.height);
    }
    
    @Override
    protected ComboPopup createPopup()
    {
        BasicComboPopup popup = new BasicComboPopup(comboBox)
        {
            public void show()
            {
                Dimension popupSize = getPopupSize();
                popupSize.setSize(popupSize.width,
                                  getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
                Rectangle popupBounds = computePopupBounds(0,
                                                           comboBox.getBounds().height, popupSize.width, popupSize.height);
                scroller.setMaximumSize(popupBounds.getSize());
                scroller.setPreferredSize(popupBounds.getSize());
                scroller.setMinimumSize(popupBounds.getSize());
                list.invalidate();
                int selectedIndex = comboBox.getSelectedIndex();
                if (selectedIndex == -1) {
                    list.clearSelection();
                } else {
                    list.setSelectedIndex(selectedIndex);
                }
                list.ensureIndexIsVisible(list.getSelectedIndex());
                setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());

                show(comboBox, popupBounds.x, popupBounds.y);
            }
        };
        popup.getAccessibleContext().setAccessibleParent(comboBox);
        return popup;
    }
}
