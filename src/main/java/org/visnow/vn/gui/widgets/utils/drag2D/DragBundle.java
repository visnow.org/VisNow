/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.utils.drag2D;

import java.awt.Cursor;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.Endpoint;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.Hyperrectangle;

/**
 * Drag bundle consists of dimension(s) and its endpoint(s) to drag, proper directionalDrag for dragging this direction, ratio(s) between screen distance and
 * dimension distance and proper cursor for showing to the user.
 * <p>
 * @author szpak
 */
public class DragBundle
{
    private final DirectionalDrag directionalDrag;
    private final Endpoint[] endpoints;
    private final int[] dimensions;
    private final double[] ratios;
    private final Cursor cursor;

    /**
     * Create single drag bundle
     */
    public DragBundle(DirectionalDrag directionalDrag, int dimension, Endpoint endpoint, double ratio, Cursor cursor)
    {
        this.directionalDrag = directionalDrag;
        this.dimensions = new int[]{dimension};
        this.endpoints = new Endpoint[]{endpoint};
        this.ratios = new double[]{ratio};
        this.cursor = cursor;
    }

    /**
     * Create double drag bundle
     */
    public DragBundle(DirectionalDrag directionalDrag, int dimension1, int dimension2, Endpoint endpoint1, Endpoint endpoint2, double ratio1, double ratio2, Cursor cursor)
    {
        this.directionalDrag = directionalDrag;
        this.dimensions = new int[]{dimension1, dimension2};
        this.endpoints = new Endpoint[]{endpoint1, endpoint2};
        this.ratios = new double[]{ratio1, ratio2};
        this.cursor = cursor;
    }

    public Cursor getCursor()
    {
        return cursor;
    }
    
    public void applyDrag(Hyperrectangle h, double x, double y, double minimumDimensionLength)
    {
        if (minimumDimensionLength < 0)
            throw new IllegalArgumentException("MinimumDimensionLength : " + minimumDimensionLength + " cannot be negative.");

        double[] distance = new double[h.getNumberOfDimensions()];
        double[] drags = directionalDrag.calculateDoubleDrag(x, y);
        Endpoint[] hyperrectangleEndpoints = new Endpoint[h.getNumberOfDimensions()];

        for (int i = 0; i < dimensions.length; i++) {
            int dim = dimensions[i];
            distance[dim] = drags[i] * ratios[i];
            hyperrectangleEndpoints[dim] = endpoints[i];
        }

        h.translate(hyperrectangleEndpoints, distance, minimumDimensionLength);
    }

}
