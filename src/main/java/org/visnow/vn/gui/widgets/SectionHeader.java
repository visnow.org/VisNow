/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import org.apache.log4j.Logger;
import org.visnow.vn.gui.swingwrappers.UserActionAdapter;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import org.visnow.vn.gui.swingwrappers.UserEvent;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIIconLoader;
import org.visnow.vn.system.swing.UIManagerKey;
import org.visnow.vn.system.swing.UIStyle;

/**
 * Horizontal Separator with additional title (JLabel).
 * setFont, setForeground, setEnabled are redirected to title font.
 *
 * @author szpak
 */
public class SectionHeader extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(SectionHeader.class);

    private boolean expandable = true;
    private boolean expanded = true;
    private boolean showIcon = true;
    private boolean showCheckBox = true;
    private boolean selectable = true;

    /**
     * Creates new form TitledSeparator
     */
    public SectionHeader()
    {
        initComponents();
        postInitComponents();
    }

    private void postInitComponents()
    {
        filler1.setVisible(false);
        titleButton.setFont(super.getFont());
        titleButton.setForeground(super.getForeground());
        updateIcon();
    }

    private void updateIcon()
    {
        titleButton.setIcon(!showIcon ? null : UIIconLoader.getIcon(expanded ? UIManagerKey.Tree_expandedIcon : UIManagerKey.Tree_collapsedIcon));
    }

    private void updateButtonBackground()
    {
        titleButton.setContentAreaFilled(super.isEnabled() && expandable);
        //fix (rewrite opaque)
        titleButton.setOpaque(false);
    }

    @Override
    public void setFont(Font font)
    {
        super.setFont(font);
        if (titleButton != null)
            titleButton.setFont(font);
    }

    @Override
    public void setForeground(Color fg)
    {
        super.setForeground(fg);
        if (titleButton != null)
            titleButton.setForeground(fg);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        titleButton.setEnabled(enabled);
        checkBox.setEnabled(enabled && selectable);
        updateButtonBackground();
    }

    @Override
    public boolean isEnabled()
    {
        return super.isEnabled();
    }

    public void setText(String text)
    {
        titleButton.setText(text);
    }

    public String getText()
    {
        return titleButton.getText();
    }

    public boolean isShowIcon()
    {
        return showIcon;
    }

    public void setShowIcon(boolean showIcon)
    {
        this.showIcon = showIcon;
        updateIcon();
    }

    public boolean isShowCheckBox()
    {
        return showCheckBox;
    }

    public void setShowCheckBox(boolean showCheckBox)
    {
        this.showCheckBox = showCheckBox;
        checkBox.setVisible(showCheckBox);
    }

    public void setSelected(boolean selected)
    {
        checkBox.setSelected(selected);
    }

    public boolean isSelected()
    {
        return checkBox.isSelected();
    }

    public boolean isSelectable()
    {
        return selectable;
    }

    public void setSelectable(boolean selectable)
    {
        this.selectable = selectable;
        checkBox.setEnabled(super.isEnabled() && selectable);
    }

    public boolean isShowSeparator()
    {
        return separator.isVisible();
    }

    public void setShowSeparator(boolean showSeparator)
    {
        separator.setVisible(showSeparator);
        filler1.setVisible(!showSeparator);
    }

    public boolean isExpandable()
    {
        return expandable;
    }

    public void setExpandable(boolean expandable)
    {
        this.expandable = expandable;
        updateButtonBackground();
        updateIcon();
    }

    public boolean isExpanded()
    {
        return expanded;
    }

    public void setExpanded(boolean expanded)
    {
        toggleExpanded(expanded);
    }

    private void toggleExpanded(boolean expanded)
    {
        this.expanded = expanded;
        updateIcon();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        titleButton = new javax.swing.JButton();
        checkBox = new javax.swing.JCheckBox();
        separator = new javax.swing.JSeparator();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));

        setBackground(javax.swing.UIManager.getDefaults().getColor("TabbedPane.selected"));
        setLayout(new java.awt.GridBagLayout());

        titleButton.setBackground(new java.awt.Color(255, 255, 255));
        titleButton.setText("Section");
        titleButton.setBorder(null);
        titleButton.setBorderPainted(false);
        titleButton.setOpaque(false);
        titleButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                titleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        add(titleButton, gridBagConstraints);

        checkBox.setOpaque(false);
        checkBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                checkBoxActionPerformed(evt);
            }
        });
        add(checkBox, new java.awt.GridBagConstraints());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        add(separator, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void titleButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_titleButtonActionPerformed
    {//GEN-HEADEREND:event_titleButtonActionPerformed
        if (expandable && isEnabled()) {
            toggleExpanded(!expanded);
            fireValueChanged(EVENT_TOGGLE_EXPANDED);
        }
    }//GEN-LAST:event_titleButtonActionPerformed

    private void checkBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_checkBoxActionPerformed
    {//GEN-HEADEREND:event_checkBoxActionPerformed
        if (isEnabled())
            fireValueChanged(EVENT_CHECKBOX_SWITCHED);
    }//GEN-LAST:event_checkBoxActionPerformed

    public static final int EVENT_CHECKBOX_SWITCHED = 1;
    public static final int EVENT_TOGGLE_EXPANDED = 2;

    private java.util.List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

    private void fireValueChanged(int eventType)
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new UserEvent(this, eventType));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox checkBox;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JSeparator separator;
    private javax.swing.JButton titleButton;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();
        JFrame f = new JFrame();

        final SectionHeader s = new SectionHeader();
        s.addUserActionListener(new UserActionAdapter()
        {
            @Override
            public void userChangeAction(UserEvent event)
            {
                LOGGER.debug("");
            }
        });
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.add(s);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
//        LOGGER.debug(s.isFocusable());
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                s.grabFocus();
            }
        });

//        KeyboardFocusManager.getCurrentKeyboardFocusManager().
    }
}
