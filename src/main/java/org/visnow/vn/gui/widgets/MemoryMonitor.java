/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

/*
 * MemoryMonitor.java
 *
 * Created on 25 listopad 2003, 17:03
 */
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.visnow.jlargearrays.MemoryCounter;
import org.visnow.vn.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author axnow
 */
public class MemoryMonitor extends JProgressBar implements Runnable
{

    /**
     * Creates a new instance of MemoryMonitor
     */
    private DecimalFormat format = new DecimalFormat("00.0");
    private long refresh = 500;
    private boolean modeStatic = true;

    public MemoryMonitor()
    {
        super();
        setStringPainted(true);
        setFont(new java.awt.Font("Dialog", 0, 11));
        updateValues();
        new Thread(this, "MemoryMonitor").start();
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
                    System.out.println("Garbage collector started...");
                    Runtime.getRuntime().gc();
                } else if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
                    MemoryMonitor.this.modeStatic = !MemoryMonitor.this.modeStatic;
                }
            }
        });
        format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        format.applyPattern("00.0%");
        this.setToolTipText("Right-click for maximum mode change static/dynamic. Double-left-click for garbage collection.");
        //preferred size used before in MainWindow
        setPreferredSize(new Dimension(240, 18));
    }

    private void updateValues()
    {
        Runtime r = Runtime.getRuntime();
        long unsafeMemory = MemoryCounter.getCounter();
        long total = r.totalMemory() + unsafeMemory;
        long free = r.freeMemory();
        long used = total - free;
        //if VisNow not initialized yet then return (previously MemoryMonitor could not be initialized by NetBeans in design mode)
        if (VisNow.get() == null)
            return;

        long max = VisNow.get().getMemoryMax();
        if (modeStatic && max != Long.MAX_VALUE) {
            setMaximum(100);
            double ratio = (double) used / (double) max;
            setValue((int) round(100 * ratio));
            String info = "";
            if (max / (1024L * 1024L) < 10) {
                info = (used / (1024L)) + "/" + (max / (1024L)) + " kB (" + format.format(getPercentComplete()) + ")";
            } else if (max / (1024L * 1024L * 1024L) < 10) {
                info = (used / (1024L * 1024L)) + "/" + (max / (1024L * 1024L)) + " MB (" + format.format(getPercentComplete()) + ")";
            } else {
                info = (used / (1024L * 1024L * 1024L)) + "/" + (max / (1024L * 1024L * 1024L)) + " GB (" + format.format(getPercentComplete()) + ")";
            }

            setString("Memory usage: " + info);
        } else {
            total = total / 1024L;
            used = used / 1024L;
            setMaximum((int) total);
            setValue((int) used);
            String info = "";
            if (total / 1024L < 10) {
                info = (used) + "/" + (total) + " kB (" + format.format(getPercentComplete()) + ")";
            } else if (total / (1024L * 1024L) < 10) {
                info = (used / 1024L) + "/" + (total / 1024L) + " MB (" + format.format(getPercentComplete()) + ")";
            } else {
                info = (used / (1024L * 1024L)) + "/" + (total / (1024L * 1024L)) + " GB (" + format.format(getPercentComplete()) + ")";
            }

            setString("Memory usage: " + info);
        }

    }

    public void run()
    {
        while (true) {
            SwingUtilities.invokeLater(new Runnable()
            {
                public void run()
                {
                    updateValues();
                }
            });

            try {
                Thread.currentThread().sleep(refresh);
            } catch (InterruptedException e) {
            }
        }
    }

    public long getRefresh()
    {
        return refresh;
    }

    /**
     * Setter for property refresh.
     *
     * @param refresh New value of property refresh.
     *
     */
    public synchronized void setRefresh(long refresh)
    {
        this.refresh = refresh;
    }
}
