/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.utils.hyperrectangle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author szpak
 */
public class Hyperrectangle
{
    double[] size;
    double[] offset;
    final int numberOfDimensions;

    public Hyperrectangle(int numberOfDimensions, double[] size, double[] offset)
    {
        this.numberOfDimensions = numberOfDimensions;
        if (size.length < numberOfDimensions || offset.length < numberOfDimensions)
            throw new IllegalArgumentException("arrays too short: " + size.length + " " + offset.length);
        for (int i = 0; i < size.length; i++)
            if (size[i] < 0) throw new IllegalArgumentException("Dimension cannot be negative: " + Arrays.toString(size));
        this.size = size.clone();
        this.offset = offset.clone();
    }

    public Hyperrectangle(int numberOfDimensions, double[] size)
    {
        this(numberOfDimensions, size, new double[]{0, 0, 0});
    }

    public double[] getSize()
    {
        return size.clone();
    }

    public double[] getOffset()
    {
        return offset.clone();
    }

    public int getNumberOfDimensions()
    {
        return numberOfDimensions;
    }

    public boolean isEmpty()
    {
        for (int i = 0; i < numberOfDimensions; i++)
            if (size[i] == 0) return true;
        return false;
    }

    public void scale(float scale)
    {
        scale(scale, false);
    }

    public void scale(double scale, boolean centered)
    {
        if (centered)
            for (int i = 0; i < 3; i++) {
                offset[i] += (size[i] - size[i] * scale) / 2;
                size[i] *= scale;
            }
        else
            for (int i = 0; i < 3; i++) {
                offset[i] *= scale;
                size[i] *= scale;
            }
    }

    public static Hyperrectangle union(Hyperrectangle h1, Hyperrectangle h2)
    {
        if (h1.numberOfDimensions != h2.numberOfDimensions)
            throw new IllegalArgumentException("dimensions don't match " + h1.numberOfDimensions + " != " + h2.numberOfDimensions);

        double[] s = new double[h1.numberOfDimensions];
        double[] o = new double[h1.numberOfDimensions];

        for (int i = 0; i < h1.numberOfDimensions; i++) {
            o[i] = Math.min(h1.offset[i], h2.offset[i]);
            s[i] = Math.max(h1.offset[i] + h1.size[i], h2.offset[i] + h2.size[i]) - o[i];
        }
        return new Hyperrectangle(h1.numberOfDimensions, s, o);
    }

    /**
     * Intersects this hyperrectangle with {@code h}.
     */
    public void intersectWith(Hyperrectangle h)
    {
        doCropTo(h, 0);
    }

    /**
     * Intersects this hyperrectangle with {@code bounds} but assuring that minimum dimension is not smaller than {@code minimumDimensionLength}.
     * <p>
     * @param bounds hyperrectangle which works as crop boundaries
     */
    public void cropTo(Hyperrectangle bounds, double minimumDimensionLength)
    {
        doCropTo(bounds, minimumDimensionLength);
    }

    protected void doCropTo(Hyperrectangle bounds, double minimumDimensionLength)
    {
        if (bounds.numberOfDimensions != numberOfDimensions)
            throw new IllegalArgumentException("dimensions don't match " + numberOfDimensions + " != " + bounds.numberOfDimensions);

        if (minimumDimensionLength < 0)
            throw new IllegalArgumentException("MinimumDimensionLength : " + minimumDimensionLength + " cannot be negative.");

        for (int i = 0; i < numberOfDimensions; i++)
            if (bounds.size[i] < minimumDimensionLength)
                throw new IllegalStateException("Cannot crop when bounding rectangle " + Arrays.toString(bounds.size) + " has dimension less than " + minimumDimensionLength);

        double[] s = size.clone();
        double[] o = offset.clone();

        for (int i = 0; i < numberOfDimensions; i++) {
            //cut low and extend to min
            if (o[i] < bounds.offset[i]) {
                s[i] = Math.max(minimumDimensionLength, s[i] + o[i] - bounds.offset[i]);
                o[i] = bounds.offset[i];
            }
            //cut & move high
            o[i] = Math.min(o[i], bounds.offset[i] + bounds.size[i] - minimumDimensionLength);
            s[i] = Math.min(s[i], bounds.offset[i] + bounds.size[i] - o[i]);
        }

        size = s;
        offset = o;
    }

    /**
     * Move hyperrectangle inside given (@code bounds} and trim if cannot fit completely inside.
     */
    public void packInside(Hyperrectangle bounds)
    {
        doPackInside(bounds);
    }

    protected void doPackInside(Hyperrectangle bounds)
    {
        if (bounds.numberOfDimensions != numberOfDimensions)
            throw new IllegalArgumentException("dimensions don't match " + numberOfDimensions + " != " + bounds.numberOfDimensions);

        for (int i = 0; i < numberOfDimensions; i++) {
            //move right
            offset[i] = Math.max(offset[i], bounds.offset[i]);
            //move left
            offset[i] = Math.min(offset[i], bounds.offset[i] + bounds.size[i] - size[i]);
            //trim left
            if (offset[i] < bounds.offset[i]) {
                offset[i] = bounds.offset[i];
                size[i] = bounds.size[i];
            }
        }
    }

    public void translate(Endpoint[] endpoints, double[] distance, double minimumDimensionLength)
    {
        doTranslate(endpoints, distance, minimumDimensionLength);
    }

    protected void doTranslate(Endpoint[] endpoints, double[] distance, double minimumDimensionLength)
    {
        if (minimumDimensionLength < 0)
            throw new IllegalArgumentException("MinimumDimensionLength : " + minimumDimensionLength + " cannot be negative.");

        if (distance.length < numberOfDimensions || endpoints.length < numberOfDimensions)
            throw new IllegalArgumentException("arrays too short: " + distance.length + " " + endpoints.length);

        double[] s = size.clone();
        double[] o = offset.clone();

        for (int i = 0; i < numberOfDimensions; i++) {
            if (endpoints[i] == Endpoint.LOW) {
                s[i] = Math.max(minimumDimensionLength, size[i] - distance[i]);
                o[i] = offset[i] + size[i] - s[i];
            } else if (endpoints[i] == Endpoint.HIGH) {
                s[i] = Math.max(minimumDimensionLength, size[i] + distance[i]);
            } else if (endpoints[i] == Endpoint.BOTH) {
                o[i] += distance[i];
                s[i] = Math.max(minimumDimensionLength, size[i]);
            }
        }

        size = s;
        offset = o;
    }

    //TODO: refactor/ make it more generic??
    public PointDouble[][] projectOnPlanes(double[] planes)
    {
        if (numberOfDimensions != 3 || planes.length != 3) throw new UnsupportedOperationException("Supported only in 3 dimensional case");

        PointDouble[][] projections = new PointDouble[planes.length][];

        for (int i = 0; i < planes.length; i++) {
            Hyperrectangle projected = this.clone();
            projected.offset[i] = planes[i];
            projected.size[i] = 1;
            PointDouble[] v2d = projected.getVertices2DProjection();
            if (i == 0) projections[i] = new PointDouble[]{v2d[0], v2d[4], v2d[6], v2d[2]};
            if (i == 1) projections[i] = new PointDouble[]{v2d[0], v2d[1], v2d[5], v2d[4]};
            if (i == 2) projections[i] = new PointDouble[]{v2d[0], v2d[2], v2d[3], v2d[1]};
        }

        return projections;
    }

    //TODO: find proper solution for this
    public PointDouble[][] getFramePoints()
    {
        ParallelSegment[] parallelSegments = getFrame();
        PointDouble[][] pointDoubles = new PointDouble[parallelSegments.length][];

        for (int i = 0; i < pointDoubles.length; i++)
            pointDoubles[i] = parallelSegments[i].get2DProjection();

        return pointDoubles;
    }

    //TODO: find proper solution for this
    public PointDouble[][] getSubFrame(double[] planes, boolean below)
    {
        ParallelSegment[] parallelSegments = getFrame();

        List<ParallelSegment> subSegments = new ArrayList<>();

        for (int i = 0; i < parallelSegments.length; i++) {
            ParallelSegment parallelSegment = parallelSegments[i];
            boolean above = false;
            for (int j = 0; j < planes.length; j++)
                if (j != parallelSegment.getParallelDimension())
                    if (!parallelSegment.isBelow(j, planes[j])) above = true;

            if (above) {
                if (!below) subSegments.add(parallelSegment.clone());
            } else {
                ParallelSegment[] split = parallelSegment.split(parallelSegment, planes[parallelSegment.getParallelDimension()]);
                if (below && split[0] != null) subSegments.add(split[0]);
                if (!below && split[1] != null) subSegments.add(split[1]);
            }
        }

        parallelSegments = subSegments.toArray(new ParallelSegment[subSegments.size()]);

        PointDouble[][] pointDoubles = new PointDouble[parallelSegments.length][];

        for (int i = 0; i < pointDoubles.length; i++)
            pointDoubles[i] = parallelSegments[i].get2DProjection();

        return pointDoubles;
    }

    //TODO: make it generic
    private ParallelSegment[] getFrame()
    {
        if (numberOfDimensions != 3) throw new UnsupportedOperationException("Supported only in 3 dimensional case");

//        ParallelSegment[][] segments = new ParallelSegment[3][];
        return new ParallelSegment[]{
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1], offset[2]}, 0, size[0]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1] + size[1], offset[2]}, 0, size[0]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1], offset[2] + size[2]}, 0, size[0]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1] + size[1], offset[2] + size[2]}, 0, size[0]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1], offset[2]}, 1, size[1]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0] + size[0], offset[1], offset[2]}, 1, size[1]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1], offset[2] + size[2]}, 1, size[1]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0] + size[0], offset[1], offset[2] + size[2]}, 1, size[1]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1], offset[2]}, 2, size[2]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0] + size[0], offset[1], offset[2]}, 2, size[2]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0], offset[1] + size[1], offset[2]}, 2, size[2]),
            new ParallelSegment(numberOfDimensions, new double[]{offset[0] + size[0], offset[1] + size[1], offset[2]}, 2, size[2])
        };

//        for (int i = 0; i < numberOfDimensions; i++) {
        //            segments[i] = new ParallelSegment[1 << (numberOfDimensions - 1)];
        //            int j = 0;
        //            for (int k = 0; k < numberOfDimensions; k++)
        //                if (k != numberOfDimensions)
        //                    segments[i][j++] = new ParallelSegment(numberOfDimensions, offset, numberOfDimensions, k)
        //                
        //        }
        //        return segments;
    }

//TODO: refactor/ make it more generic??
//    public PointDouble[][] splitFrame(double[] planes, boolean below)
//    {
//        if (numberOfDimensions != 3 || planes.length != 3) throw new UnsupportedOperationException("Supported only in 3 dimensional case");
//
//        Hyperrectangle split = this.clone();
//
//    }
    public void cut(int dimension, double position, boolean keepLow)
    {
        if (numberOfDimensions <= dimension)
            throw new IllegalArgumentException("Dimension: " + dimension + " cannot be higher then max dimension " + numberOfDimensions);

        Hyperrectangle maxHyperrectangle = this.clone();
        for (int i = 0; i < numberOfDimensions; i++)
            if (i == dimension) {
                double high = offset[i] + size[i];
                if (keepLow) {
                    maxHyperrectangle.offset[i] = Double.MIN_VALUE;
                    maxHyperrectangle.size[i] = position - Double.MIN_VALUE;
                } else {
                    maxHyperrectangle.offset[i] = position;
                    maxHyperrectangle.size[i] = Double.MAX_VALUE - position;
                }
            } else {
                maxHyperrectangle.offset[i] = Double.MIN_VALUE;
                maxHyperrectangle.size[i] = Double.MAX_VALUE - Double.MIN_VALUE;
            }

        doCropTo(maxHyperrectangle, 0);
    }

    public static final double zScaleRatio = 0.7f;

    //15 degrees
//        public static final float zYRatio = 0.5f * 0.25881904510252073948f;
//        public static final float zXRatio = 0.5f * 0.96592582628906831221f;
//30 degrees
    public static final double zYRatio = zScaleRatio * 1 / 2;
    public static final double zXRatio = zScaleRatio * 1.7320508075688771932f / 2;
//45 degrees
//    public static final double zYRatio = zScaleRatio * 1.4142135623730951455f / 2;
//    public static final double zXRatio = zScaleRatio * 1.4142135623730951455f / 2;

    /**
     * Returns 2D projection of vertices:
     * <ul>
     * <li>for 1D: resets 2nd dimension
     * <li>for 2D: keeps original
     * <li>for 3D: makes oblique projection
     * <li>not supported for higher dimensions
     * </ul>
     * <p>
     * For n - dimensional hyperrectangle 2^n points are returned.
     * Return order is from highest dimension to lowest. So for normalized cube in 3D this method returns
     * {(0,0,0), (1,0,0), (0,1,0), (1,1,0), (0,0,1), (1,0,1), (0,1,1), (1,1,1)} projected into the plane.
     * <pre>
     *         2 ---------- 3
     *        /|           /|
     *       / |          / |
     *      /  |         /  |
     *     /   |        /   |
     *    6 ---------- 7    |
     *    |    |       |    |
     *    |    0 ------|--- 1
     *    |   /        |   /
     *    |  /         |  /
     *    | /          | /
     *    |/           |/
     *    4 ---------- 5
     * <p>
     * </pre>
     * @return
     */
    public PointDouble[] getVertices2DProjection()
    {
        if (numberOfDimensions == 1)
            return new PointDouble[]{new PointDouble(offset[0], 0), new PointDouble(offset[0] + size[0], 0)};
        else if (numberOfDimensions == 2)
            return new PointDouble[]{new PointDouble(offset[0], offset[1]), new PointDouble(offset[0] + size[0], offset[1]),
                                     new PointDouble(offset[0], offset[1] + size[1]), new PointDouble(offset[0] + size[0], offset[1] + size[1])};
        else if (numberOfDimensions == 3) {
            double oZX = Math.round(offset[2] * zXRatio);
            double sZX = Math.round(size[2] * zXRatio);
            double oZY = Math.round(offset[2] * zYRatio);
            double sZY = Math.round(size[2] * zYRatio);

            return new PointDouble[]{
                new PointDouble(offset[0] - oZX, offset[1] - oZY),
                new PointDouble(offset[0] + size[0] - oZX, offset[1] - oZY),
                new PointDouble(offset[0] - oZX, offset[1] + size[1] - oZY),
                new PointDouble(offset[0] + size[0] - oZX, offset[1] + size[1] - oZY),
                new PointDouble(-sZX + offset[0] - oZX, -sZY + offset[1] - oZY),
                new PointDouble(-sZX + offset[0] + size[0] - oZX, -sZY + offset[1] - oZY),
                new PointDouble(-sZX + offset[0] - oZX, -sZY + offset[1] + size[1] - oZY),
                new PointDouble(-sZX + offset[0] + size[0] - oZX, -sZY + offset[1] + size[1] - oZY)};
        } else throw new UnsupportedOperationException("Oblique projection is supported only in 3 dimensional case.");
    }

    static final int style1DHalfGapPx = 2;
    static final int style1DHeightHalfPx = 4;

    public void draw(Graphics2D graphics2D, int width, int height, double worldStartX, double worldStartY, double worldScale, BasicStroke stroke, Color strokeColor, Color fillColor)
    {
        PointDouble[] v2D = getVertices2DProjection();
        PointInt[] screenPoints = HyperrectangleUtils.convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, v2D);

        if (!isEmpty()) {
            if (numberOfDimensions == 1) {
                int sx0 = screenPoints[0].x;
                int sx1 = screenPoints[1].x;
                int sy = screenPoints[0].y;
                graphics2D.setColor(strokeColor);
                graphics2D.setStroke(stroke);
                graphics2D.drawLine(sx0, sy, sx1, sy);
                graphics2D.setStroke(new BasicStroke(stroke.getLineWidth(), stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit()));
                graphics2D.drawLine(sx0, sy - style1DHeightHalfPx, sx0, sy + style1DHeightHalfPx);
                graphics2D.drawLine(sx1, sy - style1DHeightHalfPx, sx1, sy + style1DHeightHalfPx);

            } else if (numberOfDimensions == 2) {
                if (fillColor != null) {
                    graphics2D.setColor(fillColor);
                    graphics2D.fillRect(screenPoints[0].x, screenPoints[2].y, screenPoints[1].x - screenPoints[0].x + 1, screenPoints[0].y - screenPoints[2].y + 1);
                }
                if (strokeColor != null) {
                    graphics2D.setColor(strokeColor);
                    graphics2D.setStroke(stroke);
                    graphics2D.drawRect(screenPoints[0].x, screenPoints[2].y, screenPoints[1].x - screenPoints[0].x, screenPoints[0].y - screenPoints[2].y);
                }
            } else if (numberOfDimensions == 3) {
                if (fillColor != null) {
                    graphics2D.setColor(fillColor);
                    graphics2D.fillPolygon(new int[]{screenPoints[4].x, screenPoints[6].x, screenPoints[2].x, screenPoints[3].x, screenPoints[1].x, screenPoints[5].x},
                                           new int[]{screenPoints[4].y, screenPoints[6].y, screenPoints[2].y, screenPoints[3].y, screenPoints[1].y, screenPoints[5].y},
                                           6);
                }
                if (strokeColor != null) {
                    graphics2D.setColor(strokeColor);
                    graphics2D.setStroke(stroke);
                    graphics2D.drawPolygon(new int[]{screenPoints[7].x, screenPoints[6].x, screenPoints[2].x, screenPoints[3].x, screenPoints[1].x, screenPoints[5].x, screenPoints[4].x, screenPoints[6].x},
                                           new int[]{screenPoints[7].y, screenPoints[6].y, screenPoints[2].y, screenPoints[3].y, screenPoints[1].y, screenPoints[5].y, screenPoints[4].y, screenPoints[6].y},
                                           8);
                    graphics2D.drawLine(screenPoints[3].x, screenPoints[3].y, screenPoints[7].x, screenPoints[7].y);
                    graphics2D.drawLine(screenPoints[7].x, screenPoints[7].y, screenPoints[5].x, screenPoints[5].y);
                }
            } else throw new UnsupportedOperationException("Not implemmented yet");
        }
    }

    @Override
    public Hyperrectangle clone()
    {
        return new Hyperrectangle(numberOfDimensions, size, offset);

    }

    private static class ParallelSegment
    {
        final int numberOfDimensions;
        double[] offset;
        final int parallelDimension;
        double length;

        //TODO: make it safer
        public ParallelSegment(int numberOfDimensions, double[] offset, int parallelDimension, double length)
        {
            this.numberOfDimensions = numberOfDimensions;
            this.offset = offset.clone();
            this.parallelDimension = parallelDimension;
            this.length = length;
        }

        public PointDouble[] get2DProjection()
        {
            double[] end = offset.clone();
            end[parallelDimension] += length;

            if (numberOfDimensions == 1)
                return new PointDouble[]{new PointDouble(offset[0], 0), new PointDouble(end[0], 0)};
            else if (numberOfDimensions == 2)
                return new PointDouble[]{new PointDouble(offset[0], offset[1]), new PointDouble(end[0], end[1])};
            else if (numberOfDimensions == 3) {
                double oZX = Math.round(offset[2] * zXRatio);
                double eZX = Math.round(end[2] * zXRatio);
                double oZY = Math.round(offset[2] * zYRatio);
                double eZY = Math.round(end[2] * zYRatio);

                return new PointDouble[]{
                    new PointDouble(offset[0] - oZX, offset[1] - oZY),
                    new PointDouble(end[0] - eZX, end[1] - eZY)};
            } else throw new UnsupportedOperationException("Oblique projection is supported only in 3 dimensional case.");
        }

        //TODO: remove this crap
        public boolean isBelow(int dimension, double position)
        {
            return offset[dimension] < position;
        }

        public int getParallelDimension()
        {
            return parallelDimension;
        }

        public static ParallelSegment[] split(ParallelSegment segment, double position)
        {
            double start = segment.offset[segment.parallelDimension];
            double end = start + segment.length;
            if (start > position) return new ParallelSegment[]{null, segment.clone()};
            else if (end < position) return new ParallelSegment[]{segment.clone(), null};
            else {
                double[] o2 = segment.offset.clone();
                o2[segment.parallelDimension] = position;

                return new ParallelSegment[]{
                    new ParallelSegment(segment.numberOfDimensions, segment.offset.clone(), segment.parallelDimension, position - start),
                    new ParallelSegment(segment.numberOfDimensions, o2, segment.parallelDimension, end - position)
                };
            }
        }

        @Override
        protected ParallelSegment clone()
        {
            return new ParallelSegment(numberOfDimensions, offset.clone(), parallelDimension, length);
        }
    }
}
