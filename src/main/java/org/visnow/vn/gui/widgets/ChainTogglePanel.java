/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ChainTogglePanel extends javax.swing.JPanel
{

    private javax.swing.JPanel centerPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToggleButton jToggleButton1;

    /**
     * Creates new form ChainTogglePanel
     */
    public ChainTogglePanel()
    {
        initComponents2();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setMinimumSize(new java.awt.Dimension(20, 80));
        setPreferredSize(new java.awt.Dimension(20, 80));
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    public boolean isSelected()
    {
        return jToggleButton1.isSelected();
    }

    public void setSelected(boolean sel)
    {
        jToggleButton1.setSelected(sel);
    }

    @Override
    public boolean isEnabled()
    {
        return jToggleButton1.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        jToggleButton1.setEnabled(enabled);
    }

    private void initComponents2()
    {
        jPanel1 = new javax.swing.JPanel()
        {
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                int w = getWidth();
                int h = getHeight();
                g.setColor(Color.BLACK);
                g.drawLine(w / 2, h - getInsets().bottom - 1, w / 2, getInsets().top + 5);
                g.drawLine(w / 2, getInsets().top + 5, w - getInsets().right - 5, getInsets().top + 5);
            }
        };

        jPanel2 = new javax.swing.JPanel()
        {
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                int w = getWidth();
                int h = getHeight();
                g.setColor(Color.BLACK);
                g.drawLine(w / 2, getInsets().top + 1, w / 2, h - getInsets().bottom - 5);
                g.drawLine(w / 2, h - getInsets().bottom - 5, w - getInsets().right - 5, h - getInsets().bottom - 5);
            }
        };

        java.awt.GridBagConstraints gridBagConstraints;

        centerPanel = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();

        setMinimumSize(new java.awt.Dimension(20, 50));
        setPreferredSize(new java.awt.Dimension(20, 50));
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);

        centerPanel.setMaximumSize(new java.awt.Dimension(20, 35));
        centerPanel.setLayout(new java.awt.BorderLayout());

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/widgets/chainoff.gif"))); // NOI18N
        jToggleButton1.setBorderPainted(false);
        jToggleButton1.setContentAreaFilled(false);
        jToggleButton1.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jToggleButton1.setMaximumSize(new java.awt.Dimension(20, 30));
        jToggleButton1.setMinimumSize(new java.awt.Dimension(20, 30));
        jToggleButton1.setPreferredSize(new java.awt.Dimension(20, 30));
        jToggleButton1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/widgets/chainon.gif"))); // NOI18N
        centerPanel.add(jToggleButton1, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(centerPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel2, gridBagConstraints);

    }

    public void addActionListener(ActionListener l)
    {
        jToggleButton1.addActionListener(l);
    }

    public void removeActionListener(ActionListener l)
    {
        jToggleButton1.removeActionListener(l);
    }

    public ActionListener[] getActionListeners()
    {
        return jToggleButton1.getActionListeners();
    }
}
