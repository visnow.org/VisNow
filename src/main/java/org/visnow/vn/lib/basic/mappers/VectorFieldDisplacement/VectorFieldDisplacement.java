/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.VectorFieldDisplacement;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.PointField;
import org.visnow.vn.lib.types.VNPointField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VectorFieldDisplacement extends OutFieldVisualizationModule
{

    protected Field inField;
    protected GUI computeUI = null;
    protected Params params;
    protected boolean fromGUI = false;
    protected PointField inPointField = null;
    protected RegularField inRegularField = null;
    protected IrregularField inIrregularField = null;
    protected float[] inCoords;
    protected float[] coords;
    protected boolean ignoreUI = false;

    public VectorFieldDisplacement()
    {
        parameters = params = new Params();
        outObj.setName("displacement");
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (ignoreUI || inField == null)
                    return;
                updateCoords();
                fromGUI = true;
                fieldGeometry.updateCoords();
                if (!params.isAdjusting())
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private void updateCoords()
    {
        float scale = params.getScale();
        DataArray da = inField.getComponent(params.getVectorComponent());
        float[] v = da.getRawFloatArray().getData();
        if (da.getVectorLength() == 3)
            for (int i = 0; i < coords.length; i++)
                coords[i] = inCoords[i] + scale * v[i];
        else {
            int n = (int) inField.getNNodes();
            int m = min(3, da.getVectorLength());
            for (int i = 0, k = 0, l = 0; i < n; i++, k += 3, l += da.getVectorLength())
                for (int j = 0; j < m; j++)
                    coords[k + j] = inCoords[k + j] + scale * v[l + j];
        }
    }

    private void updateUI()
    {
        ignoreUI = true;
        computeUI.setInData(inField);
        ignoreUI = false;
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        if (!fromGUI) {
            VNField input = ((VNField) getInputFirstValue("inField"));
            Field newInField = input.getField();
            if (newInField != null && inField != newInField) {
                inField = newInField;
                int cmp = -1;
                for (int i = 0; i < inField.getNComponents(); i++)
                    if (inField.getComponent(i).isNumeric() &&
                        inField.getComponent(i).getVectorLength() > 1 &&
                        inField.getComponent(i).getVectorLength() <= 3) {
                        cmp = i;
                        break;
                    }
                if (cmp == -1)
                    return;
                updateUI();
                outField = inField.cloneShallow();
                if (inField instanceof RegularField) {
                    inRegularField = (RegularField) inField;
                    outRegularField = (RegularField) outField;
                    updateUI();
                    if (inRegularField.getCurrentCoords() != null)
                        inCoords = inRegularField.getCurrentCoords().getData();
                    else
                        inCoords = inRegularField.getCoordsFromAffine().getData();
                } 
                else if (inField instanceof IrregularField) {
                    inIrregularField = (IrregularField) inField;
                    outIrregularField = (IrregularField) outField;
                    inCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
                }
                else if (inField instanceof PointField) {
                    inPointField = (PointField) inField;
                    outPointField = (PointField) outField;
                    inCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
                }
                outField.removeCoords();
                coords = new float[3 * (int) outField.getNNodes()];
                System.arraycopy(inCoords, 0, coords, 0, inCoords.length);
                outField.setCurrentCoords(new FloatLargeArray(coords));
                prepareOutputGeometry();
            }
        }
        if (inField.getComponent(params.getVectorComponent()) != null &&
            inField.getComponent(params.getVectorComponent()).getVectorLength() > 1 &&
            inField.getComponent(params.getVectorComponent()).getVectorLength() <= 3)
            updateCoords();

        if (outField != null && outField instanceof RegularField) {
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
        } else if (outField != null && outField instanceof IrregularField) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            setOutputValue("outPointField", null);
        } else if (outField != null && outField instanceof PointField) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", new VNPointField((PointField) outField));
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
        }
        show();
        fromGUI = false;
    }

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return computeUI.getFrameRenderedListener();
    }

}
