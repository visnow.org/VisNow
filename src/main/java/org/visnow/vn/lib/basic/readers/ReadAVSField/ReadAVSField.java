/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadAVSField;

import java.io.File;
import java.nio.ByteOrder;
import javax.swing.SwingUtilities;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.readers.ReadAVSField.ReadAVSFieldShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 *
 * @author Michal Lyczek, University of Warsaw, ICM
 */
public class ReadAVSField extends OutFieldVisualizationModule
{

    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    private Core core = new Core();
    
    public ReadAVSField()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI("Field reader", "Field file", "fld", "FLD");
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(BIG_ENDIAN, true)
        };
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    @Override
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        if (p.get(FILENAME).equals("")) {
            outField = null;
            show();
            setOutputValue("outRegularField", null);
        }
        else {
            File file = new File(parameters.get(FILENAME));
            if (!file.exists()) {
                VisNow.get().userMessageSend(this, "Error loading file", "File " + file.getAbsolutePath() + " does not exist.", Level.ERROR);
                outField = null;
                show();
                setOutputValue("outRegularField", null);
                return;
            }
            outRegularField = core.readField(p.get(FILENAME), parameters.get(BIG_ENDIAN) ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
            outField = outRegularField;
            if (outRegularField == null) {
                VisNow.get().userMessageSend(this, "Error loading file", "Cannot read file " + file.getAbsolutePath(), Level.ERROR);
                outField = null;
                show();
                setOutputValue("outRegularField", null);
                return;
            }
            VisNow.get().userMessageSend(this, "<html>File successfully loaded", outRegularField.toMultilineString(), Level.INFO);
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

}
