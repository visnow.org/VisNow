/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.mouse;

import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Tuple3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.BasicRegisteredDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceType;

/**
 * Only for DeviceManager. This class was created for DeviceManager to have a pointer to
 * IPassiveDevice which is mouse.
 * This device is always used and it is always owned by all Viewers. It also can be neither
 * registered nor unregistered : ]
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class DummyMouseRegisteredDevice extends BasicRegisteredDevice
{

    public static final String MOUSE_NAME = "MOUSE_DEVICE";
    public static final String FRIENDLY_MOUSE_NAME = "Mouse";
    public static DummyMouseRegisteredDevice theOnlyDevice = null;

    /**
     *
     */
    private DummyMouseRegisteredDevice()
    {
        super(MOUSE_NAME, FRIENDLY_MOUSE_NAME);

        /* just in case, check singleton */
        if (theOnlyDevice != null)
            throw new IllegalStateException("Cannot initialize a second DummyMouseDevice - " +
                " there can be only one mouse in the application! :) ");
    }

    public static DummyMouseRegisteredDevice getInstance()
    {
        if (theOnlyDevice == null)
            theOnlyDevice = new DummyMouseRegisteredDevice();
        return theOnlyDevice;
    }

    /**
     * Mouse device acts as if it was attached to all Viewers.
     * <p/>
     * @param owner Owner Viewer
     * <p>
     * @return always true
     */
    @Override
    public boolean isOwnedByMe(Object owner)
    {
        return true;
    }

    /**
     * Mouse device acts as if it was always used by someone.
     * <p/>
     * @return always true
     */
    @Override
    public boolean isUsed()
    {
        return true;
    }

    @Override
    public boolean isAttachable()
    {
        return false;
    }

    @Override
    public void getPosition(Tuple3f position)
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public void getVelocity(Tuple3f velocity)
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public void getTransform(Transform3D transform)
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public int getButton(int no) throws Exception
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public void getButtons(int[] buttons)
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public int getButtonsCount()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public int getPositionMeanUpdateRate()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public int getPositionInstUpdateRate()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public String getVendor()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public String getVersion()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public String getID()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    /**
     * No Java3D pointer, so this method is not used.
     */
    @Override
    public void close()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    public DeviceType getDeviceType()
    {
        return DeviceType.MOUSE_DEVICE;
    }

    @Override
    protected void onStartUsing()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }

    @Override
    protected void onEndUsing()
    {
        throw new UnsupportedOperationException("This operation is not supported by " + getDeviceFriendlyName());
    }
}
