/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.FlowVisualization;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.MatrixMath;

import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.utils.SwingInstancer;

import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.ProgressAgent;

import org.visnow.vn.geometries.objects.ColormapLegend;
import org.visnow.vn.geometries.parameters.ColormapLegendParameters;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;

import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamCore;
import org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowCore;
import org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPointParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPoints;
import org.visnow.vn.lib.utils.flowVisualizationUtils.ComputeRegularFieldStreamlines;
import org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesGeometryCore;
import static org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesShared.*;
import org.visnow.vn.lib.utils.flowVisualizationUtils.ComputeIrregularFieldStreamlines;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinePresentationParams;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 * <p>
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class FlowVisualization extends OutFieldVisualizationModule
{

    public static OutputEgg[] outputEggs = null;
    public static InputEgg[] inputEggs = null;

    protected IrregularField inIrregularField;
    protected RegularField inRegularField;
    protected DataArray vectors = null;
    protected FloatLargeArray pullVects = null;
    protected Field startField = null;
    protected int pickCoords = -1;
    protected int[] startPointsDims = null;
    protected int nStartPoints;
    protected int vlen = 3;
    protected float baseScale = 1;
    protected float avgVector = 1;
    protected GUI gui = null;
    protected boolean fromInput = true;
    protected boolean updating = false;

    protected Field inField = null;
    protected Field auxField;
    protected SeedPointParams seedPointParams = new SeedPointParams();
    protected SeedPoints seedPoints = new SeedPoints(seedPointParams, presentationParams);

    protected StreamlinesGeometryCore streamlines;

    protected int runQueue = 0;

    protected float lastTime = -Float.MAX_VALUE;
    protected float currentTime = 0;

    protected ProgressAgent progressAgent;
    protected StreamlinePresentationParams streamlinePresentationParams = new StreamlinePresentationParams();
    protected AnimatedStreamCore animatedStreamCore;
    protected AnimatedStreamParams animatedStreamParams = new AnimatedStreamParams();

    protected GlyphFlowCore glyphFlowCore;
    protected GlyphFlowParams glyphFlowParams = new GlyphFlowParams();

    protected ColormapLegend colormapLegend = new ColormapLegend();

    int counter = 0;

    public FlowVisualization()
    {
        FrameModificationListener frameModificationListener = new FrameModificationListener()
        {
            @Override
            public void frameChanged(FrameModificationEvent e)
            {
                if (inField == null)
                    return;
                currentTime = e.getTimeFrame();
                update();
                if (outField != null) {
                    outField.setCurrentTime(currentTime);
                    show();
                }
            }
        };
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (ANIMATION_TYPE.getName().equals(name))
                    updateVisualization();
                else if (SETTING_SEED_POINTS.getName().equals(name))
                    updateGlyphsVisibility();
                else if (!fromInput && !updating)
                    startAction();
            }
        });

        seedPoints.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (!fromInput && !updating)
                    startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                gui = new GUI();
                gui.setParameterProxy(parameters);
                gui.setSeedPointParams(seedPointParams);
                gui.setPresentationParams(streamlinePresentationParams, animatedStreamParams, glyphFlowParams);
                gui.setDataMappingParams(dataMappingParams);
                setPanel(gui);
            }
        });
        ColormapLegendParameters legendParams = dataMappingParams.getColormapLegendParameters();
        colormapLegend.setParams(legendParams);
        outObj.addGeometry2D(colormapLegend);
        outObj.setName("Streamlines");
        seedPointParams.setPreferredSizes(1, 10000, 100);
        seedPoints.setGUI(gui.getSeedPointsGUI());
    }

    protected FrameRenderedListener frameRenderedListener = new FrameRenderedListener()
    {
        @Override
        public void frameRendered(FrameRenderedEvent e)
        {
            if (animatedStreamCore != null)
                animatedStreamCore.setRenderDone(true);
            if (glyphFlowCore != null)
                glyphFlowCore.setRenderDone(true);
        }
    };

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return frameRenderedListener;
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
       return  new Parameter[]{
            new Parameter<>(COMPONENT, "--------"),
            new Parameter<>(STEP, 0.002f),
            new Parameter<>(NUM_STEPS_FORWARD, 200),
            new Parameter<>(NUM_STEPS_BACKWARD, 0),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY),
            new Parameter<>(META_IS_START_POINT_FIELD_REGULAR, true),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{"--------"}),
            new Parameter<>(META_PREFERRED_STEPS, new float[]{1}),
            new Parameter<>(COMPUTE_PERIODICITY, false),
            new Parameter<>(SETTING_SEED_POINTS, true),
            new Parameter<>(ANIMATION_TYPE, 0)
        };
    }

    protected void validateParametersAndSetSmart(boolean resetAll)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_IS_START_POINT_FIELD_REGULAR, auxField instanceof RegularField);
        String[] vectorComponentNames = inField.getVectorComponentNames();
        parameters.set(META_VECTOR_COMPONENT_NAMES, vectorComponentNames);

        if (resetAll) {
            parameters.set(COMPONENT, vectorComponentNames[0]);
            //calculate preferred steps
            parameters.set(NUM_STEPS_FORWARD, 200);
            parameters.set(NUM_STEPS_BACKWARD, 0);
            parameters.set(COMPUTE_PERIODICITY, false);
            parameters.set(ANIMATION_TYPE, 0);
        }
        parameters.setParameterActive(true);
    }

    protected void updateGlyphsVisibility() {
        if (parameters.get(SETTING_SEED_POINTS))
            outObj.addNode(seedPoints.getSeedPointsGroup());
        else
            outObj.removeNode(seedPoints.getSeedPointsGroup());
    }

    protected void updateVisualization()
    {
        if (outIrregularField == null)
            return;
        switch (parameters.get(ANIMATION_TYPE)) {
            case NO_ANIMATION:
                outObj.clearAllGeometry();
                outObj.addNode(streamlines.getGeometry());
                break;
            case STREAM_ANIMATION:
                animatedStreamParams.clearParameterChangeListeners();
                animatedStreamCore = new AnimatedStreamCore(outIrregularField,
                                                            animatedStreamParams,
                                                            dataMappingParams);
                animatedStreamCore.updateGeometry();
                outObj.clearAllGeometry();
                outObj.addNode(animatedStreamCore.getGeometry());
                break;
            case GLYPH_ANIMATION:
                outObj.clearAllGeometry();
                gui.getGlyphFlowGUI().setInData(outIrregularField, startPointsDims);
                gui.getGlyphFlowGUI().setSelectedVectorComponentName(parameters.get(COMPONENT));
                glyphFlowCore = new GlyphFlowCore(outIrregularField,
                                                  glyphFlowParams,
                                                  dataMappingParams);
                glyphFlowCore.addUIChangeListener(gui.getGlyphFlowGUI().getAnimationStateListener());
                glyphFlowCore.updateColors();
                outObj.addNode(streamlines.getGeometry());
                outObj.addNode(glyphFlowCore.getGeometry());
        }
    }

    public synchronized void pullVectors()
    {
        int[] dims = inRegularField.getDims();
        FloatLargeArray vects = inField.getComponent(parameters.get(COMPONENT)).getRawFloatArray();
        int vlen = inField.getComponent(parameters.get(COMPONENT)).getVectorLength();
        FloatLargeArray fldCoords = inRegularField.getCoords(0);
        if (fldCoords == null) {
            pullVects = new FloatLargeArray(vects.length());
            float[][] iaf = inRegularField.getInvAffine();
            int n = inRegularField.getDimNum();
            float[][] pullMatrix = new float[n][n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < pullMatrix.length; j++)
                    pullMatrix[i][j] = iaf[j][i];
            float[] v = new float[vlen];
            for (long i = 0; i < vects.length(); i += vlen) {
                for (int k = 0; k < vlen; k++)
                    v[k] = vects.get(i + k);
                float[] w = MatrixMath.matrixVectorMult(pullMatrix, v);
                for (int k = 0; k < vlen; k++)
                    pullVects.set(i + k, w[k]);
            }
        } else
            pullVects = MatrixMath.pullVectorField(dims, fldCoords, vects);
    }

    protected void estimateScale(DataArray vectDataArray)
    {
        LargeArray vects = vectDataArray.getRawArray();
        avgVector = 0;
        long nValid = 0;
        if (inField.hasMask()) {
            LogicLargeArray mask = inField.getMask(inField.getCurrentTime());
            for (long i = 0; i < vects.length(); i++)
                if (mask.get(i / vlen) != 0) {
                    nValid += 1;
                    avgVector += vects.getFloat(i) * vects.getFloat(i);
                }
        }
        else {
            nValid = inField.getNNodes();
            for (long i = 0; i < vects.length(); i++)
                avgVector += vects.getFloat(i) * vects.getFloat(i);
        }
        avgVector = (float) Math.sqrt(avgVector / nValid);
        float[][] extents = inField.getExtents();
        if (inField.hasCoords()) {
            Arrays.fill(extents[0],  Float.MAX_VALUE);
            Arrays.fill(extents[1], -Float.MAX_VALUE);
            FloatLargeArray crds = inField.getCurrentCoords();
            for (long i = 0; i < crds.length(); i += 3)
                for (int j = 0; j < 3; j++) {
                    float c = crds.get(i + j);
                    if (c < extents[0][j])
                        extents[0][j] = c;
                    if (c > extents[1][j])
                        extents[1][j] = c;
                }
        }
        float xt = 0;
        for (int i = 0; i < inField.getTrueNSpace(); i++)
            xt += (extents[1][i] - extents[0][i]) * (extents[1][i] - extents[0][i]);
        baseScale = (float)Math.sqrt(xt) / avgVector;
    }

    protected Field lastInField = null;

    protected void update()
    {
        updating = true;
        startField = seedPoints.getSeedField();
        if (startField == null ) {
            updating = false;
            return;
        }
        nStartPoints = (int)startField.getNNodes();
        if (startField instanceof RegularField)
            startPointsDims = ((RegularField)startField).getDims();
        // 4. Run computation and propagate.
        if (inField != lastInField) {
            lastInField = inField;
            if (inField instanceof RegularField) {
                inIrregularField = null;
                inRegularField = (RegularField) inField;
            } else {
                inRegularField = null;
                inIrregularField = (IrregularField) inField;
            }
        }
        DataArray currentVectors = inField.getComponent(parameters.get(COMPONENT));
        if (currentVectors != vectors || inField.getCurrentTime() != lastTime) {
            if (inField instanceof RegularField) {
                vectors = currentVectors;
                pullVectors();
                progressAgent.setProgress(.5);
                estimateScale(vectors);
                progressAgent.setProgress(.7);
            }
            else {
                vectors = currentVectors;
                estimateScale(vectors);
                progressAgent.setProgress(.7);
            }
            lastTime = inField.getCurrentTime();
        }
        if (inField instanceof RegularField)
            streamlines = new ComputeRegularFieldStreamlines(inRegularField, baseScale, pullVects,
                                                             parameters, streamlinePresentationParams,
                                                             startField, progressAgent);
        else
            streamlines = new ComputeIrregularFieldStreamlines(inIrregularField, baseScale,
                                                               parameters, streamlinePresentationParams,
                                                               startField, progressAgent);
        streamlines.updateStreamlines();
        outIrregularField = streamlines.getOutField();
        if (outIrregularField != null) {
            if (startField instanceof RegularField) {
                StringBuilder d = new StringBuilder("[");
                for (int i = 0; i < startPointsDims.length; i++)
                    d.append(String.format(" %d ", startPointsDims[i]));
                d.append("]");
                outIrregularField.setUserData(new String[] {"streamlines field: " +
                                                            startField.getNNodes() + " streamlines",
                                                            d.toString()});
            }
            else
                outIrregularField.setUserData(new String[] {"streamlines field: " +
                                                            streamlines.getNTrajects() + " streamlines" });
            outIrregularField.getComponent("steps").setAutoResetMapRange(true);
            if (parameters.get(COMPUTE_PERIODICITY))
                outIrregularField.addComponent(streamlines.computeApproximatePeriodicity());
            dataMappingParams.setNodeData(outIrregularField);
            dataMappingParams.getColorMap0().getComponentRange().setComponentSchema(parameters.get(COMPONENT));
            streamlines.setDataMappingParams(dataMappingParams);
            streamlines.createStreamlinesGeometry();
            progressAgent.setProgress(.9);
            outObj.clearAllGeometry();
            updateVisualization();
            updateGlyphsVisibility();
            dataMappingParams.getColorMap0().addRenderEventListener((RenderEvent e) -> {
                if (streamlines != null)
                    streamlines.updateColors();
                if (animatedStreamCore != null)
                    animatedStreamCore.updateColors();
                if (glyphFlowCore != null)
                    glyphFlowCore.updateColors();
            });
            dataMappingParams.getTransparencyParams().addListener((RenderEvent e) -> {
                if (streamlines != null)
                    streamlines.updateColors();
            });
            gui.streamlinesStatus(true);
        }
        updating = false;
    }

    @Override
    public void onActive()
    {
        progressAgent = getProgressAgent(1000);
        fromInput = true;
        if (getInputFirstValue("inField") != null) {
            switchPanelToGUI();
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            if (newInField == null)
                return;
//            Field newAuxField = null;
//            int newPickCoords = -1;
//            if (getInputFirstValue("startPoints") != null)
//                newAuxField = ((VNField) getInputFirstValue("startPoints")).getField();
//            if (getInputFirstValue("pick coords") != null)
//                newPickCoords = (Integer)getInputFirstValue("pick coords");
//            else
//                newPickCoords = -1;
//            if (getInputFirstValue("time") != null)
//                currentTime = (Float)getInputFirstValue("time");
            boolean isDifferentInField = !isFromVNA() && newInField != inField;
//            boolean isDifferentAuxField =
//                    !isFromVNA() &&  newAuxField != auxField;
//            boolean isNewPick =
//                    !isFromVNA() &&  newPickCoords != pickCoords;
            inField = newInField;
//            auxField = newAuxField;
            if (isDifferentInField) {
                seedPointParams.setInputField(inField);
                progressAgent.setProgress(.25);
                seedPoints.setInField(inField);
                progressAgent.setProgress(.5);
                vectors = null;
                if (inField instanceof RegularField)
                    inRegularField = (RegularField) inField;
            }
//            pickCoords = newPickCoords;
//            if (isDifferentAuxField) {
//                seedPointParams.setAuxField(auxField);
//                seedPoints.setAuxField(auxField);
//            }
//            if (isNewPick)
//                seedPointParams.setPick(pickCoords);
//            if (isDifferentInField || isDifferentAuxField  || isNewPick)
//            if (isDifferentInField)
//                seedPoints.setInField(inField);
            validateParametersAndSetSmart(isDifferentInField);
            gui.update(parameters, false);
            gui.streamlinesStatus(false);
            update();
            fromInput = false;
            if (outIrregularField != null)
                setOutputValue("streamlinesField", new VNIrregularField(outIrregularField));
            else
                setOutputValue("streamlinesField", null);
        } else
            this.switchPanelToDummy();
    }

    public SeedPointParams getSeedPointParams()
    {
        return seedPointParams;
    }

    public StreamlinePresentationParams getStreamlinePresentationParams()
    {
        return streamlinePresentationParams;
    }

    public AnimatedStreamParams getAnimatedStreamParams()
    {
        return animatedStreamParams;
    }

    public GlyphFlowParams getGlyphFlowParams()
    {
        return glyphFlowParams;
    }

    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

}
