/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AddMargins;

import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.lib.basic.filters.AddMargins.AddMarginsShared.*;
import org.visnow.jvia.spatialops.Padding.PaddingType;

public class GUI extends javax.swing.JPanel
{
    //XXX: this should be removed (added to fix runButton wait state)
    boolean jListActive = true;

    private Parameters parameters;

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
    }

    void setParameters(Parameters parameters)
    {
//        runButton1.setPar
//        this.parameters = runButton1.getProxy);
        this.parameters = parameters;

    }

    void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        long[][] margins = p.get(MARGINS);
        iTFLow.setValue(margins[0][0]);
        iTFHigh.setValue(margins[0][1]);
        jTFLow.setValue(margins[1][0]);
        jTFHigh.setValue(margins[1][1]);
        kTFLow.setValue(margins[2][0]);
        kTFHigh.setValue(margins[2][1]);

        PaddingType padding = p.get(PADDING_TYPE);
        zeroButton.setSelected(padding == PaddingType.ZERO);
        fixedButton.setSelected(padding == PaddingType.FIXED);
        periodicButton.setSelected(padding == PaddingType.PERIODIC);
        reflectedButton.setSelected(padding == PaddingType.REFLECTED);

        reflectVectorsBox.setSelected(p.get(REFLECT_VECTORS));
        updateReflectedCB();

        jListActive = false;
        int[] selectedComponentIndices = p.get(SELECTED_COMPONENTS);
        componentList.setListData(p.get(META_COMPONENT_NAMES));
        componentList.setSelectedIndices(selectedComponentIndices);
        jListActive = true;

        runButton1.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton1.updatePendingState(setRunButtonPending);
    }

    private void updateReflectedCB()
    {
        reflectVectorsBox.setEnabled(reflectedButton.isSelected());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        marginsLabel = new javax.swing.JLabel();
        marginsPanel = new javax.swing.JPanel();
        iLabel = new javax.swing.JLabel();
        iTFLow = new org.visnow.vn.gui.components.NumericTextField();
        iTFHigh = new org.visnow.vn.gui.components.NumericTextField();
        jLabel = new javax.swing.JLabel();
        jTFLow = new org.visnow.vn.gui.components.NumericTextField();
        jTFHigh = new org.visnow.vn.gui.components.NumericTextField();
        kLabel = new javax.swing.JLabel();
        kTFLow = new org.visnow.vn.gui.components.NumericTextField();
        kTFHigh = new org.visnow.vn.gui.components.NumericTextField();
        componentsLabel = new javax.swing.JLabel();
        componentList = new javax.swing.JList();
        extensionLabel = new javax.swing.JLabel();
        extensionPanel = new javax.swing.JPanel();
        zeroButton = new javax.swing.JRadioButton();
        periodicButton = new javax.swing.JRadioButton();
        fixedButton = new javax.swing.JRadioButton();
        reflectedButton = new javax.swing.JRadioButton();
        reflectVectorsBox = new javax.swing.JCheckBox();
        runButton1 = new org.visnow.vn.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0));

        setLayout(new java.awt.GridBagLayout());

        marginsLabel.setText("Margins:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(marginsLabel, gridBagConstraints);

        marginsPanel.setLayout(new java.awt.GridBagLayout());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/visnow/vn/lib/basic/filters/AddMargins/Bundle"); // NOI18N
        iLabel.setText(bundle.getString("GUI.iLabel.text")); // NOI18N
        marginsPanel.add(iLabel, new java.awt.GridBagConstraints());

        iTFLow.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        iTFLow.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        iTFLow.setMin(0);
        iTFLow.setValue(1);
        iTFLow.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 4, 0);
        marginsPanel.add(iTFLow, gridBagConstraints);

        iTFHigh.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        iTFHigh.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        iTFHigh.setMin(0);
        iTFHigh.setValue(1);
        iTFHigh.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 4, 0);
        marginsPanel.add(iTFHigh, gridBagConstraints);

        jLabel.setText(bundle.getString("GUI.jLabel.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        marginsPanel.add(jLabel, gridBagConstraints);

        jTFLow.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTFLow.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        jTFLow.setMin(0);
        jTFLow.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 4, 0);
        marginsPanel.add(jTFLow, gridBagConstraints);

        jTFHigh.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTFHigh.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        jTFHigh.setMin(0);
        jTFHigh.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 4, 0);
        marginsPanel.add(jTFHigh, gridBagConstraints);

        kLabel.setText(bundle.getString("GUI.kLabel.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        marginsPanel.add(kLabel, gridBagConstraints);

        kTFLow.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        kTFLow.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        kTFLow.setMin(0);
        kTFLow.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        marginsPanel.add(kTFLow, gridBagConstraints);

        kTFHigh.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        kTFHigh.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        kTFHigh.setMin(0);
        kTFHigh.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                tfUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        marginsPanel.add(kTFHigh, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 4);
        add(marginsPanel, gridBagConstraints);

        componentsLabel.setText(bundle.getString("GUI.componentsLabel.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(componentsLabel, gridBagConstraints);

        componentList.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")));
        componentList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                componentListValueChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 4);
        add(componentList, gridBagConstraints);

        extensionLabel.setText(bundle.getString("GUI.extensionLabel.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(extensionLabel, gridBagConstraints);

        extensionPanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(zeroButton);
        zeroButton.setSelected(true);
        zeroButton.setText(bundle.getString("GUI.zeroButton.text")); // NOI18N
        zeroButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extensionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        extensionPanel.add(zeroButton, gridBagConstraints);

        buttonGroup1.add(periodicButton);
        periodicButton.setText(bundle.getString("GUI.periodicButton.text")); // NOI18N
        periodicButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extensionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        extensionPanel.add(periodicButton, gridBagConstraints);

        buttonGroup1.add(fixedButton);
        fixedButton.setText(bundle.getString("GUI.fixedButton.text")); // NOI18N
        fixedButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extensionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        extensionPanel.add(fixedButton, gridBagConstraints);

        buttonGroup1.add(reflectedButton);
        reflectedButton.setText(bundle.getString("GUI.reflectedButton.text")); // NOI18N
        reflectedButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extensionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        extensionPanel.add(reflectedButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 4);
        add(extensionPanel, gridBagConstraints);

        reflectVectorsBox.setText(bundle.getString("GUI.reflectVectorsBox.text")); // NOI18N
        reflectVectorsBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        reflectVectorsBox.setPreferredSize(null);
        reflectVectorsBox.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        reflectVectorsBox.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        reflectVectorsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                reflectVectorsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        add(reflectVectorsBox, gridBagConstraints);

        runButton1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                runButton1UserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButton1UserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_runButton1UserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButton1UserChangeAction

    private void extensionRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_extensionRBActionPerformed
    {//GEN-HEADEREND:event_extensionRBActionPerformed
        if (fixedButton.isSelected()) parameters.set(PADDING_TYPE, PaddingType.FIXED);
        else if (periodicButton.isSelected()) parameters.set(PADDING_TYPE, PaddingType.PERIODIC);
        else if (reflectedButton.isSelected()) parameters.set(PADDING_TYPE, PaddingType.REFLECTED);
        else parameters.set(PADDING_TYPE, PaddingType.ZERO);
        updateReflectedCB();
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_extensionRBActionPerformed

    private void reflectVectorsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_reflectVectorsBoxActionPerformed
    {//GEN-HEADEREND:event_reflectVectorsBoxActionPerformed
        parameters.set(REFLECT_VECTORS, reflectVectorsBox.isSelected());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_reflectVectorsBoxActionPerformed

    private void tfUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_tfUserChangeAction
    {//GEN-HEADEREND:event_tfUserChangeAction
        parameters.set(MARGINS, new long[][]{
            new long[]{(int) iTFLow.getValue(), (int) iTFHigh.getValue()},
            new long[]{(int) jTFLow.getValue(), (int) jTFHigh.getValue()},
            new long[]{(int) kTFLow.getValue(), (int) kTFHigh.getValue()}
        });
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_tfUserChangeAction

    private void componentListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_componentListValueChanged
    {//GEN-HEADEREND:event_componentListValueChanged
        if (jListActive && !evt.getValueIsAdjusting()) {
            parameters.set(SELECTED_COMPONENTS, componentList.getSelectedIndices());
            runButton1.setPendingIfNoAuto();
        }
    }//GEN-LAST:event_componentListValueChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JList componentList;
    private javax.swing.JLabel componentsLabel;
    private javax.swing.JLabel extensionLabel;
    private javax.swing.JPanel extensionPanel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JRadioButton fixedButton;
    private javax.swing.JLabel iLabel;
    private org.visnow.vn.gui.components.NumericTextField iTFHigh;
    private org.visnow.vn.gui.components.NumericTextField iTFLow;
    private javax.swing.JLabel jLabel;
    private org.visnow.vn.gui.components.NumericTextField jTFHigh;
    private org.visnow.vn.gui.components.NumericTextField jTFLow;
    private javax.swing.JLabel kLabel;
    private org.visnow.vn.gui.components.NumericTextField kTFHigh;
    private org.visnow.vn.gui.components.NumericTextField kTFLow;
    private javax.swing.JLabel marginsLabel;
    private javax.swing.JPanel marginsPanel;
    private javax.swing.JRadioButton periodicButton;
    private javax.swing.JCheckBox reflectVectorsBox;
    private javax.swing.JRadioButton reflectedButton;
    private org.visnow.vn.gui.widgets.RunButton runButton1;
    private javax.swing.JRadioButton zeroButton;
    // End of variables declaration//GEN-END:variables

}
