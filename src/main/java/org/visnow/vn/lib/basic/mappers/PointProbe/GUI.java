/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.PointProbe;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.mappers.PointProbe.PointProbeShared.*;
import org.visnow.vn.lib.utils.graphing.ComponentDisplayController;
import org.visnow.vn.lib.utils.graphing.GraphParams;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;

public class GUI extends javax.swing.JPanel
{

    private Parameters parameters;
    private GraphParams graphParams;

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
    }
    
    void setParameters(Parameters parameters, GraphParams graphParams)
    {
        this.parameters = parameters;
        this.graphParams = graphParams;
        componentDisplayController.setParams(graphParams);
    }
    
    
     void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        probePane = new javax.swing.JPanel();
        glyphControlPanel = new javax.swing.JPanel();
        indexProbePanel = new javax.swing.JPanel();
        geomProbePanel = new javax.swing.JPanel();
        probeTypePanel = new javax.swing.JPanel();
        geomProbeSelector = new javax.swing.JRadioButton();
        indexProbeSelector = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        sliceAddButton = new javax.swing.JButton();
        sliceRemoveButton = new javax.swing.JButton();
        slicesClearButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        fontSizeSlider = new javax.swing.JSlider();
        jLabel6 = new javax.swing.JLabel();
        positionCombo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        pointerLinesButton = new javax.swing.JRadioButton();
        sliceNumbersButton = new javax.swing.JRadioButton();
        timeSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        windowBgrButton = new javax.swing.JRadioButton();
        userBgrButton = new javax.swing.JRadioButton();
        bgrColorEditor = new org.visnow.vn.gui.widgets.ColorEditor();
        bgrTransparencySlider = new javax.swing.JSlider();
        frameBox = new javax.swing.JCheckBox();
        fillBox = new javax.swing.JCheckBox();
        componentDisplayController = new org.visnow.vn.lib.utils.graphing.ComponentDisplayController();

        setLayout(new java.awt.GridBagLayout());

        probePane.setLayout(new java.awt.GridBagLayout());

        glyphControlPanel.setMinimumSize(new java.awt.Dimension(180, 370));
        glyphControlPanel.setPreferredSize(new java.awt.Dimension(250, 400));
        glyphControlPanel.setLayout(new java.awt.GridBagLayout());

        indexProbePanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        glyphControlPanel.add(indexProbePanel, gridBagConstraints);

        geomProbePanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        glyphControlPanel.add(geomProbePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        probePane.add(glyphControlPanel, gridBagConstraints);

        probeTypePanel.setLayout(new java.awt.GridLayout(1, 0));

        buttonGroup1.add(geomProbeSelector);
        geomProbeSelector.setSelected(true);
        geomProbeSelector.setText("geometric"); // NOI18N
        geomProbeSelector.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                geomProbeSelectorActionPerformed(evt);
            }
        });
        probeTypePanel.add(geomProbeSelector);

        buttonGroup1.add(indexProbeSelector);
        indexProbeSelector.setText("by index"); // NOI18N
        indexProbeSelector.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                indexProbeSelectorActionPerformed(evt);
            }
        });
        probeTypePanel.add(indexProbeSelector);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        probePane.add(probeTypePanel, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridLayout(3, 0));

        sliceAddButton.setText("add current probe"); // NOI18N
        sliceAddButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sliceAddButtonActionPerformed(evt);
            }
        });
        jPanel5.add(sliceAddButton);

        sliceRemoveButton.setText("remove selected probe"); // NOI18N
        sliceRemoveButton.setToolTipText("click graph to select corresponding probe"); // NOI18N
        sliceRemoveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sliceRemoveButtonActionPerformed(evt);
            }
        });
        jPanel5.add(sliceRemoveButton);

        slicesClearButton.setText("clear all probes"); // NOI18N
        slicesClearButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                slicesClearButtonActionPerformed(evt);
            }
        });
        jPanel5.add(slicesClearButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 27, 0);
        probePane.add(jPanel5, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        probePane.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(probePane, gridBagConstraints);

        fontSizeSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        fontSizeSlider.setMajorTickSpacing(5);
        fontSizeSlider.setMaximum(-30);
        fontSizeSlider.setMinimum(-70);
        fontSizeSlider.setPaintTicks(true);
        fontSizeSlider.setValue(-50);
        fontSizeSlider.setMinimumSize(new java.awt.Dimension(100, 30));
        fontSizeSlider.setPreferredSize(new java.awt.Dimension(200, 30));
        fontSizeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                fontSizeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        add(fontSizeSlider, gridBagConstraints);

        jLabel6.setText("font size"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        add(jLabel6, gridBagConstraints);

        positionCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "at point", "top", "right", "bottom", "left" }));
        positionCombo.setSelectedIndex(2);
        positionCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                positionComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        add(positionCombo, gridBagConstraints);

        jLabel9.setText("labels position"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(jLabel9, gridBagConstraints);

        pointerLinesButton.setSelected(true);
        pointerLinesButton.setText("pointer lines"); // NOI18N
        pointerLinesButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                pointerLinesButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        add(pointerLinesButton, gridBagConstraints);

        sliceNumbersButton.setText("slice numbers"); // NOI18N
        sliceNumbersButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sliceNumbersButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        add(sliceNumbersButton, gridBagConstraints);

        timeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("time"));
        timeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                timeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(timeSlider, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.2;
        add(jPanel2, gridBagConstraints);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("frame and background"));
        jPanel4.setMinimumSize(new java.awt.Dimension(200, 152));
        jPanel4.setPreferredSize(new java.awt.Dimension(280, 152));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        windowBgrButton.setSelected(true);
        windowBgrButton.setText("window background color");
        windowBgrButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                windowBgrButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel4.add(windowBgrButton, gridBagConstraints);

        userBgrButton.setText("user color");
        userBgrButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                userBgrButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 76;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel4.add(userBgrButton, gridBagConstraints);

        bgrColorEditor.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        bgrColorEditor.setBrightness(0);
        bgrColorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                bgrColorEditorStateChanged(evt);
            }
        });

        javax.swing.GroupLayout bgrColorEditorLayout = new javax.swing.GroupLayout(bgrColorEditor);
        bgrColorEditor.setLayout(bgrColorEditorLayout);
        bgrColorEditorLayout.setHorizontalGroup(
            bgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 86, Short.MAX_VALUE)
        );
        bgrColorEditorLayout.setVerticalGroup(
            bgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 22, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 49;
        gridBagConstraints.ipady = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 7);
        jPanel4.add(bgrColorEditor, gridBagConstraints);

        bgrTransparencySlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "opacity", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog.plain", 1, 12))); // NOI18N
        bgrTransparencySlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                bgrTransparencySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 243;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 6);
        jPanel4.add(bgrTransparencySlider, gridBagConstraints);

        frameBox.setText("draw frame");
        frameBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                frameBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel4.add(frameBox, gridBagConstraints);

        fillBox.setSelected(true);
        fillBox.setText("fill area");
        fillBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                fillBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel4.add(fillBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(jPanel4, gridBagConstraints);

        componentDisplayController.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(componentDisplayController, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    
    private void sliceAddButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_sliceAddButtonActionPerformed
    {//GEN-HEADEREND:event_sliceAddButtonActionPerformed
        if (parameters == null)
            return;
        parameters.set(ADD_PROBE, true);
    }//GEN-LAST:event_sliceAddButtonActionPerformed

    private void slicesClearButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_slicesClearButtonActionPerformed
    {//GEN-HEADEREND:event_slicesClearButtonActionPerformed
        if (parameters == null)
            return;
        parameters.set(CLEAR_PROBES, true);
    }//GEN-LAST:event_slicesClearButtonActionPerformed

    private void sliceRemoveButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_sliceRemoveButtonActionPerformed
    {//GEN-HEADEREND:event_sliceRemoveButtonActionPerformed
       if (parameters == null)
            return;
       parameters.set(DEL_PROBE, true);
    }//GEN-LAST:event_sliceRemoveButtonActionPerformed

    private void indexProbeSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_indexProbeSelectorActionPerformed
        if (parameters == null)
            return;
        parameters.set(PROBE_TYPE, geomProbeSelector.isSelected() ? ProbeType.GEOMETRIC : ProbeType.INDEX);
        updateGlyphControlPanel();
    }//GEN-LAST:event_indexProbeSelectorActionPerformed

    private void geomProbeSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_geomProbeSelectorActionPerformed
        if (parameters == null)
            return;
        parameters.set(PROBE_TYPE, geomProbeSelector.isSelected() ? ProbeType.GEOMETRIC : ProbeType.INDEX);
        updateGlyphControlPanel();
    }//GEN-LAST:event_geomProbeSelectorActionPerformed

    private void fontSizeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_fontSizeSliderStateChanged
    {//GEN-HEADEREND:event_fontSizeSliderStateChanged
        if (parameters != null && !fontSizeSlider.getValueIsAdjusting())
        {
            double f = fontSizeSlider.getValue() / 30.;
            graphParams.setFontSize((float)Math.pow(10, f));
        }
    }//GEN-LAST:event_fontSizeSliderStateChanged

    private void positionComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_positionComboItemStateChanged
    {//GEN-HEADEREND:event_positionComboItemStateChanged
        switch ((String)positionCombo.getSelectedItem())
        {
        case "at point":
            parameters.set(LABEL_POSITION, AT_POINT);
            break;
        case "top":
            parameters.set(LABEL_POSITION, TOP);
            break;
        case "right":
            parameters.set(LABEL_POSITION, RIGHT);
            break;
        case "bottom":
            parameters.set(LABEL_POSITION, BOTTOM);
            break;
        case "left":
            parameters.set(LABEL_POSITION, LEFT);
            break;
        }
    }//GEN-LAST:event_positionComboItemStateChanged

    private void pointerLinesButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_pointerLinesButtonActionPerformed
    {//GEN-HEADEREND:event_pointerLinesButtonActionPerformed
        parameters.set(POINTER_LINE, pointerLinesButton.isSelected());
    }//GEN-LAST:event_pointerLinesButtonActionPerformed

    private void sliceNumbersButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_sliceNumbersButtonActionPerformed
    {//GEN-HEADEREND:event_sliceNumbersButtonActionPerformed
        parameters.set(POINTER_LINE, pointerLinesButton.isSelected());
    }//GEN-LAST:event_sliceNumbersButtonActionPerformed

    private void timeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_timeSliderStateChanged
    {//GEN-HEADEREND:event_timeSliderStateChanged
//        parameters.set(TIME, timeSlider.getVal());
    }//GEN-LAST:event_timeSliderStateChanged

    private void windowBgrButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_windowBgrButtonActionPerformed
    {//GEN-HEADEREND:event_windowBgrButtonActionPerformed
        bgrSelect();
    }//GEN-LAST:event_windowBgrButtonActionPerformed

    private void userBgrButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_userBgrButtonActionPerformed
    {//GEN-HEADEREND:event_userBgrButtonActionPerformed
        bgrSelect();
    }//GEN-LAST:event_userBgrButtonActionPerformed

    private void bgrColorEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_bgrColorEditorStateChanged
    {//GEN-HEADEREND:event_bgrColorEditorStateChanged
        if (graphParams != null)
            graphParams.setUserBgrColor(bgrColorEditor.getColor());
    }//GEN-LAST:event_bgrColorEditorStateChanged

    private void bgrTransparencySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_bgrTransparencySliderStateChanged
    {//GEN-HEADEREND:event_bgrTransparencySliderStateChanged
        if (graphParams != null)
            graphParams.setBgrTransparency(.01f * bgrTransparencySlider.getValue());
    }//GEN-LAST:event_bgrTransparencySliderStateChanged

    private void frameBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_frameBoxActionPerformed
    {//GEN-HEADEREND:event_frameBoxActionPerformed
       if (graphParams != null)
           graphParams.setDrawFrame(frameBox.isSelected());
    }//GEN-LAST:event_frameBoxActionPerformed

    private void fillBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_fillBoxActionPerformed
    {//GEN-HEADEREND:event_fillBoxActionPerformed
        boolean fill = fillBox.isSelected();
        if (graphParams != null)
            graphParams.setFillRect(fill); 
        bgrColorEditor.setEnabled(fill);
        windowBgrButton.setEnabled(fill);
        userBgrButton.setEnabled(fill);
        bgrTransparencySlider.setEnabled(fill);
    }//GEN-LAST:event_fillBoxActionPerformed
    
    public void setProbePanels(JPanel geomPanel, JPanel indexPanel)
    {
        parameters.setActive(false);
        parameters.setParameterActive(false);
        geomProbePanel.removeAll();
        indexProbePanel.removeAll();
        if (indexPanel != null) {
            indexProbePanel.add(indexPanel, BorderLayout.CENTER);
            indexProbeSelector.setEnabled(indexPanel != null);
        }
        if (geomPanel != null) {
            geomProbePanel.add(geomPanel, BorderLayout.CENTER);
            geomProbeSelector.setEnabled(geomPanel != null);
            geomProbeSelector.setSelected(true);
            parameters.set(PROBE_TYPE, ProbeType.GEOMETRIC);
        }
        else {
            indexProbeSelector.setSelected(true);
            parameters.set(PROBE_TYPE, ProbeType.INDEX);
        }
        updateGlyphControlPanel();
        probeTypePanel.setVisible(geomPanel != null && indexPanel != null);
        parameters.setParameterActive(true);
        parameters.setActive(true);
    }
    
    private void bgrSelect()
    {
        if (graphParams != null) {
            graphParams.setAutoBgr(windowBgrButton.isSelected());
            bgrColorEditor.setVisible(userBgrButton.isSelected());
        }
    }
    private void updateGlyphControlPanel()
    {
        boolean geom = parameters.get(PROBE_TYPE) == ProbeType.GEOMETRIC;
        geomProbePanel.setVisible(geom);
        indexProbePanel.setVisible(!geom);
    }

    public ComponentDisplayController getComponentDisplayController()
    {
        return componentDisplayController;
    }

    
    public void enableRemoveGraph(boolean enable)
    {
        sliceRemoveButton.setEnabled(enable);
    }

    public void enableAddGraph(boolean enable)
    {
        sliceAddButton.setEnabled(enable);
    }
    
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.ColorEditor bgrColorEditor;
    private javax.swing.JSlider bgrTransparencySlider;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.visnow.vn.lib.utils.graphing.ComponentDisplayController componentDisplayController;
    private javax.swing.JCheckBox fillBox;
    private javax.swing.JSlider fontSizeSlider;
    private javax.swing.JCheckBox frameBox;
    private javax.swing.JPanel geomProbePanel;
    private javax.swing.JRadioButton geomProbeSelector;
    private javax.swing.JPanel glyphControlPanel;
    private javax.swing.JPanel indexProbePanel;
    private javax.swing.JRadioButton indexProbeSelector;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton pointerLinesButton;
    private javax.swing.JComboBox<String> positionCombo;
    private javax.swing.JPanel probePane;
    private javax.swing.JPanel probeTypePanel;
    private javax.swing.JButton sliceAddButton;
    private javax.swing.JRadioButton sliceNumbersButton;
    private javax.swing.JButton sliceRemoveButton;
    private javax.swing.JButton slicesClearButton;
    private org.visnow.vn.gui.widgets.FloatSlider timeSlider;
    private javax.swing.JRadioButton userBgrButton;
    private javax.swing.JRadioButton windowBgrButton;
    // End of variables declaration//GEN-END:variables

}
