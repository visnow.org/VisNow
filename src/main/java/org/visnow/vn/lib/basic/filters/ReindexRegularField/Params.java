/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ReindexRegularField;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class Params extends Parameters
{

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<int[]>("coordinate", ParameterType.dependent, null),
        new ParameterEgg<boolean[]>("mirror", ParameterType.dependent, null)
    };

    public Params()
    {
        super(eggs);
        setValue("coordinate", new int[]{0, 1, 2});
        setValue("mirror", new boolean[]{false, false, false});
    }

    /**
     * Get the value of mirror
     *
     * @return the value of mirror
     */
    public boolean[] isMirror()
    {
        return (boolean[]) getValue("mirror");
    }

    /**
     * Set the value of mirror
     *
     * @param mirror new value of mirror
     */
    public void setMirror(boolean[] mirror)
    {
        setValue("mirror", mirror);
    }

    /**
     * Get the value of mirror at specified index
     *
     * @param index
     *              <p>
     * @return the value of mirror at specified index
     */
    public boolean isMirror(int index)
    {
        return ((boolean[]) getValue("mirror"))[index];
    }

    /**
     * Set the value of mirror at specified index.
     *
     * @param index
     * @param newMirror new value of mirror at specified index
     */
    public void setMirror(int index, boolean newMirror)
    {
        ((boolean[]) getValue("mirror"))[index] = newMirror;
    }

    /**
     * Get the value of coordinate
     *
     * @return the value of coordinate
     */
    public int[] getCoordinate()
    {
        return (int[]) getValue("coordinate");
    }

    /**
     * Set the value of coordinate
     *
     * @param coordinate new value of coordinate
     */
    public void setCoordinate(int[] coordinate)
    {
        setValue("coordinate", coordinate);
    }

    /**
     * Get the value of coordinate at specified index
     *
     * @param index
     *              <p>
     * @return the value of coordinate at specified index
     */
    public int getCoordinate(int index)
    {
        return ((int[]) getValue("coordinate"))[index];
    }

    /**
     * Set the value of coordinate at specified index.
     *
     * @param index
     * @param newCoordinate new value of coordinate at specified index
     */
    public void setCoordinate(int index, int newCoordinate)
    {
        ((int[]) getValue("coordinate"))[index] = newCoordinate;
    }
}
