/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Boundaries;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Boundaries extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    public Boundaries()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        IrregularField inField = ((VNIrregularField) getInputFirstValue("inField")).getField();
        long nNodes = inField.getNNodes();
        IntLargeArray nodeIndices = new IntLargeArray(nNodes, false);
        for (long i = 0; i < nNodes; i++)
            nodeIndices.setInt(i, -1);
        for (CellSet cellSet : inField.getCellSets()) {
            for (int i = 0; i < cellSet.getBoundaryCellArrays().length; i++) {
                CellArray ca = cellSet.getBoundaryCellArray(CellType.getType(i));
                if (ca == null || ca.getType() == CellType.POINT)
                    continue;
                if (nNodes > Integer.MAX_VALUE) {

                } else {
                    int[] nodes = ca.getNodes();
                    for (int j = 0; j < nodes.length; j++)
                        nodeIndices.setInt(nodes[j], 1);
                }
            }
        }
        int nBoundaryNodes = 0;
        for (long i = 0; i < nNodes; i++)
            if (nodeIndices.getInt(i) == 1) {
                nodeIndices.setInt(i, nBoundaryNodes);
                if (nBoundaryNodes == Integer.MAX_VALUE) {
                    // exit - too many boundary nodes
                }
                nBoundaryNodes += 1;
            }
        outIrregularField = new IrregularField(nBoundaryNodes);
        FloatLargeArray coords = inField.getCurrentCoords();
        float[] bCoords = new float[3 * nBoundaryNodes];
        int k = 0;
        for (long i = 0; i < nNodes; i++)
            if (nodeIndices.getInt(i) >= 0) {
                for (int j = 0; j < 3; j++)
                    bCoords[3 * k + j] = coords.getFloat(3 * i + j);
                k += 1;
            }

        for (DataArray da : inField.getComponents()) {
            int vlen = da.getVectorLength();
            k = 0;            
            if(da.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                LargeArray inData = da.getRawArray();
                LargeArray outData = null;
                switch(da.getType()) {
                    case FIELD_DATA_BYTE:
                        outData = new UnsignedByteLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_SHORT:
                        outData = new ShortLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_INT:
                        outData = new IntLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_FLOAT:
                        outData = new FloatLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_DOUBLE:
                        outData = new DoubleLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_LOGIC:
                        outData = new LogicLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_STRING:
                        outData = new StringLargeArray(nBoundaryNodes * vlen);
                        break;
                }
                if(outData != null) {
                    for (long i = 0; i < nNodes; i++) {
                        if (nodeIndices.getInt(i) >= 0) {
                            for (int j = 0; j < vlen; j++)
                                outData.set(vlen * k + j, inData.get(vlen * i + j));
                            k += 1;
                        }
                    }
                    outIrregularField.addComponent(DataArray.create(outData, vlen, da.getName() + "_b"));                
                }
            } else {
                ComplexFloatLargeArray inCData = (ComplexFloatLargeArray)da.getRawArray();
                ComplexFloatLargeArray outCData = new ComplexFloatLargeArray(nBoundaryNodes * vlen);                    
                for (long i = 0; i < nNodes; i++) {
                    if (nodeIndices.getInt(i) >= 0) {
                        for (int j = 0; j < vlen; j++)
                            outCData.setComplexFloat(vlen * k + j, inCData.getComplexFloat(vlen * i + j));
                        k += 1;
                    }
                }
                outIrregularField.addComponent(DataArray.create(outCData, vlen, da.getName() + "_b"));
            }
        }
            

        outIrregularField.setCurrentCoords(new FloatLargeArray(bCoords));
        for (CellSet cellSet : inField.getCellSets()) {
            CellSet outCellSet = new CellSet(cellSet.getName() + "_boundary");
            for (int i = 0; i < cellSet.getBoundaryCellArrays().length; i++) {
                CellArray ca = cellSet.getBoundaryCellArray(CellType.getType(i));
                if (ca == null || ca.getType() == CellType.POINT)
                    continue;
                int[] nodes = ca.getNodes();
                int[] bNodes = new int[nodes.length];
                for (int j = 0; j < bNodes.length; j++)
                    bNodes[j] = nodeIndices.getInt(nodes[j]);
                CellArray bCa = new CellArray(ca.getType(), bNodes, ca.getOrientations(), null);
                outCellSet.setCellArray(bCa);
                outCellSet.setBoundaryCellArray(bCa);
            }
            outIrregularField.addCellSet(outCellSet);
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();

    }
}
