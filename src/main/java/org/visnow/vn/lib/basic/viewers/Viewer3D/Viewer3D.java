/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.Viewer3D;

import java.awt.Frame;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.viewer3d.Display3DInternalFrame;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.vn.geometries.viewer3d.controls.Display3DControlsPanel;
import org.visnow.vn.gui.widgets.VisNowFrame;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.framework.MainWindow;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Viewer3D extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private static org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(Viewer3D.class);
    private VisNowFrame frame;
    private Display3DInternalFrame internalFrame = null;
    private Display3DPanel displayPanel = null;
    private Display3DControlsPanel controlsPanel = null;
    private GUI ui = null;
    private boolean showFrameOnActive = true;
    private final ChangeListener changeListener = (ChangeEvent e) -> {
        if (internalFrame == null)
            return;
        internalFrame.getDisplayPanel().clearAllGeometry();
        Vector ins = getInputValues("inObject");
        for (Object obj : ins)
            if  (obj != null  && obj instanceof VNGeometryObject) {
                VNGeometryObject geomObj = (VNGeometryObject)obj;
                if (geomObj.getGeometryObject() != null)
                    internalFrame.getDisplayPanel().addChild(geomObj.getGeometryObject());
            }
    };

    /**
     * Creates a new instance of Viewer3D
     */
    public Viewer3D(boolean exportWindows)
    {
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            ui.setViewerModule(Viewer3D.this);
            setPanel(ui);

            frame = new VisNowFrame();
            frame.setTitle("Viewer3D");
            internalFrame = new Display3DInternalFrame();
            frame.add(internalFrame);
            displayPanel = internalFrame.getDisplayPanel();
            controlsPanel = displayPanel.getControlsPanel();
            ui.setControlsPanel(controlsPanel);
            displayPanel.setControlsFrame(null);
            displayPanel.setControlsPanel(controlsPanel);
            internalFrame.setVisible(true);
            MainWindow mainWindow = VisNow.get().getMainWindow();
            frame.setBounds(VisNow.getPreferredViewerWindowBounds(mainWindow.getGraphicsConfiguration().getBounds(),
                                                                  mainWindow.getBounds()));
            frame.setVisible(!exportWindows);
        });
    }

    /**
     * Creates a new instance of Viewer3D
     */
    public Viewer3D()
    {
        this(false);
    }
    @Override
    public void setApplication(Application application)
    {
        super.setApplication(application);
        displayPanel.setApplication(application);
    }

    @Override
    public boolean isViewer()
    {
        return true;
    }

    void showWindow()
    {
        frame.setVisible(true);
        frame.setExtendedState(Frame.NORMAL);
    }

    void setTransientControlsFrame(JFrame transientControlsFrame)
    {
        displayPanel.setTransientControlsFrame(transientControlsFrame);
    }

    @Override
    public void onDelete()
    {
        internalFrame.getDisplayPanel().clearAllGeometry();
        internalFrame.getDisplayPanel().clearCanvas();
        if (internalFrame.getDisplayPanel().getControlsFrame() != null) {
            internalFrame.getDisplayPanel().getControlsFrame().dispose();
        }
        if (internalFrame.getDisplayPanel().getTransientControlsFrame() != null) {
            internalFrame.getDisplayPanel().getTransientControlsFrame().dispose();
        }
        if (ui.getDetachedFrame() != null) {
            ui.getDetachedFrame().dispose();
        }
        internalFrame.removeAll();
        frame.dispose();
        internalFrame = null;
        frame = null;
        displayPanel = null;
        controlsPanel = null;
        ui = null;
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        Object obj = link.getOutput().getValue();
        if(obj != null && obj instanceof VNGeometryObject)
            ((VNGeometryObject)obj).clearListener();        
        onActive();
    }

    @Override
    public void onInputAttach(LinkFace link)
    {
        onActive();
    }

    public void setShowFrameOnActive(boolean showFrameOnActive) {
        this.showFrameOnActive = showFrameOnActive;
    }

    @Override
    public void onActive()
    {
        internalFrame.getDisplayPanel().setPostRenderSilent(true);

        if (!internalFrame.isVisible() && !controlsPanel.isFullScreen() && showFrameOnActive)
            internalFrame.setVisible(true);

        internalFrame.getDisplayPanel().clearAllGeometry();

        Vector ins = getInputValues("inObject");
        for (Object obj : ins)
            if  (obj != null  && obj instanceof VNGeometryObject) {
                VNGeometryObject geomObj = (VNGeometryObject)obj;
                geomObj.setListener(changeListener);
                if (geomObj.getGeometryObject() != null)
                    internalFrame.getDisplayPanel().
                            addChild(geomObj.getGeometryObject());
            }

        internalFrame.getDisplayPanel().setPostRenderSilent(false);
        //if(internalFrame.getDisplayPanel().isStoringFrames())
        if (internalFrame.getDisplayPanel().isWaitingForExternalTrigger())
            internalFrame.getDisplayPanel().forceRender();
        //window.repaint();
        internalFrame.getDisplayPanel().kick();

    }

    public Display3DInternalFrame getInternalFrame()
    {
        return internalFrame;
    }

    public VisNowFrame getFrame()
    {
        return frame;
    }

    public Display3DPanel getDisplayPanel()
    {
        return displayPanel;
    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (displayPanel != null)
            displayPanel.setName(getName());

        if (frame != null)
            frame.setTitle("VisNow Viewer3D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

}
