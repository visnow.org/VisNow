/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Trajectories;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * <p>
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class Params extends Parameters
{

    protected static final String START_FRAME = "startframe";
    protected static final String END_FRAME = "endframe";
    protected static final String PREFERRED_SIZE = "preferredSize";

    public Params()
    {
        super(eggs);
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(START_FRAME, ParameterType.dependent, 1),
        new ParameterEgg<Integer>(END_FRAME, ParameterType.dependent, 100),
        new ParameterEgg<Integer>(PREFERRED_SIZE, ParameterType.dependent, 1000),};

    public int getStartFrame()
    {
        return (Integer) getValue(START_FRAME);
    }

    public void setStartFrame(int value)
    {
        this.setValue(START_FRAME, value);
        fireStateChanged();
    }

    public int getEndFrame()
    {
        return (Integer) getValue(END_FRAME);
    }

    public void setEndFrame(int value)
    {
        this.setValue(END_FRAME, value);
        fireStateChanged();
    }

    public void setStartEndFrames(int startValue, int endValue)
    {
        this.setValue(START_FRAME, startValue);
        this.setValue(END_FRAME, endValue);
        fireStateChanged();
    }
    
    public int getPreferredSize()
    {
        return (Integer) getValue(PREFERRED_SIZE);
    }

    public void setPreferredSize(int value)
    {
        this.setValue(PREFERRED_SIZE, value);
        fireStateChanged();
    }

}
