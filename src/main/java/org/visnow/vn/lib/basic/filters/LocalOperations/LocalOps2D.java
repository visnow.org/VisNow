/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.LocalOperations;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class LocalOps2D implements LocalOps
{

    ProgressAgent progressAgent;

    private int radius = 1;
    private int[] dims = null;
    private boolean[][] inside = null;

    public RegularField compute(RegularField inField, int radius, String sequence)
    {
        return compute(inField, radius, sequence, ProgressAgent.getDummyAgent());
    }

    public RegularField compute(RegularField inField, int radius, String sequence, ProgressAgent progressAgent)
    {
        this.progressAgent = progressAgent;
//        if (inField == null)
//            return null;
        this.radius = radius;
        dims = inField.getDims();
        for (int i = 0; i < dims.length; i++)
            if (dims[i] < 2 * radius + 2)
                return inField;
        inside = new boolean[2 * radius + 1][2 * radius + 1];
        for (int i = -radius; i <= radius; i++)
            for (int j = -radius; j <= radius; j++)
                inside[i + radius][j + radius] = Math.round((double) i * i + j * j) <= radius * radius;
        int nData, n;
        nData = dims[0] * dims[1];

        byte[] outBData = null;
        short[] outSData = null;
        int[] outIData = null;
        float[] outFData = null;
        double[] outDData = null;
        byte[] tmpBData = null;
        short[] tmpSData = null;
        int[] tmpIData = null;
        float[] tmpFData = null;
        double[] tmpDData = null;
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];

        RegularField outField = new RegularField(dims);
        outField.setAffine(inField.getAffine());
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (n = 0; n < inField.getNComponents(); n++) {
            DataArray dataArr = inField.getComponent(n);
            if (dataArr.getVectorLength() != 1)
                continue;
            switch (dataArr.getType()) {
                case FIELD_DATA_BYTE:
                    outBData = dataArr.getRawByteArray().getData();
                    tmpBData = new byte[nData];
                    break;
                case FIELD_DATA_SHORT:
                    outSData = dataArr.getRawShortArray().getData();
                    tmpSData = new short[nData];
                    break;
                case FIELD_DATA_INT:
                    outIData = dataArr.getRawIntArray().getData();
                    tmpIData = new int[nData];
                    break;
                case FIELD_DATA_FLOAT:
                    outFData = dataArr.getRawFloatArray().getData();
                    tmpFData = new float[nData];
                    break;
                case FIELD_DATA_DOUBLE:
                    outDData = dataArr.getRawDoubleArray().getData();
                    tmpDData = new double[nData];
                    break;
            }
            for (int s = 0; s < sequence.length(); s++)
                switch (dataArr.getType()) {
                    case FIELD_DATA_BYTE:
                        System.arraycopy(outBData, 0, tmpBData, 0, nData);
                        for (int i = 0; i < workThreads.length; i++) {
                            workThreads[i] = new Thread(new FilterByteArray(nThreads, i, outBData, tmpBData,
                                                                            sequence.charAt(s) == 'd' || sequence.charAt(s) == 'D'));
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                        break;
                    case FIELD_DATA_SHORT:
                        System.arraycopy(outSData, 0, tmpSData, 0, nData);
                        for (int i = 0; i < workThreads.length; i++) {
                            workThreads[i] = new Thread(new FilterShortArray(nThreads, i, outSData, tmpSData,
                                                                             sequence.charAt(s) == 'd' || sequence.charAt(s) == 'D'));
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                        break;
                    case FIELD_DATA_INT:
                        System.arraycopy(outIData, 0, tmpIData, 0, nData);
                        for (int i = 0; i < workThreads.length; i++) {
                            workThreads[i] = new Thread(new FilterIntArray(nThreads, i, outIData, tmpIData,
                                                                           sequence.charAt(s) == 'd' || sequence.charAt(s) == 'D'));
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                        break;
                    case FIELD_DATA_FLOAT:
                        System.arraycopy(outFData, 0, tmpFData, 0, nData);
                        for (int i = 0; i < workThreads.length; i++) {
                            workThreads[i] = new Thread(new FilterFloatArray(nThreads, i, outFData, tmpFData,
                                                                             sequence.charAt(s) == 'd' || sequence.charAt(s) == 'D'));
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                        break;
                    case FIELD_DATA_DOUBLE:
                        System.arraycopy(outDData, 0, tmpDData, 0, nData);
                        for (int i = 0; i < workThreads.length; i++) {
                            workThreads[i] = new Thread(new FilterDoubleArray(nThreads, i, outDData, tmpDData,
                                                                              sequence.charAt(s) == 'd' || sequence.charAt(s) == 'D'));
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                        break;
                }
            switch (dataArr.getType()) {
                case FIELD_DATA_BYTE:
                    outField.addComponent(DataArray.create(outBData, 1, dataArr.getName() + sequence, dataArr.getUnit(), dataArr.getUserData()));
                    break;
                case FIELD_DATA_SHORT:
                    outField.addComponent(DataArray.create(outSData, 1, dataArr.getName() + sequence, dataArr.getUnit(), dataArr.getUserData()));
                    break;
                case FIELD_DATA_INT:
                    outField.addComponent(DataArray.create(outIData, 1, dataArr.getName() + sequence, dataArr.getUnit(), dataArr.getUserData()));
                    break;
                case FIELD_DATA_FLOAT:
                    outField.addComponent(DataArray.create(outFData, 1, dataArr.getName() + sequence, dataArr.getUnit(), dataArr.getUserData()));
                    break;
                case FIELD_DATA_DOUBLE:
                    outField.addComponent(DataArray.create(outDData, 1, dataArr.getName() + sequence, dataArr.getUnit(), dataArr.getUserData()));
                    break;
            }
        }
//        if (outField.getNComponents() > 0)
        return outField;
//        else
//            return null;
    }

    class FilterByteArray implements Runnable
    {

        int from;
        int to;
        boolean dilate;
        byte[] outData;
        byte[] tmpData;

        public FilterByteArray(int nThreads, int iThread, byte[] outData, byte[] tmpData, boolean dilate)
        {
            from = (dims[1] * iThread) / nThreads;
            to = (dims[1] * (iThread + 1)) / nThreads;
            this.tmpData = tmpData;
            this.outData = outData;
            this.dilate = dilate;
        }

        public void run()
        {
            for (int j = from, m = from * dims[0]; j < to; j++) {
                progressAgent.increase();

                int j0 = j - radius;
                if (j0 < 0)
                    j0 = 0;
                int j1 = j + radius + 1;
                if (j1 > dims[1])
                    j1 = dims[1];
                for (int i = 0; i < dims[0]; i++, m++) {
                    int p = 0xff & tmpData[m];
                    int i0 = i - radius;
                    if (i0 < 0)
                        i0 = 0;
                    int i1 = i + radius + 1;
                    if (i1 > dims[0])
                        i1 = dims[0];
                    for (int jj = j0, js = j0 - j + radius; jj < j1; jj++, js++)
                        for (int ii = i0, is = i0 - i + radius, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++)
                            if (inside[is][js]) {
                                int v = 0xff & tmpData[l];
                                if (v > p && dilate)
                                    p = v;
                                if (v < p && !dilate)
                                    p = v;
                            }
                    outData[m] = (byte) (0xff & p);
                }
            }
        }
    }

    class FilterShortArray implements Runnable
    {

        int from;
        int to;
        boolean dilate;
        short[] outData;
        short[] tmpData;

        public FilterShortArray(int nThreads, int iThread, short[] outData, short[] tmpData, boolean dilate)
        {
            from = (dims[1] * iThread) / nThreads;
            to = (dims[1] * (iThread + 1)) / nThreads;
            this.outData = outData;
            this.tmpData = tmpData;
            this.dilate = dilate;
        }

        public void run()
        {
            for (int j = from, m = from * dims[0]; j < to; j++) {
                progressAgent.increase();
                int j0 = j - radius;
                if (j0 < 0)
                    j0 = 0;
                int j1 = j + radius + 1;
                if (j1 > dims[1])
                    j1 = dims[1];
                for (int i = 0; i < dims[0]; i++, m++) {
                    short p = tmpData[m];
                    int i0 = i - radius;
                    if (i0 < 0)
                        i0 = 0;
                    int i1 = i + radius + 1;
                    if (i1 > dims[0])
                        i1 = dims[0];
                    for (int jj = j0, js = j0 - j + radius; jj < j1; jj++, js++)
                        for (int ii = i0, is = i0 - i + radius, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++)
                            if (inside[is][js]) {
                                short v = tmpData[l];
                                if (v > p && dilate)
                                    p = v;
                                if (v < p && !dilate)
                                    p = v;
                            }
                    outData[m] = p;
                }
            }
        }
    }

    class FilterIntArray implements Runnable
    {

        int from;
        int to;
        boolean dilate;
        int[] outData;
        int[] tmpData;

        public FilterIntArray(int nThreads, int iThread, int[] outData, int[] tmpData, boolean dilate)
        {
            from = (dims[1] * iThread) / nThreads;
            to = (dims[1] * (iThread + 1)) / nThreads;
            this.outData = outData;
            this.tmpData = tmpData;
            this.dilate = dilate;
        }

        public void run()
        {
            for (int j = from, m = from * dims[0]; j < to; j++) {
                progressAgent.increase();
                int j0 = j - radius;
                if (j0 < 0)
                    j0 = 0;
                int j1 = j + radius + 1;
                if (j1 > dims[1])
                    j1 = dims[1];
                for (int i = 0; i < dims[0]; i++, m++) {
                    int p = tmpData[m];
                    int i0 = i - radius;
                    if (i0 < 0)
                        i0 = 0;
                    int i1 = i + radius + 1;
                    if (i1 > dims[0])
                        i1 = dims[0];
                    for (int jj = j0, js = j0 - j + radius; jj < j1; jj++, js++)
                        for (int ii = i0, is = i0 - i + radius, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++)
                            if (inside[is][js]) {
                                int v = tmpData[l];
                                if (v > p && dilate)
                                    p = v;
                                if (v < p && !dilate)
                                    p = v;
                            }
                    outData[m] = p;
                }
            }
        }
    }

    class FilterFloatArray implements Runnable
    {

        int from;
        int to;
        boolean dilate;
        float[] outData;
        float[] tmpData;

        public FilterFloatArray(int nThreads, int iThread, float[] outData, float[] tmpData, boolean dilate)
        {
            from = (dims[1] * iThread) / nThreads;
            to = (dims[1] * (iThread + 1)) / nThreads;
            this.outData = outData;
            this.tmpData = tmpData;
            this.dilate = dilate;
        }

        public void run()
        {
            for (int j = from, m = from * dims[0]; j < to; j++) {
                progressAgent.increase();
                int j0 = j - radius;
                if (j0 < 0)
                    j0 = 0;
                int j1 = j + radius + 1;
                if (j1 > dims[1])
                    j1 = dims[1];
                for (int i = 0; i < dims[0]; i++, m++) {
                    float p = tmpData[m];
                    int i0 = i - radius;
                    if (i0 < 0)
                        i0 = 0;
                    int i1 = i + radius + 1;
                    if (i1 > dims[0])
                        i1 = dims[0];
                    for (int jj = j0, js = j0 - j + radius; jj < j1; jj++, js++)
                        for (int ii = i0, is = i0 - i + radius, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++)
                            if (inside[is][js]) {
                                float v = tmpData[l];
                                if (v > p && dilate)
                                    p = v;
                                if (v < p && !dilate)
                                    p = v;
                            }
                    outData[m] = p;
                }
            }
        }
    }

    class FilterDoubleArray implements Runnable
    {

        int from;
        int to;
        boolean dilate;
        double[] outData;
        double[] tmpData;

        public FilterDoubleArray(int nThreads, int iThread, double[] outData, double[] tmpData, boolean dilate)
        {
            from = (dims[1] * iThread) / nThreads;
            to = (dims[1] * (iThread + 1)) / nThreads;
            this.outData = outData;
            this.tmpData = tmpData;
            this.dilate = dilate;
        }

        public void run()
        {
            for (int j = from, m = from * dims[0]; j < to; j++) {
                progressAgent.increase();
                int j0 = j - radius;
                if (j0 < 0)
                    j0 = 0;
                int j1 = j + radius + 1;
                if (j1 > dims[1])
                    j1 = dims[1];
                for (int i = 0; i < dims[0]; i++, m++) {
                    double p = tmpData[m];
                    int i0 = i - radius;
                    if (i0 < 0)
                        i0 = 0;
                    int i1 = i + radius + 1;
                    if (i1 > dims[0])
                        i1 = dims[0];
                    for (int jj = j0, js = j0 - j + radius; jj < j1; jj++, js++)
                        for (int ii = i0, is = i0 - i + radius, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++)
                            if (inside[is][js]) {
                                double v = tmpData[l];
                                if (v > p && dilate)
                                    p = v;
                                if (v < p && !dilate)
                                    p = v;
                            }
                    outData[m] = p;
                }
            }
        }
    }
}
