/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */
package org.visnow.vn.lib.basic.writers.NIfTIWriter;

import org.visnow.jscic.RegularField;
import java.io.IOException;
import niftijio.NiftiHeader;
import niftijio.NiftiVolume;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.UnitUtils;

/**
 * Static methods for writing NIfTI files.
 *
 * @author Piotr Wendykier (piotr.wendykier@gmail.com)
 */
public class NIfTIWriterCore
{

    /**
     * Writes VisNow RegularField field to an NIfTI file.
     *
     * @param field VisNow field
     * @param path  output file path
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeField(RegularField field, String path)
    {
        DataArray dataArray = field.getComponent(0);
        DataArrayType type = dataArray.getType();
        int nx;
        int ny = 1;
        int nz = 1;

        LargeArrayType ltype = type.toLargeArrayType();
        if (!(ltype.isNumericType() && !ltype.isComplexNumericType())) {
            return false;
        }

        int[] dims = field.getDims();
        if (dims.length > 0) {
            nx = dims[0];
        } else {
            return false;
        }
        if (dims.length > 1) {
            ny = dims[1];
        }
        if (dims.length > 2) {
            nz = dims[2];
        }

        NiftiHeader header = new NiftiHeader(nx, ny, nz, 1);
        String name = dataArray.getName();
        if (!name.isEmpty()) {
            header.descrip = new StringBuffer(name);
        }

        String unit = dataArray.getUnit();
        if (!unit.isEmpty() && !unit.equals("1")) {
            switch (UnitUtils.getUnit(unit).getCanonicalString()) {
                case "m":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_METER;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_METER;
                    break;
                case "0.001 m":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_MM;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_MM;
                    break;
                case "1.0E-6 m":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_MICRON;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_MICRON;
                    break;
                case "s":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_SEC;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_SEC;
                    break;
                case "0.001 s":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_MSEC;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_MSEC;
                    break;
                case "1.0E-6 s":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_USEC;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_USEC;
                    break;
                case "s-1":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_HZ;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_HZ;
                    break;
                case "1.0E-6":
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_PPM;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_PPM;
                    break;
                default:
                    header.xyz_unit_code = NiftiHeader.NIFTI_UNITS_UNKNOWN;
                    header.t_unit_code = NiftiHeader.NIFTI_UNITS_UNKNOWN;
                    break;
            }
        }

        switch (type) {
            case FIELD_DATA_LOGIC:
                header.setDatatype(NiftiHeader.DT_BINARY);
                break;
            case FIELD_DATA_BYTE:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_UINT8);
                break;
            case FIELD_DATA_SHORT:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_INT16);
                break;
            case FIELD_DATA_INT:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_INT32);
                break;
            case FIELD_DATA_LONG:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_INT64);
                break;
            case FIELD_DATA_FLOAT:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_FLOAT32);
                break;
            case FIELD_DATA_DOUBLE:
                header.setDatatype(NiftiHeader.NIFTI_TYPE_FLOAT64);
                break;
            default:
                return false;
        }

        float[][] affine = field.getAffine();
        if (affine != null) {
            header.qform_code = NiftiHeader.NIFTI_XFORM_SCANNER_ANAT;
            header.sform_code = NiftiHeader.NIFTI_XFORM_UNKNOWN;
            header.pixdim[1] = affine[0][0];
            header.pixdim[2] = affine[1][1];
            header.pixdim[3] = affine[2][2];
            System.arraycopy(affine[3], 0, header.qoffset, 0, 3);
        }

        NiftiVolume volume = new NiftiVolume(header);
        DoubleLargeArray data = dataArray.getRawDoubleArray();
        long stride1 = (long) ny * (long) nx;
        long stride2 = (long) nx;

        for (int k = 0; k < nz; k++) {
            for (int j = 0; j < ny; j++) {
                for (int i = 0; i < nx; i++) {
                    long idx = k * stride1 + j * stride2 + i;
                    volume.data.set(i, j, k, 0, data.getDouble(idx));
                }
            }
        }

        try {
            volume.write(path);
        } catch (IOException ex) {
            return false;
        }

        return true;
    }

}
