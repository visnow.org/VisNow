/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.GroupTransform;

import java.util.Vector;
import org.jogamp.java3d.Transform3D;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.parameters.TransformParams;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GroupTransform extends VisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected TransformParams params;

    public GroupTransform()
    {
        params = new TransformParams();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj.getGeometryObj().setUserData(new ModuleIdData(this));
        outObj2DStruct.setParentModulePort(this.getName() + ".out.outObj");
        setOutputValue("outObj", new VNGeometryObject(outObj, outObj2DStruct));
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                outObj.setTransform(new Transform3D(params.getTransform()));
            }
        });
    }

    @Override
    public void onActive()
    {
        Vector ins = getInputValues("inObject");
        for (Object obj : ins)
            if ((VNGeometryObject) obj != null &&
                ((VNGeometryObject) obj).getGeometryObject() != null)
                outObj.addChild(
                    ((VNGeometryObject) obj).getGeometryObject());
    }
}
