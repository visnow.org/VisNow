/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.readRaw;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

public class ReadRawShared
{
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //not-null, may be empty
    static final ParameterName<String[]> FILE_NAMES = new ParameterName("File names");
    //1, 2 or 3
    static final ParameterName<Integer> NUMBER_OF_DIMENSIONS = new ParameterName("Number of dimensions");
    //length == 3, each element >= 2
    static final ParameterName<long[]> DATA_DIMS = new ParameterName("Data dims");
    //length == 3, each element >= 2
    static final ParameterName<long[]> FIELD_DIMS = new ParameterName("Field dims");
    //length == 3
    static final ParameterName<long[]> FIELD_OFFSET = new ParameterName("Field offset");
    //DESCRIPTION:
    //Alignment within single file. 
    //if n >= 0 then skip n bytes head and read data aligned to start of file
    //if n < 0 then skip -n-1 bytes tail and read data aligned to end of file
    static final ParameterName<Long> IN_FILE_ALIGNMENT = new ParameterName("In-file alignment");
    static final ParameterName<NumberType> NUMBER_TYPE = new ParameterName("Number type");
    static final ParameterName<Endianness> ENDIANNESS = new ParameterName("Endianness");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    
    
    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)
    //"file_size_string" (>=0) or "file_size_string+" (>=0) 
    static final ParameterName<String> META_FILE_SIZE_INFO = new ParameterName("META File size or shortest file size");
}
