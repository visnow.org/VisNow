/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.TestField;

import org.visnow.vn.engine.core.ParameterName;

/**
 * Shared static methods and fields between GUI and logic.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TestFieldShared
{
    public enum GeometryType {TRIVIAL_GEOM, UNIFORM_RECT_GEOM, RECTANGULAR_GEOM, AFFINE_GEOM, CURVILINEAR_GEOM, RADIAL_GEOM, SPHERICAL_GEOM};
    public enum StructureType {REGULAR, IRREGULAR, TRIANGULATED};

    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //integer: 1,2 or 3
    public static final ParameterName<Integer> NUMBER_OF_DIMENSIONS = new ParameterName("Number of dimensions");
    //integer >= 2
    public static final ParameterName<Integer> DIMENSION_LENGTH = new ParameterName("Length of single dimension");
    //not null list of unique integers within range 0 .. <number_of_components>-1
    //where <number_of_components> equals FIELD_NAMES_3D.length, FIELD_NAMES_2D.length or FIELD_NAMES_1D.length depends on NUMBER_OF_DIMENSIONS
    public static final ParameterName<int[]> SELECTED_COMPONENTS = new ParameterName("Selected components");
    //type of geometry generated (valid if dim num > 1
    public static final ParameterName<GeometryType> GEOMETRY = new ParameterName("geometry");
    //type of geometry generated (valid if dim num > 1
    public static final ParameterName<Boolean> CREATE_COORDS = new ParameterName("create coords");
    //type of geometry generated (valid if dim num > 1
    public static final ParameterName<StructureType> STRUCT_TYPE = new ParameterName("structure type");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)
    public static final ParameterName<Long> META_RANDOM_SEED = new ParameterName("META Random seed");

    //outfield component names 3D
    public final static String GAUSSIANS = "Gaussians";
    public final static String TRIG_FUNCTION = "Trig function";
    public final static String SEMIELIPSOID = "Semielipsoid";
    public final static String TIME_FIELD = "Time field";
    public final static String PIPE = "Pipe";
    public final static String HOPF = "Hopf";
    public final static String VORTEX = "Vortex";
    public final static String TIME_VECTOR_FIELD = "Time vector field";
    public final static String CONST = "Constant field";

    //outfield component names 2D
    public final static String GAUSSIANS_SWIRLS = "Gaussians swirls";
    public final static String GAUSSIANS_GRADIENT = "Gaussians gradient";
    public final static String BYTE_GAUSSIANS = "Byte gaussians";
    public final static String SHORT_GAUSSIANS = "Short gaussians";
    public final static String INT_GAUSSIANS = "Int gaussians";
    public final static String LONG_GAUSSIANS = "Long gaussians";
    public final static String COMPLEX_GAUSSIANS = "Complex gaussians";
    public final static String STRING_GAUSSIANS = "String gaussians";
    public final static String LOGIC_GAUSSIANS = "Logic gaussians";

    //outfield component names 1D
    final static String VECTOR_TRIG = "Vector trig";

    //geometry types
    public final static String TRIVIAL = "Trivial";
    public final static String UNIFORM_RECT = "uniform rectangular";
    public final static String RECTANGULAR = "rectangular";
    public final static String AFFINE = "skew affine";
    public final static String CURVILINEAR = "curvilinear";
    public final static String RADIAL = "radial";
    public final static String SPHERICAL = "spherical";

    public final static String REGULAR = "regular";
    public final static String IRREGULAR = "irregular (box)";
    public final static String TRIANGULATED = "triangulated";

    public final static String COORDS = "generate coordinates";
    public final static String TYPE = "output field type";

    public static final String[] FIELD_NAMES_3D = {
        TRIG_FUNCTION,
        VORTEX,
        GAUSSIANS,
        SEMIELIPSOID,
        HOPF,
        CONST,
        TIME_VECTOR_FIELD,
        TIME_FIELD,
        PIPE
    };

    public static final String[] FIELD_NAMES_2D = {
        GAUSSIANS,
        GAUSSIANS_SWIRLS,
        GAUSSIANS_GRADIENT,
        BYTE_GAUSSIANS,
        SHORT_GAUSSIANS,
        INT_GAUSSIANS,
        LONG_GAUSSIANS,
        COMPLEX_GAUSSIANS,
        STRING_GAUSSIANS,
        LOGIC_GAUSSIANS,
        TIME_VECTOR_FIELD,
        TIME_FIELD
    };

    public static final String[] FIELD_NAMES_1D = {
        GAUSSIANS,
        TRIG_FUNCTION,
        VECTOR_TRIG,
        TIME_FIELD
    };
}
