/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.ToolsActivityWizard;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointDescriptor;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.GeometryTool;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ToolEntry extends Entry
{

    private ArrayList<PointDescriptor> pointDescriptors = new ArrayList<PointDescriptor>();
    private GeometryTool tool = null;

    public ToolEntry(String id, String name, String description)
    {
        super(Entry.ENTRY_TYPE_TOOL, id, name, description);
    }

    @Override
    public String toString()
    {
        return getName();
    }

    @Override
    public boolean isReady()
    {
        return (tool != null && pointDescriptors.size() >= tool.getMinimumNPoints());
    }

    /**
     * @return the pointDescriptors
     */
    public ArrayList<PointDescriptor> getPointDescriptors()
    {
        return pointDescriptors;
    }

    /**
     * @param pointDescriptor the pointDescriptor to set
     */
    public void setPointDescriptors(ArrayList<PointDescriptor> points)
    {
        //pointDescriptor.setName(this.id);
        this.pointDescriptors.clear();
        for (int i = 0; i < points.size(); i++) {
            pointDescriptors.add(points.get(i));
        }
        fireStateChanged();
    }

    public GeometryTool getTool()
    {
        return tool;
    }

    public void setTool(GeometryTool tool)
    {
        this.tool = tool;
        fireStateChanged();
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
    }

}
