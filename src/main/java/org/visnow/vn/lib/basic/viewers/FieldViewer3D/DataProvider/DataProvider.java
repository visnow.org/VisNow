/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider;

import java.awt.Color;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import org.visnow.vn.datamaps.ColorMap;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.RegularFieldInterpolator;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.LogicDataArray;
import org.visnow.jscic.dataarrays.StringDataArray;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels.OrthosliceNumberChangedEvent;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.ByteDataArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ShortLargeArray;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class DataProvider implements DataProviderParamsListener
{

    private RegularField inField = null;
    private RegularField auxField = null;
    private RegularField overlayField = null;
    private DataProviderParams params = new DataProviderParams();
    private DataProviderUI ui = null;
    private BufferedImage[] orthosliceImages = new BufferedImage[3];
    private BufferedImage[] orthosliceOverlayImages = new BufferedImage[3];
    private BufferedImage customSliceImage = null;
    private BufferedImage[] customOrthoSlicesImages = new BufferedImage[3];
    private BufferedImage[] customOrthoSlicesOverlays = new BufferedImage[3];
    private BufferedImage singleImage = null;
    private GeometryObject customSlice3DPlane = new GeometryObject("CustomSlice");
    private GeometryObject customOrthoSlices3DPlanes = new GeometryObject("CustomOrthoSlices");
    private SliceImage overlaySliceImage = null;
    private GeometryParams geometryParams = null;
    private RegularFieldIsoline[] mapIsoline = new RegularFieldIsoline[3];
    private RegularFieldIsoline[][] mapIsolines = new RegularFieldIsoline[3][];
    private Plane3D customPlane = null;
    private Plane3D[] customOrthoPlanes = new Plane3D[3];
    private ColorMap colorMap = null;
    private ColorMap1D overlayColorMap = ColorMapManager.getInstance().getColorMap1D(ColorMapManager.COLORMAP1D_RAINBOW);
    private int[] colorMapLUT = null;
    private int[] overlayColorMapLUT = overlayColorMap.getRGBColorTable();

    public DataProvider()
    {
        this(null);
    }

    public DataProvider(final RegularField field)
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new DataProviderUI();
                DataProvider.this.inField = field;
                for (int i = 0; i < 3; i++) {
                    orthosliceImages[i] = null;
                    orthosliceOverlayImages[i] = null;
                    customOrthoSlicesImages[i] = null;
                    customOrthoSlicesOverlays[i] = null;
                }
                singleImage = null;

                customPlane = new Plane3D(params.getCustomPlanePoint(), params.getCustomPlaneVector(), inField);
                for (int i = 0; i < 3; i++) {
                    customOrthoPlanes[i] = new Plane3D(params.getCustomOrthoPlanesPoint(), params.getCustomOrthoPlanesVector(i), inField);
                }

                params.addDataProviderParamsListener(DataProvider.this);
                if (field != null)
                    params.getDataMappingParams().setInData(field, (DataContainerSchema) null);
                DataProvider.this.colorMap = params.getDataMappingParams().getColorMap0();
                DataProvider.this.colorMapLUT = DataProvider.this.colorMap.getRGBColorTable();
                ui.setDataProvider(DataProvider.this);

                customSlice3DPlane.addNode(customPlane.getBoundedPlane3D());
                for (int i = 0; i < 3; i++) {
                    customOrthoSlices3DPlanes.addNode(customOrthoPlanes[i].getBoundedPlane3D());
                }
            }
        });
    }

    public DataProviderUI getUI()
    {
        return ui;
    }

    public BufferedImage getOrthoSlice(int axis)
    {
        if (inField == null || axis < 0 || axis > 2) {
            return null;
        }

        if (orthosliceImages[axis] == null) {
            updateOrthosliceImage(axis, true);
        }

        return orthosliceImages[axis];
    }

    public BufferedImage getOrthoSliceOverlay(int axis)
    {
        if (axis < 0 || axis > 2 || (!params.isSimpleOverlay() && overlayField == null)) {
            return null;
        }

        if (orthosliceOverlayImages[axis] == null) {
            updateOrthosliceOverlay(axis, true);
        }

        return orthosliceOverlayImages[axis];
    }

    public void resetIsolines()
    {
        for (int i = 0; i < 3; i++) {
            mapIsoline[i] = null;
        }
        fireDataProviderOrthosliceUpdated(-1);
    }

    public void resetOverlays()
    {
        for (int i = 0; i < 3; i++) {
            orthosliceOverlayImages[i] = null;
            customOrthoSlicesOverlays[i] = null;
        }
        fireDataProviderOverlayUpdated(-1);
    }

    public void updateAll()
    {
        updateOrthosliceImages();
        updateOrthosliceIsolines();
        updateOrthosliceOverlays();
        updateCustomPlane();
        updateCustomOrthoPlanes();
        updateCustomOrthoPlanesOverlays();
        updateSingleImage();
    }

    public void updateOrthosliceImages()
    {
        if (inField == null) {
            return;
        }

        for (int i = 0; i < 3; i++) {
            updateOrthosliceImage(i, true);
        }
        fireDataProviderOrthosliceUpdated(-1);
    }

    public void updateOrthosliceIsolines()
    {
        for (int i = 0; i < 3; i++) {
            updateOrthosliceIsoline(i, true);
            updateOrthosliceIsolines(i, true);
        }
        fireDataProviderOrthosliceUpdated(-1);
    }

    public void updateOrthosliceOverlays()
    {
        for (int i = 0; i < 3; i++) {
            updateOrthosliceOverlay(i, true);
        }
        fireDataProviderOverlayUpdated(-1);
    }

    public void updateOrthosliceIsoline(int axis, boolean silent)
    {
        if (auxField != null && auxField.getDims().length == 3) {
            int[] fDims = inField.getDims();
            int[] dims = auxField.getDims();
            for (int i = 0; i < 3; i++) {
                if (dims[i] != fDims[i]) {
                    mapIsoline[axis] = null;
                    return;
                }
            }

            FloatLargeArray fData = auxField.getCurrent2DFloatSlice(0, axis, params.getOrthosliceNumber(axis));
            if (fData == null)
                return;
            int[] dd = new int[2];
            switch (axis) {
                case 0:
                    dd[0] = dims[1];
                    dd[1] = dims[2];
                    break;
                case 1:
                    dd[0] = dims[0];
                    dd[1] = dims[2];
                    break;
                case 2:
                    dd[0] = dims[0];
                    dd[1] = dims[1];
                    break;
            }
            mapIsoline[axis] = new RegularFieldIsoline(params.getIsolineThreshold(), dd, fData);
            if (!silent)
                fireDataProviderOrthosliceUpdated(axis);
        }
    }

    public void updateOrthosliceIsolines(int axis, boolean silent)
    {
        if (inField != null && inField.getDims().length == 3) {
            int[] dims = inField.getDims();

            FloatLargeArray fData = inField.getCurrent2DFloatSlice(0, axis, params.getOrthosliceNumber(axis));
            int[] dd = new int[2];
            switch (axis) {
                case 0:
                    dd[0] = dims[1];
                    dd[1] = dims[2];
                    break;
                case 1:
                    dd[0] = dims[0];
                    dd[1] = dims[2];
                    break;
                case 2:
                    dd[0] = dims[0];
                    dd[1] = dims[1];
                    break;
            }
            if (params.getIsolineThresholds() != null && params.getIsolineThresholds().length == 2) {
                mapIsolines[axis] = new RegularFieldIsoline[2];
                for (int i = 0; i < params.getIsolineThresholds().length; i++)
                    mapIsolines[axis][i] = new RegularFieldIsoline(params.getIsolineThresholds()[i], dd, fData);
            }
            if (!silent)
                fireDataProviderOrthosliceUpdated(axis);
        }
    }

    public ArrayList<float[][]> getIsoline(int axis)
    {
        if (mapIsoline != null && mapIsoline[axis] != null)
            return mapIsoline[axis].getLines();
        return null;
    }

    public ArrayList<float[][]>[] getIsolines(int axis)
    {
        if (mapIsolines[axis] == null || mapIsolines[axis].length != 2)
            return null;
        ArrayList<float[][]>[] isolines = new ArrayList[2];
        for (int i = 0; i < isolines.length; i++)
            isolines[i] = mapIsolines[axis][i].getLines();
        return isolines;
    }

    public void updateOrthosliceOverlay(int axis, boolean silent)
    {
        orthosliceOverlayImages[axis] = null;

        if (params.isSimpleOverlay() && inField != null && inField.getComponent(params.getSimpleOverlayComponent()) != null) {
            int component = params.getSimpleOverlayComponent();
            int[] dims = inField.getDims();
            int w = 0, h = 0, slice;
            slice = params.getOrthosliceNumber(axis);
            if (slice < 0) {
                slice = 0;
            }
            if (slice >= dims[axis]) {
                slice = dims[axis] - 1;
            }

            float[] emptyPixel = new float[4];
            float[] overlaidPixel = new float[4];
            Color c = params.getSimpleOverlayColor();

            if (params.isSimpleOverlayMask()) {
                if (params.isSimpleOverlayInvert()) {
                    emptyPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 0.0f};
                    overlaidPixel = new float[]{0.0f, 0.0f, 0.0f, 255.0f};
                } else {
                    emptyPixel = new float[]{0.0f, 0.0f, 0.0f, 255.0f};
                    overlaidPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 0.0f};
                }
            } else if (params.isSimpleOverlayInvert()) {
                emptyPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 255.0f};
                overlaidPixel = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
            } else {
                emptyPixel = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
                overlaidPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 255.0f};
            }

            float low = params.getSimpleOverlayLow();
            float up = params.getSimpleOverlayUp();

            switch (inField.getComponent(component).getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = (byte[]) inField.getComponent(component).getRawArray().getData();
                    int b;
                    switch (axis) {
                        case 0:
                            w = dims[1];
                            h = dims[2];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    b = (int) (bData[j * dims[0] * dims[1] + i * dims[0] + slice] & 0xFF);
                                    if (b < low || b > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                        case 1:
                            w = dims[0];
                            h = dims[2];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    b = (int) (bData[j * dims[0] * dims[1] + slice * dims[0] + i] & 0xFF);
                                    if (b < low || b > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                        case 2:
                            w = dims[0];
                            h = dims[1];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    b = (int) (bData[slice * dims[0] * dims[1] + j * dims[0] + i] & 0xFF);
                                    if (b < low || b > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                    }
                    break;
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                    LargeArray dData = inField.getComponent(component).getRawArray();
                    double d;
                    switch (axis) {
                        case 0:
                            w = dims[1];
                            h = dims[2];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    d = dData.getDouble(j * dims[0] * dims[1] + i * dims[0] + slice);
                                    if (d < low || d > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                        case 1:
                            w = dims[0];
                            h = dims[2];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    d = dData.getDouble(j * dims[0] * dims[1] + slice * dims[0] + i);
                                    if (d < low || d > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                        case 2:
                            w = dims[0];
                            h = dims[1];
                            orthosliceOverlayImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                            for (int j = 0; j < h; j++) {
                                for (int i = 0; i < w; i++) {
                                    d = dData.getDouble(slice * dims[0] * dims[1] + j * dims[0] + i);
                                    if (d < low || d > up) {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, emptyPixel);
                                    } else {
                                        orthosliceOverlayImages[axis].getRaster().setPixel(i, j, overlaidPixel);
                                    }
                                }
                            }
                            break;
                    }
                    break;

            }

        } else if (overlayField != null && overlaySliceImage != null && inField.getDims().length == 3)
            orthosliceOverlayImages[axis] = overlaySliceImage.getSlice(axis, params.getOrthosliceNumber(axis));

        if (!silent) {
            fireDataProviderOverlayUpdated(axis);
        }

    }

    public Object getFieldValue(int i, int j, int k)
    {
        if (inField == null || inField.getDims().length != 3) {
            return null;
        }

        int[] dims = inField.getDims();

        if (i < 0)
            i = 0;
        if (i >= dims[0])
            i = dims[0] - 1;
        if (j < 0)
            j = 0;
        if (j >= dims[1])
            j = dims[1] - 1;
        if (k < 0)
            k = 0;
        if (k >= dims[2])
            k = dims[2] - 1;

        int component = params.getDataMappingParams().getColorMap0().getDataComponentIndex();
        DataArray da = inField.getComponent(component);
        int vlen = da.getVectorLength();
        switch (da.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = (byte[]) da.getRawArray().getData();
                if (vlen == 1) {
                    return bData[dims[0] * dims[1] * k + dims[0] * j + i];
                } else {
                    byte[] out = new byte[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = bData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_SHORT:
                short[] sData = (short[]) da.getRawArray().getData();
                if (vlen == 1) {
                    return sData[dims[0] * dims[1] * k + dims[0] * j + i];
                } else {
                    short[] out = new short[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = sData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_INT:
                int[] iData = (int[]) da.getRawArray().getData();
                if (vlen == 1) {
                    return iData[dims[0] * dims[1] * k + dims[0] * j + i] & 0xff;
                } else {
                    int[] out = new int[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = iData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_FLOAT:
                float[] fData = (float[]) da.getRawArray().getData();
                if (vlen == 1) {
                    return fData[dims[0] * dims[1] * k + dims[0] * j + i];
                } else {
                    float[] out = new float[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = fData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_DOUBLE:
                double[] dData = (double[]) da.getRawArray().getData();
                if (vlen == 1) {
                    return dData[dims[0] * dims[1] * k + dims[0] * j + i];
                } else {
                    double[] out = new double[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = dData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_STRING:
                String[] strData = ((StringDataArray) da).getRawArray().getData();
                if (vlen == 1) {
                    return strData[dims[0] * dims[1] * k + dims[0] * j + i];
                } else {
                    String[] out = new String[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = strData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v];
                    }
                    return out;
                }
            case FIELD_DATA_LOGIC:
                LogicLargeArray bitData = ((LogicDataArray) da).getRawArray();
                if (vlen == 1) {
                    return bitData.getBoolean(dims[0] * dims[1] * k + dims[0] * j + i);
                } else {
                    boolean[] out = new boolean[vlen];
                    for (int v = 0; v < vlen; v++) {
                        out[v] = bitData.getBoolean((dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v);
                    }
                    return out;
                }
            case FIELD_DATA_COMPLEX:
                float[] fReData = ((ComplexDataArray) da).getFloatRealArray().getData();
                float[] fImData = ((ComplexDataArray) da).getFloatImaginaryArray().getData();
                float[][] out = new float[vlen][];
                if (vlen == 1) {
                    out[0] = new float[]{fReData[dims[0] * dims[1] * k + dims[0] * j + i], fImData[dims[0] * dims[1] * k + dims[0] * j + i]};
                } else {
                    for (int v = 0; v < vlen; v++) {
                        out[v] = new float[]{fReData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v], fImData[(dims[0] * dims[1] * k + dims[0] * j + i) * vlen + v]};
                    }
                }
                return out;
            default:
                return null;
        }
    }

    public int[] getOrthoSliceRGB(int axis, int x, int y)
    {
        if (inField == null || inField.getDims().length != 3)
            return null;

        if (x < 0 || y < 0 || x >= orthosliceImages[axis].getWidth() || y >= orthosliceImages[axis].getHeight())
            return null;

        int[] pixel = null;
        pixel = orthosliceImages[axis].getRaster().getPixel(x, y, pixel);
        return pixel;
    }

    public void updateOrthosliceImage(int axis, boolean silent)
    {
        if (inField == null || inField.getDims().length != 3) {
            return;
        }

        String component = params.getDataMappingParams().getColorMap0().getDataComponentName();

        int[] dims = null;
        int w, h, slice;
        dims = inField.getDims();
        slice = params.getOrthosliceNumber(axis);
        if (slice < 0) {
            slice = 0;
        }
        if (slice >= dims[axis]) {
            slice = dims[axis] - 1;
        }

        float low = params.getDataMappingParams().getColorMap0().getDataMin();
        float up = params.getDataMappingParams().getColorMap0().getDataMax();
        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        double val1,
                val;
        int veclen = inField.getComponent(component).getVectorLength();

        switch (inField.getComponent(component).getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = (byte[]) inField.getComponent(component).getRawArray().getData();
                switch (axis) {
                    case 0:
                        w = dims[1];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xFF & bData[j * dims[0] * dims[1] + i * dims[0] + slice]) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (double) (0xFF & bData[(j * dims[0] * dims[1] + i * dims[0] + slice) * veclen + v]);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 1:
                        w = dims[0];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xFF & bData[j * dims[0] * dims[1] + slice * dims[0] + i]) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (double) (0xFF & bData[(j * dims[0] * dims[1] + slice * dims[0] + i) * veclen + v]);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 2:
                        w = dims[0];
                        h = dims[1];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xFF & bData[slice * dims[0] * dims[1] + j * dims[0] + i]) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (double) (0xFF & bData[(slice * dims[0] * dims[1] + j * dims[0] + i) * veclen + v]);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                }
                break;
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray dData = inField.getComponent(component).getRawArray();
                switch (axis) {
                    case 0:
                        w = dims[1];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((dData.getDouble(j * dims[0] * dims[1] + i * dims[0] + slice) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = dData.getDouble((j * dims[0] * dims[1] + i * dims[0] + slice) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 1:
                        w = dims[0];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((dData.getDouble(j * dims[0] * dims[1] + slice * dims[0] + i) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = dData.getDouble((j * dims[0] * dims[1] + slice * dims[0] + i) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 2:
                        w = dims[0];
                        h = dims[1];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((dData.getDouble(slice * dims[0] * dims[1] + j * dims[0] + i) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = dData.getDouble((slice * dims[0] * dims[1] + j * dims[0] + i) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                }
                break;
            case FIELD_DATA_COMPLEX:
                FloatLargeArray fAbsData = ((ComplexDataArray) inField.getComponent(component)).getFloatAbsArray();
                switch (axis) {
                    case 0:
                        w = dims[1];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((fAbsData.getFloat(j * dims[0] * dims[1] + i * dims[0] + slice) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = fAbsData.getFloat((j * dims[0] * dims[1] + i * dims[0] + slice) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 1:
                        w = dims[0];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((fAbsData.getFloat(j * dims[0] * dims[1] + slice * dims[0] + i) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = fAbsData.getFloat((j * dims[0] * dims[1] + slice * dims[0] + i) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 2:
                        w = dims[0];
                        h = dims[1];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) ((fAbsData.getFloat(slice * dims[0] * dims[1] + j * dims[0] + i) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = fAbsData.getFloat((slice * dims[0] * dims[1] + j * dims[0] + i) * veclen + v);
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                }
                break;
            case FIELD_DATA_LOGIC:
                LogicLargeArray logicData = ((LogicDataArray) inField.getComponent(component)).getRawArray();
                switch (axis) {
                    case 0:
                        w = dims[1];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xFF & logicData.getByte(j * dims[0] * dims[1] + i * dims[0] + slice)) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (float) (0xFF & logicData.getByte((j * dims[0] * dims[1] + i * dims[0] + slice) * veclen + v));
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 1:
                        w = dims[0];
                        h = dims[2];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xff & logicData.getByte(j * dims[0] * dims[1] + slice * dims[0] + i)) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (float) (0xFF & logicData.getByte((j * dims[0] * dims[1] + slice * dims[0] + i) * veclen + v));
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                    case 2:
                        w = dims[0];
                        h = dims[1];
                        orthosliceImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                        for (int j = 0; j < h; j++) {
                            for (int i = 0; i < w; i++) {
                                if (veclen == 1) {
                                    c = (int) (((float) (0xff & logicData.getByte(slice * dims[0] * dims[1] + j * dims[0] + i)) - low) * cs);
                                } else {
                                    val = 0;
                                    for (int v = 0; v < veclen; v++) {
                                        val1 = (float) (0xFF & logicData.getByte((slice * dims[0] * dims[1] + j * dims[0] + i) * veclen + v));
                                        val += val1 * val1;
                                    }
                                    c = (int) ((sqrt(val) - low) * cs);
                                }
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                orthosliceImages[axis].setRGB(i, j, colorMapLUT[c]);
                            }
                        }
                        break;
                }
                break;
            default:
                return;
        }

        if (!silent) {
            fireDataProviderOrthosliceUpdated(axis);
        }
    }

    public void updateSingleImage()
    {
        if (inField == null || inField.getDims().length != 2) {
            return;
        }

        int component = params.getDataMappingParams().getColorMap0().getDataComponentIndex();

        int[] dims = null;
        int w, h;
        dims = inField.getDims();
        if (dims == null || dims.length != 2) {
            return;
        }

        w = dims[0];
        h = dims[1];

        float low = params.getDataMappingParams().getColorMap0().getDataMin();
        float up = params.getDataMappingParams().getColorMap0().getDataMax();
        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;

        switch (inField.getComponent(component).getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = (byte[]) inField.getComponent(component).getRawArray().getData();
                singleImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                for (int j = 0; j < h; j++) {
                    for (int i = 0; i < w; i++) {
                        c = (int) (((float) (0xFF & bData[j * dims[0] + i]) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        singleImage.setRGB(i, j, colorMapLUT[c]);
                    }
                }
                break;
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray dData = inField.getComponent(component).getRawArray();
                singleImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                for (int j = 0; j < h; j++) {
                    for (int i = 0; i < w; i++) {
                        c = (int) (((dData.getDouble(j * dims[0] + i)) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        singleImage.setRGB(i, j, colorMapLUT[c]);
                    }
                }
                break;
            case FIELD_DATA_COMPLEX:
                FloatLargeArray fAbsData = ((ComplexDataArray) inField.getComponent(component)).getFloatAbsArray();
                singleImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                for (int j = 0; j < h; j++) {
                    for (int i = 0; i < w; i++) {
                        c = (int) (((fAbsData.getFloat(j * dims[0] + i)) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        singleImage.setRGB(i, j, colorMapLUT[c]);
                    }
                }
                break;
            case FIELD_DATA_LOGIC:
                LogicLargeArray logicData = ((LogicDataArray) inField.getComponent(component)).getRawArray();
                singleImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                for (int j = 0; j < h; j++) {
                    for (int i = 0; i < w; i++) {
                        c = (int) (((float) (0xFF & logicData.getByte(j * dims[0] + i)) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        singleImage.setRGB(i, j, colorMapLUT[c]);
                    }
                }
                break;
            default:
                return;
        }

        fireDataProviderSingleDataUpdated();
    }

    public BufferedImage getSingleImage()
    {
        return singleImage;
    }

    public BufferedImage getCustomSliceImage()
    {
        return customSliceImage;
    }

    public void updateCustomPlane()
    {
        customPlane.setPlaneParams(params.getCustomPlanePoint(), params.getCustomPlaneVector(), true);
        updateCustomPlaneImage();
    }

    private void updateCustomPlaneImage()
    {
        if (inField == null || inField.getDims().length != 3) {
            customSliceImage = null;
            return;
        }

        float[][] affine = inField.getAffine();
        int[] dims = inField.getDims();

        float[][] base = customPlane.getBase();
        float[][] extents = customPlane.getBaseExtents();
        params.setCustomPlaneBase(base);
        params.setCustomPlaneExtents(extents);

        float[] norm = new float[2];
        norm[0] = 0;
        norm[1] = 0;
        float[] upp = new float[3];
        for (int i = 0; i < 3; i++) {
            upp[i] = 0;
            for (int j = 0; j < 3; j++) {
                upp[i] += affine[i][j] * affine[i][j];
            }
        }
        for (int i = 0; i < 3; i++) {
            norm[0] += base[0][i] * base[0][i] / upp[i];
            norm[1] += base[1][i] * base[1][i] / upp[i];
        }
        norm[0] = (float) sqrt(norm[0]);
        norm[1] = (float) sqrt(norm[1]);

        for (int i = 0; i < 3; i++) {
            base[0][i] = base[0][i] / norm[0];
            base[1][i] = base[1][i] / norm[1];
        }

        float[] baseResolution = new float[2];
        for (int i = 0; i < 2; i++) {
            baseResolution[i] = 0;
            for (int j = 0; j < 3; j++) {
                baseResolution[i] += base[i][j] * base[i][j];
            }
            baseResolution[i] = (float) sqrt(baseResolution[i]);
        }
        params.setCustomPlaneUPPW(baseResolution[0]);
        params.setCustomPlaneUPPH(baseResolution[1]);

        float[] tmp = new float[3];
        for (int i = 0; i < 3; i++) {
            tmp[i] = abs(base[0][i] + base[1][i]);
        }
        float min = tmp[0];
        int mini = 0;
        if (tmp[1] < min) {
            min = tmp[1];
            mini = 1;
        }
        if (tmp[2] < min) {
            min = tmp[2];
            mini = 2;
        }

        int i0 = 0, i1 = 1;
        switch (mini) {
            case 0:
                i0 = 1;
                i1 = 2;
                break;
            case 1:
                i0 = 0;
                i1 = 2;
                break;
            case 2:
                i0 = 0;
                i1 = 1;
                break;
        }

        int w, h;
        if (extents == null) {
            customSliceImage = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
            fireDataProviderCustomSliceUpdated();
            return;
        }
        for (int i = 0; i < 3; i++) {
            tmp[i] = extents[1][i] - extents[0][i];
        }
        float detA = base[0][i0] * base[1][i1] - base[1][i0] * base[0][i1];
        w = (int) ceil(abs((tmp[i0] * base[1][i1] - base[1][i0] * tmp[i1]) / detA)) + 1;
        h = (int) ceil(abs((tmp[i1] * base[0][i0] - base[0][i1] * tmp[i0]) / detA)) + 1;

        if (w <= 0 || h <= 0) {
            customSliceImage = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
            fireDataProviderCustomSliceUpdated();
            return;
        }

        float[][] A = new float[3][3];
        float[] b = new float[3];
        float[][] b0 = new float[2][3];

        A[0][0] = affine[0][0];
        A[1][0] = affine[0][1];
        A[2][0] = affine[0][2];
        A[0][1] = affine[1][0];
        A[1][1] = affine[1][1];
        A[2][1] = affine[1][2];
        A[0][2] = affine[2][0];
        A[1][2] = affine[2][1];
        A[2][2] = affine[2][2];

        int component = params.getDataMappingParams().getColorMap0().getDataComponentIndex();
        if (component < 0)
            component = 0;

        int x, y, z, off;
        b0 = new float[2][3];
        for (int k = 0; k < 3; k++) {
            b[k] = extents[0][k] - affine[3][k];
        }
        tmp = MatrixMath.lsolve3x3(A, b);
        b0[0] = MatrixMath.lsolve3x3(A, base[0]);
        b0[1] = MatrixMath.lsolve3x3(A, base[1]);

        float low = params.getDataMappingParams().getColorMap0().getDataMin();
        float up = params.getDataMappingParams().getColorMap0().getDataMax();
        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int veclen = inField.getComponent(component).getVectorLength();
        int c;

        customSliceImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        switch (inField.getComponent(component).getType()) {
            case FIELD_DATA_BYTE:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                RegularFieldInterpolator.interpolateFieldToSliceColormappedImage(inField.getComponent(component).getRawArray(), veclen, dims, tmp, b0, customSliceImage, colorMapLUT, low, up, params.getSliceFillColor(), w, h, params.isCustomOrthoPlanesInterpolation());
                break;
            case FIELD_DATA_COMPLEX:
                RegularFieldInterpolator.interpolateFieldToSliceColormappedImage(((ComplexDataArray) inField.getComponent(component)).getFloatAbsArray(), veclen, dims, tmp, b0, customSliceImage, colorMapLUT, low, up, params.getSliceFillColor(), w, h, params.isCustomOrthoPlanesInterpolation());
                break;
            case FIELD_DATA_LOGIC:
                if (params.isCustomPlaneInterpolation()) {
                    UnsignedByteLargeArray byteData = ((LogicDataArray) inField.getComponent(component)).getRawByteArray();
                    byte[] interpolatedData;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            x = (int) tmp[0];
                            y = (int) tmp[1];
                            z = (int) tmp[2];
                            if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                                interpolatedData = inField.getInterpolatedData(byteData, tmp[0], tmp[1], tmp[2]);
                                if (interpolatedData != null && interpolatedData.length == 1) {
                                    c = (int) (((float) (0xFF & interpolatedData[0]) - low) * cs);
                                    if (c < 0) {
                                        c = 0;
                                    }
                                    if (c > colorMapSize) {
                                        c = colorMapSize;
                                    }
                                    customSliceImage.setRGB(i, j, colorMapLUT[c]);
                                }
                            } else {
                                customSliceImage.setRGB(i, j, params.getSliceFillColor());
                            }
                            tmp[0] += b0[0][0];
                            tmp[1] += b0[0][1];
                            tmp[2] += b0[0][2];
                        }
                        tmp[0] -= w * b0[0][0];
                        tmp[1] -= w * b0[0][1];
                        tmp[2] -= w * b0[0][2];
                        tmp[0] += b0[1][0];
                        tmp[1] += b0[1][1];
                        tmp[2] += b0[1][2];
                    }
                } else {
                    LogicLargeArray logicData = ((LogicDataArray) inField.getComponent(component)).getRawArray();
                    tmp[0] += 0.5f;
                    tmp[1] += 0.5f;
                    tmp[2] += 0.5f;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            x = (int) tmp[0];
                            y = (int) tmp[1];
                            z = (int) tmp[2];
                            if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                                c = (int) (((float) (0xff & logicData.getByte(z * dims[0] * dims[1] + y * dims[0] + x)) - low) * cs);
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                customSliceImage.setRGB(i, j, colorMapLUT[c]);
                            } else {
                                customSliceImage.setRGB(i, j, params.getSliceFillColor());
                            }
                            tmp[0] += b0[0][0];
                            tmp[1] += b0[0][1];
                            tmp[2] += b0[0][2];
                        }
                        tmp[0] -= w * b0[0][0];
                        tmp[1] -= w * b0[0][1];
                        tmp[2] -= w * b0[0][2];
                        tmp[0] += b0[1][0];
                        tmp[1] += b0[1][1];
                        tmp[2] += b0[1][2];
                    }
                }
                break;
            default:
                return;
        }
        fireDataProviderCustomSliceUpdated();
    }

    public GeometryObject getCustomSlice3DPlane()
    {
        return customSlice3DPlane;
    }

    /**
     * @return the params
     */
    public DataProviderParams getParams()
    {
        return params;
    }

    /**
     * @return the inField
     */
    public RegularField getInField()
    {
        return inField;
    }

    public RegularField getOverlayField()
    {
        return overlayField;
    }

    /**
     * @param inField the inField to set
     */
    public void setInField(RegularField field)
    {
        if (field == null)
            return;
        boolean reset = (this.inField == null || (this.inField != null && !this.inField.isStructureCompatibleWith(field)));
        boolean resetOverlayRange = (this.inField == null || (this.inField != null && !this.inField.isCompatibleWith(field)));
        this.inField = field;
        params.setSilent(true);
        if (inField == null) {
            this.overlaySliceImage = null;
            int[] d = {10, 10, 10};
            params.setDims(d);
            params.getDataMappingParams().setInData(field, (DataContainerSchema) null);
            params.setSilent(false);
            this.ui.setInfield(field);
            updateAll();
            return;
        }

        if (params.getSingleComponent() >= inField.getNComponents()) {
            params.setSingleComponent(0);
        }

        if(params.getSimpleOverlayComponent() < 0 || params.getSimpleOverlayComponent() >= inField.getNComponents()) {
            params.setSilent(true);
            params.setSimpleOverlay(false);
            params.setSimpleOverlayComponent(0);
            resetOverlayRange = true;
            params.setSilent(false);
        }
        if(resetOverlayRange) {
            int c = params.getSimpleOverlayComponent();
            float up = (float)inField.getComponent(c).getPreferredMaxValue();
            float low = (float)(inField.getComponent(c).getPreferredMaxValue()+inField.getComponent(c).getPreferredMinValue())/2;
            params.setSilent(true);
            params.setSimpleOverlayLowUp(low, up);
            params.setSilent(false);
        }

        int[] dims = this.inField.getDims();
        float[][] affine = field.getAffine();
        float[] baseCellSize = {0, 0, 0};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                baseCellSize[i] += affine[i][j] * affine[i][j];
            }
            baseCellSize[i] = (float) sqrt(baseCellSize[i]);
        }

        params.setDims(dims);
        params.setPoint0(affine[3]);
        params.getDataMappingParams().setInData(field, (DataContainerSchema) null);

        if (dims != null && dims.length == 3) {
            params.setUPPS(baseCellSize[0], baseCellSize[1], baseCellSize[2]);
            customPlane.setField(field);
            for (int i = 0; i < 3; i++) {
                customOrthoPlanes[i].setField(field);
            }
        } else if (dims != null && dims.length == 2) {
            params.setUPPS(baseCellSize[0], baseCellSize[1], baseCellSize[1]);
        }

        this.ui.setInfield(field);
        if (reset) {
            resetCustomPlane();
            resetCustomOrthoPlanes();
        }

        params.setSilent(false);
        updateAll();
    }

    public void setOverlayField(RegularField field)
    {
        if (inField == null)
            return;

        if (field == null) {
            this.overlayField = null;
            this.overlaySliceImage = null;
            return;
        }

        int[] inFieldDims = inField.getDims();
        int[] overlayFieldDims = field.getDims();
        if (inFieldDims.length != overlayFieldDims.length || overlayFieldDims.length != 3) {
            System.err.println("ERROR: overlay field dimensions do not match data field dimensions");
            return;
        }
        if (inFieldDims[0] != overlayFieldDims[0] || inFieldDims[1] != overlayFieldDims[1] || inFieldDims[2] != overlayFieldDims[2]) {
            System.err.println("ERROR: overlay field dimensions do not match data field dimensions");
            return;
        }

        float[][] inFieldAffine = inField.getAffine();
        float[][] overlayFieldAffine = field.getAffine();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                if (overlayFieldAffine[i][j] != inFieldAffine[i][j]) {
                    System.err.println("ERROR: overlay field affine do not match data field affine");
                    return;
                }
            }
        }

        this.overlayField = field;
        this.overlaySliceImage = new SliceImage(overlayField);
        overlaySliceImage.setMultiselImage(true);
    }

    @Override
    public void onOrthosliceNumberChanged(OrthosliceNumberChangedEvent evt)
    {
        if (evt.getAxis() == -1) {
            updateOrthosliceImages();
            updateOrthosliceIsolines();
            updateOrthosliceOverlays();
        } else {
            updateOrthosliceImage(evt.getAxis(), false);
            updateOrthosliceIsoline(evt.getAxis(), false);
            updateOrthosliceIsolines(evt.getAxis(), false);
            updateOrthosliceOverlay(evt.getAxis(), false);
        }

        geometryParams.setIntersectionPoint(params.getOrthosliceNumbers().clone());
    }

    @Override
    public void onRgbComponentChanged(RgbComponentChangedEvent evt)
    {
        updateOrthosliceImages();
        updateCustomPlaneImage();
        updateCustomOrthoPlanesImages();
        updateSingleImage();
    }

    @Override
    public void onColormapChanged(ColormapChangedEvent evt)
    {
        updateColorMap();
        updateOrthosliceImages();
        updateCustomPlaneImage();
        updateCustomOrthoPlanesImages();
        updateSingleImage();
    }

    @Override
    public void onRgbComponentWeightChanged(RgbComponentWeightChangedEvent evt)
    {
        updateOrthosliceImages();
        updateCustomPlaneImage();
        updateCustomOrthoPlanesImages();
        updateSingleImage();
    }

    @Override
    public void onOverlayChanged(DataProviderParamsEvent evt)
    {
        updateOrthosliceOverlays();
        updateCustomOrthoPlanesOverlays();
    }

    @Override
    public void onOverlayOpacityChanged(DataProviderParamsEvent evt)
    {
        fireDataProviderOverlayUpdated(-1);
    }

    @Override
    public void onCustomPlaneChanged(CustomPlaneChangedEvent evt)
    {
        updateCustomPlane();
    }

    @Override
    public void onIsolineThresholdChanged(IsolineThresholdChangedEvent evt)
    {
        updateOrthosliceIsolines();
    }

    private transient ArrayList<DataProviderListener> listenerList = new ArrayList<DataProviderListener>();

    public synchronized void addDataProviderListener(DataProviderListener listener)
    {
        listenerList.add(listener);
    }

    public synchronized void removeDataProviderListener(DataProviderListener listener)
    {
        listenerList.remove(listener);
    }

    public void centerSlices()
    {
        if (inField == null || inField.getDims().length != 3) {
            return;
        }

        int[] dims = inField.getDims();
        params.setOrthosliceNumbers(dims[0] / 2, dims[1] / 2, dims[2] / 2);

        float[][] affine = inField.getAffine();
        float[] p0 = new float[3];
        p0[0] = affine[3][0] + (affine[0][0] * (dims[0] - 1) + affine[1][0] * (dims[1] - 1) + affine[2][0] * (dims[2] - 1)) / 2;
        p0[1] = affine[3][1] + (affine[0][1] * (dims[0] - 1) + affine[1][1] * (dims[1] - 1) + affine[2][1] * (dims[2] - 1)) / 2;
        p0[2] = affine[3][2] + (affine[0][2] * (dims[0] - 1) + affine[1][2] * (dims[1] - 1) + affine[2][2] * (dims[2] - 1)) / 2;

        float[] v = {1.0f, 0.0f, 0.0f};
        params.setCustomPlaneParams(p0.clone(), v);

        float[][] v0 = {{1.0f, 0.0f, 0.0f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, -1.0f}};
        float[][][] b0
                = {
                    {
                        {0.0f, 1.0f, 0.0f},
                        {0.0f, 0.0f, -1.0f}
                    },
                    {
                        {1.0f, 0.0f, 0.0f},
                        {0.0f, 0.0f, -1.0f}
                    },
                    {
                        {1.0f, 0.0f, 0.0f},
                        {0.0f, 1.0f, 0.0f}
                    }
                };
        params.setCustomOrthoPlanesBase(b0);
        params.setCustomOrthoPlanesParams(p0.clone(), v0);
    }

    public void resetCustomPlane()
    {
        if (inField == null || inField.getDims().length != 3) {
            return;
        }

        float[][] pts = inField.getPreferredExtents();
        float[] p = new float[3];
        p[0] = (pts[1][0] + pts[0][0]) / 2;
        p[1] = (pts[1][1] + pts[0][1]) / 2;
        p[2] = (pts[1][2] + pts[0][2]) / 2;
        float[] v = {0.0f, 0.0f, 1.0f};
        params.setCustomPlaneParams(p, v);
    }

    public void resetCustomOrthoPlanes()
    {
        if (inField == null || inField.getDims().length != 3) {
            return;
        }

        float[][] pts = inField.getPreferredExtents();
        float[] p = new float[3];
        p[0] = (pts[1][0] + pts[0][0]) / 2;
        p[1] = (pts[1][1] + pts[0][1]) / 2;
        p[2] = (pts[1][2] + pts[0][2]) / 2;
        float[][] v = {{-1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, -1.0f}};
        params.setCustomOrthoPlanesParams(p, v);
    }

    private void fireDataProviderOrthosliceUpdated(int axis)
    {
        for (DataProviderListener listener : listenerList) {
            listener.onDataProviderOrthosliceUpdated(axis);
        }
    }

    private void fireDataProviderOverlayUpdated(int axis)
    {
        for (DataProviderListener listener : listenerList) {
            listener.onDataProviderOverlayUpdated(axis);
        }
    }

    private void fireDataProviderCustomSliceUpdated()
    {
        for (DataProviderListener listener : listenerList) {
            listener.onDataProviderCustomSliceUpdated();
        }
    }

    private void fireDataProviderSingleDataUpdated()
    {
        for (DataProviderListener listener : listenerList) {
            listener.onDataProviderSingleDataUpdated();
        }
    }

    /**
     * @return the geometryParams
     */
    public GeometryParams getGeometryParams()
    {
        return geometryParams;
    }

    /**
     * @param geometryParams the geometryParams to set
     */
    public void setGeometryParams(GeometryParams geometryParams)
    {
        this.geometryParams = geometryParams;
    }

    /**
     * @return the auxField
     */
    public RegularField getAuxField()
    {
        return auxField;
    }

    /**
     * @param auxField the auxField to set
     */
    public void setAuxField(RegularField auxField)
    {
        if (inField == null || inField.getDims().length != 3 || auxField == null || auxField.getDims() == null || auxField.getDims().length != 3) {
            auxField = null;
            for (int i = 0; i < 3; i++) {
                mapIsoline[i] = null;
            }
            return;
        }
        int[] d = auxField.getDims();
        int[] dims = inField.getDims();
        for (int i = 0; i < d.length; i++) {
            if (d[i] != dims[i]) {
                return;
            }
        }
        this.auxField = auxField;
        for (int i = 0; i < 3; i++) {
            mapIsoline[i] = null;

        }
    }

    @Override
    public void onCustomOrthoPlaneChanged(CustomOrthoPlaneChangedEvent evt)
    {
        boolean[] mask = evt.getChangeMask();
        if (mask == null || mask.length != 4) {
            updateCustomOrthoPlanes();
            return;
        }

        if (mask[3]) { //vector update
            updateCustomOrthoPlanes();
            return;
        }

        if (mask[0] || mask[1] || mask[2]) { //point update
            updateCustomOrthoPlanesParams(true);
        }

        if (mask[0]) {
            updateCustomOrthoPlaneImage(0, true);
            updateCustomOrthoPlaneOverlay(0, true);
        }
        if (mask[1]) {
            updateCustomOrthoPlaneImage(1, true);
            updateCustomOrthoPlaneOverlay(1, true);
        }
        if (mask[2]) {
            updateCustomOrthoPlaneImage(2, true);
            updateCustomOrthoPlaneOverlay(2, true);
        }

        if (mask[0] || mask[1] || mask[2]) {
            if (mask[0]) {
                fireDataProviderCustomOrthoSliceUpdated(0);
            }
            if (mask[1]) {
                fireDataProviderCustomOrthoSliceUpdated(1);
            }
            if (mask[2]) {
                fireDataProviderCustomOrthoSliceUpdated(2);
            }
        }

        float[] p = params.getCustomOrthoPlanesPoint();
        if (geometryParams.getInField() != null) {
            int[] p0 = geometryParams.getInField().getIndices(p[0], p[1], p[2]);
            geometryParams.setIntersectionPoint(p0);
        }

    }

    public BufferedImage getCustomOrthoSliceImage(int axis)
    {
        return customOrthoSlicesImages[axis];
    }

    public BufferedImage getCustomOrthoSliceOverlay(int axis)
    {
        if ((overlayField == null && !params.isSimpleOverlay()) || axis < 0 || axis > 2) {
            return null;
        }

        if (customOrthoSlicesOverlays[axis] == null) {
            updateCustomOrthoPlaneOverlay(axis, true);
        }

        return customOrthoSlicesOverlays[axis];
    }

    private void updateCustomOrthoPlanesParams(boolean silent)
    {
        for (int i = 0; i < 3; i++) {
            updateCustomOrthoPlaneParams(i, true);
        }
        if (!silent) {
            fireDataProviderCustomOrthoSliceUpdated(-1);
        }
    }

    public void updateCustomOrthoPlanesImages()
    {
        for (int i = 0; i < 3; i++) {
            updateCustomOrthoPlaneImage(i, true);
        }
        fireDataProviderCustomOrthoSliceUpdated(-1);
    }

    public void updateCustomOrthoPlanesOverlays()
    {
        for (int i = 0; i < 3; i++) {
            updateCustomOrthoPlaneOverlay(i, true);
        }
        fireDataProviderOrthosliceUpdated(-1);
    }

    private void updateCustomOrthoPlanes()
    {
        for (int i = 0; i < 3; i++) {
            synchronized (customOrthoPlanes) {
                updateCustomOrthoPlaneParams(i, true);
                updateCustomOrthoPlaneImage(i, true);
                updateCustomOrthoPlaneOverlay(i, true);
            }
        }
        fireDataProviderCustomOrthoSliceUpdated(-1);
    }

    private void updateCustomOrthoPlane(int axis, boolean silent)
    {
        updateCustomOrthoPlaneParams(axis, true);
        updateCustomOrthoPlaneImage(axis, true);
        updateCustomOrthoPlaneOverlay(axis, true);
        if (!silent) {
            fireDataProviderCustomOrthoSliceUpdated(axis);
        }
    }

    private void updateCustomOrthoPlaneParams(int axis, boolean silent)
    {
        if (inField == null || inField.getDims().length != 3) {
            return;
        }

        float[][] affine = inField.getAffine();
        float[] norm = new float[2];
        norm[0] = 0;
        norm[1] = 0;
        float[] upp = new float[3];
        for (int i = 0; i < 3; i++) {
            upp[i] = 0;
            for (int j = 0; j < 3; j++) {
                upp[i] += affine[i][j] * affine[i][j];
            }
        }

        customOrthoPlanes[axis].setPlaneParams(params.getCustomOrthoPlanesPoint(), params.getCustomOrthoPlanesVectors()[axis], params.getCustomOrthoPlanesBase(axis));
        float[][] base = customOrthoPlanes[axis].getBase();
        float[][] extents = customOrthoPlanes[axis].getBaseExtents();
        params.setCustomOrthoPlanesBase(axis, base);
        params.setCustomOrthoPlanesExtents(axis, extents);

        for (int i = 0; i < 3; i++) {
            norm[0] += base[0][i] * base[0][i] / upp[i];
            norm[1] += base[1][i] * base[1][i] / upp[i];
        }
        norm[0] = (float) sqrt(norm[0]);
        norm[1] = (float) sqrt(norm[1]);

        for (int i = 0; i < 3; i++) {
            base[0][i] = base[0][i] / norm[0];
            base[1][i] = base[1][i] / norm[1];
        }

        float[] baseResolution = new float[2];
        for (int i = 0; i < 2; i++) {
            baseResolution[i] = 0;
            for (int j = 0; j < 3; j++) {
                baseResolution[i] += base[i][j] * base[i][j];
            }
            baseResolution[i] = (float) sqrt(baseResolution[i]);
        }
        params.setCustomOrthoPlanesUPPW(axis, baseResolution[0]);
        params.setCustomOrthoPlanesUPPH(axis, baseResolution[1]);

        if (!silent) {
            fireDataProviderCustomOrthoSliceUpdated(axis);
        }
    }

    private void updateCustomOrthoPlaneImage(int axis, boolean silent)
    {
        if (inField == null || inField.getDims().length != 3) {
            customOrthoSlicesImages[axis] = null;
            return;
        }

        float[][] affine = inField.getAffine();
        int[] dims = inField.getDims();
        float[][] base = customOrthoPlanes[axis].getBase();
        float[][] extents = customOrthoPlanes[axis].getBaseExtents();

        float[] tmp = new float[3];
        for (int i = 0; i < 3; i++) {
            tmp[i] = abs(base[0][i] + base[1][i]);
        }
        float min = tmp[0];
        int mini = 0;
        if (tmp[1] < min) {
            min = tmp[1];
            mini = 1;
        }
        if (tmp[2] < min) {
            min = tmp[2];
            mini = 2;
        }

        int i0 = 0, i1 = 1;
        switch (mini) {
            case 0:
                i0 = 1;
                i1 = 2;
                break;
            case 1:
                i0 = 0;
                i1 = 2;
                break;
            case 2:
                i0 = 0;
                i1 = 1;
                break;
        }

        int w, h;
        if (extents == null) {
            customOrthoSlicesImages[axis] = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
            if (!silent) {
                fireDataProviderCustomOrthoSliceUpdated(axis);
            }
            return;
        }
        for (int i = 0; i < 3; i++) {
            tmp[i] = extents[1][i] - extents[0][i];
        }
        float detA = base[0][i0] * base[1][i1] - base[1][i0] * base[0][i1];
        w = (int) round(abs((tmp[i0] * base[1][i1] - base[1][i0] * tmp[i1]) / detA)) + 1;
        h = (int) round(abs((tmp[i1] * base[0][i0] - base[0][i1] * tmp[i0]) / detA)) + 1;

        if (w <= 0 || h <= 0) {
            customOrthoSlicesImages[axis] = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
            if (!silent) {
                fireDataProviderCustomOrthoSliceUpdated(axis);
            }
            return;
        }

        float[][] A = new float[3][3];
        float[] b = new float[3];
        float[][] b0 = new float[2][3];

        A[0][0] = affine[0][0];
        A[1][0] = affine[0][1];
        A[2][0] = affine[0][2];
        A[0][1] = affine[1][0];
        A[1][1] = affine[1][1];
        A[2][1] = affine[1][2];
        A[0][2] = affine[2][0];
        A[1][2] = affine[2][1];
        A[2][2] = affine[2][2];

        int component = params.getDataMappingParams().getColorMap0().getDataComponentIndex();

        int x, y, z, off;
        b0 = new float[2][3];
        for (int k = 0; k < 3; k++) {
            b[k] = extents[0][k] - affine[3][k];
        }
        tmp = MatrixMath.lsolve3x3(A, b);

        b0[0] = MatrixMath.lsolve3x3(A, base[0]);
        b0[1] = MatrixMath.lsolve3x3(A, base[1]);

        float low = params.getDataMappingParams().getColorMap0().getDataMin();
        float up = params.getDataMappingParams().getColorMap0().getDataMax();
        int veclen = inField.getComponent(component).getVectorLength();

        customOrthoSlicesImages[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        switch (inField.getComponent(component).getType()) {
            case FIELD_DATA_BYTE:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                RegularFieldInterpolator.interpolateFieldToSliceColormappedImage(inField.getComponent(component).getRawArray(), veclen, dims, tmp, b0, customOrthoSlicesImages[axis], colorMapLUT, low, up, params.getSliceFillColor(), w, h, params.isCustomOrthoPlanesInterpolation());
                break;
            case FIELD_DATA_COMPLEX:
                RegularFieldInterpolator.interpolateFieldToSliceColormappedImage(((ComplexDataArray) inField.getComponent(component)).getFloatAbsArray(), veclen, dims, tmp, b0, customOrthoSlicesImages[axis], colorMapLUT, low, up, params.getSliceFillColor(), w, h, params.isCustomOrthoPlanesInterpolation());
                break;
            case FIELD_DATA_LOGIC:
                int colorMapSize = colorMapLUT.length - 1;
                float cs = (float) colorMapSize / (up - low);
                int c;
                if (params.isCustomOrthoPlanesInterpolation()) {
                    UnsignedByteLargeArray byteData = ((LogicDataArray) inField.getComponent(component)).getRawByteArray();
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            x = (int) tmp[0];
                            y = (int) tmp[1];
                            z = (int) tmp[2];
                            if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                                c = (int) (((float) (0xFF & RegularFieldInterpolator.getInterpolatedScalarData(byteData, dims, tmp[0], tmp[1], tmp[2])) - low) * cs);
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                customOrthoSlicesImages[axis].setRGB(i, j, colorMapLUT[c]);
                            } else {
                                customOrthoSlicesImages[axis].setRGB(i, j, params.getSliceFillColor());
                            }
                            tmp[0] += b0[0][0];
                            tmp[1] += b0[0][1];
                            tmp[2] += b0[0][2];
                        }
                        tmp[0] -= w * b0[0][0];
                        tmp[1] -= w * b0[0][1];
                        tmp[2] -= w * b0[0][2];
                        tmp[0] += b0[1][0];
                        tmp[1] += b0[1][1];
                        tmp[2] += b0[1][2];
                    }
                } else {
                    LogicLargeArray logicData = ((LogicDataArray) inField.getComponent(component)).getRawArray();
                    tmp[0] += 0.5f;
                    tmp[1] += 0.5f;
                    tmp[2] += 0.5f;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            x = (int) tmp[0];
                            y = (int) tmp[1];
                            z = (int) tmp[2];
                            if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                                c = (int) (((float) (0xff & logicData.getByte(z * dims[0] * dims[1] + y * dims[0] + x)) - low) * cs);
                                if (c < 0) {
                                    c = 0;
                                }
                                if (c > colorMapSize) {
                                    c = colorMapSize;
                                }
                                customOrthoSlicesImages[axis].setRGB(i, j, colorMapLUT[c]);
                            } else {
                                customOrthoSlicesImages[axis].setRGB(i, j, params.getSliceFillColor());
                            }
                            tmp[0] += b0[0][0];
                            tmp[1] += b0[0][1];
                            tmp[2] += b0[0][2];
                        }
                        tmp[0] -= w * b0[0][0];
                        tmp[1] -= w * b0[0][1];
                        tmp[2] -= w * b0[0][2];
                        tmp[0] += b0[1][0];
                        tmp[1] += b0[1][1];
                        tmp[2] += b0[1][2];
                    }
                }
                break;
            default:
                return;
        }
        if (!silent) {
            fireDataProviderCustomOrthoSliceUpdated(axis);
        }
    }

    private void updateCustomOrthoPlaneOverlay(int axis, boolean silent)
    {
        for (int i = 0; i < customOrthoSlicesOverlays.length; i++) {
            customOrthoSlicesOverlays[i] = null;
        }

        if (params.isSimpleOverlay() && inField != null && inField.getComponent(params.getSimpleOverlayComponent()) != null) {
            float[][] affine = inField.getAffine();
            int[] dims = inField.getDims();
            float[][] base = customOrthoPlanes[axis].getBase();
            float[][] extents = customOrthoPlanes[axis].getBaseExtents();

            float[] tmp = new float[3];
            for (int i = 0; i < 3; i++) {
                tmp[i] = abs(base[0][i] + base[1][i]);
            }
            float min = tmp[0];
            int mini = 0;
            if (tmp[1] < min) {
                min = tmp[1];
                mini = 1;
            }
            if (tmp[2] < min) {
                min = tmp[2];
                mini = 2;
            }

            int i0 = 0, i1 = 1;
            switch (mini) {
                case 0:
                    i0 = 1;
                    i1 = 2;
                    break;
                case 1:
                    i0 = 0;
                    i1 = 2;
                    break;
                case 2:
                    i0 = 0;
                    i1 = 1;
                    break;
            }

            int w, h;
            if (extents == null) {
                customOrthoSlicesOverlays[axis] = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
                if (!silent) {
                    fireDataProviderCustomOrthoSliceUpdated(axis);
                }
                return;
            }
            for (int i = 0; i < 3; i++) {
                tmp[i] = extents[1][i] - extents[0][i];
            }
            float detA = base[0][i0] * base[1][i1] - base[1][i0] * base[0][i1];
            w = (int) round(abs((tmp[i0] * base[1][i1] - base[1][i0] * tmp[i1]) / detA)) + 1;
            h = (int) round(abs((tmp[i1] * base[0][i0] - base[0][i1] * tmp[i0]) / detA)) + 1;

            if (w <= 0 || h <= 0) {
                customOrthoSlicesOverlays[axis] = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
                if (!silent) {
                    fireDataProviderCustomOrthoSliceUpdated(axis);
                }
                return;
            }

            float[][] A = new float[3][3];
            float[] b = new float[3];
            float[][] b0 = new float[2][3];

            A[0][0] = affine[0][0];
            A[1][0] = affine[0][1];
            A[2][0] = affine[0][2];
            A[0][1] = affine[1][0];
            A[1][1] = affine[1][1];
            A[2][1] = affine[1][2];
            A[0][2] = affine[2][0];
            A[1][2] = affine[2][1];
            A[2][2] = affine[2][2];

            int component = params.getSimpleOverlayComponent();
            b0 = new float[2][3];
            for (int k = 0; k < 3; k++) {
                b[k] = extents[0][k] - affine[3][k];
            }
            tmp = MatrixMath.lsolve3x3(A, b);

            b0[0] = MatrixMath.lsolve3x3(A, base[0]);
            b0[1] = MatrixMath.lsolve3x3(A, base[1]);

            float[] emptyPixel = new float[4];
            float[] overlaidPixel = new float[4];
            Color c = params.getSimpleOverlayColor();

            if (params.isSimpleOverlayMask()) {
                if (params.isSimpleOverlayInvert()) {
                    emptyPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 0.0f};
                    overlaidPixel = new float[]{0.0f, 0.0f, 0.0f, 255.0f};
                } else {
                    emptyPixel = new float[]{0.0f, 0.0f, 0.0f, 255.0f};
                    overlaidPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 0.0f};
                }
            } else if (params.isSimpleOverlayInvert()) {
                emptyPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 255.0f};
                overlaidPixel = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
            } else {
                emptyPixel = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
                overlaidPixel = new float[]{c.getRed(), c.getGreen(), c.getBlue(), 255.0f};
            }

            float low = params.getSimpleOverlayLow();
            float up = params.getSimpleOverlayUp();

            switch (inField.getComponent(component).getType()) {
                case FIELD_DATA_BYTE:
                    byte[] outBData = new byte[w * h];
                    RegularFieldInterpolator.interpolateScalarFieldToSlice((UnsignedByteLargeArray) inField.getComponent(component).getRawArray(), dims, tmp, b0, outBData, w, h, params.isCustomOrthoPlanesInterpolation());
                    customOrthoSlicesOverlays[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                    int bb;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            bb = (int) (outBData[j * w + i] & 0xff);
                            if (bb < low || bb > up) {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, emptyPixel);
                            } else {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, overlaidPixel);
                            }
                        }
                    }
                    break;
                case FIELD_DATA_SHORT:
                    short[] outSData = new short[w * h];
                    RegularFieldInterpolator.interpolateScalarFieldToSlice((ShortLargeArray) inField.getComponent(component).getRawArray(), dims, tmp, b0, outSData, w, h, params.isCustomOrthoPlanesInterpolation());
                    customOrthoSlicesOverlays[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                    short ss;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            ss = outSData[j * w + i];
                            if (ss < low || ss > up) {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, emptyPixel);
                            } else {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, overlaidPixel);
                            }
                        }
                    }
                    break;
                case FIELD_DATA_INT:
                    int[] outIData = new int[w * h];
                    RegularFieldInterpolator.interpolateScalarFieldToSlice((IntLargeArray) inField.getComponent(component).getRawArray(), dims, tmp, b0, outIData, w, h, params.isCustomOrthoPlanesInterpolation());
                    customOrthoSlicesOverlays[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                    int ii;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            ii = outIData[j * w + i];
                            if (ii < low || ii > up) {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, emptyPixel);
                            } else {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, overlaidPixel);
                            }
                        }
                    }
                    break;
                case FIELD_DATA_FLOAT:
                    float[] outFData = new float[w * h];
                    RegularFieldInterpolator.interpolateScalarFieldToSlice((FloatLargeArray) inField.getComponent(component).getRawArray(), dims, tmp, b0, outFData, w, h, params.isCustomOrthoPlanesInterpolation());
                    customOrthoSlicesOverlays[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                    float ff;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            ff = outFData[j * w + i];
                            if (ff < low || ff > up) {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, emptyPixel);
                            } else {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, overlaidPixel);
                            }
                        }
                    }
                    break;
                case FIELD_DATA_DOUBLE:
                    double[] outDData = new double[w * h];
                    RegularFieldInterpolator.interpolateScalarFieldToSlice((DoubleLargeArray) inField.getComponent(component).getRawArray(), dims, tmp, b0, outDData, w, h, params.isCustomOrthoPlanesInterpolation());
                    customOrthoSlicesOverlays[axis] = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                    double dd;
                    for (int j = 0; j < h; j++) {
                        for (int i = 0; i < w; i++) {
                            dd = outDData[j * w + i];
                            if (dd < low || dd > up) {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, emptyPixel);
                            } else {
                                customOrthoSlicesOverlays[axis].getRaster().setPixel(i, j, overlaidPixel);
                            }
                        }
                    }
                    break;
            }

            if (!silent) {
                fireDataProviderOverlayUpdated(axis);
            }

        } else if (overlayField != null && overlayField.getDims().length == 3 && inField.getComponent(params.getSimpleOverlayComponent()) != null) {
            float[][] affine = overlayField.getAffine();
            int[] dims = overlayField.getDims();
            float[][] base = customOrthoPlanes[axis].getBase();
            float[][] extents = customOrthoPlanes[axis].getBaseExtents();

            float[] tmp = new float[3];
            for (int i = 0; i < 3; i++) {
                tmp[i] = abs(base[0][i] + base[1][i]);
            }
            float min = tmp[0];
            int mini = 0;
            if (tmp[1] < min) {
                min = tmp[1];
                mini = 1;
            }
            if (tmp[2] < min) {
                min = tmp[2];
                mini = 2;
            }

            int i0 = 0, i1 = 1;
            switch (mini) {
                case 0:
                    i0 = 1;
                    i1 = 2;
                    break;
                case 1:
                    i0 = 0;
                    i1 = 2;
                    break;
                case 2:
                    i0 = 0;
                    i1 = 1;
                    break;
            }

            int w, h;
            if (extents == null) {
                customOrthoSlicesOverlays[axis] = null;
                if (!silent) {
                    fireDataProviderOverlayUpdated(axis);
                }
                return;
            }
            for (int i = 0; i < 3; i++) {
                tmp[i] = extents[1][i] - extents[0][i];
            }
            float detA = base[0][i0] * base[1][i1] - base[1][i0] * base[0][i1];
            w = (int) round(abs((tmp[i0] * base[1][i1] - base[1][i0] * tmp[i1]) / detA)) + 1;
            h = (int) round(abs((tmp[i1] * base[0][i0] - base[0][i1] * tmp[i0]) / detA)) + 1;

            if (w <= 0 || h <= 0) {
                customOrthoSlicesOverlays[axis] = null;
                if (!silent) {
                    fireDataProviderOverlayUpdated(axis);
                }
                return;
            }

            float[][] A = new float[3][3];
            float[] b = new float[3];
            float[][] b0 = new float[2][3];
            A[0][0] = affine[0][0];
            A[1][0] = affine[0][1];
            A[2][0] = affine[0][2];
            A[0][1] = affine[1][0];
            A[1][1] = affine[1][1];
            A[2][1] = affine[1][2];
            A[0][2] = affine[2][0];
            A[1][2] = affine[2][1];
            A[2][2] = affine[2][2];

            int component = 0;
            b0 = new float[2][3];
            for (int k = 0; k < 3; k++) {
                b[k] = extents[0][k] - affine[3][k];
            }
            tmp = MatrixMath.lsolve3x3(A, b);
            b0[0] = MatrixMath.lsolve3x3(A, base[0]);
            b0[1] = MatrixMath.lsolve3x3(A, base[1]);

            ColorSpace colorSpace = ColorSpace.getInstance(ColorSpace.CS_sRGB);
            int[] nBits = {8, 8, 8, 8};
            ComponentColorModel colorModel = new ComponentColorModel(colorSpace, nBits, true, true, Transparency.TRANSLUCENT, 0);
            WritableRaster raster = colorModel.createCompatibleWritableRaster(w, h);
            customOrthoSlicesOverlays[axis] = new BufferedImage(colorModel, raster, false, null);
            float low = 1;
            float up = (float) overlayField.getComponent(component).getPreferredMaxValue();
            if (overlayField.getComponent(component) instanceof ByteDataArray)
                RegularFieldInterpolator.interpolateScalarFieldToSliceColormappedImage(
                        (UnsignedByteLargeArray) overlayField.getComponent(component).getRawArray(),
                        dims, tmp, b0, customOrthoSlicesOverlays[axis],
                        overlayColorMapLUT, low, up, params.getSliceFillColor(),
                        w, h, false, 0, 0x77);
            else
                RegularFieldInterpolator.interpolateScalarFieldToSliceColormappedImage(
                        overlayField.getComponent(component).getRawByteArray(),
                        dims, tmp, b0, customOrthoSlicesOverlays[axis],
                        overlayColorMapLUT, low, up, params.getSliceFillColor(),
                        w, h, false, 0, 0x77);
            if (!silent) {
                fireDataProviderOverlayUpdated(axis);
            }
        }

    }

    public GeometryObject getCustomOrthoSlices3DPlanes()
    {
        return customOrthoSlices3DPlanes;
    }

    private void fireDataProviderCustomOrthoSliceUpdated(int axis)
    {
        for (DataProviderListener listener : listenerList) {
            listener.onDataProviderCustomOrthoSliceUpdated(axis);
        }
    }

    private void updateColorMap()
    {
        this.colorMap = params.getDataMappingParams().getColorMap0().getMap();
        this.colorMapLUT = params.getDataMappingParams().getColorMap0().getRGBColorTable();
    }

    public static RegularField createCustomSliceField(float[] p, float[] v, RegularField field, int component)
    {
        if (field == null || field.getDims().length != 3) {
            return null;
        }

        float[][] affine = field.getAffine();
        int[] dims = field.getDims();

        float[] find = field.getFloatIndices(p[0], p[1], p[2]);
        if (find[0] < 0 || find[0] >= dims[0] || find[1] < 0 || find[1] >= dims[1] || find[2] < 0 || find[2] >= dims[2])
            return null;

        SimplifiedPlane3D p3d = new SimplifiedPlane3D(p, v, field);
        float[][] base = p3d.getBase();
        float[][] extents = p3d.getBaseExtents();

        float[] norm = new float[2];
        norm[0] = 0;
        norm[1] = 0;
        float[] upp = new float[3];
        for (int i = 0; i < 3; i++) {
            upp[i] = 0;
            for (int j = 0; j < 3; j++) {
                upp[i] += affine[i][j] * affine[i][j];
            }
        }
        for (int i = 0; i < 3; i++) {
            norm[0] += base[0][i] * base[0][i] / upp[i];
            norm[1] += base[1][i] * base[1][i] / upp[i];
        }
        norm[0] = (float) sqrt(norm[0]);
        norm[1] = (float) sqrt(norm[1]);

        for (int i = 0; i < 3; i++) {
            base[0][i] = base[0][i] / norm[0];
            base[1][i] = base[1][i] / norm[1];
        }

        float[] tmp = new float[3];
        for (int i = 0; i < 3; i++) {
            tmp[i] = abs(base[0][i] + base[1][i]);
        }
        float min = tmp[0];
        int mini = 0;
        if (tmp[1] < min) {
            min = tmp[1];
            mini = 1;
        }
        if (tmp[2] < min) {
            min = tmp[2];
            mini = 2;
        }

        int i0 = 0, i1 = 1;
        switch (mini) {
            case 0:
                i0 = 1;
                i1 = 2;
                break;
            case 1:
                i0 = 0;
                i1 = 2;
                break;
            case 2:
                i0 = 0;
                i1 = 1;
                break;
        }

        int w, h;
        if (extents == null) {
            return null;
        }
        for (int i = 0; i < 3; i++) {
            tmp[i] = extents[1][i] - extents[0][i];
        }
        float detA = base[0][i0] * base[1][i1] - base[1][i0] * base[0][i1];
        w = (int) round(abs((tmp[i0] * base[1][i1] - base[1][i0] * tmp[i1]) / detA)) + 1;
        h = (int) round(abs((tmp[i1] * base[0][i0] - base[0][i1] * tmp[i0]) / detA)) + 1;

        if (w <= 1 || h <= 1) {
            return null;
        }

        float[][] A = new float[3][3];
        float[] b = new float[3];
        float[][] b0 = new float[2][3];

        A[0][0] = affine[0][0];
        A[1][0] = affine[0][1];
        A[2][0] = affine[0][2];
        A[0][1] = affine[1][0];
        A[1][1] = affine[1][1];
        A[2][1] = affine[1][2];
        A[0][2] = affine[2][0];
        A[1][2] = affine[2][1];
        A[2][2] = affine[2][2];

        int x, y, z, off;
        b0 = new float[2][3];
        for (int k = 0; k < 3; k++) {
            b[k] = extents[0][k] - affine[3][k];
        }
        tmp = MatrixMath.lsolve3x3(A, b);

        b0[0] = MatrixMath.lsolve3x3(A, base[0]);
        b0[1] = MatrixMath.lsolve3x3(A, base[1]);

        byte[] outData;
        int[] outDims;
        float[][] outAffine;

        outData = new byte[w * h];
        outDims = new int[]{w, h};
        if (field.getComponent(component).getType() == DataArrayType.FIELD_DATA_BYTE) {
            RegularFieldInterpolator.interpolateScalarFieldToSlice((UnsignedByteLargeArray) field.getComponent(component).getRawArray(), dims, tmp, b0, outData, w, h, true);
        } else {
            RegularFieldInterpolator.interpolateScalarFieldToSlice(field.getComponent(component).getRawByteArray(), dims, tmp, b0, outData, w, h, true);
        }

        outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
            outAffine[0][i] = base[0][i];
            outAffine[1][i] = base[1][i];
            outAffine[2][i] = 0.0f;
            outAffine[3][i] = extents[0][i];
        }

        RegularField out = new RegularField(outDims);
        out.setAffine(outAffine);
        out.addComponent(DataArray.create(outData, 1, "slice_" + field.getComponent(component).getName()));
        return out;
    }

}
