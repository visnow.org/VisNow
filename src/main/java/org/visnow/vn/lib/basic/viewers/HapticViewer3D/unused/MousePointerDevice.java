/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.awt.MouseInfo;
import java.awt.Toolkit;
import org.jogamp.java3d.Canvas3D;

/**
 * @deprecated NOT USED!
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class MousePointerDevice extends AbstractPointerDevice
{

    static String buttonNames[];

    {
        initButtonNames();
    }

    @Override
    public void makeActive(Canvas3D canvas)
    {
        if (activeCanvas != null) {
            //remove listeners
        }
        activeCanvas = canvas;
        //add listeners
    }

    public int getDOF()
    {
        return 2;
    }

    public int getButtonCount()
    {
        return buttonNames.length;
    }

    public String[] getButtonNames()
    {
        return buttonNames;
    }

    private static void initButtonNames()
    {
        int buttonCount = MouseInfo.getNumberOfButtons();
        buttonNames = new String[buttonCount];
        if (buttonCount == 2) {
            buttonNames[0] = "L Button";
            buttonNames[1] = "R Button";
        } else {
            for (int i = 1; i <= buttonCount; ++i) {
                buttonNames[i - 1] = Toolkit.getProperty("AWT.button" + i, "Button" + i);
            }
        }
    }
}
