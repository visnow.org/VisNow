/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.ParallelCoordinates;

import java.awt.Color;
import java.util.ArrayList;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 13 October 2013
 */
public class ParallelCoordinatesParams extends Parameters
{

    public static final int GEOMETRY_TYPE_2D = 0;
    public static final int GEOMETRY_TYPE_3D = 1;
    public static final int SELECTION_LOGIC_AND = 0;
    public static final int SELECTION_LOGIC_OR = 1;

    public ParallelCoordinatesParams()
    {
        super(eggs);
        this.active = false;
        setAxesColor(Color.WHITE);
        setLineColor(Color.BLUE);
        setSelectionColor(Color.RED);
        setVariables(new ArrayList<PCVariable>());
        setDownsize(new int[]{1, 1, 1});
        setDefaultDownsize(this.getDownsize().clone());
        FontParams fp = new FontParams();
        fp.setSize(0.015f);
        setFontParams(fp);
        this.active = true;
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("nVariables", ParameterType.independent, 0),
        new ParameterEgg<int[]>("downsize", ParameterType.independent, null),
        new ParameterEgg<int[]>("defaultDownsize", ParameterType.independent, null),
        new ParameterEgg<Integer>("geometryType", ParameterType.independent, GEOMETRY_TYPE_2D),
        new ParameterEgg<ArrayList<PCVariable>>("variables", ParameterType.dependent, null),
        new ParameterEgg<Integer>("selectionLogic", ParameterType.independent, SELECTION_LOGIC_AND),
        new ParameterEgg<Float>("scale", ParameterType.independent, 1.0f),
        new ParameterEgg<Integer>("selectionType", ParameterType.independent, ParallelCoordinatesObject2D.SELECTION_TYPE_HIGHLIGHT),
        new ParameterEgg<Integer>("dataMapping", ParameterType.independent, ParallelCoordinatesObject2D.DATA_MAPPING_DIRECT),
        new ParameterEgg<Color>("axesColor", ParameterType.independent, null),
        new ParameterEgg<Color>("lineColor", ParameterType.independent, null),
        new ParameterEgg<Color>("selectionColor", ParameterType.independent, null),
        new ParameterEgg<Float>("selectionDim", ParameterType.independent, 0.3f),
        new ParameterEgg<Boolean>("clipColorRange", ParameterType.independent, false),
        new ParameterEgg<Float>("planesTransparency", ParameterType.independent, 0.5f),
        new ParameterEgg<Float>("selectionAreaTransparency", ParameterType.independent, 0.5f),
        new ParameterEgg<Integer>("lines3DType", ParameterType.independent, ParallelCoordinatesObject3D.LINES3D_TYPE_LINES),
        new ParameterEgg<Float>("lineWidth", ParameterType.independent, 0.25f),
        new ParameterEgg<FontParams>("fontParams", ParameterType.independent, null),};

    public void setNVariables(int nVariables)
    {
        if (nVariables < 2) {
            return;
        }
        this.setValue("nVariables", nVariables);
    }

    public int getNVariables()
    {
        return (Integer) this.getValue("nVariables");
    }

    public void setClipColorRange(boolean clipColorRange)
    {
        this.setValue("clipColorRange", clipColorRange);
    }

    public boolean isClipColorRange()
    {
        return (Boolean) this.getValue("clipColorRange");
    }

    public void setDataMapping(int dataMapping)
    {
        this.setValue("dataMapping", dataMapping);
    }

    public int getDataMapping()
    {
        return (Integer) this.getValue("dataMapping");
    }

    public void setSelectionDim(float selectionDim)
    {
        this.setValue("selectionDim", selectionDim);
    }

    public float getSelectionDim()
    {
        return (Float) this.getValue("selectionDim");
    }

    public void setAxesColor(Color axesColor)
    {
        this.setValue("axesColor", axesColor);
    }

    public Color getAxesColor()
    {
        return (Color) this.getValue("axesColor");
    }

    public void setLineColor(Color lineColor)
    {
        this.setValue("lineColor", lineColor);
    }

    public Color getLineColor()
    {
        return (Color) this.getValue("lineColor");
    }

    public void setSelectionColor(Color selectionColor)
    {
        this.setValue("selectionColor", selectionColor);
    }

    public Color getSelectionColor()
    {
        return (Color) this.getValue("selectionColor");
    }

    public void setDownsize(int[] downsize)
    {
        this.setValue("downsize", downsize);
    }

    public int[] getDownsize()
    {
        return (int[]) this.getValue("downsize");
    }

    public void setDefaultDownsize(int[] downsize)
    {
        this.setValue("defaultDownsize", downsize);
    }

    public int[] getDefaultDownsize()
    {
        return (int[]) this.getValue("defaultDownsize");
    }

    public void setGeometryType(int geometryType)
    {
        this.setValue("geometryType", geometryType);
    }

    public int getGeometryType()
    {
        return (Integer) this.getValue("geometryType");
    }

    public void setVariables(ArrayList<PCVariable> variables)
    {
        this.setValue("variables", variables);
    }

    public ArrayList<PCVariable> getVariables()
    {
        return (ArrayList<PCVariable>) this.getValue("variables");
    }

    public void setSelectionLogic(int selectionLogic)
    {
        this.setValue("selectionLogic", selectionLogic);
    }

    public int getSelectionLogic()
    {
        return (Integer) this.getValue("selectionLogic");
    }

    public void setScale(float scale)
    {
        this.setValue("scale", scale);
    }

    public float getScale()
    {
        return (Float) this.getValue("scale");
    }

    public void setSelectionType(int selectionType)
    {
        this.setValue("selectionType", selectionType);
    }

    public int getSelectionType()
    {
        return (Integer) this.getValue("selectionType");
    }

    public void setPlanesTransparency(float planesTransparency)
    {
        this.setValue("planesTransparency", planesTransparency);
    }

    public float getPlanesTransparency()
    {
        return (Float) this.getValue("planesTransparency");
    }

    public void setSelectionAreaTransparency(float selectionAreaTransparency)
    {
        this.setValue("selectionAreaTransparency", selectionAreaTransparency);
    }

    public float getSelectionAreaTransparency()
    {
        return (Float) this.getValue("selectionAreaTransparency");
    }

    public void setLines3DType(int lines3DType)
    {
        this.setValue("lines3DType", lines3DType);
    }

    public int getLines3DType()
    {
        return (Integer) this.getValue("lines3DType");
    }

    public void setLineWidth(float lineWidth)
    {
        this.setValue("lineWidth", lineWidth);
    }

    public float getLineWidth()
    {
        return (Float) this.getValue("lineWidth");
    }

    public void setFontParams(FontParams fontParams)
    {
        this.setValue("fontParams", fontParams);
    }

    public FontParams getFontParams()
    {
        return (FontParams) this.getValue("fontParams");
    }

    public void reset(Field field, int geometryType)
    {
        boolean oldActive = active;
        boolean oldParameterActive = parameterActive;
        active = false;
        parameterActive = false;

        setNVariables(2);

        ArrayList<PCVariable> variables = getVariables();
        variables.clear();
        int nScalarComponents = 0;
        int firstScalar = -1;
        int secondScalar = -1;
        int thirdScalar = -1;
        int fourthScalar = -1;
        for (int i = 0; i < field.getNComponents(); i++) {
            if (field.getComponent(i).getVectorLength() == 1) {
                nScalarComponents++;
                if (firstScalar == -1)
                    firstScalar = i;
                else if (firstScalar != -1 && secondScalar == -1)
                    secondScalar = i;
                else if (firstScalar != -1 && secondScalar != -1 && thirdScalar == -1)
                    thirdScalar = i;
                else if (firstScalar != -1 && secondScalar != -1 && thirdScalar != -1 && fourthScalar == -1)
                    fourthScalar = i;
            }
        }

        if (nScalarComponents > 3 && geometryType == GEOMETRY_TYPE_3D) {
            variables.add(new PCVariable(firstScalar, secondScalar,
                                         field.getComponent(firstScalar).getName(), field.getComponent(secondScalar).getName(),
                                         (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue(),
                                         (float)field.getComponent(secondScalar).getPreferredMinValue(), (float)field.getComponent(secondScalar).getPreferredMaxValue()));
            variables.add(new PCVariable(thirdScalar, fourthScalar,
                                         field.getComponent(thirdScalar).getName(), field.getComponent(fourthScalar).getName(),
                                         (float)field.getComponent(thirdScalar).getPreferredMinValue(), (float)field.getComponent(thirdScalar).getPreferredMaxValue(),
                                         (float)field.getComponent(fourthScalar).getPreferredMinValue(), (float)field.getComponent(fourthScalar).getPreferredMaxValue()));
        } else if (nScalarComponents > 1) {
            switch (geometryType) {
                case GEOMETRY_TYPE_2D:
                    variables.add(new PCVariable(firstScalar, field.getComponent(firstScalar).getName(), (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    variables.add(new PCVariable(secondScalar, field.getComponent(secondScalar).getName(), (float)field.getComponent(secondScalar).getPreferredMinValue(), (float)field.getComponent(secondScalar).getPreferredMaxValue()));
                    break;
                case GEOMETRY_TYPE_3D:
                    variables.add(new PCVariable(firstScalar, firstScalar,
                                                 field.getComponent(firstScalar).getName(), field.getComponent(firstScalar).getName(),
                                                 (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue(),
                                                 (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    variables.add(new PCVariable(secondScalar, secondScalar,
                                                 field.getComponent(secondScalar).getName(), field.getComponent(secondScalar).getName(),
                                                 (float)field.getComponent(secondScalar).getPreferredMinValue(), (float)field.getComponent(secondScalar).getPreferredMaxValue(),
                                                 (float)field.getComponent(secondScalar).getPreferredMinValue(), (float)field.getComponent(secondScalar).getPreferredMaxValue()));
                    break;
            }
        } else if (nScalarComponents > 0) {
            switch (geometryType) {
                case GEOMETRY_TYPE_2D:
                    variables.add(new PCVariable(firstScalar, field.getComponent(firstScalar).getName(), (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    variables.add(new PCVariable(firstScalar, field.getComponent(firstScalar).getName(), (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    break;
                case GEOMETRY_TYPE_3D:
                    variables.add(new PCVariable(firstScalar, firstScalar,
                                                 field.getComponent(firstScalar).getName(), field.getComponent(firstScalar).getName(),
                                                (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue(),
                                                 (float)field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    variables.add(new PCVariable(firstScalar, firstScalar,
                                                 field.getComponent(firstScalar).getName(), field.getComponent(firstScalar).getName(),
                                                (float) field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue(),
                                                (float) field.getComponent(firstScalar).getPreferredMinValue(), (float)field.getComponent(firstScalar).getPreferredMaxValue()));
                    break;
            }
        }
        setVariables(variables);
        setDownsize(getDefaultDownsize().clone());
        setPlanesTransparency(0.5f);
        setLines3DType(ParallelCoordinatesObject3D.LINES3D_TYPE_LINES);
        active = oldActive;
        parameterActive = oldParameterActive;
    }

}
