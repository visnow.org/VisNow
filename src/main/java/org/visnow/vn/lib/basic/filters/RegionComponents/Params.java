/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.RegionComponents;
import org.visnow.jscic.DataContainer;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{
    private ComponentSubrange range = new ComponentSubrange();

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("maxComponents", ParameterType.dependent, 20)
    };

    public Params()
    {
        super(eggs);
        range.numericsOnly(true).scalarsOnly(true);
    }

    public ComponentSubrange getComponentRange()
    {
        return range;
    }

    public void setField(DataContainer container)
    {
        range.setContainerSchema(container.getSchema());
    }

    
    public int getMaxComps()
    {
        return (Integer)getValue("maxComponents");
    }
    
    public void setMaxComps(int maxComps)
    {
        setValue("maxComponents", maxComps);
    }
}
