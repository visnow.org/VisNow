/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Canny;

import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Andrzej Rutkowski (rudy@mat.uni.torun.pl)
 */
public class CannyMaskedGradGUI extends javax.swing.JPanel
{

    private CannyMaskedGradParams params = new CannyMaskedGradParams();

    public CannyMaskedGradParams getParams()
    {
        return params;
    }

    public void setParams(CannyMaskedGradParams params)
    {
        this.params = params;
        updateGUI();
    }

    /**
     * Creates new form ConvolutionPanel
     */
    public CannyMaskedGradGUI()
    {
        initComponents();
        updateGUI();
    }

    private void updateGUI()
    {
        if (params.getOutputType() > outputTypeBox.getItemCount()) {
            params.setOutputType(outputTypeBox.getItemCount() - 1);
        }
        outputTypeBox.setSelectedIndex(params.getOutputType());
        dirOnly.setSelected(params.isDirOnly());
        directionBar.setValX(params.getDirX());
        directionBar.setValY(params.getDirY());
        appendMask.setSelected(params.isAppendMask());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel3 = new javax.swing.JPanel();
        outputTypeBox = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        directionBar = new org.visnow.vn.gui.widgets.DoubleSlider();
        dirOnly = new javax.swing.JCheckBox();
        appendMask = new javax.swing.JCheckBox();
        holdButton = new javax.swing.JToggleButton();

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Output"));
        jPanel3.setPreferredSize(new java.awt.Dimension(240, 246));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        outputTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Float", "8-bit" }));
        outputTypeBox.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                outputTypeBoxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 142;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel3.add(outputTypeBox, gridBagConstraints);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Direction"));
        jPanel1.setMinimumSize(new java.awt.Dimension(150, 100));
        jPanel1.setLayout(new java.awt.BorderLayout());
        jPanel1.add(directionBar, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 35;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel3.add(jPanel1, gridBagConstraints);

        dirOnly.setText("One direction only");
        dirOnly.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                dirOnlyItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel3.add(dirOnly, gridBagConstraints);

        appendMask.setText("Append mask");
        appendMask.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                appendMaskItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel3.add(appendMask, gridBagConstraints);

        holdButton.setText("Hold");
        holdButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                holdButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                        .addComponent(holdButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(holdButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(212, Short.MAX_VALUE))
                );
    }// </editor-fold>//GEN-END:initComponents

    private void outputTypeBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_outputTypeBoxItemStateChanged
        // TODO add your handling code here:
        String s = outputTypeBox.getSelectedItem().toString();
        if (s.equals("Float"))
            params.setOutputType(0);
        else if (s.equals("8-bit"))
            params.setOutputType(1);
        else
            params.setOutputType(0);
        fireStateChanged();
    }//GEN-LAST:event_outputTypeBoxItemStateChanged

    private void holdButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_holdButtonActionPerformed
        fireStateChanged();
    }//GEN-LAST:event_holdButtonActionPerformed

    private void dirOnlyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dirOnlyItemStateChanged
        params.setDirOnly(dirOnly.isSelected());
        fireStateChanged();
    }//GEN-LAST:event_dirOnlyItemStateChanged

    private void directionBarDoubleValueChanged(org.visnow.vn.lib.utils.events.DoubleValueModificationEvent evt) {//GEN-FIRST:event_directionBarDoubleValueChanged
        params.setDirX(directionBar.getX());
        params.setDirY(directionBar.getY());
        if (!directionBar.isAdjusting())
            fireStateChanged();
    }//GEN-LAST:event_directionBarDoubleValueChanged

    private void appendMaskItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_appendMaskItemStateChanged
        params.setAppendMask(appendMask.isSelected());
        fireStateChanged();
    }//GEN-LAST:event_appendMaskItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox appendMask;
    private javax.swing.JCheckBox dirOnly;
    private org.visnow.vn.gui.widgets.DoubleSlider directionBar;
    private javax.swing.JToggleButton holdButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JComboBox outputTypeBox;
    // End of variables declaration//GEN-END:variables

    private Vector<ChangeListener> changeListeners = new Vector<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListeners.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListeners.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        if (holdButton.isSelected())
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListeners) {
            listener.stateChanged(e);
        }
    }

}
