/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import javax.swing.event.ChangeEvent;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GeometryParamsEvent extends ChangeEvent
{

    public static final int TYPE_ALL = 0;
    //public static final int TYPE_POINT = 1;
    public static final int TYPE_GLYPHS = 2;
    public static final int TYPE_LABELS = 3;
    public static final int TYPE_CONNECTION = 4;
    public static final int TYPE_CONN2D = 5;
    public static final int TYPE_CONN3D = 6;
    public static final int TYPE_POINT_SELECTION = 7;
    public static final int TYPE_POINT_ADDED = 10;
    public static final int TYPE_POINT_REMOVED = 11;
    public static final int TYPE_POINT_MODIFIED = 12;
    public static final int TYPE_POINT_CLASS = 13;

    private int type = TYPE_ALL;

    public GeometryParamsEvent(Object source, int type)
    {
        super(source);
        this.type = type;
    }

    /**
     * @return the type
     */
    public int getType()
    {
        return type;
    }

}
