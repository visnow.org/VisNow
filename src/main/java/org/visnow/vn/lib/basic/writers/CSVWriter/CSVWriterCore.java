/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.CSVWriter;

import java.io.File;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.dataarrays.DataArray;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import com.univocity.parsers.tsv.TsvWriter;
import com.univocity.parsers.tsv.TsvWriterSettings;
import java.util.ArrayList;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.utils.StringUtils;

/**
 * Static methods for writing CSV files.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class CSVWriterCore
{

    private static CsvWriterSettings generateCsvSettings(String fieldDelimiter, String nullValue, String emptyValue)
    {
        CsvWriterSettings writerSettings = new CsvWriterSettings();
        if (!(fieldDelimiter == null || fieldDelimiter.isEmpty())) {
            writerSettings.getFormat().setDelimiter(fieldDelimiter);
        }
        if (nullValue != null && !nullValue.isEmpty()) {
            writerSettings.setNullValue(nullValue);
        }
        if (emptyValue != null && !emptyValue.isEmpty()) {
            writerSettings.setEmptyValue(emptyValue);
        }
        return writerSettings;
    }

    private static TsvWriterSettings generateTsvSettings(String nullValue, String emptyValue)
    {
        TsvWriterSettings writerSettings = new TsvWriterSettings();
        if (nullValue != null && !nullValue.isEmpty()) {
            writerSettings.setNullValue(nullValue);
        }
        if (emptyValue != null && !emptyValue.isEmpty()) {
            writerSettings.setEmptyValue(emptyValue);
        }
        return writerSettings;
    }

    /**
     * Writes RegualarField to a CSV or TSV file.
     * Only 1D and 2D fields without explicit corrdinates are supported.
     * Only the current time value is stored in a file.
     *
     * @param field          field
     * @param columnLabels   column labels, can be null
     * @param rowLabels      row labels, can be null
     * @param componentIndex index of the field component that will be written to a file
     * @param path           output file path
     * @param fieldDelimiter field delimeter, if null, then CSV file is created
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeField(RegularField field, RegularField columnLabels, RegularField rowLabels, int componentIndex, String path, String fieldDelimiter)
    {
        if (field == null || field.getDimNum() > 2 || componentIndex < 0 || componentIndex >= field.getNComponents() || path == null || path.isEmpty()) {
            return false;
        }
        DataArray da = field.getComponent(componentIndex);
        DataArray columnLabelsDa = columnLabels != null ? columnLabels.getComponent(0) : null;
        DataArray rowLabelsDa = rowLabels != null ? rowLabels.getComponent(0) : null;
        return writeDataArray(da, field.getLDims(), columnLabelsDa, rowLabelsDa, path, fieldDelimiter);
    }

    /**
     * Writes DataArray to a CSV or TSV file.
     * ObjectDataArray is not supported.
     * Only the current time value is stored in a file. *
     *
     * @param da             data array
     * @param fieldDims      field dimensions, must be of length 1 or 2
     * @param columnLabels   column labels, can be null
     * @param rowLabels      row labels, can be null
     * @param path           output file path
     * @param fieldDelimiter field delimeter, if null, then CSV file is created
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeDataArray(DataArray da, long[] fieldDims, DataArray columnLabels, DataArray rowLabels, String path, String fieldDelimiter)
    {
        if (da == null || da.getType() == DataArrayType.FIELD_DATA_OBJECT || da.getVectorLength() > 1 || fieldDims == null || fieldDims.length < 1 || fieldDims.length > 2 || path == null || path.isEmpty()) {
            return false;
        }
        if (fieldDelimiter != null && fieldDelimiter.equals("\\t")) {
            return writeTsvDataArray(da, fieldDims, columnLabels, rowLabels, path);
        } else {
            return writeCsvDataArray(da, fieldDims, columnLabels, rowLabels, path, fieldDelimiter);
        }
    }

    /**
     * Writes DataArray to a CSV file.
     * ObjectDataArray is not supported.
     * Only the current time value is stored in a file.
     *
     * @param da             data array
     * @param fieldDims      field dimensions, must be of length 1 or 2
     * @param columnLabels   column labels, can be null
     * @param rowLabels      row labels, can be null
     * @param path           output file path
     * @param fieldDelimiter field delimeter, can be null
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeCsvDataArray(DataArray da, long[] fieldDims, DataArray columnLabels, DataArray rowLabels, String path, String fieldDelimiter)
    {
        if (da == null || da.getType() == DataArrayType.FIELD_DATA_OBJECT || da.getVectorLength() > 1 || fieldDims == null || fieldDims.length < 1 || fieldDims.length > 2 || path == null || path.isEmpty()) {
            return false;
        }
        CsvWriter writer = null;
        try {
            CsvWriterSettings settings = generateCsvSettings(fieldDelimiter, null, null);
            writer = new CsvWriter(new File(path), settings);
            String[] columnLabelsStr = null;
            String[] rowLabelsStr = null;
            int rows, columns;
            TimeData timeData = da.getTimeData();
            ArrayList<Float> timeSeries = timeData.getTimesAsList();
            int timeColumn = timeSeries.size() > 1 ? 1 : 0;
            if (fieldDims.length == 2) {
                columns = (int) fieldDims[0];
                rows = (int) fieldDims[1];
            } else {
                columns = 1;
                rows = (int) fieldDims[0];
            }

            if (rowLabels != null && rowLabels.getVectorLength() == 1 && rowLabels.getNElements() == rows) {
                rowLabelsStr = new String[rows];
                LargeArray la = rowLabels.getRawArray();
                for (int j = 0; j < rows; j++) {
                    rowLabelsStr[j] = la.get(j).toString();
                }
            }
            int rowLabelsColumn = rowLabelsStr != null ? 1 : 0;
            if (columnLabels != null && columnLabels.getVectorLength() == 1 && columnLabels.getNElements() == columns) {
                columnLabelsStr = new String[columns + timeColumn + rowLabelsColumn];
                LargeArray la = columnLabels.getRawArray();
                if(rowLabelsColumn == 1) {
                    columnLabelsStr[0] = "";    
                }
                for (int j = rowLabelsColumn; j < rowLabelsColumn + columns; j++) {
                    columnLabelsStr[j] = la.get(j - rowLabelsColumn).toString();
                }
                if (timeColumn == 1) {
                    columnLabelsStr[rowLabelsColumn + columns] = "Time";
                }
                writer.writeHeaders(columnLabelsStr);
            }

            
            for (Float time : timeSeries) {
                LargeArray data = timeData.getValue(time);
                if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    for (int i = 0; i < rows; i++) {
                        if (rowLabelsStr != null) {
                            writer.addValue(rowLabelsStr[i]);
                        }
                        for (int j = 0; j < columns; j++) {
                            float[] elem = (float[]) data.get(i * columns + j);
                            writer.addValue(StringUtils.complexToString(elem, "%g"));
                        }
                        if (timeColumn == 1) {
                            writer.addValue(time);
                        }
                        writer.writeValuesToRow();
                    }
                } else {
                    for (int i = 0; i < rows; i++) {
                        if (rowLabelsStr != null) {
                            writer.addValue(rowLabelsStr[i]);
                        }
                        for (int j = 0; j < columns; j++) {
                            Object elem = data.get(i * columns + j);
                            writer.addValue(elem);
                        }
                        if (timeColumn == 1) {
                            writer.addValue(time);
                        }
                        writer.writeValuesToRow();
                    }
                }
            }
            writer.close();
            return true;
        } catch (Exception ex) {
            if (writer != null) {
                writer.close();
            }
            return false;
        }
    }

    /**
     * Writes DataArray to a TSV file.
     * ObjectDataArray is not supported.
     * Only the current time value is stored in a file.
     *
     * @param da           data array
     * @param fieldDims    field dimensions, must be of length 1 or 2
     * @param columnLabels column labels, can be null
     * @param rowLabels    row labels, can be null
     * @param path         output file path
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeTsvDataArray(DataArray da, long[] fieldDims, DataArray columnLabels, DataArray rowLabels, String path)
    {
        if (da == null || da.getType() == DataArrayType.FIELD_DATA_OBJECT || da.getVectorLength() > 1 || fieldDims == null || fieldDims.length < 1 || fieldDims.length > 2 || path == null || path.isEmpty()) {
            return false;
        }
        TsvWriter writer = null;
        try {
            TsvWriterSettings settings = generateTsvSettings(null, null);
            writer = new TsvWriter(new File(path), settings);
            String[] columnLabelsStr = null;
            String[] rowLabelsStr = null;
            int rows, columns;
            TimeData timeData = da.getTimeData();
            ArrayList<Float> timeSeries = timeData.getTimesAsList();
            int timeColumn = timeSeries.size() > 1 ? 1 : 0;
            if (fieldDims.length == 2) {
                columns = (int) fieldDims[0];
                rows = (int) fieldDims[1];
            } else {
                columns = 1;
                rows = (int) fieldDims[0];
            }

            if (columnLabels != null && columnLabels.getVectorLength() == 1 && columnLabels.getNElements() == columns) {
                columnLabelsStr = new String[columns + timeColumn];
                LargeArray la = columnLabels.getRawArray();
                for (int j = 0; j < columns; j++) {
                    columnLabelsStr[j] = la.get(j).toString();
                }
                if (timeColumn == 1) {
                    columnLabelsStr[columns] = "Time";
                }
                writer.writeHeaders(columnLabelsStr);
            }

            if (rowLabels != null && rowLabels.getVectorLength() == 1 && rowLabels.getNElements() == rows) {
                rowLabelsStr = new String[rows];
                LargeArray la = rowLabels.getRawArray();
                for (int j = 0; j < rows; j++) {
                    rowLabelsStr[j] = la.get(j).toString();
                }
            }

            for (Float time : timeSeries) {
                LargeArray data = timeData.getValue(time);
                if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    for (int i = 0; i < rows; i++) {
                        if (rowLabelsStr != null) {
                            writer.addValue(rowLabelsStr[i]);
                        }
                        for (int j = 0; j < columns; j++) {
                            float[] elem = (float[]) data.get(i * columns + j);
                            writer.addValue(StringUtils.complexToString(elem, "%g"));
                        }
                        if (timeColumn == 1) {
                            writer.addValue(time);
                        }
                        writer.writeValuesToRow();
                    }
                } else {
                    for (int i = 0; i < rows; i++) {
                        if (rowLabelsStr != null) {
                            writer.addValue(rowLabelsStr[i]);
                        }
                        for (int j = 0; j < columns; j++) {
                            Object elem = data.get(i * columns + j);
                            writer.addValue(elem);
                        }
                        if (timeColumn == 1) {
                            writer.addValue(time);
                        }
                        writer.writeValuesToRow();
                    }
                }
            }
            writer.close();
            return true;
        } catch (Exception ex) {
            if (writer != null) {
                writer.close();
            }
            return false;
        }
    }
}
