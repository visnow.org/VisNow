/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.haptics.forces_panels.NewForceDialog;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.AbstractRegularFieldForce;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Damping;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;

/**
 * Used in {@link NewForceDialog} to display list of forces. Displays short and quite nice names of
 * forces instead of default full qualified class names.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class NewForceWrapper
{

    IForce force;

    public NewForceWrapper(IForce force)
    {
        this.force = force;
    }

    @Override
    public String toString()
    {
        String name = force.getClassSimpleName();
        if (force instanceof AbstractRegularFieldForce || force instanceof Damping)
            return name;
        else
            return name + " (" + force.getForceCoordinateSystem() + ")";
    }

    public IForce getForce()
    {
        return force;
    }
}
