/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import org.jogamp.java3d.utils.universe.SimpleUniverse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jogamp.java3d.Bounds;
import org.jogamp.java3d.PhysicalEnvironment;
import javax.swing.JOptionPane;
import org.visnow.vn.geometries.viewer3d.Display3DInternalFrame;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PointerChangeListener;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceName;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceType;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.IPassiveDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.InputFields;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.Pointer3DViewBehavior;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.HapticException;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.HapticPointerController;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceManager;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.tablet.ITabletDevice;

/**
 * A class that lives in HapticViewer3D and is responsible for managing devices and pointers in this
 * HapticViewer3D. Previously those methods existed in HapticViewer3D. They were moved here to split
 * responsibilities in order to keep the code in HapticViewer3D clean and simple.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ViewerDeviceManager
{

    protected HapticViewer3D viewer;
    protected Display3DInternalFrame window = null;
    protected PhysicalEnvironment physEnv;
    /**
     * Devices owned (used) by the HapticViewer3D connected with this ViewerDeviceManager.
     */
    private Map<String, IPassiveDevice> devices = new HashMap<String, IPassiveDevice>();
    //
    private Map<String, InputDevicePointerController> controllers
        = new HashMap<String, InputDevicePointerController>();
    /**
     * to be changed to an Array or Map when more than one pointer will be available (needs changes
     * in Java3D).
     */
    Pointer3DViewBehavior currentPointer3D = null;

    public ViewerDeviceManager(HapticViewer3D viewer)
    {
        this.viewer = viewer;

        support = new ViewerDeviceManagerListenerSupport();
        DeviceManager.registerAsListener(support);
    }

    /**
     * Adds physical environment to the Display3DFrame object. It saves a reference to this
     * Display3DFrame object for future use.
     * <p/>
     * @param window Display3DInternalFrame used by the <code>viewer</code>. Passed in a separate method
     *               because it's constructed in a swingRun() asynchronous method.
     */
    void initialize(Display3DInternalFrame window)
    {
        this.window = window;

        physEnv = Pointer3DViewBehavior.getPhysEnvironment();

        // attach physical environment to our HapticViewer3D's canvas
        window.getDisplayPanel().getCanvas().getView()
            .setPhysicalEnvironment(physEnv);

        // PickObject must be notified about changes in active pointer to notify the pointer about currently enabled pick types.
        addPointerListener(window.getDisplayPanel().getPickObject());

    }

    /**
     * Registers using the device by the
     * <code>HapticViewer3D</code> connected with this
     * <code>ViewerDeviceManager</code>.
     * Here stays the logic of registering using the device in {@link DeviceManager} and assemling
     * the whole hierarchy of connections between a device class, controllers of the device, Java 3D
     * pointer etc.
     * <p/>
     * This method calls appropriate methods in
     * {@link DeviceManager}, {@link InputDevicePointerController}, {@link HapticViewer3D}, {@link Pointer3DViewBehavior}
     * and other classes to achieve this goal.
     * <p/>
     * @param deviceName name of the device to be used
     */
    void registerDevice(String deviceName)
    {

        /**
         * whether usage of the device was successfully registered
         */
        boolean deviceOwned = false;

        try {
            /* get the device from DeviceManager */
            IPassiveDevice device = DeviceManager.registerUsage(deviceName,
                                                                viewer,
                                                                viewer);
            HapticPointerController hapticPointerController = null;
            if (device.getDeviceType() == DeviceType.HAPTIC_DEVICE) {
                try {
                    //               /*  create panel for haptic settings */

                    IHapticDevice hapticDevice = (IHapticDevice) device;
                    final InputFields fields = viewer.getFields();

                    hapticPointerController
                        = new HapticPointerController(hapticDevice,
                                                      fields,
                                                      viewer);
                    if (!hapticPointerController.initialize()) {
                        throw new HapticException("Could not initialize the device");
                    }

                    Pointer3DViewBehavior pointer3D = new Pointer3DViewBehavior(
                        viewer.getView(),
                        hapticPointerController.getSensor(),
                        0.01);
                    hapticPointerController.setPointer(pointer3D);

                    deviceOwned = true;
                } catch (HapticException ex) {
                    JOptionPane.showMessageDialog( // TODO in a distant future: could use DeviceWarningsHandler here
                        viewer.getPanel(),
                        "An error occured while initializing haptic device " +
                        "'" + deviceName + "'" + "\n\n" +
                        "Error message: \n" +
                        ex.toString(),
                        "Error in haptic device",
                        JOptionPane.ERROR_MESSAGE);
                    //                        throw new GUI.InputDevicePointerControllerError();
                    //TODO: here should be placed uninitialization depending on the current progress of registration.
                }
            } else if (device instanceof ITabletDevice) {
                // TODO TABLET: here goes initialization for a tablet device
            } else {
                JOptionPane.showMessageDialog( // TODO in a distant future: could use DeviceWarningsHandler here
                    viewer.getPanel(),
                    "An unknown type of device was selected!");
            }

            if (deviceOwned) {
                if (hapticPointerController == null)
                    throw new IllegalArgumentException("hapticPointerController should have been initialized");

                // initialize a 3D pointer in HapticViewer3D
                this.addPointerController(hapticPointerController);

                controllers.put(deviceName, hapticPointerController);
                devices.put(deviceName, device);
                // pass the SimplePointerController to guiPanel, 
                // which will handle creating a proper device tab and will add it to itself.
                viewer.createDeviceTab(hapticPointerController);
                //TODO in a distant future: this if should be outside this try-catch and embraces by 
                //another try-catch. 
                //When anything could fail?? Maybe only in createDeviceTab or never at all?
                //On catch usage of the device should be unregistered.
            }
        } catch (IPassiveDevice.DeviceRegisterException ex) {
            JOptionPane.showMessageDialog( // TODO in a distant future: could use DeviceWarningsHandler here
                viewer.getPanel(),
                "An error occured while trying to attach the device " +
                "'" + deviceName + "'" + "\n\n" +
                "Error message: \n" +
                ex.toString(),
                "Error on attaching a device",
                JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Unregisters using the device by the
     * <code>HapticViewer3D</code> connected with this
     * <code>ViewerDeviceManager</code>.
     * <p/>
     * Here stays the logic of dismantling the whole hierarchy of connections between a device
     * class, controllers of the device, Java 3D pointer etc.
     * This method calls appropriate methods in
     * {@link DeviceManager}, {@link InputDevicePointerController}, {@link HapticViewer3D} and other
     * to achieve this goal.
     * <p/>
     * @param deviceName name of the device to be stopped begin used
     */
    void unregisterDevice(String deviceName)
    {
        IPassiveDevice device = devices.get(deviceName);

        /* check that such a device exists and its usage can be unregistered in general 
         * and can be unregistered by this object
         */
        if (device != null && device.isAttachable() && device.isOwnedByMe(viewer) && device.isUsed()) {
            try {

                // unregister usage of the device
                // check that we can indeed unregister this device
                device.assertCanUnregisterUsage(viewer);

                // remove device from Java3D - this will call close() in InputPointerController
                InputDevicePointerController pointerController = controllers.get(deviceName);
                this.removePointerController(pointerController);
                controllers.remove(deviceName);

                // now mark the device as unused in DeviceManager
                DeviceManager.unregisterUsage(device, viewer);
                devices.remove(deviceName);

                // uninitialize tab in GUI
                viewer.removeDeviceTab(pointerController);

            } catch (IPassiveDevice.DeviceRegisterException ex) {
                JOptionPane.showMessageDialog( // TODO in a distant future: could use DeviceWarningsHandler here
                    viewer.getPanel(),
                    "An error occured while trying to detach the device " +
                    "'" + deviceName + "'" + "\n\n" +
                    "Error message: \n" +
                    ex.toString(),
                    "Error on detaching a device",
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * <p>
     * Adds a pointer to a HapticViewer3D (to a Java3D scene).</p>
     *
     * <p>
     * NOTE: There can be <b>ONLY ONE</b> pointer in the same time!! It's a Java3D
     * limitation!! This can be overcome by editing Java3D source for setting
     * ViewPlatformBehavior.</p>
     *
     * @see <a
     * href="http://stackoverflow.com/questions/9857548/add-multiple-behavior-to-viewingplatform-java3d">A
     * post on StackOverflow</a>
     * @see <a
     * href="http://download.java.net/media/java3d/javadoc/1.3.2/com/sun/j3d/utils/universe/ViewingPlatform.html#setViewPlatformBehavior(org.jogamp.java3d.utils.behaviors.vp.ViewPlatformBehavior)">setViewPlatformBehavior
     * in Java documentation</a>
     * @param pointerController a controller with a pointer to be added
     */
    public void addPointerController(InputDevicePointerController pointerController)
    {
        // TODO MEDIUM: when adding a pointer when there is already registered one, the previous one should be unregistered! (needed when having multiple devices attached)

        Pointer3DViewBehavior pointer3D = pointerController.getPointer();
        currentPointer3D = pointer3D;

        physEnv.addInputDevice(pointerController);

        SimpleUniverse u = window.getDisplayPanel().getUniverse();

        // The commented line below assumes using custom Java3D! There must be changed visibility 
        // of the method addViewPlatformBehavior from package to public.
        // This enable us to have more than one 3D cursor at the time.
        // It may have some side effects, but noone knows that yet...
        //        u.getViewingPlatform().addViewPlatformBehavior(pointer3D); 
        // Enables having only one pointer cursor at the time
        u.getViewingPlatform().setViewPlatformBehavior(currentPointer3D);

        // Add key listener. It could be null, but it's not a problem - it will do nothing.
        window.getDisplayPanel().getCanvas().addKeyListener(currentPointer3D.getKeyListener());

        Bounds bounds = window.getDisplayPanel().getJ3DBounds();
        currentPointer3D.setSchedulingBounds(bounds);
        currentPointer3D.initializeTargetTG(window.getDisplayPanel().getRotateObject());

        // notify listeneres about pointer change
        pointerSupport.firePointer3DChanged(currentPointer3D);

    }

    /**
     * <p>
     * Removes the pointer from a HapticViewer3D (from a Java3D scene). Here should be only only
     * calls specific to Java3D and generic calls to InputDevicePointerController - NO CALLS
     * depending on type of a device. Such calls please place in {@link #unregisterDevice}.
     * <p/>
     * @param pointerController a controller with a pointer to be added
     */
    public void removePointerController(InputDevicePointerController pointerController)
    {

        Pointer3DViewBehavior pointer3D = pointerController.getPointer();
        if (pointer3D != currentPointer3D) {
            JOptionPane.showMessageDialog(
                viewer.getPanel(),
                "Its weird, but the module tried to unregister a pointer that wasn't " +
                "registered in this HapticViewer3D.\n\n",
                "Error on removing pointer controller",
                JOptionPane.ERROR_MESSAGE);
            //TODO LOW: maybe here should be an exception?
        } else {

            SimpleUniverse u = window.getDisplayPanel().getUniverse();

            // The commented line below assumes using custom Java3D! There must be changed visibility 
            // of the method addViewPlatformBehavior from package to public.
            // This enable us to have more than one 3D cursor at the time.
            // It may have some side effects, but noone knows that yet...
            //        u.getViewingPlatform().removeViewPlatformBehavior(pointerController.getPointer());  
            // Remove key listener. This prevents silent memory leaks. It could be null, but it's not a problem - in such case the method will do nothing.
            window.getDisplayPanel().getCanvas().removeKeyListener(currentPointer3D.getKeyListener());

            // Remove pointer from Java3D scene.
            // As stated above, so far universe in Java3D allows to have only one pointer cursor at the time, so we set it here to null.
            u.getViewingPlatform().setViewPlatformBehavior(null);

            physEnv.removeInputDevice(pointerController);
            pointerController.close(); // this must be called after PhysicalEnvironment.removeInputDevice(InputDevice)

            currentPointer3D = null;

            // notify of a change in 3D pointer
            pointerSupport.firePointer3DChanged(currentPointer3D);
        }
    }

    /**
     * To be used ONLY by HapticViewer3D.onDelete(). Calls {@link #unregisterAllDevices()}.
     */
    void onDelete()
    {
        unregisterAllDevices();
    }

    /**
     * To be used ONLY by onDelete(). Unregisteres all devices used by this HapticViewer3D.
     */
    protected void unregisterAllDevices()
    {
        for (String deviceName : devices.keySet()) {
            unregisterDevice(deviceName);
        }
    }

    public interface IDeviceListener
    {

        /**
         * Fired once - after a listener was registered.
         */
        public void devicesInitialList(List<DeviceName> devicesNames);

        /**
         * Fired after a new device was added.
         */
        public void deviceAdded(DeviceName deviceName);

        /**
         * Fired after a device was removed.
         */
        public void deviceRemoved(DeviceName deviceName);

        /**
         * Fired when a state of a device has changed (it started being used or stopped being
         * used).
         */
        public void deviceChangedState(DeviceName deviceName);
    }

    //
    //
    /**
     * An object responsible for listening to DeviceManager and passing events to GUI.
     */
    ViewerDeviceManagerListenerSupport support;

    public void addRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener l)
    {
        support.addRegisteredDevicesListener(l);
    }

    public void removeRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener l)
    {
        support.removeRegisteredDevicesListener(l);
    }

    //
    //
    /**
     * An object responsible for notifying listeners of change of active device pointer
     */
    ViewerDeviceManagerPointerListenerSupport pointerSupport = new ViewerDeviceManagerPointerListenerSupport();

    public void addPointerListener(PointerChangeListener l)
    {
        pointerSupport.addListener(l);
    }

    public void removePointerListener(PointerChangeListener l)
    {
        pointerSupport.removeListener(l);
    }
}
