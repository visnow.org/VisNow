/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointsWizard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Vector;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class PointsWizardOld
{

    private String[] pointDescriptions = null;
    private int currentIndex = -1;

    public PointsWizardOld(String filePath)
    {
        Vector<String> lines = new Vector<String>();

        try {
            File f = new File(filePath);
            BufferedReader input = new BufferedReader(new FileReader(f));

            String line = input.readLine();
            while (line != null) {
                lines.add(new String(line));
                line = input.readLine();
            }
            input.close();

            pointDescriptions = new String[lines.size()];
            for (int i = 0; i < lines.size(); i++) {
                pointDescriptions[i] = lines.get(i);
            }
        } catch (Exception ex) {
            pointDescriptions = null;
            return;
        }
    }

    public boolean start()
    {
        currentIndex = 0;
        if (pointDescriptions == null || currentIndex > pointDescriptions.length - 1) {
            currentIndex = -1;
            return false;
        }
        return true;
    }

    public boolean next()
    {
        currentIndex++;
        if (pointDescriptions == null || currentIndex > pointDescriptions.length - 1) {
            currentIndex = -1;
            return false;
        }
        return true;
    }

    /**
     * @return the pointDescriptions
     */
    public String[] getPointDescriptions()
    {
        return pointDescriptions;
    }

    public String getPointDescription(int i)
    {
        if (i >= 0 && i < pointDescriptions.length)
            return pointDescriptions[i];
        else
            return "";
    }

    /**
     * @return the currentIndex
     */
    public int getCurrentIndex()
    {
        return currentIndex;
    }

    public void back()
    {
        if (currentIndex > 0)
            currentIndex--;
    }

}
