/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.KernelEditor;

import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.utilities.KernelEditor.KernelEditorShared.*;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Michal Lyczek (lyczek@icm.edu.pl)
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class KernelEditor extends ModuleCore
{
    private static final Logger LOGGER = Logger.getLogger(KernelEditor.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    private Core core = new Core();

    public KernelEditor()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                setPanel(computeUI);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(KERNEL_TYPE, KernelType.CONSTANT),
            new Parameter<>(RANK, 2),
            new Parameter<>(RADIUS, 1),
            new Parameter<>(NORMALIZE_KERNEL, false),
            new Parameter<>(GAUSSIAN_SIGMA, 0.5f),
            new Parameter<>(KERNEL, new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f})
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        Parameters p=parameters.getReadOnlyClone();
        

        notifyGUIs(p, isFromVNA(), false);

        core.update(p.get(RADIUS), p.get(RANK), p.get(GAUSSIAN_SIGMA), p.get(NORMALIZE_KERNEL), p.get(KERNEL_TYPE), p.get(KERNEL));

        synchronized (parameters) {
            parameters.setParameterActive(false);
            if (parameters.get(KERNEL_TYPE) != KernelType.CUSTOM)
                parameters.set(KERNEL, core.getOutField().getComponent(0).getRawFloatArray().getData());
            parameters.setParameterActive(true);

            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, isFromVNA(), false);

        setOutputValue("outField", new VNRegularField(core.getOutField()));
    }
}
