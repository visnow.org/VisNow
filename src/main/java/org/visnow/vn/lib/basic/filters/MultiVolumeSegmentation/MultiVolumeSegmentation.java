/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MultiVolumeSegmentation;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class MultiVolumeSegmentation extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI ui = new GUI();
    protected Params params;
    protected RegularField inField = null;
    protected IrregularField inPts = null;
    protected RegularField outField = null;
    protected Segmentation segmentation;
    protected boolean fromGUI = false;

    public MultiVolumeSegmentation()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                if (inField == null || inPts == null)
                    return;
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (!fromGUI) {
            if (getInputFirstValue("inField") == null || getInputFirstValue("startPoints") == null)
                return;
            inField = ((VNRegularField) getInputFirstValue("inField")).getField();
            Field pts = ((VNField) getInputFirstValue("startPoints")).getField();
            if (inField == null || pts == null || !(pts instanceof IrregularField))
                return;
            inPts = (IrregularField) pts;
            return;
        }
        outField = inField.cloneShallow();
        outField.removeComponents();
        segmentation = new Segmentation(params, inField, inPts);
        segmentation.addFloatValueModificationListener(
            new FloatValueModificationListener()
            {
                public void floatValueChanged(FloatValueModificationEvent e)
                {
                    setProgress(e.getVal());
                }
            });
        segmentation.compute();
        outField.addComponent(DataArray.create(segmentation.getAreas(), 1, "areas"));
        outField.addComponent(DataArray.create(segmentation.getBd(), 1, "data"));
        String[] dataMap = new String[inPts.getNCellSets() + 3];
        dataMap[0] = "MAP";
        dataMap[1] = "0:background";
        dataMap[2] = "1:unassigned";
        for (int i = 0; i < inPts.getNCellSets(); i++)
            dataMap[i + 3] = "" + (i + 2) + ":" + (inPts.getCellSet(i).getName() + 2);
        outField.getComponent(0).setUserData(dataMap);
        setOutputValue("outField", new VNRegularField(outField));
    }
}
