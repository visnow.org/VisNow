//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.util.Arrays;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Class holding static methods for converting VisNow components to data series (string representation of a component).
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewerCore
{

    /**
     * Creates data series from a given component.
     *
     * @param da    component
     * @param inFld field
     *
     * @return data series
     */
    public static DataSet createSeries(DataArray da, RegularField inFld)
    {
        if (da == null || inFld == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }

        if (inFld.getDimNum() > 2) {
            throw new IllegalArgumentException("3D fields are not supported.");
        }

        long[] ldims = inFld.getLDims();
        int[] dims = new int[ldims.length];
        for (int i = 0; i < dims.length; i++) {
            if (ldims[i] >= Integer.MAX_VALUE) {
                throw new IllegalArgumentException("Dimensions of this field are too large.");
            } else {
                dims[i] = (int) ldims[i];
            }
        }
        
        LargeArray data;
        double[] stats;
        DataArraySchema schema = da.getSchema();
        if (da.isNumeric() && da.getType() != DataArrayType.FIELD_DATA_COMPLEX && !Arrays.equals(schema.getPhysicalMappingCoefficients(), new double[]{1., 0.})) {
            data = da.getPhysicalDoubleArray();
            stats = new double[]{da.getPhysMinValue(), schema.dataRawToPhys(da.getMeanValue()), da.getPhysMaxValue(), schema.dataRawToPhys(da.getStandardDeviationValue())};
        } else {
            data = da.getRawArray();
            stats = new double[]{da.getMinValue(), da.getMeanValue(), da.getMaxValue(), da.getStandardDeviationValue()};

        }
        return new DataSet(da.getName(), data, dims, da.getVectorLength(), stats);
        
    }

    /**
     * Creates data series from a given component.
     *
     * @param da         component
     * @param inFld      field
     * @param seriesName series name
     *
     * @return data series
     */
    public static DataSet createSeries(DataArray da, RegularField inFld, String seriesName)
    {
        DataArray daClone = da.cloneShallow();
        daClone.setName(seriesName);
        return createSeries(daClone, inFld);
    }

    private TableViewerCore()
    {

    }
}
