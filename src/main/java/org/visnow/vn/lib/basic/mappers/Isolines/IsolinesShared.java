/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isolines;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import org.visnow.vn.lib.gui.ComponentBasedUI.array.ComponentValuesArray;

/**
 *
 * @author know, University of Warsaw, ICM
 */
public class IsolinesShared
{
    static final String THRESHOLDS_STRING           = "Thresholds";
    static final String COMPUTE_UNCERTAINTY_STRING  = "Uncertainty component generation";
    static final String TIME_STRING                 = "Selected time";
    static final String SELECTED_CELL_SETS_STRING   = "Selected cell sets";
    static final String BOUNDARY_ISOLINES_STRING    = "boundary isolines";
    static final String RUNNING_MESSAGE_STRING      = "Running message";
    static final String META_CELL_SET_NAMES_STRING  = "Cell set names";
    static final String META_REGULAR_DIMS_STRING    = "regular infield dims";
    static final String INFIELD_TIME_RANGE_STRING   = "infield time range";


    static final ParameterName<ComponentValuesArray> THRESHOLDS    = new ParameterName<>(THRESHOLDS_STRING);
    static final ParameterName<Boolean> COMPUTE_UNCERTAINTY        = new ParameterName<>(COMPUTE_UNCERTAINTY_STRING);
    static final ParameterName<Float> TIME                         = new ParameterName<>(TIME_STRING);
    static final ParameterName<boolean[]> SELECTED_CELL_SETS       = new ParameterName<>(SELECTED_CELL_SETS_STRING);
    static final ParameterName<Boolean> BOUNDARY_ISOLINES          = new ParameterName<>(BOUNDARY_ISOLINES_STRING);
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>(RUNNING_MESSAGE_STRING);
    static final ParameterName<String[]> META_CELL_SET_NAMES       = new ParameterName<>(META_CELL_SET_NAMES_STRING);
    static final ParameterName<Integer> META_REGULAR_DIMS          = new ParameterName<>(META_REGULAR_DIMS_STRING);
    static final ParameterName<float[]> META_TIME_RANGE            = new ParameterName<>(INFIELD_TIME_RANGE_STRING);

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(THRESHOLDS, new ComponentValuesArray().preferredCount(3).startSingle(false)));
                add(new Parameter<>(COMPUTE_UNCERTAINTY, false));
                add(new Parameter<>(TIME, 0f));
                add(new Parameter<>(SELECTED_CELL_SETS, new boolean[] {}));
                add(new Parameter<>(BOUNDARY_ISOLINES, false));
                add(new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY));
                add(new Parameter<>(META_CELL_SET_NAMES, new String[] {}));
                add(new Parameter<>(META_REGULAR_DIMS, -1));
                add(new Parameter<>(META_TIME_RANGE, new float[]{0, 0}));
            }
        };
    }
}
