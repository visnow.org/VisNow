/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase;

import java.util.Vector;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.FileErrorFrame;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class Reader
{

    protected static final int[][] UCDnodeOrders
        = {
            {0},
            {0, 1},
            {0, 1, 2},
            {0, 1, 2, 3},
            {0, 1, 2, 3},
            {4, 0, 3, 2, 1},
            {0, 1, 2, 3, 4, 5},
            {0, 1, 2, 3, 4, 5, 6, 7}
        };

    protected static String[] enSightCellNames
        = {"point",
           "bar",
           "tria", "quad",
           "tetra", "pyramid", "penta", "hexa"};

    protected static String[] enSightPartSectionNames
        = {
            "point",
            "bar",
            "tria", "quad",
            "tetra", "pyramid", "penta", "hexa",
            "part"};

    protected static String[] enSightSectionNames
        = {"coordinates",
           "point",
           "bar",
           "tria", "quad",
           "tetra", "pyramid", "penta", "hexa",
           "part"};

    protected Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];

    protected enum nodeId
    {

        NONE, IGNORE, USE
    };

    protected nodeId useNodeId = nodeId.NONE;
    protected static final int NODES = 0;
    protected static final int POINTS = 1;
    protected static final int SEGMENTS = 2;
    protected static final int TRIANGLES = 3;
    protected static final int QUADS = 4;
    protected static final int TETRAS = 5;
    protected static final int PYRAMIDS = 6;
    protected static final int PRISMS = 7;
    protected static final int HEXAHEDRAS = 8;
    protected Vector<int[]> partCounts = new Vector<int[]>();
    protected Vector<String> partNames = new Vector<String>();

    Reader()
    {
        for (int i = 0; i < stdCells.length; i++)
            stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);
    }

    public Vector<int[]> getPartCounts()
    {
        return partCounts;
    }

    public void setPartCounts(Vector<int[]> partCounts)
    {
        this.partCounts = partCounts;
    }

    public Vector<String> getPartNames()
    {
        return partNames;
    }

    public void setPartNames(Vector<String> partNames)
    {
        this.partNames = partNames;
    }

    abstract public IrregularField readEnSightGoldGeometry(Parameters params, String filename);

    abstract public DataArray readEnSightGoldVariable(Parameters params, int nData, int veclen, String name, String filename);

}
