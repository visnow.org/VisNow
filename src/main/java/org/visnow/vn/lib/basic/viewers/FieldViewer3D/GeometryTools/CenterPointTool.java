/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParameter;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CenterPointTool extends GeometryTool
{

    private final int pointRadius = 3;

    private Point startPoint = null;
    private Point currentPoint = null;
    private Point endPoint = null;

    @Override
    public void paint(Graphics g)
    {
        if (holding && startPoint != null && currentPoint != null) {
            g.setColor(Color.RED);
            int[][] pp = new int[2][2];
            pp[0][0] = startPoint.x;
            pp[0][1] = startPoint.y;
            pp[1][0] = currentPoint.x;
            pp[1][1] = currentPoint.y;

            g.drawLine(pp[0][0], pp[0][1], pp[1][0], pp[1][1]);
            int[] c = new int[2];
            c[0] = (startPoint.x + currentPoint.x) / 2;
            c[1] = (startPoint.y + currentPoint.y) / 2;
            g.drawLine(c[0] - pointRadius, c[1], c[0] + pointRadius, c[1]);
            g.drawLine(c[0], c[1] - pointRadius, c[0], c[1] + pointRadius);
            float d = (float) (sqrt((pp[1][0] - pp[0][0]) * (pp[1][0] - pp[0][0]) + (pp[1][1] - pp[0][1]) * (pp[1][1] - pp[0][1])));
            int cx = (int) round((float) c[0] - d / 2.0f);
            int cy = (int) round((float) c[1] - d / 2.0f);
            int cd = (int) round(d);
            g.drawOval(cx, cy, cd, cd);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
        if (holding) {
            currentPoint = e.getPoint();
            fireGeometryToolRepaintNeeded();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {

    }

    @Override
    public void mouseClicked(MouseEvent e)
    {

    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        holding = true;
        this.startPoint = e.getPoint();
        this.currentPoint = e.getPoint();
        fireGeometryToolRepaintNeeded();
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        holding = false;
        this.endPoint = e.getPoint();
        fireGeometryToolRepaintNeeded();
        metadata = null;
        fireGeometryToolStateChanged();
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {

    }

    @Override
    public void mouseExited(MouseEvent e)
    {

    }

    @Override
    public Cursor getCursor()
    {
        return Cursor.getDefaultCursor();
    }

    @Override
    public boolean isMouseWheelBlocking()
    {
        return holding;
    }

    @Override
    public int[][] getPoints()
    {
        if (startPoint == null || endPoint == null)
            return null;

        int[][] out = new int[1][2];
        out[0][0] = (endPoint.x + startPoint.x) / 2;
        out[0][1] = (endPoint.y + startPoint.y) / 2;
        return out;
    }

    @Override
    public Metadata[] getPointMetadata()
    {
        return null;
    }

    @Override
    public int[][] getConnections()
    {
        return null;
    }

    @Override
    public CalculableParameter getCalculable()
    {
        return null;
    }

    @Override
    public Metadata getCalculableMetadata()
    {
        return null;
    }

    @Override
    public int getMinimumNPoints()
    {
        return 1;
    }

}
