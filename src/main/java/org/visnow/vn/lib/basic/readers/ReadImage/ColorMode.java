/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadImage;

import java.util.HashMap;
import java.util.Map;

/**
 * Color modes used in ReadImage (UI and Params).
 * <p>
 * @author szpak
 */
public enum ColorMode
{
    ORIGINAL("Original"),
    BT709("Grayscale BT.709"),
    YIQ("Grayscale YIQ"),
    AVERAGE("Average grayscale"),
    CUSTOM("Custom grayscale");

    final String text;

    private ColorMode(String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return text;
    }

    private static Map<Enum, float[]> desaturateWeights = new HashMap<Enum, float[]>()
    {
        {
            put(ColorMode.BT709, new float[]{0.2126f, 0.7152f, 0.0722f});
            put(ColorMode.YIQ, new float[]{0.299f, 0.587f, 0.114f});
            put(ColorMode.AVERAGE, new float[]{1 / 3f, 1 / 3f, 1 / 3f});
        }
    };

    /**
     * Returns cloned weights for passed {@code colorMode}.
     */
    static float[] getWeights(ColorMode colorMode)
    {
        float[] weights = desaturateWeights.get(colorMode);
        if (weights != null) return weights.clone();
        else throw new IllegalArgumentException("Weight for " + colorMode.name() + " not found");
    }
}
