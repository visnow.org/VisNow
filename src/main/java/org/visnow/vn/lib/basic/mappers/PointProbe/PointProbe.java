/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.PointProbe;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.utils.CellSetGeometryUtilities;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.ResizeEvent;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.objects.Geometry2D;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickListener;
import static org.visnow.vn.lib.basic.mappers.PointProbe.PointProbeShared.*;
import static org.visnow.vn.lib.basic.mappers.PointProbe.PointProbeShared.ProbeType.*;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import org.visnow.vn.lib.utils.graphing.GraphParams;
import org.visnow.vn.lib.utils.pointProbe.GeometricPointProbe;
import org.visnow.vn.lib.utils.pointProbe.IndexPointProbe;
import org.visnow.vn.lib.utils.pointProbe.MultiLabels;
import org.visnow.vn.lib.utils.pointProbe.PointValuesDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointProbe extends OutFieldVisualizationModule
{

    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    
    protected IrregularField probeField = null;
    
    protected GUI computeUI = null;
    
    protected boolean newGraphedFieldSet = false;

    protected Probe currentProbe = null;
    protected GeometricPointProbe geometricProbe;
    protected IndexPointProbe indexProbe;
    
    protected OpenBranchGroup indexProbeGlyph;
    protected IndexSliceParams indexProbeParams;
    
    protected OpenBranchGroup currentProbeGlyph;
    
    protected int windowWidth  = 500;
    protected int windowHeight = 500;
    protected boolean fromParams = false;
    protected boolean probeReady = false;
    protected int maxProbesOnMargin = 6;
    
    protected MultiLabels labels = new MultiLabels();
    protected GraphParams graphParams = labels.getGraphParams();
    protected PointValuesDisplay pickedLabel;
    
    
    Geometry2D labelsGeometry = new Geometry2D()
        {
            @Override
            public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
            {
                labels.draw2D(vGraphics, ltw, w, h);
            }
        };
    
    
    protected ResizeListener resizeListener = new ResizeListener() {
        @Override
        public void resized(ResizeEvent e)
        {
            updateAddCapability();
        }
    };
    
    PickListener graphPickingListener = new PickListener()
    {
        @Override
        public void pickChanged(PickEvent e)
        {
            for (ProbeDisplay display : labels.getDisplays()) 
                display.setSelected(false);
            PointValuesDisplay pickedLabel = labels.pickedDisplay(e.getEvt().getX(), e.getEvt().getY());
            if (pickedLabel != null) {
                pickedLabel.setSelected(true);
                computeUI.enableRemoveGraph(true);
                    refreshDisplay();
            }
        }
    };
    
    private boolean canAddProbe = parameters.get(LABEL_POSITION) == AT_POINT ||
                                  labels.getDisplays().size() < maxProbesOnMargin;
    
    ChangeListener probeListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            probeField = currentProbe.getSliceField();
            probeReady = true;
//            startAction();
            updateAddCapability();
        }
    };
    
    private final void updateAddCapability()
    {
        Position probesPosition = parameters.get(LABEL_POSITION);
//        int nGraphs = labels.getDisplays().size();
//        int[] maxGraphs = labels.maxGraphCount();
//        canAddProbe = 
//            (probesPosition != TOP && probesPosition != BOTTOM || nGraphs < maxGraphs[0]) &&
//            (probesPosition != LEFT && probesPosition != RIGHT || nGraphs < maxGraphs[1]);
//        computeUI.enableAddGraph(probeReady && canAddProbe);
    }
    
    public PointProbe()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) 
            {
                if (graphParams.isAutoBgr())
                    graphParams.setUserBgrColor(e.getSelectedColor());
            }
        };
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name == null || inField == null)
                    return;
                fromParams = true;
                switch (name) {
                case ADD_PROBE_STRING:
                    probeField = currentProbe.getSliceField();
                    if (probeField == null)
                        return;
                    for (ProbeDisplay display : labels.getDisplays()) 
                        display.setSelected(false);
                    labels.addDisplay(probeField, currentProbe.getPlaneCenter());
                    pickedLabel = (PointValuesDisplay)labels.getDisplays().get(labels.getDisplays().size() - 1);
                    pickedLabel.setSelected(true);
                    computeUI.enableRemoveGraph(true);
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    probeReady = false;
                    updateAddCapability();
                    startAction();
                    break;
                case DEL_PROBE_STRING:
                    labels.removeDisplay(pickedLabel);
                    updateAddCapability();
                    pickedLabel = null;
                    startAction();
                    break;
                case CLEAR_PROBES_STRING:
                    outField = null;
                    labels.clearDisplays();
                    updateAddCapability();
                    pickedLabel = null;
                    startAction();
                    break;   
                case LABEL_POSITION_STRING:
                case INIT_MARGIN_STRING:
                case FONT_SIZE_STRING:
                    labels.setProbesPosition(parameters.get(LABEL_POSITION));
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    labels.setRelMargin(parameters.get(INIT_MARGIN) / (float)200);
                    refreshDisplay();
                    break;
                case POINTER_LINE_STRING:
                    labels.setPointerLine(parameters.get(POINTER_LINE));
                    refreshDisplay();
                    break;
                case PROBE_TYPE_STRING:
                    if (geometricProbe != null && parameters.get(PROBE_TYPE) == GEOMETRIC) 
                        currentProbe = geometricProbe;
                    else 
                        currentProbe = indexProbe;
                    currentProbeGlyph = currentProbe.getGlyphGeometry();
                    doOutput();
                    break;
                default:
                }
                computeUI.enableRemoveGraph(pickedLabel != null);
            }
        });
        
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters, graphParams);
                ui.getPresentationGUI().hideTransformPanel();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        
        outObj.addPickListener(graphPickingListener);
    }   

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ADD_PROBE, false),
            new Parameter<>(DEL_PROBE, false),
            new Parameter<>(CLEAR_PROBES, false),
            new Parameter<>(LABEL_POSITION, RIGHT),
            new Parameter<>(POINTER_LINE, true),
            new Parameter<>(INIT_MARGIN, 5),
            new Parameter<>(PROBE_TYPE, GEOMETRIC),
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(ADD_PROBE, false);
            parameters.set(DEL_PROBE, false);
            parameters.set(CLEAR_PROBES, false);
            parameters.set(LABEL_POSITION, RIGHT);
            parameters.set(POINTER_LINE, true);
            parameters.set(INIT_MARGIN, 5);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    protected void refreshDisplay()
    {
        if (outObj.getRenderingWindow() != null)
            outObj.getRenderingWindow().refresh();
    }
    
    protected void doOutput()
    {
        if (labels.getDisplays().isEmpty())
            setOutputValue("accumulatedOutField", null);
        else {
            ArrayList<ProbeDisplay> shownGraphs = labels.getDisplays();
            IrregularField outIrregularField  = null;
            for (int i = 0; i < shownGraphs.size(); i++)
                outIrregularField = MergeIrregularField.merge(outIrregularField, 
                                                      shownGraphs.get(i).getField(), i, false, true);
            outIrregularField.updatePreferredExtents();
            outField = outIrregularField;
            setOutputValue("accumulatedOutField", new VNIrregularField((IrregularField)outField));
        }
        
        prepareOutputGeometry();
        presentationParams.getRenderingParams().setDisplayMode(RenderingParams.NODES);
        show();
        if (currentProbeGlyph.getParent() == null)
            outObj.addNode(currentProbeGlyph);
        outObj.addGeometry2D(labelsGeometry);
    }

    @Override
    public void onActive()
    {
        if (fromParams) 
            fromParams = false;
        else {
            if (getInputFirstValue("inField") == null) {
                outObj.clearAllGeometry();
                outObj.clearGeometries2D();
                return;
            }
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;
            validateParamsAndSetSmart(isDifferentField && !isFromVNA());
            notifyGUIs(parameters, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (isNewField) {
                labels.clearDisplays();
                probeField = null;
                outField = null;
                setOutputValue("accumulatedOutField", null);
                if (geometricProbe != null)
                    geometricProbe.clearChangeListeners();
                geometricProbe = null;
                if (indexProbe != null)
                    indexProbe.clearChangeListeners();
                indexProbe = null;
                if (inField instanceof IrregularField) {
                    CellSetGeometryUtilities.addGeometryDataToCellSets(((IrregularField) inField).getCellSets(), inField.getCoords(0));
                }
                validateParamsAndSetSmart(true);
                
                if (inField instanceof RegularField) {
                    indexProbe = new IndexPointProbe();
                    indexProbe.setInData(inField, dataMappingParams);
                    indexProbe.addChangeListener(probeListener);
                    indexProbeGlyph = indexProbe.getGlyphGeometry();
                    currentProbe = indexProbe;
                }
                if (inField.getTrueNSpace() >= 2) {
                    geometricProbe = new GeometricPointProbe();
                    geometricProbe.setInData(inField, dataMappingParams);
                    geometricProbe.addChangeListener(probeListener);
                    currentProbe = geometricProbe;
                }
                currentProbeGlyph = currentProbe.getGlyphGeometry();
                probeField = currentProbe.getSliceField();
                computeUI.setProbePanels(geometricProbe != null ? geometricProbe.getGlyphGUI() : null,
                                         indexProbe != null ?     indexProbe.getGlyphGUI() :     null);
                computeUI.getComponentDisplayController().setInField(inField);
                outField = null;
                labels.clearDisplays();
                newGraphedFieldSet = false;
                probeReady = canAddProbe = true;
                computeUI.enableAddGraph(probeReady && canAddProbe);
            }
        }
        doOutput();
    }
}
