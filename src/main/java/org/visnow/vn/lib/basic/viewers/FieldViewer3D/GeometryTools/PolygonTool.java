/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Vector;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParameter;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class PolygonTool extends GeometryTool
{

    private final int pointRadius = 2;

    private Vector<Point> points = new Vector<Point>();
    ;
    private Vector<Metadata> pointMetadata = new Vector<Metadata>();
    private Point currentPoint = null;

    private boolean firstClick = true;

    @Override
    public void paint(Graphics g)
    {
        if (holding && points.size() > 0) {
            g.setColor(Color.RED);
            for (int i = 0; i < points.size() - 1; i++) {
                g.fillRect(points.get(i).x - pointRadius, points.get(i).y - pointRadius, 2 * pointRadius + 1, 2 * pointRadius + 1);
                g.drawLine(points.get(i).x, points.get(i).y, points.get(i + 1).x, points.get(i + 1).y);
            }
            g.fillRect(points.get(points.size() - 1).x - pointRadius, points.get(points.size() - 1).y - pointRadius, 2 * pointRadius + 1, 2 * pointRadius + 1);
            g.drawLine(points.get(points.size() - 1).x, points.get(points.size() - 1).y, currentPoint.x, currentPoint.y);
            g.drawLine(currentPoint.x, currentPoint.y, points.get(0).x, points.get(0).y);
        }
    }

    public void mouseDragged(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
        if (holding) {
            currentPoint = e.getPoint();
            fireGeometryToolRepaintNeeded();
        }
    }

    public void mouseClicked(MouseEvent e)
    {
        if (firstClick) {
            points.clear();
            pointMetadata.clear();
            firstClick = false;
        }

        if (e.getClickCount() > 1) {
            //double click - end polyline
            currentPoint = null;
            holding = false;
            firstClick = true;
            metadata = null; //TODO dodac pole na metadane
            fireGeometryToolStateChanged();
        } else {
            //single click - next point
            points.add(e.getPoint());
            if (metadata != null)
                pointMetadata.add(metadata.clone());

            currentPoint = e.getPoint();
            holding = true;
        }
        fireGeometryToolRepaintNeeded();
    }

    public void mousePressed(MouseEvent e)
    {
    }

    public void mouseReleased(MouseEvent e)
    {
    }

    public void mouseEntered(MouseEvent e)
    {
    }

    public void mouseExited(MouseEvent e)
    {
    }

    @Override
    public Cursor getCursor()
    {
        return Cursor.getDefaultCursor();
    }

    @Override
    public boolean isMouseWheelBlocking()
    {
        return false;
    }

    @Override
    public int[][] getPoints()
    {
        if (points.isEmpty())
            return null;

        int[][] out = new int[points.size()][2];
        for (int i = 0; i < points.size(); i++) {
            out[i][0] = points.get(i).x;
            out[i][1] = points.get(i).y;
        }
        return out;
    }

    @Override
    public Metadata[] getPointMetadata()
    {
        if (points.isEmpty() || pointMetadata.isEmpty())
            return null;

        Metadata[] out = new Metadata[pointMetadata.size()];
        for (int i = 0; i < pointMetadata.size(); i++) {
            out[i] = pointMetadata.get(i);
        }
        return out;
    }

    @Override
    public int[][] getConnections()
    {
        if (points.size() < 2)
            return null;

        int[][] out = new int[points.size()][2];
        for (int i = 0; i < points.size() - 1; i++) {
            out[i][0] = i;
            out[i][1] = i + 1;
        }
        out[points.size() - 1][0] = points.size() - 1;
        out[points.size() - 1][1] = 0;
        return out;
    }

    @Override
    public CalculableParameter getCalculable()
    {
        return null;
    }

    @Override
    public Metadata getCalculableMetadata()
    {
        return null;
    }

    @Override
    public int getMinimumNPoints()
    {
        return 2;
    }

}
