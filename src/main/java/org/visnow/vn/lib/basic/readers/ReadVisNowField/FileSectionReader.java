/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.LineNumberReader;
import java.util.Scanner;
import javax.imageio.stream.ImageInputStream;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.TileUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jscic.dataarrays.StringDataArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.BooleanArrayIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataElementIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataFileSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileType;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.IntArrayIOSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FileSectionReader
{

    protected Field outField = null;
    protected RegularField regularField = null;
    protected FileSectionSchema sectionSchema = null;
    protected FileType fileType;
    protected float time = 0;
    protected LineNumberReader reader = null;
    protected ImageInputStream inStream = null;
    protected Scanner scanner = null;
    protected SectionModel model = null;
    protected int nComps;
    protected long nData;
    protected int[][] tileBds;
    protected String filePath;
    protected boolean streamRead = false;
    protected boolean continuingRead = false;
    protected String decimalSeparator = null;
    protected String entrySeparator = null;

    public FileSectionReader(Field outField, FieldIOSchema schema,
                             LineNumberReader reader, ImageInputStream inStream, Scanner scanner, FileType fileType, String filePath)
    {
        this.outField = outField;
        if (outField instanceof RegularField)
            regularField = (RegularField) outField;
        this.reader = reader;
        this.inStream = inStream;
        this.scanner = scanner;
        this.fileType = fileType;
        this.filePath = filePath;
    }

    public void setSectionSchema(FileSectionSchema sectionSchema, float time, String decimalSeparator, String entrySeparator) throws VNIOException
    {
        this.sectionSchema = sectionSchema;
        model = new SectionModel(sectionSchema, outField, time, fileType);
        tileBds = sectionSchema.getTile();
        this.time = time;
        if (decimalSeparator != null) {
            String tmp = decimalSeparator.replaceAll("\"", "");
            if (tmp.equals("."))
                this.decimalSeparator = null;
            else
                this.decimalSeparator = tmp;
        }
        this.entrySeparator = entrySeparator;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public void setContinuingRead(boolean continuingRead)
    {
        this.continuingRead = continuingRead;
    }

    public void setTime(float time)
    {
        this.time = time;
    }

    public void setStreamRead(boolean streamRead)
    {
        this.streamRead = streamRead;
    }

    public int readSection()
            throws VNIOException
    {
        switch (fileType) {
            case BINARY_BIG_ENDIAN:
            case BINARY_LITTLE_ENDIAN:
                if (ReadBinarySectionData.readSectionData(model, inStream, filePath) > 0)
                    return 2;
                break;
            case ASCII_CONTINUOUS:
                if (ReadASCIIContinuousSectionData.readSectionData(model, scanner, filePath, decimalSeparator, entrySeparator) > 0)
                    return 2;
                break;
            case ASCII_COLUMN:
                if (ReadASCIIColumnSectionData.readSectionData(model, reader, filePath, decimalSeparator, entrySeparator) > 0)
                    return 2;
                break;
            case ASCII_FIXED_COLUMN:
                if (ReadASCIIFixedColumnSectionData.readSectionData(model, reader, filePath, decimalSeparator) > 0)
                    return 2;
        }
        if (sectionSchema.getComponents().isEmpty())
            return 0;

        if (tileBds != null && regularField != null) {
            for (int iComp = 0; iComp < model.nItems; iComp++) {
                int cmp = model.comps[iComp];
                int coord = model.coords[iComp];
                if (cmp < regularField.getNComponents()) {
                    DataArray ar = regularField.getComponent(cmp);
                    switch (ar.getType()) {
                        case FIELD_DATA_LOGIC:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.boolArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_BYTE:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.byteArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_SHORT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.shortArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_INT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.intArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_LONG:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.longArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_FLOAT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.floatArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_DOUBLE:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.dblArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_COMPLEX:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.cplxArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_STRING:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), model.strArrs[iComp],
                                         ar.getVectorLength(), coord);
                        default:
                            break;
                    }
                } else if (cmp == regularField.getNComponents()) {
                    TileUtils.putTile(tileBds, regularField.getDims(),
                                 regularField.produceCoords(time), model.floatArrs[iComp],
                                 3, coord);
                } else if (cmp == regularField.getNComponents() + 1) {
                    TileUtils.putTile(tileBds, regularField.getDims(),
                                 regularField.produceMask(time), model.byteArrs[iComp],
                                 1, coord);
                }
            }
        } else {
            for (int iComp = 0; iComp < model.nItems; iComp++) {
                DataElementIOSchema sch = model.schemas[iComp];
                DataContainer container = sch.getDataset();
                int vlen = sch.getVectorLength();
                int cmp = model.comps[iComp];
                int coord = model.coords[iComp];
                if (coord >= 0) {
                    if (sch instanceof IntArrayIOSchema) {
                        IntLargeArray iTarget = ((IntArrayIOSchema) sch).getIntArray();
                        IntLargeArray iSource = model.intArrs[iComp];
                        if (iSource != iTarget)
                            for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                iTarget.setInt(j, iSource.getInt(i));
                    } else if (sch instanceof BooleanArrayIOSchema) {
                        LogicLargeArray blSource = model.boolArrs[iComp];
                        LogicLargeArray blTarget = ((BooleanArrayIOSchema) sch).getBoolArray();
                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                            blTarget.setByte(j, blSource.getByte(i));
                    } else if (cmp < container.getNComponents()) {
                        DataArray ar = container.getComponent(cmp);
                        switch (ar.getType()) {
                            case FIELD_DATA_LOGIC:
                                LogicLargeArray blTarget = (LogicLargeArray) (ar.produceData(time));
                                LogicLargeArray blSource = model.boolArrs[iComp];
                                for (int i = 0, j = coord; i < nData; i++, j += vlen)
                                    blTarget.setBoolean(j, blSource.getBoolean(i));
                                break;
                            case FIELD_DATA_BYTE:
                                UnsignedByteLargeArray bTarget = ((UnsignedByteLargeArray) ar.produceData(time));
                                UnsignedByteLargeArray bSource = model.byteArrs[iComp];
                                if (bSource != bTarget) {
                                    if (bTarget.length() == bSource.length())
                                        LargeArrayUtils.arraycopy(bSource, 0, bTarget, 0, bSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            bTarget.setByte(j, bSource.getByte(i));
                                }
                                break;
                            case FIELD_DATA_SHORT:
                                ShortLargeArray sTarget = ((ShortLargeArray) ar.produceData(time));
                                ShortLargeArray sSource = model.shortArrs[iComp];
                                if (sSource != sTarget) {
                                    if (sTarget.length() == sSource.length())
                                        LargeArrayUtils.arraycopy(sSource, 0, sTarget, 0, sSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            sTarget.setShort(j, sSource.getShort(i));
                                }
                                break;
                            case FIELD_DATA_INT:
                                IntLargeArray iTarget = ((IntLargeArray) ar.produceData(time));
                                IntLargeArray iSource = model.intArrs[iComp];
                                if (iSource != iTarget) {
                                    if (iTarget.length() == iSource.length())
                                        LargeArrayUtils.arraycopy(iSource, 0, iTarget, 0, iSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            iTarget.setInt(j, iSource.getInt(i));
                                }
                                break;
                            case FIELD_DATA_LONG:
                                LongLargeArray lTarget = ((LongLargeArray) ar.produceData(time));
                                LongLargeArray lSource = model.longArrs[iComp];
                                if (lSource != lTarget) {
                                    if (lTarget.length() == lSource.length())
                                        LargeArrayUtils.arraycopy(lSource, 0, lTarget, 0, lSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            lTarget.setLong(j, lSource.getLong(i));
                                }
                                break;
                            case FIELD_DATA_FLOAT:
                                FloatLargeArray fTarget = ((FloatLargeArray) ar.produceData(time));
                                FloatLargeArray fSource = model.floatArrs[iComp];
                                if (fSource != fTarget) {
                                    if (fTarget.length() == fSource.length())
                                        LargeArrayUtils.arraycopy(fSource, 0, fTarget, 0, fSource.length());
                                    else
                                    for (long i = coord, j = 0; j < fSource.length(); i += vlen, j++)
                                        fTarget.setFloat(i, fSource.getFloat(j));
                                }
                                break;
                            case FIELD_DATA_DOUBLE:
                                DoubleLargeArray dTarget = ((DoubleLargeArray) ar.produceData(time));
                                DoubleLargeArray dSource = model.dblArrs[iComp];
                                if (dSource != dTarget) {
                                    if (dTarget.length() == dSource.length())
                                        LargeArrayUtils.arraycopy(dSource, 0, dTarget, 0, dSource.length());
                                    else
                                    for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                        dTarget.setDouble(j, dSource.getDouble(i));
                                }
                                break;
                            case FIELD_DATA_COMPLEX:
                                ComplexFloatLargeArray cplTarget = ((ComplexFloatLargeArray) ar.produceData(time));
                                ComplexFloatLargeArray cplSource = model.cplxArrs[iComp];
                                if (cplTarget != cplSource) {
                                    if (cplTarget.length() == cplSource.length())
                                        LargeArrayUtils.arraycopy(cplSource, 0, cplTarget, 0, cplSource.length());
                                    else
                                    for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                        cplTarget.setComplexFloat(j, cplSource.getComplexFloat(i));
                                }
                                break;
                            case FIELD_DATA_STRING:
                                StringLargeArray stTarget = ((StringDataArray) ar).produceData(time);
                                StringLargeArray stSource = model.strArrs[iComp];
                                if (stSource != stTarget) {
                                    if (stTarget.length() == stSource.length())
                                        LargeArrayUtils.arraycopy(stSource, 0, stTarget, 0, stSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            stTarget.set(j, stSource.get(i));
                                }
                                break;
                            default:
                                break;
                        }
                    } else if (cmp == container.getNComponents()) {
                        FloatLargeArray fTarget = outField.produceCoords(time);
                        FloatLargeArray fSource = model.floatArrs[iComp];
                        if (fTarget != fSource)
                        {
                            if (fTarget.length() == fSource.length())
                                LargeArrayUtils.arraycopy(fSource, 0, fTarget, 0, fSource.length());
                            else
                            for (long i = coord, j = 0; j < fSource.length(); i += vlen, j++)
                                fTarget.setFloat(i, fSource.getFloat(j));
                        }
                    } else if (cmp == container.getNComponents() + 1) {
                        LogicLargeArray mask = outField.produceMask(time);
                        for (long i = 0; i < model.boolArrs[iComp].length(); i++)
                            mask.setByte(i, model.boolArrs[iComp].getByte(i));
                    }
                }
            }
        }
        return 0;
    }
}
