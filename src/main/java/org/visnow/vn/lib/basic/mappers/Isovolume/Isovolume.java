//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isovolume;

import javax.swing.event.ChangeEvent;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import org.visnow.vn.lib.gui.ComponentBasedUI.value.ComponentValue;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.subset.subvolume.IrregularFieldSplitter;
import org.visnow.vn.lib.utils.field.subset.subvolume.RegularFieldSplitter;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Isovolume extends OutFieldVisualizationModule {

    /**
     *
     * inField - a 3D field to create isovolume; at least one scalar data
     * component must be present.
     * <p>
     * outField - isovolume field will be created by update method - can be
     * void, can contain no node data (geometry only)
     *
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;

    protected IrregularField irregularInField;

    protected RegularField regularInField;
    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    protected Params params;
    protected ComponentValue threshold;
    protected boolean debug = true;
    protected boolean ignoreUI = false;

    public Isovolume() {
        parameters = params = new Params();
        threshold = params.getThreshold();
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParams(params);
            ui.addComputeGUI(computeUI);
        });
        outObj.setName("isovolume");
        params.addChangeListener((ChangeEvent evt) -> {
            if (ignoreUI || params.getRunState() == NO_RUN) {
                ignoreUI = false;
                return;
            }
            fromGUI = true;
            startAction();
        });
        setPanel(ui);
    }

    @Override
    public void onActive() {
        if (getInputFirstValue("inField") != null) {
            if (!fromGUI) {
                ignoreUI = true;
                VNField inFld = (VNField) getInputFirstValue("inField");
                if (inFld != null) {
                    Field newInField = inFld.getField();
                    if (newInField == null || newInField.getNComponents() < 1) {
                        ignoreUI = false;
                        return;
                    }
                    if (inField == null || inField != newInField) {
                        inField = newInField;
                        threshold.setContainerSchema(inField.getSchema());
                        outField = outIrregularField = null;
                        if (params.getRunState() == NO_RUN) {
                            prepareOutputGeometry();
                            show();
                        }
                    }
                    ignoreUI = false;
                }
            }
            fromGUI = false;
            if (params.getRunState() == NO_RUN)
                return;
            params.setRunState(NO_RUN);

            DataArray isoDataArray = inField.getComponent(threshold.getComponentName());
            float thr = threshold.getValue();
            if (inField instanceof IrregularField)
                outIrregularField = IrregularFieldSplitter.splitField((IrregularField)inField, isoDataArray.getRawFloatArray(), isoDataArray.getVectorLength(),
                                                                     thr, params.getType() > 0);
            else
                outIrregularField = RegularFieldSplitter.splitField((RegularField)inField, isoDataArray.getRawFloatArray(), isoDataArray.getVectorLength(),
                                                                     thr, params.getType() > 0);
            if (outIrregularField != null) {
                setOutputValue("outField", new VNIrregularField(outIrregularField));
            } else {
                setOutputValue("outField", null);
            }
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
    }
}
