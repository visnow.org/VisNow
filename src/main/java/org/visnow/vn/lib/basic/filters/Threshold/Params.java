/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Threshold;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Float>("lowerThreshold", ParameterType.dependent, 0.f),
        new ParameterEgg<Float>("upperThreshold", ParameterType.dependent, 1.f),
        new ParameterEgg<Integer>("component", ParameterType.dependent, 0),
        new ParameterEgg<Boolean>("setLowToMin", ParameterType.dependent, false),
        new ParameterEgg<Boolean>("invalidate", ParameterType.dependent, false),
        new ParameterEgg<Boolean>("setUpToMax", ParameterType.dependent, false)
    };

    public Params()
    {
        super(eggs);
    }

    public int getComponent()
    {
        return (int) getValue("component");
    }

    public void setComponent(int component)
    {
        setValue("component", component);
    }

    public float getLowerThreshold()
    {
        return (Float) getValue("lowerThreshold");
    }

    public void setLowerThreshold(float lowerThreshold)
    {
        setValue("lowerThreshold", lowerThreshold);
    }

    public float getUpperThreshold()
    {
        return (Float) getValue("upperThreshold");
    }

    public void setUpperThreshold(float upperThreshold)
    {
        setValue("upperThreshold", upperThreshold);
    }

    public boolean isLowToMin()
    {
        return (Boolean) getValue("setLowToMin");
    }

    public void setLowToMin(boolean setLowToMin)
    {
        setValue("setLowToMin", setLowToMin);
    }

    public boolean isUpToMax()
    {
        return (Boolean) getValue("setUpToMax");
    }

    public void setUpToMax(boolean setUpToMax)
    {
        setValue("setUpToMax", setUpToMax);
    }

    public boolean isInvalidate()
    {
        return (Boolean) getValue("invalidate");
    }

    public void setInvalidate(boolean invalidate)
    {
        setValue("invalidate", invalidate);
    }

}
