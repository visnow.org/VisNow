/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.InterpolationToRegularField;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class InterpolationToRegularFieldShared
{
    
    static final String RESET_BOX_STRING = "Reset box";
    static final String RUNNING_MESSAGE_STRING = "Running message";
    static final String AUTO_STRING = "auto";
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!

    static final ParameterName<Boolean> GLOBAL_RESOLUTION  = new ParameterName("global resolution");
    static final ParameterName<Integer> RESOLUTION  = new ParameterName("Resolution");
    static final ParameterName<Integer> RESOLUTION0 = new ParameterName("Resolution 0");
    static final ParameterName<Integer> RESOLUTION1 = new ParameterName("Resolution 1");
    static final ParameterName<Integer> RESOLUTION2 = new ParameterName("Resolution 2");
    static final ParameterName<Boolean> IS_OUTPUT = new ParameterName("Output");
    static final ParameterName<Boolean> RESET_BOX = new ParameterName(RESET_BOX_STRING);
    static final ParameterName<Boolean> AUTO = new ParameterName(AUTO_STRING);
    static final ParameterName<Boolean> GLYPH_CHANGED = new ParameterName("Glyph changed");

    static final ParameterName<Integer> META_TRUE_NSPACE = new ParameterName("True nspace");
    static final ParameterName<Float>   META_DIAMETER = new ParameterName("Meta diametrer");
    
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>(RUNNING_MESSAGE_STRING);
}
