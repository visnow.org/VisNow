/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isosurface;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import org.visnow.vn.lib.gui.ComponentBasedUI.array.ComponentValuesArray;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class IsosurfaceShared
{
    public static final String THRESHOLDS_STRING           = "Thresholds";
    public static final String SMOOTHING_STEP_COUNT_STRING = "mesh smoothing steps";
    public static final String COMPUTE_UNCERTAINTY_STRING  = "Uncertainty component generation";
    public static final String CELLSET_SEPARATION_STRING   = "Isosurfaces as separate cell sets";
    public static final String GRID_STRING                 = "Create grid lines";
    public static final String TIME_STRING                 = "Selected time";
    public static final String RUNNING_MESSAGE_STRING      = "Running message";
    public static final String REGULAR_INPUT_STRING        = "regular infield";
    public static final String INFIELD_TIME_RANGE_STRING   = "infield time range";


    static final ParameterName<ComponentValuesArray> THRESHOLDS    = new ParameterName<>(THRESHOLDS_STRING);
    static final ParameterName<Integer> SMOOTHING_STEP_COUNT       = new ParameterName<>(SMOOTHING_STEP_COUNT_STRING);
    static final ParameterName<Boolean> COMPUTE_UNCERTAINTY        = new ParameterName<>(COMPUTE_UNCERTAINTY_STRING);
    static final ParameterName<Boolean> CELLSET_SEPARATION         = new ParameterName<>(CELLSET_SEPARATION_STRING);
    static final ParameterName<Boolean> GRID                       = new ParameterName<>(GRID_STRING);
    static final ParameterName<Float> TIME                         = new ParameterName<>(TIME_STRING);
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>(RUNNING_MESSAGE_STRING);
    static final ParameterName<Boolean> META_REGULAR_INPUT         = new ParameterName<>(REGULAR_INPUT_STRING);
    static final ParameterName<float[]> META_TIME_RANGE            = new ParameterName<>(INFIELD_TIME_RANGE_STRING);

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(THRESHOLDS, new ComponentValuesArray().preferredCount(1).startSingle(true)));
                add(new Parameter<>(SMOOTHING_STEP_COUNT, 0));
                add(new Parameter<>(COMPUTE_UNCERTAINTY, false));
                add(new Parameter<>(CELLSET_SEPARATION, false));
                add(new Parameter<>(GRID, false));
                add(new Parameter<>(TIME, 0f));
                add(new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY));
                add(new Parameter<>(META_REGULAR_INPUT, true));
                add(new Parameter<>(META_TIME_RANGE, new float[]{0, 0}));
            }
        };
    }
}
