/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadCDM4;

import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.readers.ReadCDM4.ReadCDM4Shared.*;

/**
 * Reader for HDF5 and CDM4 files.
 * 
 * It uses two libraries:
 * 
 * NetCDF-Java library {@link http://www.unidata.ucar.edu/software/thredds/current/netcdf-java}
 * for reading files compatible with the Common Data Model version 4 (abbreviated CDM4 here).
 * 
 * JHDF5 {@link https://wiki-bsse.ethz.ch/display/JHDF5}
 * for reading VisNow fields stored in HDF5 files.
 * 
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCDM4 extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadCDM4.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    public ReadCDM4()
    {

        parameters.addParameterChangelistener((String name) -> {
            startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            ui.addComputeGUI(computeUI);
            setPanel(ui);
            computeUI.setParameters(parameters);
        });
    }

    /**
     * Reads data from a file and returns a Field.
     *
     * @param params        parameters
     * @param progressAgent progress monitor
     *
     * @return a new Field or null if an error occurred  
     */
    public static Field createField(Parameters params, ProgressAgent progressAgent)
    {
        String filename = params.get(FILENAME);
        String visNowFieldName = params.get(VISNOW_FIELD_NAME);
        String[] visNowComponentNames = params.get(VISNOW_COMPONENT_NAMES);
        String[] variablesNames = params.get(VARIABLE_NAMES);
        Boolean firstDimensionIsTime = params.get(FIRST_DIMENSION_IS_TIME);
        Boolean lastDimensionIsVector = params.get(LAST_DIMENSION_IS_VECTOR);

        if (visNowFieldName.equals("")) {
            return ReadCDM4Core.createFieldFromVariables(filename, variablesNames, firstDimensionIsTime, lastDimensionIsVector, progressAgent);
        } else {
            return ReadHDF5Core.readVisNowField(filename, visNowFieldName, visNowComponentNames, progressAgent);
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadCDM4Shared.getDefaultParameters();
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        ProgressAgent progressAgent = getProgressAgent(100);
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        progressAgent.setProgress(0.0f);

        if (p.get(FILENAME).isEmpty()) {
            outField = null;
            progressAgent.setProgress(1.0);
            show();
            setOutputValue("outCDM4Datasets", null);
            return;
        } else {
            outField = createField(p, progressAgent);
            if (outField != null) {
                setOutputValue("outCDM4Datasets", new VNRegularField((RegularField) outField));
            } else {
                setOutputValue("outCDM4Datasets", null);
            }
            progressAgent.setProgress(0.9);
            prepareOutputGeometry();
            progressAgent.setProgress(1.0);
            show();
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            computeUI.activateOpenDialog();
        }
    }
}
