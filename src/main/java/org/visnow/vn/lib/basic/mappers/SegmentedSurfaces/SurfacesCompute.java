/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.SegmentedSurfaces;

/*
 *Isosurface.java
 *
 *Created on August 14, 2004, 2:06 PM
 */
import javax.swing.JProgressBar;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.utils.isosurface.IsosurfaceEngineParams;
import org.visnow.vn.lib.utils.isosurface.RegularFieldIsosurface;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SurfacesCompute
{

    private static final int CHUNK_SIZE = 1000;
    private RegularField inField = null;
    private Params params = null;
    private int nSets = 0;
    private int[] nNodes = null;
    private float[][] coords = null;
    private float[][] normals = null;
    private int[] nTriangles = null;
    private int[][] triangles = null;
    private int nThreads = 1;

    class SliceRemap implements org.visnow.vn.lib.utils.isosurface.RegularFieldIsosurface.SliceRemap
    {
        @Override
        public float[] remap(float[] slice, float val)
        {
            float[] remapped = new float[slice.length];
            for (int i = 0; i < slice.length; i++)
                remapped[i] = slice[i] == val ? 1 : -1;
            return remapped;
        }
    }

    /**
     * Creates a new instance of Isosurface
     */
    public SurfacesCompute()
    {
    }

    public SurfacesCompute(RegularField inField, JProgressBar progressBar, Params params)
    {
        if (inField == null || params == null)
            return;
        this.inField = inField;
        this.params = params;
        DataArray da = inField.getComponent(0);
        nSets = da.getUserData() != null ? da.getUserData().length - 1 : 0;
        nNodes = new int[nSets];
        nTriangles = new int[nSets];
        coords = new float[nSets][];
        normals = new float[nSets][];
        triangles = new int[nSets][];
    }

    public void updateSurfaces()
    {
        nThreads = params.getNThreads();
        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < nThreads; i++) {
            workThreads[i] = new Thread(new Compute(i));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            }catch (Exception e) {
            }
        params.setDimensionsChanged(false);
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public float[] getCoords(int n)
    {
        return coords[n];
    }

    public float[] getNormals(int n)
    {
        return normals[n];
    }

    public int[] getTriangles(int n)
    {
        return triangles[n];
    }

    class Compute implements Runnable
    {

        int iThread;

        public Compute(int iThread)
        {
            this.iThread = iThread;
        }

        public void run()
        {
            int start = 1;
            int nRuns = (nSets + nThreads - start - 1) / nThreads;
            for (int nSet = iThread + start; nSet < nSets; nSet += nThreads) {
                RegularFieldIsosurface isosurfaceEngine = new RegularFieldIsosurface(inField);
                isosurfaceEngine.setSliceRemap(new SliceRemap());
                IsosurfaceEngineParams engineParams =
                        new IsosurfaceEngineParams(0, params.getDownsize(),
                                                   params.getLow(), params.getUp(),
                                                   params.getSmoothSteps(), params.isSmoothing(),
                                                   false, false, false, 0);
                IrregularField outFld = isosurfaceEngine.makeIsosurface(engineParams, nSet);
                nNodes[nSet] = (int)outFld.getNNodes();
                nTriangles[nSet] = outFld.getCellSet(0).getCellArray(CellType.TRIANGLE).getNCells();
                coords[nSet] = outFld.getCoords(0).getData();
                normals[nSet] = outFld.getNormals().getData();
                triangles[nSet] = outFld.getCellSet(0).getCellArray(CellType.TRIANGLE).getNodes();
            }
        }
    }

    public float[][] getCoords()
    {
        return coords;
    }

    public int[] getNNodes()
    {
        return nNodes;
    }

    public int[] getNTriangles()
    {
        return nTriangles;
    }

    public float[][] getNormals()
    {
        return normals;
    }

    public int[][] getTriangles()
    {
        return triangles;
    }

    private transient FloatValueModificationListener statusListener = null;

    public void clearFloatValueModificationListener()
    {
        statusListener = null;
    }

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

}
