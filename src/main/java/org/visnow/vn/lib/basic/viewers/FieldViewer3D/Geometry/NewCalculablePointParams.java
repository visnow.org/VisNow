/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class NewCalculablePointParams
{

    private CalculablePointsPool.CalculablePointType type = null;
    private String name = "";
    private ArrayList<PointDescriptor> points = new ArrayList<PointDescriptor>();

    /**
     * @return the type
     */
    public CalculablePointsPool.CalculablePointType getType()
    {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(CalculablePointsPool.CalculablePointType type)
    {
        this.type = type;
        fireStateChanged();
    }

    /**
     * @return the points
     */
    public ArrayList<PointDescriptor> getPoints()
    {
        return points;
    }

    public void addPoint(PointDescriptor pd)
    {
        this.points.add(pd);
        fireStateChanged();
    }

    public void removePoint(int i)
    {
        this.points.remove(i);
        fireStateChanged();
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    public void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList) {
            listener.stateChanged(e);
        }
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
        fireStateChanged();
    }

    void addPoints(ArrayList<PointDescriptor> pds)
    {
        if (pds == null)
            return;

        this.points.addAll(pds);
        fireStateChanged();
    }

}
