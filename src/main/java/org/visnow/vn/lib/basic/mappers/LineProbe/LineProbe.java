/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.LineProbe;

import java.util.ArrayList;
import java.util.Arrays;
import org.jogamp.java3d.J3DGraphics2D;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.utils.CellSetGeometryUtilities;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.ResizeEvent;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import org.visnow.vn.geometries.objects.Geometry2D;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickListener;
import static org.visnow.vn.lib.basic.mappers.LineProbe.LineProbeShared.*;
import static org.visnow.vn.lib.basic.mappers.LineProbe.LineProbeShared.ProbeType.*;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.events.MouseRestingEvent;
import org.visnow.vn.lib.utils.events.MouseRestingListener;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import org.visnow.vn.lib.utils.lineProbe.BasicLineProbe;
import org.visnow.vn.lib.utils.graphing.GraphDisplay;
import org.visnow.vn.lib.utils.graphing.MultiGraphs;
import org.visnow.vn.lib.utils.lineProbe.IndexProbe1D;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.graphing.GraphParams;
import static org.visnow.vn.lib.utils.graphing.GraphParams.*;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class LineProbe extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected Field inField = null;

    protected IrregularField probeField = null;
    protected RegularField regularProbeField = null;

    protected GUI computeUI = null;

    protected boolean newGraphedFieldSet = false;

    protected Probe currentProbe = null;
    protected BasicLineProbe geometricProbe;
    protected InteractiveGlyph geometricProbeGlyph;

    protected IndexProbe1D indexProbe;
    protected OpenBranchGroup indexProbeGlyph;
    protected IndexSliceParams indexProbeParams;

    protected OpenBranchGroup currentProbeGlyph;

    protected MultiGraphs graphs = new MultiGraphs();
    protected GraphParams graphParams = graphs.getGraphParams();
    protected GraphDisplay pickedGraph = null;
    protected int windowWidth = 500;
    protected int windowHeight = 500;
    protected boolean fromParams = false;
    protected boolean probeReady = false;
    protected int maxProbesOnMargin = 6;

    Geometry2D graphsGeometry = new Geometry2D()
    {
        @Override
        public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
        {

            graphs.draw2D(vGraphics, ltw, w, h);
        }
    };

    protected MouseRestingListener toolTipListener = new MouseRestingListener()
    {
        @Override
        public void mouseResting(MouseRestingEvent e)
        {
            graphParams.setTooltipPosition(new int[]{e.getX(), e.getY()});
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    };

    protected ResizeListener resizeListener = (ResizeEvent e) -> {
        updateAddCapability();
    };

    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return toolTipListener;
    }

    PickListener graphPickingListener = new PickListener()
    {
        @Override
        public void pickChanged(PickEvent e)
        {
            for (ProbeDisplay display : graphs.getDisplays())
                display.setSelected(false);
            pickedGraph = graphs.pickedDisplay(e.getEvt().getX(), e.getEvt().getY());
            if (pickedGraph != null) {
                pickedGraph.setSelected(true);
                computeUI.enableRemoveGraph(true);
                refreshDisplay();
                computeUI.showGraphTitle(pickedGraph.getTitle());
            }
        }
    };

    private boolean canAddProbe = parameters.get(GRAPHS_POSITION) == AT_POINT ||
        graphs.getDisplays().size() < maxProbesOnMargin;

    ChangeListener probeListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            probeField = currentProbe.getSliceField();
            if (currentProbe instanceof IndexProbe1D)
                regularProbeField = ((IndexProbe1D) currentProbe).getRegularSliceField();
            else
                regularProbeField = null;
            probeReady = true;
            startAction();
            updateAddCapability();
        }
    };

    private void updateAddCapability()
    {
        Position probesPosition = parameters.get(GRAPHS_POSITION);
        int nGraphs = graphs.getDisplays().size();
        int[] maxGraphs = graphs.maxGraphCount();
        canAddProbe
            = (probesPosition != TOP && probesPosition != BOTTOM || nGraphs < maxGraphs[0]) &&
            (probesPosition != LEFT && probesPosition != RIGHT || nGraphs < maxGraphs[1]);
        computeUI.enableAddGraph(probeReady && canAddProbe);
    }

    public LineProbe()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e)
            {
                graphParams.setWindowBgrColor(e.getSelectedColor());
            }
        };

        parameters.addParameterChangelistener((String name) -> {
            if (name == null)
                return;
            fromParams = true;
            switch (name) {
                case ADD_SLICE_STRING:
                    probeField = currentProbe.getSliceAreaField();
                    if (probeField == null)
                        return;
                    for (ProbeDisplay display : graphs.getDisplays())
                        display.setSelected(false);
                    graphs.addDisplay(probeField, currentProbe.getPlaneCenter());
                    pickedGraph = (GraphDisplay) graphs.getDisplays().get(graphs.getDisplays().size() - 1);
                    pickedGraph.setSelected(true);
                    computeUI.enableRemoveGraph(true);
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    probeReady = false;
                    updateAddCapability();
                    startAction();
                    break;
                case DEL_SLICE_STRING:
                    graphs.removeDisplay(pickedGraph);
                    updateAddCapability();
                    pickedGraph = null;
                    startAction();
                    break;
                case CLEAR_SLICES_STRING:
                    outField = null;
                    graphs.clearDisplays();
                    updateAddCapability();
                    pickedGraph = null;
                    startAction();
                    break;
                case GRAPH_POSITION_STRING:
                case INIT_MARGIN_STRING:
                case GAP_STRING:
                    graphs.setProbesPosition(parameters.get(GRAPHS_POSITION));
                    graphs.setGap(parameters.get(GAP));
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    graphs.setRelMargin(parameters.get(INIT_MARGIN) / (float) 200);
                    refreshDisplay();
                    break;
                case GRAPH_TITLE_STRING:
                    ProbeDisplay currentGraph = graphs.getSelection();
                    if (currentGraph != null)
                        currentGraph.setTitle(parameters.get(GRAPH_TITLE));
                    refreshDisplay();
                    break;
                case POINTER_LINE_STRING:
                    graphs.setPointerLine(parameters.get(POINTER_LINE));
                    refreshDisplay();
                    break;
                case PROBE_TYPE_STRING:
                    if (geometricProbe != null && parameters.get(PROBE_TYPE) == GEOMETRIC)
                        currentProbe = geometricProbe;
                    else if (indexProbe != null)
                        currentProbe = indexProbe;
                    currentProbe.setDataMappingParams(dataMappingParams);
                    currentProbeGlyph = currentProbe.getGlyphGeometry();
                    doOutput();
                    break;
                default:
            }
            computeUI.enableRemoveGraph(pickedGraph != null);
        });

        graphParams.addParameterChangelistener((String name) -> {
            switch (name) {
                case EFFECTIVE_BGR_COLOR:
                    computeUI.getGraphGUI().updateEffectiveBgrColor();
                    break;
                case GRAPH_WIDTH:
                case GRAPH_HEIGHT:
                    graphs.setProbesPosition(parameters.get(GRAPHS_POSITION));
                    break;
            }
            if (inField != null && !graphs.isPacking() &&
                outObj.getRenderingWindow() != null)
                outObj.getRenderingWindow().refresh();
            computeUI.enableAddGraph(probeReady && canAddProbe);
        });

        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParameters(parameters);
            computeUI.setGraphParams(graphParams);
            JPanel probeGraphPanel = computeUI.getProbeGraphsPanel();
            computeUI.remove(probeGraphPanel);
            ui.getPresentationGUI().hideTransformPanel();
            ui.addComputeGUI(computeUI, "Probe");
            ui.insertAdditionalGUI(probeGraphPanel, "Graphs");
            setPanel(ui);
        });

        outObj.addPickListener(graphPickingListener);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ADD_SLICE, false),
            new Parameter<>(DEL_SLICE, false),
            new Parameter<>(CLEAR_SLICES, false),
            new Parameter<>(SHOW_ACCUMULATED, false),
            new Parameter<>(GRAPHS_POSITION, RIGHT),
            new Parameter<>(POINTER_LINE, true),
            new Parameter<>(INIT_MARGIN, 5),
            new Parameter<>(GAP, 10),
            new Parameter<>(GRAPH_TITLE, ""),
            new Parameter<>(PROBE_TYPE, GEOMETRIC)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(ADD_SLICE, false);
            parameters.set(DEL_SLICE, false);
            parameters.set(CLEAR_SLICES, false);
            parameters.set(SHOW_ACCUMULATED, false);
            parameters.set(GRAPHS_POSITION, RIGHT);
            parameters.set(POINTER_LINE, true);
            parameters.set(INIT_MARGIN, 5);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    protected void refreshDisplay()
    {
        if (outObj.getRenderingWindow() != null)
            outObj.getRenderingWindow().refresh();
    }

    protected void doOutput()
    {
        if (probeField == null && regularProbeField == null) {
            setOutputValue("currentOutField", null);
            setOutputValue("currentRegularOutField", null);
        } else if (regularProbeField != null) {
            setOutputValue("currentRegularOutField", new VNRegularField(regularProbeField));
            setOutputValue("currentOutField", null);
        } else {
            setOutputValue("currentRegularOutField", null);
            setOutputValue("currentOutField", new VNIrregularField(probeField));
        }
        if (graphs.getDisplays().isEmpty())
            setOutputValue("accumulatedOutField", null);
        else {
            ArrayList<ProbeDisplay> shownGraphs = graphs.getDisplays();
            outIrregularField = null;
            for (int i = 0; i < shownGraphs.size(); i++)
                outIrregularField = MergeIrregularField.merge(outIrregularField,
                                                              shownGraphs.get(i).getField(), i, false, true);
            outIrregularField.updatePreferredExtents();
            outField = outIrregularField;
            setOutputValue("accumulatedOutField", new VNIrregularField((IrregularField) outField));
        }

        prepareOutputGeometry();
        presentationParams.getRenderingParams().setDisplayMode(RenderingParams.EDGES);
        show();
        if (currentProbeGlyph.getParent() == null)
            outObj.addNode(currentProbeGlyph);
        currentProbe.setDataMappingParams(dataMappingParams);
        outObj.addGeometry2D(graphsGeometry);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            outObj.clearAllGeometry();
            outObj.clearGeometries2D();
            return;
        }
        if (fromParams)
            fromParams = false;
        else {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;
            validateParamsAndSetSmart(isDifferentField && !isFromVNA());
            notifyGUIs(parameters, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (isNewField) {
                graphs.clearDisplays();
                probeField = null;
                regularProbeField = null;
                outField = null;
                setOutputValue("currentRegularOutField", null);
                setOutputValue("currentOutField", null);
                setOutputValue("accumulatedOutField", null);
                if (geometricProbe != null)
                    geometricProbe.clearChangeListeners();
                geometricProbe = null;
                if (indexProbe != null)
                    indexProbe.clearChangeListeners();
                indexProbe = null;
                if (inField instanceof IrregularField) {
                    CellSetGeometryUtilities.addGeometryDataToCellSets(((IrregularField) inField).getCellSets(), inField.getCoords(0));
                }
                validateParamsAndSetSmart(true);
                dataMappingParams.setInData(newField, (DataContainer) null);
                if (inField instanceof RegularField) {
                    indexProbe = new IndexProbe1D();
                    indexProbe.setInData(inField, dataMappingParams);
                    indexProbe.addChangeListener(probeListener);
                    indexProbeGlyph = indexProbe.getGlyphGeometry();
                    currentProbe = indexProbe;
                }
                if (inField.getTrueNSpace() >= 2) {
                    geometricProbe = new BasicLineProbe();
                    geometricProbe.setInData(inField, dataMappingParams);
                    geometricProbeGlyph = geometricProbe.getGlyph();
                    geometricProbe.addChangeListener(probeListener);
                    currentProbe = geometricProbe;
                }
                currentProbeGlyph = currentProbe.getGlyphGeometry();
                probeField = currentProbe.getSliceField();
                computeUI.setProbePanels(geometricProbe != null ? geometricProbe.getGlyphGUI() : null,
                                         indexProbe != null ? indexProbe.getGlyphGUI() : null);

                outField = null;
                graphs.clearDisplays();
                newGraphedFieldSet = false;
                probeReady = canAddProbe = true;
                computeUI.enableAddGraph(probeReady && canAddProbe);
            }
            if (!newGraphedFieldSet && probeField != null) {
                computeUI.setGraphedField(probeField);
                newGraphedFieldSet = true;
            }
        }
        doOutput();
    }
}
