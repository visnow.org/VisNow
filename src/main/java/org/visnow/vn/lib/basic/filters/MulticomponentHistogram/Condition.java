/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

/**
 *
 * @author babor
 */
public abstract class Condition
{

    public static enum Operator
    {

        EQUAL
            {
                @Override
                public String toString()
                {
                    return "=";
                }
            },
        NOT_EQUAL
            {
                @Override
                public String toString()
                {
                    return "!=";
                }
            },
        GREATER
            {
                @Override
                public String toString()
                {
                    return ">";
                }
            },
        GREATER_OR_EQUAL
            {
                @Override
                public String toString()
                {
                    return ">=";
                }
            },
        LESS
            {
                @Override
                public String toString()
                {
                    return "<";
                }
            },
        LESS_OR_EQUAL
            {
                @Override
                public String toString()
                {
                    return "<=";
                }
            }

    };

    public static enum Logic
    {

        AND
            {
                @Override
                public String toString()
                {
                    return "AND";
                }
            },
        OR
            {
                @Override
                public String toString()
                {
                    return "OR";
                }
            }
    };

    protected Operator operator = null;

    public abstract boolean check(int n);

    protected boolean decide(int value1, int value2)
    {
        switch (operator) {
            case EQUAL:
                return (value1 == value2);
            case NOT_EQUAL:
                return (value1 != value2);
            case GREATER:
                return (value1 > value2);
            case GREATER_OR_EQUAL:
                return (value1 >= value2);
            case LESS:
                return (value1 < value2);
            case LESS_OR_EQUAL:
                return (value1 <= value2);
        }
        return false;
    }

    protected boolean decide(float value1, float value2)
    {
        switch (operator) {
            case EQUAL:
                return (value1 == value2);
            case NOT_EQUAL:
                return (value1 != value2);
            case GREATER:
                return (value1 > value2);
            case GREATER_OR_EQUAL:
                return (value1 >= value2);
            case LESS:
                return (value1 < value2);
            case LESS_OR_EQUAL:
                return (value1 <= value2);
        }
        return false;
    }

    public Operator getOperator()
    {
        return operator;
    }

    public void setOperator(Operator op)
    {
        this.operator = op;
    }

}
