/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class CustomSlicesDescriptor
{

    private static int id = 0;
    protected String name = "";
    protected float[] p0;
    protected float[][] v;

    public CustomSlicesDescriptor(String s, float[] p0, float[][] v)
    {
        name = s;
        this.p0 = p0;
        this.v = v;
    }

    public CustomSlicesDescriptor(float[] p0, float[][] v)
    {
        name = "planes" + nextId();
        this.p0 = p0;
        this.v = v;
    }

    public CustomSlicesDescriptor(int n, float[] p0, float[][] v)
    {
        name = "planes" + n;
        this.p0 = p0;
        this.v = v;
    }

    public float[] getOriginPoint()
    {
        return p0;
    }

    public void setOriginPoint(float[] p0)
    {
        this.p0 = p0;
    }

    public float[][] getVectors()
    {
        return v;
    }

    public void setVectors(float[][] v)
    {
        this.v = v;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static int nextId()
    {
        return id++;
    }

    public static void resetIdCounter()
    {
        id = 0;
    }

    public static void setIdCounter(int n)
    {
        id = n;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
