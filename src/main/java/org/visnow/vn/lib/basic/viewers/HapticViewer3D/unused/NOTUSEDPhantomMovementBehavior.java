/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import org.jogamp.java3d.utils.behaviors.sensor.SensorEvent;
import org.jogamp.java3d.utils.behaviors.sensor.SensorEventAgent;
import org.jogamp.java3d.utils.behaviors.sensor.SensorInputAdaptor;
import java.util.Enumeration;
import java.util.Iterator;
import org.jogamp.java3d.Behavior;
import org.jogamp.java3d.Sensor;
import org.jogamp.java3d.Transform3D;
import org.jogamp.java3d.TransformGroup;
import org.jogamp.java3d.WakeupCondition;
import org.jogamp.java3d.WakeupCriterion;
import org.jogamp.java3d.WakeupOnElapsedFrames;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;

/**
 * @deprecated NOT USED!
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class NOTUSEDPhantomMovementBehavior extends Behavior
{

    private Sensor sensor = null;
    private SensorEventAgent eventAgent = null;
    private TransformGroup transformGroup = null;
    private WakeupCondition conditions = new WakeupOnElapsedFrames(0);
    private InputDevicePointerController phantom;

    public NOTUSEDPhantomMovementBehavior(InputDevicePointerController phantom, TransformGroup tg)
    {
        transformGroup = tg;
        this.phantom = phantom;
        sensor = phantom.getSensor(0);
        eventAgent = new SensorEventAgent(this);
        eventAgent.addSensorButtonListener(sensor, 0, new ButtonSL());
    }

    @Override
    public void initialize()
    {
        wakeupOn(conditions);
    }

    @Override
    public void processStimulus(Iterator<WakeupCriterion> itrtr) {
        eventAgent.dispatchEvents();
        wakeupOn(conditions);
    }

    class ButtonSL extends SensorInputAdaptor
    {

        private boolean dragging = false;
        private Transform3D baseSceneTransform = new Transform3D();
        private Transform3D basePhantomTransform = new Transform3D();
        private Transform3D currentPhantomTransform = new Transform3D();

        @Override
        public void dragged(SensorEvent e)
        {
            phantom.getSensor(0).lastRead(currentPhantomTransform);
            if (!dragging) {
                transformGroup.getTransform(baseSceneTransform);
                basePhantomTransform.set(currentPhantomTransform);
                dragging = true;
            }
            Transform3D phantomDiff = new Transform3D();
            phantomDiff.mulInverse(currentPhantomTransform, basePhantomTransform);
            phantomDiff.mul(baseSceneTransform);
            transformGroup.setTransform(phantomDiff);
        }

        @Override
        public void released(SensorEvent e)
        {
            dragging = false;
        }
    }
}
