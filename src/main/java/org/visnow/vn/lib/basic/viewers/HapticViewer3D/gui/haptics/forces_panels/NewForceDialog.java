/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.haptics.forces_panels;

import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JList;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.CenterableJDialog;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.InputFields;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.ForceContext;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.JListClickHelper;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.NewForceWrapper;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;

/**
 * Displays a list of types of forces that can be added to our haptic scene. VWORLD/TRACKER/LOCAL
 * in parenthesis means in which coordinate system coordinates of points are expressed.
 * <p>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class NewForceDialog extends CenterableJDialog
{

    private CenterableJDialog dialog;
    private IForce force;
    private InputFields fields;

    /**
     * Creates new form NewForceFrame
     */
    public NewForceDialog(Window owner, InputFields fields)
    {
        super(owner);

        this.fields = fields;
        this.setModalityType(DEFAULT_MODALITY_TYPE);

        dialog = new CenterableJDialog(owner, "Set force parameters");
        dialog.setModal(true);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        initComponents();

        /* mouse listener - user can click twice on name of the force instead of selecting it 
         * and clicking Add */
        forceTypeCB.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {

                if (evt.getButton() == MouseEvent.BUTTON1 &&
                    evt.getClickCount() == 2) { // left double click - go Next

                    JList list = (JList) evt.getSource();

                    int index = JListClickHelper.getClickedLocation(list, evt.getPoint()); //could not be locationToIndex(evt.getPoint()), see comment in getClickedLocation
                    if (index >= 0) {
                        list.setSelectedIndex(index);
                        addButtonActionPerformed(null);
                    }
                }
            }
        });

    }

    public IForce getForce()
    {
        return force;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        addButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        forceTypeCB = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Choose force type");
        setResizable(false);

        jLabel1.setText("Force type:");

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        forceTypeCB.setModel(new DefaultComboBoxModel(ForceContext.newForces));
        jScrollPane1.setViewportView(forceTypeCB);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 35, Short.MAX_VALUE)
                                                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(addButton)
                                        .addComponent(cancelButton))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        NewForceWrapper selectedForce = (NewForceWrapper) forceTypeCB.getSelectedValue();
        if (selectedForce != null) {

            AbstractEditPanel editPanel
                = AbstractEditPanel.createPanel(selectedForce.getForce(),
                                                fields);

            dialog.setContentPane(editPanel);
            dialog.pack();
            dialog.centerOn(this);

            setVisible(false);
            dialog.setVisible(true); // this call makes the thread to block 
            // and wait for closing the dialog

            force = editPanel.getForce();

        }
    }//GEN-LAST:event_addButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        force = null;
        setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed
     // Variables declaration - do not modify//GEN-BEGIN:variables

    private javax.swing.JButton addButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JList forceTypeCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
//revised.
