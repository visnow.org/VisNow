/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools;

import java.awt.Point;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Metadata implements Cloneable
{

    private Object data = null;
    private String name = null;

    //private Point location = null;
    public Metadata(Object data)
    {
        //this(null, data, null);
        this(null, data);
    }

    //public Metadata(String name, Object data, Point location) {
    public Metadata(String name, Object data)
    {
        this.name = name;
        this.data = data;
        //this.location = location;
    }

    public Object getObject()
    {
        return data;
    }

    public void setObject(Object data)
    {
        this.data = data;
    }

    @Override
    public Metadata clone()
    {
        try {
            Metadata out = (Metadata) super.clone();
            if (name != null)
                out.setName(new String(name));
            //            if(location != null)
            //                out.setLocation(new Point(location));
            return out;
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    //    /**
    //     * @return the location
    //     */
    //    public Point getLocation() {
    //        return location;
    //    }
    //
    //    /**
    //     * @param location the location to set
    //     */
    //    public void setLocation(Point location) {
    //        this.location = location;
    //    }
}
