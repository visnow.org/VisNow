/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics;

import org.jogamp.vecmath.Tuple3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.IPassiveDevice;

/**
 * Interface for getting from an haptic device read-only parameters: force vector, flag "force
 * enabled", general force clamp and device's max generated force capability.
 * <p/>
 * To be used when only read-only access to a device suffices (e.g. displaying a force magnitude bar
 * in view).
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface IHapticReadOnlyDevice extends IPassiveDevice
{

    /**
     * Get force vector from the device.
     * If forces are disabled it will return zero.
     *
     * @param outForce Force vector
     */
    public void getForce(Tuple3f outForce);

    /**
     * Get maximum force that a device can generate through a period of time without being damaged.
     * In Phantom it's a HD_NOMINAL_MAX_CONTINUOUS_FORCE constant.
     *
     * Although this method will be called only a few times, it would be better if it was optimized.
     *
     * @return maximum allowed continuous force
     * <p>
     * @throws HapticException
     */
    public double getDeviceNominalMaxContinuousForce() throws HapticException;

    /**
     * Get maximum force that a device can generate IN OPTIMAL CONDITIONS!! without being damaged
     * (when the motors are at room temperature).
     * <p/>
     * In Phantom it's a HD_NOMINAL_MAX_FORCE constant.
     *
     * Although this method will be called only a few times, it would be better if it was optimized.
     *
     * @return maximum allowed force in optimal conditions
     * <p>
     * @throws HapticException
     */
    public double getDeviceNominalMaxExertableForce() throws HapticException;

    /**
     * Returns maximum level of damping that is recommended for the device, i.e. the
     * damping constant used in
     * <code>F = kx - dv</code> <br/>
     * where: <br/>
     * <ul>
     * <li><var>d</var> is the damping constant,</li>
     * <li><var>v</var> is the velocity of the device.</li>
     *
     * If the device doesn't support this, it should return Double.MAX_VALUE
     *
     * @return maximum recommended value of a damping constant or Double.MAX_VALUE if unknown
     * <p>
     * @throws HapticException
     */
    public double getNominalMaxDamping() throws HapticException;
}
