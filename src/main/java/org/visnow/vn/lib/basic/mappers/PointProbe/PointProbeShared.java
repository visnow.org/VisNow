/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.PointProbe;


import java.awt.Color;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class PointProbeShared 
{
    public static enum ProbeType {GEOMETRIC, INDEX};
    
    public static final String ADD_PROBE_STRING = "Add probe";
    public static final String DEL_PROBE_STRING = "Remove probe";
    public static final String CLEAR_PROBES_STRING = "Clear probes";
    public static final String LABEL_POSITION_STRING = "graphs position";
    public static final String INIT_MARGIN_STRING = "init margin";
    public static final String FONT_SIZE_STRING = "gap";
    public static final String POINTER_LINE_STRING = "pointer line";
    public static final String PROBE_TYPE_STRING = "probe type";
    public static final String SELECTED_LABELS_STRING = "selected labels";

    static final ParameterName<Boolean>    ADD_PROBE = new ParameterName(ADD_PROBE_STRING);
    static final ParameterName<Boolean>    DEL_PROBE = new ParameterName(DEL_PROBE_STRING);
    static final ParameterName<Boolean>    CLEAR_PROBES = new ParameterName(CLEAR_PROBES_STRING);
    static final ParameterName<Position>   LABEL_POSITION = new ParameterName(LABEL_POSITION_STRING);
    static final ParameterName<Boolean>    POINTER_LINE = new ParameterName(POINTER_LINE_STRING);
    static final ParameterName<ProbeType>  PROBE_TYPE = new ParameterName(PROBE_TYPE_STRING);
    static final ParameterName<Integer>    INIT_MARGIN = new ParameterName(INIT_MARGIN_STRING);
    static final ParameterName<boolean[]>  SELECTED_LABELS = new ParameterName(SELECTED_LABELS_STRING);
}
