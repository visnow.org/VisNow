/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SkeletonizerParams extends Parameters
{
    public static final int SKELETONIZE = 3;
    public static final int MIN_LENGTH = 2;
    public static final int SELECT = 1;
    protected static final String COMPONENT = "component";
    protected static final String THRESHOLD = "threshold";
    protected static final String MIN_CENTER_DEPTH = "minimal center depth";
    protected static final String MAX_RADIUS = "max radius";
    protected static final String ABOVE = "above";
    protected static final String START_POINTS = "start points";
    protected static final String MIN_COMPUTED_SEG = "minimal segment length";
    protected static final String MIN_SEG_LEN = "minimal displayed segment length";
    protected static final String SEGMENTED = "segmented";
    protected static final String SETS = "sets";
    protected static final String NSETS = "nsets";
    protected static final String RECOMPUTE = "recompute";
    
    @SuppressWarnings("unchecked")
    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(COMPONENT, ParameterType.dependent, 0),
        new ParameterEgg<Float>(THRESHOLD, ParameterType.dependent, 128.f),
        new ParameterEgg<Float>(MAX_RADIUS, ParameterType.dependent, 50.f),
        new ParameterEgg<Float>(MIN_CENTER_DEPTH, ParameterType.dependent, 0.f),
        new ParameterEgg<Boolean>(ABOVE, ParameterType.dependent, true),
        new ParameterEgg<int[][]>(START_POINTS, ParameterType.dependent, null),
        new ParameterEgg<Integer>(MIN_COMPUTED_SEG, ParameterType.dependent, 20),
        new ParameterEgg<Integer>(MIN_SEG_LEN, ParameterType.dependent, 100),
        new ParameterEgg<Boolean>(SEGMENTED, ParameterType.dependent, false),
        new ParameterEgg<int[]>(SETS, ParameterType.dependent, null),
        new ParameterEgg<Integer>(NSETS, ParameterType.dependent, 1),
        new ParameterEgg<Integer>(RECOMPUTE, ParameterType.independent, 0)
    };

    public SkeletonizerParams()
    {
        super(eggs);
    }

    /**
     * Get the value of recompute
     *
     * @return the value of recompute
     */
    public int isRecompute()
    {
        return (Integer) getValue(RECOMPUTE);
    }

    /**
     * Set the value of recompute
     *
     * @param recompute new value of recompute
     */
    public void setRecompute(int recompute)
    {
        setValue(RECOMPUTE, recompute);
    }

    /**
     * Get the value of nSets
     *
     * @return the value of nSets
     */
    public int getNSets()
    {
        return (Integer) getValue(NSETS);
    }

    /**
     * Set the value of nSets
     *
     * @param nSets new value of nSets
     */
    public void setNSets(int nSets)
    {
        setValue(NSETS, nSets);
    }

    /**
     * Get the value of sets
     *
     * @return the value of sets
     */
    public int[] getSets()
    {
        return (int[]) getValue(SETS);
    }

    /**
     * Set the value of sets
     *
     * @param sets new value of sets
     */
    public void setSets(int[] sets)
    {
        setValue(SETS, sets);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    
    public int[][] getStartPoints()
    {
        return (int[][]) getValue(START_POINTS);
    }

    
    public void setStartPoints(int[][] startPoints)
    {
        setValue(START_POINTS, startPoints);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    /**
     * Get the value of sets at specified index
     *
     * @param index
     *              <p>
     * @return the value of sets at specified index
     */
    public int getSets(int index)
    {
        return ((int[]) getValue(SETS))[index];
    }

    /**
     * Set the value of sets at specified index.
     *
     * @param index
     * @param newSets new value of sets at specified index
     */
    public void setSets(int index, int newSets)
    {
        ((int[]) getValue(SETS))[index] = newSets;
    }

    /**
     * Get the value of segmented
     *
     * @return the value of segmented
     */
    public boolean isSegmented()
    {
        return (Boolean) getValue(SEGMENTED);
    }

    /**
     * Set the value of segmented
     *
     * @param segmented new value of segmented
     */
    public void setSegmented(boolean segmented)
    {
        setValue(SEGMENTED, segmented);
    }

    public int getMinComputedSegLen()
    {
        return (Integer) getValue(MIN_COMPUTED_SEG);
    }

    public void setMinComputedSegLen(int minComputedSegLen)
    {
        setValue(MIN_COMPUTED_SEG, minComputedSegLen);
        setValue(RECOMPUTE, MIN_LENGTH);
        setActive(true);
    }

    public float getMinCenterDepth()
    {
        return (Float) getValue(MIN_CENTER_DEPTH);
    }

    public void setMinCenterDepth(float minCenterDepth)
    {
        setValue(MIN_CENTER_DEPTH, minCenterDepth);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    public int getMinSegLen()
    {
        return (Integer) getValue(MIN_SEG_LEN);
    }

    public void setMinSegLen(int minSegLen)
    {
        setValue(MIN_SEG_LEN, minSegLen);
        setValue(RECOMPUTE, MIN_LENGTH);
        setActive(true);
    }

    public float getThreshold()
    {
        return (Float) getValue(THRESHOLD);
    }

    public void setThreshold(float threshold)
    {
        setValue(THRESHOLD, threshold);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    public float getMaxRadius()
    {
        return (Float) getValue(MAX_RADIUS);
    }

    public void setMaxRadius(float threshold)
    {
        setValue(MAX_RADIUS, threshold);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    public boolean isAbove()
    {
        return (Boolean) getValue(ABOVE);
    }

    public void setAbove(boolean above)
    {
        setValue(ABOVE, above);
        setValue(RECOMPUTE, SKELETONIZE);
    }

    public int getComponent()
    {
        return (Integer) getValue(COMPONENT);
    }

    public void setComponent(int colorComponent)
    {
        setValue(COMPONENT, colorComponent);
        setValue(RECOMPUTE, SKELETONIZE);
    }
}
