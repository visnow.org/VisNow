/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.utils.TableUtilities;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.SteppedComboBox;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends javax.swing.JPanel
{

    private Parameters parameters;
    private int nComps = 0;
    private static final String[] actionTableHeader = new String[]{
        "component", "action", "retain", "physical min", "physical max"
    };
    private JPopupMenu retainColumnPopup;
    private static final Class[] actionTableTypes = new Class[]{
        String.class, String.class, Boolean.class, Float.class, Float.class
    };
    private static final int[] actionColumnWidth = new int[]{130, 50, 50, 50, 50};

    private static final String[] complexSplitTableHeader = new String[]{
        "component", "Re", "Im", "Abs", "Arg"
    };
    private static final Class[] complexSplitTableTypes = new Class[]{
        String.class, Boolean.class, Boolean.class, Boolean.class, Boolean.class
    };

    private static final String[] vectorsTableHeader = new String[]{
        "Name", "x", "y", "z", "||"
    };

    private static final Class[] vectorTableTypes = new Class[]{
        String.class, String.class, String.class, String.class, Boolean.class
    };

    private static final String[] complexCombineTableHeader = new String[]{
        "name", "Re", "Im"
    };
    private final Vector<String> compNames = new Vector<>();
    private final ArrayList<Integer> compIndices = new ArrayList<>();
    private final Vector<String> vCompNames = new Vector<>();
    private final ArrayList<Integer> vCompIndices = new ArrayList<>();

    private static final String[] vCompTableHeader = new String[]{
        "vector component", "||", "/||", "split"
    };
    private static final Class[] vCompTableTypes = new Class[]{
        String.class, Boolean.class, Boolean.class, Boolean.class
    };
    private final Vector<String> complexCompNames = new Vector<>();
    private final Vector<String> nonComplexCompNames = new Vector<>();
    private final ArrayList<Integer> complexCompIndices = new ArrayList<>();
    private final ArrayList<Integer> nonComplexCompIndices = new ArrayList<>();

    private static final int[] createVectorsColumnWidth = new int[]{140, 45, 45, 45, 20};
    private static final int[] vectorsCompColumnWidth = new int[]{160, 20, 20, 30};
    private static final int[] complexSplitPreferredColumnWidth = new int[]{150, 40, 40, 40, 40};

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        actionTable.setRowHeight(22);
        for (int i = 1; i < actionColumnWidth.length; i++)
            actionTable.getColumnModel().getColumn(i).setWidth(actionColumnWidth[i]);
        actionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        actionTable.getTableHeader().setReorderingAllowed(false);
        for (int i = 1; i < createVectorsColumnWidth.length; i++)
            createVectorsTable.getColumnModel().getColumn(i).setWidth(actionColumnWidth[i]);
        createVectorsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for (int i = 1; i < vectorsCompColumnWidth.length; i++)
            vectorOperationsTable.getColumnModel().getColumn(i).setWidth(vectorsCompColumnWidth[i]);

        vectorOperationsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        complexSplitTable.setRowHeight(22);
        complexSplitTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        maskComponentSelector.setAddNullComponent(true);
        maskComponentSelector.setScalarComponentsOnly(true);
        maskComponentSelector.setStartNull(true);

        //create retain column popup menu        
        JMenuItem retainColumnPopupSelectAll = new JMenuItem("select all");
        retainColumnPopupSelectAll.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                retainColumnSelectAll();
            }
        });
        JMenuItem retainColumnPopupUnselectAll = new JMenuItem("unselect all");
        retainColumnPopupUnselectAll.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                retainColumnUnselectAll();
            }
        });
        JMenuItem retainColumnPopupInvert = new JMenuItem("invert selection");
        retainColumnPopupInvert.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                retainColumnInvert();
            }
        });
        retainColumnPopup = new JPopupMenu();
        retainColumnPopup.add(retainColumnPopupSelectAll);
        retainColumnPopup.add(retainColumnPopupUnselectAll);
        retainColumnPopup.add(retainColumnPopupInvert);

        actionTable.getTableHeader().addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                int c = actionTable.getTableHeader().columnAtPoint(e.getPoint());
                if (e.getButton() == MouseEvent.BUTTON3 && (e.getComponent() instanceof JTableHeader) && c == 2) {
                    retainColumnPopup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        actionTable.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                int c = actionTable.columnAtPoint(e.getPoint());
                if (e.getButton() == MouseEvent.BUTTON3 && c == 2) {
                    retainColumnPopup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });

        TableUtilities.addContentTooltipPopup(actionTable, 0);
        TableUtilities.addContentTooltipPopup(vectorOperationsTable, 0);
        TableUtilities.addContentTooltipPopup(complexSplitTable, 0);
    }

    private boolean retainColumnSelectionInProgress = false;

    private void retainColumnSelectAll()
    {
        retainColumnSelectionInProgress = true;
        boolean change = false;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            if (!(Boolean) actionTable.getModel().getValueAt(i, 2))
                change = true;
            actionTable.getModel().setValueAt(true, i, 2);
        }
        retainColumnSelectionInProgress = false;
        if (change)
            ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    private void retainColumnUnselectAll()
    {
        retainColumnSelectionInProgress = true;
        boolean change = false;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            if ((Boolean) actionTable.getModel().getValueAt(i, 2))
                change = true;
            actionTable.getModel().setValueAt(false, i, 2);
        }
        retainColumnSelectionInProgress = false;
        if (change)
            ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    private void retainColumnInvert()
    {
        retainColumnSelectionInProgress = true;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            actionTable.getModel().setValueAt(!(Boolean) actionTable.getModel().getValueAt(i, 2), i, 2);
        }
        retainColumnSelectionInProgress = false;
        ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;

    }

    void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {

        String[] names = p.get(META_COMPONENT_NAMES);
        nComps = names.length;
        DataContainerSchema schema = p.get(META_SCHEMA);
        compIndices.clear();
        vCompIndices.clear();
        complexCompIndices.clear();
        nonComplexCompIndices.clear();

        compNames.clear();
        vCompNames.clear();
        complexCompNames.clear();
        nonComplexCompNames.clear();
        compNames.add("");
        compIndices.add(-100);

        for (int i = 0; i < names.length; i++) {
            if (p.get(META_COMPONENT_TYPES)[i] != null && p.get(META_COMPONENT_TYPES)[i].isNumericType()) {
                if (p.get(META_COMPONENT_VECLEN)[i] == 1) {
                    compNames.add(p.get(META_COMPONENT_NAMES)[i]);
                    compIndices.add(i);
                } else if (p.get(META_COMPONENT_VECLEN)[i] > 1){
                    vCompNames.add(p.get(META_COMPONENT_NAMES)[i]);
                    vCompIndices.add(i);
                }

                if (p.get(META_COMPONENT_TYPES)[i] == DataArrayType.FIELD_DATA_COMPLEX) {
                    complexCompNames.add(p.get(META_COMPONENT_NAMES)[i]);
                    complexCompIndices.add(i);
                } else if (p.get(META_COMPONENT_VECLEN)[i] == 1) {
                    nonComplexCompNames.add(p.get(META_COMPONENT_NAMES)[i]);
                    nonComplexCompIndices.add(i);
                }
            }
        }

        Object[][] actionTableContent = new Object[nComps][5];
        for (int i = 0; i < nComps; i++) {
            actionTableContent[i][0] = names[i];
            for (int j = 0; j < ComponentOperationsShared.actionCodes.length; j++)
                if (ComponentOperationsShared.actionCodes[j] == p.get(ACTIONS)[i]) {
                    actionTableContent[i][1] = ComponentOperationsShared.actionNames[j];
                    break;
                }
            actionTableContent[i][2] = p.get(RETAIN)[i];
            actionTableContent[i][3] = p.get(MIN)[i];
            actionTableContent[i][4] = p.get(MAX)[i];
        }

        DefaultTableModel tm = new javax.swing.table.DefaultTableModel(actionTableContent, actionTableHeader)
        {
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return actionTableTypes[columnIndex];
            }

        };
        tm.addTableModelListener(new TableModelListener()
        {

            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (retainColumnSelectionInProgress)
                    return;
                updateParameters();
                parameters.fireParameterChanged(null);
                runButton1.setPendingIfNoAuto();
            }
        });
        actionTable.setModel(tm);
        for (int i = 1; i < 5; i++)
            actionTable.getColumnModel().getColumn(i).setWidth(actionColumnWidth[i]);
        SteppedComboBox comboBox = new SteppedComboBox(ComponentOperationsShared.actionNames);
        setUpComponentColumn(comboBox, actionTable.getColumnModel().getColumn(1));

        if (p.get(ADD_TO_MASK) == true) {
            addToMaskRadioButton.setSelected(true);
        } else {
            replaceMaskRadioButton.setSelected(true);
        }

        maskComponentSelector.setDataSchema(schema);
        maskComponentSelector.setNull();

        String[] coordItems = null;
        int[] coordIndices = null;

        coordItems = new String[]{"x coord", "y coord", "z coord"};
        coordIndices = new int[]{nComps, nComps + 1, nComps + 2};
            
        maskComponentSelector.addExtraItems(coordItems, coordIndices);
        maskComponentSelector.setComponent(p.get(MASK_COMPONENT));
        maskRangeSlider.setMinMax(p.get(MASK_MIN_MAX)[0], p.get(MASK_MIN_MAX)[1]);
        maskRangeSlider.setLowUp(p.get(MASK_LOW_UP)[0], p.get(MASK_LOW_UP)[1]);
        recomputeMinMaxBox.setSelected(parameters.get(RECOMPUTE_MIN_MAX));

        coordsPanel.setVisible(true);
        useCoordsCB.setEnabled(true);
        useCoordsCB.setSelected(p.get(USE_COORDS));

        if (p.get(USE_COORDS)) {
            String[] extraNames = null;
            int[] extraIndices = null;
            if (!p.get(META_IS_REGULAR_FIELD)) {
                complexCombinePanel.setVisible(false);
                complexSplitPanel.setVisible(false);
                extraNames = new String[]{"x", "y", "z", "0"};
                extraIndices = new int[]{-10, -11, -12, -100};
            } else {
                int nDims = p.get(NDIMS);
                switch (nDims) {
                    case 1:
                        extraNames = new String[]{"x", "y", "z", "i", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -100};
                        break;
                    case 2:
                        extraNames = new String[]{"x", "y", "z", "i", "j", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -2, -100};
                        break;
                    default:
                        extraNames = new String[]{"x", "y", "z", "i", "j", "k", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -2, -3, -100};
                        break;
                }

            }

            xComponentSelector.addExtraItems(extraNames, extraIndices);
            xComponentSelector.setDataSchema(p.get(META_SCHEMA));
            xComponentSelector.setComponent(p.get(XCOORD_COMPONENT));

            yComponentSelector.addExtraItems(extraNames, extraIndices);
            yComponentSelector.setDataSchema(p.get(META_SCHEMA));
            yComponentSelector.setComponent(p.get(YCOORD_COMPONENT));

            zComponentSelector.addExtraItems(extraNames, extraIndices);
            zComponentSelector.setDataSchema(p.get(META_SCHEMA));
            zComponentSelector.setComponent(p.get(ZCOORD_COMPONENT));

            indexComponentCB.setEnabled(true);
            indexComponentCB.setSelected(p.get(ADD_INDEX_COMPONENT));

        } else {
            indexComponentCB.setEnabled(false);
        }

        Vector<VectorComponent> vc = p.get(VECTOR_COMPONENTS);
        Object[][] cvcTableContent = new Object[10][5];
        for (int i = 0; i < 10; i++) {
            cvcTableContent[i][0] = null;
            cvcTableContent[i][1] = null;
            cvcTableContent[i][2] = null;
            cvcTableContent[i][3] = null;
            cvcTableContent[i][4] = null;
        }
        if (vc.size() > 0) {
            for (int i = 0; i < vc.size(); i++) {
                cvcTableContent[i][0] = vc.get(i).getName();
                int[] scalarComponents = vc.get(i).getScalarComponents();
                if (scalarComponents != null && scalarComponents.length > 0) {
                    for (int j = 0; j < scalarComponents.length; j++) {
                        if (scalarComponents[j] >= 0) {
                            cvcTableContent[i][j + 1] = names[scalarComponents[j]];
                        }
                    }
                }
                cvcTableContent[i][4] = vc.get(i).isComputeNorm();
            }
        }

        SteppedComboBox scalarCompComboBox = new SteppedComboBox(compNames);
        setUpComponentColumn(scalarCompComboBox, createVectorsTable.getColumnModel().getColumn(1));
        setUpComponentColumn(scalarCompComboBox, createVectorsTable.getColumnModel().getColumn(2));
        setUpComponentColumn(scalarCompComboBox, createVectorsTable.getColumnModel().getColumn(3));
        
        DefaultTableModel cvctm = new javax.swing.table.DefaultTableModel(cvcTableContent, vectorsTableHeader)
        {
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return vectorTableTypes[columnIndex];
            }
        };
        cvctm.addTableModelListener(new TableModelListener()
        {

            @Override
            public void tableChanged(TableModelEvent e)
            {
                updateParameters();
                parameters.fireParameterChanged(null);
                runButton1.setPendingIfNoAuto();
            }
        });

        createVectorsTable.setModel(cvctm);
        SteppedComboBox compComboBox = new SteppedComboBox(compNames);
        for (int i = 1; i < 4; i++)
            setUpComponentColumn(compComboBox, createVectorsTable.getColumnModel().getColumn(i));

        Object[][] vectorCompTableContent = new Object[vCompNames.size()][4];
        for (int i = 0; i < vCompNames.size(); i++) {
            vectorCompTableContent[i][0] = vCompNames.get(i);
            vectorCompTableContent[i][1] = p.get(VCNORMS)[i];
            vectorCompTableContent[i][2] = p.get(VCNORMALIZE)[i];
            vectorCompTableContent[i][3] = p.get(VCSPLIT)[i];
        }
        DefaultTableModel vtm = new javax.swing.table.DefaultTableModel(vCompNames.size(), 4)
        {
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return vCompTableTypes[columnIndex];
            }
        };
        vtm.setDataVector(vectorCompTableContent, vCompTableHeader);
        vtm.addTableModelListener(new TableModelListener()
        {

            @Override
            public void tableChanged(TableModelEvent e)
            {
                updateParameters();
                parameters.fireParameterChanged(null);
                runButton1.setPendingIfNoAuto();
            }
        });
        for (int i = 1; i < vectorsCompColumnWidth.length; i++)
            vectorOperationsTable.getColumnModel().getColumn(i).setWidth(vectorsCompColumnWidth[i]);
        vectorOperationsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        vectorOperationsTable.setModel(vtm);
        vectorOperationsTable.getColumnModel().getColumn(0).setPreferredWidth(150);
        Object[][] complexSpltTableContent = new Object[complexCompNames.size()][5];
        boolean[] splitRe = p.get(COMPLEX_SPLIT_RE);
        if (splitRe == null || splitRe.length != complexCompNames.size()) {
            splitRe = new boolean[complexCompNames.size()];
            for (int i = 0; i < splitRe.length; i++) {
                splitRe[i] = false;
            }
        }
        boolean[] splitIm = p.get(COMPLEX_SPLIT_IM);
        if (splitIm == null || splitIm.length != complexCompNames.size()) {
            splitIm = new boolean[complexCompNames.size()];
            for (int i = 0; i < splitIm.length; i++) {
                splitIm[i] = false;
            }
        }
        boolean[] splitAbs = p.get(COMPLEX_SPLIT_ABS);
        if (splitAbs == null || splitAbs.length != complexCompNames.size()) {
            splitAbs = new boolean[complexCompNames.size()];
            for (int i = 0; i < splitAbs.length; i++) {
                splitAbs[i] = false;
            }
        }
        boolean[] splitArg = p.get(COMPLEX_SPLIT_ARG);
        if (splitArg == null || splitArg.length != complexCompNames.size()) {
            splitArg = new boolean[complexCompNames.size()];
            for (int i = 0; i < splitArg.length; i++) {
                splitArg[i] = false;
            }
        }

        for (int i = 0; i < complexCompNames.size(); i++) {
            complexSpltTableContent[i][0] = p.get(META_COMPONENT_NAMES)[complexCompIndices.get(i)];
            complexSpltTableContent[i][1] = splitRe[i];
            complexSpltTableContent[i][2] = splitIm[i];
            complexSpltTableContent[i][3] = splitAbs[i];
            complexSpltTableContent[i][4] = splitArg[i];
        }
        DefaultTableModel ctm = new javax.swing.table.DefaultTableModel(complexSpltTableContent, complexSplitTableHeader)
        {
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return complexSplitTableTypes[columnIndex];
            }
        };
        ctm.addTableModelListener(new TableModelListener()
        {

            @Override
            public void tableChanged(TableModelEvent e)
            {
                updateParameters();
                parameters.fireParameterChanged(null);
                runButton1.setPendingIfNoAuto();
            }
        });
        complexSplitTable.setModel(ctm);
        for (int i = 1; i < 5; i++)
            complexSplitTable.getColumnModel().getColumn(i).setPreferredWidth(complexSplitPreferredColumnWidth[i]);
        complexSplitPanel.setVisible(complexCompNames.size() > 0);

        Vector<ComplexComponent> cc = p.get(COMPLEX_COMBINE_COMPONENTS);
        String[][] cccTableContent = new String[10][3];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 3; j++) {
                cccTableContent[i][j] = null;
            }
        }
        if (nonComplexCompNames.size() > 0) {
            for (int i = 0; i < cc.size(); i++) {
                cccTableContent[i][0] = cc.get(i).getName();

                int tmp = cc.get(i).getRealComponent();
                int k = -1;
                for (int j = 0; j < nonComplexCompNames.size(); j++) {
                    if (nonComplexCompIndices.get(j) == tmp) {
                        k = j;
                        break;
                    }
                }
                cccTableContent[i][1] = nonComplexCompNames.get(k);

                tmp = cc.get(i).getImagComponent();
                k = -1;
                for (int j = 0; j < nonComplexCompNames.size(); j++) {
                    if (nonComplexCompIndices.get(j) == tmp) {
                        k = j;
                        break;
                    }
                }
                cccTableContent[i][2] = nonComplexCompNames.get(k);
            }
        }
        DefaultTableModel ccctm = new javax.swing.table.DefaultTableModel(cccTableContent, complexCombineTableHeader);
        ccctm.addTableModelListener(new TableModelListener()
        {

            @Override
            public void tableChanged(TableModelEvent e)
            {
                updateParameters();
                parameters.fireParameterChanged(null);
                runButton1.setPendingIfNoAuto();
            }
        });
        complexCombineTable.setModel(ccctm);

        SteppedComboBox nonComplexCompComboBox = new SteppedComboBox(nonComplexCompNames);
        setUpComponentColumn(nonComplexCompComboBox, complexCombineTable.getColumnModel().getColumn(1));
        setUpComponentColumn(nonComplexCompComboBox, complexCombineTable.getColumnModel().getColumn(2));
        complexCombinePanel.setVisible(nonComplexCompNames.size() > 0);

        runButton1.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton1.updatePendingState(setRunButtonPending);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        maskButtonGroup = new javax.swing.ButtonGroup();
        mainPanel = new javax.swing.JPanel();
        functionPane = new javax.swing.JTabbedPane();
        componentselectorPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        actionTable = new javax.swing.JTable();
        functionPane1 = new javax.swing.JTabbedPane();
        vectorPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        createVectorsTable = new javax.swing.JTable();
        force3DBox = new javax.swing.JCheckBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        vectorOperationsTable = new javax.swing.JTable();
        complexPanel = new javax.swing.JPanel();
        complexCombinePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        complexCombineTable = new javax.swing.JTable();
        complexSplitPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        complexSplitTable = new javax.swing.JTable();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        maskPanel = new javax.swing.JPanel();
        addToMaskRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        replaceMaskRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        maskComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        maskRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        recomputeMinMaxBox = new javax.swing.JCheckBox();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        coordsPanel = new javax.swing.JPanel();
        useCoordsCB = new javax.swing.JCheckBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        indexComponentCB = new javax.swing.JCheckBox();
        xPanel = new javax.swing.JPanel();
        xComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        yPanel = new javax.swing.JPanel();
        yComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        zPanel = new javax.swing.JPanel();
        zComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        xScaleField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        xShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        yScaleField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        yShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        zScaleField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        zShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        xVarShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        yVarShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        zVarShiftField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        runButton1 = new org.visnow.vn.gui.widgets.RunButton();

        setLayout(new java.awt.BorderLayout());

        mainPanel.setLayout(new java.awt.GridBagLayout());

        functionPane.setToolTipText("<html>combine scalars as Re/Im part<p>split complex values into Re/Im<p>compute module and argument</html>");

        componentselectorPanel.setToolTipText("<html>change component type, <p>drop or apply algebraic function</html> ");
        componentselectorPanel.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 200));

        actionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "component", "action", "retain", "physical min", "physical max"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(actionTable);
        if (actionTable.getColumnModel().getColumnCount() > 0) {
            actionTable.getColumnModel().getColumn(0).setPreferredWidth(130);
            actionTable.getColumnModel().getColumn(1).setResizable(false);
            actionTable.getColumnModel().getColumn(1).setPreferredWidth(50);
            actionTable.getColumnModel().getColumn(2).setResizable(false);
            actionTable.getColumnModel().getColumn(2).setPreferredWidth(50);
            actionTable.getColumnModel().getColumn(3).setResizable(false);
            actionTable.getColumnModel().getColumn(3).setPreferredWidth(50);
            actionTable.getColumnModel().getColumn(4).setResizable(false);
            actionTable.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        componentselectorPanel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        vectorPanel.setToolTipText("<html>combine scalars into vector<p>compute norm or split vector in scalars</html>");
        vectorPanel.setLayout(new java.awt.GridBagLayout());

        jScrollPane2.setPreferredSize(new java.awt.Dimension(100, 100));

        createVectorsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Name", "x", "y", "z", "||"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        createVectorsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        createVectorsTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(createVectorsTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        vectorPanel.add(jScrollPane2, gridBagConstraints);

        force3DBox.setText("force 3D vectors");
        force3DBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                force3DBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        vectorPanel.add(force3DBox, gridBagConstraints);

        jScrollPane3.setPreferredSize(new java.awt.Dimension(100, 100));

        vectorOperationsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "component", "||", "/||", "split"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        vectorOperationsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        vectorOperationsTable.setDragEnabled(true);
        jScrollPane3.setViewportView(vectorOperationsTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        vectorPanel.add(jScrollPane3, gridBagConstraints);

        functionPane1.addTab("vectors", vectorPanel);

        complexPanel.setLayout(new java.awt.GridBagLayout());

        complexCombinePanel.setLayout(new java.awt.BorderLayout());

        jLabel1.setText("Combine components to complex");
        complexCombinePanel.add(jLabel1, java.awt.BorderLayout.NORTH);

        jScrollPane4.setPreferredSize(new java.awt.Dimension(100, 100));

        complexCombineTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "component", "Re", "Im"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        complexCombineTable.setRowSelectionAllowed(false);
        jScrollPane4.setViewportView(complexCombineTable);

        complexCombinePanel.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        complexPanel.add(complexCombinePanel, gridBagConstraints);

        complexSplitPanel.setLayout(new java.awt.BorderLayout());

        jLabel2.setText("Split complex components");
        complexSplitPanel.add(jLabel2, java.awt.BorderLayout.NORTH);

        jScrollPane5.setPreferredSize(new java.awt.Dimension(100, 100));

        complexSplitTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "component", "Re", "Im", "Abs", "Arg"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        complexSplitTable.setRowSelectionAllowed(false);
        jScrollPane5.setViewportView(complexSplitTable);

        complexSplitPanel.add(jScrollPane5, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        complexPanel.add(complexSplitPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        complexPanel.add(filler2, gridBagConstraints);

        functionPane1.addTab("complex", complexPanel);

        componentselectorPanel.add(functionPane1, java.awt.BorderLayout.PAGE_END);

        functionPane.addTab("basic", componentselectorPanel);

        maskPanel.setToolTipText("mask as invalid values outside specified range");
        maskPanel.setLayout(new java.awt.GridBagLayout());

        maskButtonGroup.add(addToMaskRadioButton);
        addToMaskRadioButton.setText("add to mask");
        addToMaskRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                addToMaskRadioButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        maskPanel.add(addToMaskRadioButton, new java.awt.GridBagConstraints());

        maskButtonGroup.add(replaceMaskRadioButton);
        replaceMaskRadioButton.setText("replace mask");
        replaceMaskRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                replaceMaskRadioButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        maskPanel.add(replaceMaskRadioButton, new java.awt.GridBagConstraints());

        maskComponentSelector.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maskComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        maskPanel.add(maskComponentSelector, gridBagConstraints);

        maskRangeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maskRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        maskPanel.add(maskRangeSlider, gridBagConstraints);

        recomputeMinMaxBox.setText("recompute min/max for data arrays");
        recomputeMinMaxBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recomputeMinMaxBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        maskPanel.add(recomputeMinMaxBox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        maskPanel.add(filler1, gridBagConstraints);

        functionPane.addTab("mask", maskPanel);

        coordsPanel.setLayout(new java.awt.BorderLayout());

        useCoordsCB.setText("set coordinates from data");
        useCoordsCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useCoordsCBActionPerformed(evt);
            }
        });
        coordsPanel.add(useCoordsCB, java.awt.BorderLayout.NORTH);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        indexComponentCB.setText("add index component");
        indexComponentCB.setToolTipText("adds integer component (node index)");
        indexComponentCB.setEnabled(false);
        indexComponentCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                indexComponentCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel3.add(indexComponentCB, gridBagConstraints);

        xPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("x coordinate component"));
        xPanel.setLayout(new java.awt.GridBagLayout());

        xComponentSelector.setAddNullComponent(false);
        xComponentSelector.setEnabled(false);
        xComponentSelector.setScalarComponentsOnly(true);
        xComponentSelector.setTitle("");
        xComponentSelector.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                xComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        xPanel.add(xComponentSelector, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(xPanel, gridBagConstraints);

        yPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("y coordinate component"));
        yPanel.setLayout(new java.awt.GridBagLayout());

        yComponentSelector.setAddNullComponent(false);
        yComponentSelector.setEnabled(false);
        yComponentSelector.setScalarComponentsOnly(true);
        yComponentSelector.setTitle("");
        yComponentSelector.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                yComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        yPanel.add(yComponentSelector, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(yPanel, gridBagConstraints);

        zPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("z coordinate component"));
        zPanel.setLayout(new java.awt.GridBagLayout());

        zComponentSelector.setAddNullComponent(false);
        zComponentSelector.setEnabled(false);
        zComponentSelector.setScalarComponentsOnly(true);
        zComponentSelector.setTitle("");
        zComponentSelector.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                zComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        zPanel.add(zComponentSelector, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(zPanel, gridBagConstraints);

        jLabel3.setText("y variable shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel3, gridBagConstraints);

        jLabel4.setText("x scale");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel4, gridBagConstraints);

        jLabel5.setText("x coord shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel5, gridBagConstraints);

        xScaleField.setEnabled(false);
        xScaleField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xScaleFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(xScaleField, gridBagConstraints);

        xShiftField.setText("0");
        xShiftField.setEnabled(false);
        xShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(xShiftField, gridBagConstraints);

        yScaleField.setEnabled(false);
        yScaleField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yScaleFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(yScaleField, gridBagConstraints);

        jLabel6.setText("y coord shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel6, gridBagConstraints);

        jLabel7.setText("z variable shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel7, gridBagConstraints);

        jLabel8.setText("z coord shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel8, gridBagConstraints);

        yShiftField.setText("0");
        yShiftField.setEnabled(false);
        yShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(yShiftField, gridBagConstraints);

        zScaleField.setEnabled(false);
        zScaleField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zScaleFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(zScaleField, gridBagConstraints);

        zShiftField.setText("0");
        zShiftField.setEnabled(false);
        zShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(zShiftField, gridBagConstraints);

        jLabel9.setText("x variable shift");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel9, gridBagConstraints);

        xVarShiftField.setText("0");
        xVarShiftField.setEnabled(false);
        xVarShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xVarShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(xVarShiftField, gridBagConstraints);

        jLabel10.setText("y scale");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel10, gridBagConstraints);

        yVarShiftField.setText("0");
        yVarShiftField.setEnabled(false);
        yVarShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yVarShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(yVarShiftField, gridBagConstraints);

        jLabel11.setText("z scale");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jLabel11, gridBagConstraints);

        zVarShiftField.setText("0");
        zVarShiftField.setEnabled(false);
        zVarShiftField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zVarShiftFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(zVarShiftField, gridBagConstraints);

        jScrollPane6.setViewportView(jPanel3);

        coordsPanel.add(jScrollPane6, java.awt.BorderLayout.CENTER);

        functionPane.addTab("coords", coordsPanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        mainPanel.add(functionPane, gridBagConstraints);

        runButton1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButton1UserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        mainPanel.add(runButton1, gridBagConstraints);

        add(mainPanel, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void indexComponentCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_indexComponentCBActionPerformed
        parameters.set(ADD_INDEX_COMPONENT, indexComponentCB.isSelected());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_indexComponentCBActionPerformed

    private void xComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_xComponentSelectorStateChanged
        parameters.set(XCOORD_COMPONENT, xComponentSelector.getComponent());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_xComponentSelectorStateChanged

    private void yComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_yComponentSelectorStateChanged
        parameters.set(YCOORD_COMPONENT, yComponentSelector.getComponent());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_yComponentSelectorStateChanged

    private void zComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_zComponentSelectorStateChanged
        parameters.set(ZCOORD_COMPONENT, zComponentSelector.getComponent());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_zComponentSelectorStateChanged

    private void useCoordsCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useCoordsCBActionPerformed
        parameters.setParameterActive(false);
        if (useCoordsCB.isSelected()) {
            xComponentSelector.setEnabled(true);
            xVarShiftField.setEnabled(true);
            xScaleField.setEnabled(true);
            xShiftField.setEnabled(true);
            yComponentSelector.setEnabled(true);
            yVarShiftField.setEnabled(true);
            yScaleField.setEnabled(true);
            yShiftField.setEnabled(true);
            zComponentSelector.setEnabled(true);
            zVarShiftField.setEnabled(true);
            zScaleField.setEnabled(true);
            zShiftField.setEnabled(true);
            indexComponentCB.setEnabled(true);

            String[] extraNames = null;
            int[] extraIndices = null;
            if (!parameters.get(META_IS_REGULAR_FIELD)) {
                complexCombinePanel.setVisible(false);
                complexSplitPanel.setVisible(false);
                extraNames = new String[]{"x", "y", "z", "0"};
                extraIndices = new int[]{-10, -11, -12, -100};
            } else {
                int nDims = parameters.get(NDIMS);
                switch (nDims) {
                    case 1:
                        extraNames = new String[]{"x", "y", "z", "i", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -100};
                        break;
                    case 2:
                        extraNames = new String[]{"x", "y", "z", "i", "j", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -2, -100};
                        break;
                    default:
                        extraNames = new String[]{"x", "y", "z", "i", "j", "k", "0"};
                        extraIndices = new int[]{-10, -11, -12, -1, -2, -3, -100};
                        break;
                }

            }

            xComponentSelector.addExtraItems(extraNames, extraIndices);
            xComponentSelector.setDataSchema(parameters.get(META_SCHEMA));
            xComponentSelector.setComponent(parameters.get(XCOORD_COMPONENT));

            yComponentSelector.addExtraItems(extraNames, extraIndices);
            yComponentSelector.setDataSchema(parameters.get(META_SCHEMA));
            yComponentSelector.setComponent(parameters.get(YCOORD_COMPONENT));

            zComponentSelector.addExtraItems(extraNames, extraIndices);
            zComponentSelector.setDataSchema(parameters.get(META_SCHEMA));
            zComponentSelector.setComponent(parameters.get(ZCOORD_COMPONENT));

            indexComponentCB.setSelected(parameters.get(ADD_INDEX_COMPONENT));
        } else {
            xComponentSelector.setEnabled(false);
            xVarShiftField.setEnabled(false);
            xScaleField.setEnabled(false);
            xShiftField.setEnabled(false);
            yComponentSelector.setEnabled(false);
            yVarShiftField.setEnabled(false);
            yScaleField.setEnabled(false);
            yShiftField.setEnabled(false);
            zComponentSelector.setEnabled(false);
            zVarShiftField.setEnabled(false);
            zScaleField.setEnabled(false);
            zShiftField.setEnabled(false);
            indexComponentCB.setEnabled(false);
        }
        parameters.setParameterActive(true);
        parameters.set(USE_COORDS, useCoordsCB.isSelected());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_useCoordsCBActionPerformed

    private void maskComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_maskComponentSelectorStateChanged
    {//GEN-HEADEREND:event_maskComponentSelectorStateChanged
        if (maskComponentSelector.getComponent() < 0) {
            runButton1.setPendingIfNoAuto();
            return;
        }
        int i = maskComponentSelector.getComponent();
        int ncomponents = parameters.get(META_COMPONENT_NAMES).length;
        float[][] ext = parameters.get(META_EXTENDS);

        if (i < ncomponents) {
            DataContainerSchema schema = parameters.get(META_SCHEMA);
            maskRangeSlider.setMinMax((float) schema.getComponentSchema(i).getPreferredPhysMinValue(), (float) schema.getComponentSchema(i).getPreferredPhysMaxValue());
        } else if (i == ncomponents) { //x coordinate
            maskRangeSlider.setMinMax(ext[0][0], ext[1][0]);
        } else if (i == ncomponents + 1) { //y coordinate
            maskRangeSlider.setMinMax(ext[0][1], ext[1][1]);
        } else if (i == ncomponents + 2) { //z coordinate
            maskRangeSlider.setMinMax(ext[0][2], ext[1][2]);
        }
        parameters.set(MASK_COMPONENT, maskComponentSelector.getComponent());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_maskComponentSelectorStateChanged

    private void recomputeMinMaxBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_recomputeMinMaxBoxActionPerformed
    {//GEN-HEADEREND:event_recomputeMinMaxBoxActionPerformed
        parameters.set(RECOMPUTE_MIN_MAX, recomputeMinMaxBox.isSelected());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_recomputeMinMaxBoxActionPerformed

    private void runButton1UserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButton1UserChangeAction
    {//GEN-HEADEREND:event_runButton1UserChangeAction
        updateParameters();
        parameters.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_runButton1UserChangeAction

    private void maskRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_maskRangeSliderStateChanged
    {//GEN-HEADEREND:event_maskRangeSliderStateChanged
        if (!maskRangeSlider.isAdjusting()) {
            parameters.set(MASK_MIN_MAX, new float[]{maskRangeSlider.getMin(), maskRangeSlider.getMax()}, MASK_LOW_UP, new float[]{maskRangeSlider.getLow(), maskRangeSlider.getUp()});
            runButton1.setPendingIfNoAuto();
        }
    }//GEN-LAST:event_maskRangeSliderStateChanged

    private void xVarShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_xVarShiftFieldActionPerformed
    {//GEN-HEADEREND:event_xVarShiftFieldActionPerformed
        parameters.set(XVAR_SHIFT, xVarShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_xVarShiftFieldActionPerformed

    private void force3DBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_force3DBoxActionPerformed
    {//GEN-HEADEREND:event_force3DBoxActionPerformed
        parameters.set(FIX3D, force3DBox.isSelected());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_force3DBoxActionPerformed

    private void replaceMaskRadioButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_replaceMaskRadioButtonUserChangeAction
    {//GEN-HEADEREND:event_replaceMaskRadioButtonUserChangeAction
        parameters.set(ADD_TO_MASK, false);
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_replaceMaskRadioButtonUserChangeAction

    private void addToMaskRadioButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_addToMaskRadioButtonUserChangeAction
    {//GEN-HEADEREND:event_addToMaskRadioButtonUserChangeAction
        parameters.set(ADD_TO_MASK, true);
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_addToMaskRadioButtonUserChangeAction

    private void xScaleFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_xScaleFieldActionPerformed
    {//GEN-HEADEREND:event_xScaleFieldActionPerformed
        parameters.set(XCOORD_SCALE_VALUE, xScaleField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_xScaleFieldActionPerformed

    private void yScaleFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_yScaleFieldActionPerformed
    {//GEN-HEADEREND:event_yScaleFieldActionPerformed
        parameters.set(YCOORD_SCALE_VALUE, yScaleField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_yScaleFieldActionPerformed

    private void zScaleFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_zScaleFieldActionPerformed
    {//GEN-HEADEREND:event_zScaleFieldActionPerformed
        parameters.set(ZCOORD_SCALE_VALUE, zScaleField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_zScaleFieldActionPerformed

    private void yVarShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_yVarShiftFieldActionPerformed
    {//GEN-HEADEREND:event_yVarShiftFieldActionPerformed
        parameters.set(YVAR_SHIFT, yVarShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_yVarShiftFieldActionPerformed

    private void zVarShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_zVarShiftFieldActionPerformed
    {//GEN-HEADEREND:event_zVarShiftFieldActionPerformed
        parameters.set(ZVAR_SHIFT, zVarShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_zVarShiftFieldActionPerformed

    private void xShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_xShiftFieldActionPerformed
    {//GEN-HEADEREND:event_xShiftFieldActionPerformed
        parameters.set(XCOORD_SHIFT, xShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_xShiftFieldActionPerformed

    private void yShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_yShiftFieldActionPerformed
    {//GEN-HEADEREND:event_yShiftFieldActionPerformed
        parameters.set(YCOORD_SHIFT, yShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_yShiftFieldActionPerformed

    private void zShiftFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_zShiftFieldActionPerformed
    {//GEN-HEADEREND:event_zShiftFieldActionPerformed
        parameters.set(ZCOORD_SHIFT, zShiftField.getValue());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_zShiftFieldActionPerformed

    public void setUpComponentColumn(SteppedComboBox comboBox,
                                     TableColumn componentColumn)
    {
        componentColumn.setCellEditor(new DefaultCellEditor(comboBox));
        DefaultTableCellRenderer renderer
            = new DefaultTableCellRenderer();
        componentColumn.setCellRenderer(renderer);
    }

    private void updateParameters()
    {
        parameters.setParameterActive(false);
        int[] actions = new int[nComps];
        boolean[] retain = new boolean[nComps];
        double[] clampMin = new double[nComps];
        double[] clampMax = new double[nComps];
        for (int i = 0; i < nComps; i++) {
            String s = (String) (actionTable.getValueAt(i, 1));
            for (int j = 0; j < ComponentOperationsShared.actionNames.length; j++)
                if (s.equalsIgnoreCase(ComponentOperationsShared.actionNames[j])) {
                    actions[i] = ComponentOperationsShared.actionCodes[j];
                    break;
                }
            retain[i] = (Boolean) (actionTable.getValueAt(i, 2));
            clampMin[i] = (Double) actionTable.getValueAt(i, 3);
            clampMax[i] = (Double) actionTable.getValueAt(i, 4);
        }
        parameters.set(ACTIONS, actions);
        parameters.set(RETAIN, retain);
        parameters.set(MIN, clampMin);
        parameters.set(MAX, clampMax);

        Vector<VectorComponent> vc = new Vector();

        for (int i = 0; i < createVectorsTable.getModel().getRowCount(); i++) {
            String name = (String) createVectorsTable.getValueAt(i, 0);
            if (name == null || name.length() < 1)
                continue;
            int[] components = new int[3];
            for (int j = 0; j < 3; j++) {
                int k = compNames.indexOf((String) createVectorsTable.getValueAt(i, j + 1));
                if (k < 0)
                    components[j] = -1;
                else
                    components[j] = compIndices.get(k);
            }
            boolean norm = false;
            String[] componentNames = new String[3];
            for (int j = 0; j < 3; j++) 
                componentNames[j] = (String) createVectorsTable.getValueAt(i, j + 1);
            if (createVectorsTable.getValueAt(i, 4) != null)
                norm = (Boolean) createVectorsTable.getValueAt(i, 4);
            vc.add(new VectorComponent(name, components, componentNames, norm));
        }
        parameters.set(VECTOR_COMPONENTS, vc);
        if (vCompNames.size() == 0)
            parameters.set(VCNORMS, null);
        else {
            boolean[] vCN = new boolean[vCompNames.size()];
            boolean[] vCNormalize = new boolean[vCompNames.size()];
            boolean[] vCS = new boolean[vCompNames.size()];
            for (int i = 0; i < vCN.length; i++) {
                vCN[i] = (Boolean) vectorOperationsTable.getValueAt(i, 1);
                vCNormalize[i] = (Boolean) vectorOperationsTable.getValueAt(i, 2);
                vCS[i] = (Boolean) vectorOperationsTable.getValueAt(i, 3);
            }
            parameters.set(VCNORMS, vCN);
            parameters.set(VCNORMALIZE, vCNormalize);
            parameters.set(VCSPLIT, vCS);
        }
        parameters.set(FIX3D, force3DBox.isSelected());

        Vector<ComplexComponent> vcc = new Vector();
        if (nonComplexCompNames.size() > 0) {
            for (int i = 0; i < complexCombineTable.getModel().getRowCount(); i++) {
                String name = (String) complexCombineTable.getValueAt(i, 0);
                if (name == null || name.length() < 1)
                    continue;
                int[] components = new int[2];
                for (int j = 0; j < 2; j++) {
                    int k = nonComplexCompNames.indexOf((String) complexCombineTable.getValueAt(i, j + 1));
                    if (k < 0)
                        components[j] = -1;
                    else
                        components[j] = nonComplexCompIndices.get(k);
                }
                vcc.add(new ComplexComponent(name, components[0], components[1]));
            }
            parameters.set(COMPLEX_COMBINE_COMPONENTS, vcc);
        }

        if (complexCompNames.size() > 0) {
            boolean[] splitRe = new boolean[complexCompNames.size()];
            boolean[] splitIm = new boolean[complexCompNames.size()];
            boolean[] splitAbs = new boolean[complexCompNames.size()];
            boolean[] splitArg = new boolean[complexCompNames.size()];

            for (int i = 0; i < complexCompNames.size(); i++) {
                splitRe[i] = (Boolean) (complexSplitTable.getValueAt(i, 1));
                splitIm[i] = (Boolean) (complexSplitTable.getValueAt(i, 2));
                splitAbs[i] = (Boolean) (complexSplitTable.getValueAt(i, 3));
                splitArg[i] = (Boolean) (complexSplitTable.getValueAt(i, 4));
            }
            parameters.set(COMPLEX_SPLIT_RE, splitRe);
            parameters.set(COMPLEX_SPLIT_IM, splitIm);
            parameters.set(COMPLEX_SPLIT_ABS, splitAbs);
            parameters.set(COMPLEX_SPLIT_ARG, splitArg);
        } else {
            parameters.set(COMPLEX_SPLIT_RE, new boolean[]{});
            parameters.set(COMPLEX_SPLIT_IM, new boolean[]{});
            parameters.set(COMPLEX_SPLIT_ABS, new boolean[]{});
            parameters.set(COMPLEX_SPLIT_ARG, new boolean[]{});
        }

        parameters.set(XCOORD_COMPONENT, xComponentSelector.getComponent());
        parameters.set(YCOORD_COMPONENT, yComponentSelector.getComponent());
        parameters.set(ZCOORD_COMPONENT, zComponentSelector.getComponent());

        parameters.set(XVAR_SHIFT, xVarShiftField.getValue());
        parameters.set(YVAR_SHIFT, yVarShiftField.getValue());
        parameters.set(ZVAR_SHIFT, zVarShiftField.getValue());

        parameters.set(XCOORD_SCALE_VALUE, xScaleField.getValue());
        parameters.set(YCOORD_SCALE_VALUE, yScaleField.getValue());
        parameters.set(ZCOORD_SCALE_VALUE, zScaleField.getValue());

        parameters.set(XCOORD_SHIFT, xShiftField.getValue());
        parameters.set(YCOORD_SHIFT, yShiftField.getValue());
        parameters.set(ZCOORD_SHIFT, zShiftField.getValue());

        parameters.setParameterActive(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable actionTable;
    private org.visnow.vn.gui.swingwrappers.RadioButton addToMaskRadioButton;
    private javax.swing.JPanel complexCombinePanel;
    private javax.swing.JTable complexCombineTable;
    private javax.swing.JPanel complexPanel;
    private javax.swing.JPanel complexSplitPanel;
    private javax.swing.JTable complexSplitTable;
    private javax.swing.JPanel componentselectorPanel;
    private javax.swing.JPanel coordsPanel;
    private javax.swing.JTable createVectorsTable;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JCheckBox force3DBox;
    private javax.swing.JTabbedPane functionPane;
    private javax.swing.JTabbedPane functionPane1;
    private javax.swing.JCheckBox indexComponentCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JPanel mainPanel;
    private javax.swing.ButtonGroup maskButtonGroup;
    private org.visnow.vn.lib.gui.DataComponentSelector maskComponentSelector;
    private javax.swing.JPanel maskPanel;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider maskRangeSlider;
    private javax.swing.JCheckBox recomputeMinMaxBox;
    private org.visnow.vn.gui.swingwrappers.RadioButton replaceMaskRadioButton;
    private org.visnow.vn.gui.widgets.RunButton runButton1;
    private javax.swing.JCheckBox useCoordsCB;
    private javax.swing.JTable vectorOperationsTable;
    private javax.swing.JPanel vectorPanel;
    private org.visnow.vn.lib.gui.DataComponentSelector xComponentSelector;
    private javax.swing.JPanel xPanel;
    private org.visnow.vn.gui.components.FloatFormattedTextField xScaleField;
    private org.visnow.vn.gui.components.FloatFormattedTextField xShiftField;
    private org.visnow.vn.gui.components.FloatFormattedTextField xVarShiftField;
    private org.visnow.vn.lib.gui.DataComponentSelector yComponentSelector;
    private javax.swing.JPanel yPanel;
    private org.visnow.vn.gui.components.FloatFormattedTextField yScaleField;
    private org.visnow.vn.gui.components.FloatFormattedTextField yShiftField;
    private org.visnow.vn.gui.components.FloatFormattedTextField yVarShiftField;
    private org.visnow.vn.lib.gui.DataComponentSelector zComponentSelector;
    private javax.swing.JPanel zPanel;
    private org.visnow.vn.gui.components.FloatFormattedTextField zScaleField;
    private org.visnow.vn.gui.components.FloatFormattedTextField zShiftField;
    private org.visnow.vn.gui.components.FloatFormattedTextField zVarShiftField;
    // End of variables declaration//GEN-END:variables
}
