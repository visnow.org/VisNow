/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.DifferentialOperations;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton.RunState;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class DifferentialOperationsShared
{
    public enum ScalarOperation
    {
        GRADIENT, GRADIENT_NORM, GRADIENT_COMPONENTS, NORMALIZED_GRADIENT, 
        LAPLACIAN, HESSIAN, HESSIAN_EIGEN
    };

    public enum VectorOperation
    {
        DERIV, DERIV_COMPONENTS, DIV, ROT
    };
    

    public enum TimeOperation
    {
        D_DT, D2_DT2
    };
    
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //Specification:
    //
    //not null array of arrays
    //length == META_SCALAR_COMPONENT_NAMES.length
    //each element is not null array of unique values
    static final ParameterName<ScalarOperation[][]> SCALAR_OPERATIONS = 
            new ParameterName<>("Operations for every scalar component");
    //not null array of arrays
    //length == META_VECTOR_COMPONENT_NAMES.length
    //each element is not null array of unique values
    static final ParameterName<VectorOperation[][]> VECTOR_OPERATIONS = 
            new ParameterName<>("Operations for every vector component");
    //not null array of arrays
    //length == META_VECTOR_COMPONENT_NAMES.length
    //each element is not null array of unique values
    static final ParameterName<TimeOperation[][]> TIME_OPERATIONS = 
            new ParameterName<>("Time derivatives of components");

    static final ParameterName<RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    
    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //not null array of Strings, may be empty
    static final ParameterName<String[]> META_SCALAR_COMPONENT_NAMES = new ParameterName("META scalar component names");
    //not null array of Strings, may be empty
    static final ParameterName<String[]> META_VECTOR_COMPONENT_NAMES = new ParameterName("META vector component names");
    //not null array of Strings, may be empty
    static final ParameterName<String[]> META_TIME_COMPONENT_NAMES = new ParameterName("META time component names");
    static final ParameterName<Boolean> COMPUTE_FULLY = new ParameterName("compute all timesteps");  
}
