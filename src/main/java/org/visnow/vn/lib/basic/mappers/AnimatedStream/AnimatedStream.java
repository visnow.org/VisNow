/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.AnimatedStream;

import java.awt.Color;
import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.LineStripArray;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class AnimatedStream  extends VisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    private OpenBranchGroup outGroup = new OpenBranchGroup();
    
    private Params params ;
    
    private Field inField = null;
    private ComponentColorMap colorMap;
    
    private int nFrames = 1;
    private int segmentLength = 2;
    private int frame = 0;
    private int nStreamlines = 0;
    private int nSegments = 1;
    private int segSkip = 1;
    private int dir = 0;
    private boolean animating = false;
    private boolean renderDone = true;
    private boolean fromGUI = false;
    private boolean ignoreUI = false;
    private int outNNodes;
    
    private float[] inCoordVals;
    private float[] outCoords;
    
    private float[] inColorVals;
    int vlen = 1;
    private float minV = 0, dv = 1;
    private byte[] cMap;
    private float[] bgr = new float[3];
    private byte[] outColors = null;
    private float lineWidth = 1;
    private Color lastBgrColor = Color.BLACK;
    
    private LineStripArray lineStrips = null;
    private OpenShape3D lineShape = new OpenShape3D();
    private OpenAppearance appearance = new OpenAppearance();
    private OpenLineAttributes lineAttributes = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);
    
    private GUI computeGUI = null;
    
    public AnimatedStream()
    {
        parameters = params = new Params();
        colorMap = params.getMap();
        colorMap.getComponentRange().setAddNull(true);
        colorMap.addRenderEventListener(new RenderEventListener() {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                updateColoring();
            }
            
        });
        outObj.setName("animated streamlines");
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (ignoreUI)
                    return;
                fromGUI = true;
                if (params.getAnimate() != Params.STOP) {
                    dir = params.getAnimate();
                    new Thread(new Animation()).start();
                } else {
                    if (params.getLineWidth() != lineWidth)
                    {
                        lineWidth = params.getLineWidth();
                        lineAttributes.setLineWidth(lineWidth);
                    }
                    createOutput();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeGUI = new GUI();
            }
        });
        colorMap.addRenderEventListener(new RenderEventListener() {
            @Override
            public void renderExtentChanged(RenderEvent e) {
                if (outCoords != null)
                    updateColoring();
            }
        });
        computeGUI.setParams(params);
        setPanel(computeGUI);
    }

    private FrameRenderedListener frameRenderedListener = new FrameRenderedListener()
    {
        @Override
        public void frameRendered(FrameRenderedEvent e)
        {
            renderDone = true;
        }
    };
    
    @Override
    public ColorListener getBackgroundColorListener()
    {
        return new ColorListener(){
            @Override
            public void colorChoosen(ColorEvent e) {
                bgrColor = e.getSelectedColor();
                if (bgrColor.equals(lastBgrColor))
                    return;
//                System.out.println("bgr changed");
                bgr = bgrColor.getRGBColorComponents(bgr);
                lastBgrColor = bgrColor;
                if (outCoords != null)
                    updateColoring();
            }
            
        };
    }

    private class Animation implements Runnable
    {

        @Override
        public synchronized void run()
        {
            if (animating)
                return;
            animating = true;
            while (params.getAnimate() != Params.STOP) {
                renderDone = false;
                frame = (frame + dir) % segSkip;
                update();
                Thread.yield();
                try {
                    while (!renderDone) {
                        wait(10);
                    }
                    wait(params.getDelay() + 1);
                } catch (InterruptedException c) {
                    System.out.println("interrupt");
                }
            }
            animating = false;
        }
    }
    
    @Override
    public void onInitFinishedLocal()
    {
        outObj.setCreator(this);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }


    private void updateValues()
    {
        Color3f defaultColor = colorMap.getDefaultColor();
        float[] c = new float[3];
        defaultColor.get(c);
        byte[] defColor = new byte[]{(byte)(0xff & (int)(c[0] * 255)), 
                                     (byte)(0xff & (int)(c[1] * 255)), 
                                     (byte)(0xff & (int)(c[2] * 255))};
        for (int iStreamline = 0, l = 0; iStreamline < nStreamlines; iStreamline++) 
            for (int iSegment = 0; iSegment < nSegments; iSegment++) {
                int m = iStreamline + nStreamlines * (frame + iSegment * segSkip - segmentLength);
                for (int i = 0; i <= segmentLength; i++, m += nStreamlines, l += 3) {
                    int mm = m;
                    if (mm < 0)
                        mm = iStreamline;
                    if (m < 0)
                        System.arraycopy(inCoordVals, 3 * mm, outCoords, l, 3);
                    else
                        System.arraycopy(inCoordVals, 3 * mm, outCoords, l, 3);
                    if (inColorVals != null) {
                        float v = 0;
                        if (vlen == 1)
                            v = inColorVals[mm];
                        else {
                            for (int j = 0; j < vlen; j++) 
                                v += inColorVals[vlen * mm + 1] * inColorVals[vlen * mm + 1];
                            v = (float)Math.sqrt(v);
                        }
                        int cmapInd = (int)((v - minV) * dv);
                        if (cmapInd < 0)    cmapInd = 0; 
                        if (cmapInd >= ColorMapManager.SAMPLING_TABLE)    
                            cmapInd =  ColorMapManager.SAMPLING_TABLE - 1; 
                        if (params.isCometEffect())
                            for (int j = 0; j < 3; j++) 
                                outColors[l + j] = (byte)(0xff & 
                                        (i * (0xff & cMap[3 * cmapInd + j])) / segmentLength);
                        else
                            System.arraycopy(cMap, 3 * cmapInd, outColors, l, 3);
                    }
                    else {
                        if (params.isCometEffect()) {
                            float t = i / (float)segmentLength;
                            for (int j = 0; j < 3; j++) 
                                outColors[l + j] = (byte)(0xff & (int)(255 * (t * c[j] + (1 -t) * bgr[j])));
                        }
                        else
                            System.arraycopy(defColor, 0, outColors, l, 3);
                        
                    }
                }
            }
    }

    private void update()
    {
        updateValues();
        lineStrips.setCoordRefFloat(outCoords);
        lineStrips.setColorRefByte(outColors);
    }
    
    private void updateColoring()
    {
        DataArray mapArray = inField.getComponent(colorMap.getDataComponentName());
        if (mapArray != null) {
            inColorVals = mapArray.getRawArray().getFloatData();
            vlen = mapArray.getVectorLength();
            minV = colorMap.getDataMin();
            cMap = colorMap.getRGBByteColorTable();
            dv = ColorMapManager.SAMPLING_TABLE / (colorMap.getDataMax() - minV);
        }
        else
            inColorVals = null;
        lineAttributes.setLineWidth(lineWidth);
        update();
    }
    
    private void createOutput()
    {
        segmentLength = params.getSegmentLength() - 1;
        segSkip = segmentLength + params.getGap();
        nSegments = nFrames / segSkip;
        outNNodes = nStreamlines * (segmentLength + 1) * nSegments;
        frame = 0;   
        inCoordVals = inField.getCurrentCoords().getData();
        outCoords = new float[3 * outNNodes];
        outColors =new byte[3 * outNNodes];
        int[] lineLengths = new int[nStreamlines * nSegments];
        Arrays.fill(lineLengths, segmentLength + 1);
        outObj.clearAllGeometry();
        lineShape.removeAllGeometries();
        lineStrips = new LineStripArray(outNNodes,
                                        GeometryArray.COORDINATES | GeometryArray.COLOR_3 | GeometryArray.BY_REFERENCE,
                                        lineLengths);    
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_READ);
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_WRITE);
        lineStrips.setCoordRefFloat(outCoords);
        lineStrips.setColorRefByte(outColors);
        params.getMap().setContainerSchema(inField.getSchema());
        updateColoring();
        lineShape.addGeometry(lineStrips);
        appearance.setLineAttributes(lineAttributes);
        lineShape.setAppearance(appearance);
        outGroup.removeAllChildren();
        outGroup.addChild(lineShape);
        outObj.addNode(outGroup);
    }
    
    @Override
    public void onActive()
    {
        if (!fromGUI) {
            if (getInputFirstValue("inField") == null)
                return;
            ignoreUI = true;
            VNField input = ((VNField) getInputFirstValue("inField"));
            if (inField != input.getField()) {
                Field in = input.getField();
                if (in == null){
                        computeGUI.setInText("<html>Empty input field");
                        computeGUI.setNFrames(0);
                        return;
                    }
                if (in.getUserData() != null && 
                    in.getUserData().length > 0 && 
                    in.getUserData()[0].startsWith("streamlines field: ")) {
                    inField = in;
                    computeGUI.setInText("");
                    String[] items = in.getUserData()[0].split(" +");
                    nStreamlines = Integer.parseInt(items[2]);
                    nFrames = (int)(in.getNNodes() / nStreamlines);
                    inField = in;
                } else {
                    computeGUI.setInText("<html>Bad input field:<p>must have \"streamlines field:\" annotation</html>");
                    computeGUI.setNFrames(0);
                    return;
                }
                computeGUI.setNFrames(nFrames);
                outObj.setExtents(inField.getPreferredExtents());
            }
            ignoreUI = false;
        }
        if (inField == null)
            return;
        createOutput();
        
        fromGUI = false;
    }

    public FrameRenderedListener getFrameRenderedListener()
    {
        return frameRenderedListener;
    }
}
