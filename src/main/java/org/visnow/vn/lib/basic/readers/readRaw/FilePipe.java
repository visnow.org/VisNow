/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.readRaw;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.imageio.stream.FileImageInputStream;

/**
 * Utility class for serving multiple files as single stream with additional options:
 * - auto skip head/tail for each file
 * - reading only particular number of bytes from each file (slice mode).
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
class FilePipe
{
    final private String[] filenames;
    final private ByteOrder byteOrder;
    final private long inFileAlignment;
    final private boolean asSlices; //as slices or as stream
    final private long singleSliceByteLength;

    private int currentFileNameIndex;
    private FileImageInputStream currentInputStream;
    private long currentFileBytesRead;
    private long currentFileEndAfterAlignmentAndSlicing;
    private byte[] buffer = new byte[0];

    /**
     * Assuming that every file is big enough to fit inFileAlignment (and singleSliceByteLength - in slice mode)
     * <p>
     * @param asSlices              read only singleSliceByteLength from each file
     * @param singleSliceByteLength number of bytes to read from each file (only when asSlices == true)
     */
    public FilePipe(String[] filenames, ByteOrder byteOrder, long inFileAlignment, boolean asSlices, long singleSliceByteLength)
    {
        this.filenames = filenames;
        this.byteOrder = byteOrder;
        this.inFileAlignment = inFileAlignment;
        this.asSlices = asSlices;
        this.singleSliceByteLength = singleSliceByteLength;
    }

    private void nextFile() throws IOException
    {
        File file = new File(filenames[currentFileNameIndex++]);
        currentInputStream = new FileImageInputStream(file);
        long currentFileLength = file.length();
        if (inFileAlignment >= 0) {
            currentFileBytesRead = inFileAlignment;
            currentInputStream.skipBytes(currentFileBytesRead);
            if (asSlices) currentFileEndAfterAlignmentAndSlicing = singleSliceByteLength + inFileAlignment;
            else currentFileEndAfterAlignmentAndSlicing = currentFileLength;
        } else {
            if (asSlices) {
                currentFileBytesRead = currentFileLength - (-1 - inFileAlignment) - singleSliceByteLength;
                currentInputStream.skipBytes(currentFileBytesRead);
                currentFileEndAfterAlignmentAndSlicing = currentFileLength - (-1 - inFileAlignment);
            } else {
                currentFileEndAfterAlignmentAndSlicing = currentFileLength - (-1 - inFileAlignment);
                currentFileBytesRead = 0;
            }
        }
    }

    private void readBytes(int len) throws IOException
    {
        if (buffer.length < len) buffer = new byte[len];
        if (currentInputStream == null) nextFile();
        int read = 0;
        while (read < len) {
            long bytesLeft = currentFileEndAfterAlignmentAndSlicing - currentFileBytesRead;
            if (bytesLeft == 0) nextFile();
            int toRead = (int) Math.min(len - read, bytesLeft);
            currentInputStream.readFully(buffer, read, toRead);
            read += toRead;
            currentFileBytesRead += toRead;
        }
    }

    public void skipBytes(long len) throws IOException
    {
        if (currentInputStream == null) nextFile();
        long skipped = 0;
        while (skipped < len) {
            long bytesLeft = currentFileEndAfterAlignmentAndSlicing - currentFileBytesRead;
            if (bytesLeft == 0) nextFile();
            long toSkip = Math.min(len - skipped, bytesLeft);
            currentInputStream.skipBytes(toSkip);
            skipped += toSkip;
            currentFileBytesRead += toSkip;
        }
    }

    public void readFully(byte[] array, int off, int len) throws IOException
    {
        readBytes(len);
        System.arraycopy(buffer, 0, array, off, len);
    }

    public void readFully(short[] array, int off, int len) throws IOException
    {
        readBytes(len * 2);
        ByteBuffer.wrap(buffer).order(byteOrder).asShortBuffer().get(array, off, len);
    }

    public void readFully(int[] array, int off, int len) throws IOException
    {
        readBytes(len * 4);
        ByteBuffer.wrap(buffer).order(byteOrder).asIntBuffer().get(array, off, len);
    }

    public void readFully(float[] array, int off, int len) throws IOException
    {
        readBytes(len * 4);
        ByteBuffer.wrap(buffer).order(byteOrder).asFloatBuffer().get(array, off, len);
    }

    public void readFully(double[] array, int off, int len) throws IOException
    {
        readBytes(len * 8);
        ByteBuffer.wrap(buffer).order(byteOrder).asDoubleBuffer().get(array, off, len);
    }
}
