/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isosurface;

import org.visnow.vn.lib.utils.isosurface.IrregularFieldIsosurface;
import org.visnow.vn.lib.utils.isosurface.IsosurfaceEngine;
import org.visnow.vn.lib.utils.isosurface.IsosurfaceEngineParams;
import org.visnow.vn.lib.utils.isosurface.RegularFieldIsosurface;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.mappers.Isosurface.IsosurfaceShared.*;
import org.visnow.vn.lib.gui.DownsizeUIShared;
import org.visnow.vn.lib.gui.cropUI.CropUIShared;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import static org.visnow.vn.lib.utils.field.MergeTimesteps.*;
import org.visnow.vn.lib.utils.field.SmoothTriangulation;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Isosurface extends OutFieldVisualizationModule
{

    class SliceRemap implements org.visnow.vn.lib.utils.isosurface.RegularFieldIsosurface.SliceRemap
    {
        @Override
        public float[] remap(float[] slice, float val)
        {
            float[] remapped = new float[slice.length];
            for (int i = 0; i < slice.length; i++) {
                remapped[i] = slice[i] - val;
                if (remapped[i] == 0)
                    remapped[i] += .001f;
            }
            return remapped;
        }

    }

    private static final Logger LOGGER = Logger.getLogger(Isosurface.class);
    /**
     *
     * inField - a 3D field to create isosurface;
     * at least one scalar data component must be present.
     * <p>
     * outField - isosurface field will be created by update method -
     * can be void, can contain no node data (geometry only)
     *
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;
    protected IsosurfaceGUI computeUI = null;
    protected SmoothTriangulation smoother = new SmoothTriangulation();
    protected IsosurfaceEngine isosurfaceEngine;
    protected int stateStatus = 0;

    private int runQueue = 0;

    /**
     * Instantiates Isosurface module.
     */
    public Isosurface()
    {
        parameters.addParameterChangelistener((String name) -> {
            if (!parameters.isParameterActive())
                return;
            if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                runQueue++;
                startAction();
            } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                startAction();
        });

        parameters.get(THRESHOLDS).addChangeListener((e) -> {
            if (inField == null)
                return;
            float[] oldRange = parameters.get(META_TIME_RANGE);
            DataArray da = inField.getComponent(parameters.get(THRESHOLDS).getComponentName());
            if (da == null)
                return;
            float[] newRange = new float[] {da.getStartTime(), da.getEndTime()};
            if (newRange[0] != oldRange[0] || newRange[1] != oldRange[1]) {
                parameters.set(META_TIME_RANGE, newRange);
                if (computeUI != null) {
                    if(newRange[0] == newRange[1])
                        computeUI.updateTimeSlider(new float[]{newRange[0]}, parameters.get(TIME), true);
                    else
                        computeUI.updateTimeSlider(newRange, parameters.get(TIME), true);
                }
            }
            if (!parameters.isParameterActive())
                return;
            if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                startAction();
        });
        renderingParams.setDisplayMode(RenderingParams.SURFACE);
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new IsosurfaceGUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        outObj.setName("isosurface");
        smoother.addFloatValueModificationListener(
                new FloatValueModificationListener()
                {
                    @Override
                    public void floatValueChanged(FloatValueModificationEvent e)
                    {
                        setProgress(e.getVal() / 10 + .9f);
                    }
                });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        List<Parameter> defaultParameters = createDefaultParametersAsList();
        defaultParameters.addAll(CropUIShared.createDefaultParametersAsList());
        defaultParameters.addAll(DownsizeUIShared.createDefaultParametersAsList());
        return defaultParameters.toArray(new Parameter[defaultParameters.size()]);
    }

    //TODO: remove this (may be problem on many threads)
    protected FloatValueModificationListener progressListener = new FloatValueModificationListener()
    {

        @Override
        public void floatValueChanged(FloatValueModificationEvent e)
        {
            setProgress((stateStatus + e.getVal()) / parameters.get(THRESHOLDS).getValues().length);
        }
    };

    /**
     * Updates module state.
     *
     * @param parameterProxy ParameterProxy object
     * @param updateRenderingParams true if rendering parameters should be updated
     */
    public void update(ParameterProxy parameterProxy, boolean updateRenderingParams)
    {
        ParameterProxy p = parameterProxy;
        IrregularField tmpField = null;
        float[] thresholds = p.get(THRESHOLDS).getValues();
        int[] up = p.get(CropUIShared.HIGH);
        for (int j = 0; j < up.length; j++) up[j]++;

        IsosurfaceEngineParams engineParams
                = new IsosurfaceEngineParams(p.get(THRESHOLDS).getComponentIndex(), p.get(DownsizeUIShared.DOWNSIZE),
                                             p.get(CropUIShared.LOW), up, p.get(SMOOTHING_STEP_COUNT), p.get(SMOOTHING_STEP_COUNT) > 0,
                                             p.get(COMPUTE_UNCERTAINTY), p.get(CELLSET_SEPARATION), p.get(GRID), p.get(TIME));

        stateStatus = 0;
        for (int i = 0; i < thresholds.length; i++, stateStatus++) {

            IrregularField currentField
                    = isosurfaceEngine.makeIsosurface(engineParams, p.get(THRESHOLDS).getValues()[i]);
            if (currentField != null)
                tmpField = MergeIrregularField.merge(tmpField, currentField, i, p.get(CELLSET_SEPARATION));
        }
        if (tmpField == null) {
            outField = null;
            outIrregularField = null;
            setOutputValue("isosurfaceField", null);
            show();
        } else {
            if (p.get(SMOOTHING_STEP_COUNT) > 0) {
                smoother.setInField(tmpField);
                outField = tmpField.cloneShallow();
                outIrregularField = (IrregularField) outField;
                outIrregularField.setCoords(new FloatLargeArray(smoother.smoothCoords(p.get(SMOOTHING_STEP_COUNT), .5f)), 0);
                if (tmpField.getNormals() != null)
                    outIrregularField.setNormals(new FloatLargeArray(smoother.smoothNormals(p.get(SMOOTHING_STEP_COUNT), .5f)));
            } else {
                outField = tmpField;
                outIrregularField = (IrregularField) outField;
            }
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outIrregularField.setName(inField.getName());
            prepareOutputGeometry();

            float[] thrs = new float[thresholds.length];
            DataArraySchema componentSchema = p.get(THRESHOLDS).getComponentSchema();
            double[] coeff = componentSchema.getPhysicalMappingCoefficients();
            for (int i = 0; i < thrs.length; i++)
                thrs[i] = (float)((thresholds[i] - coeff[1]) / coeff[0]);
            irregularFieldGeometry.getColormapLegend(0).setThrTable(thrs, componentSchema.getName());
            if (updateRenderingParams) {
                renderingParams.setDisplayMode(RenderingParams.SURFACE);
            }
            show();
            setOutputValue("isosurfaceField", new VNIrregularField(outIrregularField));
        }
    }

    private void validateParamsAndSetSmart(boolean resetFully, boolean differentRegularDims)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE)
            parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_REGULAR_INPUT, inField instanceof RegularField);
        parameters.get(THRESHOLDS).setContainerSchema(inField.getSchema());
        float[] mergedTimeSteps =
                mergedTimeSteps(inField, new String[] {"coords",
                                                       "mask",
                                                       parameters.get(THRESHOLDS).getComponentName()});
        parameters.set(META_TIME_RANGE, new float[]{mergedTimeSteps[0],
                                                    mergedTimeSteps[mergedTimeSteps.length -1]});
        parameters.set(TIME, inField.getCurrentTime());
        //validate/smart thresholds

        boolean isRegular = inField instanceof RegularField;

        if (isRegular) {
            //validate/smart crop
            int[] previousDims = parameters.get(CropUIShared.META_FIELD_DIMENSION_LENGTHS);
            int[] dims = ((RegularField) inField).getDims();
            parameters.set(CropUIShared.META_FIELD_DIMENSION_LENGTHS, dims);
            if (resetFully)
                CropUIShared.resetLowHighRange(parameters);
            else if (differentRegularDims) {
                CropUIShared.rescaleAll(parameters, previousDims);
            }

            //validate/smart downsize
            if (resetFully || differentRegularDims) {
                int[] low = parameters.get(CropUIShared.LOW);
                int[] high = parameters.get(CropUIShared.HIGH);
                int[] croppedDims = new int[low.length];
                for (int i = 0; i < croppedDims.length; i++)
                    croppedDims[i] = high[i] - low[i] + 1;

                int[] smartDownsize = DownsizeUIShared.getSmartDefault(croppedDims);
                parameters.set(DownsizeUIShared.META_DEFAULT_DOWNSIZE, smartDownsize,
                               DownsizeUIShared.DOWNSIZE, smartDownsize);
            }
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            //1a. set distinction flags
            boolean isNewField = !isFromVNA() && newInField != inField;
            Parameters p = null;
            if (isNewField) {
                boolean isNonCompatibleField = !isFromVNA() &&
                        (inField == null ||
                        inField instanceof IrregularField && newInField instanceof RegularField ||
                        inField instanceof RegularField && newInField instanceof IrregularField);
                boolean isNewIrregularField = !isFromVNA() &&
                        inField instanceof IrregularField && newInField instanceof IrregularField && newInField != inField;
                boolean isDifferentRegularDims = !isFromVNA() &&
                        inField != null && inField instanceof RegularField && newInField instanceof RegularField &&
                        !Arrays.toString(((RegularField) inField).getDims()).equals(Arrays.toString(((RegularField) newInField).getDims()));
                inField = newInField;
                synchronized (parameters) {
                    validateParamsAndSetSmart(isNonCompatibleField || isNewIrregularField, isDifferentRegularDims);
                    //2b. clone param (local read-only copy)
                    p = parameters.getReadOnlyClone();
                    notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);
                }
            }

            //TODO: handle & test: inField.getNComponents() < 1
            //4. run computation and propagate
            p = parameters.getReadOnlyClone();
            if (p == null)
                return;
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                if (inField instanceof RegularField) {
                    isosurfaceEngine = new RegularFieldIsosurface((RegularField) inField);
                    ((RegularFieldIsosurface)isosurfaceEngine).setSliceRemap(new SliceRemap());
                    isosurfaceEngine.addFloatValueModificationListener(progressListener);
                } else {
                    isosurfaceEngine = new IrregularFieldIsosurface((IrregularField) inField);
                    isosurfaceEngine.addFloatValueModificationListener(progressListener);
                }
                update(parameters, isNewField);
            }
        }
    }

}
