/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentOperations;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class VectorComponent implements Cloneable
{

    String name = "";
    String[] scalarComponentNames = null;
    int[] scalarComponents = null;
    boolean computeNorm = false;

    public VectorComponent()
    {

    }
    
    public VectorComponent(String name, int[] scalarComponents, String[] scalarComponentNames, boolean computeNorm)
    {
        this.name = name;
        this.scalarComponents = scalarComponents;
        this.scalarComponentNames = scalarComponentNames;
        this.computeNorm = computeNorm;
    }

    public VectorComponent(String name, String[] scalarComponentNames, boolean computeNorm)
    {
        this(name, null, scalarComponentNames, computeNorm);
    }
    
    public VectorComponent(String name, int[] scalarComponents, boolean computeNorm)
    {
        this(name, scalarComponents, null, computeNorm);
    }

    public String getName()
    {
        return name;
    }

    public String[] getScalarComponentNames()
    {
        return scalarComponentNames;
    }

    public int[] getScalarComponents()
    {
        return scalarComponents;
    }

    public boolean isComputeNorm()
    {
        return computeNorm;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        super.clone();
        return new VectorComponent(name, scalarComponents.clone(), computeNorm);
    }

}
