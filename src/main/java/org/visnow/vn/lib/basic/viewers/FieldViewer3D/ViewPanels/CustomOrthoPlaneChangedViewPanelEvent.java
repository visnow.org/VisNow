/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels;

/**
 *
 * @author vis
 */
public class CustomOrthoPlaneChangedViewPanelEvent extends ViewPanelEvent
{

    private int axis = 0;
    private float[] point = new float[3];
    private float[][] vectors = new float[3][3];
    private float[][][] base = new float[3][2][3];

    public CustomOrthoPlaneChangedViewPanelEvent(Object source, int axis, float[] point, float[][] vectors, float[][][] base)
    {
        super(source);
        this.axis = axis;
        for (int i = 0; i < 3; i++) {
            this.point[i] = point[i];
            for (int j = 0; j < 3; j++) {
                this.vectors[i][j] = vectors[i][j];
                for (int k = 0; k < 2; k++) {
                    this.base[i][k][j] = base[i][k][j];
                }
            }
        }
    }

    /**
     * @return the point
     */
    public float[] getPoint()
    {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(float[] point)
    {
        for (int i = 0; i < 3; i++) {
            this.point[i] = point[i];
        }
    }

    /**
     * @return the vector
     */
    public float[][] getVectors()
    {
        return vectors;
    }

    /**
     * @param vectors the vector to set
     */
    public void setVectors(float[][] vectors)
    {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.vectors[i][j] = vectors[i][j];
            }
        }
    }
    
    /**
     * @return the base 
     */
    public float[][][] getBase()
    {
        return base;
    }
    
    /**
     * @return the axis
     */
    public int getAxis()
    {
        return axis;
    }

    /**
     * @param axis the axis to set
     */
    public void setAxis(int axis)
    {
        this.axis = axis;
    }

}
