/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.InterpolatedSlice;

import java.util.Arrays;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;

import org.visnow.vn.geometries.objects.RegularField2DGeometry;
import org.visnow.vn.geometries.objects.SignalingTransform3D;
import static org.visnow.vn.lib.basic.mappers.InterpolatedSlice.InterpolatedSliceShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class InterpolatedSlice extends OutFieldVisualizationModule
{

    protected static final int TRANSFORM = 0;
    protected static final int AXS = 1;
    protected static final int DIMS = 2;
    protected int change = TRANSFORM;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField = null;
    protected GUI computeUI = null;
    protected boolean fromUI = false;
    protected boolean ignoreUI = false;
    protected float[][] extents = new float[][]{{0, 0, 0}, {1, 1, 1}};
    /**
     * @see RegularField#affine
     */
    protected float[][] baseAffine = new float[4][3];
    /**
     * @see RegularField#affine
     */
    protected float[][] affine = new float[4][3];
    protected float[][] inData;
    protected float[][] outData;
    protected int[] dims;
    protected int[] lastDimensions = new int[]{100, 100};
    protected DataArray[] dArrs;
    protected int lastAxis = -1;
    protected int nThreads = 1;
    protected int[] inDims;
    protected float[][] inAffine = null;
    protected float[][] inInvAffine = null;
    protected LogicLargeArray valid = null;
    private SignalingTransform3D transform = new SignalingTransform3D();

    public InterpolatedSlice()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (parameters.get(ADJUSTING)) {
                    if (inField == null)
                        return;
                    transformSlice();
                } else {
                    change = TRANSFORM;
                    if (parameters.get(AXIS) != lastAxis)
                        change = AXS;
                    for (int i = 0; i < 2; i++) {
                        if (parameters.get(RESOLUTION)[i] != dims[i] && change != DIMS)
                            change = DIMS;
                    }
                    startAction();
                }
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(RESOLUTION, new int[]{100, 100}),
            new Parameter<>(AXIS, 2),
            new Parameter<>(TRANSFORM_MATRIX, new double[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}),
            new Parameter<>(ROTATION_VALUES, new float[3]),
            new Parameter<>(TRANSLATION_VALUES, new float[3]),
            new Parameter<>(SCALE_VALUES, new float[3]),
            new Parameter<>(SCALE, 1f),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_DIAMETER, 1f)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_DIAMETER, inField.getDiameter());
        if (resetParameters) {
            parameters.set(RESOLUTION, new int[]{100, 100});
            parameters.set(AXIS, 2);
            parameters.set(TRANSFORM_MATRIX, new double[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1});
            parameters.set(ADJUSTING, false);
            parameters.set(ROTATION_VALUES, new float[3]);
//            extents = inField.getPreferredExtents();
//            parameters.set(TRANSLATION_VALUES, new float[] {(extents[0][0] + extents[1][0]) / 2, 
//                                                            (extents[0][1] + extents[1][1]) / 2, 
//                                                            (extents[0][2] + extents[1][2]) / 2});
            parameters.set(SCALE_VALUES, new float[3]);
            parameters.set(SCALE, 1f);

        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    /**
     * Computes new slice (after rotation, translation etc.).
     */
    private void transformSlice()
    {
        double[] trMatrix = parameters.get(TRANSFORM_MATRIX);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = 0;
                for (int k = 0; k < 3; k++)
                    affine[i][j] += trMatrix[j * 4 + k] * baseAffine[i][k];
                if (i == 3)
                    affine[i][j] += trMatrix[j * 4 + 3];
            }
        }
        outRegularField.setAffine(affine);
        updateSliceData();
    }

    class IrregularFieldInterpolator implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;

        public IrregularFieldInterpolator(int nThr, int iThr)
        {
            this.nThreads = nThr;
            this.iThread = iThr;
        }

        public void run()
        {
            LogicLargeArray inMask = inField.getCurrentMask();
            float[] p = new float[3];
            for (int i = iThread; i < dims[1]; i += nThreads) {
                for (int j = 0, l = i * dims[0]; j < dims[0]; j++, l++) {
                    for (int k = 0; k < 3; k++) {
                        p[k] = affine[3][k] + i * affine[1][k] + j * affine[0][k];
                    }
                    SimplexPosition tCoords = inField.getFieldCoords(p);
                    valid.setBoolean(l, tCoords != null);
                    if (tCoords != null) {
                        boolean isValid = true;
                        if (inMask != null)
                            for (int k = 0; k < 4; k++) {
                                isValid = isValid && inMask.getBoolean(tCoords.getVertices()[k]);
                            }
                        if (isValid)
                            for (int k = 0; k < inData.length; k++) {
                                int vl = dArrs[k].getVectorLength();
                                for (int m = 0; m < 4; m++) {
                                    int s = vl * tCoords.getVertices()[m];
                                    int t = vl * l;
                                    for (int n = 0; n < vl; n++) {
                                        outData[k][t + n] += tCoords.getCoords()[m] * inData[k][s + n];
                                    }
                                }
                            }
                        else
                            valid.setBoolean(l, false);
                    }
                }
            }
        }
    }

    class AffineFieldInterpolator implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;

        public AffineFieldInterpolator(int nThr, int iThr)
        {
            this.nThreads = nThr;
            this.iThread = iThr;
        }

        public void run()
        {
            float[] p = new float[3];
            float[] q = new float[3];
            int[] ind = new int[3];
            int ii, ij, ik;
            float t, u, v;
            int off2 = inDims[0] * inDims[1];
            int off1 = inDims[0];
            LogicLargeArray inMask = inField.getCurrentMask();
            boolean isValid = true;
            for (int i = iThread; i < dims[1]; i += nThreads) {
                for (int j = 0, l = i * dims[0]; j < dims[0]; j++, l++) {
                    for (int k = 0; k < 3; k++) {
                        p[k] = affine[3][k] + i * affine[1][k] + j * affine[0][k] - inAffine[3][k];
                    }
                    for (int k = 0; k < 3; k++) {
                        q[k] = 0;
                        for (int m = 0; m < 3; m++) {
                            q[k] += inInvAffine[k][m] * p[m];
                        }
                        ind[k] = (int) (q[k] + 100000) - 100000;
                        q[k] -= ind[k];
                    }
                    ii = ind[2];
                    t = q[2];
                    ij = ind[1];
                    u = q[1];
                    ik = ind[0];
                    v = q[0];
                    valid.setBoolean(l, !(ii < 0 || ii >= inDims[2] - 1 ||
                                          ij < 0 || ij >= inDims[1] - 1 ||
                                          ik < 0 || ik >= inDims[0] - 1));
                    if (inMask != null) {
                        int n = ik + off1 * ij + off2 * ii;
                        isValid = isValid && inMask.getBoolean(n) && inMask.getBoolean(n + 1) && inMask.getBoolean(n + off1) && inMask.getBoolean(n + off1 + 1) &&
                            inMask.getBoolean(n + off2) && inMask.getBoolean(n + off2 + 1) && inMask.getBoolean(n + off2 + off1) && inMask.getBoolean(n + off2 + off1 + 1);
                    }
                    if (isValid)
                        for (int k = 0; k < inData.length; k++) {
                            int vl = dArrs[k].getVectorLength();
                            int n = ik + off1 * ij + off2 * ii;
                            float[] data = inData[k];
                            if (!valid.getBoolean(l))
                                for (int m = 0; m < vl; m++) {
                                    outData[k][l * vl + m] = 0;
                                }
                            else
                                for (int m = 0; m < vl; m++) {
                                    outData[k][vl * l + m]
                                        = (1 - t) * ((1 - u) * ((1 - v) * data[vl * n + m] + v * data[vl * (n + 1) + m]) +
                                        u * ((1 - v) * data[vl * (n + off1) + m] + v * data[vl * (n + off1 + 1) + m])) +
                                        t * ((1 - u) * ((1 - v) * data[vl * (n + off2) + m] + v * data[vl * (n + off2 + 1) + m]) +
                                        u * ((1 - v) * data[vl * (n + off2 + off1) + m] + v * data[vl * (n + off2 + off1 + 1) + m]));
                                }
                        }
                    else
                        valid.setBoolean(l, false);
                }
            }
        }
    }

    private void updateSliceData()
    {
        Thread[] workThreads = new Thread[nThreads];
        for (float[] outData1 : outData) {
            for (int j = 0; j < outData1.length; j++) {
                outData1[j] = 0;
            }
        }
        if (inField instanceof RegularField && inField.getCurrentCoords() == null) {
            inDims = ((RegularField) inField).getDims();
            inAffine = ((RegularField) inField).getAffine();
            inInvAffine = ((RegularField) inField).getInvAffine();
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            if (inField instanceof RegularField && inField.getCurrentCoords() == null)
                workThreads[iThread] = new Thread(new AffineFieldInterpolator(nThreads, iThread));
            else
                workThreads[iThread] = new Thread(new IrregularFieldInterpolator(nThreads, iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads) {
            try {
                workThread.join();
            }catch (Exception e) {
            }
        }
        outRegularField.setCurrentMask(valid);
        outRegularField.setAffine(affine);
        if (regularFieldGeometry != null)
            ((RegularField2DGeometry) regularFieldGeometry).updateData();
    }

    private void updateOutField()
    {
        dims = parameters.get(RESOLUTION);
        System.arraycopy(dims, 0, lastDimensions, 0, dims.length);
        outRegularField = new RegularField(dims);
        outRegularField.setAffine(affine);
        valid = new LogicLargeArray(outRegularField.getNNodes());
        outRegularField.setCurrentMask(valid);
        int nNumDArrays = 0;
        for (int i = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).isNumeric())
                nNumDArrays += 1;
        }
        inData = new float[nNumDArrays][];
        outData = new float[nNumDArrays][];
        dArrs = new DataArray[nNumDArrays];
        for (int i = 0, j = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).isNumeric()) {
                DataArray da = inField.getComponent(i);
                inData[j] = da.getRawFloatArray().getData();
                dArrs[j] = inField.getComponent(j);
                outData[j] = new float[dArrs[j].getVectorLength() * dims[0] * dims[1]];
                outRegularField.addComponent(DataArray.create(outData[j], dArrs[j].getVectorLength(), dArrs[j].getName()));
                outRegularField.getComponent(j).setPreferredRanges(dArrs[j].getPreferredMinValue(), dArrs[j].getPreferredMaxValue(), 
                                                                   dArrs[j].getPreferredPhysMinValue(), dArrs[j].getPreferredPhysMaxValue());
                j += 1;
            }
        }

    }

    private void updateDims()
    {
        dims = parameters.get(RESOLUTION);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                baseAffine[i][j] *= (float) lastDimensions[i] / (float) dims[i];
                affine[i][j] *= (float) lastDimensions[i] / (float) dims[i];
            }
            lastDimensions[i] = dims[i];
        }
        updateOutField();
    }

    private void updateInitSlicePosition()
    {

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = baseAffine[i][j] = 0;
            }
        }
        switch (parameters.get(AXIS)) {
            case 0:
                baseAffine[3][0] = .5f * (extents[0][0] + extents[1][0]);
                baseAffine[3][1] = extents[0][1];
                baseAffine[3][2] = extents[0][2];
                baseAffine[0][1] = (extents[1][1] - extents[0][1]) / (dims[0] - 1);
                baseAffine[1][2] = (extents[1][2] - extents[0][2]) / (dims[1] - 1);
                break;
            case 1:
                baseAffine[3][0] = extents[0][0];
                baseAffine[3][1] = .5f * (extents[0][1] + extents[1][1]);
                baseAffine[3][2] = extents[0][2];
                baseAffine[0][0] = (extents[1][0] - extents[0][0]) / (dims[0] - 1);
                baseAffine[1][2] = (extents[1][2] - extents[0][2]) / (dims[1] - 1);
                break;
            case 2:
                baseAffine[3][0] = extents[0][0];
                baseAffine[3][1] = extents[0][1];
                baseAffine[3][2] = .5f * (extents[0][2] + extents[1][2]);
                baseAffine[0][0] = (extents[1][0] - extents[0][0]) / (dims[0] - 1);
                baseAffine[1][1] = (extents[1][1] - extents[0][1]) / (dims[1] - 1);
                break;
        }
        for (int i = 0; i < 4; i++) {
            System.arraycopy(baseAffine[i], 0, affine[i], 0, 3);
        }
        lastAxis = parameters.get(AXIS);
        outRegularField.setAffine(affine);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;
            extents = inField.getPreferredExtents();

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField && !isFromVNA());
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);
            
            if(isDifferentField) {
                if (inField.getGeoTree() == null && inField.getCurrentCoords() != null) {
                    SwingInstancer.swingRunAndWait(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            computeUI.creatingTreeInfo(true);
                        }
                    });
                    inField.createGeoTree();
                    if (inField.getGeoTree() == null)
                        return;
                }
                SwingInstancer.swingRunAndWait(new Runnable()
                {
                    public void run()
                    {
                        computeUI.creatingTreeInfo(false);
                    }
                });
                updateOutField();
                outField = outRegularField;
                updateInitSlicePosition();
                transformSlice();
                prepareOutputGeometry();
                lastAxis = -1;
            }
            switch (change) {
                 case TRANSFORM:
                     break;
                 case AXS:
                     updateInitSlicePosition();
                     break;
                 case DIMS:
                     updateDims();
                     regularFieldGeometry.setField(outRegularField);
             }
             change = TRANSFORM;
             transformSlice();
             setOutputValue("outField", new VNRegularField(outRegularField));
             show();
        } else {
            outField = null;
            prepareOutputGeometry();
            show();
            setOutputValue("outField", null);
        }
    }
}
