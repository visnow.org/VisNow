/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.*;
import org.visnow.vn.lib.utils.io.VNIOException;
/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularFieldHeaderParser extends HeaderParser
{

    private RegularField regularField;
    private boolean extentsRead = false, affineRead = false;
    public RegularFieldHeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        super(r, headerFile, fileName);
    }
    public RegularFieldIOSchema parseHeader()
            throws VNIOException
    {
        Vector<String[]> res = new Vector<String[]>();
        ParseResult result;
        RegularFieldIOSchema schema;
        String name = "";
        int[] dims = null;
        float[][] affine = new float[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 0}};
        float[][] extents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
        String[] affineEntries = new String[]{"v0", "v1", "v2", "orig"};
        String[] affineExtentEntries = new String[]{"r0", "r1", "r2", "orig"};
        String[] userData = null;
        boolean hasCoords = false, hasMask = false;
        int nDims = -1;
        try {
            line = nextLine();
            result = processLine(line, new String[]{"dim", "field", "name"},
                    new String[]{"c", "file", "x", "y", "z", "v", "o", "t"}, res);
            switch (result) {
                case ACCEPTED:
                    for (int i = 0; i < res.size(); i++) {
                        String[] strings = res.get(i);
                        if ((strings[0].startsWith("name") || strings[0].startsWith("field")) && strings.length > 1)
                            name = strings[1];
                        if (strings[0].startsWith("dim")) {
                            if (strings.length < 2) {
                                throw new VNIOException("no dimensions specified after \"dim\"", fileName, r.getLineNumber());
                            }
                            nDims = strings.length - 1;
                            dims = new int[nDims];
                            for (int j = 0; j < nDims; j++)
                                try {
                                    dims[j] = Integer.parseInt(strings[j + 1]);
                                } catch (NumberFormatException e) {
                                    throw new VNIOException(strings[j + 1] + " is not integer ", fileName, r.getLineNumber());
                                }
                        }
                        if (strings[0].startsWith("valid") || strings[0].startsWith("mask"))
                            hasMask = true;
                        if (strings[0].startsWith("coord"))
                            hasCoords = true;
                        if (strings[0].startsWith("user:")) {
                            userData = strings[0].substring(5).split(";");
                            for (int j = 0; j < userData.length; j++)
                                if (userData[j].startsWith("__"))
                                    try {
                                        int k = Integer.parseInt(userData[j].substring(2));
                                        userData[j] = stringsInLine[k];
                                    } catch (NumberFormatException e) {
                                    }
                        }
                    }
                    break;
                case EOF:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                case ERROR:
                    throw new VNIOException("bad field file ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("field dimensions must be specified in the first non-comment line ", fileName, r.getLineNumber());
                default:
                    break;
            }
            if (nDims > 0) {
                regularField = new RegularField(dims);
                if (name.isEmpty())
                    name = "Field name";
                regularField.setName(name);
                if (hasCoords)
                    regularField.setCurrentCoords(new FloatLargeArray(3 * regularField.getNNodes()));
                if (hasMask)
                    regularField.setCurrentMask(new LogicLargeArray(regularField.getNNodes(), true));
                regularField.setUserData(userData);
            }
            else
                throw new VNIOException("field dimensions must be specified in the first non-comment line ", fileName, r.getLineNumber());
            schema = new RegularFieldIOSchema(regularField, headerFile, fileName);
            line = nextLine();
            parseAxesDescription(regularField);
            extent_loop:
            while (true) {
                result = processLine(line, axesNames, new String[]{"c", "f", "v", "r", "o", "t", "u", "p"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 3; i++)
                                if (res.get(0)[0].startsWith(axesNames[i])) {
                                    extents[0][i] = Float.parseFloat(res.get(0)[1]);
                                    extents[1][i] = Float.parseFloat(res.get(0)[2]);
                                    break;
                                }
                            extentsRead = true;
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid extents line ", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in user extents section ", fileName, r.getLineNumber());
                    case BREAK:
                        break extent_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            affine_loop:
            while (true) {
                result = processLine(line, new String[]{"o", "v", "r"}, new String[]{"c", "f", "t", "u", "p"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 4; i++)
                                if (res.get(0)[0].startsWith(affineEntries[i]))
                                    for (int j = 1; j < max(3, res.get(0).length); j++)
                                        affine[i][j - 1] = Float.parseFloat(res.get(0)[j]);
                                else if (res.get(0)[0].startsWith(affineExtentEntries[i]))
                                    for (int j = 1; j < max(3, res.get(0).length); j++)
                                        affine[i][j - 1] = Float.parseFloat(res.get(0)[j]) / (regularField.getDims()[i] - 1);
                            affineRead = true;
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid affine table entry line", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in affine data section ", fileName, r.getLineNumber());
                    case BREAK:
                        break affine_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            if (extentsRead && !affineRead)
                regularField.setAffine(MatrixMath.computeAffineFromExtents(regularField.getDims(), extents));
            else if (affineRead)
                regularField.setAffine(affine);
            extents = regularField.getExtents();
            regularField.setPreferredExtents(extents, extents );
            field = regularField;
            parseExtentEntry(regularField);
            parseDataItems(regularField);
            DataFileSchema dataFileSchema;
            schema.generateDataInputStatus();
            parsedSchema = schema;
            file_loop:
            while ((dataFileSchema = parseFileEntry()) != null)
                schema.addFileSchema(dataFileSchema);
            r.close();
        } catch (IOException e) {
            throw new VNIOException("bad header file ", fileName, r.getLineNumber());
        }
        return schema;
    }

    @Override
    protected FilePartSchema parseFileSectionEntry(FileType fileType, Vector<String[]> tokens)
            throws VNIOException
    {
        int stride = -1;
        int[][] tile = null;
        int cOffset = 0;
        Vector<DataElementIOSchema> compSchemas = new Vector<DataElementIOSchema>();

        for (int i = 0; i < tokens.size(); i++) {
            String[] strings = tokens.get(i);
            if (strings[0].startsWith("stride") && strings.length > 1)
                stride = Integer.parseInt(strings[1]);
            else if (strings[0].startsWith("tile"))
                try {
                    tile = new int[regularField.getDims().length][3];
                    for (int[] tile1 : tile)
                        tile1[2] = 1;
                    if (strings.length == 2 * regularField.getDims().length + 1)
                        for (int j = 0; j < regularField.getDims().length; j++) {
                            tile[j][0] = Integer.parseInt(strings[2 * j + 1]);
                            tile[j][1] = Integer.parseInt(strings[2 * j + 2]);
                        }
                    else if (strings.length == regularField.getDims().length + 1)
                        for (int j = 0; j < regularField.getDims().length; j++) {
                            String[] tileSpecs = strings[j + 1].split(":");
                            tile[j][0] = Integer.parseInt(tileSpecs[0]);
                            tile[j][1] = Integer.parseInt(tileSpecs[1]);
                            if (tileSpecs.length > 2)
                                tile[j][2] = Integer.parseInt(tileSpecs[2]);
                        }
                } catch (NumberFormatException e) {
                    throw new VNIOException("number format error in tile definition ", fileName, r.getLineNumber());
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new VNIOException("invalid tile boundaries ", fileName, r.getLineNumber());
                }
            else
                cOffset = parseComponentSchema(regularField, strings, fileType, compSchemas, cOffset);
        }
        if (stride == -1)
            stride = cOffset;
        FileSectionSchema secSchema;
        if (compSchemas.isEmpty())
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, null,
                            fileType.isBinary());
        else
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, vlens,
                            fileType.isBinary());
        if (tile != null)
            secSchema.setTile(tile);
        return secSchema;
    }
}
