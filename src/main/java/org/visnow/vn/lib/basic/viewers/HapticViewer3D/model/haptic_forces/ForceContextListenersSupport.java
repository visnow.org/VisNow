/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import java.util.ArrayList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;

/**
 * Helper object for managing {@link ListDataListener} and {@link IForceContextChangeListener}
 * objects in {@link ForceContext}.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
class ForceContextListenersSupport
{

    protected final ArrayList<ListDataListener> basicListeners = new ArrayList<ListDataListener>();
    protected final ArrayList<IForceContextChangeListener> changeListeners = new ArrayList<IForceContextChangeListener>();

    //
    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been added.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index with an added force
     */
    protected void fireForceAdded(IForce f, int index, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.INTERVAL_ADDED, index, index);
        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.intervalAdded(e);
            }
        }
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been removed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index with a removed force
     */
    protected void fireForceRemoved(int index, ForceContext fc)
    {
        fireForceRemoved(index, index, fc);
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been removed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param fromIndex first index with a removed force
     * @param toIndex   last index with a removed force
     */
    protected void fireForceRemoved(int fromIndex, int toIndex, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.INTERVAL_REMOVED, fromIndex, toIndex);
        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.intervalRemoved(e);
            }
        }
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has changed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index of a modified force
     */
    protected void fireForceChanged(int index, ForceContext fc)
    {
        fireForceChanged(index, index, fc);
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that forces from indexes
     * <code>fromIndex</code>-
     * <code>toIndex</code>
     * have changed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param fromIndex one end of the interval
     * @param toIndex   the other end of the interval
     */
    protected void fireForceChanged(int fromIndex, int toIndex, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.CONTENTS_CHANGED, fromIndex, toIndex);

        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.contentsChanged(e);
            }
        }
    }

    public void addListDataListener(ListDataListener l)
    {
        synchronized (basicListeners) {
            basicListeners.add(l);
        }
    }

    public void removeListDataListener(ListDataListener l)
    {
        synchronized (basicListeners) {
            basicListeners.remove(l);
        }
    }

    public void addForceContextChangeListener(IForceContextChangeListener l)
    {
        synchronized (changeListeners) {
            changeListeners.add(l);
        }
    }

    public void removeForceChangeListener(IForceContextChangeListener l)
    {
        synchronized (changeListeners) {
            changeListeners.remove(l);
        }
    }
}
//revised
