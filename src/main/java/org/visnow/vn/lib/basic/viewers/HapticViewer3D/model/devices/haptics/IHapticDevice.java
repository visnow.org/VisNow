/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics;

import org.jogamp.vecmath.Tuple3f;

/**
 * Active device (a device with force feedback).
 *
 * Adds methods responsible for commanding force and haptic listeners.
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public interface IHapticDevice extends IHapticReadOnlyDevice
{

    /**
     * Adds a haptic listener to be added to the device
     *
     * @param listener The IHapticListener object to be added to the notify list
     */
    public void addHapticListener(IHapticListener listener);

    /**
     * Removes haptic listener
     *
     * @param listener The IHapticListener object to be removed from the notify list
     */
    public void removeHapticListener(IHapticListener listener);

    /**
     * Sets force
     *
     * @param force Force to be rendered by a haptic device
     */
    public void setForce(Tuple3f force);

    /**
     * Sets torque
     *
     * @param torque Torque to be rendered by a 6DOF haptic device
     */
    public void setTorque(Tuple3f torque);
}
