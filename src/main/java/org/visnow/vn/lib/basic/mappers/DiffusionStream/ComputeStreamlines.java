/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.DiffusionStream;

import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class ComputeStreamlines
{

    protected IrregularField outField = null;
    protected int nSp = 3;
    protected float[] startCoords = null;
    protected float[] coords = null;
    protected int[] toSteps = null;
    protected float[] vectors = null;
    protected int nvert = 0;
    protected int[] lines = null;
    protected Params params = null;
    protected int nForward = 0;
    protected int nSrc;
    protected float[] vects = null;
    protected int vlen = 3;
    protected int[] threadProgress = null;
    protected float[][] streamlineCoords = null;
    protected float[][] velocityVectors = null;
    protected int nThreads;
    protected int[] indices = null;

    public ComputeStreamlines(Field inField, Params params)
    {
        this.params = params;
        nForward = params.getNThreads() * (params.getNForwardSteps() / params.getNThreads());
        vects = inField.getComponent(params.getVectorComponent()).getRawFloatArray().getData();
        vlen = inField.getComponent(params.getVectorComponent()).getVectorLength();
    }

    abstract void updateStreamlines();

    public void setStartPoints(Field startPoints)
    {
        nSrc = (int) startPoints.getNNodes();
        if (startPoints.getCurrentCoords() != null)
            startCoords = startPoints.getCurrentCoords().getData();
        else if (startPoints instanceof RegularField)
            startCoords = ((RegularField) startPoints).getCoordsFromAffine().getData();
        toSteps = new int[nSrc];
    }

    public float[] getvNorms()
    {
        return vectors;
    }

    public IrregularField getOutField()
    {
        return outField;
    }

    public float[][] getStreamlineCoords()
    {
        return streamlineCoords;
    }

    public float[] getCoords()
    {
        return coords;
    }

    public int[] getFromSteps()
    {
        return new int[nSrc];
    }

    public int[] getToSteps()
    {
        return toSteps;
    }

    public int[] getLines()
    {
        return lines;
    }

    public int getNvert()
    {
        return nvert;
    }

    protected transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    public void clearFloatValueModificationListener()
    {
        statusListener = null;
    }

    protected void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }

}
