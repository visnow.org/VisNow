/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LargeArray;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Compute2D extends SkeletonCompute
{

    public Compute2D()
    {
    }

    public Compute2D(RegularField inField, SkeletonizerParams params)
    {
        this.params = params;
        setInField(inField);
    }

    private class ComputeSetBoundary2D implements Runnable
    {

        int iThread = 0, cSet = 0;

        public ComputeSetBoundary2D(int iThread, int cSet)
        {
            this.iThread = iThread;
            this.cSet = cSet;
        }

        @Override
        public void run()
        {
            for (int i1 = (iThread * dims[1]) / nThreads; i1 < ((iThread + 1) * dims[1]) / nThreads; i1++) {
                if (iThread == 0)
                    fireStatusChanged((i1 * .1f) / dims[1]);
                if (i1 == 0 || i1 == dims[1] - 1)
                    continue;
                for (int i0 = 1, i = i1 * dims[0] + 1; i0 < dims[0] - 1; i0++, i++)
                    if (bData[i] == cSet)
                        boundaryDistance[i] = BIGVAL;
                    else {
                        for (int j = 0; j < nNeighb; j++)
                            if (bData[i + off[j]] == cSet) {
                                boundaryDistance[i] = -1;
                                nodeQueue.insert(i);
                                break;
                            }
                    }
            }
        }
    }

    protected class ComputeThresholdBoundary2D implements Runnable
    {

        int iThread = 0;

        public ComputeThresholdBoundary2D(int iThread)
        {
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            for (int i1 = (iThread * dims[1]) / nThreads; i1 < ((iThread + 1) * dims[1]) / nThreads; i1++) {
                if (iThread == 0)
                    fireStatusChanged((i1 * .1f) / dims[1]);
                if (i1 == 0 || i1 == dims[1] - 1)
                    continue;
                for (int i0 = 1, i = i1 * dims[0] + 1; i0 < dims[0] - 1; i0++, i++)
                    if (bData[i] != 0)
                        boundaryDistance[i] = BIGVAL;
                    else {
                        for (int j = 0; j < nNeighb; j++)
                            if (bData[i + off[j]] != 0) {
                                boundaryDistance[i] = -1;
                                nodeQueue.insert(i);
                                break;
                            }
                    }
            }
        }
    }

    @Override
    protected void findInnermostNodes()
    {
        if (params.getStartPoints() != null)
        {
            int[][] sp = params.getStartPoints();
            nCentrePoints = 0;
            centres = new int[sp.length];
            for (int[] sp1 : sp) {
                int k = sp1[1] * dims[0] + sp1[0];
                if (boundaryDistance[k] > 0) {
                    centres[nCentrePoints] = k;
                    radii[nCentrePoints] = boundaryDistance[k];
                }
            }
            if (nCentrePoints > 0)
                return;
        }
        nCentrePoints = 0;
        Arrays.fill(bData, (byte) 0);
        for (int i1 = 1; i1 < dims[1] - 1; i1++)
            for (int i0 = 1, i = i1 * dims[0] + 1; i0 < dims[0] - 1; i0++, i++) {
                short d = boundaryDistance[i];
                if (d < initDiameter / 2)
                    continue;
                boolean ismax = true;
                for (int j = 0; j < nNeighb; j++)
                    if (bData[i + off[j]] == 1)
                        continue;
                    else if (boundaryDistance[i + off[j]] > d) {
                        ismax = false;
                        break;
                    } else if (boundaryDistance[i + off[j]] == d && bData[i] == 0) {
                        ismax = false;
                        bData[i] = 1;
                        break;
                    }
                if (ismax) {
                    if (nCentrePoints >= centres.length) {
                        int[] tmpCentres = new int[2 * centres.length];
                        short[] tmpRadii = new short[2 * centres.length];
                        System.arraycopy(centres, 0, tmpCentres, 0, centres.length);
                        System.arraycopy(radii, 0, tmpRadii, 0, radii.length);
                        centres = tmpCentres;
                        radii = tmpRadii;
                    }
                    centres[nCentrePoints] = i;
                    radii[nCentrePoints] = boundaryDistance[i];
                    nCentrePoints += 1;
                }
            }
    }

    @Override
    protected void setBoundaryData(int cSet)
    {
        bData = (byte[])inData.getRawArray().getData();
        if (mask != null)
            for (int m = 0; m < mask.length(); m++)
                if (!mask.getBoolean(m))
                    bData[m] = 0;
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeSetBoundary2D(iThread, cSet));
            workThreads[iThread].start();
        }
        for (int i = 0; i < workThreads.length; i++)
            try {
                workThreads[i].join();
            } catch (Exception e) {
            }
        clearOutDMargins();
        // outd is now initialized by: 0 if outside on 1 voxel wide margim, -1 if on the boundary, MAXVAL if inside
        // nodeQueue is initialized as the list of indices of boundary voxels
    }

    @Override
    protected void setBoundaryData()
    {
        fireActivityChanged("finding volume boundary");
        if (above) {
            LargeArray dData = inData.getRawArray();
            for (int m = 0; m < ndata; m++)
                bData[m] = (dData.getDouble(m) > threshold) ? (byte) 0xff : 0;
        } else {
            LargeArray dData = inData.getRawArray();
            for (int m = 0; m < ndata; m++)
                bData[m] = (dData.getDouble(m) < threshold) ? (byte) 0xff : 0;
        }
        if (mask != null)
            for (int m = 0; m < mask.length(); m++)
                if (!mask.getBoolean(m))
                    bData[m] = 0;
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeThresholdBoundary2D(iThread));
            workThreads[iThread].start();
        }
        for (int i = 0; i < workThreads.length; i++)
            try {
                workThreads[i].join();
            } catch (Exception e) {
            }
        // bdata is now binary map of inside skeletonized area (255) and outside or masked (0)
        clearOutDMargins();
        // outd is now initialized by: 0 if outside or on 1 voxel wide margin, -1 if on the boundary, MAXVAL if inside
        // nodeQueue is initialized as the list of indices of boundary voxels

    }

    @Override
    protected void clearOutDMargins()
    {
        int k = dims[0] - 1;
        for (int i1 = 0; i1 < dims[1]; i1++)
            boundaryDistance[i1 * dims[0]] = boundaryDistance[i1 * dims[0] + k] = 0;
        k = dims[0];
        for (int i0 = 0; i0 < dims[0]; i0++)
            boundaryDistance[i0] = boundaryDistance[k] = 0;
    }

}
