/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.ObjectFlow;

import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import static org.visnow.vn.lib.basic.mappers.ObjectFlow.ObjectFlowShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * Main class of time select/animate module
 * <p>
 * @author Krzysztof S. Nowinski (know@icm.edu.pl), University of Warsaw ICM
 * @author Jędrzej M. Nowosielski (jnow@icm.edu.pl), University of Warsaw ICM
 */
public class ObjectFlow extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;
    protected ObjectFlowGUI computeUI = null;
    protected ObjectFlowShared params;
    protected RegularField inRegularField = null;
    protected IrregularField inIrregularField = null;

    /**
     * Creates a new instance of the module main class
     */
    public ObjectFlow()
    {

        outObj.setName("displacement");
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {

                if (outField == null)
                    return;
                outField.setCurrentTime(parameters.get(TIME_FRAME));
                if (outField.hasMask()) {
                    show();
                } else {
                    if (outField.getCurrentCoords() != null) {
                        fieldGeometry.updateCoords(outField.getCurrentCoords());
                    }
                    fieldGeometry.updateDataMap();
                }

                if (outObj.getCurrentViewer() != null) {
                    outObj.getCurrentViewer().setWaitForExternalTrigger(parameters.get(CONTINUOUS_UPDATE));
                }

                if (!parameters.get(ADJUSTING) || parameters.get(CONTINUOUS_UPDATE)) {

                    startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ObjectFlowGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(NUMBER_OF_FRAMES, 2),
            new Parameter<>(START_TIME, 0.0f),
            new Parameter<>(END_TIME, 1.0f),
            new Parameter<>(TIME_FRAME, 0.0f),
            new Parameter<>(CONTINUOUS_UPDATE, false),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_START_TIME, 0.0f),
            new Parameter<>(META_END_TIME, 1.0f),
            new Parameter<>(META_TIME_UNIT, "")

        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        int nFrames = parameters.get(NUMBER_OF_FRAMES);
        float startTime = parameters.get(START_TIME);
        float endTime = parameters.get(END_TIME);
        float timeFrame = parameters.get(TIME_FRAME);

        // 1.  validate parameters
        parameters.set(META_TIME_UNIT, inField.getTimeUnit());
        parameters.set(META_START_TIME, inField.getStartTime());
        parameters.set(META_END_TIME, inField.getEndTime());

        if (startTime < inField.getStartTime()) {
            parameters.set(START_TIME, inField.getStartTime());
        }
        if (endTime > inField.getEndTime()) {
            parameters.set(END_TIME, inField.getEndTime());
        }

        if (getInputFirstValue("timeMoment") != null) {
            float frameFromModulePort = (Float) getInputFirstValue("timeMoment");
            if (frameFromModulePort != timeFrame) {
                timeFrame = frameFromModulePort;
                parameters.set(TIME_FRAME, timeFrame);
            }
        }

        if (timeFrame >= parameters.get(END_TIME)) {
            parameters.set(TIME_FRAME, parameters.get(END_TIME));
        }

        if (timeFrame < parameters.get(START_TIME)) {
            parameters.set(TIME_FRAME, parameters.get(START_TIME));
        }

        // 2. reset parameters / set smart values
        if (resetParameters) {
            parameters.set(NUMBER_OF_FRAMES, inField.getNFrames());
            parameters.set(START_TIME, inField.getStartTime());
            parameters.set(END_TIME, inField.getEndTime());
            parameters.set(TIME_FRAME, inField.getCurrentTime());

            if (getInputFirstValue("timeMoment") != null) {
                float frameFromModulePort = (Float) getInputFirstValue("timeMoment");
                if (frameFromModulePort != parameters.get(TIME_FRAME) && frameFromModulePort >= parameters.get(START_TIME) && frameFromModulePort < parameters.get(END_TIME)) {
                    parameters.set(TIME_FRAME, frameFromModulePort);
                }
            }
        }

        parameters.setParameterActive(true);

    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj.setCreator(this);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {

            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();

            //1a. set "different Field" flag
            boolean isDifferentField = !isFromVNA() && (inField == null || newInField != inField);
            boolean isNewField = (newInField != inField);
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                //2. validate params
                validateParamsAndSetSmart(isDifferentField);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }

            //3. update gui (ObjectFlowGUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
            notifyGUIs(p, isDifferentField || isFromVNA(), false);

            //4. run computation and propagate
            if (inField != null) {

                if (getInputFirstValue("timeMoment") != null && outField != null) {

                    outField.setCurrentTime(parameters.get(TIME_FRAME));
                    if (outField.hasMask()) {
                        show();
                    } else {
                        if (outField.getCurrentCoords() != null) {
                            fieldGeometry.updateCoords(outField.getCurrentCoords());
                        }
                        fieldGeometry.updateDataMap();
                    }

                    if (outObj.getCurrentViewer() != null) {
                        outObj.getCurrentViewer().setWaitForExternalTrigger(false);
                    }
                }

                if (isNewField || isFromVNA() || outField == null) {
                    outField = inField.cloneShallow();
                    prepareOutputGeometry();
                }
                outField.setCurrentTime(parameters.get(TIME_FRAME));
                outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());

                if (outField instanceof RegularField) {
                    setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                    setOutputValue("outIrregularField", null);
                    setOutputValue("outPointField", null);
                } else if (outField instanceof IrregularField) {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
                    setOutputValue("outPointField", null);
                } else if (outField instanceof PointField) {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", null);
                    setOutputValue("outPointField", new VNPointField((PointField)outField));
                }

            } else {
                outField = null;
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
                setOutputValue("outPointField", null);
            }

            //create default presentation of outField
            show(); //and send it to output

        } else {
            this.switchPanelToDummy();
        }

    }

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return computeUI.getFrameRenderedListener();
    }

    /**
     * Computation ObjectFlowGUI setter.
     * Set of a panel displayed in the "Computation" tab.
     *
     * @param computeUI alternative ObjectFlowGUI (alternative to default one instantiated in constructor)
     */
    public void setComputeUI(ObjectFlowGUI computeUI)
    {
        this.computeUI = computeUI;
    }
}
