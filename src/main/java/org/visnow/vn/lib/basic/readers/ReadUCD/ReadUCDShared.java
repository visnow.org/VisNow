/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadUCD;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Norbert_2
 */


public class ReadUCDShared {
    
    static final ParameterName<String> FILENAME = new ParameterName("file name");
    static final ParameterName<Boolean> MATERIAL_AS_SETS = new ParameterName("material as sets");
    static final ParameterName<Boolean> INDICES = new ParameterName("indices");
    static final ParameterName<Integer> INPUT_SOURCE = new ParameterName("input source");
    static final ParameterName<Boolean> CELL_TO_NODE = new ParameterName("Cell to node");
    static final ParameterName<Boolean> MERGE_CELL_SETS = new ParameterName("Merge cell sets");
    static final ParameterName<Boolean> DROP_CELL_DATA = new ParameterName("Drop cell data");
    
}

