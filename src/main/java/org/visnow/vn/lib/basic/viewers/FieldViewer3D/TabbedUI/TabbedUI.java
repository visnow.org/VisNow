/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.TabbedUI;

import java.util.Vector;
import javax.swing.JPanel;

/**
 *
 * @author vis
 */
public class TabbedUI extends javax.swing.JPanel
{

    Vector<UITab> tabs = new Vector<UITab>();

    /**
     * Creates new form TabbedUI
     */
    public TabbedUI()
    {
        initComponents();
        updateGUI();
    }

    public void updateGUI()
    {
        tabbedPane.removeAll();
        for (int i = 0; i < tabs.size(); i++) {
            if (tabs.get(i).isActive())
                tabbedPane.addTab(tabs.get(i).getTitle(), tabs.get(i));
        }
    }

    public UITab addUITab(UITab tab)
    {
        for (int i = 0; i < tabs.size(); i++) {
            if (tabs.get(i).equals(tab))
                return tabs.get(i);
        }

        tabs.add(tab);
        updateGUI();
        return tab;
    }

    public void removeUITab(UITab tab)
    {
        tabs.remove(tab);
        updateGUI();
    }

    public void removeAllUITabs()
    {
        tabs.clear();
        updateGUI();
    }

    public void removeUITab(String title)
    {
        removeUITab(getUITab(title));
    }

    public UITab getUITab(String title)
    {
        for (int i = 0; i < tabs.size(); i++) {
            if (tabs.get(i).getTitle().equals(title)) {
                return tabs.get(i);
            }
        }
        return addUITab(new UITab(title));
    }

    public void addUIToTab(JPanel ui, String title)
    {
        getUITab(title).addUI(ui);
    }

    public void addUIToTab(JPanel ui, String title, int position)
    {
        tabs.insertElementAt(new UITab(title), position);
        getUITab(title).addUI(ui);
        updateGUI();
    }

    public void setUITabActive(String position, boolean vis)
    {
        getUITab(position).setActive(vis);
        updateGUI();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();

        setBackground(java.awt.SystemColor.control);
        setMaximumSize(new java.awt.Dimension(230, 2147483647));
        setMinimumSize(new java.awt.Dimension(230, 7));
        setLayout(new java.awt.BorderLayout());

        tabbedPane.setBackground(java.awt.SystemColor.control);
        add(tabbedPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
     // Variables declaration - do not modify//GEN-BEGIN:variables

    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables

}
