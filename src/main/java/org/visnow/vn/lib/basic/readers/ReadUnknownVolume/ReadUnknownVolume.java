/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadUnknownVolume;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.parameters.RegularField3DParams;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadUnknownVolume extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of CreateGrid
     */
    protected InteractiveStructureFinder finder = new InteractiveStructureFinder();
    protected JFrame uiFrame = new JFrame();
    protected RegularField3DParams regularField3DmapParams = new RegularField3DParams();
    protected boolean fromGUI = false;
    protected GUI computeUI = null;


    //   protected RegularFieldGeometry regularFieldGeometry = null;
    //   protected OpenBranchGroup outGroup = null;
    public ReadUnknownVolume()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                uiFrame.setIconImage(new ImageIcon(getClass().getResource(VisNow.getIconPath())).getImage());
                uiFrame.setTitle("read unknown volume");
                uiFrame.getContentPane().setLayout(new java.awt.BorderLayout());
                uiFrame.getContentPane().add(finder);
                uiFrame.pack();
                uiFrame.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
                uiFrame.setVisible(true);
                setPanel(ui);
                computeUI = new GUI();
                computeUI.setFinder(uiFrame);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                finder.addChangeListener(new ChangeListener()
                {
                    @Override
                    public void stateChanged(ChangeEvent evt)
                    {
                        fromGUI = true;
                        startAction();
                    }
                });
            }
        });
    }

    @Override
    public void onDelete()
    {
        uiFrame.dispose();
        detach();
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    public void update()
    {
        int[] dims = new int[]{finder.getNx(), finder.getNy(), finder.getNz()};
        outRegularField = new RegularField(dims);
        float[][] pts = new float[2][3];
        switch (finder.getDataType()) {
            case FIELD_DATA_BYTE:
                byte[] inBData = finder.getBData();
                if (inBData != null) {
                    byte[] bData = new byte[dims[0] * dims[1] * dims[2]];
            bloop:  for (int i = 0, j = finder.getSkip(), l = 0; i < dims[2]; i++) {
                        for (int k = 0; k < dims[1]; k++)
                            for (int m = 0; m < dims[0]; m++, l++, j++) {
                                if (j >= inBData.length)
                                    break;
                                bData[l] = inBData[j];
                            }
                        j += finder.getSliceSkip();
                    }
                    outRegularField.addComponent(DataArray.create(bData, 1, "volume"));
                }
                break;
            case FIELD_DATA_SHORT:
                short[] inSData = finder.getSData();
                if (inSData != null) {
                    short[] sData = new short[dims[0] * dims[1] * dims[2]];
            sloop:  for (int i = 0, j = finder.getSkip(), l = 0; i < dims[2]; i++) {
                        for (int k = 0; k < dims[1]; k++)
                            for (int m = 0; m < dims[0]; m++, l++, j++){
                                if (j >= inSData.length)
                                    break;
                                sData[l] = inSData[j];
                            }
                        j += finder.getSliceSkip();
                    }
                    outRegularField.addComponent(DataArray.create(sData, 1, "volume"));
                }
                break;
            case FIELD_DATA_INT:
                int[] inIData = finder.getIData();
                if (inIData != null) {
                    int[] iData = new int[dims[0] * dims[1] * dims[2]];
            iloop:  for (int i = 0, j = finder.getSkip(), l = 0; i < dims[2]; i++) {
                        for (int k = 0; k < dims[1]; k++)
                            for (int m = 0; m < dims[0]; m++, l++, j++){
                                if (j >= inIData.length)
                                    break iloop;
                                iData[l] = inIData[j];
                            }
                        j += finder.getSliceSkip();
                    }
                    outRegularField.addComponent(DataArray.create(iData, 1, "volume"));
                }
                break;
            case FIELD_DATA_FLOAT:
                float[] inFData = finder.getFData();
                if (inFData != null) {
                    float[] fData = new float[dims[0] * dims[1] * dims[2]];
            floop:  for (int i = 0, j = finder.getSkip(), l = 0; i < dims[2]; i++) {
                        for (int k = 0; k < dims[1]; k++)
                            for (int m = 0; m < dims[0]; m++, l++, j++){
                                if (j >= inFData.length)
                                    break floop;
                                fData[l] = inFData[j];
                            }
                        j += finder.getSliceSkip();
                    }
                    outRegularField.addComponent(DataArray.create(fData, 1, "volume"));
                }
                break;
        }
        pts[1][0] = dims[0] - 1.f;
        pts[1][1] = dims[1] - 1.f;
        pts[1][2] = dims[2] - 1.f;
        pts[0][0] = pts[0][1] = pts[0][2] = 0.f;
        //outRegularField.setDims(dims);
        outRegularField.setPreferredExtents(pts);
        if (finder.getScale() == null || finder.getScale().length != 3) {
            float[] sc
                = {
                    1.f, 1.f, 1.f
                };
            outRegularField.setScale(sc);
        } else
            outRegularField.setScale(finder.getScale());
    }

    @Override
    public void onActive()
    {
        if (!fromGUI)
            return;
        update();
        setOutputValue("volume", new VNRegularField(outRegularField));
        setOutputValue("field object", new VNGeometryObject(outObj));
        outField = outRegularField;
        prepareOutputGeometry();
        show();
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            finder.activateOpenDialog();

    }

}
