//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import org.visnow.vn.gui.utils.DataSeriesChangedListener;
import org.visnow.vn.gui.utils.RemoveComponentActionListener;
import java.awt.GridBagConstraints;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.COPY;
import javax.swing.event.CellEditorListener;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;
import org.visnow.vn.system.swing.UIIconLoader;
import org.visnow.vn.system.swing.UIManagerKey;

/**
 * Panel holding the list of selected components and their visualization (as JTable).
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class ExtendedTablePanel extends javax.swing.JPanel
{

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ExtendedTablePanel.class);
    private TablePanel tablePanel = null;
    private final ComponentsTable seriesTableContainer;
    private DataSeriesChangedListener seriesToDisplayChangedListener;
    private RemoveComponentActionListener removeTableActionListener;

    /**
     * Creates new form Viewer1DFrame
     */
    public ExtendedTablePanel()
    {
        initComponents();

        componentsList.setTransferHandler(new TransferHandler()
        {
            @Override
            public int getSourceActions(JComponent c)
            {
                return COPY;
            }

            @Override
            protected Transferable createTransferable(JComponent c)
            {
                JList list = (JList) c;
                List values = list.getSelectedValuesList();

                StringBuffer buff = new StringBuffer();

                for (int i = 0; i < values.size(); i++) {
                    Object val = values.get(i);
                    buff.append(val == null ? "" : val.toString());
                    if (i != values.size() - 1) {
                        buff.append("\n");
                    }
                }
                return new StringSelection(buff.toString());
            }
        });
        componentsList.setDragEnabled(true);

        seriesTableContainer = new ComponentsTable();
        GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);

        seriesTableContainer.setTransferHandler(new TransferHandler()
        {
            @Override
            public boolean canImport(TransferHandler.TransferSupport info)
            {
                // only import Strings                
                return info.isDataFlavorSupported(DataFlavor.stringFlavor);
            }

            @Override
            public boolean importData(TransferHandler.TransferSupport info)
            {
                if (!info.isDrop()) {
                    return false;
                }

                // Check for String flavor
                if (!info.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    LOGGER.error("Table doesn't accept a drop of this type.");
                    return false;
                }

                try {
                    Transferable t = info.getTransferable();
                    String data = (String) t.getTransferData(DataFlavor.stringFlavor);

                    String[] compNames = data.split("\n");
                    for (String compName : compNames) {
                        compName = compName.trim();
                    }
                    if (seriesToDisplayChangedListener != null) {
                        seriesChangedAction(compNames);
                    }
                    return true;
                } catch (UnsupportedFlavorException | IOException ex) {
                    Logger.getLogger(ComponentsTable.class.getName()).log(Level.SEVERE, null, ex);
                }
                return false;
            }
        });

        componentsManagerContainer.add(seriesTableContainer, gridBagConstraints);
        componentsManagerContainer.revalidate();
        componentsManagerContainer.repaint();

        Icon icon = UIIconLoader.getIcon(UIManagerKey.InternalFrame_paletteCloseIcon);
        closeTableButton.setIcon(icon);
    }

    public void setTablePanel(TablePanel tablePanel)
    {
        this.tablePanel = tablePanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        layeredPanel = new javax.swing.JLayeredPane();
        closeTableButton = new javax.swing.JButton();
        tableContainer = new javax.swing.JPanel();
        componentsManagerContainer = new javax.swing.JPanel();
        componentsListContainer = new javax.swing.JScrollPane();
        componentsList = new javax.swing.JList();
        componentsListLabel = new javax.swing.JLabel();
        componentsListTooltip = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(800, 200));
        setPreferredSize(new java.awt.Dimension(0, 0));
        setLayout(new java.awt.GridBagLayout());

        layeredPanel.setLayout(new java.awt.GridBagLayout());

        closeTableButton.setBackground(new java.awt.Color(153, 153, 153));
        closeTableButton.setBorderPainted(false);
        closeTableButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeTableButton.setFocusPainted(false);
        closeTableButton.setMaximumSize(new java.awt.Dimension(18, 22));
        closeTableButton.setMinimumSize(new java.awt.Dimension(18, 22));
        closeTableButton.setOpaque(false);
        closeTableButton.setPreferredSize(new java.awt.Dimension(18, 22));
        closeTableButton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        closeTableButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                closeTableButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        layeredPanel.add(closeTableButton, gridBagConstraints);

        tableContainer.setBackground(new java.awt.Color(255, 255, 255));
        tableContainer.setMinimumSize(new java.awt.Dimension(400, 300));
        tableContainer.setPreferredSize(new java.awt.Dimension(400, 300));
        tableContainer.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
        layeredPanel.add(tableContainer, gridBagConstraints);

        componentsManagerContainer.setMaximumSize(new java.awt.Dimension(250, 2147483647));
        componentsManagerContainer.setMinimumSize(new java.awt.Dimension(350, 300));
        componentsManagerContainer.setPreferredSize(new java.awt.Dimension(350, 300));
        componentsManagerContainer.setLayout(new java.awt.GridBagLayout());

        componentsListContainer.setViewportView(componentsList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.3;
        componentsManagerContainer.add(componentsListContainer, gridBagConstraints);

        componentsListLabel.setText("Components List: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        componentsManagerContainer.add(componentsListLabel, gridBagConstraints);

        componentsListTooltip.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        componentsListTooltip.setForeground(new java.awt.Color(120, 120, 120));
        componentsListTooltip.setText("[Drag and drop items on the table]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        componentsManagerContainer.add(componentsListTooltip, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 0);
        layeredPanel.add(componentsManagerContainer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(layeredPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void closeTableButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeTableButtonActionPerformed
        if (removeTableActionListener != null) {
            removeTableActionListener.removeComponent(this);
        }
    }//GEN-LAST:event_closeTableButtonActionPerformed

    public void addSeriesListener(DataSeriesChangedListener seriesChangedListener)
    {
        seriesToDisplayChangedListener = seriesChangedListener;
    }

    private void seriesChangedAction(String[] names)
    {
        seriesToDisplayChangedListener.addSeriesToDisplay(names);
    }

    public void addRemoveSeriesListener(ActionListener removeSeriesAL)
    {
        seriesTableContainer.addRemoveSeriesActionListener(removeSeriesAL);
    }

    public void addSaveAsCSVSeriesListener(ActionListener saveSeriesAL)
    {
        seriesTableContainer.addSaveAsCSVSeriesActionListener(saveSeriesAL);
    }

    public void addSaveAsHDF5SeriesListener(ActionListener saveSeriesAL)
    {
        seriesTableContainer.addSaveAsHDF5SeriesActionListener(saveSeriesAL);
    }

    public void addKeyPressedOnSeriesTableListener(KeyListener keyListener)
    {
        seriesTableContainer.getTable().addKeyListener(keyListener);
    }

    public void addSeriesFormatChangedListener(CellEditorListener seriesFormatChangedCEL)
    {
        seriesTableContainer.addSeriesFormatChangedListener(seriesFormatChangedCEL);
    }

    public void addRemoveTableListener(RemoveComponentActionListener removePlotAL)
    {
        removeTableActionListener = removePlotAL;
    }

    public String getSelectedComponentName()
    {
        return (String) componentsList.getSelectedValue();
    }

    public String[] getRemovedSeriesNames()
    {
        return seriesTableContainer.getRemovedSeriesNames();
    }

    public int getSelectedRow()
    {
        return seriesTableContainer.getSelectedRow();
    }

    public String getSelectedSeriesName()
    {
        return seriesTableContainer.getSelectedSeriesName();
    }

    public String getSeriesFormat(FormatEditor fEditor)
    {
        return seriesTableContainer.getSeriesFormat(fEditor);
    }

    public void showComponentsList(boolean show)
    {
        componentsListContainer.setVisible(show);
        componentsListLabel.setVisible(show);
        componentsListTooltip.setVisible(show);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeTableButton;
    private javax.swing.JList componentsList;
    private javax.swing.JScrollPane componentsListContainer;
    private javax.swing.JLabel componentsListLabel;
    private javax.swing.JLabel componentsListTooltip;
    private javax.swing.JPanel componentsManagerContainer;
    private javax.swing.JLayeredPane layeredPanel;
    private javax.swing.JPanel tableContainer;
    // End of variables declaration//GEN-END:variables
    public void showTable()
    {
        if (tablePanel != null) {

            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);

            tableContainer.removeAll();
            tableContainer.invalidate();
            tableContainer.add(tablePanel, gridBagConstraints);
            tableContainer.validate();
        }
    }

    public void initGUIComponents(String[] list)
    {
        DefaultListModel listModel = new DefaultListModel();
        if (list.length > 0) {
            for (int i = 0; i < list.length; i++) {
                listModel.addElement(list[i]);
            }
            componentsList.setModel(listModel);
        } else {
            componentsList.setModel(listModel);
        }
    }

    public void update(Map<String, DataSet> series, boolean oneDTab)
    {

        Object[][] data = new Object[series.size()][6];
        Iterator<DataSet> values = series.values().iterator();
        if (tablePanel == null) {
            tablePanel = new TablePanel();
        }
        String lastSelection = tablePanel.getSelectedTabName();
        tablePanel.removeAllTables();
        if (series.size() > 0) {
            for (int i = 0; i < series.size(); i++) {
                DataSet dataSet = values.next();
                double[] stats = dataSet.getStats();
                data[i] = new Object[]{dataSet.getName(), dataSet.getFormat(), stats[0], stats[1], stats[2], stats[3]};
                tablePanel.addTable(dataSet, oneDTab);
            }
            if (lastSelection != null && !lastSelection.isEmpty()) {
                tablePanel.setSelectedTabByName(lastSelection);
            } else {
                tablePanel.setSelectedIndex(0);
            }
        }
        seriesTableContainer.updateDataTable(data);
        seriesTableContainer.repaint();
    }

    public void updateTable(DataSet dataSet, boolean oneDTab)
    {

        if (tablePanel != null) {
            tablePanel.updateTable(dataSet, oneDTab);
        }
    }

    public void saveTables(Map<String, DataSet> series, String[] names, String format)
    {
        if (tablePanel != null) {
            tablePanel.saveTables(series, names, format);
        }
    }
}
