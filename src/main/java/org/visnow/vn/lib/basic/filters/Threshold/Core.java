/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Threshold;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.Field;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private Params params = new Params();
    private Field inField = null;
    private Field outField = null;

    public Core()
    {
    }

    public void setParams(Params params)
    {
        this.params = params;
    }

    public void update()
    {
        if (inField == null)
            return;
        outField = inField.cloneShallow();
        outField.removeComponents();
        for (int i = 0; i < inField.getNComponents(); i++) {
            if (i == params.getComponent()) {
                outField.addComponent(thresholdComponent(inField.getComponent(params.getComponent()), params.getLowerThreshold(), params.getUpperThreshold(), params.isLowToMin(), params.isUpToMax()));
            } else {
                outField.addComponent(inField.getComponent(i).cloneShallow());
            }
        }

        if (params.isInvalidate()) {
            LogicLargeArray mask;
            if (inField.getCurrentMask() != null)
                mask = outField.getCurrentMask().clone();
            else
                mask = new LogicLargeArray(outField.getNElements(), true);

            float[] data = inField.getComponent(params.getComponent()).getRawFloatArray().getData();
            for (int i = 0; i < data.length; i++) {
                if (mask.getBoolean(i) && data[i] < params.getLowerThreshold() || data[i] > params.getUpperThreshold())
                    mask.setBoolean(i, false);
            }
            outField.setCurrentMask(mask);
        }
    }

    public void setInField(Field inField)
    {
        this.inField = inField;
    }

    public Field getOutField()
    {
        return outField;
    }

    public static DataArray thresholdComponent(DataArray da, float low, float up, boolean lowToMin, boolean upToMax)
    {
        switch (da.getType()) {
            case FIELD_DATA_BYTE:
                byte[] inB = (byte[]) da.getRawArray().getData();
                byte[] outB = new byte[inB.length];
                byte lowB = 0;
                byte upB = (byte) (0xff & 255);
                if (!lowToMin)
                    lowB = (byte) (0xff & (int) low);
                if (!upToMax)
                    upB = (byte) (0xff & (int) up);
                for (int j = 0; j < inB.length; j++) {
                    if ((0xFF & inB[j]) <= low)
                        outB[j] = lowB;
                    else if ((0xFF & inB[j]) >= up)
                        outB[j] = upB;
                    else
                        outB[j] = inB[j];
                }
                return DataArray.create(outB, 1, da.getName());
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray inD = da.getRawArray();
                LargeArray outD = LargeArrayUtils.create(inD.getType(), inD.length(), false);
                double lowD = da.getPreferredMinValue();
                if (!lowToMin)
                    lowD = low;
                double upD = da.getPreferredMaxValue();
                if (!upToMax)
                    upD = up;
                for (long j = 0; j < inD.length(); j++)
                    if (inD.getDouble(j) <= low)
                        outD.setDouble(j, lowD);
                    else if (inD.getDouble(j) >= up)
                        outD.setDouble(j, upD);
                    else
                        outD.setDouble(j, inD.getDouble(j));
                return DataArray.create(outD, 1, da.getName());
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }
}
