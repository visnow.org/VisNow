/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Geometries;

/**
 * All geometries must extend this class.
 * Obligatory user-defined static methods in the extent:
 * <ul>
 * <li><tt>public static boolean is1D()</tt></li>
 * <li><tt>public static boolean is2D()</tt></li>
 * <li><tt>public static boolean is3D()</tt></li>
 * <li><tt>public static boolean isAffine()</tt></li>
 * <li><tt>public static boolean isExplicit()</tt></li>
 * <li><tt>public static String getName()</tt></li>
 * <li><tt>public static boolean hasJLargeArraysSupport()</tt> (not used, for now)</li>
 * </ul>
 * Not implementing any of these methods will (most probably) result in runtime
 * errors, or, worse, undefined behavior.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 */
public abstract class AbstractGeometry
{
    protected float[][] affine = null;
    protected float[] coords = null;

    /**
     * Method, that computes actual coordinates of every point in the grid.
     *
     * @param dims   An array containing the dimensions of the grid. Can be ignored
     *               (i.e. set <tt>null</tt> for affine geometries.
     * @see org.visnow.jscic.RegularField
     */
    public abstract void compute(int[] dims);

    public void setAffine(float[][] affine)
    {
        this.affine = affine;
    }

    public float[][] getAffine()
    {
        return this.affine;
    }

    public void setCoords(float[] coords)
    {
        this.coords = coords;
    }

    public float[] getCoords()
    {
        return this.coords;
    }
}