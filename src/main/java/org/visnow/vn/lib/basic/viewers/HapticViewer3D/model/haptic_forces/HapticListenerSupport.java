/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import java.util.*;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticListener;

/**
 *
 * Helper class for managing haptic listeners (
 * <code>IHapticListener</code>).
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class HapticListenerSupport
{

    private final HashSet<IHapticListener> listeners = new HashSet<IHapticListener>();

    public HapticListenerSupport()
    {
    }

    /**
     * Adds a new IHapticListener to the notify list of any events that are fired
     *
     * @param listener The IHapticListener to add
     */
    public void addHapticListener(IHapticListener listener)
    {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    /**
     * Removes a IHapticListener from the notify list for any events fired from then on
     *
     * @param listener The IHapticListener to remove
     */
    public void removeHapticListener(IHapticListener listener)
    {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    /**
     * Returns the list of IHapticListener objects to notify as an Array of IHapticListener objects
     *
     * @return An array of IHapticListener objects
     */
    public IHapticListener[] getHapticListeners()
    {
        synchronized (listeners) {
            return this.listeners.toArray(new IHapticListener[listeners.size()]);
        }
    }

    /**
     * Returns the number of IHapticListener objects on the notify list
     *
     * @return A integer value for the number of IHapticListener objects
     */
    public int getNumOfListeners()
    {
        synchronized (listeners) {
            return listeners.size();
        }
    }

    /**
     * Fires a Device callback event to all IHapticListener objects that are set to be notified
     */
    public void fireDeviceCallback()
    {
        synchronized (listeners) {
            for (IHapticListener l : listeners) {
                l.callback();
            }
        }
    }
}
