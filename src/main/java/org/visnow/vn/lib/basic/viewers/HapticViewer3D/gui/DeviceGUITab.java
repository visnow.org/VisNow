/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.HapticException;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.HapticPointerController;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.haptics.HapticGUI;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.tablet.TabletGUI;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.TabletPointerController;

/**
 * Base class for device tabs. Tab shows parameters of a device like haptic, tablet etc. and allows
 * modifying some of them.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public abstract class DeviceGUITab
    extends javax.swing.JPanel
{

    InputDevicePointerController pointerController;

    /**
     * Only for GUI Builder and constructors in children classes.
     * Developers should use only a static {@link #createDeviceTab} factory.
     *
     * @deprecated Only for GUI Builder and constructors in children classes.
     */
    public DeviceGUITab()
    {
    }

    /**
     * The only method to be used to create a new device tab. DO NOT use constructor instead.
     *
     * @param deviceController device controller to be used by this tab
     * <p>
     * @return created tab
     */
    public static DeviceGUITab createDeviceTab(InputDevicePointerController deviceController)
    {

        DeviceGUITab tab;

        if (deviceController instanceof HapticPointerController) {
            try {
                HapticGUI hapticTab = new HapticGUI(deviceController);
                tab = hapticTab;
            } catch (HapticException ex) {
                throw new RuntimeException(
                    "I'm sorry, I could not initialize haptic device.\n\n" +
                    "Error: \n" + ex.toString());
            }
        } else if (deviceController instanceof TabletPointerController) {
            TabletGUI tabletTab = new TabletGUI(deviceController);
            tab = tabletTab;
        } else {
            throw new IllegalArgumentException("Could not create a Device tab - " +
                "pointerController for device " + deviceController.getDevice().getDeviceFriendlyName() +
                " must be an object of a known pointer controller class");

        }
        return tab;
    }

    /**
     * On closing the tab.
     */
    public abstract void close();
}
// revised.
