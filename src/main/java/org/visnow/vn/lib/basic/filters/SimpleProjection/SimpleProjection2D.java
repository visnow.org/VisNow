/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SimpleProjection;

import static org.apache.commons.math3.util.FastMath.*;

import org.visnow.jscic.RegularField;

import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 * 
 * Modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl;
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 * since revision 25.
 * 
 * Notes:
 * Method of projection codes:
 *   0 = Max
 *   1 = Min
 *   2 = Mean
 *   3 = Normalized mean
 * Hard-coded to make this class independent of former "Params" class.
 */
public class SimpleProjection2D
{
    public static final int METHOD_MAX = 0;
    public static final int METHOD_MIN = 1;
    public static final int METHOD_MEAN = 2;
    public static final int METHOD_NORMMEAN = 3;
    
    public SimpleProjection2D()
    {
    }

    public static RegularField compute(RegularField inField, int method, int axis)
    {
        RegularField outField = null;
        
        if (inField == null) {
            return outField;
        }

        if (inField.getDims().length != 2) {
            return outField;
        }
        
        if(axis < 0 || axis > 1)
            return null;        

        int[] inDims = inField.getDims();
        int[] outDims = new int[1];
        int u;
        if (inDims == null) {
            return outField;
        }

        switch (axis) {
            case 0:
                u = 1;
                break;
            case 1:
                u = 0;
                break;
            default:
                u = 0;
        }
        outDims[0] = inDims[u];
        outField = new RegularField(outDims);

        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];

        for (int i = 0; i < 3; i++) {
            outAffine[0][i] = inAffine[u][i];
            outAffine[1][i] = 0.0f;
            outAffine[2][i] = 0.0f;
            outAffine[3][i] = inAffine[3][i];
        }
        outField.setAffine(outAffine);

        for (int i = 0; i < inField.getNComponents(); i++) {
            switch (inField.getComponent(i).getType()) {
                case FIELD_DATA_SHORT:
                    short[] sOut;
                    sOut = projection((short[])inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(sOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_INT:
                    int[] iOut;
                    iOut = projection((int[])inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(iOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_FLOAT:
                    float[] fOut;
                    fOut = projection((float[])inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(fOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_DOUBLE:
                    double[] dOut;
                    dOut = projection((double[])inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(dOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] fOutR,
                     fOutI;
                    fOutR = projection(((ComplexDataArray) inField.getComponent(i)).getFloatRealArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    fOutI = projection(((ComplexDataArray) inField.getComponent(i)).getFloatImaginaryArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(new ComplexFloatLargeArray(new FloatLargeArray(fOutR), new FloatLargeArray(fOutI)), inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_BYTE:
                    byte[] bOut;
                    bOut = projection((byte[])inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(bOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                default:
                /* Not numeric type, like "String". */
            }
        }
        return outField;
    }

    private static byte[] projection(byte[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 2)
            return null;
        if (outDims == null || outDims.length != 1)
            return null;
        if(axis < 0 || axis > 1)
            return null;        

        byte[] out = new byte[outDims[0] * veclen];
        byte[] tmp = new byte[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int y = 0; y < inDims[1]; y++) {
                    off = y * inDims[0] * veclen;
                    for (int x = 0; x < inDims[0]; x++) {
                        off2 = x * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[x + v] = data[off2 + v];
                        }
                    }
                    off = y * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                    }
                }
                break;
            case 1:
                for (int x = 0; x < inDims[0]; x++) {
                    off = x * veclen;
                    for (int y = 0; y < inDims[1]; y++) {
                        off2 = y * inDims[0] * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[y + v] = data[off2 + v];
                        }
                    }
                    off = x * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                    }
                }
                break;
        }

        if (method == 3) {
            int v = (out[0] & 0xff);
            int max = v;
            int min = v;
            for (int i = 0; i < out.length; i++) {
                v = (out[i] & 0xff);
                if (v > max)
                    max = v;
                if (v < min)
                    min = v;
            }

            float d = (float) max - (float) min;
            int newv;
            for (int i = 0; i < out.length; i++) {
                v = (out[i] & 0xff);
                newv = round(255.0f * ((float) v - (float) min) / d);
                out[i] = (byte) newv;
            }
        }

        return out;
    }

    private static short[] projection(short[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 2)
            return null;
        if (outDims == null || outDims.length != 1)
            return null;
        if(axis < 0 || axis > 1)
            return null;        

        short[] out = new short[outDims[0] * veclen];
        short[] tmp = new short[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int y = 0; y < inDims[1]; y++) {
                    off = y * inDims[0] * veclen;
                    for (int x = 0; x < inDims[0]; x++) {
                        off2 = x * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[x + v] = data[off2 + v];
                        }
                    }
                    off = y * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
            case 1:
                for (int x = 0; x < inDims[0]; x++) {
                    off = x * veclen;
                    for (int y = 0; y < inDims[1]; y++) {
                        off2 = y * inDims[0] * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[y + v] = data[off2 + v];
                        }
                    }
                    off = x * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
        }
        return out;
    }

    private static int[] projection(int[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 2)
            return null;
        if (outDims == null || outDims.length != 1)
            return null;
        if(axis < 0 || axis > 1)
            return null;
        
        int[] out = new int[outDims[0] * veclen];
        int[] tmp = new int[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int y = 0; y < inDims[1]; y++) {
                    off = y * inDims[0] * veclen;
                    for (int x = 0; x < inDims[0]; x++) {
                        off2 = x * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[x + v] = data[off2 + v];
                        }
                    }
                    off = y * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
            case 1:
                for (int x = 0; x < inDims[0]; x++) {
                    off = x * veclen;
                    for (int y = 0; y < inDims[1]; y++) {
                        off2 = y * inDims[0] * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[y + v] = data[off2 + v];
                        }
                    }
                    off = x * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
        }
        return out;
    }

    private static float[] projection(float[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 2)
            return null;
        if (outDims == null || outDims.length != 1)
            return null;
        if(axis < 0 || axis > 1)
            return null;

        float[] out = new float[outDims[0] * veclen];
        float[] tmp = new float[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int y = 0; y < inDims[1]; y++) {
                    off = y * inDims[0] * veclen;
                    for (int x = 0; x < inDims[0]; x++) {
                        off2 = x * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[x + v] = data[off2 + v];
                        }
                    }
                    off = y * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
            case 1:
                for (int x = 0; x < inDims[0]; x++) {
                    off = x * veclen;
                    for (int y = 0; y < inDims[1]; y++) {
                        off2 = y * inDims[0] * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[y + v] = data[off2 + v];
                        }
                    }
                    off = x * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
        }
        return out;
    }

    private static double[] projection(double[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 2)
            return null;
        if (outDims == null || outDims.length != 1)
            return null;
        if(axis < 0 || axis > 1)
            return null;
        
        double[] out = new double[outDims[0] * veclen];
        double[] tmp = new double[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int y = 0; y < inDims[1]; y++) {
                    off = y * inDims[0] * veclen;
                    for (int x = 0; x < inDims[0]; x++) {
                        off2 = x * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[x + v] = data[off2 + v];
                        }
                    }
                    off = y * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
            case 1:
                for (int x = 0; x < inDims[0]; x++) {
                    off = x * veclen;
                    for (int y = 0; y < inDims[1]; y++) {
                        off2 = y * inDims[0] * veclen + off;
                        for (int v = 0; v < veclen; v++) {
                            tmp[y + v] = data[off2 + v];
                        }
                    }
                    off = x * veclen;
                    switch (method) {
                        case 0:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = max(tmp);
                            }
                            break;
                        case 2:
                        case 3:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = mean(tmp);
                            }
                            break;
                        case 1:
                            for (int v = 0; v < veclen; v++) {
                                out[off + v] = min(tmp);
                            }
                            break;
                    }
                }
                break;
        }
        return out;
    }

    private static byte max(byte[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        int max = (int) (in[0] & 0xff);
        int tmp;
        for (int i = 1; i < in.length; i++) {
            tmp = (int) (in[i] & 0xff);
            if (tmp > max)
                max = tmp;
        }
        return (byte) max;
    }

    private static int max(int[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        int max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max)
                max = in[i];
        }
        return max;
    }

    private static short max(short[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        short max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max)
                max = in[i];
        }
        return max;
    }

    private static float max(float[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        float max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max)
                max = in[i];
        }
        return max;
    }

    private static double max(double[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        double max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max)
                max = in[i];
        }
        return max;
    }

    private static byte min(byte[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        int min = (int) (in[0] & 0xff);
        int tmp;
        for (int i = 1; i < in.length; i++) {
            tmp = (int) (in[i] & 0xff);
            if (tmp < min)
                min = tmp;
        }
        return (byte) min;
    }

    private static int min(int[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        int min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min)
                min = in[i];
        }
        return min;
    }

    private static short min(short[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        short min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min)
                min = in[i];
        }
        return min;
    }

    private static float min(float[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        float min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min)
                min = in[i];
        }
        return min;
    }

    private static double min(double[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        double min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min)
                min = in[i];
        }
        return min;
    }

    private static long sum(byte[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += (in[i] & 0xff);
        }
        return sum;
    }

    private static long sum(int[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static long sum(short[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static double sum(float[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        float sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static double sum(double[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        double sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static byte mean(byte[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = sum(in);
        int mean = (int) round((double) sum / (double) in.length);
        return (byte) mean;
    }

    private static int mean(int[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = sum(in);
        return (int) round((double) sum / (double) in.length);
    }

    private static short mean(short[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        long sum = sum(in);
        return (short) round((double) sum / (double) in.length);
    }

    private static float mean(float[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        double sum = sum(in);
        return (float) (sum / (double) in.length);
    }

    private static double mean(double[] in)
    {
        if (in == null || in.length < 1)
            return 0;

        double sum = sum(in);
        return (sum / (double) in.length);
    }

}
