/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.hough;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private float progress;
    private Params params = new Params();
    private RegularField inField = null;
    private RegularField outField = null;

    public Core()
    {
    }

    public float getProgress()
    {
        return progress;
    }

    public void setParams(Params params)
    {
        this.params = params;
    }

    public void update()
    {
        if (inField == null) {
            return;
        }

        if (inField.getDims().length != 2) {
            return;
        }

        int[] dims = {255, 255, 255};
        outField = new RegularField(dims);

        for (int i = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).getVectorLength() != 1) {
                continue;
            }

            outField.addComponent(DataArray.create(calculateHough(i), 1, inField.getComponent(i).getName()));
        }
    }

    public void setInField(RegularField inField)
    {
        this.inField = inField;
    }

    public RegularField getOutField()
    {
        return outField;
    }

    private void bresenhamEllipse(int inputOffset, int zOffset, int xP, int yP, int xQ, int yQ, byte[] input, float[] tab)
    {

        int v = 0xFF & input[inputOffset];
        int x = xP, y = yP, y2 = 255 * y, D = 0, HX = xQ - xP, HY = yQ - yP, c, M, xInc = 1, yInc = 1, yInc2 = 255;
        if (HX < 0) {
            xInc = -1;
            HX = -HX;
        }
        if (HY < 0) {
            yInc = -1;
            yInc2 = -255;
            HY = -HY;
        }
        if (HY <= HX) {
            c = 2 * HX;
            M = 2 * HY;
            for (;;) {
                if (x >= 0 && x < 255 && y >= 0 && y < 255) {
                    tab[zOffset + y2 + x] += v;
                }
                if (x == xQ) {
                    break;
                }
                x += xInc;
                D += M;
                if (D > HX) {
                    y += yInc;
                    y2 += yInc2;
                    D -= c;
                }
            }
        } else {
            c = 2 * HY;
            M = 2 * HX;
            for (;;) {
                if (x >= 0 && x < 255 && y >= 0 && y < 255) {
                    tab[zOffset + y2 + x] += v;
                }
                if (y == yQ) {
                    break;
                }
                y += yInc;
                y2 += yInc2;
                D += M;
                if (D > HY) {
                    x += xInc;
                    D -= c;
                }
            }
        }
    }

    private float[] calculateHough(int i)
    {
        byte[] in = inField.getComponent(i).getRawByteArray().getData();            
        float[] out = new float[255 * 255 * 255];

        progress = 0;

        for (int zz = 0; zz < 255; zz++) {
            int zSqr = zz * zz;
            int zOffset = 255 * 255 * zz;
            int inputOffset = 0;
            for (int yy = 0; yy < 255; yy++) {
                inputOffset = 256 * yy;
                double cc = ((double) (zSqr - yy * yy) / (4 * zSqr));
                if (cc > 0) {
                    double sqrt_c = 255 * sqrt(cc);
                    for (int xx = 0; xx < 255; xx++) {
                        bresenhamEllipse(inputOffset, zOffset, xx, 0, (int) (xx - sqrt_c), 255, in, out);
                        bresenhamEllipse(inputOffset, zOffset, xx, 0, (int) (xx + sqrt_c), 255, in, out);
                        inputOffset++;
                    }
                }
            }

            progress = 0.8f * zz / 255;
            fireStateChanged();
        }

        int j = 0;
        for (int zz = 0; zz < 255; zz++) {
            for (int yy = 0; yy < 255; yy++) {
                double v = PI * (3 / 2 * (zz + (254 - yy) / 2) - sqrt(zz * (254 - yy) / 2));
                if (v > 1) {
                    for (int xx = 0; xx < 255; xx++) {

                        out[j] /= v;
                    }
                }
            }
            progress = 0.8f + 0.2f * zz / 255;
            fireStateChanged();
        }
        return out;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList
        = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList) {
            listener.stateChanged(e);
        }
    }
}
