/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.TransformGeometry;

public class GUI extends javax.swing.JPanel
{

    private String path = null;
    private String lastPath = null;
    protected Params params = new Params();

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
    }

    /**
     * Set the value of params
     *
     * @param params new value of params
     */
    public void setParams(Params params)
    {
        this.params = params;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        formatGroup = new javax.swing.ButtonGroup();
        fileChooser = new javax.swing.JFileChooser();
        coordsBox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/visnow/vn/lib/basic/writers/FieldWriter/Bundle"); // NOI18N
        coordsBox.setText(bundle.getString("GUI.coordsBox.text")); // NOI18N
        coordsBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                coordsBoxActionPerformed(evt);
            }
        });
        add(coordsBox, java.awt.BorderLayout.NORTH);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 236, Short.MAX_VALUE)
                );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 357, Short.MAX_VALUE)
                );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void coordsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_coordsBoxActionPerformed
    {//GEN-HEADEREND:event_coordsBoxActionPerformed
        if (params != null)
            params.setGenerateCoords(coordsBox.isSelected());
    }//GEN-LAST:event_coordsBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox coordsBox;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.ButtonGroup formatGroup;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
