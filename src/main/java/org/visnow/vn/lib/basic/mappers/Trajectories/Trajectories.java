/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Trajectories;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Trajectories extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;
    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    protected Params params;
    protected Core core = new Core();

    public Trajectories()
    {
        parameters = params = new Params();
        core.setParams(params);
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        outObj.setName("trajectories");
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        setPanel(ui);
    }

    public void update()
    {
        if (inField == null)
            return;

        core.setInField(inField);
        core.update();
        outIrregularField = core.getOutField();
        RegularField mappedField = core.getMappedField();
        if (outIrregularField == null) {
            setOutputValue("trajectoriesField", null);
        } else {
            setOutputValue("trajectoriesField", new VNIrregularField((outIrregularField)));
        }
        if (mappedField == null) {
            setOutputValue("mappedTrajectoriesField", null);
        } else {
            setOutputValue("mappedTrajectoriesField", new VNRegularField(mappedField));
        }
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }

    @Override
    public void onActive()
    {
        if (!fromGUI) {
            VNField inFld = (VNField) getInputFirstValue("inField");
            if (inFld != null) {
                Field newInField = inFld.getField();
                if (newInField != null && inField != newInField) {
                    inField = newInField;
                    if (inField.getNFrames() < 2) {
                        return;
                    }
                    computeUI.setInField(inField);
                }
            }
        }
        fromGUI = false;
        if (!params.isActive()) {
            return;
        }
        update();
    }
}
