/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import javax.swing.ListModel;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.IForceContextChangeListener;

/**
 * An interface for a JList with forces in HapticGUI
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface IForceListModel extends ListModel
{

    /**
     * Replaces one force with another.
     * <p/>
     * @param oldForce force to be replaced
     * @param newForce new force
     * <p>
     * @throws IllegalArgumentException if <code>oldForce</code> doesn't exist
     */
    public void set(IForce oldForce, IForce newForce);

    /**
     * Deletes force from a given index. Index must exist.
     *
     * @param index index to be removed
     * <p/>
     */
    public void remove(int index);

    /**
     * Sets from enabled to disabled or opposite.
     */
    public void swapEnabled(int index);

    public IForce get(int index);

    /**
     * Duplicates forces from a given index and returns the index of a duplicate. Returns -1 on
     * error (unchangeable force to be duplicated).
     * <p/>
     * @param index index of a force to be duplicated
     * <p>
     * @return index of a duplicated item or -1 on error
     */
    int duplicateForce(int index);

    /**
     * Adds a new force at the end of the list.
     *
     * Synchronization is made on a local copy of forces set. Blocking
     * synchronization is unacceptable due to real-time requirements
     * of haptic devices.
     *
     * @param force Force to be added
     */
    public void addForce(IForce force);

    /**
     * Inserts
     * <code>force</code> on index
     * <code>onIndex</code> (shifts all following forces). To insert force at the end, set
     * <code>onIndex</code> to -1 or call {@link #addForce(IForce)
     * }.
     * <p/>
     * @param force
     * @param onIndex
     */
    public void addForce(IForce force, int onIndex);

    /**
     * Currently unused.
     */
    public void addForceContextChangeListener(IForceContextChangeListener l);

    /**
     * Currently unused.
     */
    public void removeForceContextChangeListener(IForceContextChangeListener l);
}
// revised.
