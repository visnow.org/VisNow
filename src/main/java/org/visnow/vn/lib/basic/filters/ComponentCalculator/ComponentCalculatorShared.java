/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentCalculator;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class ComponentCalculatorShared
{
    static enum Precision {
        SINGLE, DOUBLE
    }
    
//Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //not-null, each element not-null (may be empty)
    static final ParameterName<String[]> EXPRESSION_LINES = new ParameterName("Expression lines");
    static final ParameterName<Precision> PRECISION = new ParameterName("Precision");

    static final ParameterName<Boolean> META_IS_GENERATOR = new ParameterName("META is generator");


    static final ParameterName<Boolean> IGNORE_UNITS = new ParameterName("Ignore units");

    static final ParameterName<Boolean> RETAIN = new ParameterName("Retain");
    
    static final ParameterName<Integer> META_FIELD_DIMENSION = new ParameterName("META dimensions");            // regular input field dimensions length = 1, 2 or 3
    static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");      // component names not-null, length >= 0, (0 means that module works in "Generator" mode)
    static final ParameterName<int[]> META_COMPONENT_VECLEN = new ParameterName("META component vecLenS");      // component vector lengths not-null, length = META_COMPONENT_NAMES.length, each element >=1
    static final ParameterName<String[]> INPUT_ALIASES = new ParameterName("Input field component aliases");    // component name aliases not-null, length = META_COMPONENT_NAMES.length each element not empty
    

    //**** generator parameters
    
    static final ParameterName<int[]> GENERATOR_DIMENSION_LENGTHS = new ParameterName("Generator dimensions");  //generated regular field dimensions length =  1, 2 or 3; each element >= 2
    static final ParameterName<float[][]> GENERATOR_EXTENTS = new ParameterName("Generator extents");           //generated regular field geometric extents 
                                                                                                                //length = 2, each element length = GENERATOR_DIMENSION_LENGTHS.length,  with constraint [0][i] < [1][i]

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
}
