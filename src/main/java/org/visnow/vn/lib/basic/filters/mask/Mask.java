/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.mask;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNRegularField;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Mask extends ModuleCore
{

    private GUI ui = new GUI();
    protected Params params;
    RegularField inField = null;

    public Mask()
    {
        parameters = params = new Params();
        ui.setParams(params);
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        setPanel(ui);
        //WTF-MUI:addModuleUI(ui);
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected RegularField calculateMask(RegularField inFld)
    {
        RegularField outFld = new RegularField(inFld.getDims());
        for (int i = 0; i < inField.getNComponents(); i++) {
            float[] data = inFld.getComponent(i).getRawFloatArray().getData();
            float[] data2 = new float[data.length];
            System.arraycopy(data, 0, data2, 0, data.length);
            int[] dims = inFld.getDims();
            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i1 + i2 * dims[0]];
                    for (int i3 = 0; i3 < dims[2]; i3++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }
            //
            for (int i3 = 0; i3 < dims[2]; i3++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i2 * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i1 = 0; i1 < dims[0]; i1++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i3 = 0; i3 < dims[2]; i3++) {
                    float min = data[i1 + i3 * dims[0] * dims[1]];
                    for (int i2 = 0; i2 < dims[1]; i2++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            // ----------
            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i1 + i2 * dims[0] + (dims[2] - 1) * dims[0] * dims[1]];
                    for (int i3 = dims[2] - 1; i3 > 0; i3--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }
            //
            for (int i3 = 0; i3 < dims[2]; i3++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[dims[0] - 1 + i2 * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i1 = dims[0] - 1; i1 > 0; i1--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i3 = 0; i3 < dims[2]; i3++) {
                    float min = data[i1 + (dims[1] - 1) * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i2 = dims[1] - 1; i2 > 0; i2--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            outFld.addComponent(DataArray.create(data2, inField.getComponent(i).getVectorLength(), "mask_" + inField.getComponent(i).getName()));
        }
        if (inField.getCurrentCoords() != null) {
            outFld.setCurrentCoords(inField.getCurrentCoords());
        } else {
            outFld.setAffine(inField.getAffine());
        }
        return outFld;
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") == null) {
            return;
        }
        RegularField inFld = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inFld == null) {
            return;
        }
        if (inFld != inField) {
            inField = inFld;
            ui.setInField(inField);
        }
        ui.update();
        RegularField outField = calculateMask(inFld);

        setOutputValue("outField", new VNRegularField(outField));
    }
}
