/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.util.Vector;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ComplexCore
{

    private Parameters params = null;
    private Field inField = null;
    protected RegularField outRegularField = null;
    protected Field outField = null;

    public ComplexCore()
    {
    }

    public void setData(Field inField, Field outField, Parameters p)
    {
        this.inField = inField;
        this.outField = outField;
        this.params = p;
    }

    void update()
    {
        if (inField == null || params == null) {
            outField = null;
            return;
        }

        int nComplexComps = 0;
        int nNonComplexComps = 0;
        for (int i = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                nComplexComps++;
            } else if (inField.getComponent(i).getVectorLength() == 1) {
                nNonComplexComps++;
            }
        }

        if (nNonComplexComps > 0) {
            Vector<ComplexComponent> cc = params.get(COMPLEX_COMBINE_COMPONENTS);
            for (int i = 0; i < cc.size(); i++) {
                outField.addComponent(DataArray.create(new ComplexFloatLargeArray(inField.getComponent(cc.get(i).getRealComponent()).getRawFloatArray(), inField.getComponent(cc.get(i).getImagComponent()).getRawFloatArray()), 1, cc.get(i).getName(), null, null));
            }
        }

        if (nComplexComps > 0) {
            boolean[] splitRe = params.get(COMPLEX_SPLIT_RE);
            if (splitRe == null || splitRe.length != nComplexComps) {
                splitRe = new boolean[nComplexComps];
                for (int i = 0; i < splitRe.length; i++) {
                    splitRe[i] = false;
                }
            }

            boolean[] splitIm = params.get(COMPLEX_SPLIT_IM);
            if (splitIm == null || splitIm.length != nComplexComps) {
                splitIm = new boolean[nComplexComps];
                for (int i = 0; i < splitIm.length; i++) {
                    splitIm[i] = false;
                }
            }

            boolean[] splitAbs = params.get(COMPLEX_SPLIT_ABS);
            if (splitAbs == null || splitAbs.length != nComplexComps) {
                splitAbs = new boolean[nComplexComps];
                for (int i = 0; i < splitAbs.length; i++) {
                    splitAbs[i] = false;
                }
            }

            boolean[] splitArg = params.get(COMPLEX_SPLIT_ARG);
            if (splitArg == null || splitArg.length != nComplexComps) {
                splitArg = new boolean[nComplexComps];
                for (int i = 0; i < splitArg.length; i++) {
                    splitArg[i] = false;
                }
            }

            int c = 0;
            for (int i = 0; i < inField.getNComponents(); i++) {
                if (inField.getComponent(i).getType() != DataArrayType.FIELD_DATA_COMPLEX)
                    continue;

                if (splitRe[c])
                    outField.addComponent(DataArray.create(((ComplexDataArray) inField.getComponent(i)).getFloatRealArray(), inField.getComponent(i).getVectorLength(), "Re_" + inField.getComponent(i).getName()));
                if (splitIm[c])
                    outField.addComponent(DataArray.create(((ComplexDataArray) inField.getComponent(i)).getFloatImaginaryArray(), inField.getComponent(i).getVectorLength(), "Im_" + inField.getComponent(i).getName()));
                if (splitAbs[c])
                    outField.addComponent(DataArray.create(((ComplexDataArray) inField.getComponent(i)).getFloatAbsArray(), inField.getComponent(i).getVectorLength(), "Abs_" + inField.getComponent(i).getName()));
                if (splitArg[c])
                    outField.addComponent(DataArray.create(((ComplexDataArray) inField.getComponent(i)).getFloatArgArray(), inField.getComponent(i).getVectorLength(), "Arg_" + inField.getComponent(i).getName()));

                c++;
            }
        }
    }

    Field getOutField()
    {
        return outField;
    }
}
