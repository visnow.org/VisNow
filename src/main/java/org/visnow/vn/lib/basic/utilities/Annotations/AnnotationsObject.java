/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.Annotations;

import java.util.ArrayList;
import org.jogamp.java3d.*;
import org.visnow.vn.geometries.objects.DataMappedGeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.textUtils.Texts2D;
import org.visnow.vn.geometries.utils.Texts3D;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnnotationsObject extends DataMappedGeometryObject
{

    private Texts2D glyphs = null;
    private OpenBranchGroup outGroup = null;
    private final FontParams fontParams;
    private final ArrayList<float[]> coordsList;
    private final ArrayList<String[]> textsList;

    public AnnotationsObject(FontParams fontParams,
                             ArrayList<float[]> coordsList,
                             ArrayList<String[]> textsList)
    {
        name = "text glyphs";
        this.fontParams = fontParams;
        this.coordsList = coordsList;
        this.textsList  = textsList;
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height)
    {
        glyphs.draw(vGraphics, ltw, width, height);
    }

    public void prepareGlyphs()
    {
        Display3DPanel panel = getCurrentViewer();
        if (panel == null)
            return;
        fontParams.createFontMetrics(localToWindow, panel.getWidth(), panel.getHeight());
        if (outGroup != null)
            outGroup.detach();
        outGroup = null;
        glyphs = null;
        if (textsList == null || textsList.isEmpty()) {
            geometryObj.getCurrentViewer().refresh();
            return;
        }
        String[][] texts = new String[textsList.size()][1];
        String[] texts3 = new String[textsList.size()];
        for (int i = 0; i < texts.length; i++) {
            texts3[i] = texts[i][0] = textsList.get(i)[0];
        }
        float[] crds = new float[3 * coordsList.size()];
        for (int i = 0; i < coordsList.size(); i++)
            System.arraycopy(coordsList.get(i), 0, crds, 3 * i, 3);
        if (fontParams.isThreeDimensional()) {
            outGroup = new Texts3D(crds, texts3, fontParams);
            addNode(outGroup);
            setExtents(((Texts3D) outGroup).getExtents());
        } else {
            glyphs = new Texts2D(crds, texts, fontParams);
            geometryObj.getCurrentViewer().refresh();
        }
    }

    public String[][] getTexts() {
        return glyphs == null ? null : glyphs.getTexts();
    }

    public float[] getTextCoords() {
        return glyphs == null ? null : glyphs.getTextCoords();
    }

    public void update()
    {
        prepareGlyphs();
    }

}
