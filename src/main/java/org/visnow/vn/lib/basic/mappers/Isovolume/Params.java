//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isovolume;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.value.ComponentValue;

/**
 *
 * <p>
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{

    protected static final String TYPE = "type";
    protected static final String RUN = "run";
    protected ComponentValue threshold = new ComponentValue();

    /**
     * isoComponent - data component for isosurfacing
     * threshold - isosurface threshold
     * low - input field will be cropped from below according to these indices before isosurfacing
     * up - input field will be cropped from above according to these indices before isosurfacing
     * downsize - input field will be downsized according to these indices before isosurfacing
     */
    private boolean recompute = true;

    public Params()
    {
        super(eggs);
        threshold.setContinuousUpdate(false);
        threshold.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(TYPE, ParameterType.independent, 1),
        new ParameterEgg<RunState>(RUN,ParameterType.independent, NO_RUN)
    };
    

    public int getType()
    {
        return (Integer) getValue(TYPE);
    }

    public void setType(int type)
    {
        setValue(TYPE, type);
        fireStateChanged();
    }
    
    public RunState getRunState()
    {
        return (RunState)getValue(RUN);
    }
    
    public void setRunState(RunState state)
    {
        setValue(RUN, state);
        fireStateChanged();
    }

    public ComponentValue getThreshold()
    {
        return threshold;
    }


}
