/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d;

import java.util.LinkedList;
import java.util.List;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.IDragListener;

/**
 * Handles adding and removing listeners of type IDragListener and notifies them of changes
 * <p>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class Pointer3DViewBehaviorSupport
{

    List<IDragListener> listeners = new LinkedList<IDragListener>();

    public void addDragListener(IDragListener listener)
    {
        listeners.add(listener);
    }

    public void removeDragListener(IDragListener listener)
    {
        listeners.remove(listener);
    }

    public void fireDragEvent(IDragListener.DragEvent e)
    {
        for (IDragListener l : listeners)
            l.onDragEvent(e);
    }
}
//revised.
