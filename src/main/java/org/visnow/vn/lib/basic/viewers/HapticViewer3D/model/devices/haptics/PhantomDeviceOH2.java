/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics;

import java.util.concurrent.Semaphore;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3f;
import openhaptics.hdapi.Device;
import openhaptics.hdapi.HDException;
import openhaptics.hdapi.Scheduler;
import openhaptics.hdapi.SchedulerCallback;
import openhaptics.hdapi.misc.CalibrationStyle;
import openhaptics.hdapi.misc.CallbackCode;
import openhaptics.hdapi.misc.CapabilityParameter;
import openhaptics.hdapi.misc.ErrorCode;
import openhaptics.hdapi.misc.MiscParams;
import openhaptics.hdapi.misc.Parameter;
import openhaptics.hlapi.Context;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 *
 * TODO MEDIUM: check whether there are memory leaks - try attaching Phantom to the Viewer3D and
 * detaching it many times.
 */
public class PhantomDeviceOH2
    extends BasicHapticDevice
    implements IHapticDevice
{

    //TODO LOW: check that getters and setters are properly synchronized
    public static final String DEFAULT_PHANTOM = "Default PHANToM";
    public static final int BUTTONS_COUNT = 1;
    /**
     * Access this variable only after acquiring a lock on
     * <code>PhantomDeviceOH2.class</code>.
     */
    public static boolean scheduleStarted = false;
    //
    protected int hhd = Device.INVALID_HANDLE;
    protected int calibrationStatus = -1;
    protected int calibrationStyle = -1;
    private VisApiCallback visCallback;
    /**
     * HL Context.
     * It's strange, but it seems it must be created to... enable reading position in haptic
     * callback
     * function (using an *HD* function Device.get....() !!). Without that reading position works
     * only for the first fraction of second :/
     */
    protected Context context;
    //
    /**
     * A counter for Phantom devices.<br/>
     * There is only one Scheduler for all Phantom devices, therefore when the first DEVICE is
     * initialized, Scheduler must be started and when the last is closed, it should be stop.
     * <p/>
     * Access this variable only after acquiring a lock on
     * <code>PhantomDeviceOH2.class</code>.
     */
    private static int devicesCount = 0;
    //
    protected long callbackHandle;
    //
    /**
     * A lock for all variables that are used in haptic thread
     */
    private static final Object hapticVariablesLock = new Object();
    //
    // == variables that are used in haptic thread ==
    /**
     * variable used in haptic thread
     */
    protected Point3d force = new Point3d();
    /**
     * variable used in haptic thread
     */
    protected Point3d torque = new Point3d();
    /**
     * Variable used in haptic thread
     * From Open Haptics Toolkit API Reference:
     * Current position of the device facing the device base.
     * Right is + x, up is +y, toward user is +z.
     * Unit: mm (???)
     */
    protected Point3d position = new Point3d();
    /**
     * variable used in haptic thread
     */
    protected Point3d velocity = new Point3d();
    /**
     * variable used in haptic thread
     */
    protected Transform3D transform = new Transform3D();
    /**
     * variable used in haptic thread
     */
    protected int buttonState = 0;
    //
    // === used to measure manually frames per second 
    // (device update rate: INSTANTANEOUS_UPDATE_RATE and UPDATE_RATE have to small period of measure)
    protected static long MILISECONDS_MEASURE_TIME = 1000;
    protected long lastMilisecondsTime = 0;
    protected long nextMilisecondsTime = 0;
    protected long framesCounter = 0;
    // ===
    //
    protected Point3d lastPosition = new Point3d();
    protected long lastPositionNanotime = System.nanoTime();
    //
    /**
     * Temporary array
     */
    protected final double[] transformMatrix = new double[16];
    /**
     * Temporary array
     */
    private final double[] doubleBuff = new double[16];
    //
    /**
     * Update rate.
     */
    private int updateRate;
    /**
     * Instantaneous update rate.
     */
    private int instUpdateRate;
    // ================
    //
    public final static double translationScaling = 0.001; //mm -> meters
    /**
     * If set to false, velocity will be fetched from the device itself. If set to true, it will be
     * calculated in callback in VisApiCallback using difference in position and instantaneous
     * update rate.
     * <p/>
     * Sometimes for any reason velocity read from device is zero or gives strange results (non-zero
     * when haptic is not moving and only small values when moving it).
     * Maybe it's after building a project without clean. Try running clean & build.
     * If it doesn't work, set this variable to true.
     */
    private final boolean ESTIMATE_VELOCITY = true;
    static final org.apache.log4j.Logger LOGGER
        = org.apache.log4j.Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    //
    /**
     * Lock for all
     * <code>nominal....</code> variables below. Probably it is not necessary - variables will be
     * written only once in program run.
     */
    private final Object nominalLock = new Object();
    private double nominalMaxContinuousForce = Double.NaN; // = 6.16 N (similar to the PHANTOM Premium User Guide Appendix H: PHANTOM Specification: 6.2 N)
    private double nominalMaxForce = Double.NaN; // = 37.4 N  (similar to the PHANTOM Premium User Guide Appendix H: PHANTOM Specification: 37.5 N)
    private double nominalMaxDamping = Double.NaN;

    //    static {
    //        System.loadLibrary("JOpenHaptics");
    //    }
    public static void f() throws HDException
    {
        final double[] d = new double[3];
        Device.getDoublev(Parameter.CURRENT_VELOCITY, d);
    }

    static int ur1, ir1;

    public static void main(String args[])
    {
        try {
            final PhantomDeviceOH2 d = new PhantomDeviceOH2(DEFAULT_PHANTOM);

            d.addHapticListener(new IHapticListener()
            {
                long ts;

                @Override
                public void callback()
                {
                    ts = System.currentTimeMillis();
                    try {
                        ur1 = Device.getInteger(Parameter.UPDATE_RATE);
                        ir1 = Device.getInteger(Parameter.INSTANTANEOUS_UPDATE_RATE);

                        boolean value;
                        value = Device.isEnabled(CapabilityParameter.FORCE_OUTPUT);
                        LOGGER.info("FORCE_OUTPUT 1: " + value);

                    } catch (HDException ex) {
                        LOGGER.error(ex);
                    }
                }
            });

            //            Context.createContext(d.getHhd());
            //
            //            int lastUr = 0, lastIr = 0;
            //            while (true) {
            //                if (lastUr != ur1 || lastIr != ir1) {
            //                    lastUr = ur1;
            //                    lastIr = ir1;
            //                    System.out.println(ur1 + " / " + ir1);
            //                }
            //            }
            Tuple3f pos = new Point3f();
            while (true) {
                d.getPosition(pos);
                System.out.println("position: " + pos);
            }

        } catch (HapticException ex) {
            LOGGER.error(ex);
        }
    }

    public PhantomDeviceOH2() throws HapticException
    {
        this(DEFAULT_PHANTOM);
    }

    /**
     * Initializes phantom device.
     *
     * @param deviceName Device name (ID) the same as in Sensable Phantom Configuration program
     */
    public PhantomDeviceOH2(String deviceName) throws HapticException
    {
        this(deviceName, "PHANTOM");
    }

    /**
     *
     * @param deviceName         Device name or null for default device
     * @param deviceFriendlyName
     *                           <p>
     * @throws HapticException
     */
    public PhantomDeviceOH2(String deviceName, String deviceFriendlyName) throws HapticException
    {
        super(deviceName, deviceFriendlyName);

        try {
            if (deviceName == null) {
                throw new NullPointerException("deviceName cannot be null");
            }
            if (deviceFriendlyName == null) {
                throw new NullPointerException("deviceFriendlyName cannot be null");
            }

            /**
             * HD: init device.
             */
            try {
                hhd = Device.initDevice(deviceName);
            } catch (HDException ex) {
                throw new HapticException("HDException in native initDevice(). " +
                    "Make sure that Phantom device is properly connected.\n\n" +
                    "Error: " + ex);
            } catch (Exception ex) {
                throw new HapticException("HDException in native initDevice(). " +
                    "Make sure that Phantom device is properly connected.\n\n" +
                    "Error: " + ex);
            } catch (UnsatisfiedLinkError err) {
                throw new HapticException("HDException in native initDevice(). " +
                    "Make sure that Phantom device is properly connected.\n\n" +
                    "Error: " + err);
            }

            /* create haptic context, call to delete() when destructing */
            context = Context.createContext(hhd);//TODO LOW: maybe context is not needed (it's from HL, while later we use only HD)

            // enable scheduler
            onPhantomDeviceCreated();

            /* ATTENTION!!! scheduleSynchronous DOES NOT wait!!! Therefore we must use a semaphore here...  */

            /* Set safety parameters */
            /**
             * Setting here sometimes works and sometime doesn't...
             * Make sure that values of those flags in callback() are the same.
             * That is really strange, no reason for it was found, but sometimes setting flags here
             * doesn't reflect their value in callback() function.
             *
             * E.g.: setting FORCE_OUTPUT to false sometimes DOES NOT WORK! It will be still set to
             * on in callback() function (it's weird and dangerous, I know...)
             *
             * E.g.: MAX_FORCE_CLAMPING is here false, but in callback() it is true!
             *
             */
            //
            {
                PhantomSetSafetyStatus parameters = new PhantomSetSafetyStatus();

                PhantomSetSafety setSafetyCallback = new PhantomSetSafety();
                try {
                    //scheduleSynchronous DOES NOT wait; that's why a semaphore is here...
                    Scheduler.scheduleSynchronous(setSafetyCallback, parameters, Scheduler.getDefaultPriority());
                    parameters.semaphore.acquire();
                    if (!parameters.success)
                        throw new HapticException("Could not set safety parameters!" +
                            (parameters.error_message == null ? ""
                            : "\n\n" + "Error: " + parameters.error_message));
                } catch (HDException ex) {
                    /* thrown only by scheduleSynchronous itself (not by a callback!) */
                    throw new HapticException("Could not set safety parameters\n\n" +
                        "Error: " + ex);
                }

            }
            {
                PhantomReadParametersStatus forceParams = new PhantomReadParametersStatus();

                /* Read force limitations */
                PhantomReadForceLimitations readParamsCallback = new PhantomReadForceLimitations();

                try {
                    //scheduleSynchronous DOES NOT wait; that's why a semaphore is here...
                    Scheduler.scheduleSynchronous(readParamsCallback, forceParams, Scheduler.getDefaultPriority());
                    forceParams.semaphore.acquire();
                    if (!forceParams.success)
                        throw new HapticException("Could not read device force limitations!");
                } catch (HDException ex) {
                    throw new HapticException("HDException in Scheduler.scheduleSynchronous\n\nError: " + ex);
                }
                LOGGER.info("finished readParamsCallback, code: " + String.valueOf(forceParams.success));
            }

            // callback computing forces (will be called by OpenHaptics approx. 1000 times a second)
            // to be scheduled in onStartUsing()
            visCallback = new VisApiCallback();

        } catch (InterruptedException ex) {
            throw new HapticException("InterruptedException was thrown!\n\nError: \n" + ex.toString());
        }
    }

    protected void refreshCalibrationStatus() throws HapticException
    {

        PhantomReadCalibrationStatus calibrationStat = new PhantomReadCalibrationStatus();
        PhantomReadCalibrationStatusCallback calibrationCallback = new PhantomReadCalibrationStatusCallback();
        try {
            //scheduleSynchronous DOES NOT wait; that's why a semaphore is here...
            Scheduler.scheduleSynchronous(calibrationCallback, calibrationStat, Scheduler.getDefaultPriority());
            //            LOGGER.info("calibration success (before semaphore): " + calibrationStat.success);
            calibrationStat.semaphore.acquire();
            //            LOGGER.info("calibration success (after semaphore): " + calibrationStat.success);
            if (!calibrationStat.success)
                throw new HapticException("Could not check calibration of the device!");
            this.calibrationStatus = calibrationStat.calibrationState;
            this.calibrationStyle = calibrationStat.calibrationStyle;

        } catch (HDException ex) {
            throw new HapticException("Could not check calibration of the device\n\nError: " + ex);
        } catch (InterruptedException ex) {
            throw new HapticException("Could not check calibration of the device (InterruptedException was thrown)!\n\n" +
                "Error: \n" + ex.toString());
        }
    }

    /**
     * Get calibration status of the device.
     * <p/>
     * @return values from openhaptics.hdapi.misc.CalibrationState
     */
    public int getCalibrationStatus() throws HapticException
    {
        refreshCalibrationStatus();
        if (calibrationStatus == -1)
            throw new IllegalStateException("calibrationStatus was not initialized!");
        return calibrationStatus;
    }

    /**
     * Get available calibration styles (may be more than one).
     * <p/>
     * @return values from openhaptics.hdapi.misc.CalibrationStyle
     */
    public int getCalibrationStyles() throws HapticException
    {
        refreshCalibrationStatus();
        if (calibrationStyle == -1)
            throw new IllegalStateException("calibrationStatus was not initialized!");
        return calibrationStyle;
    }

    public void calibrate() throws HapticException
    {

        PhantomCalibrateCallback calibrate = new PhantomCalibrateCallback();
        PhantomCalibrateStatus status = new PhantomCalibrateStatus();
        try {
            //scheduleSynchronous DOES NOT wait; that's why a semaphore is here...
            Scheduler.scheduleSynchronous(calibrate, status, Scheduler.getDefaultPriority());
            status.semaphore.acquire();
            if (!status.success)
                throw new HapticException("Couldn't calibrate the device!");
        } catch (HDException ex) {
            throw new HapticException("Couldn't calibrate the device.\n\n" +
                "Error: " + ex);
        } catch (InterruptedException ex) {
            throw new HapticException("Couldn't calibrate the device (InterruptedException was thrown)!\n\n" +
                "Error: \n" + ex.toString());
        }

    }

    @Override
    public void setForce(Tuple3f force)
    {
        synchronized (hapticVariablesLock) {
            this.force.set(force);
        }
    }

    @Override
    public void getForce(Tuple3f force)
    {
        synchronized (hapticVariablesLock) {
            force.set(this.force);
        }
    }

    @Override
    public void setTorque(Tuple3f torque)
    {
        synchronized (hapticVariablesLock) {
            this.torque.set(torque);
        }
    }

    @Override
    public void getPosition(Tuple3f position)
    {
        position.set(this.position); //synchronization isn't needed because pullData() we assign reference to a new Point3d
    }

    @Override
    public void getVelocity(Tuple3f velocity)
    {
        synchronized (hapticVariablesLock) {
            velocity.set(this.velocity);
        }
    }

    @Override
    public void getTransform(Transform3D transform)
    {
        synchronized (hapticVariablesLock) {
            transform.set(this.transform);
        }
    }

    public int getButtonsMask()
    {
        synchronized (hapticVariablesLock) {
            return this.buttonState;
        }
    }

    @Override
    public int getButton(int no) throws Exception
    {
        int mask;
        switch (no) {
            case 1:
                mask = MiscParams.BUTTON_1;
                break;
            case 2:
                mask = MiscParams.BUTTON_2;
                break;
            case 3:
                mask = MiscParams.BUTTON_3;
                break;
            case 4:
                mask = MiscParams.BUTTON_4;
                break;
            default:
                throw new IllegalArgumentException("Wrong button number: " + no);
        }
        synchronized (hapticVariablesLock) {
            return (this.buttonState & mask) > 0 ? 1 : 0;
        }
    }

    @Override
    public void getButtons(int[] buttons)
    {
        synchronized (hapticVariablesLock) {
            for (int i = 0; i < getButtonsCount(); ++i) {
                try {
                    buttons[i] = getButton(i + 1);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e); // this should never ever happen!
                }

            }
        }
    }

    @Override
    public int getPositionMeanUpdateRate()
    {
        synchronized (hapticVariablesLock) {
            return updateRate;
        }
    }

    @Override
    public int getPositionInstUpdateRate()
    {
        synchronized (hapticVariablesLock) {
            return instUpdateRate;
        }
    }

    @Override
    public int getButtonsCount()
    {
        return BUTTONS_COUNT;
    }

    @Override
    public String getVendor()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getVersion()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getID()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    long tempLastMilisecondsRead = System.currentTimeMillis();

    @Override
    final public double getDeviceNominalMaxContinuousForce() throws HapticException
    {
        synchronized (nominalLock) {
            if (Double.isNaN(nominalMaxContinuousForce)) {
                try {
                    nominalMaxContinuousForce = Device.getDouble(Parameter.NOMINAL_MAX_CONTINUOUS_FORCE);
                } catch (HDException ex) {
                    throw new HapticException(
                        "HDException when trying to get nominal max allowed continuous force " +
                        "(\"NOMINAL_MAX_CONTINUOUS_FORCE\"):\n" +
                        ex.toString());
                }
            }
            return nominalMaxContinuousForce;
        }
    }

    @Override
    final public double getDeviceNominalMaxExertableForce() throws HapticException
    {
        synchronized (nominalLock) {
            if (Double.isNaN(nominalMaxForce)) {
                try {
                    nominalMaxForce = Device.getDouble(Parameter.NOMINAL_MAX_FORCE);
                } catch (HDException ex) {
                    throw new HapticException(
                        "HDException when trying to get nominal max allowed force " +
                        "(\"NOMINAL_MAX_FORCE\"):\n" +
                        ex.toString());
                }
            }
            return nominalMaxForce;
        }
    }

    @Override
    final public double getNominalMaxDamping() throws HapticException
    {
        synchronized (nominalLock) {
            if (Double.isNaN(nominalMaxDamping)) {
                try {
                    nominalMaxDamping = Device.getDouble(Parameter.NOMINAL_MAX_DAMPING) / translationScaling;
                } catch (HDException ex) {
                    throw new HapticException(
                        "HDException when trying to get recommended maximum value " +
                        "of a damping constant:\n" +
                        ex.toString());
                }
            }
            return nominalMaxDamping;
        }
    }

    /* Use those two classes below as a workaround for scheduleSynchronous that does not wait. */
    private class PhantomCallbackStatus
    {

        public boolean success = false;
        public Semaphore semaphore = new Semaphore(0);
    }

    private abstract class PhantomCallback implements SchedulerCallback
    {

        /**
         *
         * @param userData must be of type PhantomCallbackStatus
         * <p>
         * @return the result of a callback (probably CallbackCode.DONE)
         */
        @Override
        public int callback(Object userData)
        {

            PhantomCallbackStatus status = (PhantomCallbackStatus) userData;

            try {
                return mycallback(userData);
            } finally {
                status.semaphore.release(); // that's a workaround for an 
            }
        }

        public abstract int mycallback(Object userData);
    }

    private class PhantomReadParametersStatus extends PhantomCallbackStatus
    {
    }

    private class PhantomReadForceLimitations extends PhantomCallback
    {

        @Override
        public int mycallback(Object userData)
        {
            PhantomReadParametersStatus status = (PhantomReadParametersStatus) userData;
            status.success = false;
            try {
                Device.beginFrame(hhd);

                getDeviceNominalMaxContinuousForce();
                getDeviceNominalMaxExertableForce();

                status.success = true;
            } catch (HDException ex) {
                LOGGER.info(ex);
            } catch (HapticException ex) {
                LOGGER.info(ex);
            }
            try {
                Device.endFrame(hhd);
            } catch (HDException ex) {
                LOGGER.info(ex);
            }
            return CallbackCode.DONE;
        }
    }

    private class PhantomReadCalibrationStatus extends PhantomCallbackStatus
    {

        /**
         * Value from CalibrationState
         */
        public int calibrationState;
        /**
         * Value from CalibrationStyle
         */
        public int calibrationStyle;
    }

    private class PhantomReadCalibrationStatusCallback extends PhantomCallback
    {

        @Override
        public int mycallback(Object userData)
        {

            PhantomReadCalibrationStatus status = (PhantomReadCalibrationStatus) userData;

            status.success = false;
            status.calibrationState = -1;
            try {
                Device.beginFrame(hhd);

                status.calibrationState = Device.checkCalibration();
                status.calibrationStyle = Device.checkCalibrationStyle();
                status.success = true;

            } catch (HDException ex) {
                LOGGER.info(ex);
            }

            try {
                Device.endFrame(hhd);
            } catch (HDException ex) {
                LOGGER.info(ex);
            }
            return CallbackCode.DONE; // callback can be finished now

        }
    }

    private class PhantomSetSafetyStatus extends PhantomCallbackStatus
    {

        String error_message = null;
    }

    private class PhantomSetSafety extends PhantomCallback
    {

        @Override
        public int mycallback(Object userData)
        {
            PhantomSetSafetyStatus status = (PhantomSetSafetyStatus) userData;
            status.success = false;
            try {
                Device.beginFrame(hhd);

                LOGGER.info("=== PhantomSetSafetyParameters =====");

                //TODO: add fatal error when error occured or value wasn't changed!
                boolean value;

                // enable clamping the overall force to the maximum achievable
                value = Device.isEnabled(CapabilityParameter.MAX_FORCE_CLAMPING);
                if (!value) {
                    Device.enable(CapabilityParameter.MAX_FORCE_CLAMPING, true);
                    value = Device.isEnabled(CapabilityParameter.MAX_FORCE_CLAMPING);
                }
                LOGGER.info("MAX_FORCE_CLAMPING: " + value);

                // enable returning an error when maximum achievable force was exceeded
                value = Device.isEnabled(CapabilityParameter.SOFTWARE_FORCE_LIMIT);
                LOGGER.info("SOFTWARE_FORCE_LIMIT: " + value);
                if (!value) {
                    Device.enable(CapabilityParameter.SOFTWARE_FORCE_LIMIT, true);
                    value = Device.isEnabled(CapabilityParameter.SOFTWARE_FORCE_LIMIT);
                    LOGGER.info("SOFTWARE_FORCE_LIMIT: " + value);

                }

                // set velocity limit to 1 m/s
                try {
                    value = Device.isEnabled(Parameter.SOFTWARE_VELOCITY_LIMIT);
                    if (!value) {
                        Device.enable(Parameter.SOFTWARE_VELOCITY_LIMIT, true);
                    }
                    LOGGER.info("HD_SOFTWARE_VELOCITY_LIMIT: " + value);
                    Device.setDouble(Parameter.SOFTWARE_VELOCITY_LIMIT, 1000);
                    double velLimit = Device.getDouble(Parameter.SOFTWARE_VELOCITY_LIMIT);
                    LOGGER.info("Software velocity limit: " + velLimit);
                } catch (HDException ex) {
                    status.error_message = "Setting SOFTWARE_VELOCITY_LIMIT failed!\n\n" +
                        "Error: " + ex;
                    return CallbackCode.DONE;
                }
                //
                // software force impulse check
                // HD_SOFTWARE_FORCE_IMPULSE_LIMIT
                // unit: N/ms
                // ATTENTION: it looks like it doesn't work correctly - it doesn't limit the force :/
                try {
                    value = Device.isEnabled(Parameter.SOFTWARE_FORCE_IMPULSE_LIMIT);
                    if (!value) {
                        Device.enable(Parameter.SOFTWARE_FORCE_IMPULSE_LIMIT, true);
                    }
                    LOGGER.info("SOFTWARE_FORCE_IMPULSE_LIMIT: " + value);
                    Device.setDouble(Parameter.SOFTWARE_FORCE_IMPULSE_LIMIT, 1.5 / 1000);
                    double impulseLimit = Device.getDouble(Parameter.SOFTWARE_FORCE_IMPULSE_LIMIT);
                    LOGGER.info("Software force impulse limit (N/s): " + (1000 * impulseLimit));
                } catch (HDException ex) {
                    status.error_message = "Setting SOFTWARE_FORCE_IMPULSE_LIMIT failed!\n\n" +
                        "Error: " + ex;
                    return CallbackCode.DONE;
                }
                //
                // limit force ramping rate (growing force after scheduler is started or after an error)
                // HD_FORCE_RAMPING_RATE
                // unit: N/s

                // seems like it didn't work either - no result difference was observed between setting it to 0.015 
                // and leaving it on default (0.5)
                try {
                    double dbl = Device.getDouble(Parameter.FORCE_RAMPING_RATE);
                    LOGGER.info("HD_FORCE_RAMPING_RATE = " + dbl);

                    // NOTE: this feature cannot be checked whether it is enabled or not, 
                    // because it results in HDException with INVALID_ENUM. Strange :/
                    //                    value = Device.isEnabled(Parameter.FORCE_RAMPING_RATE);
                    //                    LOGGER.info("HD_FORCE_RAMPING_RATE: " + value);
                    //                    if (!value) {
                    //                        Device.enable(Parameter.FORCE_RAMPING_RATE, true);
                    //                    }
                    Device.setDouble(Parameter.FORCE_RAMPING_RATE, 0.15 / 1000);
                    double rampingRate = Device.getDouble(Parameter.FORCE_RAMPING_RATE);
                    LOGGER.info("Ramping rate (N/s): " + rampingRate);
                } catch (HDException ex) {
                    status.error_message = "Setting FORCE_RAMPING_RATE failed!\n\n" +
                        "Error: " + ex;
                    return CallbackCode.DONE;
                }

                status.success = true;
            } catch (HDException ex) {
                LOGGER.info(ex);
                status.error_message = "Unknown error in PhantomSetSafetyParameters!\n\n" +
                    "Error: " + ex;
                return CallbackCode.DONE;
            }

            try {
                Device.endFrame(hhd);
            } catch (HDException ex) {
                LOGGER.info(ex);
                status.error_message = "Unknown error in PhantomSetSafetyParameters (in endFrame())!\n\n" +
                    "Error: " + ex;
                return CallbackCode.DONE;
            }

            LOGGER.info("=== Finishing PhantomSetSafetyParameters =====");

            return CallbackCode.DONE;
        }
    }

    private class PhantomCalibrateStatus extends PhantomCallbackStatus
    {
    }

    private class PhantomCalibrateCallback extends PhantomCallback
    {

        @Override
        public int mycallback(Object userData)
        {
            PhantomCalibrateStatus status = (PhantomCalibrateStatus) userData;

            try {
                Device.beginFrame(hhd);
                Device.updateCalibration(CalibrationStyle.ENCODER_RESET);
                status.success = true;
            } catch (HDException ex) {
                LOGGER.info(ex);
            }

            try {
                Device.endFrame(hhd);
            } catch (HDException ex) {
                LOGGER.info(ex);
            }

            return CallbackCode.DONE;
        }
    }

    private class VisApiCallback implements SchedulerCallback
    {

        @Override
        public int callback(Object userData)
        {

            try {
                if (support.getNumOfListeners() > 0) {

                    Device.beginFrame(hhd); // begins haptic frame

                    //          Get data from haptic device and store them locally
                    pullData();

                    //          Fire compute force callbacks
                    support.fireDeviceCallback();

                    //          Set forces to a haptic device
                    pushData();

                    Device.endFrame(hhd); // ends haptic frame
                }

            } catch (Throwable thr) {
                //catch all exceptions, so jni code does not have to do it
                //                thr.printStackTrace();
                LOGGER.error(thr.getMessage(), thr);
                //or Thread.getDefaultUncaughtExceptionHandler().uncaught(Thread.current(), thr);
                //but it should be queued for posting (envoking from native Thread)
            }

            return CallbackCode.CONTINUE;

        }
    }

    /**
     * Set forces to a haptic device
     */
    private void pushData()
    {
        synchronized (hapticVariablesLock) {
            try {
                /* set force */
                doubleBuff[0] = force.x;
                doubleBuff[1] = force.y;
                doubleBuff[2] = force.z;
                Device.setDoublev(Parameter.CURRENT_FORCE, doubleBuff);

                /* set torque */
                doubleBuff[0] = torque.x;
                doubleBuff[1] = torque.y;
                doubleBuff[2] = torque.z;
                Device.setDoublev(Parameter.CURRENT_GIMBAL_TORQUE, doubleBuff);
            } catch (HDException ex) {
                LOGGER.error(ex);
            }
        }
    }

    /**
     * Get data from haptic device and store them locally
     */
    private void pullData()
    {

        synchronized (hapticVariablesLock) {

            lastPosition.set(position);

            // UPDATE_RATE has a very short period of measure (it's being refresh many times a second), 
            // so I measure update rate by myself
            ++framesCounter;
            long tempMilisecondsRead = System.currentTimeMillis();
            if (tempMilisecondsRead > nextMilisecondsTime) {
                // update rate
                long milisecondsDiff = tempMilisecondsRead - lastMilisecondsTime;
                updateRate = round((float) framesCounter * 1000 / milisecondsDiff);
                lastMilisecondsTime = tempMilisecondsRead;
                nextMilisecondsTime = tempMilisecondsRead + MILISECONDS_MEASURE_TIME;
                framesCounter = 0;
            }

            try {
                instUpdateRate = Device.getInteger(Parameter.INSTANTANEOUS_UPDATE_RATE);

                /* get buttons */
                buttonState = Device.getInteger(Parameter.CURRENT_BUTTONS);

                /* get transform matrix */
                Device.getDoublev(Parameter.CURRENT_TRANSFORM, doubleBuff);
                transformMatrix[0] = doubleBuff[0];
                transformMatrix[1] = doubleBuff[4];
                transformMatrix[2] = doubleBuff[8];
                transformMatrix[3] = doubleBuff[12] * translationScaling;
                transformMatrix[4] = doubleBuff[1];
                transformMatrix[5] = doubleBuff[5];
                transformMatrix[6] = doubleBuff[9];
                transformMatrix[7] = doubleBuff[13] * translationScaling;
                transformMatrix[8] = doubleBuff[2];
                transformMatrix[9] = doubleBuff[6];
                transformMatrix[10] = doubleBuff[10];
                transformMatrix[11] = doubleBuff[14] * translationScaling;
                transformMatrix[12] = doubleBuff[3];
                transformMatrix[13] = doubleBuff[7];
                transformMatrix[14] = doubleBuff[11];
                transformMatrix[15] = doubleBuff[15];

                Transform3D tmpt = new Transform3D(transformMatrix);
                transform = tmpt;

                Point3d pos = new Point3d();
                transform.transform(pos);
                position = pos;

                /*                
                 * Vector3d pos1 = new Vector3d();
                 * transform.get(pos1); 
                 */

                /* get velocity */
                if (ESTIMATE_VELOCITY) {
                    velocity.set(position);
                    velocity.sub(lastPosition);
                    velocity.scale(instUpdateRate);
                } else {
                    Point3d tmp = new Point3d();
                    Device.getDoublev(Parameter.CURRENT_VELOCITY, doubleBuff);
                    tmp.set(doubleBuff);
                    tmp.scale(translationScaling);
                    velocity = tmp;
                }

            } catch (HDException ex) {
                LOGGER.error(ex);
            }

        }
    }

    final protected float getDeviceParamFloat(Parameter parameter) throws HapticException
    {
        try {
            return Device.getFloat(parameter);
        } catch (HDException ex) {
            throw new HapticException("HDException when trying to get parameter " + parameter.name() +
                " (number #" + parameter.getCode() + "):\n" +
                ex.toString());
        }
    }

    /**
     * Counts Phantom devices that are initialized in OpenHaptics API. Starts a Scheduler if needed
     * (when inializing the first device).
     * <p/>
     * @throws HapticException
     */
    protected final void onPhantomDeviceCreated() throws HapticException
    {

        synchronized (PhantomDeviceOH2.class) {
            if (devicesCount == 0) {
                try {
                    Scheduler.start(); // should be called once, e.g. when devices were initialized, see API Reference Manual
                    LOGGER.info(" -- Phantom scheduler has been started");
                } catch (HDException ex) {
                    if (ex.getErrorCode() == ErrorCode.INVALID_OPERATION.getCode())
                        throw new HapticException(
                            "HDException in Scheduler.start()\n\n" +
                            "Error:" + ex + "\n\n " +
                            "Please double check that you don't have a running program that uses this device.");
                    else
                        throw new HapticException(
                            "HDException in Scheduler.start()\n\n" +
                            "Error:" + ex);
                }
            }
            ++devicesCount;
        }

    }

    protected void onPhantomDeviceClosed()
    {
        synchronized (PhantomDeviceOH2.class) {
            --devicesCount;

            /* if that was last Phantom device, stop the scheduler - it will not be needed anymore */
            if (devicesCount == 0) {
                try {
                    Scheduler.stop(); // should be called once, when uninitializing, see API Reference Manual
                    LOGGER.info(" -- Phantom scheduler has been stopped");
                } catch (HDException e) {
                    //Nothing to do, probably scheduler is not running
                }
            }
        }
    }

    @Override
    protected void onStartUsing() throws HapticException
    {
        try {
            callbackHandle = Scheduler.scheduleAsynchronous(visCallback, null, Scheduler.getDefaultPriority());
        } catch (HDException ex) {
            throw new HapticException("HDException in Scheduler.scheduleAsynchronous\n\nError: " + ex);
        }

    }

    @Override
    protected void onEndUsing() throws HapticException
    {
        try {
            Scheduler.unschedule(callbackHandle);
        } catch (HDException e) {
            //If the scheduled task has already ended - it's ok
            LOGGER.error(e.getMessage(), e);
        }

        --devicesCount;

        //        Device.disableDevice(hhd); // makes java.exe crash - no idea why
        //        context.delete(); // makes java.exe crash - no idea why
    }

    @Override
    public void close()
    {
        destroy();
    }

    /**
     * Should be called when this device will never ever be used again - when DeviceManager is being
     * destroyed.
     */
    public void destroy()
    {
        onPhantomDeviceClosed();
    }
}
