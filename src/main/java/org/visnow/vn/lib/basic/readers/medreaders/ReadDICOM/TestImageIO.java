/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM;

import java.util.Iterator;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.spi.ImageWriterSpi;

public class TestImageIO {

    public static void main(String[] args) {
        System.out.println("---- Java Image I/O Readers ----");
        
        String names[] = ImageIO.getReaderFormatNames();
        for (int i = 0; i < names.length; ++i) {
            System.out.println("" + names[i] + ":");
            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName(names[i]);
            while(it.hasNext()) {
                ImageReader reader = it.next();
                ImageReaderSpi spi = reader.getOriginatingProvider();
                System.out.println("    - "+spi.getDescription(Locale.US)+" "+spi.getVendorName()+" v"+spi.getVersion()+" ("+spi.getPluginClassName()+")");
            }
        }

/*        
        System.out.println("");
        System.out.println(" ----Java Image I/O Writers ----");
        names = ImageIO.getWriterFormatNames();
        for (int i = 0; i < names.length; ++i) {
            System.out.println("" + names[i] + ":");
            Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(names[i]);
            while(it.hasNext()) {
                ImageWriter writer = it.next();
                ImageWriterSpi spi = writer.getOriginatingProvider();
                System.out.println("      "+spi.getDescription(Locale.US)+" "+spi.getVendorName()+" v"+spi.getVersion()+" ("+spi.getPluginClassName()+")");
            }
        }
*/

    }
}
