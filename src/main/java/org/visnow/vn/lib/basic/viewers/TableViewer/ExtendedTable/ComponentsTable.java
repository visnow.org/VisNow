/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.visnow.vn.gui.utils.TableUtilities;
import org.visnow.vn.gui.utils.ExtendedMenuItem;

/**
 * Table that contains the name and statistics of selected components.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class ComponentsTable extends JPanel
{

    private final boolean DEBUG = false;
    private final JTable table;
    private final TableModel tableModel;
    private JPopupMenu popup;
    private ExtendedMenuItem removeSeriesPopupItem;
    private ExtendedMenuItem saveAsCSVSeriesPopupItem;
    private ExtendedMenuItem saveAsHDF5SeriesPopupItem;

    public ComponentsTable()
    {
        super(new GridLayout(1, 0));

        tableModel = new ComponentsTable.TableModel();

        table = new JTable(tableModel);
        TableUtilities.addContentTooltipPopup(table, 0);
        table.setPreferredScrollableViewportSize(new Dimension(450, 300));
        table.setFillsViewportHeight(true);
        table.putClientProperty("terminateEditOnFocusLost", true);
        table.setDefaultEditor(String.class, new FormatEditor());
        table.getTableHeader().setReorderingAllowed(false);
        table.getColumnModel().getColumn(0).setPreferredWidth(120);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        table.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
        createPopupMenu();

    }

    public String[] getRemovedSeriesNames()
    {
        int nSelectedRows = table.getSelectedRowCount();
        boolean isEmptyRow = false;
        if (nSelectedRows > 0) {
            String[] seriesToRemoveNames = new String[nSelectedRows];
            for (int i = 0; i < nSelectedRows; i++) {
                seriesToRemoveNames[i] = table.getValueAt(table.getSelectedRows()[i], 0).toString();
                if (seriesToRemoveNames[i].equals("")) {
                    isEmptyRow = true;
                    break;
                }
            }
            if (!isEmptyRow) {
                return seriesToRemoveNames;
            } else
                return null;
        }
        return null;
    }

    public void addRemoveSeriesActionListener(ActionListener remSeriesAL)
    {
        removeSeriesPopupItem.addActionListener(remSeriesAL);
    }

    public void addSaveAsCSVSeriesActionListener(ActionListener remSeriesAL)
    {
        saveAsCSVSeriesPopupItem.addActionListener(remSeriesAL);
    }

    public void addSaveAsHDF5SeriesActionListener(ActionListener remSeriesAL)
    {
        saveAsHDF5SeriesPopupItem.addActionListener(remSeriesAL);
    }

    public void addSeriesFormatChangedListener(CellEditorListener seriesFormatChangedCEL)
    {
        table.getDefaultEditor(String.class).addCellEditorListener(seriesFormatChangedCEL);
    }

    private void createPopupMenu()
    {
        popup = new JPopupMenu();
        removeSeriesPopupItem = new ExtendedMenuItem("Remove series");
        JMenu sectionsMenu = new JMenu("Save as");
        saveAsCSVSeriesPopupItem = new ExtendedMenuItem("Save as CSV...");
        sectionsMenu.add(saveAsCSVSeriesPopupItem);
        saveAsHDF5SeriesPopupItem = new ExtendedMenuItem("Save as HDF5...");
        sectionsMenu.add(saveAsHDF5SeriesPopupItem);
        popup.add(removeSeriesPopupItem);
        popup.add(sectionsMenu);
        MouseListener popupListener = new PopupListener(popup);
        table.addMouseListener(popupListener);
    }

    public String getSeriesFormat(FormatEditor fEditor)
    {
        return (String) fEditor.getCellEditorValue();
    }

    public int getSelectedRow()
    {
        return table.getSelectedRow();
    }

    public String getSelectedSeriesName()
    {
        return (String) table.getValueAt(table.getSelectedRow(), 0);
    }

    public JTable getTable()
    {
        return this.table;
    }

    public void updateDataTable(Object[][] data)
    {
        if (data.length == 0) {
            tableModel.data = new Object[][]{
                {"", "", "", "", "", ""},};
        } else {
            tableModel.data = new Object[data.length][data[0].length];
            for (int i = 0; i < data.length; i++) {
                tableModel.data[i] = Arrays.copyOf(data[i], data[i].length);
            }
        }
        table.clearSelection();
        table.setModel(tableModel);
    }

    class TableModel extends AbstractTableModel
    {

        private String[] columnNames = {"component",
                                        "format",
                                        "min",
                                        "avg",
                                        "max",
                                        "<html>\u03c3</html>"
        };

        private Object[][] data = {
            {"", "", "", "", "", ""},};

        @Override
        public int getColumnCount()
        {
            return columnNames.length;
        }

        @Override
        public int getRowCount()
        {
            return data.length;
        }

        @Override
        public String getColumnName(int col)
        {
            return columnNames[col];
        }

        @Override
        public Object getValueAt(int row, int col)
        {
            return data[row][col];
        }

        public void setColumnNames(String[] columnNames)
        {
            this.columnNames = columnNames;
        }

        public void setData(Object[][] data)
        {
            this.data = data;
        }

        @Override
        public Class getColumnClass(int c)
        {
            return getValueAt(0, c).getClass();
        }

        @Override
        public boolean isCellEditable(int row, int col)
        {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col == 1) {
                return true;
            }

            return false;
        }

        @Override
        public void setValueAt(Object value, int row, int col)
        {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col +
                    " to " + value +
                    " (an instance of " +
                    value.getClass() + ")");
            }

            data[row][col] = value;
            fireTableCellUpdated(row, col);

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData()
        {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i = 0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j = 0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

    private class PopupListener extends MouseAdapter
    {

        private final JPopupMenu popup;

        PopupListener(JPopupMenu popupMenu)
        {
            popup = popupMenu;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            table.validate();
            if (e.getButton() == java.awt.event.MouseEvent.BUTTON3 && table.getSelectedRow() == -1) {
                int r = table.rowAtPoint(e.getPoint());
                if (r >= 0 && r < table.getRowCount()) {
                    table.setRowSelectionInterval(r, r);
                }
            }
            maybeShowPopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            mousePressed(e);
        }

        private void maybeShowPopup(MouseEvent e)
        {
            if (e.isPopupTrigger() && table.getSelectedRow() != -1) {
                int r = table.rowAtPoint(e.getPoint());
                if (r >= 0 && r < table.getRowCount()) {
                    int x = e.getX();
                    int y = e.getY();
                    popup.show(e.getComponent(), x, y);
                }
            }
        }
    }
}
