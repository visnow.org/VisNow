/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SimpleProjection;

import static org.apache.commons.math3.util.FastMath.*;

import org.visnow.jscic.RegularField;

import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 *
 * Modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl; University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling) since
 * revision 25.
 *
 * Notes: Method of projection codes: 0 = Max 1 = Min 2 = Mean 3 = Normalized
 * mean Hard-coded to make this class independent of former "Params" class.
 */
public class SimpleProjection3D
{

    public static final int METHOD_MAX = 0;
    public static final int METHOD_MIN = 1;
    public static final int METHOD_MEAN = 2;
    public static final int METHOD_NORMMEAN = 3;

    public SimpleProjection3D()
    {
    }

    public static RegularField compute(RegularField inField, int method, int axis0, int axis1)
    {
        RegularField firstProjection = compute(inField, method, axis0);
        switch (axis0) {
            case 0:
                axis1--;
                break;
            case 1:
                if (axis1 == 2) {
                    axis1 = 0;
                } else if (axis1 == 0) {
                    axis1 = 1;
                }
                break;
        }
        return SimpleProjection2D.compute(firstProjection, method, axis1);
    }

    public static RegularField compute(RegularField inField, int method, int axis)
    {
        RegularField outField = null;

        if (inField == null) {
            return outField;
        }

        if (inField.getDims().length != 3) {
            return outField;
        }
        
        if(axis < 0 || axis > 2)
            return null;        

        int[] inDims = inField.getDims();
        int[] outDims = new int[2];
        int u, v;
        if (inDims == null) {
            return outField;
        }

        // Set local variables.
        switch (axis) {
            case 0:
                u = 1;
                v = 2;
                break;
            case 1:
                u = 0;
                v = 2;
                break;
            case 2:
                u = 0;
                v = 1;
                break;
            default:
                u = 0;
                v = 1;
        }
        outDims[0] = inDims[u];
        outDims[1] = inDims[v];
        outField = new RegularField(outDims);

        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];

        for (int i = 0; i < 3; i++) {
            outAffine[0][i] = inAffine[u][i];
            outAffine[1][i] = inAffine[v][i];
            outAffine[2][i] = 0.0f;
            outAffine[3][i] = inAffine[3][i];
        }
        outField.setAffine(outAffine);

        for (int i = 0; i < inField.getNComponents(); i++) {
            switch (inField.getComponent(i).getType()) {
                case FIELD_DATA_BYTE:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                    Object dOut = projection(inField.getComponent(i).getRawArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(dOut, inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] fOutR,
                     fOutI;
                    fOutR = projection(((ComplexDataArray) inField.getComponent(i)).getFloatRealArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    fOutI = projection(((ComplexDataArray) inField.getComponent(i)).getFloatImaginaryArray().getData(), inField.getComponent(i).getVectorLength(), inDims, outDims, axis, method);
                    outField.addComponent(DataArray.create(new ComplexFloatLargeArray(new FloatLargeArray(fOutR), new FloatLargeArray(fOutI)), inField.getComponent(i).getVectorLength(), inField.getComponent(i).getName()));
                    break;
                default:
                /* Not numeric type, like "String". */
            }
        }
        return outField;
    }

    private static Object projection(Object data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        Class componentType = data.getClass().getComponentType();
        if (componentType == Byte.TYPE) {
            return projection((byte[]) data, veclen, inDims, outDims, axis, method);
        } else if (componentType == Short.TYPE) {
            return projection((short[]) data, veclen, inDims, outDims, axis, method);
        } else if (componentType == Integer.TYPE) {
            return projection((int[]) data, veclen, inDims, outDims, axis, method);
        } else if (componentType == Float.TYPE) {
            return projection((float[]) data, veclen, inDims, outDims, axis, method);
        } else if (componentType == Double.TYPE) {
            return projection((double[]) data, veclen, inDims, outDims, axis, method);
        } else {
            throw new IllegalArgumentException("Unsupported array type");
        }
    }

    private static byte[] projection(byte[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 3) {
            return null;
        }
        if (outDims == null || outDims.length != 2) {
            return null;
        }
        if(axis < 0 || axis > 2)
            return null; 
        
        byte[] out = new byte[outDims[0] * outDims[1] * veclen];
        byte[] tmp = new byte[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int y = 0; y < inDims[1]; y++) {
                        off = z * inDims[1] * inDims[0] * veclen + y * inDims[0] * veclen;
                        for (int x = 0; x < inDims[0]; x++) {
                            off2 = x * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[x + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[1] * veclen + y * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 1:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = z * inDims[1] * inDims[0] * veclen + x * veclen;
                        for (int y = 0; y < inDims[1]; y++) {
                            off2 = y * inDims[0] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[y + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                            case 3:
                            case 2:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                for (int y = 0; y < inDims[1]; y++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = y * inDims[0] * veclen + x * veclen;
                        for (int z = 0; z < inDims[2]; z++) {
                            off2 = z * inDims[0] * inDims[1] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[z + v] = data[off2 + v];
                            }
                        }
                        off = y * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
        }

        if (method == 3) {
            int v = (out[0] & 0xff);
            int max = v;
            int min = v;
            for (int i = 0; i < out.length; i++) {
                v = (out[i] & 0xff);
                if (v > max) {
                    max = v;
                }
                if (v < min) {
                    min = v;
                }
            }

            float s = 255.0f / ((float) max - (float) min);
            int newv;
            for (int i = 0; i < out.length; i++) {
                v = (out[i] & 0xff);
                newv = round(s * ((float) v - (float) min));
                out[i] = (byte) newv;
            }
        }

        return out;
    }

    private static int[] projection(int[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 3) {
            return null;
        }
        if (outDims == null || outDims.length != 2) {
            return null;
        }
        if(axis < 0 || axis > 2)
            return null; 
        
        int[] out = new int[outDims[0] * outDims[1] * veclen];
        int[] tmp = new int[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int y = 0; y < inDims[1]; y++) {
                        off = z * inDims[1] * inDims[0] * veclen + y * inDims[0] * veclen;
                        for (int x = 0; x < inDims[0]; x++) {
                            off2 = x * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[x + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[1] * veclen + y * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 1:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = z * inDims[1] * inDims[0] * veclen + x * veclen;
                        for (int y = 0; y < inDims[1]; y++) {
                            off2 = y * inDims[0] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[y + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                for (int y = 0; y < inDims[1]; y++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = y * inDims[0] * veclen + x * veclen;
                        for (int z = 0; z < inDims[2]; z++) {
                            off2 = z * inDims[0] * inDims[1] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[z + v] = data[off2 + v];
                            }
                        }
                        off = y * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return out;
    }

    private static short[] projection(short[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 3) {
            return null;
        }
        if (outDims == null || outDims.length != 2) {
            return null;
        }
        if(axis < 0 || axis > 2)
            return null; 
        
        short[] out = new short[outDims[0] * outDims[1] * veclen];
        short[] tmp = new short[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int y = 0; y < inDims[1]; y++) {
                        off = z * inDims[1] * inDims[0] * veclen + y * inDims[0] * veclen;
                        for (int x = 0; x < inDims[0]; x++) {
                            off2 = x * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[x + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[1] * veclen + y * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 1:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = z * inDims[1] * inDims[0] * veclen + x * veclen;
                        for (int y = 0; y < inDims[1]; y++) {
                            off2 = y * inDims[0] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[y + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                for (int y = 0; y < inDims[1]; y++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = y * inDims[0] * veclen + x * veclen;
                        for (int z = 0; z < inDims[2]; z++) {
                            off2 = z * inDims[0] * inDims[1] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[z + v] = data[off2 + v];
                            }
                        }
                        off = y * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return out;
    }

    private static float[] projection(float[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 3) {
            return null;
        }
        if (outDims == null || outDims.length != 2) {
            return null;
        }
        if(axis < 0 || axis > 2)
            return null; 
        
        float[] out = new float[outDims[0] * outDims[1] * veclen];
        float[] tmp = new float[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int y = 0; y < inDims[1]; y++) {
                        off = z * inDims[1] * inDims[0] * veclen + y * inDims[0] * veclen;
                        for (int x = 0; x < inDims[0]; x++) {
                            off2 = x * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[x + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[1] * veclen + y * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 1:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = z * inDims[1] * inDims[0] * veclen + x * veclen;
                        for (int y = 0; y < inDims[1]; y++) {
                            off2 = y * inDims[0] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[y + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                for (int y = 0; y < inDims[1]; y++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = y * inDims[0] * veclen + x * veclen;
                        for (int z = 0; z < inDims[2]; z++) {
                            off2 = z * inDims[0] * inDims[1] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[z + v] = data[off2 + v];
                            }
                        }
                        off = y * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return out;
    }

    private static double[] projection(double[] data, int veclen, int[] inDims, int[] outDims, int axis, int method)
    {
        if (inDims == null || inDims.length != 3) {
            return null;
        }
        if (outDims == null || outDims.length != 2) {
            return null;
        }
        if(axis < 0 || axis > 2)
            return null; 
        
        double[] out = new double[outDims[0] * outDims[1] * veclen];
        double[] tmp = new double[inDims[axis] * veclen];
        int off;
        int off2;
        switch (axis) {
            case 0:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int y = 0; y < inDims[1]; y++) {
                        off = z * inDims[1] * inDims[0] * veclen + y * inDims[0] * veclen;
                        for (int x = 0; x < inDims[0]; x++) {
                            off2 = x * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[x + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[1] * veclen + y * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 1:
                for (int z = 0; z < inDims[2]; z++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = z * inDims[1] * inDims[0] * veclen + x * veclen;
                        for (int y = 0; y < inDims[1]; y++) {
                            off2 = y * inDims[0] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[y + v] = data[off2 + v];
                            }
                        }
                        off = z * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                for (int y = 0; y < inDims[1]; y++) {
                    for (int x = 0; x < inDims[0]; x++) {
                        off = y * inDims[0] * veclen + x * veclen;
                        for (int z = 0; z < inDims[2]; z++) {
                            off2 = z * inDims[0] * inDims[1] * veclen + off;
                            for (int v = 0; v < veclen; v++) {
                                tmp[z + v] = data[off2 + v];
                            }
                        }
                        off = y * inDims[0] * veclen + x * veclen;
                        switch (method) {
                            case 0:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = max(tmp);
                                }
                                break;
                            case 2:
                            case 3:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = mean(tmp);
                                }
                                break;
                            case 1:
                                for (int v = 0; v < veclen; v++) {
                                    out[off + v] = min(tmp);
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return out;
    }

    private static byte max(byte[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        int max = (int) (in[0] & 0xff);
        int tmp;
        for (int i = 1; i < in.length; i++) {
            tmp = (int) (in[i] & 0xff);
            if (tmp > max) {
                max = tmp;
            }
        }
        return (byte) max;
    }

    private static int max(int[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        int max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max) {
                max = in[i];
            }
        }
        return max;
    }
    
    private static short max(short[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        short max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max) {
                max = in[i];
            }
        }
        return max;
    }

    private static float max(float[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        float max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max) {
                max = in[i];
            }
        }
        return max;
    }

    private static double max(double[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double max = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] > max) {
                max = in[i];
            }
        }
        return max;
    }

    private static byte min(byte[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        int min = (int) (in[0] & 0xff);
        int tmp;
        for (int i = 1; i < in.length; i++) {
            tmp = (int) (in[i] & 0xff);
            if (tmp < min) {
                min = tmp;
            }
        }
        return (byte) min;
    }

    private static int min(int[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        int min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min) {
                min = in[i];
            }
        }
        return min;
    }

    private static short min(short[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        short min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min) {
                min = in[i];
            }
        }
        return min;
    }

    private static float min(float[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        float min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min) {
                min = in[i];
            }
        }
        return min;
    }

    private static double min(double[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double min = in[0];
        for (int i = 1; i < in.length; i++) {
            if (in[i] < min) {
                min = in[i];
            }
        }
        return min;
    }

    private static long sum(byte[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += (long) (in[i] & 0xff);
        }
        return sum;
    }

    private static long sum(int[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }
    
    private static long sum(short[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static double sum(float[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += (double) in[i];
        }
        return sum;
    }

    private static double sum(double[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double sum = 0;
        for (int i = 1; i < in.length; i++) {
            sum += in[i];
        }
        return sum;
    }

    private static byte mean(byte[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = sum(in);
        int mean = (int) round((double) sum / (double) in.length);
        return (byte) mean;
    }

    private static int mean(int[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = sum(in);
        return (int) round((double) sum / (double) in.length);
    }
    
    private static short mean(short[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        long sum = sum(in);
        return (short) round((double) sum / (double) in.length);
    }

    private static float mean(float[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double sum = sum(in);
        return (float) (sum / (double) in.length);
    }

    private static double mean(double[] in)
    {
        if (in == null || in.length < 1) {
            return 0;
        }

        double sum = sum(in);
        return (sum / (double) in.length);
    }

}
