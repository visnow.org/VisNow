/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.material_science.ReadVASP;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class AtomTemplate
{

    /**
     * atomic number (0 for default unknown atom)
     */
    private int number;

    /**
     * atomic symbol (important - use proper upper/lowercase letter - Ca and not CA)
     */
    private String symbol;

    /**
     * full name
     */
    private String name;

    /**
     * atomic mass used in centering routines
     */
    private float mass;

    /**
     * standard radius for ball presentation
     */
    private float vdWRadius;

    /**
     *
     */
    private float ionRadius;

    /**
     * Creates a new instance of AtomTemplate
     */
    public AtomTemplate(int number, float mass, String symbol, String name,
                        float vdWRadius, float ionRadius)
    {
        this.number = number;
        this.mass = mass;
        this.symbol = symbol;
        this.name = name;
        this.mass = 1;
        this.vdWRadius = vdWRadius;
        this.ionRadius = ionRadius;
    }

    public AtomTemplate(int number, String symbol, String name,
                        float vdWRadius, float ionRadius)
    {
        this(number, 1, symbol, name, vdWRadius, ionRadius);
    }

    public AtomTemplate()
    {
        this(0, 1, "du", "dummy", .1f, .1f);
    }

    public String getSymbol()
    {
        return symbol;
    }

    /**
     * Setter for property symbol.
     * <p>
     * @param symbol New value of property symbol.
     *
     */
    public void setSymbol(java.lang.String symbol)
    {
        this.symbol = symbol;
    }

    /**
     * Getter for property number.
     * <p>
     * @return Value of property number.
     *
     */
    public int getNumber()
    {
        return number;
    }

    /**
     * Setter for property number.
     * <p>
     * @param number New value of property number.
     *
     */
    public void setNumber(int number)
    {
        this.number = number;
    }

    /**
     * Getter for property mass.
     * <p>
     * @return Value of property mass.
     *
     */
    public float getMass()
    {
        return mass;
    }

    /**
     * Setter for property mass.
     * <p>
     * @param mass New value of property mass.
     *
     */
    public void setMass(float mass)
    {
        this.mass = mass;
    }

    public float getVdWRadius()
    {
        return (float) vdWRadius;
    }

    public void setVdWRadius(float r)
    {
        vdWRadius = r;
    }

    public float getIonRadius()
    {
        return (float) ionRadius;
    }

    public void setIonRadius(float r)
    {
        ionRadius = r;
    }

}
