/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.ObjectFlow;

import org.visnow.vn.engine.core.ParameterName;

/**
 * This class defines parameters of the time select/animate module
 * <p>
 * @author Jędrzej M. Nowosielski (jnow@icm.edu.pl), University of Warsaw ICM
 */
public class ObjectFlowShared
{
    // PARAMETERS
    public static final ParameterName<Integer> NUMBER_OF_FRAMES = new ParameterName("Number of frames");
    public static final ParameterName<Float> START_TIME = new ParameterName("startTime");
    public static final ParameterName<Float> END_TIME = new ParameterName("endTime");
    public static final ParameterName<Float> TIME_FRAME = new ParameterName("timeFrame");
    public static final ParameterName<Boolean> ADJUSTING = new ParameterName("adjusting");
    public static final ParameterName<Boolean> CONTINUOUS_UPDATE = new ParameterName("continuous update");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    static final ParameterName<String> META_TIME_UNIT = new ParameterName("META timeUnit");
    static final ParameterName<Float> META_START_TIME = new ParameterName("META startTime"); //defined by input field 
    static final ParameterName<Float> META_END_TIME = new ParameterName("META endTime");

}
