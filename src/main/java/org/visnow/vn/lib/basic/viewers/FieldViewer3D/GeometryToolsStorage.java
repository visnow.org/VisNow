/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D;

import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.AngleTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.CenterPointTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.DiameterTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.GeometryTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.LineTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.PointTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.PolygonTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.PolylineTool;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools.RadiusTool;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GeometryToolsStorage
{

    public static final int GEOMETRY_TOOL_POINT = 0;
    public static final int GEOMETRY_TOOL_CENTER_POINT = 1;
    public static final int GEOMETRY_TOOL_LINE = 2;
    public static final int GEOMETRY_TOOL_RADIUS = 3;
    public static final int GEOMETRY_TOOL_DIAMETER = 4;
    public static final int GEOMETRY_TOOL_POLYLINE = 5;
    public static final int GEOMETRY_TOOL_POLYGON = 6;
    public static final int GEOMETRY_TOOL_ANGLE = 7;

    public GeometryToolsStorage()
    {
    }

    public static GeometryTool getGeometryTool(int n)
    {
        switch (n) {
            case GEOMETRY_TOOL_POINT:
                return new PointTool();
            case GEOMETRY_TOOL_CENTER_POINT:
                return new CenterPointTool();
            case GEOMETRY_TOOL_LINE:
                return new LineTool();
            case GEOMETRY_TOOL_RADIUS:
                return new RadiusTool();
            case GEOMETRY_TOOL_DIAMETER:
                return new DiameterTool();
            case GEOMETRY_TOOL_POLYLINE:
                return new PolylineTool();
            case GEOMETRY_TOOL_POLYGON:
                return new PolygonTool();
            case GEOMETRY_TOOL_ANGLE:
                return new AngleTool();
            default:
                return null;
        }
    }

    public static int getGeometryToolType(GeometryTool gt)
    {
        if (gt == null)
            return -1;

        if (gt instanceof PointTool)
            return GEOMETRY_TOOL_POINT;
        if (gt instanceof AngleTool)
            return GEOMETRY_TOOL_ANGLE;
        if (gt instanceof CenterPointTool)
            return GEOMETRY_TOOL_CENTER_POINT;
        if (gt instanceof DiameterTool)
            return GEOMETRY_TOOL_DIAMETER;
        if (gt instanceof LineTool)
            return GEOMETRY_TOOL_LINE;
        if (gt instanceof PolygonTool)
            return GEOMETRY_TOOL_POLYGON;
        if (gt instanceof PolylineTool)
            return GEOMETRY_TOOL_POLYLINE;
        if (gt instanceof RadiusTool)
            return GEOMETRY_TOOL_RADIUS;

        return -1;
    }
}
