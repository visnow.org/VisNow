/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AccumulateTimesteps;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.AccumulateTimesteps.AccumulateTimesteps.State.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.filters.AccumulateTimesteps.AccumulateTimestepsShared.*;
import org.visnow.vn.lib.basic.utilities.Clipboard.Clipboard;
import static org.visnow.vn.lib.utils.field.MergeTimesteps.*;
import static org.visnow.vn.lib.utils.field.MergeTimesteps.Action.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class AccumulateTimesteps extends OutFieldVisualizationModule
{
    public enum State {NO_INPUT, WAITING, PAUSED, ACCUMULATING, BAD_INPUT, EMPTY_SELECTION};

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected Field inField = null;
    protected State state = NO_INPUT;
    protected boolean newField = true;
    protected static int clipboardItem = 0;

    public AccumulateTimesteps()
    {
        parameters.addParameterChangelistener((name) -> {
            if (inField == null)
                return;
            switch (name) {
                case RESET_STRING:
                    if (parameters.get(RESET)) {
                        reset();
                        switchState(WAITING);
                        parameters.set(RESET, false);
                    }
                    break;
                case ACCUMULATE_STRING:
                    if (parameters.get(ACCUMULATE_INPUTS)) {
                        boolean validSelection = false;
                        for (Action action : parameters.get(ACTIONS))
                            if (action != IGNORE)
                                validSelection = true;
                        if (!validSelection)
                            switchState(EMPTY_SELECTION);
                        else if (state == WAITING   || state == PAUSED ||
                                 state == BAD_INPUT || state == EMPTY_SELECTION)
                            switchState(ACCUMULATING);
                        parameters.set(ACCUMULATE_INPUTS, false);
                    }
                    break;
                case PAUSE_STRING:
                    if (parameters.get(PAUSE)) {
                        if (state == ACCUMULATING)
                            switchState(PAUSED);
                        parameters.set(PAUSE, false);
                    }
                    break;
                case TO_CLIPBOARD_STRING:
                    if (parameters.get(TO_CLIPBOARD) && outField != null) {
                        Clipboard.add("accumulated_field_" + clipboardItem, outField);
                        clipboardItem += 1;
                        parameters.set(TO_CLIPBOARD, false);
                    }
                    break;
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParameterProxy(parameters);
            ui.addComputeGUI(computeUI);
            setPanel(ui);
        });
        outObj.setName("accumulated");
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return AccumulateTimestepsShared.createDefaultParameters();
    }

    private void switchState(State newState)
    {
        if (newState == state && (state != WAITING || !newField))
            return;
        if (newState == WAITING && inField != null) {
            setSchema(inField.getSchema(),
                      inField.hasCoords(), inField.hasCoords() && inField.getCoords().getNSteps() > 1,
                      inField.hasMask(), inField.hasMask() && inField.getMask().getNSteps() > 1);
            reset();
            if (computeUI != null)
                computeUI.updateGUI(parameters, true);
        }
        else if (newState == ACCUMULATING && state != PAUSED && inField != null)
            startAction();
        state = newState;
        if (computeUI != null)
            computeUI.setState(state);
    }

    private void reset()
    {
        outField = null;
        setOutputValue("regularOutField", null);
        setOutputValue("irregularOutField", null);
        outObj.clearAllGeometry();
    }

    public void setSchema(DataContainerSchema schema, boolean hasCoords, boolean hasTimeCoords, boolean hasMask, boolean hasTimeMask)
    {
        ArrayList<DataArraySchema> compSchemas = schema.getComponentSchemas();
        int nItems = compSchemas.size();
        if (hasCoords)
            nItems += 1;
        if (hasMask)
            nItems += 1;
        String[] itemNames = new String[nItems];
        Action[] itemActions = new Action[nItems];
        int l = 0;
        for (int i = 0; i < compSchemas.size(); i++, l++) {
            itemNames[l] = compSchemas.get(i).getName();
            if (compSchemas.get(i).getTimeDataSchema().getStartTime() ==
                compSchemas.get(i).getTimeDataSchema().getEndTime())
                itemActions[l] = ACCUMULATE;
            else
                itemActions[l] = MERGE;
        }
        if (hasCoords) {
            itemNames[l] = "coordinates";
            itemActions[l] = hasTimeCoords ? MERGE : ACCUMULATE;
            l += 1;
        }
        if (hasMask) {
            itemNames[l] = "mask";
            itemActions[l] = hasTimeMask ? MERGE : ACCUMULATE;
            l += 1;
        }
        parameters.set(ITEMS, itemNames);
        parameters.set(ACTIONS, itemActions);
            Parameters p = parameters.getReadOnlyClone();
            notifyGUIs(p, false, false);

    }


    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        Field lastIn = inField;
        VNField input = ((VNField) getInputFirstValue("inField"));
        inField = input.getField();
        if (inField == null)
            return;
        newField = inField != lastIn;
        switch (state) {
            case NO_INPUT:
                switchState(WAITING);
                return;
            case WAITING:
                switchState(WAITING);
                return;
            case PAUSED:
            case BAD_INPUT:
                return;
            case ACCUMULATING:
                if (outField == null)
                    outField = init(inField, parameters.get(ITEMS), parameters.get(ACTIONS));
                else if (inField.isStructureCompatibleWith(outField))
                    outField = mergeTimesteps(outField, inField,
                                              parameters.get(ITEMS), parameters.get(ACTIONS), parameters.get(TIME_INCREMENT));
                else {
                    switchState(BAD_INPUT);
                    return;
                }
                break;
        }
        if (outField == null) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
            return;
        }
        switch (outField.getType()) {
        case FIELD_REGULAR:
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
            break;
        case FIELD_IRREGULAR:
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            setOutputValue("outPointField", null);
            break;
        case FIELD_POINT:
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", new VNPointField((PointField) outField));
            break;
        }
        show();
    }
}
