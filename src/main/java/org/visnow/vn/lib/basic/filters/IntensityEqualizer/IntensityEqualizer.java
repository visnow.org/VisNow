/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.IntensityEqualizer;

import javax.swing.event.ChangeEvent;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.IntensityEqualizer.IntensityEqualizerShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class IntensityEqualizer extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private int runQueue = 0;
    private IntensityEqualizerGUI computeUI = null;
    private final IntensityEqualizerCore core = new IntensityEqualizerCore();
    RegularField inField = null;
    boolean componentChanged = false;

    public IntensityEqualizer()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        parameters.get(COMPONENT).addChangeListener((ChangeEvent e) -> {
            componentChanged = true;
            if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                startAction();
        });        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new IntensityEqualizerGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(COMPONENT, new ComponentFeature(true, true, false, false, false, true)),
            new Parameter<>(GAIN, 1.0f),
            new Parameter<>(WEIGHTS, new float[]{1.f}),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(parameters, resetFully, setRunButtonPending);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.get(COMPONENT).setContainerSchema(inField.getSchema());
            int n = IntensityEqualizerCore.estimateNLevels(inField.getDims());
            float[] weights = new float[n];
            for (int i = 0; i < weights.length; i++) {
                weights[i] = 1.0f;
            }                
            parameters.set(WEIGHTS, weights);
            parameters.set(GAIN, 1.0f);
        }
        parameters.setParameterActive(true);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            if(newField == null) {
                inField = null;
                outField = null;
                outRegularField = null;
                setOutputValue("outField", null);
                prepareOutputGeometry();
                show(); 
                return;
            }
            
            boolean isChangedField = !isFromVNA() && (inField == null || newField.getTimestamp() != inField.getTimestamp());           
            inField = newField;
            
            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isChangedField || !newField.isDataCompatibleWith(inField));
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isChangedField, isFromVNA() || isChangedField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0);
                
                if(isFromVNA() || isChangedField || componentChanged) {
                    componentChanged = false;
                    int totalSteps = (int) inField.getNNodes();
                    ProgressAgent progressAgent = getProgressAgent(totalSteps);
                    core.setInField(inField, p.get(COMPONENT).getComponentIndex(), progressAgent);
                }
                
                core.updateOutput(p.get(WEIGHTS), p.get(GAIN), true, true);
                outRegularField = core.getOutField();
                if(outRegularField != null)
                    setOutputValue("outField", new VNRegularField(outRegularField));
                else 
                    setOutputValue("outField", null);
                outField = outRegularField;
                prepareOutputGeometry();
                show();
               
            }
        }
    }
}
