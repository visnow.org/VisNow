/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM;

import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomDirectory;
import com.pixelmed.dicom.DicomDirectoryRecord;
import com.pixelmed.dicom.DicomDirectoryRecordFactory;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.DicomFileUtilities;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.TagFromName;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMShared.*;

/**
 *
 * <p>
 * <p>
 */
public class GUI extends javax.swing.JPanel
{

    private JFileChooser dataFileChooser = new JFileChooser();
    private String lastPath = null;
    private Parameters parameters = null;
    private boolean silent = false;
    private DicomContentFrame frame = new DicomContentFrame();
    private String fileName = "";

    private FileFilter dicomdirFileFilter = new FileFilter()
    {
        @Override
        public boolean accept(File f)
        {
            if (f.isFile()) {
                String path = f.getAbsolutePath();
                String cmp = path.substring(path.lastIndexOf(File.separator) + 1);
                return cmp.equalsIgnoreCase("dicomdir");
            } else {
                return true;
            }
        }

        @Override
        public String getDescription()
        {
            String desc = "DICOMDIR file";
            return desc;
        }
    };

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        dataFileChooser.setLocation(0, 0);
//        silent = true;
//        updateGUI();
//        silent = false;

        frame.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                parameters.setParameterActive(false);
                if (frame.getCurrentNameList() != null) {
                    if (frame.getSelectedString() != null && frame.getSelectedString().length() > 1) {
                        if (fileName.length() < 1 && frame.getCurrentNameList().size() == 1) {
                            parameters.set(INFO_STRING, "" + frame.getCurrentNameList().get(0) + "" + frame.getSelectedString());
                        } else if (fileName.length() > 0) {
                            parameters.set(INFO_STRING, "" + fileName + "" + frame.getSelectedString());
                        } else {
                            parameters.set(INFO_STRING, "" + parameters.get(DIR_PATH) + "" + frame.getSelectedString());
                        }
                    }
                    parameters.set(READ_AS_VOLUME, true);
                    parameters.set(FRAMES_AS_TIME, frame.isFramesAsTime());
                    if (frame.isFramesAsTime())
                        parameters.set(FRAMES_RANGE, frame.getFramesRange());
                    else
                        parameters.set(FRAMES_RANGE, null);

                    ArrayList<String> fileList = frame.getCurrentNameList();
                    if (fileList == null) parameters.set(FILE_LIST, null);
                    else parameters.set(FILE_LIST, fileList.toArray(new String[fileList.size()]));
                }
                parameters.setParameterActive(true);
                parameters.fireParameterChanged(null);
            }
        });
        if ("meter".equalsIgnoreCase(VisNow.get().getMainConfig().getDefaultLengthUnit()))
            scaleMRB.setSelected(true);
        else if ("centimeter".equalsIgnoreCase(VisNow.get().getMainConfig().getDefaultLengthUnit()))
            scaleCMRB.setSelected(true);
        else
            scaleMMRB.setSelected(true);
        frame.centerOnScreen();

        downsizeUI.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                if (!silent) {
                    parameters.setParameterActive(false);
                    parameters.set(DOWNSIZE, downsizeUI.getDownsize());
                    parameters.setParameterActive(true);
                }
            }
        });
    }
    
    public void simplifyUI()
    {
        readAsPanel.setVisible(false);
        downsizeUI.setVisible(false);
        sliceDenisingLevelSlider.setVisible(false);
        histoPanel.setVisible(false);
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    protected void updateGUI(final ParameterProxy p)
    {

        if (p.get(DIR_PATH).length() > 0 && p.get(FILE_LIST).length > 0) {
            ArrayList<String> fileList = new ArrayList<>();
            String dirPath = p.get(DIR_PATH);
            boolean dicomdirExists = true;
            File dicomdir = new File(dirPath + File.separator + "DICOMDIR");
            if(!dicomdir.exists()) {
                dicomdir = new File(dirPath + File.separator + "dicomdir");
                if(!dicomdir.exists())
                    dicomdirExists = false;
            }
            if (dicomdirExists) {
                fileName = dicomdir.getAbsolutePath();
                AttributeList atl = new AttributeList();
                DicomDirectory dd;
                try {
                    atl.read(fileName, null, true, true);
                    dd = new DicomDirectory(atl);
                    frame.setDicomTreeModel(dd, fileName.substring(0, fileName.lastIndexOf(File.separator)));
                    showDicomWindowButton.setEnabled(true);
                } catch (IOException ex) {
                    showDicomWindowButton.setEnabled(false);
                } catch (DicomException ex) {
                    showDicomWindowButton.setEnabled(false);
                }
            } else {
                String[] files = p.get(FILE_LIST);
                for (int i = 0; i < files.length; i++) {
                    File file = new File(files[i]);
                    if (!DicomFileUtilities.isDicomOrAcrNemaFile(file.getAbsolutePath())) {
                        System.out.println("INFO: File " + file.getAbsolutePath() + " is not a DICOM compatible file - skipping");
                        continue;
                    }

                    if (!file.exists() || file.getName().toLowerCase().equals("dicomdir")) {
                        continue;
                    }

                    fileList.add(file.getAbsolutePath());
                }

                if (fileList.size() > 0) {
                    try {
                        DicomDirectoryRecordFactory ddrf = new DicomDirectoryRecordFactory();
                        DicomDirectoryRecord[] images = new DicomDirectoryRecord[fileList.size()];
                        for (int i = 0; i < images.length; i++) {
                            File f = new File(fileList.get(i));
                            if (!f.exists()) {
                                images[i] = null;
                                continue;
                            }

                            AttributeList atl = new AttributeList();
                            DicomInputStream in = new DicomInputStream(f);
                            atl.read(in, TagFromName.PixelData);

                            Attribute att = atl.putNewAttribute(TagFromName.ReferencedFileID);
                            att.setValue(f.getName());

                            images[i] = ddrf.getNewImageDirectoryRecord(null, atl);
                        }

                        boolean one = false;
                        for (int i = 0; i < images.length; i++) {
                            if (images[i] != null) {
                                one = true;
                                break;
                            }
                        }
                        if (!one) {
                            showDicomWindowButton.setEnabled(false);
                        } else {
                            frame.setDicomTreeModel(new SimplifiedDicomDirectory(images, dirPath), dirPath);
                            showDicomWindowButton.setEnabled(true);
                        }
                    } catch (DicomException ex) {
                        showDicomWindowButton.setEnabled(false);
                    } catch (IOException ex) {
                        showDicomWindowButton.setEnabled(false);
                    }
                } else {
                    showDicomWindowButton.setEnabled(false);
                }
            }
        } else {
            showDicomWindowButton.setEnabled(false);
        }
        lowCropTF.setEnabled(false);
        highCropTF.setEnabled(false);
        inpaintMissingSlicesCB.setEnabled(false);
        if (p == null) {
            bytesDataRB.setEnabled(false);
            autoDataRB.setEnabled(false);
            histoDataRB.setEnabled(false);
            windowDataRB.setEnabled(false);
            readAsStackCB.setEnabled(false);
            scaleMMRB.setEnabled(false);
            scaleMRB.setEnabled(false);
            positionCenterRB.setEnabled(false);
            positionOriginalRB.setEnabled(false);
            positionZeroRB.setEnabled(false);
            return;
        }
        lowCropTF.setText(p.get(LOW).toString());
        highCropTF.setText(p.get(HIGH).toString());

        bytesDataRB.setEnabled(true);
        autoDataRB.setEnabled(true);
        histoDataRB.setEnabled(true);
        windowDataRB.setEnabled(true);
        inpaintMissingSlicesCB.setEnabled(true);
        readAsStackCB.setEnabled(true);

        scaleCMRB.setEnabled(true);
        scaleMMRB.setEnabled(true);
        scaleMRB.setEnabled(true);
        if (p.get(LENGTH_UNIT) == ReadDICOMShared.UNIT_M) {
            scaleMRB.setSelected(true);
        } else if (p.get(LENGTH_UNIT) == ReadDICOMShared.UNIT_CM) {
            scaleCMRB.setSelected(true);
        } else {
            scaleMMRB.setSelected(true);
        }

        positionCenterRB.setEnabled(true);
        positionOriginalRB.setEnabled(true);
        positionZeroRB.setEnabled(true);
        switch (p.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                positionCenterRB.setSelected(true);
                break;
            case ReadDICOMShared.POSITION_ORIGINAL:
                positionOriginalRB.setSelected(true);
                break;
            case ReadDICOMShared.POSITION_ZERO:
                positionZeroRB.setSelected(true);
                break;
        }

        switch (p.get(READ_AS)) {
            case ReadDICOMShared.READ_AS_AUTO:
                autoDataRB.setSelected(true);
                break;
            case ReadDICOMShared.READ_AS_BYTES:
                bytesDataRB.setSelected(true);
                lowCropTF.setEnabled(true);
                highCropTF.setEnabled(true);
                break;
            case ReadDICOMShared.READ_AS_HISTOGRAM:
                histoDataRB.setSelected(true);
                lowCropTF.setEnabled(true);
                highCropTF.setEditable(false);
                break;
            case ReadDICOMShared.READ_AS_WINDOW:
                windowDataRB.setSelected(true);
                break;
        }

        interpolateDataCB.setSelected(p.get(INTERPOLATE_DATA));
        interpolateManualTF.setEnabled(false);
        if (p.get(INTERPOLATE_DATA)) {
            interpolateManualRB.setEnabled(true);
            interpolatePixelSizeRB.setEnabled(true);
            interpolateSliceDistRB.setEnabled(true);
            switch (p.get(INTERPOLATE_DATA_VOXEL_SIZE_FROM)) {
                case ReadDICOMShared.VOXELSIZE_FROM_MANUALVALUE:
                    interpolateManualRB.setSelected(true);
                    interpolateManualTF.setText("" + p.get(INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE));
                    interpolateManualTF.setEnabled(true);
                    break;
                case ReadDICOMShared.VOXELSIZE_FROM_PIXELSIZE:
                    interpolatePixelSizeRB.setSelected(true);
                    break;
                case ReadDICOMShared.VOXELSIZE_FROM_SLICESDISTANCE:
                    interpolateSliceDistRB.setSelected(true);
                    break;
            }
        } else {
            interpolateManualRB.setEnabled(false);
            interpolatePixelSizeRB.setEnabled(false);
            interpolateSliceDistRB.setEnabled(false);
            interpolateManualTF.setEnabled(false);
        }

        sliceDenisingLevelSlider.setValue(p.get(SLICE_DENOISING_LEVEL));

        inpaintMissingSlicesCB.setSelected(p.get(INPAINT_MISSING_SLICES));
        downsizeUI.setDownsize(p.get(DOWNSIZE));

        readAsStackCB.setSelected(p.get(IGNORE_ORIENTATION));
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        openDICOMDIRButton = new javax.swing.JButton();
        openDICOMFilesButton = new javax.swing.JButton();
        geometryPanel = new javax.swing.JPanel();
        scaleMRB = new javax.swing.JRadioButton();
        scaleMMRB = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        positionZeroRB = new javax.swing.JRadioButton();
        positionCenterRB = new javax.swing.JRadioButton();
        positionOriginalRB = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        scaleCMRB = new javax.swing.JRadioButton();
        readAsPanel = new javax.swing.JPanel();
        autoDataRB = new javax.swing.JRadioButton();
        bytesDataRB = new javax.swing.JRadioButton();
        histoDataRB = new javax.swing.JRadioButton();
        windowDataRB = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lowCropTF = new javax.swing.JTextField();
        highCropTF = new javax.swing.JTextField();
        interpolatePanel = new javax.swing.JPanel();
        interpolateDataCB = new javax.swing.JCheckBox();
        interpolatePixelSizeRB = new javax.swing.JRadioButton();
        interpolateSliceDistRB = new javax.swing.JRadioButton();
        interpolateManualRB = new javax.swing.JRadioButton();
        interpolateManualTF = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        inpaintMissingSlicesCB = new javax.swing.JCheckBox();
        sliceDenisingLevelSlider = new javax.swing.JSlider();
        downsizeUI = new org.visnow.vn.lib.gui.DownsizeUI();
        histoPanel = new javax.swing.JPanel();
        histoArea = new org.visnow.vn.lib.gui.HistoArea();
        applyButton = new javax.swing.JButton();
        histoLogCB = new javax.swing.JCheckBox();
        showDicomWindowButton = new javax.swing.JButton();
        readAsStackCB = new javax.swing.JCheckBox();
        subdirectoryButton = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        openDICOMDIRButton.setText("Open DICOMDIR");
        openDICOMDIRButton.setMinimumSize(new java.awt.Dimension(94, 20));
        openDICOMDIRButton.setPreferredSize(new java.awt.Dimension(94, 22));
        openDICOMDIRButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openDICOMDIRButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 6);
        add(openDICOMDIRButton, gridBagConstraints);

        openDICOMFilesButton.setText("Open DICOM file(s)");
        openDICOMFilesButton.setMinimumSize(new java.awt.Dimension(94, 20));
        openDICOMFilesButton.setPreferredSize(new java.awt.Dimension(94, 22));
        openDICOMFilesButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openDICOMFilesButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 7, 6);
        add(openDICOMFilesButton, gridBagConstraints);

        geometryPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Geometry"));
        geometryPanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup3.add(scaleMRB);
        scaleMRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        scaleMRB.setText("m");
        scaleMRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                scaleMRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        geometryPanel.add(scaleMRB, gridBagConstraints);

        buttonGroup3.add(scaleMMRB);
        scaleMMRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        scaleMMRB.setSelected(true);
        scaleMMRB.setText("mm");
        scaleMMRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                scaleMMRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        geometryPanel.add(scaleMMRB, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel4.setText("Space unit:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 4, 0, 0);
        geometryPanel.add(jLabel4, gridBagConstraints);

        buttonGroup4.add(positionZeroRB);
        positionZeroRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        positionZeroRB.setText("0");
        positionZeroRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                positionZeroRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        geometryPanel.add(positionZeroRB, gridBagConstraints);

        buttonGroup4.add(positionCenterRB);
        positionCenterRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        positionCenterRB.setText("center");
        positionCenterRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                positionCenterRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        geometryPanel.add(positionCenterRB, gridBagConstraints);

        buttonGroup4.add(positionOriginalRB);
        positionOriginalRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        positionOriginalRB.setSelected(true);
        positionOriginalRB.setText("original");
        positionOriginalRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                positionOriginalRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        geometryPanel.add(positionOriginalRB, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel5.setText("Position:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 4, 0, 0);
        geometryPanel.add(jLabel5, gridBagConstraints);

        buttonGroup3.add(scaleCMRB);
        scaleCMRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        scaleCMRB.setText("cm");
        scaleCMRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                scaleCMRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        geometryPanel.add(scaleCMRB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(geometryPanel, gridBagConstraints);

        readAsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Read data type:"));
        readAsPanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(autoDataRB);
        autoDataRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        autoDataRB.setSelected(true);
        autoDataRB.setText("auto");
        autoDataRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                autoDataRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        readAsPanel.add(autoDataRB, gridBagConstraints);

        buttonGroup1.add(bytesDataRB);
        bytesDataRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        bytesDataRB.setText("bytes");
        bytesDataRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bytesDataRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        readAsPanel.add(bytesDataRB, gridBagConstraints);

        buttonGroup1.add(histoDataRB);
        histoDataRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        histoDataRB.setText("histogram equalization");
        histoDataRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                histoDataRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        readAsPanel.add(histoDataRB, gridBagConstraints);

        buttonGroup1.add(windowDataRB);
        windowDataRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        windowDataRB.setText("DICOM embeded window");
        windowDataRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                windowDataRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        readAsPanel.add(windowDataRB, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel1.setText("Low crop:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jLabel1, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel2.setText("High crop:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jLabel2, gridBagConstraints);

        lowCropTF.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        lowCropTF.setEnabled(false);
        lowCropTF.setMinimumSize(new java.awt.Dimension(40, 19));
        lowCropTF.setPreferredSize(new java.awt.Dimension(40, 19));
        lowCropTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                lowCropTFActionPerformed(evt);
            }
        });
        lowCropTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                lowCropTFFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel1.add(lowCropTF, gridBagConstraints);

        highCropTF.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        highCropTF.setEnabled(false);
        highCropTF.setMinimumSize(new java.awt.Dimension(40, 19));
        highCropTF.setPreferredSize(new java.awt.Dimension(40, 19));
        highCropTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                highCropTFActionPerformed(evt);
            }
        });
        highCropTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                highCropTFFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel1.add(highCropTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        readAsPanel.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(readAsPanel, gridBagConstraints);

        interpolatePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Data interpolation"));
        interpolatePanel.setLayout(new java.awt.GridBagLayout());

        interpolateDataCB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        interpolateDataCB.setText("interpolate data to regular mesh");
        interpolateDataCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                interpolateDataCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        interpolatePanel.add(interpolateDataCB, gridBagConstraints);

        buttonGroup2.add(interpolatePixelSizeRB);
        interpolatePixelSizeRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        interpolatePixelSizeRB.setText("pixel size");
        interpolatePixelSizeRB.setMaximumSize(new java.awt.Dimension(68, 16));
        interpolatePixelSizeRB.setMinimumSize(new java.awt.Dimension(68, 16));
        interpolatePixelSizeRB.setPreferredSize(new java.awt.Dimension(68, 16));
        interpolatePixelSizeRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                interpolatePixelSizeRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        interpolatePanel.add(interpolatePixelSizeRB, gridBagConstraints);

        buttonGroup2.add(interpolateSliceDistRB);
        interpolateSliceDistRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        interpolateSliceDistRB.setText("slice distance");
        interpolateSliceDistRB.setMaximumSize(new java.awt.Dimension(88, 16));
        interpolateSliceDistRB.setMinimumSize(new java.awt.Dimension(88, 16));
        interpolateSliceDistRB.setName(""); // NOI18N
        interpolateSliceDistRB.setPreferredSize(new java.awt.Dimension(88, 16));
        interpolateSliceDistRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                interpolateSliceDistRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        interpolatePanel.add(interpolateSliceDistRB, gridBagConstraints);

        buttonGroup2.add(interpolateManualRB);
        interpolateManualRB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        interpolateManualRB.setText("manual");
        interpolateManualRB.setMaximumSize(new java.awt.Dimension(66, 16));
        interpolateManualRB.setMinimumSize(new java.awt.Dimension(61, 16));
        interpolateManualRB.setPreferredSize(new java.awt.Dimension(61, 16));
        interpolateManualRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                interpolateManualRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        interpolatePanel.add(interpolateManualRB, gridBagConstraints);

        interpolateManualTF.setMinimumSize(new java.awt.Dimension(4, 16));
        interpolateManualTF.setPreferredSize(new java.awt.Dimension(40, 16));
        interpolateManualTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                interpolateManualTFActionPerformed(evt);
            }
        });
        interpolateManualTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                interpolateManualTFFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
        interpolatePanel.add(interpolateManualTF, gridBagConstraints);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel3.setText("[mm]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(6, 2, 0, 5);
        interpolatePanel.add(jLabel3, gridBagConstraints);

        inpaintMissingSlicesCB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        inpaintMissingSlicesCB.setText("inpaint missing slices");
        inpaintMissingSlicesCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                inpaintMissingSlicesCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        interpolatePanel.add(inpaintMissingSlicesCB, gridBagConstraints);

        sliceDenisingLevelSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        sliceDenisingLevelSlider.setMajorTickSpacing(1);
        sliceDenisingLevelSlider.setMaximum(5);
        sliceDenisingLevelSlider.setMinorTickSpacing(1);
        sliceDenisingLevelSlider.setPaintLabels(true);
        sliceDenisingLevelSlider.setPaintTicks(true);
        sliceDenisingLevelSlider.setToolTipText("");
        sliceDenisingLevelSlider.setValue(0);
        sliceDenisingLevelSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "slice denoising level", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        sliceDenisingLevelSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                sliceDenisingLevelSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        interpolatePanel.add(sliceDenisingLevelSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(interpolatePanel, gridBagConstraints);

        downsizeUI.setMinimumSize(new java.awt.Dimension(120, 70));
        downsizeUI.setPreferredSize(new java.awt.Dimension(200, 70));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(downsizeUI, gridBagConstraints);

        histoPanel.setLayout(new java.awt.BorderLayout());

        histoArea.setMinimumSize(new java.awt.Dimension(180, 120));
        histoArea.setPreferredSize(new java.awt.Dimension(200, 150));
        histoPanel.add(histoArea, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(histoPanel, gridBagConstraints);

        applyButton.setText("Apply");
        applyButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                applyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 3, 5, 3);
        add(applyButton, gridBagConstraints);

        histoLogCB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        histoLogCB.setText("log values histogram");
        histoLogCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                histoLogCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        add(histoLogCB, gridBagConstraints);

        showDicomWindowButton.setText("Show DICOM window");
        showDicomWindowButton.setEnabled(false);
        showDicomWindowButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showDicomWindowButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 6, 5);
        add(showDicomWindowButton, gridBagConstraints);

        readAsStackCB.setText("read as stack (ignore orientation)");
        readAsStackCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                readAsStackCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(readAsStackCB, gridBagConstraints);

        subdirectoryButton.setText("Read all DICOMSs from directory");
        subdirectoryButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                subdirectoryButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 7, 6);
        add(subdirectoryButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void openDICOMDIRButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_openDICOMDIRButtonActionPerformed
    {//GEN-HEADEREND:event_openDICOMDIRButtonActionPerformed
        dataFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        dataFileChooser.setMultiSelectionEnabled(false);
        dataFileChooser.setFileFilter(dicomdirFileFilter);

        if (lastPath == null) {
            dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadDICOM.class)));
        } else {
            dataFileChooser.setCurrentDirectory(new File(lastPath));
        }

        int returnVal = dataFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fileName = dataFileChooser.getSelectedFile().getAbsolutePath();
            lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadDICOM.class);
            if (fileName == null || fileName.length() < 1 || fileName.equals(" ")) {
                return;
            }

            File f = new File(fileName);
            if (!f.exists() || !f.getName().toLowerCase().equals("dicomdir")) {
                return;
            }

            if (!DicomFileUtilities.isDicomOrAcrNemaFile(fileName)) {
                System.out.println("INFO: Selected file is not a DICOM compatible file!");
                parameters.set(FILE_LIST, new String[]{});
                return;
            }

            String dirPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
            parameters.setParameterActive(false);
            parameters.set(DIR_PATH, dirPath);
            parameters.setParameterActive(true);

            AttributeList atl = new AttributeList();
            DicomDirectory dd;
            try {
                atl.read(fileName, null, true, true);
                dd = new DicomDirectory(atl);
                frame.setDicomTreeModel(dd, fileName.substring(0, fileName.lastIndexOf(File.separator)));
                showDicomWindowButton.setEnabled(true);
                frame.setVisible(true);
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (DicomException ex) {
                ex.printStackTrace();
            }
        } else {
            fileName = "";
            //showDicomWindowButton.setEnabled(false);
        }
    }//GEN-LAST:event_openDICOMDIRButtonActionPerformed

    private void openDICOMFilesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openDICOMFilesButtonActionPerformed
        dataFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        dataFileChooser.setMultiSelectionEnabled(true);
        dataFileChooser.resetChoosableFileFilters();
        if (lastPath == null) {
            dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadDICOM.class)));
        } else {
            dataFileChooser.setCurrentDirectory(new File(lastPath));
        }

        int returnVal = dataFileChooser.showOpenDialog(this);
        String dirPath = "";
        ArrayList<String> fileList = new ArrayList<String>();
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] files = dataFileChooser.getSelectedFiles();
            if (files == null || files.length < 1) {
                files = new File[1];
                files[0] = dataFileChooser.getSelectedFile();
            }
            lastPath = files[0].getAbsolutePath().substring(0, files[0].getAbsolutePath().lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadDICOM.class);
            dirPath = files[0].getAbsolutePath().substring(0, files[0].getAbsolutePath().lastIndexOf(File.separator));
            parameters.setParameterActive(false);
            parameters.set(DIR_PATH, dirPath);
            parameters.setParameterActive(true);

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (!DicomFileUtilities.isDicomOrAcrNemaFile(files[i].getAbsolutePath())) {
                        System.out.println("INFO: File " + files[i].getAbsolutePath() + " is not a DICOM compatible file - skipping");
                        continue;
                    }

                    if (!files[i].exists() || files[i].getName().toLowerCase().equals("dicomdir")) {
                        continue;
                    }

                    fileList.add(files[i].getAbsolutePath());
                }

                if (fileList.size() < 1) {
                    return;
                }

                try {
                    SimplifiedDicomDirectory sdd = null;
                    DicomDirectoryRecordFactory ddrf = new DicomDirectoryRecordFactory();
                    DicomDirectoryRecord[] images = new DicomDirectoryRecord[fileList.size()];
                    for (int i = 0; i < images.length; i++) {
                        File f = new File(fileList.get(i));
                        if (!f.exists()) {
                            images[i] = null;
                            continue;
                        }

                        AttributeList atl = new AttributeList();
                        DicomInputStream in = new DicomInputStream(f);
                        atl.read(in, TagFromName.PixelData);

                        Attribute att = atl.putNewAttribute(TagFromName.ReferencedFileID);
                        att.setValue(f.getName());

                        images[i] = ddrf.getNewImageDirectoryRecord(null, atl);
                    }

                    boolean one = false;
                    for (int i = 0; i < images.length; i++) {
                        if (images[i] != null) {
                            one = true;
                            break;
                        }
                    }
                    if (!one)
                        return;

                    frame.setDicomTreeModel(new SimplifiedDicomDirectory(images, dirPath), dirPath);
                    showDicomWindowButton.setEnabled(true);
                    frame.setVisible(true);
                } catch (DicomException ex) {
                    ex.printStackTrace();
                    return;
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return;
                }
            }
        } else {
            showDicomWindowButton.setEnabled(false);
        }
    }//GEN-LAST:event_openDICOMFilesButtonActionPerformed

    private void autoDataRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoDataRBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
        lowCropTF.setEnabled(false);
        highCropTF.setEnabled(false);
        parameters.setParameterActive(true);
    }//GEN-LAST:event_autoDataRBActionPerformed

    private void bytesDataRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bytesDataRBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(READ_AS, ReadDICOMShared.READ_AS_BYTES);
        lowCropTF.setEnabled(true);
        highCropTF.setEnabled(true);
        parameters.setParameterActive(true);
    }//GEN-LAST:event_bytesDataRBActionPerformed

    private void histoDataRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_histoDataRBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(READ_AS, ReadDICOMShared.READ_AS_HISTOGRAM);
        lowCropTF.setEnabled(false);
        highCropTF.setEnabled(true);
        parameters.setParameterActive(true);
    }//GEN-LAST:event_histoDataRBActionPerformed

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        parameters.setParameterActive(true);
        parameters.fireParameterChanged(null);
    }//GEN-LAST:event_applyButtonActionPerformed

    private void highCropTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_highCropTFActionPerformed
        parameters.setParameterActive(false);
        try {
            int v = Integer.parseInt(highCropTF.getText());
            parameters.set(HIGH, v);
        } catch (Exception ex) {
            highCropTF.setText("" + parameters.get(HIGH));
        }
        parameters.setParameterActive(true);
    }//GEN-LAST:event_highCropTFActionPerformed

    private void lowCropTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lowCropTFActionPerformed
        parameters.setParameterActive(false);
        try {
            int v = Integer.parseInt(lowCropTF.getText());
            parameters.set(LOW, v);
        } catch (Exception ex) {
            lowCropTF.setText("" + parameters.get(LOW));
        }
        parameters.setParameterActive(true);
    }//GEN-LAST:event_lowCropTFActionPerformed

    private void highCropTFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_highCropTFFocusLost
        highCropTFActionPerformed(null);
    }//GEN-LAST:event_highCropTFFocusLost

    private void lowCropTFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_lowCropTFFocusLost
        lowCropTFActionPerformed(null);
    }//GEN-LAST:event_lowCropTFFocusLost

    private void histoLogCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_histoLogCBActionPerformed
        histoArea.setLogScale(histoLogCB.isSelected());
    }//GEN-LAST:event_histoLogCBActionPerformed

    private void windowDataRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_windowDataRBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(READ_AS, ReadDICOMShared.READ_AS_WINDOW);
        lowCropTF.setEnabled(false);
        highCropTF.setEnabled(false);
        parameters.setParameterActive(true);
    }//GEN-LAST:event_windowDataRBActionPerformed

    private void showDicomWindowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showDicomWindowButtonActionPerformed
        if (frame != null) {
            frame.setVisible(true);
        }
    }//GEN-LAST:event_showDicomWindowButtonActionPerformed

    private void interpolateDataCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpolateDataCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(INTERPOLATE_DATA, interpolateDataCB.isSelected());
        parameters.setParameterActive(true);
        if (interpolateDataCB.isSelected()) {
            interpolateManualRB.setEnabled(true);
            interpolatePixelSizeRB.setEnabled(true);
            interpolateSliceDistRB.setEnabled(true);
            interpolateManualTF.setEnabled(true);
        } else {
            interpolateManualRB.setEnabled(false);
            interpolatePixelSizeRB.setEnabled(false);
            interpolateSliceDistRB.setEnabled(false);
            interpolateManualTF.setEnabled(false);
        }
    }//GEN-LAST:event_interpolateDataCBActionPerformed

    private void interpolatePixelSizeRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpolatePixelSizeRBActionPerformed
        if (interpolatePixelSizeRB.isSelected()) {
            parameters.setParameterActive(false);
            parameters.set(INTERPOLATE_DATA_VOXEL_SIZE_FROM, ReadDICOMShared.VOXELSIZE_FROM_PIXELSIZE);
            parameters.setParameterActive(true);
        }
    }//GEN-LAST:event_interpolatePixelSizeRBActionPerformed

    private void interpolateSliceDistRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpolateSliceDistRBActionPerformed
        if (interpolateSliceDistRB.isSelected()) {
            parameters.setParameterActive(false);
            parameters.set(INTERPOLATE_DATA_VOXEL_SIZE_FROM, ReadDICOMShared.VOXELSIZE_FROM_SLICESDISTANCE);
            parameters.setParameterActive(true);
        }
    }//GEN-LAST:event_interpolateSliceDistRBActionPerformed

    private void interpolateManualRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpolateManualRBActionPerformed
        if (interpolateManualRB.isSelected()) {
            parameters.setParameterActive(false);
            parameters.set(INTERPOLATE_DATA_VOXEL_SIZE_FROM, ReadDICOMShared.VOXELSIZE_FROM_MANUALVALUE);
            parameters.setParameterActive(true);
        }
    }//GEN-LAST:event_interpolateManualRBActionPerformed

    private void interpolateManualTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpolateManualTFActionPerformed
        try {
            float v = Float.parseFloat(interpolateManualTF.getText());
            parameters.setParameterActive(false);
            parameters.set(INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE, v);
            parameters.setParameterActive(true);
        } catch (NumberFormatException ex) {
            interpolateManualTF.setText("" + parameters.get(INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE));
        }
    }//GEN-LAST:event_interpolateManualTFActionPerformed

    private void interpolateManualTFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_interpolateManualTFFocusLost
        interpolateManualTFActionPerformed(null);
    }//GEN-LAST:event_interpolateManualTFFocusLost

    private void inpaintMissingSlicesCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inpaintMissingSlicesCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(INPAINT_MISSING_SLICES, inpaintMissingSlicesCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_inpaintMissingSlicesCBActionPerformed

    private void readAsStackCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readAsStackCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(IGNORE_ORIENTATION, readAsStackCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_readAsStackCBActionPerformed

    private void sliceDenisingLevelSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_sliceDenisingLevelSliderStateChanged
    {//GEN-HEADEREND:event_sliceDenisingLevelSliderStateChanged
        parameters.setParameterActive(false);
        parameters.set(SLICE_DENOISING_LEVEL, sliceDenisingLevelSlider.getValue());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_sliceDenisingLevelSliderStateChanged

    private void scaleMRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_scaleMRBActionPerformed
    {//GEN-HEADEREND:event_scaleMRBActionPerformed
        setUnit();
    }//GEN-LAST:event_scaleMRBActionPerformed

    private void scaleMMRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_scaleMMRBActionPerformed
    {//GEN-HEADEREND:event_scaleMMRBActionPerformed
        setUnit();
    }//GEN-LAST:event_scaleMMRBActionPerformed

    private void setUnit()
    {
        parameters.setParameterActive(false);
        if (scaleMRB.isSelected())
            parameters.set(LENGTH_UNIT, ReadDICOMShared.UNIT_M);
        else if (scaleCMRB.isSelected())
            parameters.set(LENGTH_UNIT, ReadDICOMShared.UNIT_CM);
        else
            parameters.set(LENGTH_UNIT, ReadDICOMShared.UNIT_MM);
        parameters.setParameterActive(true);
    }

    private void positionZeroRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_positionZeroRBActionPerformed
    {//GEN-HEADEREND:event_positionZeroRBActionPerformed
        processPosition();
    }//GEN-LAST:event_positionZeroRBActionPerformed

    private void positionCenterRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_positionCenterRBActionPerformed
    {//GEN-HEADEREND:event_positionCenterRBActionPerformed
        processPosition();
    }//GEN-LAST:event_positionCenterRBActionPerformed

    private void positionOriginalRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_positionOriginalRBActionPerformed
    {//GEN-HEADEREND:event_positionOriginalRBActionPerformed
        processPosition();
    }//GEN-LAST:event_positionOriginalRBActionPerformed

    private void scaleCMRBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_scaleCMRBActionPerformed
    {//GEN-HEADEREND:event_scaleCMRBActionPerformed
        setUnit();
    }//GEN-LAST:event_scaleCMRBActionPerformed

    private void subdirectoryButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_subdirectoryButtonActionPerformed
    {//GEN-HEADEREND:event_subdirectoryButtonActionPerformed
        dataFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dataFileChooser.setMultiSelectionEnabled(true);
        dataFileChooser.resetChoosableFileFilters();
        if (lastPath == null) {
            dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadDICOM.class)));
        } else {
            dataFileChooser.setCurrentDirectory(new File(lastPath));
        }

        int returnVal = dataFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] files = dataFileChooser.getSelectedFile().listFiles();
            Arrays.sort(files);
            ArrayList<String> fileList = new ArrayList<>();
            lastPath = files[0].getAbsolutePath().substring(0, files[0].getAbsolutePath().lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadDICOM.class);
            String dirPath = files[0].getAbsolutePath().substring(0, files[0].getAbsolutePath().lastIndexOf(File.separator));
            parameters.setParameterActive(false);
            parameters.set(DIR_PATH, dirPath);
            parameters.setParameterActive(true);

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (!DicomFileUtilities.isDicomOrAcrNemaFile(files[i].getAbsolutePath())) {
                        System.out.println("INFO: File " + files[i].getAbsolutePath() + " is not a DICOM compatible file - skipping");
                        continue;
                    }

                    if (!files[i].exists() || files[i].getName().toLowerCase().equals("dicomdir")) {
                        continue;
                    }

                    fileList.add(files[i].getAbsolutePath());
                }

                String[] list = fileList.toArray(new String[0]);
                parameters.set(FILE_LIST, list);
            }
        } else {
            showDicomWindowButton.setEnabled(false);
        }
    }//GEN-LAST:event_subdirectoryButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private javax.swing.JRadioButton autoDataRB;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JRadioButton bytesDataRB;
    private org.visnow.vn.lib.gui.DownsizeUI downsizeUI;
    private javax.swing.JPanel geometryPanel;
    private javax.swing.JTextField highCropTF;
    private org.visnow.vn.lib.gui.HistoArea histoArea;
    private javax.swing.JRadioButton histoDataRB;
    private javax.swing.JCheckBox histoLogCB;
    private javax.swing.JPanel histoPanel;
    private javax.swing.JCheckBox inpaintMissingSlicesCB;
    private javax.swing.JCheckBox interpolateDataCB;
    private javax.swing.JRadioButton interpolateManualRB;
    private javax.swing.JTextField interpolateManualTF;
    private javax.swing.JPanel interpolatePanel;
    private javax.swing.JRadioButton interpolatePixelSizeRB;
    private javax.swing.JRadioButton interpolateSliceDistRB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField lowCropTF;
    private javax.swing.JButton openDICOMDIRButton;
    private javax.swing.JButton openDICOMFilesButton;
    private javax.swing.JRadioButton positionCenterRB;
    private javax.swing.JRadioButton positionOriginalRB;
    private javax.swing.JRadioButton positionZeroRB;
    private javax.swing.JPanel readAsPanel;
    private javax.swing.JCheckBox readAsStackCB;
    private javax.swing.JRadioButton scaleCMRB;
    private javax.swing.JRadioButton scaleMMRB;
    private javax.swing.JRadioButton scaleMRB;
    private javax.swing.JButton showDicomWindowButton;
    private javax.swing.JSlider sliceDenisingLevelSlider;
    private javax.swing.JButton subdirectoryButton;
    private javax.swing.JRadioButton windowDataRB;
    // End of variables declaration//GEN-END:variables


    /**
     * @return the histoArea
     */
    public org.visnow.vn.lib.gui.HistoArea getHistoArea()
    {
        return histoArea;
    }

    void activateOpenDialog()
    {
        openDICOMDIRButtonActionPerformed(null);
    }

    private void processPosition()
    {
        parameters.setParameterActive(false);
        if (positionCenterRB.isSelected()) {
            parameters.set(POSITION, ReadDICOMShared.POSITION_CENTER);
        } else if (positionOriginalRB.isSelected()) {
            parameters.set(POSITION, ReadDICOMShared.POSITION_ORIGINAL);
        } else if (positionZeroRB.isSelected()) {
            parameters.set(POSITION, ReadDICOMShared.POSITION_ZERO);
        }
        parameters.setParameterActive(true);
    }


    public class SimplifiedDicomDirectory implements TreeModel
    {

        private ArrayList<DicomDirectoryRecord> images;
        private String top = "All";
        private String parentPath = "";

        public SimplifiedDicomDirectory(DicomDirectoryRecord[] images, String parentPath)
        {
            this.images = new ArrayList<DicomDirectoryRecord>();
            for (int i = 0; i < images.length; i++) {
                this.images.add(images[i]);
            }
            this.top = "All (" + this.images.size() + ")";
            this.parentPath = parentPath;
        }

        public Object getRoot()
        {
            return top;
        }

        public Object getChild(Object parent, int index)
        {
            if (parent instanceof DicomDirectoryRecord)
                return ((DicomDirectoryRecord) parent).getChildAt(index);
            else if (parent == top)
                return images.get(index);
            else
                return null;
        }

        public int getChildCount(Object parent)
        {
            if (parent instanceof DicomDirectoryRecord)
                return ((DicomDirectoryRecord) parent).getChildCount();
            else if (parent == top)
                return images.size();
            else
                return 0;
        }

        public boolean isLeaf(Object node)
        {
            if (node instanceof DicomDirectoryRecord)
                return ((DicomDirectoryRecord) node).isLeaf();
            else if (node == top)
                return (images.isEmpty());
            else
                return true;
        }

        public void valueForPathChanged(TreePath path, Object newValue)
        {
        }

        public int getIndexOfChild(Object parent, Object child)
        {
            if (parent instanceof DicomDirectoryRecord && child instanceof DicomDirectoryRecord)
                return ((DicomDirectoryRecord) parent).getIndex((DicomDirectoryRecord) child);
            else if (parent == top && child instanceof DicomDirectoryRecord)
                return images.indexOf(child);
            else
                return -1;
        }

        private ArrayList<TreeModelListener> listeners;

        public void addTreeModelListener(TreeModelListener tml)
        {
            if (listeners == null)
                listeners = new ArrayList<TreeModelListener>();
            listeners.add(tml);
        }

        public void removeTreeModelListener(TreeModelListener tml)
        {
            if (listeners != null)
                listeners.remove(tml);
        }

        @SuppressWarnings("unchecked")
        public ArrayList<String> getReferencedNameList()
        {
            ArrayList<String> out = new ArrayList<String>();
            for (int i = 0; i < images.size(); i++) {
                Vector<String> tmp = DicomDirectory.findAllContainedReferencedFileNames(images.get(i), parentPath);
                out.addAll(tmp);
            }
            return out;
        }

    }

    public void openDICOMDir()
    {
        openDICOMDIRButtonActionPerformed(null);
    }
}
