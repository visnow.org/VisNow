/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public abstract class DataElementIOSchema
{

    public static final int COORDS = -1;
    public static final int MASK = -2;
    public static final int GLOBAL = -1;
    protected final DataContainer dataset;
    protected final int coord;
    protected final DataArrayType type;
    protected final int offsetFrom;
    protected final int offsetTo;
    protected final long nData;
    protected final int veclen;
    protected final String name;

    public DataElementIOSchema(String name, DataContainer dataset, int coord, DataArrayType type, int veclen, long nData, int offsetFrom, int offsetTo)
    {
        this.name = name;
        this.dataset = dataset;
        this.coord = coord;
        this.type = type;
        this.veclen = veclen;
        this.nData = nData;
        this.offsetFrom = offsetFrom;
        this.offsetTo = offsetTo;
    }

    public DataElementIOSchema(DataContainer dataset, int cmp, int coord, DataArrayType type, int veclen, long nData, int offsetFrom, int offsetTo)
    {
        this.name = cmp <  dataset.getNComponents() ? dataset.getComponent(cmp).getName() :
                    cmp == dataset.getNComponents() ? "coords" : "mask";
        this.dataset = dataset;
        this.coord = coord;
        this.type = type;
        this.veclen = veclen;
        this.nData = nData;
        this.offsetFrom = offsetFrom;
        this.offsetTo = offsetTo;
    }



    @Override
    public abstract String toString();

    public String getName()
    {
        return name;
    }

    /**
     * Get the value of offsetTo
     *
     * @return the value of offsetTo
     */
    public int getOffsetTo()
    {
        return offsetTo;
    }


    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public DataArrayType getType()
    {
        return type;
    }

    /**
     * Get the value of offsetFrom
     *
     * @return the value of offsetFrom
     */
    public int getOffsetFrom()
    {
        return offsetFrom;
    }

    /**
     * Get the value of coord
     *
     * @return the value of coord
     */
    public int getCoord()
    {
        return coord;
    }

    /**
     * Get the value of dataset
     *
     * @return the value of dataset
     */
    public DataContainer getDataset()
    {
        return dataset;
    }

    public int getVectorLength()
    {
        return veclen;
    }

    public long getnData()
    {
        return nData;
    }

}
