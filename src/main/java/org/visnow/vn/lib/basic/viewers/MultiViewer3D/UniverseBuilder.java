/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.MultiViewer3D;

import org.jogamp.java3d.utils.behaviors.mouse.MouseRotate;
import org.jogamp.java3d.*;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Vector3f;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
class UniverseBuilder
{

    protected Locale locale = null;
    protected CameraView rightCamera = new CameraView();
    protected CameraView frontCamera = new CameraView();
    protected CameraView topCamera = new CameraView();
    protected CameraView mainCamera = new CameraView();
    protected final ModelScene scene = new ModelScene();
    protected BoundingSphere bounds = new BoundingSphere(new Point3d(0., 0., 0.), 100.0);
    protected MouseRotate myMouseRotate = new MouseRotate();
    protected double mouseRotateSensitivity = .002;
    protected CameraView[] cameras = new CameraView[]{rightCamera, frontCamera, topCamera, mainCamera};
    protected Vector3f rightPos = new Vector3f(-4.0f, 0.0f, 0.0f);
    protected Vector3f frontPos = new Vector3f(0.0f, -4.0f, 0.0f);
    protected Vector3f topPos = new Vector3f(0.0f, 0.0f, 4.0f);
    protected Transform3D xformSide = new Transform3D();
    protected Transform3D xformFront = new Transform3D();
    protected Transform3D xformTop = new Transform3D();

    public UniverseBuilder()
    {
        create();
    }

    private void create()
    {
        xformSide.rotX(-PI / 2.0);
        xformSide.rotY(-PI / 2.0);
        xformFront.rotX(PI / 2.0);
        BranchGroup sceneBG = scene.getSceneGraph();
        Transform3D xform2;
        final VirtualUniverse universe = new VirtualUniverse();
        locale = new Locale(universe);

        rightCamera.setName("side");
        rightCamera.setOrthoView(true);
        Transform3D xform = new Transform3D();
        xform.set(rightPos);
        xform.mul(xformSide);
        rightCamera.getViewPlatformTransformGroup().setTransform(xform);

        frontCamera.setName("front/back");
        frontCamera.setOrthoView(true);
        frontCamera.setStdAxes(new int[][]{{0, 1}, {2, 1}, {1, -1}});
        xform = new Transform3D();
        xform.set(frontPos);
        xform.mul(xformFront);
        frontCamera.getViewPlatformTransformGroup().setTransform(xform);

        topCamera.setName("top/bottom");
        topCamera.setOrthoView(true);
        topCamera.setStdAxes(new int[][]{{0, 1}, {1, 1}, {2, 1}});
        xform = new Transform3D();
        xform.set(topPos);
        topCamera.getViewPlatformTransformGroup().setTransform(xform);

        mainCamera.setName("perspective view");
        mainCamera.setOrthoView(false);
        xform = new Transform3D();
        xform.rotX(PI / 8.0);
        xform.rotY(PI / 8.0);
        xform.setScale(.5);
        xform2 = new Transform3D();
        Vector3f vec = new Vector3f(0.0f, 0.0f, 4.0f);
        xform2.set(vec);
        xform.mul(xform2);
        mainCamera.getViewPlatformTransformGroup().setTransform(xform);

        for (int i = 0; i < cameras.length; i++) {
            cameras[i].setScene(scene);
            if (cameras[i].isOrthoView()) {
                cameras[i].getView().setProjectionPolicy(View.PARALLEL_PROJECTION);
                scene.getSceneRotateGroup().addChild(cameras[i].getRootBG());
            } else {
                cameras[i].getView().setProjectionPolicy(View.PERSPECTIVE_PROJECTION);
                locale.addBranchGraph(cameras[i].getRootBG());
            }
        }

        sceneBG.compile();
        locale.addBranchGraph(sceneBG);
    }

    public CameraView getRightCamera()
    {
        return this.rightCamera;
    }

    public CameraView getMainCamera()
    {
        return this.mainCamera;
    }

    public CameraView getFrontCamera()
    {
        return this.frontCamera;
    }

    public CameraView getTopCamera()
    {
        return this.topCamera;
    }

    public ModelScene getScene()
    {
        return scene;
    }

    public void translateT(float d)
    {
        Transform3D xform = new Transform3D();
        topPos.x = d;
        xform.set(topPos);
        xform.mul(xformTop);
        topCamera.getViewPlatformTransformGroup().setTransform(xform);
        xform = new Transform3D();
        frontPos.x = d;
        xform.set(frontPos);
        xform.mul(xformFront);
        frontCamera.getViewPlatformTransformGroup().setTransform(xform);
    }

    public void translateU(float d)
    {
        Transform3D xform = new Transform3D();
        topPos.y = d;
        xform.set(topPos);
        xform.mul(xformTop);
        topCamera.getViewPlatformTransformGroup().setTransform(xform);
        xform = new Transform3D();
        rightPos.y = d;
        xform.set(rightPos);
        xform.mul(xformSide);
        rightCamera.getViewPlatformTransformGroup().setTransform(xform);
    }

    public void translateV(float d)
    {
        Transform3D xform = new Transform3D();
        rightPos.z = d;
        xform.set(rightPos);
        xform.mul(xformSide);
        rightCamera.getViewPlatformTransformGroup().setTransform(xform);
        xform = new Transform3D();
        frontPos.z = d;
        xform.set(frontPos);
        xform.mul(xformFront);
        frontCamera.getViewPlatformTransformGroup().setTransform(xform);
    }

}
