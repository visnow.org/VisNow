/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.TestField;

import org.visnow.vn.engine.core.ProgressAgent;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.geometries.parameters.RenderingParams;
import static org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared.GeometryType.*;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.jscic.utils.FieldUtils;


/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class TestField extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(TestField.class);
    //module elements
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected long randomSeed = 0;
    protected double[][] randoms = new double[256][5];

    public TestField()
    {
        super();
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
            }
        });

        Random random = new Random(randomSeed);

        for (int i = 0; i < randoms.length; i++) {
            randoms[i][0] = 2 * random.nextDouble() - 1;
            randoms[i][1] = 2 * random.nextDouble() - 1;
            randoms[i][2] = 20 * (random.nextDouble() + .1) / (i + 50);
            randoms[i][3] = 20 * (random.nextDouble() + .1) / (i + 50);
            randoms[i][4] = 100 * (random.nextDouble() - .5) / (i + 50);

        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(NUMBER_OF_DIMENSIONS, 2),
            new Parameter<>(STRUCT_TYPE, StructureType.REGULAR),
            new Parameter<>(DIMENSION_LENGTH, 50),
            new Parameter<>(SELECTED_COMPONENTS, new int[]{0, 1}),
            new Parameter<>(GEOMETRY, GeometryType.UNIFORM_RECT_GEOM),
            new Parameter<>(CREATE_COORDS, false),
            new Parameter<>(META_RANDOM_SEED, System.currentTimeMillis())
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void createTestRegularField(int dimNum, int resolution, int[] components, int nThreads, ProgressAgent progressAgent)
    {
        int[] dims = new int[dimNum];
        switch (dimNum) {
            case 1:
                dims[0] = resolution;
                break;
            case 2:
                dims[0] = Math.max(2, (int) (.8 * resolution));
                dims[1] = (int) (1.25 * resolution);
                break;
            case 3:
                dims[0] = Math.max(2, (int) (.8 * resolution));
                dims[1] = resolution;
                dims[2] = (int) (1.25 * resolution);

                break;
            default:
                Arrays.fill(dims, resolution);
                break;
        }
        outField = outRegularField = new RegularField(dims);
        outRegularField.setName("Test regular field");
        GeometryType geometryType = parameters.get(GEOMETRY);
        if (geometryType == CURVILINEAR_GEOM && dimNum == 3) 
            outRegularField.setCoords(TestField3DCore.createCurvilinearCoords(resolution, dims), 0);
        else if (geometryType == RADIAL_GEOM && dimNum == 2)
            outRegularField.setCoords(TestField2DCore.createRadialCoords(resolution, dims), 0);
        else if (geometryType == SPHERICAL_GEOM && dimNum == 2)
            outRegularField.setCoords(TestField2DCore.createSphericalCoords(resolution, dims), 0);
        else {
            float[][] affine = new float[4][3];
            for (float[] aff : affine)
                Arrays.fill(aff, 0);
            switch (geometryType) {
            case TRIVIAL_GEOM:
                for (int i = 0; i < dimNum; i++) 
                    affine[i][i] = 1;
                break;
            case UNIFORM_RECT_GEOM:
                for (int i = 0; i < dimNum; i++) {
                    affine[i][i] = 1.f / (resolution - 1);
                    affine[3][i] = (-.5f * (dims[i] - 1)) / (resolution - 1);
                }
                break;    
            case RECTANGULAR_GEOM:
                for (int i = 0; i < dimNum; i++) {
                    affine[i][i] = 1.f / (dims[i] - 1);
                    affine[3][i] = -.5f * (dims[i] - 1);
                }
                break;  
            case AFFINE_GEOM:
                for (int i = 0; i < dimNum; i++) {
                    for (int j = 0; j <= i; j++)
                        affine[i][j] = 1.f / (dims[i] - 1);
                    affine[3][i] = -.5f * (dims[i] - 1);
                }
                break;  
            }
            outRegularField.setAffine(affine);
            if (parameters.get(CREATE_COORDS)) {
                FloatLargeArray crds = outRegularField.getCoordsFromAffine();
                outRegularField.setCoords(crds, 0);
            }
        }
        outRegularField.checkTrueNSpace();

        List<DataArray> dataArrays = null;
        switch (dimNum) {
            case 3:
                dataArrays = TestField3DCore.createDataArrays(nThreads, dims, components, progressAgent);

                break;
            case 2:
                dataArrays = TestField2DCore.createDataArrays(nThreads, dims, components, randoms, progressAgent);
                break;
            case 1:
                dataArrays = TestField1DCore.createDataArrays(resolution, components, randoms, progressAgent);
                FloatLargeArray coords = new FloatLargeArray(3 * resolution);
                for (long i = 0; i < resolution; i++) {
                    double s = 0;
                    float dd = 5.f / resolution;
                    coords.set(3 * i, (float)(cos(3 * i * dd + s) + .1 * cos(10 * i * dd + s)));
                    coords.set(3 * i + 1, (float) sin(3 * i * dd + s));
                    coords.set(3 * i + 2, (float)(abs(i - resolution / 2) * dd) * (float)(abs(i - resolution / 2) * dd));
                }
                outRegularField.setCurrentCoords(coords);
                break;
            default:
        }
        for (DataArray dataArray : dataArrays)
            outRegularField.addComponent(dataArray);

        outRegularField.setCurrentTime(0);
        outRegularField.setCurrentTime(outRegularField.getStartTime());
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters)
            parameters.set(META_RANDOM_SEED, System.currentTimeMillis());
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart(!isFromVNA());
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, isFromVNA(), false);

        int resolution = p.get(DIMENSION_LENGTH);
        int[] selectedComponents = p.get(SELECTED_COMPONENTS);
        ProgressAgent progressAgent = getProgressAgent(resolution + resolution / 2);
        //long time;
        if (selectedComponents.length == 0) {
            setOutputValue("outField", null);
            outRegularField = null;
            outIrregularField = null;
            outField = null;
        } else {
            createTestRegularField(p.get(NUMBER_OF_DIMENSIONS), resolution, selectedComponents,
                                   VisNow.availableProcessors(), progressAgent);
            if (p.get(STRUCT_TYPE) == StructureType.REGULAR) {
                outField = outRegularField;
                setOutputValue("outRegularField", new VNRegularField(outRegularField));
                setOutputValue("outIrregularField", null);
            }
            if (p.get(STRUCT_TYPE) == StructureType.IRREGULAR) {
                outField = outIrregularField = FieldUtils.convertToIrregular(outRegularField);
                FieldUtils.convertNodeDataToCellData(outIrregularField);
                setOutputValue("outIrregularField", new VNIrregularField(outIrregularField));
                setOutputValue("outRegularField", null);
            }
            if (p.get(STRUCT_TYPE) == StructureType.TRIANGULATED) {
                outField = outIrregularField = outRegularField.getTriangulated();
                FieldUtils.convertNodeDataToCellData(outIrregularField);
                setOutputValue("outIrregularField", new VNIrregularField(outIrregularField));
                setOutputValue("outRegularField", null);
            }
        }
        if (p.get(NUMBER_OF_DIMENSIONS) == 2) {
            presentationParams.getRenderingParams().setDisplayMode(RenderingParams.SURFACE | RenderingParams.IMAGE);
            if (parameters.get(GEOMETRY) != SPHERICAL_GEOM) 
                presentationParams.getRenderingParams().setShadingMode(RenderingParams.UNSHADED);
            else 
                presentationParams.getRenderingParams().setShadingMode(RenderingParams.GOURAUD_SHADED);
        }
        if (p.get(NUMBER_OF_DIMENSIONS) == 1)
            presentationParams.getRenderingParams().setDisplayMode(RenderingParams.EDGES);
        prepareOutputGeometry();
        progressAgent.increase(resolution / 5);
        show();
        progressAgent.setProgress(1.0f);
    }
}
