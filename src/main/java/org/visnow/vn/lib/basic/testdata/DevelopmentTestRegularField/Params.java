/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField;

import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Geometries.AffineOrthogonal;

/* Default geometry */
/**
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 */
public final class Params extends Parameters
{

    private static final ParameterEgg[] eggs = new ParameterEgg[]{
        /* "dimensions" parameter is used to pass dims[]. */
        new ParameterEgg<int[]>("dims", ParameterType.independent, null),
        /* Order of components will be important in the resulting field. */
        new ParameterEgg<String[]>("componentsClassNames", ParameterType.independent, null),
        new ParameterEgg<String>("geometryClassName", ParameterType.independent, null),
        new ParameterEgg<Boolean>("maskSetting", ParameterType.independent, null)
    };

    public Params()
    {
        super(eggs);

        /* Default values. */
        this.setValue("dims", new int[]{120, 95, 135});

        this.setValue("componentsClassNames", DevelopmentTestRegularField.getComponentsClassNames());
        this.setValue("geometryClassName", AffineOrthogonal.class.getSimpleName());

        this.setValue("maskSetting", Boolean.FALSE);
    }

    public void setDims(int[] dataDimensions)
    {
        if (dataDimensions != null) {
            int[] dims = new int[dataDimensions.length];
            for (int i = 0; i < dataDimensions.length; i++) {
                dims[i] = Math.max(1, dataDimensions[i]);
            }
            this.setValue("dims", dims);
        }
    }

    public int[] getDims()
    {
        return (int[]) this.getValue("dims");
    }

    public void setComponentsClassNames(String[] dataComponentClassNames)
    {
        this.setValue("componentsClassNames", dataComponentClassNames);
    }

    public String[] getComponentsClassNames()
    {
        return (String[]) this.getValue("componentsClassNames");
    }

    public void setGeometryClassName(String geometryClassName)
    {
        this.setValue("geometryClassName", geometryClassName);
    }

    public String getGeometryClassName()
    {
        return (String) this.getValue("geometryClassName");
    }

    public void setMaskSetting(Boolean flag)
    {
        this.setValue("maskSetting", flag);
    }

    public Boolean getMaskSetting()
    {
        return (Boolean) this.getValue("maskSetting");
    }
}
