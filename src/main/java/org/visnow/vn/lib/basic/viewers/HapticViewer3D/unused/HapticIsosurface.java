/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

/*
 import org.jogamp.vecmath.Point3f;
 import org.jogamp.vecmath.Tuple3f;
 import org.jogamp.vecmath.Vector3d;
 import org.jogamp.vecmath.Vector3f;
 import org.visnow.jscic.dataarrays.DataArray;
 import org.visnow.jscic.RegularField;
 import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.AbstractRegularFieldForce;
 */
//
//**
// * Comment by Łukasz Czerwiński: Class not maintained now. It was intended to simulate haptic
// * isosurface, but the way of working wasn't specified by Know. Probably to be done one day...
// * <p/>
// * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
// */
//public class HapticIsosurface extends AbstractRegularFieldForce {
//
//    private boolean colliding = false;
//    private Point3f firstIntersection = new Point3f();
//    private Point3f currIntersection = new Point3f();
//    private Vector3f move = new Vector3f();
//    private Vector3d forceV = new Vector3d();
//    private float[] gradientDir;
//    private float[] gradientData;
//    private float threshold;
////TODO in distant future: add static coordinate mode
//
//    public HapticIsosurface() {
//        super(CoordinateSystem.LOCAL); // added whatever
//    }
//
//    public HapticIsosurface(HapticIsosurface aThis) {
//        super(aThis);
//        this.colliding = aThis.colliding;
//        this.firstIntersection.set(aThis.firstIntersection);
//        this.currIntersection.set(aThis.currIntersection);
//    }
////
//
//    /**
//     * TODO in a distant future: Najlepiej jakby on dostawał field+data+gradient+threshold
//     * TODO in a distant future: Tak naprawdę to pole oddziaływania, ja mogę sobie wcześniej
//     * wyliczyć
//     * i nie bawić się w zbieżność
//     * TODO in a distant future: Operować na float, czy na double
//     */
////    public HapticIsosurface(RegularField field, DataArray data) {
////        super(field, data);
////    }
//    public float getThreshold() {
//        return threshold;
//    }
//
//    public void setThreshold(float threshold) {
//        this.threshold = threshold;
//    }
//
//    @Override
//    public void getForce(HapticLocationData locationData, Vector3f out_force)
//            throws ITrackerToVworldGetter.NoDataException,
//                   ILocalToVworldGetter.NoDataException {
//
//        Point3f position = locationData.getCurrentLocalPosition();
//
//        if (dataChangingOrNull) {
//            out_force.set(ZERO_FORCE);
//            return;
//        }
//        //konwersja pozycji sensora na indeksy w tablicy danych
//        convertPointToLocalGeom(position);
//
//        //sprawdzenie, czy punkt jest w obrebie pola
//        if (pointIsOutsideGeom(indexesArr)) {
//            out_force.set(ZERO_FORCE);
//            return;
//        }
//        
//        Point3f pointLocal = position; // line added just to avoid compilation errors
//
//        if (!colliding) {
//            colliding = true;
//            firstIntersection.set(pointLocal);
//            currIntersection.set(pointLocal);
//            gradientDir = getGradientValue(pointLocal);
//            float norm = (float) -sqrt(gradientDir[0] * gradientDir[0] + gradientDir[1] * gradientDir[1] + gradientDir[2] * gradientDir[2]);
//            gradientDir[0] /= norm;
//            gradientDir[1] /= norm;
//            gradientDir[2] /= norm;
//        } else {
//            float epsilon = 0.01f;
//            boolean wasTrans = false;
//            boolean wasOutside = false;
//            float mscale = -1;
//            move.set(getGradientValue(pointLocal));
//            move.negate();
//            currIntersection.set(pointLocal);
//            while (move.length() > epsilon && !wasTrans) {
//                currIntersection.add(move);
//                if (pointIsOutsideGeom(currIntersection)) {
//                    break;
//                }
//                if (getScalarValue(currIntersection) < threshold != wasOutside) {
//                    wasOutside = !wasOutside;
//                    wasTrans = true;
//                    mscale *= -0.5f;
//                }
//                move.set(getGradientValue(currIntersection));
//                move.scale(mscale);
//            }
//            /*
//             * Wyswietlanie obiektu proxy
//             move.set(currIntersection);
//             move.x = move.x * diameter.x / dims[0];
//             move.y = move.y * diameter.y / dims[1];
//             move.z = move.z * diameter.z / dims[2];
//             move.add(pointA);
//
//             objTransTm.setTranslation(move);
//             objTrans.setTransform(objTransTm);
//             */
//        }
//
//        //sensor w obrebie pola wektorowego, interpolacja wspolrzednych
//        switch (data.getType()) {
//            case FIELD_DATA_FLOAT:
////                float value = getScalarValue(point);
////                //System.out.println(" T" + (value - threshold));
////                if (value > threshold) {
////                    //TODO in a distant future: takze w druga strone
////
////                    //opcja 1 nawigacja po gradiencie
////                    //force.set(getGradientValue(point));
////                    //sensorToGeom.transform(force);
////                    ///force.normalize();
////                    //force.scale(-(value-threshold)*(value-threshold) * params.getForceScale());
////
////                    //opcja 2 nawigacja po najblizszym punkcie zetkniecia
////                    forceV.x = currIntersection.x - point.x;
////                    forceV.y = currIntersection.y - point.y;
////                    forceV.z = currIntersection.z - point.z;
////
////                    localToVworld.transform(forceV);
////                    forceV.scale(forceScale);
////
////                    outForce.set(forceV);
////                    return;
////                } else {
////                    outForce.set(ZERO_FORCE);
////                    return;
////                }
//            default:
//                System.err.println("Unhandled data type: " + data.getType());
//                out_force.set(ZERO_FORCE);
//        }
//    }
//
//    private float[] getGradientValue(Tuple3f point) {
//        return field.getInterpolatedData(gradientData, point.x, point.y, point.z);
//    }
//
//    private float getScalarValue(Tuple3f point) {
//        return field.getInterpolatedData(data.getFData().getData(), point.x, point.y, point.z)[0];
//    }
//
//    @Override
//    public synchronized void setData(
//            int dataComponentIndex, RegularField field, DataArray data, float forceScale) {
//        ComputeGradient3D cg = new ComputeGradient3D(field.getDims(), data.getFData().getData(), 2);
//        cg.run();
//        super.dataChangingOrNull = true;
//        this.gradientData = cg.getOutData();
//        super.setData(dataComponentIndex, field, data, forceScale);
//    }
//
//    @Override
//    public IForce clone() {
//        return new HapticIsosurface(this);
//    }
//
//    @Override
//    public String getClassSimpleName() {
//        return "Isosurface";
//    }
//}

/* Following line added by Szymon Jaranowski (spj), so that file doesn't
 * compile every time project is ran (F6) in NetBeans. */
public class HapticIsosurface
{
}
