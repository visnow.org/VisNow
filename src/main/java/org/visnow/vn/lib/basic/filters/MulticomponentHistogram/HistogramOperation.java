/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.Field;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class HistogramOperation
{

    public static enum Operation
    {

        COUNT
            {
                @Override
                public String toString()
                {
                    return "count";
                }
            },
        SUM
            {
                @Override
                public String toString()
                {
                    return "sum";
                }
            },
        MIN
            {
                @Override
                public String toString()
                {
                    return "min";
                }
            },
        MAX
            {
                @Override
                public String toString()
                {
                    return "max";
                }
            },
        AVG
            {
                @Override
                public String toString()
                {
                    return "average";
                }
            },
        STD
            {
                @Override
                public String toString()
                {
                    return "stddev";
                }
            },
        VSTD
            {
                @Override
                public String toString()
                {
                    return "vstddev";
                }
            },

    };

    private Operation operation = Operation.COUNT;
    private int componentIndex = -1;
    private Field field = null;
    private boolean log = false;
    private float logConst = 1.0f;
    private boolean dropBg = false;

    public HistogramOperation(Operation operation, int componentIndex, Field field, boolean log, boolean dropBackground)
    {
        this(operation, componentIndex, field, log, 1.0f, dropBackground);
    }

    public HistogramOperation(Operation operation, int componentIndex, Field field, boolean log, float logConst, boolean dropBackground)
    {
        this.operation = operation;
        this.componentIndex = componentIndex;
        this.field = field;
        this.log = log;
        this.logConst = logConst;
        this.dropBg = dropBackground;
    }

    public HistogramOperation()
    {
        this(Operation.COUNT, -1, null, false, 1.0f, false);
    }

    public HistogramOperation(boolean log, float logConst, boolean dropBackground)
    {
        this(Operation.COUNT, -1, null, log, logConst, dropBackground);
    }

    public HistogramOperation(boolean log, boolean dropBackground)
    {
        this(Operation.COUNT, -1, null, log, 1.0f, dropBackground);
    }

    public Operation getOperation()
    {
        return operation;
    }

    public void setOperation(Operation operation)
    {
        this.operation = operation;
    }

    public int getComponentIndex()
    {
        return componentIndex;
    }

    public DataArray getComponent()
    {
        if (field == null)
            return null;

        if (componentIndex < 0 || componentIndex >= field.getNComponents())
            return null;

        return field.getComponent(componentIndex);
    }

    public float getComponentValue(int node)
    {
        return this.getComponent().getFloatElement(node)[0];
    }

    public float[] getVectorComponentValue(int node)
    {
        return this.getComponent().getFloatElement(node);
    }

    public void setComponentIndex(int componentIndex)
    {
        this.componentIndex = componentIndex;
    }

    public Field getField()
    {
        return field;
    }

    public void setField(Field field)
    {
        this.field = field;
    }

    public boolean isLog()
    {
        return log;
    }

    public void setLog(boolean log)
    {
        this.log = log;
    }

    public boolean isDropBackgound()
    {
        return dropBg;
    }

    public void setDropBackgound(boolean db)
    {
        this.dropBg = db;
    }

    @Override
    public String toString()
    {
        DataArray da = this.getComponent();
        String out = "histogram";
        if (da != null) {
            out += " " + da.getName() + " " + operation.toString();
        }
        return out;
    }

    public float getLogConst()
    {
        return logConst;
    }

    public void setLogConst(float logConst)
    {
        this.logConst = logConst;
    }

}
