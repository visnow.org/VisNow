/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.gui.widgets.VisNowFrame;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickEvent;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickListener;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.VolumeRenderer;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider.DataProvider;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.ConnectionDescriptor;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryFieldConverter;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryUI;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.Glypher;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointDescriptor;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class FieldViewer3D extends ModuleCore
{

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FieldViewer3D.class);
    private VisNowFrame frame;
    private FieldDisplay3DInternalFrame internalFrame;
    private VolumeRenderer volRender = null;
    private RegularField currentField = null;
    private GUI ui = null;
    private DataProvider dp = null;
    private GeometryUI geomUI = null;
    private GeometryParams gparams = new GeometryParams();
    private CalculableParams cparams = new CalculableParams();
    private Glypher glypher = new Glypher();
    private boolean fromGeomUI = false;
    private Vector<GeometryObject> inObjects = new Vector<GeometryObject>();

    /**
     * Creates a new instance of TestViewer3D
     */
    public FieldViewer3D()
    {
        volRender = new VolumeRenderer(false);
        dp = new DataProvider();
        dp.setGeometryParams(gparams);

        glypher.setParams(gparams);
        glypher.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                internalFrame.getDisplay3DPanel().repaint();
            }
        });

        volRender.addPick3DListener(new FieldPickListener()
        {
            @Override
            public void handlePick3D(FieldPickEvent e)
            {
                gparams.addPoint(e.getIndices());
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                frame = new VisNowFrame();
                frame.setTitle("VisNow - field viewer 3D");
                frame.setBounds(100, 100, 1200, 800);
                internalFrame = new FieldDisplay3DInternalFrame();
                frame.add(internalFrame);
                internalFrame.setDataProvider(dp);
                internalFrame.getDisplay3DPanel().addChild(volRender.getOutObject());
                internalFrame.setGeometryParams(gparams);
                internalFrame.setCalculableParams(cparams);
                internalFrame.getDisplay3DPanel().addChild(glypher.getGlyphsObject());

                geomUI = new GeometryUI(internalFrame);
                geomUI.setParams(gparams, cparams);
                geomUI.addChangeListener(internalFrame.getManager());

                internalFrame.addUI(dp.getUI(), "Slices");
                internalFrame.addUI(volRender.getVolRenderUI(), "3D View");
                internalFrame.addUI(geomUI, "Geometry");
                internalFrame.pack();
                internalFrame.setVisible(true);

                ui = new GUI();
                ui.addChangeListener(new ChangeListener()
                {
                    @Override
                    public void stateChanged(ChangeEvent evt)
                    {
                        frame.setVisible(true);
                        frame.setExtendedState(Frame.NORMAL);
                    }
                });

                geomUI.getOutputPointsButton().addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        fromGeomUI = true;
                        startAction();
                    }
                });

                setPanel(ui);
                
                frame.setMinimumSize(internalFrame.getMinimumSize());
                frame.setVisible(true);
            }
        });
    }

    public FieldDisplay3DInternalFrame getInternalFrame()
    {
        return this.internalFrame;
    }

    public VisNowFrame getFrame()
    {
        return this.frame;
    }

    @Override
    public void onDelete()
    {
        //WTF-MUI: getUI().removeAll();
        ui = null;
        internalFrame.getDisplay3DPanel().clearCanvas();
        if (internalFrame.getDisplay3DPanel().getControlsFrame() != null) {
            internalFrame.getDisplay3DPanel().getControlsFrame().dispose();
        }
        if (internalFrame.getDisplay3DPanel().getTransientControlsFrame() != null) {
            internalFrame.getDisplay3DPanel().getTransientControlsFrame().dispose();
        }
        frame.dispose();
        volRender = null;
        internalFrame = null;
        frame = null;
        dp = null;
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        if (!fromGeomUI) {
            if (getInputFirstValue("inField") == null || ((VNRegularField) getInputFirstValue("inField")).getField() == null) 
                return;

            RegularField inField = ((VNRegularField) getInputFirstValue("inField")).getField();
            if (inField != currentField) {
                currentField = inField;
                if (inField == null || (inField.getDims().length != 3 && inField.getDims().length != 2)) {
                    inField = null;
                }

                dp.setInField(inField);
                gparams.setInField(inField);
                gparams.setIntersectionPoint(dp.getParams().getOrthosliceNumbers().clone());
                if (inField != null && inField.getDimNum() == 3)
                    volRender.setInField(inField);
            }

            if (getInputFirstValue("inPointsGeometryField") != null &&
                ((VNField) getInputFirstValue("inPointsGeometryField")).getField() != null) {
                Field inPointsGeometryField = ((VNField) getInputFirstValue("inPointsGeometryField")).getField();
                ArrayList<PointDescriptor> pointsDescriptors = new ArrayList<PointDescriptor>();
                ArrayList<ConnectionDescriptor> connectionDescriptors = new ArrayList<ConnectionDescriptor>();
                GeometryFieldConverter.field2pac(inPointsGeometryField, inField, pointsDescriptors, connectionDescriptors);

                gparams.clearPoints();
                PointDescriptor pd;
                for (int i = 0; i < pointsDescriptors.size(); i++) {
                    pd = pointsDescriptors.get(i);
                    gparams.addPoint(pd.getName(), pd.getIndices(), pd.getMembership());
                }
                ConnectionDescriptor cd;
                for (int i = 0; i < connectionDescriptors.size(); i++) {
                    cd = connectionDescriptors.get(i);
                    gparams.addConnection(gparams.getPointsDescriptorByName(cd.getP1().getName()), gparams.getPointsDescriptorByName(cd.getP2().getName()), cd.getName());
                }
            }

            glypher.setParams(gparams);

            geomUI.setParams(gparams, cparams);

            internalFrame.setGeometryParams(gparams);

            if (inField != null && inField.getDimNum() == 3) {
                internalFrame.getDisplay3DPanel().clearAllGeometry();
                internalFrame.getDisplay3DPanel().addChild(volRender.getOutObject());
                internalFrame.getDisplay3DPanel().addChild(glypher.getGlyphsObject());
                Vector ins = getInputValues("inObject");
                for (Object obj : ins) {
                    if ((VNGeometryObject) obj != null && ((VNGeometryObject) obj).getGeometryObject() != null) {
                        internalFrame.getDisplay3DPanel().addChild(((VNGeometryObject) obj).getGeometryObject());
                    }
                }
                internalFrame.getDisplay3DPanel().repaint();
            }
        }
        IrregularField outPoints = gparams.getPointsGeometryField(gparams.isAddFieldData());
        if (outPoints != null)
            setOutputValue("outPointsGeometryField", new VNIrregularField(outPoints));
        else
            setOutputValue("outPointsGeometryField", null);

        fromGeomUI = false;
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        onActive();
    }

    @Override
    public void onInputAttach(LinkFace link)
    {
        if (link.getInput().getName().equals("inObject")) {
            onActive();
        }
    }

    @Override
    public boolean isViewer()
    {
        return true;
    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow FieldViewer3D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

}
