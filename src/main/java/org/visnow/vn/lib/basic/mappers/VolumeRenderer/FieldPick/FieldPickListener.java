/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick;

import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;

/**
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 * @author based on code by Krzysztof S. Nowinski University of Warsaw, ICM (moved from Pick3DListener)
 */
abstract public class FieldPickListener
{

    boolean active = true;

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isActive()
    {
        return this.active;
    }

    /**
     * Called by PickObject to notify the Pick3DListener about pick event. Its only responsibility
     * is to call handlePick3D() iff Pick3DListener is active.
     * <p/>
     * @param e pick event to be passed to handlePick3D
     */
    final public void pick3DChanged(FieldPickEvent e)
    {
        if (active)
            handlePick3D(e);
    }

    /**
     * Handles pick event. Method specific for every module that wants to handle it.
     * <p/>
     * NOTE: Do NOT call it directly - it should be called ONLY BY
     * <code>pick3DChanged()</code>.
     * <p/>
     * @param e pick event
     */
    protected abstract void handlePick3D(FieldPickEvent e);
}
