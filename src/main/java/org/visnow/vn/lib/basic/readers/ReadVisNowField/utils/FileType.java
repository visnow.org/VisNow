//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

/**
 *
 * @author know
 */
public enum FileType
{
    ASCII_COLUMN(        "a", "col", true,  true,  "ascii column"),
    ASCII_CONTINUOUS(    "a", "con", false, true,  "ascii continuous"),
    ASCII_FIXED_COLUMN(  "a", "f",   false, true,  "ascii fixed char column"),
    BINARY_BIG_ENDIAN(   "b", "b",   true,  false, "binary big endian"),
    BINARY_LITTLE_ENDIAN("b", "l",   false, false, "binary big endian"),
    UNKNOWN(             "?", "",    true,  false, "unknown");

    private final String prefix;
    private final String subprefix;
    private final boolean isDefault;
    private final boolean isAscii;
    private final String text;

    FileType(String prefix, String subprefix, boolean isDefault, boolean isAscii, String text)
    {
        this.prefix = prefix;
        this.subprefix = subprefix;
        this.isDefault = isDefault;
        this.isAscii = isAscii;
        this.text = text;
    }

    public static FileType getType(String prefix, String subprefix)
    {
        if (subprefix != null && !subprefix.isEmpty())
            for (FileType value : FileType.values())
                if (prefix.toLowerCase().startsWith(value.prefix) &&
                    subprefix.toLowerCase().startsWith(value.subprefix))
                    return value;
        for (FileType value : FileType.values())
            if (prefix.toLowerCase().startsWith(value.prefix) && value.isDefault)
                return value;
        return UNKNOWN;
    }

    public boolean isAscii()
    {
        return isAscii;
    }

    public boolean isBinary()
    {
        return !isAscii;
    }

    public FileType defaultType()
    {
        if (prefix.equals("a"))
            return ASCII_COLUMN;
        if (prefix.equals("b"))
            return BINARY_BIG_ENDIAN;
        return UNKNOWN;
    }

    public static boolean isSubprefix(String s)
    {
        for (FileType value : FileType.values())
            if (s.toLowerCase().startsWith(value.subprefix) && value != UNKNOWN)
                return true;
        return false;
    }

    @Override
    public String toString()
    {
        return text;
    }
}
