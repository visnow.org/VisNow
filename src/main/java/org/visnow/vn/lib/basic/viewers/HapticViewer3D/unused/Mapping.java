/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.util.HashMap;

/**
 * @deprecated NOT USED!
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class Mapping
{

    private static Mapping instance;
    private HashMap<String, MappingFunction> functions = new HashMap<String, MappingFunction>();

    private Mapping()
    {
        functions.put("Rotate", new MappingFunction("Rotate"));
        functions.put("Translate", new MappingFunction("Translate"));
        functions.put("Scale", new MappingFunction("Scale"));
        functions.put("Pick", new MappingFunction("Pick"));
    }

    public static Mapping getInstance()
    {
        if (instance == null)
            instance = new Mapping();
        return instance;
    }

    public String[] getFunctionNames()
    {
        return (String[]) functions.keySet().toArray();
    }

    public synchronized boolean lock(String functionName, Object caller)
    {
        MappingFunction fun = functions.get(functionName);
        if (fun == null)
            throw new RuntimeException("No such function: " + functionName);
        return fun.lock(caller);
    }

    public synchronized void unlock(String functionName, Object caller)
    {
        MappingFunction fun = functions.get(functionName);
        if (fun == null)
            throw new RuntimeException("No such function: " + functionName);
        fun.unlock(caller);
    }

    protected class MappingFunction
    {

        private Object lockedOn = null;
        private String name;

        MappingFunction(String name)
        {
            this.name = name;
        }

        /**
         * Get name of function
         *
         * @return name of the function
         */
        public String getName()
        {
            return name;
        }

        /**
         * Lock function on specific device
         *
         * Lock function on specific device, such that other can't access it.
         * Got some error checking to force proper lock and unlock calls.
         * Otherwise it could be as simply as lock=true.
         *
         * @param caller who wants to get a lock, you must later call unlock with the same argument
         * <p>
         * @return true, if lock was assigned to caller; false, if it is locked on another object
         */
        public boolean lock(Object caller)
        {
            if (caller == null) {
                throw new NullPointerException("Argument cannot be null");
            }
            if (lockedOn == caller) {
                throw new RuntimeException("You [" + caller + "] already got lock");
            }

            if (lockedOn != null) {
                return false;
            }

            lockedOn = caller;
            return true;
        }

        public void unlock(Object caller)
        {
            if (caller == null) {
                throw new NullPointerException("Argument cannot be null");
            }
            if (lockedOn != caller) {
                throw new RuntimeException("You don't own this lock");
            }

            lockedOn = null;
        }
    }
}
