/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers;

import java.util.Arrays;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.utils.io.InputSource;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class ReaderParams extends Parameters
{

    @SuppressWarnings("unchecked")
    public ReaderParams()
    {
        super(eggs);
    }

    private static ParameterEgg[] concatenate(ParameterEgg[] additional)
    {
        ParameterEgg[] tab = Arrays.copyOf(eggs, ReaderParams.eggs.length + additional.length);
        for (int i = 0; i < additional.length; ++i) {
            tab[ReaderParams.eggs.length + i] = additional[i];
        }
        return tab;
    }

    @SuppressWarnings("unchecked")
    public ReaderParams(ParameterEgg[] additional)
    {
        super(concatenate(additional));
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<String>("fileName", ParameterType.filename, ""),
        new ParameterEgg<String[]>("fileNames", ParameterType.filename, null),
        new ParameterEgg<Boolean>("big endian", ParameterType.independent, true),
        new ParameterEgg<Boolean>("show", ParameterType.independent, true),
        new ParameterEgg<Integer>("input source", ParameterType.independent, InputSource.FILE)
    };

    public String getFileName()
    {
        return (String) getValue("fileName");
    }

    public void setFileName(String fileName)
    {
        setValue("fileName", fileName);
    }

    public String[] getFileNames()
    {
        return (String[]) getValue("fileNames");
    }

    public void setFileNames(String[] fileNames)
    {
        setValue("fileNames", fileNames);
    }

    public boolean isShow()
    {
        return (Boolean) getValue("show");
    }

    public void setShow(boolean show)
    {
        setValue("show", show);
    }

    public int getSource()
    {
        return (Integer) getValue("input source");
    }

    public void setSource(int source)
    {
        setValue("input source", source);
    }

    public boolean isBigEndian()
    {
        return (Boolean) getValue("big endian");
    }

    public void setBigEndian(boolean big_endian)
    {
        setValue("big endian", big_endian);
    }
}
