/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Axes3D;

/*
 * Axes3DObject.java
 *
 * Created on November 4, 2003, 2:35 PM
 */
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.objects.DataMappedGeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import java.awt.Color;
import java.util.IllegalFormatException;
import org.jogamp.java3d.Appearance;
import org.jogamp.java3d.Billboard;
import org.jogamp.java3d.BoundingSphere;
import org.jogamp.java3d.J3DGraphics2D;
import org.jogamp.java3d.LineArray;
import org.jogamp.java3d.LineAttributes;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Point3d;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.jscic.Field;
import org.visnow.vn.geometries.objects.TextBillboard;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.vn.lib.utils.Range;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.geometries.textUtils.Texts2D;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Axes3DObject extends DataMappedGeometryObject
{

    static String[] defAxDesc = {
        "X", "Y", "Z"
    };
    static String[] defValueFormats = {
        "%4.1f", "%4.1f", "%4.1f"
    };
    private FontParams fontParams;
    private String[] axDesc = defAxDesc;
    private String[] valueFormat = defValueFormats;
    private boolean[] grid;
    private float x0, x1, vx0, vx1;
    private float y0, y1, vy0, vy1;
    private float z0, z1, vz0, vz1;
    private float sx, sy, sz;
    private int nx, ny, nz;
    private Range rx, ry, rz;
    private int[][] axPos;
    private boolean showBox;
    private boolean showAxes;
    private boolean showUnits;
    private boolean drawX = true, drawY = true, drawZ = true;
    private int drawn = 3;
    private int nLabels = 0;
    private float[][] ext;
    private Field field = null;
    private Color xColor = Color.RED;
    private Color yColor = Color.GREEN;
    private Color zColor = new Color(.5f,.5f,1);
    private Axes3DParams params = null;
    private Texts2D xValueLabels = null;
    private Texts2D yValueLabels = null;
    private Texts2D zValueLabels = null;
    private Texts2D xAxisLabel = null;
    private Texts2D yAxisLabel = null;
    private Texts2D zAxisLabel = null;

    public Axes3DObject()
    {
        name = "axes " + id;
    }

    /**
     * Creates a new instance of Axes3DObject
     */
    public void update(Field field, Axes3DParams params)
    {
        if (params == null || field == null) {
            drawX = drawY = drawZ = false;
            return;
        }
        this.field = field;
        this.params = params;
        fontParams = params.getFontParams();
        axDesc = params.getAxDescs();
        valueFormat = params.getAxFormats();
        axPos = params.getAxPos();
        grid = params.getGridLines().clone();
        ext = field.getPreferredExtents();
        this.setExtents(field.getPreferredExtents());
        showBox = params.isBox();
        showAxes = params.isAxes();
        showUnits = params.isShowUnits();
        float[][] physExts = ext;
        if (field.getPreferredPhysicalExtents() != null) {
            physExts = field.getPreferredPhysicalExtents();
        }
        float r = 0;
        x0 = ext[0][0];
        x1 = ext[1][0];
        y0 = ext[0][1];
        y1 = ext[1][1];
        z0 = ext[0][2];
        z1 = ext[1][2];
        sx = (physExts[1][0] - physExts[0][0]) / (x1 - x0);
        sy = (physExts[1][1] - physExts[0][1]) / (y1 - y0);
        sz = (physExts[1][2] - physExts[0][2]) / (z1 - z0);
        if (r < x1 - x0) {
            r = x1 - x0;
        }
        if (r < y1 - y0) {
            r = y1 - y0;
        }
        if (r < z1 - z0) {
            r = z1 - z0;
        }
        r *= 2;
        drawn = 3;
        drawX = x1 - x0 > .01f * r;
        drawY = y1 - y0 > .01f * r;
        drawZ = z1 - z0 > .01f * r;
        if (!drawX) {
            drawn -= 1;
        }
        if (!drawY) {
            drawn -= 1;
        }
        if (!drawZ) {
            drawn -= 1;
        }
        if (drawn < 1) {
            return;
        }
        if (drawX) {
            nx = (int) (params.getXLabelDensity() * (x1 - x0) / r);
            if (nx < 2) nx = 2;
            vx0 = physExts[0][0] + sx * (x0 - ext[0][0]);
            vx1 = physExts[1][0] + sx * (x1 - ext[1][0]);
            rx = new Range(nx, vx0, vx1, false);
            vx0 = rx.getLow();
            vx1 = rx.getUp();
            x0 = ext[0][0] + (vx0 - physExts[0][0]) / sx;
            x1 = ext[1][0] + (vx1 - physExts[1][0]) / sx;
        }
        if (drawY) {
            ny = (int) (params.getYLabelDensity() * (y1 - y0) / r);
            if (ny < 2) ny = 2;
            vy0 = physExts[0][1] + sy * (y0 - ext[0][1]);
            vy1 = physExts[1][1] + sy * (y1 - ext[1][1]);
            ry = new Range(ny, vy0, vy1, false);
            vy0 = ry.getLow();
            vy1 = ry.getUp();
            y0 = ext[0][1] + (vy0 - physExts[0][1]) / sy;
            y1 = ext[1][1] + (vy1 - physExts[1][1]) / sy;
        }
        if (drawZ) {
            nz = (int) (params.getZLabelDensity() * (z1 - z0) / r);
            if (nz < 2) nz = 2;
            vz0 = physExts[0][2] + sz * (z0 - ext[0][2]);
            vz1 = physExts[1][2] + sz * (z1 - ext[1][2]);
            rz = new Range(nz, vz0, vz1, false);
            vz0 = rz.getLow();
            vz1 = rz.getUp();
            z0 = ext[0][2] + (vz0 - physExts[0][2]) / sz;
            z1 = ext[1][2] + (vz1 - physExts[1][2]) / sz;
        }
        // x0-x1, y0-y1, z0--z1 are geom extends corrected for being not too short
        // v.0-v.1 are range ext corrected for .0-.1 ranges

        // v.0-v.1 are rounded range ext, .0-.1  are corresponding geometric extends
        createAxes();
    }

    public void createAxes()
    {
        if (getCurrentViewer() == null)
            return;
        if (localToWindow == null)
            localToWindow = getCurrentViewer().getLocToWin();
        if (localToWindow == null ||
            fontParams == null || field  == null || params  == null)
            return;
        String[] ad = new String[3];
        int i, j, k, m;
        float gx, gy, gz;
        float[] t = new float[18];
        fontParams.createFontMetrics(localToWindow,
                                     getCurrentViewer().getWidth(),
                                     getCurrentViewer().getHeight());
        if (drawX) {
            t[0] = x0;
            t[3] = x1;
            switch (axPos[0][0]) {
                case Axes3DParams.MIN:
                    t[1] = t[4] = y0;
                    break;
                case Axes3DParams.ZERO:
                    t[1] = t[4] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[1] = t[4] = .5f * (y0 + y1);
                    break;
                case Axes3DParams.MAX:
                    t[1] = t[4] = y1;
                    break;
            }
            switch (axPos[0][1]) {
                case Axes3DParams.MIN:
                    t[2] = t[5] = z0;
                    break;
                case Axes3DParams.ZERO:
                    t[2] = t[5] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[2] = t[5] = .5f * (z0 + z1);
                    break;
                case Axes3DParams.MAX:
                    t[2] = t[5] = z1;
                    break;
            }
        }
        if (drawY) {
            t[7] = y0;
            t[10] = y1;
            switch (axPos[1][0]) {
                case Axes3DParams.MIN:
                    t[6] = t[9] = x0;
                    break;
                case Axes3DParams.ZERO:
                    t[6] = t[9] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[6] = t[9] = .5f * (x0 + x1);
                    break;
                case Axes3DParams.MAX:
                    t[6] = t[9] = x1;
                    break;
            }
            switch (axPos[1][1]) {
                case Axes3DParams.MIN:
                    t[8] = t[11] = z0;
                    break;
                case Axes3DParams.ZERO:
                    t[8] = t[11] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[8] = t[11] = .5f * (z0 + z1);
                    break;
                case Axes3DParams.MAX:
                    t[8] = t[11] = z1;
                    break;
            }
        }
        if (drawZ) {
            t[14] = z0;
            t[17] = z1;
            switch (axPos[2][0]) {
                case Axes3DParams.MIN:
                    t[12] = t[15] = x0;
                    break;
                case Axes3DParams.ZERO:
                    t[12] = t[15] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[12] = t[15] = .5f * (x0 + x1);
                    break;
                case Axes3DParams.MAX:
                    t[12] = t[15] = x1;
                    break;
            }
            switch (axPos[2][1]) {
                case Axes3DParams.MIN:
                    t[13] = t[16] = y0;
                    break;
                case Axes3DParams.ZERO:
                    t[13] = t[16] = 0;
                    break;
                case Axes3DParams.CENTER:
                    t[13] = t[16] = .5f * (y0 + y1);
                    break;
                case Axes3DParams.MAX:
                    t[13] = t[16] = y1;
                    break;
            }
        }
        geometryObj.removeAllChildren();
        OpenBranchGroup axes = new OpenBranchGroup();
        if (axDesc.length == 3) {
            for (int l = 0; l < axDesc.length; l++) {
                ad[l] = new String(axDesc[l]);
            }
        } else {
            for (int l = 0; l < defAxDesc.length; l++) {
                ad[l] = new String(defAxDesc[l]);
            }
        }

        if(showUnits && axDesc.length == 3) {
            for (int l = 0; l < 3; l++) {
                String u = field.getCoordsUnit(l);
                if(u != null && !u.isEmpty() && !"1".equals(u))
                    ad[l] = ad[l] + " ["+u+"]";
            }
        }


        float colorCorrection = min(2, max(0, fontParams.getColorCorrection()));
        float val1 = min(colorCorrection, 1);
        float val0 = max(colorCorrection - 1, 0);
        float[] col = {
            val1, val0, val0, val1, val0, val0,
            val0, val1, val0, val0, val1, val0,
            val0, val0, val1, val0, val0, val1
        };

        //axes
        if (showAxes) {
            LineArray l = new LineArray(6, LineArray.COORDINATES | LineArray.COLOR_3);

            if (colorCorrection < 1) {
                for (int n = 0; n < col.length; n++) {
                    col[n] *= colorCorrection;
                }
            } else {
                for (int n = 0; n < col.length; n++) {
                    if (col[n] == 0) {
                        col[n] = colorCorrection - 1;
                    }
                }
            }

            l.setCoordinates(0, t, 0, 6);
            l.setColors(0, col);
            l.setCapability(LineArray.ALLOW_COUNT_READ);
            l.setCapability(LineArray.ALLOW_FORMAT_READ);
            l.setCapability(LineArray.ALLOW_COORDINATE_READ);
            OpenLineAttributes la = new OpenLineAttributes(params.getLineWidth(), LineAttributes.PATTERN_SOLID, true);
            OpenShape3D tr = new OpenShape3D();
            Appearance ap = new Appearance();
            ap.setLineAttributes(la);
            tr.setGeometry(l);
            tr.setAppearance(ap);
            axes.addChild(tr);
        }

        //box
        if (showBox) {

            float[] t1 = {
                x0, y0, z0, x1, y0, z0, x0, y0, z1, x1, y0, z1, x0, y1, z0, x1, y1, z0, x0, y1, z1, x1, y1, z1,
                x0, y0, z0, x0, y1, z0, x0, y0, z1, x0, y1, z1, x1, y0, z0, x1, y1, z0, x1, y0, z1, x1, y1, z1,
                x0, y0, z0, x0, y0, z1, x0, y1, z0, x0, y1, z1, x1, y0, z0, x1, y0, z1, x1, y1, z0, x1, y1, z1
            };
            float[] col1 = {
                val1, val0, val0, val1, val0, val0, val1, val0, val0, val1, val0, val0,
                val1, val0, val0, val1, val0, val0, val1, val0, val0, val1, val0, val0,
                val0, val1, val0, val0, val1, val0, val0, val1, val0, val0, val1, val0,
                val0, val1, val0, val0, val1, val0, val0, val1, val0, val0, val1, val0,
                val0, val0, val1, val0, val0, val1, val0, val0, val1, val0, val0, val1,
                val0, val0, val1, val0, val0, val1, val0, val0, val1, val0, val0, val1
            };
            if (colorCorrection < 1) {
                for (int n = 0; n < col1.length; n++) {
                    col1[n] *= colorCorrection;
                }
            } else {
                for (int n = 0; n < col1.length; n++) {
                    if (col1[n] == 0) {
                        col1[n] = colorCorrection - 1;
                    }
                }
            }
            LineArray l1 = new LineArray(24, LineArray.COORDINATES |
                LineArray.COLOR_3);
            l1.setCoordinates(0, t1);
            l1.setColors(0, col1);
            l1.setCapability(LineArray.ALLOW_COUNT_READ);
            l1.setCapability(LineArray.ALLOW_FORMAT_READ);
            l1.setCapability(LineArray.ALLOW_COORDINATE_READ);
            OpenLineAttributes la1 = new OpenLineAttributes(.8f * params.getLineWidth(), LineAttributes.PATTERN_SOLID, true);
            OpenShape3D tr1 = new OpenShape3D();
            OpenAppearance ap1 = new OpenAppearance();
            ap1.setLineAttributes(la1);
            tr1.setGeometry(l1);
            tr1.setAppearance(ap1);
            axes.addChild(tr1);
        }

        //ticks
        int ndraw = 0;
        nx = ny = nz = 0;
        gx = gy = gz = 0;
        if (drawX) {
            nx = 10 * rx.getNsteps();
            sx = rx.getStep();
            gx = (x1 - x0) / nx;
            ndraw++;
        }
        if (drawY) {
            ny = 10 * ry.getNsteps();
            sy = ry.getStep();
            gy = (y1 - y0) / ny;
            ndraw++;
        }
        if (drawZ) {
            nz = 10 * rz.getNsteps();
            sz = rz.getStep();
            gz = (z1 - z0) / nz;
            ndraw++;
        }

        if (ndraw == 1) {
            if (gx != 0) {
                gy = gz = gx;
            } else if (gy != 0) {
                gz = gx = gy;
            } else {
                gz = gx = gy;
            }
        }

        int nGrid = nx + ny + nz;
        double[] t2 = new double[12 * nGrid];
        float[] col2 = new float[12 * nGrid];
        LineArray l2 = new LineArray(4 * nGrid, LineArray.COORDINATES | LineArray.COLOR_3);
        k = m = 0;
        if (drawX) {
            for (i = 0; i < nx; i++, m++) {
                int s = 3;
                if (i % 5 == 0) {
                    s = 2;
                }
                if (i % 10 == 0) {
                    s = 1;
                }
                t2[3 * k] = t2[3 * k + 3] = t2[3 * k + 6] = t2[3 * k + 9] = x0 + i * gx;
                t2[3 * k + 1] = t[1] - gy / s;
                t2[3 * k + 4] = t[1] + gy / s;
                t2[3 * k + 7] = t2[3 * k + 10] = t[1];
                t2[3 * k + 2] = t2[3 * k + 5] = t[2];
                t2[3 * k + 8] = t[2] - gz / s;
                t2[3 * k + 11] = t[2] + gz / s;
                for (j = 0; j < 4; j++) {
                    col2[3 * k + 3 * j] = val1;
                    col2[3 * k + 3 * j + 1] = val0;
                    col2[3 * k + 3 * j + 2] = val0;
                }
                k += 4;
            }
        }
        if (drawY) {
            for (i = 0; i < ny; i++, m++) {
                int s = 3;
                if (i % 5 == 0) {
                    s = 2;
                }
                if (i % 10 == 0) {
                    s = 1;
                }
                t2[3 * k] = t2[3 * k + 3] = t[6];
                t2[3 * k + 6] = t[6] - gx / s;
                t2[3 * k + 9] = t[6] + gx / s;
                t2[3 * k + 1] = t2[3 * k + 4] = t2[3 * k + 7] = t2[3 * k + 10] = y0 + i * gy;
                t2[3 * k + 8] = t2[3 * k + 11] = t[8];
                t2[3 * k + 2] = t[8] - gz / s;
                t2[3 * k + 5] = t[8] + gz / s;
                for (j = 0; j < 4; j++) {
                    col2[3 * k + 3 * j] = val0;
                    col2[3 * k + 3 * j + 1] = val1;
                    col2[3 * k + 3 * j + 2] = val0;
                }
                k += 4;
            }
        }
        if (drawZ) {
            for (i = 0; i < nz; i++, m++) {
                int s = 3;
                if (i % 5 == 0) {
                    s = 2;
                }
                if (i % 10 == 0) {
                    s = 1;
                }
                t2[3 * k] = t2[3 * k + 3] = t[12];
                t2[3 * k + 6] = t[12] - gx / s;
                t2[3 * k + 9] = t[12] + gx / s;
                t2[3 * k + 1] = t[13] - gy / s;
                t2[3 * k + 4] = t[13] + gy / s;
                t2[3 * k + 7] = t2[3 * k + 10] = t[13];
                t2[3 * k + 2] = t2[3 * k + 5] = t2[3 * k + 8] = t2[3 * k + 11] = z0 + i * gz;
                for (j = 0; j < 4; j++) {
                    col2[3 * k + 3 * j] = val0;
                    col2[3 * k + 3 * j + 1] = val0;
                    col2[3 * k + 3 * j + 2] = val1;
                }
                k += 4;
            }
        }

        l2.setCoordinates(0, t2);
        l2.setColors(0, col2);
        l2.setCapability(LineArray.ALLOW_COUNT_READ);
        l2.setCapability(LineArray.ALLOW_FORMAT_READ);
        l2.setCapability(LineArray.ALLOW_COORDINATE_READ);
        OpenShape3D tr2 = new OpenShape3D();
        OpenAppearance ap2 = new OpenAppearance();
        OpenLineAttributes la2 = new OpenLineAttributes(.8f * params.getLineWidth(), LineAttributes.PATTERN_SOLID, true);
        ap2.setLineAttributes(la2);
        tr2.setGeometry(l2);
        tr2.setAppearance(ap2);
        axes.addChild(tr2);

        //grid
        nx = ny = nz = 0;
        gx = gy = gz = 0;
        nLabels = 0;
        if (drawX) {
            nx = rx.getNsteps();
            sx = rx.getStep();
            gx = (x1 - x0) / nx;
            nLabels += nx - 1;
        }
        if (drawY) {
            ny = ry.getNsteps();
            sy = ry.getStep();
            gy = (y1 - y0) / ny;
            nLabels += ny - 1;
        }
        if (drawZ) {
            nz = rz.getNsteps();
            sz = rz.getStep();
            gz = (z1 - z0) / nz;
            nLabels += nz - 1;
        }
        grid[0] = grid[0] && drawX;
        grid[1] = grid[1] && drawY;
        grid[2] = grid[2] && drawZ;
        if (grid[0] || grid[1] || grid[2]) {
            LineArray l3 = new LineArray(4 * nLabels, LineArray.COORDINATES | LineArray.COLOR_3);
            nGrid = 0;
            if (grid[0]) {
                nGrid += nx - 1;
            }
            if (grid[1]) {
                nGrid += ny - 1;
            }
            if (grid[2]) {
                nGrid += nz - 1;
            }
            float[] t3 = new float[12 * nGrid];
            float[] col3 = new float[12 * nGrid];
            k = m = 0;
            if (grid[0]) {
                for (i = 1; i < nx; i++, m++) {
                    t3[3 * k]      = t3[3 * k + 3] = t3[3 * k + 6] = t3[3 * k + 9] = x0 + i * gx;
                    t3[3 * k + 1]  = t3[3 * k + 4] = t3[3 * k + 7] = y0;
                    t3[3 * k + 10] = y1;
                    t3[3 * k + 2]  = t3[3 * k + 8] = t3[3 * k + 11] = z0;
                    t3[3 * k + 5]  = z1;
                    for (j = 0; j < 4; j++) {
                        col3[3 * k + 3 * j] = val1;
                        col3[3 * k + 3 * j + 1] = val0;
                        col3[3 * k + 3 * j + 2] = val0;
                    }
                    k += 4;
                }
            }
            if (grid[1]) {
                for (i = 1; i < ny; i++, m++) {
                    t3[3 * k] = t3[3 * k + 3] = t3[3 * k + 6] = x0;
                    t3[3 * k + 9] = x1;
                    t3[3 * k + 1] = t3[3 * k + 4] = t3[3 * k + 7] = t3[3 * k + 10] = y0 + i * gy;
                    t3[3 * k + 2] = t3[3 * k + 8] = t3[3 * k + 11] = z0;
                    t3[3 * k + 5] = z1;
                    for (j = 0; j < 4; j++) {
                        col3[3 * k + 3 * j] = val0;
                        col3[3 * k + 3 * j + 1] = val1;
                        col3[3 * k + 3 * j + 2] = val0;
                    }
                    k += 4;
                }
            }
            if (grid[2]) {
                for (i = 1; i < nz; i++, m++) {
                    t3[3 * k] = t3[3 * k + 3] = t3[3 * k + 6] = x0;
                    t3[3 * k + 9] = x1;
                    t3[3 * k + 1] = t3[3 * k + 7] = t3[3 * k + 10] = y0;
                    t3[3 * k + 4] = y1;
                    t3[3 * k + 2] = t3[3 * k + 5] = t3[3 * k + 8] = t3[3 * k + 11] = z0 + i * gz;
                    for (j = 0; j < 4; j++) {
                        col3[3 * k + 3 * j] = val0;
                        col3[3 * k + 3 * j + 1] = val0;
                        col3[3 * k + 3 * j + 2] = val1;
                    }
                    k += 4;
                }
            }

            l3.setCoordinates(0, t3);
            l3.setColors(0, col3);
            l3.setCapability(LineArray.ALLOW_COUNT_READ);
            l3.setCapability(LineArray.ALLOW_FORMAT_READ);
            l3.setCapability(LineArray.ALLOW_COORDINATE_READ);
            OpenShape3D tr3 = new OpenShape3D();
            OpenAppearance ap3 = new OpenAppearance();
            OpenLineAttributes la3 = new OpenLineAttributes(.6f * params.getLineWidth(), LineAttributes.PATTERN_DOT, true);
            ap3.setLineAttributes(la3);
            tr3.setGeometry(l3);
            tr3.setAppearance(ap3);
            axes.addChild(tr3);
        }
        if (fontParams.isThreeDimensional()) {
            xValueLabels = null;
            yValueLabels = null;
            zValueLabels = null;
            xAxisLabel  = null;
            yAxisLabel  = null;
            zAxisLabel  = null;
            double[] center = new double[]{0, 0, 0};
            double r = 0;
            for (int ii = 0; ii < center.length; ii++) {
                center[ii] = (ext[0][ii] + ext[1][ii]) / 2;
                r += (ext[0][ii] - ext[1][ii]) * (ext[0][ii] - ext[1][ii]);
            }
            r = sqrt(r);
            BoundingSphere bSphere = new BoundingSphere(new Point3d(center), r);
            Color3f red = new Color3f(val1, val0, val0);
            Color3f green = new Color3f(val0, val1, val0);
            Color3f blue = new Color3f(val0, val0, val1);
            if (drawX) {
                axes.addChild(TextBillboard.createBillboard(ad[0], fontParams, red, 1.4f,
                                                            new float[]{x1 + .3f * gx, t[1], t[2]},
                                                            Billboard.ROTATE_ABOUT_POINT, bSphere));
                try {
                    for (i = 1, m = 0; i < nx; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(valueFormat[0], rx.getLow() + i * sx), fontParams, red, 1,
                                                                    new float[]{x0 + i * gx, t[1] - .3f * gy, t[2] - .3f * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                } catch (IllegalFormatException ex) {
                    VisNow.get().userMessageSend(new UserMessage("", "axes 3D", "Incorrect text format",
                                                                 "Specified format is incorrect: " + valueFormat[0] +
                                                                 "<br>" + "Dropped to default one: " + defValueFormats[1],
                                                                 Level.WARNING));
                    for (i = 1, m = 0; i < nx; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(defValueFormats[0], rx.getLow() + i * sx), fontParams, red, 1,
                                                                    new float[]{x0 + i * gx, t[1] - .3f * gy, t[2] - .3f * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                }
            }
            if (drawY) {
                axes.addChild(TextBillboard.createBillboard(ad[1], fontParams, green, 1.4f,
                                                                   new float[]{t[6], y1 + .3f * gy, t[8]},
                                                                   Billboard.ROTATE_ABOUT_POINT, bSphere));
                try {
                    for (i = 1; i < ny; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(valueFormat[1], ry.getLow() + i * sy), fontParams, green, 1,
                                                                    new float[]{t[6] - .3f * gx, y0 + i * gy, t[8] - .3f * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                } catch (IllegalFormatException ex) {
                    VisNow.get().userMessageSend(new UserMessage("", "axes 3D", "Incorrect text format",
                                                                 "Specified format is incorrect: " + valueFormat[1] +
                                                                 "<br>" + "Dropped to default one: " + defValueFormats[1],
                                                                 Level.WARNING));
                    for (i = 1; i < ny; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(defValueFormats[1], ry.getLow() + i * sy), fontParams, green, 1,
                                                                    new float[]{t[6] - .3f * gx, y0 + i * gy, t[8] - .3f * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                }
            }
            if (drawZ) {
                axes.addChild(TextBillboard.createBillboard(ad[2], fontParams, blue, 1.4f,
                                                                   new float[]{t[12], t[13], z1 + .3f * gz},
                                                                   Billboard.ROTATE_ABOUT_POINT, bSphere));
                try {
                    for (i = 1; i < nz; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(valueFormat[2], rz.getLow() + i * sz), fontParams, blue, 1,
                                                                    new float[]{t[12] - .3f * gx, t[13] - .3f * gy, z0 + i * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                } catch (IllegalFormatException ex) {
                    VisNow.get().userMessageSend(new UserMessage("", "axes 3D", "Incorrect text format",
                                                                 "Specified format is incorrect: " + valueFormat[2] +
                                                                 "<br>" + "Dropped to default one: " + defValueFormats[1],
                                                                 Level.WARNING));
                    for (i = 1; i < nz; i++, m++) {
                        axes.addChild(TextBillboard.createBillboard(String.format(defValueFormats[2], rz.getLow() + i * sz), fontParams, blue, 1,
                                                                    new float[]{t[12] - .3f * gx, t[13] - .3f * gy, z0 + i * gz},
                                                                    Billboard.ROTATE_ABOUT_POINT, bSphere));
                    }
                }
            }
        } else {
            float[] xAxisLabelCoords, yAxisLabelCoords, zAxisLabelCoords;
            if (drawX) {
                float[] xValueLabelCoords = new float[3 * nx - 3];
                String[][] xValueLabelTexts = new String[nx - 1][1];
                for (i = 1; i < nx; i++, m++) {
                    xValueLabelCoords[3 * i - 3] = x0 + i * gx;
                    xValueLabelCoords[3 * i - 2] = t[1];
                    xValueLabelCoords[3 * i - 1] = t[2];
                    try {
                        xValueLabelTexts[i - 1][0] = String.format(valueFormat[0], rx.getLow() + i * sx);
                    } catch (IllegalFormatException ex) {
                        xValueLabelTexts[i - 1][0] = String.format(defValueFormats[0], rx.getLow() + i * sx);
                    }
                }
                xColor = new Color(val1, val0, val0);
                xValueLabels = new Texts2D(xValueLabelCoords, xValueLabelTexts, fontParams);
                xAxisLabelCoords = new float[]{x1 + .3f * gx, t[1], t[2]};
                xAxisLabel = new Texts2D(xAxisLabelCoords, new String[][] {{ad[0]}}, fontParams);
            }
            if (drawY) {
                float[] yValueLabelCoords = new float[3 * ny - 3];
                String[][] yValueLabelTexts = new String[ny - 1][1];
                for (i = 1; i < ny; i++, m++) {
                    yValueLabelCoords[3 * i - 3] = t[6];
                    yValueLabelCoords[3 * i - 2] = y0 + i * gy;
                    yValueLabelCoords[3 * i - 1] = t[8];
                    try {
                        yValueLabelTexts[i - 1][0] = String.format(valueFormat[1], ry.getLow() + i * sy);
                    } catch (IllegalFormatException ex) {
                        yValueLabelTexts[i - 1][0] = String.format(defValueFormats[1], ry.getLow() + i * sy);
                    }
                }
                yColor = new Color(val0, val1, val0);
                yValueLabels = new Texts2D(yValueLabelCoords, yValueLabelTexts, fontParams);
                yAxisLabelCoords = new float[]{t[6], y1 + .3f * gy, t[8]};
                yAxisLabel = new Texts2D(yAxisLabelCoords, new String[][] {{ad[1]}}, fontParams);
            }
            if (drawZ) {
                float[] zValueLabelCoords = new float[3 * nz - 3];
                String[][] zValueLabelTexts = new String[nz - 1][1];
                for (i = 1; i < nz; i++, m++) {
                    zValueLabelCoords[3 * i - 3] = t[12];
                    zValueLabelCoords[3 * i - 2] = t[13];
                    zValueLabelCoords[3 * i - 1] = z0 + i * gz;
                    try {
                        zValueLabelTexts[i - 1][0] = String.format(valueFormat[2], rz.getLow() + i * sz);
                    } catch (IllegalFormatException ex) {
                        zValueLabelTexts[i - 1][0] = String.format(defValueFormats[2], rz.getLow() + i * sz);
                    }
                }
                zColor = new Color(val0, val0, val1);
                zValueLabels = new Texts2D(zValueLabelCoords, zValueLabelTexts, fontParams);
                zAxisLabelCoords = new float[]{t[12], t[13], z1 + .3f * gz};
                zAxisLabel = new Texts2D(zAxisLabelCoords, new String[][] {{ad[2]}}, fontParams);
            }
        }
        geometryObj.addChild(axes);
        if (geometryObj.getCurrentViewer() != null) {
            geometryObj.getCurrentViewer().refresh();
        }
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h) 
    {
        fontParams.createFontMetrics(ltw, w, h);
        if (drawX) {
            xValueLabels.draw(vGraphics, ltw, w, h, xColor);
            xAxisLabel.draw(vGraphics, ltw, w, h, xColor);
        }
        if (drawY) {
            yValueLabels.draw(vGraphics, ltw, w, h, yColor);
            yAxisLabel.draw(vGraphics, ltw, w, h, yColor);
        }
        if (drawZ) {
            zValueLabels.draw(vGraphics, ltw, w, h, zColor);
            zAxisLabel.draw(vGraphics, ltw, w, h, zColor);
        }
    }

    @Override
    public void clearAllGeometry()
    {
        super.clearAllGeometry();
        fontParams = null;
    }

    @Override
    public void setCurrentViewer(Display3DPanel panel)
    {
        super.setCurrentViewer(panel);
        createAxes();
    }

}
