//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer1D.utils;

import java.awt.Stroke;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import org.jfree.chart.ui.StrokeChooserPanel;
import org.jfree.chart.ui.StrokeSample;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.FieldViewer1DCore;

/**
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class StrokeDialog
{

    private final JComponent[] components;
    private final String title;
    private final int messageType;
    private final JRootPane rootPane;
    private final String[] options;
    private final int optionIndex;
    private final StrokeChooserPanel panel;
    private final StrokeSample[] availableStrokeSamples;

    public StrokeDialog()
    {

        this.title = "Pick a Stroke";
        this.messageType = JOptionPane.PLAIN_MESSAGE;
        this.rootPane = null;
        this.options = new String[]{"OK", "Cancel"};
        this.optionIndex = 0;
        availableStrokeSamples = new StrokeSample[FieldViewer1DCore.PLOT_STROKES.length];
        for (int i = 0; i < availableStrokeSamples.length; i++) {
            availableStrokeSamples[i] = new StrokeSample(FieldViewer1DCore.PLOT_STROKES[i]);
        }
        panel = new StrokeChooserPanel(new StrokeSample(availableStrokeSamples[0].getStroke()), this.availableStrokeSamples);
        components = new JComponent[]{panel};
    }

    public int show()
    {
        int optionType = JOptionPane.OK_CANCEL_OPTION;
        Object optionSelection = null;

        if (options.length != 0) {
            optionSelection = options[optionIndex];
        }

        int selection = JOptionPane.showOptionDialog(rootPane,
                                                     components, title, optionType, messageType, null,
                                                     options, optionSelection);

        return selection;
    }

    public Stroke getSelectedStroke()
    {
        return panel.getSelectedStroke();
    }

}
