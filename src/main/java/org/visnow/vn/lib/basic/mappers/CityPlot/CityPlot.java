/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.CityPlot;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.mappers.CityPlot.CityPlotShared.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.graph3d.AbstractGraph3D;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CityPlot extends AbstractGraph3D
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI;
    protected boolean ignoreUI = false;
    protected boolean fromUI = false;

    protected RegularField regularInField;
    protected int[] inDims = null;
    protected float[] xtCoords = null;
    protected float[] normals = null;
    protected int axis = 0, lastAxis = 0;
    protected int[] cells = null;
    protected byte[] orientations = null;
    protected float[][] physExtents = new float[2][3];

    public CityPlot()
    {
        outObj.setName("city plot");
        parameters.addParameterChangelistener((name) -> {
            if ((parameters.get(ADJUSTING) || name.equals(X_COMPONENT_STRING) || name.equals(Y_COMPONENT_STRING)) &&
                 irregularFieldGeometry != null) {
                newOutFieldStructure = false;
                updateCoords();
                irregularFieldGeometry.updateCoords();
            } else {
                newOutFieldStructure = true;
                startAction();
            }
        });
        parameters.get(X_COMPONENT).addChangeListener((ChangeEvent e) -> {
            updateCoords();
            show();
        });
        parameters.get(Y_COMPONENT).addChangeListener((ChangeEvent e) -> {
            updateCoords();
            show();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParameters(parameters);
            ui.addComputeGUI(computeUI);
            setPanel(ui);
            genericGUI = computeUI.getGenericGraph3DGUI();
        });
    }

    void updateCells()
    {
        int nCells = (int)inField.getNNodes();
        cells = new int[8 * nCells];
        for (int i = 0; i < cells.length; i++)
            cells[i] = i;
        orientations = new byte[nCells];
        Arrays.fill(orientations, (byte)1);
    }

    @Override
    protected void updateCoords()
    {
        updateScale();
        float xLow = parameters.get(X_RANGE)[0];
        float xUp =  parameters.get(X_RANGE)[1];
        float yLow = parameters.get(Y_RANGE)[0];
        float yUp =  parameters.get(Y_RANGE)[1];
        float[] xVals = null;
        float xValMin = 0, dx = 0;
        float[] yVals = null;
        float yValMin = 0, dy = 0;
        DataArray xDA = regularInField.getComponent(parameters.get(X_COMPONENT).getComponentName());
        if (xDA != null) {
            xVals = xDA.getVectorLength() == 1 ? xDA.getRawFloatArray().getData() : xDA.getVectorNorms().getData();
            xValMin = (float)xDA.getPreferredMinValue();
            dx = (xUp - xLow) / (float)(xDA.getPreferredMaxValue() - xValMin);
        }
        DataArray yDA = regularInField.getComponent(parameters.get(Y_COMPONENT).getComponentName());
        if (yDA != null) {
            yVals = yDA.getVectorLength() == 1 ? yDA.getRawFloatArray().getData() : yDA.getVectorNorms().getData();
            yValMin = (float)yDA.getPreferredMinValue();
            dy = (yUp - yLow) / (float)(yDA.getPreferredMaxValue() - yValMin);
        }

        int dp = inDims[0] + 1;
        if (inField.hasMask()) {
            byte[] mask = inField.getCurrentMask().getByteData();
            for (int i = 0, k = 0, m = 0; i < inDims[1]; i++)
                for (int j = 0; j < inDims[0]; j++, k += 24, m++) {
                    int p = i * dp + j;
                    if (mask[m] == 0) {
                        for (int l = 0; l < 3; l++)
                            outCoords[k + l]      = outCoords[k + 3 + l]  = outCoords[k + 6 + l]  = outCoords[k + 9 + l] =
                            outCoords[k + 12 + l] = outCoords[k + 15 + l] = outCoords[k + 18 + l] = outCoords[k + 21 + l] =
                                   xtCoords[3 *  p + l];
                        continue;
                    }
                    float v = scale * (vals[inDims[0] * i + j] - base);
                    float dx0 = xVals == null ? xUp - xLow : dx * (xVals[inDims[0] * i + j] - xValMin);
                    float dy0 = yVals == null ? yUp - yLow : dy * (yVals[inDims[0] * i + j] - yValMin);
                    for (int l = 0; l < 3; l++) {
                        float x = xLow, y = yLow;
                        outCoords[k + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);

                        outCoords[k + 12 + l] = outCoords[k + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));
                        x = xLow + dx0;
                        y = yLow;
                        outCoords[k + 3 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p +       l]);
                        outCoords[k + 15 + l] = outCoords[k + 3 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p +       l]));
                        x = xLow + dx0;
                        y = yLow + dy0;
                        outCoords[k + 6 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);
                        outCoords[k + 18 + l] = outCoords[k + 6 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));
                        x = xLow;
                        y = yLow+ dy0;
                        outCoords[k + 9 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);
                        outCoords[k + 21 + l] = outCoords[k + 9 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));

                    }
                }
        }
        else
            for (int i = 0, k = 0; i < inDims[1]; i++)
                for (int j = 0; j < inDims[0]; j++, k += 24) {
                    int p = i * dp + j;
                    float v = scale * (vals[inDims[0] * i + j] - base);
                    float dx0 = xVals == null ? xUp - xLow : dx * (xVals[inDims[0] * i + j] - xValMin);
                    float dy0 = yVals == null ? yUp - yLow : dy * (yVals[inDims[0] * i + j] - yValMin);
                    for (int l = 0; l < 3; l++) {
                        float x = xLow, y = yLow;
                        outCoords[k + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);
                        outCoords[k + 12 + l] = outCoords[k + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));
                        x = xLow + dx0;
                        y = yLow;
                        outCoords[k + 3 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p +       l]);
                        outCoords[k + 15 + l] = outCoords[k + 3 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p +       l]));
                        x = xLow + dx0;
                        y = yLow + dy0;
                        outCoords[k + 6 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);
                        outCoords[k + 18 + l] = outCoords[k + 6 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));
                        x = xLow;
                        y = yLow+ dy0;
                        outCoords[k + 9 + l] =
                                y  * (x * xtCoords[3 * (p + dp + 1) + l] + (1-x) * xtCoords[3 * (p + dp) + l]) +
                           (1 - y) * (x * xtCoords[3 * (p      + 1) + l] + (1-x) * xtCoords[3 *  p       + l]);
                        outCoords[k + 21 + l] = outCoords[k + 9 + l] +
                                v * (y  * (x * normals[3 * (p + dp + 1) + l] + (1-x) * normals[3 * (p + dp) + l]) +
                                (1 - y) * (x * normals[3 * (p      + 1) + l] + (1-x) * normals[3 *  p       + l]));

                    }
                }
        outIrregularField.setCoords(new FloatLargeArray(outCoords), 0);
    }

    private static float[] createNormals(int[] d, float[] crd)
    {
        float[] norm = new float[crd.length];
        float[] p0 = new float[3], u =  new float[3], v =  new float[3], w  = new float[3];
        for (int i = 0, l = 0; i < d[1]; i++)
            for (int j = 0; j < d[0]; j++, l += 3) {
                System.arraycopy(crd, l, p0, 0, 3);
                if (j < d[0] - 1)
                    for (int k = 0; k < 3; k++)
                        u[k] = crd[l + 3 + k] - p0[k];
                else
                    for (int k = 0; k < 3; k++)
                        u[k] = p0[k] - crd[l - 3 + k];
                if (i < d[1] - 1)
                    for (int k = 0; k < 3; k++)
                        v[k] = crd[l + 3 * d[0] + k] - p0[k];
                else
                    for (int k = 0; k < 3; k++)
                        v[k] = p0[k] - crd[l - 3 * d[0] + k];
                w[0] = u[1] * v[2] - u[2] * v[1];
                w[1] = u[2] * v[0] - u[0] * v[2];
                w[2] = u[0] * v[1] - u[1] * v[0];
                float r = (float)Math.sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
                for (int k = 0; k < 3; k++)
                    norm[l + k] = w[k] / r;
            }
        return norm;
    }

    private static float[] createDualMesh(int[] d, float[][] af)
    {
        float[] xcrd = new float[3 * (d[0] + 1) * (d[1] + 1)];
        for (int i = 0, l = 0; i <= d[1]; i++)
            for (int j = 0; j <= d[0]; j++, l += 3)
                for (int k = 0; k < 3; k++)
                    xcrd[l + k] = af[3][k] + (i - .5f) * af[1][k] + (j - .5f) * af[0][k];
        return xcrd;
    }

    private static float[] createDualMesh(int[] d, float[] crd)
    {
        float[] xcrd = new float[3 * (d[0] + 1) * (d[1] + 1)];
        int[] off = new int[] {0, 3, 3 * d[0], 3 * d[0] + 3};
        for (int i = 0; i < d[1] - 1; i++) {
            // internal nodes - cell centers
            for (int j = 0, l = 3 * i * d[0], m = 3 * (i + 1) * (d[0] + 1) + 3; j < d[0] - 1; j++, l += 3, m += 3) {
                for (int k = 0; k < 3; k++) {
                    xcrd[m + k] = 0;
                    for (int p = 0; p < off.length; p++)
                       xcrd[m + k] += crd[l + off[p] + k] ;
                    xcrd[m + k] /= 4;
                 }
            }
            // 0 column nodes
            int m = 3 * (i + 1) * (d[0] + 1);
            int p = 3 * i * d[0];
            for (int k = 0; k < 3; k++)
                xcrd[m + k] = (3 * crd[p            + k] - crd[p            + 3 + k] +
                               3 * crd[p + 3 * d[0] + k] - crd[p + 3 * d[0] + 3 + k]) / 4;
            // d[0] column nodes
            m = 3 * ((i + 1) * (d[0] + 1) + d[0]);
            p = 3 * (i * d[0] + d[0] - 1);
            for (int k = 0; k < 3; k++)
                xcrd[m + k] = (3 * crd[p            + k] - crd[p            - 3 + k] +
                               3 * crd[p + 3 * d[0] + k] - crd[p + 3 * d[0] - 3 + k]) / 4;
        }
        for (int j = 0, m = 3, n = 3 * d[1] * (d[0] + 1) + 3, p = 0, q = 3 * (d[1] - 1) * d[0];
             j < d[0] - 1; j++, m += 3, n += 3, p += 3, q += 3) {
            // 0 row nodes
            for (int k = 0; k < 3; k++)
                xcrd[m + k] = (3 * crd[p     + k] - crd[p + 3 * d[0]     + k] +
                               3 * crd[p + 3 + k] - crd[p + 3 * d[0] + 3 + k]) / 4;
            // d[1] row nodes
            for (int k = 0; k < 3; k++)
                xcrd[n + k] = (3 * crd[q     + k] - crd[q - 3 * d[0]     + k] +
                               3 * crd[q + 3 + k] - crd[q - 3 * d[0] + 3 + k]) / 4;
        }
        int d1 = 3 * (d[0] + 1);
        for (int k = 0; k < 3; k++) {
            xcrd[k]     = (2 * xcrd[    3 + k] - xcrd[    6 + k] + 2 * xcrd[    d1 + k] - xcrd[    2 * d1 + k]) / 2;
            int l = 3 * d[0];
            xcrd[l + k] = (2 * xcrd[l - 3 + k] - xcrd[l - 6 + k] + 2 * xcrd[l + d1 + k] - xcrd[l + 2 * d1 + k]) / 2;
            l = 3 * d[1] * (d[0] + 1);
            xcrd[l + k] = (2 * xcrd[l + 3 + k] - xcrd[l + 6 + k] + 2 * xcrd[l - d1 + k] - xcrd[l - 2 * d1 + k]) / 2;
            l = 3 * (d[1] * (d[0] + 1) + d[0]);
            xcrd[l + k] = (2 * xcrd[l - 3 + k] - xcrd[l - 6 + k] + 2 * xcrd[l - d1 + k] - xcrd[l - 2 * d1 + k]) / 2;
        }
        return xcrd;
    }

    @Override
    protected void updateInputCoords()
    {
        super.updateInputCoords();
        regularInField = (RegularField)inField;
        inDims = regularInField.getDims();
        if (regularInField.hasCoords())
            xtCoords = createDualMesh(inDims, regularInField.getCurrentCoords().getData());
        else
            xtCoords = createDualMesh(inDims, regularInField.getAffine());
        if (fieldPlane >= 0) {
            normals = new float[xtCoords.length];
            for (int i = 0; i < normals.length; i += 3) {
                normals[i] = normals[i + 1] = normals[i + 2] = 0;
                normals[i + fieldPlane] = 1;
            }
        }
        else
            normals = createNormals(new int[] {inDims[0] + 1, inDims[1] + 1}, xtCoords);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        Parameter[] generalParams = super.createDefaultParameters();
        Parameter[] currentParams = new Parameter[]{
            new Parameter<>(X_COMPONENT,  new ComponentFeature(true, false, false, true, true, true)),
            new Parameter<>(X_RANGE,      new float[]{0, 1}),
            new Parameter<>(Y_COMPONENT,  new ComponentFeature(true, false, false, true, true, true)),
            new Parameter<>(Y_RANGE,      new float[]{0, 1})
        };
        Parameter[] params = new Parameter[generalParams.length + currentParams.length];
        System.arraycopy(generalParams, 0, params, 0, generalParams.length);
        System.arraycopy(currentParams, 0, params, generalParams.length, currentParams.length);
        return params;
    }

    @Override
    protected void updateRenderingParams()
    {
        renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
    }

    @Override
    protected void additionalParamsSetting(Field in)
    {
        if (!in.isDataCompatibleWith(inField)) {
            parameters.get(X_COMPONENT).setContainerSchema(in.getSchema());
            parameters.get(Y_COMPONENT).setContainerSchema(in.getSchema());
        }
    }

    @Override
    protected void updateOutputData()
    {
        outIrregularField.getComponents().clear();
        for (DataArray dta : regularInField.getComponents()) {
            int vl = dta.getVectorLength();
            LargeArray inDB = dta.getRawArray();
            LargeArray outDB = LargeArrayUtils.create(inDB.getType(), 8 * inDB.length());
            for (long i = 0; i < inField.getNNodes(); i++)
                for (int j = 0; j < 8; j++)
                    LargeArrayUtils.arraycopy(inDB, i * vl, outDB, (8 * i + j) * vl, vl);
            outIrregularField.addComponent(DataArray.create(outDB, vl, dta.getName()).
                                           preferredRanges(dta.getPreferredMinValue(),    dta.getPreferredMaxValue(),
                                                           dta.getPreferredPhysMinValue(),dta.getPreferredPhysMaxValue()));
        }
    }

    @Override
    public void createOutField()
    {
        outIrregularField = new IrregularField(8 * (int) regularInField.getNNodes());
        if (inField.hasMask()) {
            LogicLargeArray outMask = new LogicLargeArray(8 * (int) regularInField.getNNodes());
            for (long i = 0, l = 0; i < regularInField.getNNodes(); i++)
                for (int j = 0; j < 8; j++, l++)
                    outMask.setBoolean(l, inField.getCurrentMask().getBoolean(i));
            outIrregularField.setCurrentMask(outMask);
        }
        outCoords = new float[3 * (int) outIrregularField.getNNodes()];
        updateInputCoords();
        createGraph3DData();
        updateCoords();
        outIrregularField.setCurrentCoords(new FloatLargeArray(outCoords));
        updateCells();
        CellSet cs = new CellSet();
        cs.setCellArray(new CellArray(CellType.HEXAHEDRON, cells, orientations, null));
        cs.generateExternFaces();
        outIrregularField.addCellSet(cs);
        outField = outIrregularField;
    }

}
