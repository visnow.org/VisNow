/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

 package org.visnow.vn.lib.basic.readers.ReadCDM4;

import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;

/**
 * Parameters for HDF5/CDM4 reader.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCDM4Shared
{

    public static final ParameterName<String> FILENAME = new ParameterName("File name");

    public static final ParameterName<String[]> VARIABLE_NAMES = new ParameterName("Variable names");

    public static final ParameterName<String> VISNOW_FIELD_NAME = new ParameterName("VisNow field name");

    public static final ParameterName<String[]> VISNOW_COMPONENT_NAMES = new ParameterName("VisNow component names");

    public static final ParameterName<Boolean> FIRST_DIMENSION_IS_TIME = new ParameterName("First dimension is time");

    public static final ParameterName<Boolean> LAST_DIMENSION_IS_VECTOR = new ParameterName("Last dimension is vector");

    public static Parameter[] getDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(VISNOW_FIELD_NAME, ""),
            new Parameter<>(VISNOW_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(VARIABLE_NAMES, new String[]{}),
            new Parameter<>(FIRST_DIMENSION_IS_TIME, false),
            new Parameter<>(LAST_DIMENSION_IS_VECTOR, false)
        };
    }
}
