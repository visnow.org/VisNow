/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class CalculableParameter
{

    protected String name;
    protected ArrayList<PointDescriptor> pointDescriptors = new ArrayList<PointDescriptor>();
    private String shortcut = "";
    private float[] parameters = null;

    public CalculableParameter(String name)
    {
        this.name = name;
    }

    public CalculableParameter(String name, float[] parameters)
    {
        this.name = name;
        this.parameters = parameters;
    }

    public abstract boolean isPointDescriptorsReady();

    public void setPointDescriptors(ArrayList<PointDescriptor> pds)
    {
        if (pds == null)
            return;

        pointDescriptors.clear();
        for (int i = 0; i < pds.size(); i++) {
            pointDescriptors.add(pds.get(i));
        }
    }

    public ArrayList<PointDescriptor> getDependantPointDescriptors()
    {
        return pointDescriptors;
    }

    public String getDependantPointNameList()
    {
        String out = "";
        if (pointDescriptors.size() < 1)
            return out;

        for (int i = 0; i < pointDescriptors.size() - 1; i++) {
            out += pointDescriptors.get(i).getName() + ", ";
        }
        out += pointDescriptors.get(pointDescriptors.size() - 1).getName();
        return out;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public float[] getParameters()
    {
        return parameters;
    }

    public abstract float getValue();

    /**
     * @return the location
     */
    public abstract int getLocationPointIndex();

    public PointDescriptor getLocationPoint()
    {
        if (this.pointDescriptors == null || this.pointDescriptors.size() <= getLocationPointIndex())
            return null;
        return pointDescriptors.get(getLocationPointIndex());
    }

    public abstract CalculableParamsPool.CalculableType getType();

    /**
     * @return the shortcut
     */
    public String getShortcut()
    {
        return shortcut;
    }

    /**
     * @param shortcut the shortcut to set
     */
    public void setShortcut(String shortcut)
    {
        this.shortcut = shortcut;
    }

}
