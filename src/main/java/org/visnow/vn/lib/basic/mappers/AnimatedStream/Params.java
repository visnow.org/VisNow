/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.AnimatedStream;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.ComponentColorMap;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{

    public static final int FORWARD = 1;
    public static final int STOP = 0;
    public static final int BACK = -1;

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("segmentLength", ParameterType.dependent, 5),
        new ParameterEgg<Integer>("segmentGap", ParameterType.dependent, 1),
        new ParameterEgg<Float>("lineWidth", ParameterType.dependent, 1.f),
        new ParameterEgg<Boolean>("comet", ParameterType.independent, true),
        new ParameterEgg<Integer>("delay", ParameterType.independent, 0),
        new ParameterEgg<Integer>("animate", ParameterType.independent, STOP),
        new ParameterEgg<ComponentColorMap>("map", ParameterType.independent, new ComponentColorMap())
    };

    public Params()
    {
        super(eggs);
    }

    public int getGap()
    {
        return (Integer) getValue("segmentGap");
    }

    public void setGap(int val)
    {
        if (val < 0)
            return;
        setValue("segmentGap", val);
        fireStateChanged();
    }

    public int getDelay()
    {
        return (Integer) getValue("delay");
    }

    public void setDelay(int delay)
    {
        setValue("delay", delay);
    }

    public int getSegmentLength()
    {
        return (Integer) getValue("segmentLength");
    }

    public void setSegmentLength(int segmentLength)
    {
        if (segmentLength < 3)
            return;
        setValue("segmentLength", segmentLength);
        fireStateChanged();
    }

    public int getAnimate()
    {
        return (Integer) getValue("animate");
    }

    public void setAnimate(int animate)
    {
        setValue("animate", animate);
        fireStateChanged();
    }
    
    public boolean isCometEffect()
    {
        return (Boolean)getValue("comet");
    }
    
    public void setCometEffect(boolean val)
    {
        setValue("comet", val);
        fireStateChanged();
    }
    
    public float getLineWidth()
    {
        return (Float)getValue("lineWidth");
    }
    
    public void setLineWidth(float val)
    {
        setValue("lineWidth", val);
        fireStateChanged();
    }
    public ComponentColorMap getMap()
    {
        return (ComponentColorMap)getValue("map");
    }
}
