/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Glyphs;

import java.awt.Color;
import static org.apache.commons.math3.util.FastMath.*;

import org.jogamp.java3d.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;

import org.visnow.vn.datamaps.ColorMap;

import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;

import org.visnow.vn.geometries.geometryTemplates.Glyph;
import org.visnow.vn.geometries.objects.generics.*;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;

import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.utils.SwingInstancer;

import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.DataContainerSchema;

import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.geometryTemplates.ScalarGlyphTemplates;
import org.visnow.vn.geometries.geometryTemplates.VectorGlyphTemplates;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.field.subset.DownsizeSelect;
import org.visnow.vn.lib.utils.field.subset.MaskSelector;
import org.visnow.vn.lib.utils.field.subset.NodeSelector;
import org.visnow.vn.lib.utils.field.subset.ThresholdMaskSelector;
import org.visnow.vn.lib.utils.field.subset.ThresholdSelector;


/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 *
 * Revisions above 566 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Glyphs extends VisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected OpenBranchGroup outGroup = new OpenBranchGroup();

    protected Params params;

    protected Field inField = null;
    protected float lowv = 0;
    protected float dv = 0;
    protected boolean isValidity = false;
    protected LogicLargeArray valid = null;
    protected DataArray glyphDataArray;
    protected float[] baseCoords = null;
    protected float[] baseU = null;
    protected float[] baseV = null;
    protected float[] baseW = null;
    protected int nGlyphs, nstrip, nvert, nind, ncol;
    protected boolean isNormals = false;
    protected long[] glyphIn = null;
    protected int[] cIndex = null;
    protected int[] pIndex = null;
    protected int[] strips = null;
    protected float[] verts = null;
    protected float[] normals = null;
    protected byte[] colors = null;
    protected float[] uvData = null;

    protected Glyph gt = null;

    protected IndexedGeometryStripArray surf = null;

    protected OpenShape3D surfaces = new OpenShape3D();
    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenTransparencyAttributes transparencyAttributes = new OpenTransparencyAttributes();
    protected OpenLineAttributes lattr = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);

    protected ColorMap colorMap = null;

    protected float lastTime = -Float.MAX_VALUE;
    protected int currentColorMode = -1;

    protected Texture2D texture = null;

    protected static final boolean[][] resetGeometry
        = {{false, true, true, true, true, true, true, true, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, true, true, true, true, true, true, true, false}
        };

    private boolean fromUI = false;
    private boolean fromIn = false;

    protected GUI gui = null;

    public Glyphs()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (fromIn)
                    return;
                if (inField != null) {
                    fromUI = true;
                    update();
                    fromUI = false;
                }
            }
        });

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {

            }
        });

        appearance.setTransparencyAttributes(transparencyAttributes);
        appearance.getPolygonAttributes().setBackFaceNormalFlip(true);

        surfaces.setCapability(Shape3D.ENABLE_PICK_REPORTING);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_READ);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
        surfaces.setCapability(Geometry.ALLOW_INTERSECT);
        surfaces.setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);

        surfaces.setAppearance(appearance);

        outGroup.addChild(surfaces);

        outObj.addNode(outGroup);

        RenderEventListener renderListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (inField != null) {
                    int extent = e.getUpdateExtent();
                    int cMode = dataMappingParams.getColorMode();
                    if (renderingParams.getDisplayMode() == RenderingParams.BACKGROUND)
                        cMode = DataMappingParams.UNCOLORED;
                    if (currentColorMode < 0) {
                        updateGeometry();
                        currentColorMode = cMode;
                        return;
                    }
                    if (extent == RenderEvent.COLORS || extent == RenderEvent.TRANSPARENCY || extent == RenderEvent.TEXTURE) {
                        if (resetGeometry[currentColorMode][cMode])
                            updateGeometry();
                        else
                            updateColors();
                        currentColorMode = cMode;
                        return;
                    }
                    if (extent == RenderEvent.COORDS)
                        updateCoords();
                    if (extent == RenderEvent.GEOMETRY)
                        updateGeometry();
                    currentColorMode = cMode;
                }
            }
        };
        dataMappingParams.addRenderEventListener(renderListener);
        renderingParams.addRenderEventListener(renderListener);

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                gui = new GUI();
                gui.setParams(params);
                setPanel(gui);
            }
        });
    }

    private void prepareLocalCoords()
    {
        if (glyphDataArray == null)
            return;
        float[] um = {0, 0, 0};
        float[] vm = {0, 0, 0};
        float[] wm = {0, 0, 0};

        baseU = new float[3 * nGlyphs];
        baseV = new float[3 * nGlyphs];
        baseW = new float[3 * nGlyphs];
        //float[] p = {0, 0, 0};
        int vlen = glyphDataArray.getVectorLength();
        for (int i = 0; i < nGlyphs; i++) {
            float[] p = glyphDataArray.getFloatElement(glyphIn[i]);

            float pn = 0;
            for (int j = 0; j < p.length; j++)
                pn += p[j] * p[j];
            if (vlen == 1 || vlen > 3 || pn < 1e-20) {
                for (int j = 0; j < 3; j++)
                    baseU[3 * i + j] = baseV[3 * i + j] = baseW[3 * i + j] = 0;
                baseU[3 * i] = baseV[3 * i + 1] = baseW[3 * i + 2] = 1;
                continue;
            }
            pn = (float) sqrt(pn);
            for (int j = 0; j < p.length; j++)
                um[j] = p[j] / pn;
            if (abs(um[0]) > abs(um[1]) && abs(um[0]) > abs(um[2])) {
                vm[0] = vm[1] = 0;
                vm[2] = 1;
            } else {
                vm[1] = vm[2] = 0;
                vm[0] = 1;
            }
            wm[0] = um[1] * vm[2] - um[2] * vm[1];
            wm[1] = um[2] * vm[0] - um[0] * vm[2];
            wm[2] = um[0] * vm[1] - um[1] * vm[0];
            pn = (float) sqrt(wm[0] * wm[0] + wm[1] * wm[1] + wm[2] * wm[2]);
            for (int j = 0; j < 3; j++)
                wm[j] /= pn;
            vm[0] = um[2] * wm[1] - um[1] * wm[2];
            vm[1] = um[0] * wm[2] - um[2] * wm[0];
            vm[2] = um[1] * wm[0] - um[0] * wm[1];
            System.arraycopy(um, 0, baseW, 3 * i, 3);
            System.arraycopy(vm, 0, baseV, 3 * i, 3);
            System.arraycopy(wm, 0, baseU, 3 * i, 3);
        }
    }

    private void prepareGlyphCount()
    {
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        NodeSelector nodeSelector = new NodeSelector();
        DataArray thresholdDataArray = inField.getComponent(params.getValidComponentRange().getComponentName());
        if (thresholdDataArray != null && inField.hasMask())
            nodeSelector = new ThresholdMaskSelector(inField.getCurrentMask(),
                                                     thresholdDataArray,
                                                     params.getValidComponentRange().getLow(),
                                                     params.getValidComponentRange().getUp(),
                                                     params.isInsideRange());
        else if (thresholdDataArray != null)
            nodeSelector = new ThresholdSelector(thresholdDataArray,
                                                 params.getValidComponentRange().getLow(),
                                                 params.getValidComponentRange().getUp(),
                                                 params.isInsideRange());
        else if (inField.hasMask())
            nodeSelector = new MaskSelector(inField.getCurrentMask());

        int maxNGlyphs = 1;
        if (inField instanceof RegularField) {
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            for (int i = 0; i < ((RegularField) inField).getDimNum(); i++)
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
        } else
            maxNGlyphs = (int)params.getDownsizeParams().getPreferredSize();
        glyphIn = DownsizeSelect.select(inField, params.getDownsizeParams(), maxNGlyphs,
                                        nodeSelector, params.getLowCrop(), params.getUpCrop());
        nGlyphs = glyphIn.length;
        colors = new byte[3 * nGlyphs];
        baseCoords = new float[3 * nGlyphs];
        surf = null;
        updateBaseCoords();
    }

    private void updateBaseCoords()
    {
        for (int i = 0; i < baseCoords.length; i++)
            baseCoords[i] = 0;
        if (inField.getCurrentCoords() != null) {
            FloatLargeArray fldCoords = inField.getCurrentCoords();
            for (int i = 0; i < nGlyphs; i++)
                for (int j = 0; j < 3; j++)
                    baseCoords[3 * i + j] = fldCoords.get(3 * glyphIn[i] + j);
        } else if (inField instanceof RegularField) {
            float[][] inAff = ((RegularField) inField).getAffine();
            long[] dims = ((RegularField) inField).getLDims();
            int i0 = 0, i1 = 0, i2 = 0;
            for (int i = 0; i < nGlyphs; i++) {
                long j = glyphIn[i];
                i0 = (int) (j % dims[0]);
                if (dims.length > 1) {
                    j /= dims[0];
                    i1 = (int) (j % dims[1]);
                    if (dims.length > 2)
                        i2 = (int) (j / dims[1]);
                }
                for (int k = 0; k < 3; k++)
                    baseCoords[3 * i + k] = inAff[3][k] + i0 * inAff[0][k] + i1 * inAff[1][k] + i2 * inAff[2][k];
            }
        }
    }

    public void updateColors()
    {
        int[] glyphIndices = new int[glyphIn.length];
        for (int i = 0; i < glyphIndices.length; i++) {
            glyphIndices[i] = (int)glyphIn[i];
        }
        colors = ColorMapper.mapColorsIndexed(inField, dataMappingParams, glyphIndices, renderingParams.getDiffuseColor(), colors);
        if (inField.getComponent(dataMappingParams.getTransparencyParams().getComponentRange().getComponentName()) == null &&
            params.getTransparency() == 0) {
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        }
        surf.setColors(0, colors);
    }

    public void updateGeometry()
    {
        surfaces.removeAllGeometries();
        surf = null;

        int cMode = dataMappingParams.getColorMode();
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND)
            cMode = DataMappingParams.UNCOLORED;
        generateGlyphs(cMode);
        surfaces.addGeometry(surf);
        outObj.setExtents(inField.getPreferredExtents());

        appearance.setUserData(this);
        appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

        Color bgrColor2 = renderingParams.getBackgroundColor();
        float[] bgrColorComps = new float[3];
        bgrColor2.getColorComponents(bgrColorComps);
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND)
            appearance.getColoringAttributes().setColor(new Color3f(bgrColorComps[0], bgrColorComps[1], bgrColorComps[2]));
        else
            appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());

        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
    }

    private void generateGlyphs(int cMode)
    {
        if (nGlyphs < 1)
            return;
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        int type = params.getType();
        int lod = params.getLod();

        if (glyphDataArray == null || glyphDataArray.getVectorLength() == 1)
            gt = ScalarGlyphTemplates.glyph(type, lod);
        else
            gt = VectorGlyphTemplates.glyph(type, lod);
        nstrip = nGlyphs * gt.getNstrips();
        nvert = nGlyphs * gt.getNverts();
        nind = nGlyphs * gt.getNinds();
        ncol = nGlyphs;
        strips = new int[nstrip];
        verts = new float[3 * nvert];
        normals = new float[3 * nvert];
        pIndex = new int[nind];
        cIndex = new int[nind];
        makeIndices();
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 1)
            prepareLocalCoords();
        if (verts == null || verts.length != 3 * nGlyphs * gt.getNverts())
            verts = new float[3 * nGlyphs * gt.getNverts()];

        int verticesMode = GeometryArray.COORDINATES;
        isNormals = false;
        if (gt.getType() != Glyph.LINE_STRIPS) {
            verticesMode |= GeometryArray.NORMALS;
            isNormals = true;
        }
        if (cMode == DataMappingParams.UVTEXTURED)
            verticesMode |= GeometryArray.TEXTURE_COORDINATE_2;
        else
            verticesMode |= GeometryArray.COLOR_4;

        if (isNormals && (normals == null || normals.length != 3 * nGlyphs * gt.getNverts()))
            normals = new float[3 * nGlyphs * gt.getNverts()];

        switch (gt.getType()) {
            case Glyph.TRIANGLE_STRIPS:
                surf = new IndexedTriangleStripArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.TRIANGLE_FANS:
                surf = new IndexedTriangleFanArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.LINE_STRIPS:
                surf = new IndexedLineStripArray(nvert, verticesMode, nind, strips);
                break;
        }
        surf.setCapability(IndexedLineStripArray.ALLOW_COUNT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_FORMAT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_INDEX_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_WRITE);
        if (cMode == DataMappingParams.UVTEXTURED) {
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_WRITE);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_WRITE);
        } else {
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_WRITE);
            surf.setColorIndices(0, cIndex);
        }
        surf.setCoordinates(0, verts);
        surf.setCoordinateIndices(0, pIndex);
        if (isNormals) {
            surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_WRITE);
            surf.setNormals(0, normals);
            surf.setNormalIndices(0, pIndex);
        }
        OpenMaterial mat = new OpenMaterial();
        mat.setShininess(15.f);
        mat.setColorTarget(OpenMaterial.AMBIENT_AND_DIFFUSE);
        appearance.setMaterial(mat);
        PolygonAttributes pattr = new PolygonAttributes(
            PolygonAttributes.POLYGON_FILL,
            PolygonAttributes.CULL_NONE, 0.f, true);
        appearance.setPolygonAttributes(pattr);
        OpenColoringAttributes colAttrs = new OpenColoringAttributes();
        colAttrs.setColor(renderingParams.getDiffuseColor());
        appearance.setColoringAttributes(colAttrs);
        appearance.setLineAttributes(lattr);
        updateCoords();
    }

    private void updateCoords()
    {
        lattr.setLineWidth(params.getLineThickness());
        float scale = params.getScale();
        float s = 0;
        float st = 0;
        boolean useAbs = params.isUseAbs();
        boolean useSqrt = params.isUseSqrt();
        float[] tVerts = gt.getVerts();
        float[] tNorms = gt.getNormals();
        float[] p = new float[3];
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        if (glyphDataArray == null) {
            s = scale;
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        } else if (glyphDataArray.getVectorLength() == 1)
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (params.isConstantDiam())
                    s = scale;
                else {
                    s = glyphDataArray.getFloatElement(glyphIn[i])[0];
                    if (useAbs || useSqrt)
                        s = abs(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= scale;
                }
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        else
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                System.arraycopy(baseU, 3 * i, u, 0, 3);
                System.arraycopy(baseV, 3 * i, v, 0, 3);
                System.arraycopy(baseW, 3 * i, w, 0, 3);
                float[] vs = glyphDataArray.getFloatElement(glyphIn[i]);
                if (params.isConstantDiam())
                    s = scale;
                else {
                    s = 0;
                    for (int j = 0; j < vs.length; j++)
                        s += vs[j] * vs[j];
                    s = (float) sqrt(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= scale;
                }
                if (params.isConstantThickness())
                    st = params.getThickness();
                else
                    st = s;
                if (isNormals)
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++) {
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
                            normals[k] = tNorms[3 * j] * u[l] + tNorms[3 * j + 1] * v[l] + tNorms[3 * j + 2] * w[l];
                        }
                else
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++)
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
            }
        surf.setCoordinates(0, verts);
        if (isNormals)
            surf.setNormals(0, normals);
    }

    public void update()
    {
        if (params.getChange() == Params.GEOMETRY_CHANGED) {
            prepareGlyphCount();
            currentColorMode = dataMappingParams.getColorMode();
        }
        if (nGlyphs < 1)
            return;
        if (params.getChange() >= Params.GLYPHS_CHANGED)
            updateGeometry();
        if (params.getChange() >= Params.COORDS_CHANGED)
            updateCoords();
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        appearance.getTransparencyAttributes().setTransparency(params.getTransparency());
        if (params.getTransparency() == 0)
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        else
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
        params.setChange(0);

        outObj.clearAllGeometry();
        outObj.addNode(outGroup);
        outObj.setExtents(inField.getPreferredExtents());
    }

    protected void makeIndices()
    {
        int istrip = 0, iind = 0, ivert = 0, icol = 0;
        for (int n = 0; n < nGlyphs; n++) {
            for (int i = 0; i < gt.getNstrips(); i++, istrip++)
                strips[istrip] = gt.getStrips()[i];
            for (int i = 0; i < gt.getNinds(); i++, iind++) {
                pIndex[iind] = ivert + gt.getPntsIndex()[i];
                cIndex[iind] = icol;
            }
            ivert += gt.getNverts();
            icol += 1;
        }
    }

    @Override
    public void onActive()
    {
        if (!fromUI) {
            fromIn = true;
            if (getInputFirstValue("inField") == null)
                return;
            Field inFld = ((VNField) getInputFirstValue("inField")).getField();
            if (inFld == null)
                return;
            if (inField != inFld) {
                inField = inFld;
                outObj.setExtents(inField.getPreferredExtents());
                lastTime = inField.getCurrentTime();
                dataMappingParams.setInData(inFld, (DataContainerSchema)null);
                params.getDownsizeParams().setFieldData(inField.getNNodes(),
                        inField instanceof RegularField ? ((RegularField)inField).getDims() : null);
                params.getValidComponentRange().setContainer(inField);
                gui.setInData(inField, dataMappingParams);
                isValidity = inField.hasMask();
                valid = inField.getCurrentMask();
                params.setChange(Params.GEOMETRY_CHANGED);
            } else {
                lastTime = inField.getCurrentTime();
                isValidity = inField.hasMask();
                valid = inField.getCurrentMask();
                params.setChange(Params.GEOMETRY_CHANGED);
            }
            fromIn = false;
        }
        update();
        fromUI = false;
    }
}
