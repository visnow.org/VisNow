/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ExtendedVolumeSegmentation;

import java.util.ArrayList;
import java.util.Stack;
import javax.swing.ProgressMonitor;
import org.visnow.jscic.RegularField;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RangeSegmentVolume extends SegmentVolume
{

    protected ArrayList<Integer> indices = null;
    protected byte[] data = null;
    protected float rangeMin = -1;
    protected float rangeMax;
    /*
     * mask[i] = 1 for background,
     * mask[i] = 0 for voxels with unknown status
     * mask[i] = k>1 for k-throws segmented set
     */

    //   protected short tollerance = 300;
    protected Stack<int[]> queue = null;
    protected Stack<int[]> newQueue = null;
    protected Stack<int[]> free = new Stack<int[]>();
    protected boolean inside;
    protected int[][] framePoints = null;
    protected int segIndex = 2;
    protected short[] ndist;
    
    private static final short[][] partNeighbDist
            = {{}, {3, 3}, {4, 3, 4, 3, 3, 4, 3, 4},
            {4, 4, 3, 4, 4,
                4, 3, 4, 3, 3, 4, 3, 4,
                4, 4, 3, 4, 4}};

    /**
     * Holds value of property backgroundThreshold.
     */
    /**
     * Creates a new instance of SegmentVolume
     * <p>
     * @param inField
     */
    public RangeSegmentVolume(RegularField inField, RegularField distField, RegularField outField)
    {
        dims = inField.getDims();
        off = inField.getPartNeighbOffsets();
        ndist = partNeighbDist[dims.length];
        ndata = (int) inField.getNNodes();

        this.inField = inField;
        outd = (int[])distField.getComponent(0).getRawArray().getData();
        mask = (byte[])outField.getComponent(0).getRawArray().getData();
    }

    @Override
    public void computeDistance(ArrayList<int[]> selectedPoints, boolean[] allowed)
    {
        if (allowed == null || selectedPoints.isEmpty())
            return;
        boolean isSomethingAllowed = false;
        for (int i = 0; i < allowed.length; i++)
            if (allowed[i]) {
                isSomethingAllowed = true;
                break;
            }
        if (!isSomethingAllowed)
            return;
        this.selectedPoints = selectedPoints;
        this.allowed = allowed;
        status = 0;
        new Thread(new ComputeSimilarity()).start();
    }

    private class ComputeSimilarity implements Runnable
    {

        @SuppressWarnings("unchecked")
        @Override
        public synchronized void run()
        {
            int cd, d;
            int[] p;
            int[] qSegment;
            int[] outQSeg = null;
            ProgressMonitor monitor = new ProgressMonitor(null, "Computing similarity field...", " ", 0, 100);
            monitor.setMillisToDecideToPopup(100);
            queue = new Stack<int[]>();
            newQueue = new Stack<int[]>();
            monitor.setMillisToPopup(100);
            for (int i = 0; i < ndata; i++)
                outd[i] = Short.MAX_VALUE;
            //set outd to 0 on boundary 
            extendMargins((short) 0);
            qSegment = new int[SEGLEN];
            //initializing queue to selected points and setting seed values at selected points        
            for (int i = 0; i < selectedPoints.size(); i++) {
                p = selectedPoints.get(i);
                int k = (dims[1] * p[2] + p[1]) * dims[0] + p[0];
                outd[k] = 0;
                if (i % SEGLEN == 0) {
                    qSegment = new int[SEGLEN];
                    queue.push(qSegment);
                    for (int j = 0; j < SEGLEN; j++)
                        qSegment[j] = -1;
                }
                qSegment[i % SEGLEN] = k;
            }
            while (!queue.empty()) {
                outQSeg = null;
                int ll = 0, nNew = 0;
                while (!queue.empty()) {
                    qSegment = queue.pop();
                    for (int k = 0; k < qSegment.length && qSegment[k] != -1; k++) {
                        int n = qSegment[k];
                        if (!allowed[mask[n]])
                            continue;
                        cd = outd[n];

                        for (int j = 0; j < 18; j++) {
                            int of = off[j];
                            if (!allowed[mask[n + of]] || (0xFF & data[n + of]) < low || (0xFF & data[n + of]) > up)
                                continue;
                            d = outd[n + of];
                            if (d < cd)
                                continue;
                            short l = (short) (cd + ndist[j]);
                            if (l < d) {
                                outd[n + of] = l;
                                if (lmax < l)
                                    lmax = l;
                                if (outQSeg == null || ll == SEGLEN) {
                                    if (free.empty()) {
                                        outQSeg = new int[SEGLEN];
                                    } else
                                        outQSeg = free.pop();
                                    newQueue.push(outQSeg);
                                    ll = 0;
                                }
                                outQSeg[ll] = n + of;
                                ll += 1;
                                nNew += 1;
                            }
                        }
                    }
                    free.push(qSegment);
                }
                //            System.out.println(""+nNew);
                if (outQSeg != null)
                    for (int i = ll; i < outQSeg.length; i++)
                        outQSeg[i] = -1;
                queue = newQueue;
                newQueue = new Stack<int[]>();
            }
            free.clear();
            status = SIMILARITY_COMPUTED;
            fireStateChanged();
        }
    }

    @Override
    public void setTollerance(short tollerance)
    {
    }

    @Override
    public void setIndices(ArrayList<Integer> indices)
    {
    }

    @Override
    public void setWeights(float[] weights)
    {
    }

    public void setComponent(int component)
    {
        data = (byte[])inField.getComponent(component).getRawArray().getData();
    }

}
