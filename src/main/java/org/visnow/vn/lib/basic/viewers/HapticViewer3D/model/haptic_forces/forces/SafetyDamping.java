/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Safety damping: F = - k * v^2, should be always enabled to reduce the risk of making harm to a
 * user. Tracker coordinate system is used to get velocity and compute force.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class SafetyDamping extends AbstractForce
{

    private static final Logger LOGGER
        = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    //
    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.TRACKER;
    //
    /**
     * damping constant, it's always negative
     */
    protected float dampingConstant;
    //
    /**
     * For new forces.
     */
    public static final float DEFAULT_DAMPING_CONSTANT = 15f;

    public SafetyDamping()
    {
        this(DEFAULT_DAMPING_CONSTANT);
    }

    protected SafetyDamping(float dampingConstant)
    {
        super(forceCoordinateSystem);

        setDampingConstant(dampingConstant);
    }

    /**
     * Computes the safety damping (F = -k * v^2) and stores it in
     * <code>outForce</code>.
     */
    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
    {

        Vector3f velocity = locationData.getCurrentTrackerVelocity();

        out_force.scale(dampingConstant * velocity.length(), velocity);
    }

    /**
     * Copy constructor. To be used only by
     * <code>clone()</code> method.
     * <p/>
     * @param force Object to be copied from
     */
    protected SafetyDamping(SafetyDamping force)
    {
        super(force);

        this.dampingConstant = force.dampingConstant;
    }

    @Override
    public IForce clone()
    {
        return new SafetyDamping(this);
    }

    @Override
    public String getName()
    {
        return getClassSimpleName() + " [k=" + dampingConstant + "]";
    }

    @Override
    public String getClassSimpleName()
    {
        return "Safety damping";
    }

    /**
     * Sets damping constant. It always sets the sign of the constant to minus.
     * <p/>
     * @param dampingConstant value to be set
     */
    public final void setDampingConstant(float dampingConstant)
    {
        this.dampingConstant = -abs(dampingConstant);
    }

    /**
     * Gets damping constant
     * <p/>
     * @return {@link #dampingConstant}
     */
    public float getDampingConstant()
    {
        return dampingConstant;
    }

    /**
     * Safety damping cannot be disabled. This method will do nothing.
     * <p/>
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled)
    {
        LOGGER.info("Safety damping cannot be disabled");
    }

    @Override
    public boolean canBeChanged()
    {
        return false;
    }
}
// revised.
