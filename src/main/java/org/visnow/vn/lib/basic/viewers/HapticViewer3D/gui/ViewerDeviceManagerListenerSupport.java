/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceManager;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceName;

/**
 * A helper class used by {@link ViewerDeviceManager} for managing listeners of class
 * {@link ViewerDeviceManager.IDeviceListener}.
 * <p/>
 * Apart from that this class when a new listener is registered, it sends back to it the initial
 * list of available devices.
 * <p/>
 * This class implements {@link DeviceManager.IDeviceManagerListener} interface. Thanks to
 * that, an object of this class can be registered as a listener of changes on the list of devices
 * in DeviceManager and instantly pass those changes to all objects that implement
 * {@link ViewerDeviceManager.IDeviceListener}.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ViewerDeviceManagerListenerSupport
    implements DeviceManager.IDeviceManagerListener
{

    @Override
    public void devicesInitialList(List<DeviceName> devicesRefs)
    {
        // do nothing
    }

    @Override
    public void deviceAdded(DeviceName deviceRef)
    {
        fireDeviceAdded(deviceRef);
    }

    @Override
    public void deviceRemoved(DeviceName deviceRef)
    {
        fireDeviceRemoved(deviceRef);
    }

    @Override
    public void deviceChangedState(DeviceName deviceRef)
    {
        fireDeviceChangedState(deviceRef);
    }

    /* PASSING EVENTS TO BUTTONS IN GUI */
    /**
     * List of listeners waiting for device changes.
     */
    private static ArrayList<ViewerDeviceManager.IDeviceListener> registeredDevicesListeners
        = new ArrayList<ViewerDeviceManager.IDeviceListener>();

    public ViewerDeviceManagerListenerSupport()
    {
    }

    /**
     * Adds
     * <code>listener</code> to the list of objects that are notified of changes in devices (new
     * device, removing device(?), changes in "used"/"not used" status).
     * This call immediately results in notifying the
     * <code>listener</code> of the list of already added devices.
     *
     * @param listener listener to be added
     */
    public synchronized void addRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener listener)
    {
        registeredDevicesListeners.add(listener);

        // sent to the newly added listener the list of already registered devices
        List<DeviceName> devicesNames = DeviceManager.getDevicesNamesList();
        listener.devicesInitialList(devicesNames);
    }

    /**
     * Removes
     * <code>listener</code> from the list of objects that are notified of changes in devices (new
     * device, removing device(?), changes in "used"/"not used" status).
     *
     * @param listener listener to be removed
     */
    public synchronized void removeRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener listener)
    {
        registeredDevicesListeners.remove(listener);
    }

    /**
     * Sends a list of registered devices to all listeners.
     */
    public synchronized void fireInitDevicesList()
    {
        List<DeviceName> devicesNames = DeviceManager.getDevicesNamesList();

        for (ViewerDeviceManager.IDeviceListener listener : registeredDevicesListeners) {
            listener.devicesInitialList(devicesNames);
        }
    }

    /**
     * Sends a newly added device to all listeners.
     */
    public synchronized void fireDeviceAdded(DeviceName deviceRef)
    {

        for (ViewerDeviceManager.IDeviceListener listener : registeredDevicesListeners) {
            listener.deviceAdded(deviceRef);
        }
    }

    /**
     * Sends a just removed device to all listeners.
     */
    public synchronized void fireDeviceRemoved(DeviceName deviceRef)
    {

        for (ViewerDeviceManager.IDeviceListener listener : registeredDevicesListeners) {
            listener.deviceRemoved(deviceRef);
        }
    }

    /**
     * Sends to all listeners a device that just has changed its state.
     */
    public synchronized void fireDeviceChangedState(DeviceName deviceRef)
    {

        for (ViewerDeviceManager.IDeviceListener listener : registeredDevicesListeners) {
            listener.deviceChangedState(deviceRef);
        }
    }
}
