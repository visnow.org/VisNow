/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider;

import java.awt.image.*;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SliceImage
{

    private RegularField inField = null;
    private DefaultColorMap1D cMap = ColorMapManager.getInstance().getColorMap1D(ColorMapManager.COLORMAP1D_GRAY);
    private int[] components = {0, 0, 0};
    private int[] weights = {100, 100, 100};
    private boolean multiselImage = false;
    private byte[] colorMapLUT = null;

    /**
     * Creates a new instance of SliceImage
     */
    public SliceImage(RegularField inField)
    {
        if (inField.getDims().length != 3) {
            return;
        }
        this.inField = inField;
    }

    public void setColorMap(DefaultColorMap1D cMap)
    {
        this.cMap = cMap;
    }

    public void setColorMap(int cMap)
    {
        this.cMap = ColorMapManager.getInstance().getColorMap1D(cMap);

    }

    public void selectComponent(int comp)
    {
        if (comp < 0 || comp >= inField.getNComponents()) {
            return;
        }
        components[0] = components[1] = components[2] = comp;
    }

    public void setMultiselImage(boolean multiselImage)
    {
        this.multiselImage = multiselImage;
    }

    public void selectComponents(int[] comp)
    {
        if (comp == null || comp.length != 3 ||
            comp[0] < 0 || comp[0] >= inField.getNComponents() ||
            comp[1] < 0 || comp[1] >= inField.getNComponents() ||
            comp[2] < 0 || comp[2] >= inField.getNComponents()) {
            return;
        }
        setComponents(comp);
    }

    public BufferedImage getSlice(int axis, int slice)
    {
        if (axis < 0 || axis > 2) {
            return null;
        }
        if (slice < 0 || slice >= inField.getDims()[axis]) {
            return null;
        }
        int nn0 = 0, nn1 = 1;
        if (axis == 0) {
            nn0 = 1;
            nn1 = 2;
        }
        if (axis == 1) {
            nn1 = 2;
        }
        int n0 = inField.getDims()[nn0];
        int n1 = inField.getDims()[nn1];
        int i, i0, i1;
        int[] rgba = new int[4];
        //        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        //        int[] nBits = {8, 8, 8, 8};
        //        ComponentColorModel colorModel = new ComponentColorModel(cs, nBits,
        //                true, true, Transparency.TRANSLUCENT, 0);
        //        WritableRaster raster =
        //                colorModel.createCompatibleWritableRaster(n0, n1);
        //        BufferedImage bImage =
        //                new BufferedImage(colorModel, raster, false, null);

        BufferedImage bImage = new BufferedImage(n0, n1, BufferedImage.TYPE_INT_ARGB);
        WritableRaster raster = bImage.getRaster();

        if (multiselImage) {
            int component = components[0];
            FloatLargeArray data = inField.getCurrent2DFloatSlice(component, axis, slice);
            cMap = ColorMapManager.getInstance().getColorMap1D(ColorMapManager.COLORMAP1D_RAINBOW);
            colorMapLUT = cMap.getRGBByteColorTable();
            float low = 1;
            float up = (float)inField.getComponent(component).getPreferredMaxValue();
            int colorMapSize = (colorMapLUT.length / 3);
            float s = (float) colorMapSize / (up - low);
            int c;
            for (i1 = 0; i1 < n1; i1++) {
                for (i0 = 0; i0 < n0; i0++) {
                    float d = data.getFloat(i1 * n0 + i0);
                    c = (int) ((d - low) * s);
                    if (c < 0) {
                        c = 0;
                    }
                    if (c >= colorMapSize) {
                        c = colorMapSize - 1;
                    }
                    for (int k = 0; k < 3; k++) {
                        rgba[k] = (int) (colorMapLUT[3 * c + k] & 0xff);
                    }
                    rgba[3] = 0x77;
                    if (d == 0) {
                        rgba[3] = 0;
                    }
                    raster.setPixel(i0, i1, rgba);
                }
            }
        } else if ((components[0] == components[1]) && (components[1] == components[2])) {
            colorMapLUT = cMap.getRGBByteColorTable();
            int component = components[0];
            float low = (float)inField.getComponent(component).getPreferredMinValue();
            float up = (float)inField.getComponent(component).getPreferredMaxValue();
            int colorMapSize = colorMapLUT.length - 1;
            float s = (float) colorMapSize / (up - low);
            int c;
            float d;
            FloatLargeArray data = inField.getCurrent2DFloatSlice(component, axis, slice);
            for (i1 = 0; i1 < n1; i1++) {
                for (i0 = 0; i0 < n0; i0++) {
                    d = data.getFloat(i1 * n0 + i0);
                    c = (int) ((d - low) * s);
                    if (c < 0) {
                        c = 0;
                    }
                    if (c > colorMapSize / 3) {
                        c = colorMapSize / 3;
                    }
                    for (int k = 0; k < 3; k++) {
                        rgba[k] = (int) (colorMapLUT[3 * c + k] & 0xff);
                    }
                    rgba[3] = 0xFF;
                    raster.setPixel(i0, i1, rgba);
                }
            }
        } else {
            FloatLargeArray[] data = new FloatLargeArray[3];
            float[] low = new float[3];
            float[] up = new float[3];
            for (int j = 0; j < components.length; j++) {
                int component = components[j];
                data[j] = inField.getCurrent2DFloatSlice(component, axis, slice);
                low[j] = (float)inField.getComponent(component).getPreferredMinValue();
                up[j] = (float)inField.getComponent(component).getPreferredMaxValue();
            }
            for (i1 = 0; i1 < n1; i1++) {
                for (i0 = 0; i0 < n0; i0++) {
                    for (int c = 0; c < 3; c++) {
                        rgba[c] = (int) (255 * weights[c] * (data[c].getFloat(i1 * n0 + i0) - low[c]) / (100. * (up[c] - low[c])));
                        if (rgba[c] > 255) {
                            rgba[c] = 255;
                        }
                    }
                    rgba[3] = 0xFF;
                    raster.setPixel(i0, i1, rgba);
                }
            }
        }
        return bImage;
    }

    public void setComponents(int[] components)
    {
        this.components = components;
    }

    public void setWeights(int[] weights)
    {
        this.weights = weights;
    }
}
