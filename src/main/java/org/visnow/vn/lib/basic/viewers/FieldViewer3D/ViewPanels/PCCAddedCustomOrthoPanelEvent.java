/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels;

import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParameter;

/**
 *
 * @author vis
 */
public class PCCAddedCustomOrthoPanelEvent extends ViewPanelEvent
{

    private float[][] points = null;
    private int[][] connections = null;
    private CalculableParameter calculable = null;

    public PCCAddedCustomOrthoPanelEvent(Object source, float[][] points, int[][] connections, CalculableParameter calculable)
    {
        super(source);
        if (points != null) {
            int n = points.length;
            this.points = new float[n][3];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < 3; j++) {
                    this.points[i][j] = points[i][j];
                }
            }
        }

        if (connections != null) {
            int n = connections.length;
            this.connections = new int[n][2];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < 2; j++) {
                    this.connections[i][j] = connections[i][j];
                }
            }
        }

        this.calculable = calculable;
    }

    /**
     * @return the points
     */
    public float[][] getPoints()
    {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(float[][] points)
    {
        this.points = points;
    }

    /**
     * @return the connections
     */
    public int[][] getConnections()
    {
        return connections;
    }

    /**
     * @param connections the connections to set
     */
    public void setConnections(int[][] connections)
    {
        this.connections = connections;
    }

    /**
     * @return the calculable
     */
    public CalculableParameter getCalculable()
    {
        return calculable;
    }

    /**
     * @param calculable the calculable to set
     */
    public void setCalculable(CalculableParameter calculable)
    {
        this.calculable = calculable;
    }

}
