/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class PointDescriptor
{

    private static int id = 0;
    protected String name = "";
    protected int[] indices;
    protected float[] worldCoords;
    protected float[] physicalCoords;
    private int membership = -1;

    public PointDescriptor(String s, int[] p, float[] c, int membership)
    {
        this.name = s;
        this.indices = p;
        this.worldCoords = c;
        this.membership = membership;
    }

    public PointDescriptor(String s, int[] p, float[] c)
    {
        this(s, p, c, -1);
    }

    public PointDescriptor(int n, int[] p, float[] c, int membership)
    {
        this("p" + n, p, c, membership);
    }

    public PointDescriptor(int n, int[] p, float[] c)
    {
        this("p" + n, p, c, -1);
    }

    public PointDescriptor(int[] p, float[] c, int membership)
    {
        this(nextPoint(), p, c, membership);
    }

    public PointDescriptor(int[] p, float[] c)
    {
        this(p, c, -1);
    }

    public int[] getIndices()
    {
        return indices;
    }

    public void setIndices(int[] indices)
    {
        this.indices = indices;
    }

    public float[] getWorldCoords()
    {
        return worldCoords;
    }

    public void setWorldCoords(float[] coords)
    {
        this.physicalCoords = this.worldCoords = coords;
    }

    public float[] getPhysicalCoords()
    {
        return physicalCoords;
    }

    public void setPhysicalCoords(float[] physicalCoords)
    {
        this.physicalCoords = physicalCoords;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static int nextPoint()
    {
        return id++;
    }

    public static void resetPointCounter()
    {
        id = 0;
    }

    public static void setPointCounter(int n)
    {
        id = n;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public boolean isDependant()
    {
        return false;
    }

    /**
     * @return the membership
     */
    public int getMembership()
    {
        return membership;
    }

    /**
     * @param membership the membership to set
     */
    public void setMembership(int membership)
    {
        this.membership = membership;
    }
}
