/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.CityPlot;

import org.visnow.jscic.RegularFieldSchema;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.graph3d.GenericGraph3DShared;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CityPlotShared extends GenericGraph3DShared
{

    public static final String X_COMPONENT_STRING =   "x box component";
    public static final String Y_COMPONENT_STRING =   "y box component";
    public static final String X_RANGE_STRING =       "x box range";
    public static final String Y_RANGE_STRING =       "y box range";

    public static final ParameterName<ComponentFeature> X_COMPONENT = new ParameterName<>(X_COMPONENT_STRING);
    public static final ParameterName<float[]> X_RANGE =              new ParameterName<>(X_RANGE_STRING);
    public static final ParameterName<ComponentFeature> Y_COMPONENT = new ParameterName<>(Y_COMPONENT_STRING);
    public static final ParameterName<float[]> Y_RANGE =              new ParameterName<>(Y_RANGE_STRING);

    public static final ParameterName<RegularFieldSchema> META_FIELD_SCHEMA = new ParameterName("META field schema");
}
