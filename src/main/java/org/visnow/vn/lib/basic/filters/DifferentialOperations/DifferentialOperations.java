/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.DifferentialOperations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.utils.field.LargeFieldDifferentialOperations.*;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class DifferentialOperations extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private Field inField = null;
    private GUI computeUI = null;

    private int runQueue = 0;

    /**
     * Creates a new instance of CreateGrid
     */
    public DifferentialOperations()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(META_SCALAR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_TIME_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(SCALAR_OPERATIONS, new ScalarOperation[][]{}),
            new Parameter<>(VECTOR_OPERATIONS, new VectorOperation[][]{}),
            new Parameter<>(TIME_OPERATIONS, new TimeOperation[][]{}),
            new Parameter<>(COMPUTE_FULLY, false),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }


    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        String[] newScalarComponentNames = new String[0];
        String[] newVectorComponentNames = new String[0];
        if (inField instanceof RegularField) {
            newScalarComponentNames = inField.getScalarComponentNames();
            newVectorComponentNames = inField.getVectorComponentNames();
        }
        int nTimeDependentComponents     = 0;
        if (inField.hasCoords() && inField.getCoords().getNSteps() > 1)
            nTimeDependentComponents += 1;
        for (DataArray da : inField.getComponents())
            if (da.isTimeDependant())
                nTimeDependentComponents += 1;
        String[] newTimeComponentNames = new String[nTimeDependentComponents];
        nTimeDependentComponents     = 0;
        if (inField.hasCoords() && inField.getCoords().getNSteps() > 1) {
            newTimeComponentNames[nTimeDependentComponents] = "coords";
            nTimeDependentComponents += 1;
        }
        for (DataArray da : inField.getComponents())
            if (da.isTimeDependant()) {
                newTimeComponentNames[nTimeDependentComponents] = da.getName();
                nTimeDependentComponents += 1;
            }
        ScalarOperation[][] newSO = new ScalarOperation[newScalarComponentNames.length][0];
        VectorOperation[][] newVO = new VectorOperation[newVectorComponentNames.length][0];
        TimeOperation[][]   newTO = new TimeOperation[nTimeDependentComponents][0];

        if (!resetParameters) { //try to restore vector/scalar operations (based on component names)
            List<String> scalarComponentNames = Arrays.asList(parameters.get(META_SCALAR_COMPONENT_NAMES));
            List<String> vectorComponentNames = Arrays.asList(parameters.get(META_VECTOR_COMPONENT_NAMES));
            List<String> timeComponentNames   = Arrays.asList(parameters.get(META_TIME_COMPONENT_NAMES));

            ScalarOperation[][] sO = parameters.get(SCALAR_OPERATIONS);
            VectorOperation[][] vO = parameters.get(VECTOR_OPERATIONS);
            TimeOperation[][]   tO = parameters.get(TIME_OPERATIONS);

            //if just only one component then use the same operations
            if (newScalarComponentNames.length == 1 && scalarComponentNames.size() == newScalarComponentNames.length)
                newSO = parameters.get(SCALAR_OPERATIONS);
            //otherwise restore by component name
            else {
                for (int i = 0; i < newScalarComponentNames.length; i++) {
                    String componentName = newScalarComponentNames[i];
                    int oldPosition = scalarComponentNames.indexOf(componentName);
                    if (oldPosition != -1) newSO[i] = sO[oldPosition];
                }
            }

            //if just only one component then use the same operations
            if (newVectorComponentNames.length == 1 && vectorComponentNames.size() == newVectorComponentNames.length)
                newVO = parameters.get(VECTOR_OPERATIONS);
            //otherwise restore by component names
            else {
                for (int i = 0; i < newVectorComponentNames.length; i++) {
                    String componentName = newVectorComponentNames[i];
                    int oldPosition = vectorComponentNames.indexOf(componentName);
                    if (oldPosition != -1) newVO[i] = vO[oldPosition];
                }
            }
            //if just only one component then use the same operations
            if (newTimeComponentNames.length == 1 && timeComponentNames.size() == newTimeComponentNames.length)
                newTO = parameters.get(TIME_OPERATIONS);
            //otherwise restore by component name
            else {
                for (int i = 0; i < newTimeComponentNames.length; i++) {
                    String componentName = newTimeComponentNames[i];
                    int oldPosition = timeComponentNames.indexOf(componentName);
                    if (oldPosition != -1) newTO[i] = tO[oldPosition];
                }
            }
        }

        parameters.set(VECTOR_OPERATIONS, newVO);
        parameters.set(SCALAR_OPERATIONS, newSO);
        parameters.set(TIME_OPERATIONS,   newTO);

        parameters.set(META_SCALAR_COMPONENT_NAMES, newScalarComponentNames);
        parameters.set(META_VECTOR_COMPONENT_NAMES, newVectorComponentNames);
        parameters.set(META_TIME_COMPONENT_NAMES,   newTimeComponentNames);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentType = (((inField == null) != (newField == null)) ||
                                      ((inField instanceof RegularField) != (newField instanceof RegularField)));
            boolean bothRegular = (inField != null) && (newField != null) &&
                                   inField instanceof RegularField && newField instanceof RegularField;

            boolean isDifferentDimNum = !isFromVNA() && (inField == null || isDifferentType ||
                    bothRegular && ((RegularField)inField).getDimNum() != ((RegularField)newField).getDimNum());
            boolean isNewField = !isFromVNA() && newField != inField;

            inField = newField;
            computeUI.fieldIsTimeDependent(inField.isTimeDependant());

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentDimNum);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentDimNum, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

                HashMap<String, ScalarOperation[]> scalarOps = new HashMap<>();
                HashMap<String, VectorOperation[]> vectorOps = new HashMap<>();
                HashMap<String, TimeOperation[]> timeOps = new HashMap<>();
                String[] tcn = p.get(META_TIME_COMPONENT_NAMES);
                for (int i = 0; i < tcn.length; i++) timeOps.put(tcn[i], p.get(TIME_OPERATIONS)[i]);

                if (inField instanceof RegularField){
                    String[] scn = p.get(META_SCALAR_COMPONENT_NAMES);
                    for (int i = 0; i < scn.length; i++)
                        scalarOps.put(scn[i], p.get(SCALAR_OPERATIONS)[i]);
                    String[] vcn = p.get(META_VECTOR_COMPONENT_NAMES);
                    for (int i = 0; i < vcn.length; i++)
                        vectorOps.put(vcn[i], p.get(VECTOR_OPERATIONS)[i]);
                }
                int totalSteps = 1; // 1 for prepare geometry :)
                for (ScalarOperation[] scalarOperations : scalarOps.values()) totalSteps += scalarOperations.length;
                for (VectorOperation[] vectorOperations : vectorOps.values()) totalSteps += vectorOperations.length;
                for (TimeOperation[]   timeOperations   : timeOps.values())   totalSteps += timeOperations.length;
                boolean compute_Fully = p.get(COMPUTE_FULLY);
                ProgressAgent progressAgent = getProgressAgent(totalSteps);

                outField = computeDifferentialOperators(inField, scalarOps, vectorOps, timeOps, compute_Fully,
                                                   progressAgent, VisNow.availableProcessors());
                if (outField != null && outField instanceof RegularField) {
                    setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                    setOutputValue("outIrregularField", null);
                    setOutputValue("outPointField", null);
                } else if (outField != null && outField instanceof IrregularField) {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
                    setOutputValue("outPointField", null);
                } else if (outField != null && outField instanceof PointField) {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", null);
                    setOutputValue("outPointField", new VNPointField((PointField) outField));
                } else {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", null);
                    setOutputValue("outPointField", null);
                }
                prepareOutputGeometry();
                show();
                progressAgent.increase();
            }
        }
    }
}
