/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.UCDWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class UCDWriter extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected Params params;
    protected IrregularField inField;

    public UCDWriter()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }
    
    /**
     * Reorders cell nodes.
     * 
     * @param type        cell type
     * @param nodes       cell nodes
     * @param orientation orientation
     */
    private static void reorder(CellType type, int[] nodes, byte orientation)
    {
        if (nodes.length == type.getNVertices() && orientation == 0) {
            int t;
            switch (type) {
                case TRIANGLE:
                    t = nodes[1];
                    nodes[1] = nodes[2];
                    nodes[2] = t;
                    break;
                case QUAD:
                    t = nodes[1];
                    nodes[1] = nodes[3];
                    nodes[3] = t;
                    break;
                case TETRA:
                    t = nodes[2];
                    nodes[2] = nodes[3];
                    nodes[3] = t;
                    break;
                case PYRAMID:
                    t = nodes[1];
                    nodes[1] = nodes[3];
                    nodes[3] = t;
                    break;
                case PRISM:
                    t = nodes[1];
                    nodes[1] = nodes[2];
                    nodes[2] = t;
                    t = nodes[4];
                    nodes[4] = nodes[5];
                    nodes[5] = t;
                    break;
                case HEXAHEDRON:
                    t = nodes[1];
                    nodes[1] = nodes[3];
                    nodes[3] = t;
                    t = nodes[7];
                    nodes[7] = nodes[5];
                    nodes[5] = t;
                    break;
                default:
            }
        }
    }

    public void update()
    {
        String outFileName = params.getFileName();
        if (outFileName != null && !outFileName.isEmpty()) {
            try (PrintWriter outFile = new PrintWriter(new FileOutputStream(outFileName))) {
                int nNodes = (int) inField.getNNodes();
                int nNodeData = inField.getNComponents();
                int nComponents = 0;
                int nCellComponents = 0;
                for (DataArray dta : inField.getComponents()) {
                    nComponents += dta.getVectorLength();
                }
                for (DataArraySchema dta : inField.getCellDataSchema().getComponentSchemas()) {
                    nCellComponents += dta.getVectorLength();
                }
                int nCellSets = inField.getNCellSets();
                int nCells = 0;
                for (int i = 0; i < nCellSets; i++) {
                    CellSet cs = inField.getCellSet(i);
                    for (int j = 0; j < Cell.getNProperCellTypes(); j++) {
                        CellArray ca = cs.getCellArray(CellType.getType(j));
                        if (ca != null) {
                            nCells += ca.getNCells();
                        }
                    }
                }
                DataContainerSchema globalCellDataSchema = inField.getCellDataSchema();
                
                outFile.println("# AVS unstructured field file");
                outFile.printf("%1d %6d %3d %3d 0 %n", nNodes, nCells, nComponents, nCellComponents);
                float[] coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
                if (inField.getTrueNSpace() == 2)
                    for (int i = 0; i < nNodes; i++) 
                        outFile.printf("%6d %12.5e %12.5e %n", 
                                i + 1, coords[2 * i], coords[2 * i + 1]);
                else
                    for (int i = 0; i < nNodes; i++) 
                        outFile.printf("%6d %12.5e %12.5e %12.5e %n", 
                                i + 1, coords[3 * i], coords[3 * i + 1], coords[3 * i + 2]);
                for (int iCellSet = 0, n = 1; iCellSet < inField.getNCellSets(); iCellSet++) {
                    for (int cellType = 0; cellType < Cell.getNProperCellTypes(); cellType++) {
                        int nv = CellType.getType(cellType).getNVertices();
                        CellArray ca = inField.getCellSet(iCellSet).getCellArray(CellType.getType(cellType));
                        if (ca == null) {
                            continue;
                        }
                        for (int cell = 0; cell < ca.getNCells(); cell++, n++) {
                            outFile.printf("%6d %3d %8s ", n, iCellSet, ca.getType().getUCDName());
                            int[] nds = ca.getNodes(cell);
//                            Cell.reorder(ca.getType(), nds, ca.getOrientations()[cell]);
                            if (ca.getType() == CellType.PYRAMID)
                                outFile.printf("%6d %6d %6d %6d %6d", nds[4] + 1, nds[0] + 1, nds[1] + 1, nds[2] + 1, nds[3] + 1);
                            else
                                for (int l = 0; l < nv; l++) 
                                    outFile.printf("%6d ", nds[l] + 1);
                            outFile.println();
                        }
                    }
                }
                DataArray[] das = new DataArray[nNodeData];
                int[] vlen = new int[nNodeData];
                float[][] data = new float[nNodeData][];
                outFile.printf("%1d ", nNodeData);
                for (int i = 0; i < nNodeData; i++) {
                    das[i] = inField.getComponent(i);
                    vlen[i] = das[i].getVectorLength();
                    data[i] = das[i].getRawFloatArray().getData();
                    outFile.printf("%2d ", das[i].getVectorLength());
                }
                outFile.println();
                for (int i = 0; i < nNodeData; i++) {
                    outFile.printf("%8s, %8s%n", das[i].getName(), das[i].getUnit());
                }
                for (int i = 0; i < nNodes; i++) {
                    outFile.printf("%6d ", i + 1);
                    for (int j = 0; j < nNodeData; j++) {
                        for (int k = 0; k < vlen[j]; k++) {
                            outFile.printf("%12.5e ", data[j][i * vlen[j] + k]);
                        }
                    }
                    outFile.println();
                }
                if (nCellComponents > 0) {
                    outFile.printf("%3d  ", nCellComponents); 
                    for (int i = 0; i < globalCellDataSchema.getNComponents(); i++) 
                        outFile.printf("%3d  ", globalCellDataSchema.getComponentSchema(i).getVectorLength());
                    outFile.println("");
                    for (int i = 0; i < globalCellDataSchema.getNComponents(); i++) 
                        outFile.println(globalCellDataSchema.getComponentSchema(i).getName() + ", " +
                                        globalCellDataSchema.getComponentSchema(i).getUnit());
                    for (int iCellSet = 0, n = 0; iCellSet < inField.getNCellSets(); iCellSet++) {
                        CellSet cs = inField.getCellSet(iCellSet);
                        for (int j = 0; j < Cell.getNProperCellTypes(); j++) {
                            CellArray ca = cs.getCellArray(CellType.getType(j));
                            if (ca == null) 
                                continue;
                            for (int k = 0; k < ca.getNCells(); k++, n++) {
                                outFile.printf("%6d  ", n + 1);
                                int ind = ca.getDataIndices(k);
                                for (int l = 0; l < globalCellDataSchema.getNComponents(); l++) {
                                    DataArray da = cs.getComponent(globalCellDataSchema.getComponentSchema(l).getName());
                                    float[] dt = da.getFloatElement(ind);
                                    for (int m = 0; m < dt.length; m++) 
                                        outFile.printf("%12.5e ", dt[m]);
                                }
                                outFile.println();
                            }
                        }
                    }
                }
                ui.setResultText(outFileName + " succesfully written");
            } catch (FileNotFoundException ex) {
                ui.setResultText(outFileName + " does not exist");
            }
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            return;
        }
        Field fld = ((VNField) getInputFirstValue("inField")).getField();
        if (fld instanceof IrregularField) {
            inField = (IrregularField) fld;
        } else {
            inField = null;
        }

        update();
    }
}
