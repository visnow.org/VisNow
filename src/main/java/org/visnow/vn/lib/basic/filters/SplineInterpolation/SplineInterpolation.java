/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SplineInterpolation;

import static org.apache.commons.math3.util.FastMath.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.LogicDataArray;

import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;

import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.basic.filters.SplineInterpolation.SplineInterpolationShared.Type;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.numeric.splines.SplineUtils;

import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.SplineInterpolation.SplineInterpolationShared.*;

/**
 * Spline interpolation.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 *
 * Revisions above 25 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class SplineInterpolation extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(SplineInterpolation.class);
    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
   
    protected RegularField inField = null;
   
    private int runQueue = 0;
    
    private GUI gui = new GUI();
    
    private SplineUtils spUtil = null;

    public SplineInterpolation()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                gui = new org.visnow.vn.lib.basic.filters.SplineInterpolation.GUI();
                gui.setParameters(parameters);
                ui.addComputeGUI(gui);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(TYPE, Type.DENSITY),
            new Parameter<>(DENSITY_TYPE_PARAMETER, 3),
            new Parameter<>(NEW_DIMS_TYPE_PARAMETER, new int[]{70, 70, 70}),
            new Parameter<>(NEW_CELL_SIZE_TYPE_PARAMETER, 0.025f),
            new Parameter<>(COMPONENTS, new int[]{0}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
                
            new Parameter<>(META_MAX_CELL_SIZE, 0.025f),
            new Parameter<>(META_INPUT_FIELD_DIMS, new int[]{50, 50, 50}),
            new Parameter<>(META_INPUT_FIELD_CELL_VOLUME, 1.0),
            new Parameter<>(META_INPUT_FIELD_AFFINE_NORM, new double[]{1.0, 1.0, 1.0}),
            new Parameter<>(META_INPUT_FIELD_IS_GEOMETRY_AFFINE, true),
            new Parameter<>(META_COMPONENTS_NAMES, new String[]{"component1", "component2", "component3"})
        };
    }
    
    private void validateParametersAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(TYPE, Type.DENSITY);
            parameters.set(DENSITY_TYPE_PARAMETER, 3);
            
            int[] inDims = inField.getDims();
            int[] outDims;
            switch (inDims.length) {
            case 3:
                outDims = new int[]{inDims[0] + 20, inDims[1] + 20, inDims[2] + 20};
                break;
            case 2:
                outDims = new int[]{inDims[0] + 20, inDims[1] + 20};
                break;
            default:
                outDims = new int[]{inDims[0] + 20};
            }
            parameters.set(NEW_DIMS_TYPE_PARAMETER, outDims);
            
            parameters.set(NEW_CELL_SIZE_TYPE_PARAMETER, 0.8f * (float) getArrayMin(inField.getAffineNorm(), inDims.length));
            parameters.set(COMPONENTS, new int[]{0});
        } else {
            // Get old parameters.
            int[] selectedComponentIndices = parameters.get(COMPONENTS);
            String[] componentNames = parameters.get(META_COMPONENTS_NAMES);
            
            // Get old selections.
            String[] selectedComponentNames = new String[selectedComponentIndices.length];
            for (int i = 0; i < selectedComponentIndices.length; ++i)
                selectedComponentNames[i] = componentNames[selectedComponentIndices[i]];
            
            // Search for components with the same names.
            ArrayList<Integer> newSelectedComponentsIndices = new ArrayList();            
            for (int i = 0; i < selectedComponentNames.length; ++i) {
                int idx = Arrays.asList(inField.getComponentNames()).indexOf(selectedComponentNames[i]);
                if (idx >= 0)
                    newSelectedComponentsIndices.add(idx);
            }
            // Check if resulting list is not empty. Select one component if so.
            if (newSelectedComponentsIndices.isEmpty())
                newSelectedComponentsIndices.add(0);
            
            parameters.set(COMPONENTS, convertIntegers(newSelectedComponentsIndices));
        }
        
        boolean isGeometryAffine = inField.getCurrentCoords() == null;

        if (isGeometryAffine) {
            parameters.set(META_INPUT_FIELD_CELL_VOLUME, inField.getCellVolume());
            parameters.set(META_INPUT_FIELD_AFFINE_NORM, Arrays.copyOf(inField.getAffineNorm(), inField.getAffineNorm().length));
        }
        parameters.set(META_INPUT_FIELD_IS_GEOMETRY_AFFINE, isGeometryAffine);
        parameters.set(META_COMPONENTS_NAMES, Arrays.copyOf(inField.getComponentNames(), inField.getComponentNames().length));
        parameters.set(META_INPUT_FIELD_DIMS, Arrays.copyOf(inField.getDims(), inField.getDims().length));
        parameters.set(META_MAX_CELL_SIZE, getMaxCellSize(inField.getPreferredExtents()));
        
        if (parameters.get(NEW_CELL_SIZE_TYPE_PARAMETER) > parameters.get(META_MAX_CELL_SIZE))
            parameters.set(NEW_CELL_SIZE_TYPE_PARAMETER, parameters.get(META_MAX_CELL_SIZE));
        
        parameters.setParameterActive(true);
    }

    private static double getArrayMin(double[] arr, int len)
    {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < Math.min(arr.length, len); ++i)
            if (arr[i] < min)
                min = arr[i];
        return min;
    }
    
    private static float getArrayMin(float[] arr, int len)
    {
        float min = Float.MAX_VALUE;
        for (int i = 0; i < Math.min(arr.length, len); ++i)
            if (arr[i] < min)
                min = arr[i];
        return min;
    }
    
    private static float getNarrowestExtent(float[][] extents)
    {
        float[] widths = new float[extents[0].length];
        for (int i = 0; i < extents[0].length; ++i) {
            widths[i] = extents[1][i] - extents[0][i];
            if (widths[i] <= 0.0f)
                widths[i] = Float.MAX_VALUE;
        }
        
        return getArrayMin(widths, widths.length);
    }

    private static float getMaxCellSize(float[][] extents)
    {
        return getNarrowestExtent(extents) / 3.0f;
    }
    
    
    private static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next();
        }
        return ret;
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        gui.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    // Should take "inField" explicitly and be "static". Left, as it is, for now.
    protected RegularField computeInterpolatedField(Parameters clonedParameters)
    {
        RegularField outputField = null;
        
        int[] dims = inField.getDims();
        int[] outDims = null;
        int[] interpolatedComponents = clonedParameters.get(COMPONENTS);
        
        int numThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        
        switch (clonedParameters.get(TYPE)) {
            case DENSITY:
                int density = clonedParameters.get(DENSITY_TYPE_PARAMETER);
                outDims = new int[dims.length];
                for (int i = 0; i < dims.length; i++)
                    outDims[i] = density * (dims[i] - 1) + 1;
                outputField = new RegularField(outDims);
                if (inField.getCurrentCoords() != null) {
                    spUtil = new SplineUtils(dims, 3, density, inField.getCurrentCoords(), numThreads);
                    spUtil.addFloatValueModificationListener(
                        new FloatValueModificationListener()
                        {
                            public void floatValueChanged(FloatValueModificationEvent e) {
                                setProgress(e.getVal());
                            }
                        });
                    outputField.setCurrentCoords(spUtil.splineInterpolate());
                } else {
                    float[][] inAffine = inField.getAffine();
                    float[][] outAffine = new float[4][3];
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            outAffine[i][j] = inAffine[i][j] / density;
                    for (int i = 0; i < 3; i++)
                        outAffine[3][i] = inAffine[3][i];
                    outputField.setAffine(outAffine);
                }
                for (int n = 0; n < interpolatedComponents.length; n++) {
                    DataArray da = inField.getComponent(interpolatedComponents[n]);
                    if (!da.isNumeric())
                        continue;
                    spUtil = new SplineUtils(dims, da.getVectorLength(), density, da.getRawFloatArray(), numThreads);
                    spUtil.addFloatValueModificationListener(
                        new FloatValueModificationListener()
                        {
                            public void floatValueChanged(FloatValueModificationEvent e)
                            {
                                setProgress(e.getVal());
                            }
                        });
                    if (da.getType() == DataArrayType.FIELD_DATA_LOGIC) {
                        outputField.addComponent(createLogicArray(spUtil.splineInterpolate(), da.getVectorLength(), da.getName(), da.getUnit(), da.getUserData()));
                    } else {
                        outputField.addComponent(DataArray.create(spUtil.splineInterpolate(), da.getVectorLength(), da.getName()));
                    }
                }
                break;
            case NEW_DIMS:
                outDims = clonedParameters.get(NEW_DIMS_TYPE_PARAMETER);
                
                int numDims = outDims.length;
                outputField = new RegularField(outDims);
                
                if (inField.getCurrentCoords() != null) {
                    spUtil = new SplineUtils(dims, 3, outDims, inField.getCurrentCoords(), numThreads);
                    spUtil.addFloatValueModificationListener(
                        new FloatValueModificationListener()
                        {

                            public void floatValueChanged(FloatValueModificationEvent e)
                            {
                                setProgress(e.getVal());
                            }
                        });
                    outputField.setCurrentCoords(spUtil.splineInterpolate());
                } else {
                    float[][] inAffine = inField.getAffine();
                    float[][] outAffine = new float[4][3];
                    for (int i = 0; i < numDims; i++) {
                        float d = (dims[i] - 1.f) / (outDims[i] - 1.f);
                        for (int j = 0; j < numDims; j++)
                            outAffine[i][j] = inAffine[i][j] * d;
                    }
                    System.arraycopy(inAffine[3], 0, outAffine[3], 0, numDims);
                    outputField.setAffine(outAffine);
                }
                for (int n = 0; n < interpolatedComponents.length; n++) {
                    DataArray da = inField.getComponent(interpolatedComponents[n]);
                    if (!da.isNumeric())
                        continue;
                    spUtil = new SplineUtils(dims, da.getVectorLength(), outDims, da.getRawFloatArray(), numThreads);
                    if (da.getType() == DataArrayType.FIELD_DATA_LOGIC) {
                        outputField.addComponent(createLogicArray(spUtil.splineInterpolate(), da.getVectorLength(), da.getName(), da.getUnit(), da.getUserData()));
                    } else {
                        outputField.addComponent(DataArray.create(spUtil.splineInterpolate(), da.getVectorLength(), da.getName()));
                    }
                }
                break;
            case NEW_CELL_SIZE:
                outDims = new int[dims.length];
                
                float newCellSize = clonedParameters.get(NEW_CELL_SIZE_TYPE_PARAMETER);
                
                float[][] inAffine = inField.getAffine();
                float[][] outAffine = new float[4][3];
                float d = inAffine[0][0] / newCellSize;
                
                outDims[0] = (int) ((dims[0] - 1) * d) + 1;
                //         outField = new RegularField(outDims);
                for (int j = 0; j < 3; j++)
                    outAffine[0][j] = inAffine[0][j] / d;
                if (dims.length > 1) {
                    d = inAffine[1][1] / newCellSize;
                    outDims[1] = (int) ((dims[1] - 1) * d) + 1;
                }
                //         outField = new RegularField(outDims);
                for (int j = 0; j < 3; j++)
                    outAffine[1][j] = inAffine[1][j] / d;
                if (dims.length > 2) {
                    d = inAffine[2][2] / newCellSize;
                    outDims[2] = (int) ((dims[2] - 1) * d) + 1;
                }
                outputField = new RegularField(outDims);
                for (int j = 0; j < 3; j++)
                    outAffine[2][j] = inAffine[2][j] / d;
                System.arraycopy(inAffine[3], 0, outAffine[3], 0, 3);
                outputField.setAffine(outAffine);
                for (int n = 0; n < interpolatedComponents.length; n++) {
                    DataArray da = inField.getComponent(interpolatedComponents[n]);
                    if (!da.isNumeric())
                        continue;
                    spUtil = new SplineUtils(dims, da.getVectorLength(), inAffine, newCellSize, da.getRawFloatArray(), numThreads);
                    spUtil.addFloatValueModificationListener(
                        new FloatValueModificationListener()
                        {
                            public void floatValueChanged(FloatValueModificationEvent e)
                            {
                                setProgress(e.getVal());
                            }
                        });
                    FloatLargeArray outFData = spUtil.splineInterpolate();
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            UnsignedByteLargeArray outBData = new UnsignedByteLargeArray(outFData.length());
                            for (long i = 0; i < outFData.length(); i++)
                                outBData.setByte(i, (byte)((int) (max(0, min(255, outFData.getFloat(i)))) & 0xff));
                            outputField.addComponent(DataArray.create(outBData, da.getVectorLength(), da.getName()).preferredRange(da.getPreferredMinValue(), da.getPreferredMaxValue()).
                                                            unit(da.getUnit()).userData(da.getUserData()));
                            break;
                        case FIELD_DATA_SHORT:
                            ShortLargeArray outSData = new ShortLargeArray(outFData.length());
                            for (long i = 0; i < outFData.length(); i++)
                                outSData.setShort(i, (short) outFData.getFloat(i));
                            outputField.addComponent(DataArray.create(outSData, da.getVectorLength(), da.getName()).preferredRange(da.getPreferredMinValue(), da.getPreferredMaxValue()).
                                                            unit(da.getUnit()).userData(da.getUserData()));
                            break;
                        case FIELD_DATA_INT:
                            IntLargeArray outIData = new IntLargeArray(outFData.length());
                            for (long i = 0; i < outFData.length(); i++)
                                outIData.setInt(i, (int) outFData.getFloat(i));
                            outputField.addComponent(DataArray.create(outIData, da.getVectorLength(), da.getName()).preferredRange(da.getPreferredMinValue(), da.getPreferredMaxValue()).
                                                            unit(da.getUnit()).userData(da.getUserData()));
                            break;
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                        case FIELD_DATA_COMPLEX:
                            outputField.addComponent(DataArray.create(outFData, da.getVectorLength(), da.getName()).preferredRange(da.getPreferredMinValue(), da.getPreferredMaxValue()).
                                                            unit(da.getUnit()).userData(da.getUserData()));
                            break;
                        case FIELD_DATA_LOGIC:
                            outputField.addComponent(createLogicArray(outFData, da.getVectorLength(), da.getName(), da.getUnit(), da.getUserData()));
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
        
        if (outputField != null)
            if (outputField.getNComponents() == 0)
                return null;
        
        return outputField;
    }

    private LogicDataArray createLogicArray(FloatLargeArray data, int veclen, String name, String units, String[] userData)
    {
        LogicLargeArray outLData = new LogicLargeArray(data.length());

        for (long i = 0; i < data.length(); i++)
            outLData.setByte(i, (byte)(data.get(i) < 0.5 ? 0 : 1));

        return new LogicDataArray(outLData, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LOGIC, outLData.length()/veclen, veclen, false));
    }
    
    @Override
    public void onActive()
    {
        LOGGER.debug("FromVNA: " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            // 1. Get new field.
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            // 1a. Set "Different Field" flag.
            boolean hasDifferentDims = (inField != null) ? !Arrays.equals(inField.getDims(), newInField.getDims()) : true;
            boolean isDifferentField = !isFromVNA() && (inField == null || hasDifferentDims);
            boolean isNewField = !isFromVNA() && newInField != inField;
            inField = newInField;

            // 2. Validate clonedParameters.             
            Parameters p;
            synchronized (parameters) {
                validateParametersAndSetSmart(isDifferentField);
                // 2b. Clone clonedParameters (local read-only copy).
                p = parameters.getReadOnlyClone();
            }
            // 3. Update GUI (GUI doesn't change clonedParameters! Assuming correct set of clonedParameters).
            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); // Can run (-> decreased) in "run dynamically" mode on input attach or new inField data.

                // 4. Run computation and propagate.
                outRegularField = computeInterpolatedField(p);
                if (outRegularField == null)
                    setOutputValue("outField", null);
                else
                    setOutputValue("outField", new VNRegularField(outRegularField));
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
