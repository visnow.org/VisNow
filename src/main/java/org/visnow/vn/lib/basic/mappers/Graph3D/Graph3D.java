/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Graph3D;

import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.graph3d.AbstractGraph3D;
import org.visnow.vn.lib.utils.graph3d.GenericGraph3DGUI;
import static org.visnow.vn.lib.utils.graph3d.GenericGraph3DShared.ADJUSTING;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Graph3D extends AbstractGraph3D
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    GenericGraph3DGUI computeUI;
    public Graph3D()
    {
        outObj.setName("graph3D");
        parameters.addParameterChangelistener((String name) -> {
            if (parameters.get(ADJUSTING)) {
                newOutFieldStructure = false;
                updateCoords();
                if (irregularFieldGeometry != null)
                    irregularFieldGeometry.updateCoords();
                if (regularFieldGeometry != null)
                    regularFieldGeometry.updateCoords();
            } else {
                newOutFieldStructure = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            genericGUI = computeUI = new GenericGraph3DGUI();
            computeUI.setParameters(parameters);
            ui.addComputeGUI(computeUI);
            setPanel(ui);
        });
    }

    @Override
    protected void updateCoords()
    {
        updateScale();
        for (int i = 0, l = 0; i < inField.getNNodes(); i++) {
            float v = scale * (vals[i] - base);
            for (int j = 0; j < 3; j++, l++)
                outCoords[l] = inCoords[l] + v * normals[l];
        }
        outField.setCurrentCoords(new FloatLargeArray(outCoords));
    }

    @Override
    protected void updateOutputData()
    {
        outField.getComponents().clear();
        for (DataArray dta : inField.getComponents())
            outField.addComponent(dta.cloneShallow());
    }

    @Override
    public void createOutField()
    {
        if (inField instanceof RegularField) {
            outField = outRegularField = ((RegularField)inField).cloneShallow();
            outIrregularField = null;
        }
        else {
            outField = outIrregularField = ((IrregularField)inField).cloneShallow();
            outRegularField = null;
        }
        createGraph3DData();
        outCoords = new float[3 * (int)inField.getNNodes()];
        updateCoords();
    }

    @Override
    protected void updateRenderingParams()
    {
        renderingParams.setShadingMode(RenderingParams.GOURAUD_SHADED);
    }

    @Override
    public void onActive()
    {
        super.onActive();
        if (outField == null) {
            setOutputValue("irregularGraphField", null);
            setOutputValue("regularGraphField", null);
            return;
        }
        if (inField instanceof RegularField) {
            setOutputValue("regularGraphField", new VNRegularField((RegularField)outField));
            setOutputValue("irregularGraphField", null);
        }
        else {
            setOutputValue("irregularGraphField", new VNIrregularField((IrregularField)outField));
            setOutputValue("regularGraphField", null);
        }
    }

}
