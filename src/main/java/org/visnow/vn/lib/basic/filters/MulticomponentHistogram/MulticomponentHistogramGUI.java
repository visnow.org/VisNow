/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import java.awt.BorderLayout;
import java.util.Arrays;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.lib.basic.filters.MulticomponentHistogram.MulticomponentHistogramShared.*;
import org.visnow.vn.system.swing.EnumCellEditor;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class MulticomponentHistogramGUI extends javax.swing.JPanel
{
    private ParameterProxy parameterProxy;
    //unfortunatelly this flag is necessary to distinguish user action on jTable from setter actions.
    private boolean tableActive = true;

    /**
     * Creates new form MulticomponentHistogramGUI
     */
    MulticomponentHistogramGUI()
    {
        initComponents();
        preprocessingPanel.setVisible(preprocessingHeader.isExpanded());
        updatePreprocessingEnableState();
        //FIXME: temporary solution - preprocessing panel hidden
        preprocessingHeader.setVisible(false);
        preprocessingPanel.setVisible(false);

        additionalBinOperationPanel.setVisible(additionalBinOperationHeader.isExpanded());
        histogramOptionsPanel.setVisible(histogramHeader.isExpanded());

        //set up operation table
        //note: make sure that column names in GUI match BinOperations enum order.
        operationTablePanel.add(operationsTable.getTableHeader(), BorderLayout.NORTH);

        operationsTable.getColumnModel().getColumn(0).setPreferredWidth(60);
        for (int i = 1; i < BinOperationType.values().length + 1; i++) operationsTable.getColumnModel().getColumn(i).setPreferredWidth(30);

        operationsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        for (int i = 1; i < BinOperationType.values().length + 1; i++)
            operationsTable.getColumnModel().getColumn(i).setCellEditor(new EnumCellEditor(BinOperationState.values(), true));

        ((DefaultTableModel) operationsTable.getModel()).addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (tableActive) {
                    DefaultTableModel operationsTableModel = (DefaultTableModel) operationsTable.getModel();
                    BinOperationState[][] states = new BinOperationState[operationsTableModel.getRowCount()][];

                    for (int i = 0; i < states.length; i++) {
                        states[i] = new BinOperationState[BinOperationType.values().length];
                        for (int j = 0; j < states[i].length; j++)
                            states[i][j] = (BinOperationState) operationsTableModel.getValueAt(i, j + 1);
                    }
                    parameterProxy.set(BIN_OPERATIONS, states);
                }
            }
        });
    }

    void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        //putting run button between logic and gui
        parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
    }

    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        //update dimmension radio buttons and spinners
        int[] dimLen = p.get(DIMENSION_LENGTHS);
        dim1DRB.setSelected(dimLen.length == 1);
        dim2DRB.setSelected(dimLen.length == 2);
        dim3DRB.setSelected(dimLen.length == 3);
        dim1LengthSpinner.setValue(dimLen[0]);
        if (dimLen.length >= 2) dim2LengthSpinner.setValue(dimLen[1]);
        if (dimLen.length >= 3) dim3LengthSpinner.setValue(dimLen[2]);
        updateSpinnerVisibility(dimLen.length);

        //update option check boxes
        roundLength32CB.setSelected(p.get(ROUND_BYTE_DIMS_TO_32));
        inheritExtentsCB.setSelected(p.get(INHERIT_EXTENTS));

        //update binning source radio buttons and combo boxes
        int[] binningSourceIndices = p.get(BINNING_SOURCES);
        boolean componentsAsIntervalSource = binningSourceIndices[0] >= 0;
        String[] sourceCoordinateNames = new String[]{"X coordinate", "Y coordinate", "Z coordinate"};
        Integer[] sourceCoordinateValues = new Integer[]{-1, -2, -3};
        String[] sourceComponentNames = p.get(META_SCALAR_COMPONENT_NAMES);
        sourceComponents1CB.setListData(sourceComponentNames);
        sourceComponents2CB.setListData(sourceComponentNames);
        sourceComponents3CB.setListData(sourceComponentNames);
        sourceCoordinates1CB.setListData(sourceCoordinateValues, sourceCoordinateNames);
        sourceCoordinates2CB.setListData(sourceCoordinateValues, sourceCoordinateNames);
        sourceCoordinates3CB.setListData(sourceCoordinateValues, sourceCoordinateNames);

        if (!componentsAsIntervalSource) for (int i = 0; i < binningSourceIndices.length; i++) binningSourceIndices[i] = -1 - binningSourceIndices[i];

        updateIntervalSourceCBs(componentsAsIntervalSource, dimLen.length, binningSourceIndices);
        intervalComponentsRB.setSelected(componentsAsIntervalSource);
        intervalCoordinatesRB.setSelected(!componentsAsIntervalSource);

        //update option check boxes and input field
        resetFirstBinCB.setSelected(p.get(RESET_FIRST_BIN));
        logScaleCB.setSelected(p.get(LOG_SCALE));
        logBaseNTF.setValue(p.get(LOG_SCALE_BASE));

        updateLogBaseNTFEnableState();

        //update advanced bin operation table
        DefaultTableModel operationsTableModel = (DefaultTableModel) operationsTable.getModel();

        tableActive = false;
        while (operationsTableModel.getRowCount() > 0) operationsTableModel.removeRow(0);

        BinOperationState[][] binOperationStates = p.get(BIN_OPERATIONS);
        String[] allComponentNames = p.get(META_ALL_COMPONENT_NAMES);
        for (int i = 0; i < allComponentNames.length; i++) {
            Object[] row = new Object[BinOperationType.values().length + 1];
            row[0] = allComponentNames[i];
            System.arraycopy(binOperationStates[i], 0, row, 1, row.length - 1);
            operationsTableModel.addRow(row);
        }

        tableActive = true;

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateSpinnerVisibility(int dimensionNumber)
    {
        length2_3Label.setVisible(dimensionNumber == 3);
        dim3LengthSpinner.setVisible(dimensionNumber == 3);
        length1_2Label.setVisible(dimensionNumber >= 2);
        dim2LengthSpinner.setVisible(dimensionNumber >= 2);
    }

    private void updateIntervalSourceCBs(boolean sourceComponents, int numberOfDimensions, int[] selectedIndexes)
    {
        sourceComponents1CB.setVisible(sourceComponents);
        sourceComponents2CB.setVisible(sourceComponents && numberOfDimensions >= 2);
        sourceComponents3CB.setVisible(sourceComponents && numberOfDimensions >= 3);
        sourceCoordinates1CB.setVisible(!sourceComponents);
        sourceCoordinates2CB.setVisible(!sourceComponents && numberOfDimensions >= 2);
        sourceCoordinates3CB.setVisible(!sourceComponents && numberOfDimensions >= 3);

        if (selectedIndexes != null)
            if (sourceComponents) {
                sourceComponents1CB.setSelectedIndex(selectedIndexes[0]);
                if (selectedIndexes.length >= 2) sourceComponents2CB.setSelectedIndex(selectedIndexes[1]);
                if (selectedIndexes.length == 3) sourceComponents3CB.setSelectedIndex(selectedIndexes[2]);
            } else {
                sourceCoordinates1CB.setSelectedIndex(selectedIndexes[0]);
                if (selectedIndexes.length >= 2) sourceCoordinates2CB.setSelectedIndex(selectedIndexes[1]);
                if (selectedIndexes.length == 3) sourceCoordinates3CB.setSelectedIndex(selectedIndexes[2]);
            }
    }

    private void updatePreprocessingEnableState()
    {
        preprocessingPanel.setEnabled(preprocessingHeader.isSelected());
        filteringLabel.setEnabled(preprocessingHeader.isSelected());
        filterScrollPane.setEnabled(preprocessingHeader.isSelected());
        filterTable.setEnabled(preprocessingHeader.isSelected());
        filterTable.getTableHeader().setEnabled(preprocessingHeader.isSelected());
        filtersPanel.setEnabled(preprocessingHeader.isSelected());
    }

    private void updateLogBaseNTFEnableState()
    {
        logBaseNTF.setEnabled(logScaleCB.isSelected());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        resolutionBG = new javax.swing.ButtonGroup();
        binningBG = new javax.swing.ButtonGroup();
        fixedGridBagLayoutPanel2 = new org.visnow.vn.system.swing.FixedGridBagLayoutPanel();
        preprocessingHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        preprocessingPanel = new javax.swing.JPanel();
        filteringLabel = new javax.swing.JLabel();
        filtersPanel = new javax.swing.JPanel();
        filterScrollPane = new javax.swing.JScrollPane();
        filterTable = new javax.swing.JTable();
        histogramHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        histogramOptionsPanel = new javax.swing.JPanel();
        dimensionPanel = new javax.swing.JPanel();
        dim1DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim2DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim3DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dimensionValuePanel = new javax.swing.JPanel();
        binLabel = new javax.swing.JLabel();
        dim1LengthSpinner = new javax.swing.JSpinner();
        length1_2Label = new javax.swing.JLabel();
        dim2LengthSpinner = new javax.swing.JSpinner();
        length2_3Label = new javax.swing.JLabel();
        dim3LengthSpinner = new javax.swing.JSpinner();
        roundLength32CB = new javax.swing.JCheckBox();
        inheritExtentsCB = new javax.swing.JCheckBox();
        binningPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        intervalComponentsRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        intervalCoordinatesRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        sourceComponents1CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        sourceComponents2CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        sourceComponents3CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        sourceCoordinates1CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        sourceCoordinates2CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        sourceCoordinates3CB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        resetFirstBinCB = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        logScaleCB = new javax.swing.JCheckBox();
        logBaseNTF = new org.visnow.vn.gui.components.NumericTextField();
        maskEmptyBinsBox = new javax.swing.JCheckBox();
        additionalBinOperationHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        additionalBinOperationPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        operationTablePanel = new javax.swing.JPanel();
        operationsTable = new javax.swing.JTable();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());

        preprocessingHeader.setExpanded(false);
        preprocessingHeader.setText("Preprocessing");
        preprocessingHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                preprocessingHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(preprocessingHeader, gridBagConstraints);

        preprocessingPanel.setLayout(new java.awt.GridBagLayout());

        filteringLabel.setText("Data filtering:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 0);
        preprocessingPanel.add(filteringLabel, gridBagConstraints);

        filtersPanel.setMinimumSize(new java.awt.Dimension(180, 200));
        filtersPanel.setPreferredSize(new java.awt.Dimension(180, 200));
        filtersPanel.setLayout(new java.awt.BorderLayout());

        filterTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null}
            },
            new String []
            {
                "", "Component", "Operator", "Value/Component"
            }
        ));
        filterTable.getTableHeader().setResizingAllowed(false);
        filterTable.getTableHeader().setReorderingAllowed(false);
        filterScrollPane.setViewportView(filterTable);

        filtersPanel.add(filterScrollPane, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 16, 8);
        preprocessingPanel.add(filtersPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(preprocessingPanel, gridBagConstraints);

        histogramHeader.setShowCheckBox(false);
        histogramHeader.setText("Histogram");
        histogramHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                histogramHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(histogramHeader, gridBagConstraints);

        histogramOptionsPanel.setLayout(new java.awt.GridBagLayout());

        dimensionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dimensions"));
        dimensionPanel.setLayout(new java.awt.GridBagLayout());

        resolutionBG.add(dim1DRB);
        dim1DRB.setText("1D (classic histogram)");
        dim1DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(dim1DRB, gridBagConstraints);

        resolutionBG.add(dim2DRB);
        dim2DRB.setSelected(true);
        dim2DRB.setText("2D (scatterplot-like)");
        dim2DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(dim2DRB, gridBagConstraints);

        resolutionBG.add(dim3DRB);
        dim3DRB.setText("3D (scatterplot-like)");
        dim3DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(dim3DRB, gridBagConstraints);

        dimensionValuePanel.setLayout(new java.awt.GridBagLayout());

        binLabel.setText("Bins:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        dimensionValuePanel.add(binLabel, gridBagConstraints);

        dim1LengthSpinner.setModel(new javax.swing.SpinnerNumberModel(2, 2, null, 1));
        dim1LengthSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                lengthSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dimensionValuePanel.add(dim1LengthSpinner, gridBagConstraints);

        length1_2Label.setText("x");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        dimensionValuePanel.add(length1_2Label, gridBagConstraints);

        dim2LengthSpinner.setModel(new javax.swing.SpinnerNumberModel(2, 2, null, 1));
        dim2LengthSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                lengthSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dimensionValuePanel.add(dim2LengthSpinner, gridBagConstraints);

        length2_3Label.setText("x");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        dimensionValuePanel.add(length2_3Label, gridBagConstraints);

        dim3LengthSpinner.setModel(new javax.swing.SpinnerNumberModel(2, 2, null, 1));
        dim3LengthSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                lengthSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dimensionValuePanel.add(dim3LengthSpinner, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(dimensionValuePanel, gridBagConstraints);

        roundLength32CB.setText("Round dims to 32 (byte only)");
        roundLength32CB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                roundLength32CBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(roundLength32CB, gridBagConstraints);

        inheritExtentsCB.setText("Inherit extents from data");
        inheritExtentsCB.setToolTipText("<html>If checked then geometric extents will reflect min..max of component data<br>\n(always on when coordinates are data source for intervals)");
        inheritExtentsCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                inheritExtentsCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        dimensionPanel.add(inheritExtentsCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        histogramOptionsPanel.add(dimensionPanel, gridBagConstraints);

        binningPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Bin options"));
        binningPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Data source for intervals:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 0);
        binningPanel.add(jLabel1, gridBagConstraints);

        binningBG.add(intervalComponentsRB);
        intervalComponentsRB.setSelected(true);
        intervalComponentsRB.setText("<html>Components<br> (classic: counting values)");
        intervalComponentsRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                intervalSourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        binningPanel.add(intervalComponentsRB, gridBagConstraints);

        binningBG.add(intervalCoordinatesRB);
        intervalCoordinatesRB.setText("<html>Coordinates<br>(spatial-density-like)");
        intervalCoordinatesRB.setToolTipText("Works best with point cloud data");
        intervalCoordinatesRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                intervalSourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        binningPanel.add(intervalCoordinatesRB, gridBagConstraints);

        sourceComponents1CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceComponents1CB, gridBagConstraints);

        sourceComponents2CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceComponents2CB, gridBagConstraints);

        sourceComponents3CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceComponents3CB, gridBagConstraints);

        sourceCoordinates1CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceCoordinates1CB, gridBagConstraints);

        sourceCoordinates2CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceCoordinates2CB, gridBagConstraints);

        sourceCoordinates3CB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                sourceChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 8, 2, 2);
        binningPanel.add(sourceCoordinates3CB, gridBagConstraints);

        resetFirstBinCB.setText("Reset first bin");
        resetFirstBinCB.setToolTipText("<html>Set to avoid near-zero-noise counting<br>\n(useful for e.g., medical data)\n");
        resetFirstBinCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetFirstBinCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        binningPanel.add(resetFirstBinCB, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        logScaleCB.setText("Log scale with constant:");
        logScaleCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                logScaleCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel2.add(logScaleCB, gridBagConstraints);

        logBaseNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        logBaseNTF.setMin(1e-30);
        logBaseNTF.setRevertIfOutOfBounds(true);
        logBaseNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                logBaseNTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(logBaseNTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        binningPanel.add(jPanel2, gridBagConstraints);

        maskEmptyBinsBox.setText("Mask out empty bins");
        maskEmptyBinsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                maskEmptyBinsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        binningPanel.add(maskEmptyBinsBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 16, 0);
        histogramOptionsPanel.add(binningPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(histogramOptionsPanel, gridBagConstraints);

        additionalBinOperationHeader.setExpanded(false);
        additionalBinOperationHeader.setShowCheckBox(false);
        additionalBinOperationHeader.setText("Additional bin operations");
        additionalBinOperationHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                additionalBinOperationHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(additionalBinOperationHeader, gridBagConstraints);

        additionalBinOperationPanel.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText("Create components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 0);
        additionalBinOperationPanel.add(jLabel5, gridBagConstraints);

        operationTablePanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, javax.swing.UIManager.getDefaults().getColor("ScrollBar.darkShadow")));
        operationTablePanel.setLayout(new java.awt.BorderLayout());

        operationsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Component", "Sum", "Min", "Max", "Avg", "Std", "VStd"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        operationsTable.setRowSelectionAllowed(false);
        operationsTable.getTableHeader().setReorderingAllowed(false);
        operationTablePanel.add(operationsTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 16, 8);
        additionalBinOperationPanel.add(operationTablePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(additionalBinOperationPanel, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private int[] getDimensionLengths()
    {
        int[] lengths = new int[]{(int) dim1LengthSpinner.getValue(), (int) dim2LengthSpinner.getValue(), (int) dim3LengthSpinner.getValue()};
        return Arrays.copyOf(lengths, dim3DRB.isSelected() ? 3 : dim2DRB.isSelected() ? 2 : 1);
    }

    private int[] getIntervalSources()
    {
        int[] sources;
        if (intervalComponentsRB.isSelected())
            sources = new int[]{sourceComponents1CB.getSelectedIndex(), sourceComponents2CB.getSelectedIndex(), sourceComponents3CB.getSelectedIndex()};
        else //-1, -2, -3 for spacial data
            sources = new int[]{(Integer) sourceCoordinates1CB.getSelectedItem(), (Integer) sourceCoordinates2CB.getSelectedItem(), (Integer) sourceCoordinates3CB.getSelectedItem()};

        return Arrays.copyOf(sources, dim3DRB.isSelected() ? 3 : dim2DRB.isSelected() ? 2 : 1);
    }

    private void dimRBChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_dimRBChangeAction
    {//GEN-HEADEREND:event_dimRBChangeAction
        int dimensionNumber = dim3DRB.isSelected() ? 3 : dim2DRB.isSelected() ? 2 : 1;
        updateSpinnerVisibility(dimensionNumber);
        updateIntervalSourceCBs(intervalComponentsRB.isSelected(), dimensionNumber, null);
        parameterProxy.set(DIMENSION_LENGTHS, getDimensionLengths(),
                           BINNING_SOURCES, getIntervalSources());
    }//GEN-LAST:event_dimRBChangeAction

    private void lengthSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_lengthSpinnerStateChanged
    {//GEN-HEADEREND:event_lengthSpinnerStateChanged
        parameterProxy.set(DIMENSION_LENGTHS, getDimensionLengths());
    }//GEN-LAST:event_lengthSpinnerStateChanged

    private void roundLength32CBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_roundLength32CBActionPerformed
    {//GEN-HEADEREND:event_roundLength32CBActionPerformed
        parameterProxy.set(ROUND_BYTE_DIMS_TO_32, roundLength32CB.isSelected());
    }//GEN-LAST:event_roundLength32CBActionPerformed

    private void inheritExtentsCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_inheritExtentsCBActionPerformed
    {//GEN-HEADEREND:event_inheritExtentsCBActionPerformed
        parameterProxy.set(INHERIT_EXTENTS, inheritExtentsCB.isSelected());
    }//GEN-LAST:event_inheritExtentsCBActionPerformed

    private void intervalSourceChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_intervalSourceChangeAction
    {//GEN-HEADEREND:event_intervalSourceChangeAction
        updateIntervalSourceCBs(intervalComponentsRB.isSelected(), dim3DRB.isSelected() ? 3 : dim2DRB.isSelected() ? 2 : 1, null);
        parameterProxy.set(BINNING_SOURCES, getIntervalSources());
    }//GEN-LAST:event_intervalSourceChangeAction

    private void resetFirstBinCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetFirstBinCBActionPerformed
    {//GEN-HEADEREND:event_resetFirstBinCBActionPerformed
        parameterProxy.set(RESET_FIRST_BIN, resetFirstBinCB.isSelected());
    }//GEN-LAST:event_resetFirstBinCBActionPerformed

    private void logScaleCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_logScaleCBActionPerformed
    {//GEN-HEADEREND:event_logScaleCBActionPerformed
        parameterProxy.set(LOG_SCALE, logScaleCB.isSelected());
        updateLogBaseNTFEnableState();
    }//GEN-LAST:event_logScaleCBActionPerformed

    private void logBaseNTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_logBaseNTFUserChangeAction
    {//GEN-HEADEREND:event_logBaseNTFUserChangeAction
        parameterProxy.set(LOG_SCALE_BASE, logBaseNTF.getValue().floatValue());
    }//GEN-LAST:event_logBaseNTFUserChangeAction

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void preprocessingHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_preprocessingHeaderUserChangeAction
    {//GEN-HEADEREND:event_preprocessingHeaderUserChangeAction
        preprocessingPanel.setVisible(preprocessingHeader.isExpanded());
        updatePreprocessingEnableState();
    }//GEN-LAST:event_preprocessingHeaderUserChangeAction

    private void additionalBinOperationHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_additionalBinOperationHeaderUserChangeAction
    {//GEN-HEADEREND:event_additionalBinOperationHeaderUserChangeAction
        additionalBinOperationPanel.setVisible(additionalBinOperationHeader.isExpanded());
    }//GEN-LAST:event_additionalBinOperationHeaderUserChangeAction

    private void histogramHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_histogramHeaderUserChangeAction
    {//GEN-HEADEREND:event_histogramHeaderUserChangeAction
        histogramOptionsPanel.setVisible(histogramHeader.isExpanded());
    }//GEN-LAST:event_histogramHeaderUserChangeAction

    private void sourceChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_sourceChangeAction
    {//GEN-HEADEREND:event_sourceChangeAction
        parameterProxy.set(BINNING_SOURCES, getIntervalSources());
    }//GEN-LAST:event_sourceChangeAction

    private void maskEmptyBinsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_maskEmptyBinsBoxActionPerformed
    {//GEN-HEADEREND:event_maskEmptyBinsBoxActionPerformed
        parameterProxy.set(MASK_OUT_EMPTY_BINS, maskEmptyBinsBox.isSelected());
    }//GEN-LAST:event_maskEmptyBinsBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.SectionHeader additionalBinOperationHeader;
    private javax.swing.JPanel additionalBinOperationPanel;
    private javax.swing.JLabel binLabel;
    private javax.swing.ButtonGroup binningBG;
    private javax.swing.JPanel binningPanel;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim1DRB;
    private javax.swing.JSpinner dim1LengthSpinner;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim2DRB;
    private javax.swing.JSpinner dim2LengthSpinner;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim3DRB;
    private javax.swing.JSpinner dim3LengthSpinner;
    private javax.swing.JPanel dimensionPanel;
    private javax.swing.JPanel dimensionValuePanel;
    private javax.swing.Box.Filler filler;
    private javax.swing.JScrollPane filterScrollPane;
    private javax.swing.JTable filterTable;
    private javax.swing.JLabel filteringLabel;
    private javax.swing.JPanel filtersPanel;
    private org.visnow.vn.system.swing.FixedGridBagLayoutPanel fixedGridBagLayoutPanel2;
    private org.visnow.vn.gui.widgets.SectionHeader histogramHeader;
    private javax.swing.JPanel histogramOptionsPanel;
    private javax.swing.JCheckBox inheritExtentsCB;
    private org.visnow.vn.gui.swingwrappers.RadioButton intervalComponentsRB;
    private org.visnow.vn.gui.swingwrappers.RadioButton intervalCoordinatesRB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel length1_2Label;
    private javax.swing.JLabel length2_3Label;
    private org.visnow.vn.gui.components.NumericTextField logBaseNTF;
    private javax.swing.JCheckBox logScaleCB;
    private javax.swing.JCheckBox maskEmptyBinsBox;
    private javax.swing.JPanel operationTablePanel;
    private javax.swing.JTable operationsTable;
    private org.visnow.vn.gui.widgets.SectionHeader preprocessingHeader;
    private javax.swing.JPanel preprocessingPanel;
    private javax.swing.JCheckBox resetFirstBinCB;
    private javax.swing.ButtonGroup resolutionBG;
    private javax.swing.JCheckBox roundLength32CB;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceComponents1CB;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceComponents2CB;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceComponents3CB;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceCoordinates1CB;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceCoordinates2CB;
    private org.visnow.vn.gui.swingwrappers.ComboBox sourceCoordinates3CB;
    // End of variables declaration//GEN-END:variables
}
