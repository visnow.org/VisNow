/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d;

import org.jogamp.java3d.Bounds;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickObject;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.IPassiveDevice;

/**
 * Should be implemented by all device pointers used in VisNow. Currenlty there is only one such
 * pointer: {@link Pointer3DViewBehavior}.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public interface IViewPointer
{

    public void pointerActivate(IPassiveDevice pointer);

    /* temporary debug functions */
    public boolean isPickModule3DActive();

    public boolean getLoosePointer();

    //TODO MEDIUM refactor: move PointerStateListener outside from Pointer3DViewBehavior!
    public void addPointerStateListener(Pointer3DViewBehavior.PointerStateListener listener);

    public void setSchedulingBounds(Bounds region);

    //TODO MEDIUM refactor: move PickModuleListener and PickPlainListener outside from PickObject - maybe here?
    public void setPickModuleListener(PickObject.PickModuleListener l);

    public void removePickModuleListener();

    public void setPickPlainListener(PickObject.PickPlaneListener l);

    public void removePickPlainListener();
}
