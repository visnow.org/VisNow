/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import static org.apache.commons.math3.util.FastMath.*;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ConnectionDescriptor
{

    private PointDescriptor p1 = null;
    private PointDescriptor p2 = null;
    private String name = "";

    public ConnectionDescriptor(String name, PointDescriptor p1, PointDescriptor p2)
    {
        this.name = name;
        this.p1 = p1;
        this.p2 = p2;
    }

    public float getLength()
    {
        if (p1 == null || p2 == null)
            return 0;

        float out = 0;
        float[] c1 = p1.getWorldCoords();
        float[] c2 = p2.getWorldCoords();
        float[] dc = new float[3];
        for (int i = 0; i < 3; i++) {
            dc[i] = c1[i] - c2[i];
            out += dc[i] * dc[i];
        }
        out = (float) sqrt(out);
        return out;
    }

    /**
     * @return the p1
     */
    public PointDescriptor getP1()
    {
        return p1;
    }

    public float[] getP1WorldCoordsDistanced(float scale)
    {
        float[] c1 = p1.getWorldCoords();
        float[] c2 = p2.getWorldCoords();
        float[] v = new float[3];
        for (int i = 0; i < 3; i++) {
            v[i] = scale * (c2[i] - c1[i]);
        }

        float[] out1 = new float[3];
        for (int i = 0; i < 3; i++) {
            out1[i] = c1[i] - v[i];
        }
        return out1;
    }

    public float[] getP2WorldCoordsDistanced(float scale)
    {
        float[] c1 = p1.getWorldCoords();
        float[] c2 = p2.getWorldCoords();
        float[] v = new float[3];
        for (int i = 0; i < 3; i++) {
            v[i] = scale * (c2[i] - c1[i]);
        }

        float[] out2 = new float[3];
        for (int i = 0; i < 3; i++) {
            out2[i] = c2[i] + v[i];
        }
        return out2;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(PointDescriptor p1)
    {
        this.p1 = p1;
    }

    /**
     * @return the p2
     */
    public PointDescriptor getP2()
    {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(PointDescriptor p2)
    {
        this.p2 = p2;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

}
