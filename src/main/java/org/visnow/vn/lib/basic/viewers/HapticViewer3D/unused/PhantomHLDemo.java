/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import openhaptics.OH;
import openhaptics.hdapi.Device;
import openhaptics.hdapi.HDException;
import openhaptics.hlapi.Context;
import openhaptics.hlapi.HLException;
import openhaptics.hlapi.effects.Viscous;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class PhantomHLDemo
{

    static {
        OH.init();
    }

    public static void main(String[] args)
    {
        try {
            //            int hHD = Device.initDevice(Device.DEFAULT_PHANTOM);
            int hHD = Device.initDevice("DEFAULT_PHANTOM");
            Context context = Context.createContext(hHD);

            /*
             CustomEffect ce = new CustomEffect(context, new CustomEffectCallback() {

             public void computeForce(Vector3d force, Cache cache) {
             force.set(2,0,0);
             }

             public void startForceProc(Cache cache) {
             }

             public void stopForceProc(Cache cache) {
             }
             });
             *
             */
            Viscous ce = new Viscous(context, 1.0, 1.0);

            ce.start();

            System.out.println("Press any thing to stop force");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            ce.stop();

            System.out.println("Press any thing to exit");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            context.delete();
            Device.disableDevice(hHD);
        } catch (HLException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HDException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PhantomHLDemo()
    {
    }
}
