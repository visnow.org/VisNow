/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.haptics.forces_panels;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.InputFields;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Damping;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.DampingField;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Gravity;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Spring;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.VectorFieldForce;

/**
 * Abstract base class for all panels in which parameters of a force can be set. It is intended to
 * be used as a dialog box.
 * <p/>
 * It provides a
 * factory creating a panel for a given force and convenient way to handle Esc and Enter keys in the
 * panel.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public abstract class AbstractEditPanel extends JPanel
{

    /**
     * Returns the force edited in this panel.
     */
    public abstract IForce getForce();

    @Override
    public void setVisible(boolean aFlag)
    {
        getTopLevelAncestor().setVisible(aFlag);
    }

    /**
     * Factory for creating a panel for a proper type of force.
     * <p/>
     * @param f      force object to be edited or set
     * @param fields data input fields (used only in VectorFieldForce, DampingField and
     *               HapticIsosurface)
     * <p>
     * @return panel for editing / setting force parameters
     */
    public static AbstractEditPanel createPanel(IForce f,
                                                InputFields fields)
    {

        AbstractEditPanel editPanel;

        IForce force = f.clone();
        if (force instanceof Damping) {
            editPanel = new EditDampingPanel((Damping) force);
        } else if (force instanceof Gravity) {
            editPanel = new EditGravityPanel((Gravity) force);
        } else if (force instanceof Spring) {
            editPanel = new EditSpringPanel((Spring) force);
        } else if (force instanceof VectorFieldForce) {
            editPanel = new EditVectorFieldPanel((VectorFieldForce) force, fields);
        } else if (force instanceof DampingField) {
            editPanel = new EditDampingFieldPanel((DampingField) force, fields);
        } else {
            throw new UnsupportedOperationException(); // should never happen
        }
        return editPanel;
    }

    public AbstractEditPanel()
    {
        this(true);
    }

    public AbstractEditPanel(boolean addKeyListeners)
    {
        if (addKeyListeners) {
            addKeyListeners();
        }
    }

    // ====  handling Enter and Escape keys ====
    /**
     * Method with undescore sign is needed as a hack for Netbeans bug which prevents developer
     * from changing visibility of an event handler from private to protected.
     */
    abstract protected void _okButtonActionPerformed();

    /**
     * Method with undescore sign is needed as a hack for Netbeans bug which prevents developer
     * from changing visibility of an event handler from private to protected.
     */
    abstract protected void _cancelButtonActionPerformed();

    //
    final static String CANCEL_ACTION = "cancel-action";
    final static String ENTER_ACTION = "enter-action";

    /**
     * Escape as an equivalent for Cancel and Enter - OK.
     */
    final protected void addKeyListeners()
    {

        InputMap im = this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = this.getActionMap();

        // escape - the same as cancelButtonActionPerformed
        im.put(KeyStroke.getKeyStroke("ESCAPE"), CANCEL_ACTION);
        am.put(CANCEL_ACTION, new CancelAction());

        // enter - the same as okButtonActionPerformed
        im.put(KeyStroke.getKeyStroke("ENTER"), ENTER_ACTION);
        am.put(ENTER_ACTION, new EnterAction());

    }

    class CancelAction extends AbstractAction
    {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent ev)
        {
            _cancelButtonActionPerformed();
        }
    }

    class EnterAction extends AbstractAction
    {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent ev)
        {
            _okButtonActionPerformed();
        }
    }

    class NegativeScaleException extends Exception
    {

        public NegativeScaleException()
        {
        }

        public NegativeScaleException(String message)
        {
            super(message);
        }

        public NegativeScaleException(String message, Throwable cause)
        {
            super(message, cause);
        }

        public NegativeScaleException(Throwable cause)
        {
            super(cause);
        }
    }
}
///revised.
