/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isolines;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jogamp.java3d.LineAttributes;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.RegularFieldNeighbors;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2D;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.mappers.Isolines.IsolinesShared.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.array.ComponentValuesArray;
import org.visnow.vn.lib.gui.cropUI.CropUIShared;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import static org.visnow.vn.lib.utils.field.MergeTimesteps.mergedTimeSteps;
import org.visnow.vn.lib.utils.field.SliceRegularField;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;
import org.visnow.vn.lib.utils.isolines.RegularFieldIsolines;
import org.visnow.vn.lib.utils.isolines.IrregularFieldIsolines;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Isolines extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected VNField input = null;
    protected RegularFieldIsolines regularFieldIsolines = null;
    protected IrregularFieldIsolines irregularFieldIsolines = null;
    protected IsolinesGUI computeUI = null;
    protected Field inField = null;
    protected Isolines2D out2D;
    protected int component = -1;
    protected int[] size;

    public Isolines()
    {
        parameters.addParameterChangelistener((String name) -> {
            if (inField == null || !parameters.isParameterActive())
                return;
            startAction();
        });
        parameters.get(THRESHOLDS).addChangeListener((e) -> {
            if (inField == null || !parameters.isParameterActive())
                return;
            float[] oldRange = parameters.get(META_TIME_RANGE);
            DataArray da = inField.getComponent(parameters.get(THRESHOLDS).getComponentName());
            if (da == null)
                return;
            float[] newRange = new float[] {da.getStartTime(), da.getEndTime()};
            if (newRange[0] != oldRange[0] || newRange[1] != oldRange[1]) {
                parameters.set(META_TIME_RANGE, newRange);
                if (computeUI != null) {
                    if(newRange[0] == newRange[1])
                        computeUI.updateTimeSlider(new float[]{newRange[0]}, parameters.get(TIME), true);
                    else
                        computeUI.updateTimeSlider(newRange, parameters.get(TIME), true);
                }                    
            }
            startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new IsolinesGUI();
            computeUI.setParameterProxy(parameters);
            ui.addComputeGUI(computeUI);
            setPanel(ui);
        });
        outObj.setName("isolines");
    }

    @Override
    public void onDelete()
    {
        detach();
        ui = null;
        regularFieldIsolines = null;
        out2D = null;
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        List<Parameter> defaultParameters = createDefaultParametersAsList();
        defaultParameters.addAll(CropUIShared.createDefaultParametersAsList());
        return defaultParameters.toArray(new Parameter[defaultParameters.size()]);
    }

    public class Isolines2D extends GeometryObject2D implements Cloneable
    {
        private ArrayList<float[]>[] data = null;
        private float[][] coords = null;
        private Color[] colors = null;
        private float lineWidth = 1;
        private int lineStyle = LineAttributes.PATTERN_SOLID;

        public Isolines2D(String name)
        {
            super(name);
        }

        public Isolines2D(String name, ArrayList<float[]>[] data, int[] dims)
        {
            super(name);
            if (regularFieldIsolines != null) {
                setData(data, dims);
            }
        }

        public Isolines2D(String name, float[][] coords)
        {
            super(name);
            if (regularFieldIsolines == null) {
                return;
            }
            this.coords = coords;
        }

        public void setColors(Color[] colors)
        {
            this.colors = colors;
        }

        public void setLineWidth(float lineWidth)
        {
            this.lineWidth = lineWidth;
        }

        private void setLineStyle(int lineStyle)
        {
            this.lineStyle = lineStyle;
        }

        public final void setData(ArrayList<float[]>[] data, int[] dims)
        {
            this.data = data;
            this.width = dims[0];
            this.height = dims[1];
        }

        public int getDataSize()
        {
            if (data != null)
                return this.data.length;
            return 0;
        }

        @Override
        public void drawLocal2D(Graphics2D g, AffineTransform at)
        {
            if (regularFieldIsolines == null || colors == null) {
                return;
            }

            switch (lineStyle) {
                case LineAttributes.PATTERN_DASH:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        10.0f, 5.0f
                    }, 0.0f));
                    break;
                case LineAttributes.PATTERN_DASH_DOT:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        10.0f, 5.0f, lineWidth, 5.0f
                    }, 0.0f));
                    break;
                case LineAttributes.PATTERN_DOT:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        lineWidth, 5.0f
                    }, 0.0f));
                    break;
                default:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f));
                    break;
            }

            g.translate(at.getTranslateX(), at.getTranslateY());
            if (data == null) {
                if (coords == null) {
                    g.translate(-at.getTranslateX(), -at.getTranslateY());
                    return;
                }
                for (int i = 0; i < coords.length; i++) {
                    g.setColor(colors[i]);
                    for (int j = 0; j < coords[i].length; j += 4) {
                        g.drawLine((int) ((coords[i][j] + 0.5) * at.getScaleX()), (int) ((height - coords[i][j + 1] - 0.5) * at.getScaleY()),
                                   (int) ((coords[i][j + 2] + 0.5) * at.getScaleX()), (int) ((height - coords[i][j + 3] - 0.5) * at.getScaleY()));
                    }
                }
                g.translate(-at.getTranslateX(), -at.getTranslateY());
                return;
            }

            for (int n = 0; n < data.length; n++) {
                g.setColor(colors[n]);
                ArrayList<float[]> vdata = data[n];
                for (int j = 0; j < vdata.size(); j++) {
                    float[] line = vdata.get(j);
                    int m = line.length / 2;
                    int[] ix = new int[m];
                    int[] iy = new int[m];
                    for (int i = 0; i < m; i++) {
                        ix[i] = (int) ((line[2 * i] + 0.5) * at.getScaleX());
                        iy[i] = (int) ((height - line[2 * i + 1] - 0.5) * at.getScaleY());
                    }
                    g.drawPolyline(ix, iy, m);
                }
            }
            g.translate(-at.getTranslateX(), -at.getTranslateY());
        }
    }

    private void setRange(float a, float b, float[] q, int[] l, float min, float scale, float weight)
    {
        int low, up;
        a = scale * (a - min);
        b = scale * (b - min);
        if (b < a) {
            float t = a;
            a = b;
            b = t;
        }
        low =  Math.max(0, (int)(a + .5));
        up  =  Math.min((int)(b + 1.5), q.length);
        if (low == up)
            return;
        float t = 1/(b - a);
        for (int i = low; i < up; i++) {
            q[i] += weight * t;
            l[i] += weight;
        }
    }

    public DataArray thrQuality()
    {
        RegularField regularInField = (RegularField)inField;
        ComponentValuesArray thrs = parameters.get(THRESHOLDS);
        DataArray isoData = inField.getComponent(thrs.getComponentName());
        int THR_QUALITY_PRECISION = 1024;
        float[] quality = new float[THR_QUALITY_PRECISION];
        int[] length = new int[THR_QUALITY_PRECISION];
        int[] dims = regularInField.getDims();
        float[] vals = isoData.getVectorNorms().getData();
        float[] thresholds = thrs.getValues();
        if (thresholds == null || outIrregularField == null || outIrregularField.getNNodes() < 1)
            return null;
        float[] thrQuality = new float[(int)outIrregularField.getNNodes()];
        float low = thresholds[0], up = thresholds[0];
        for (int i = 0; i < thresholds.length; i++) {
            if (low > thresholds[i]) low = thresholds[i];
            if (up  < thresholds[i]) up  = thresholds[i];
        }
        if (low == up) {
            low = (float)isoData.getMinValue();
            up  = (float)isoData.getMaxValue();
        }
        if (low == up)
            return null;
        Arrays.fill(quality, 0);
        Arrays.fill(length, 0);
        float d = (THR_QUALITY_PRECISION - 1) / (up - low);

        int[] nbhrs = RegularFieldNeighbors.symmetricNeighbors(dims)[0];
        float[] weights = RegularFieldNeighbors.SYMMETRIC_WEIGHTS2D[0];
        for (int i = 1; i < dims[1] - 1; i++)
            for (int j = 1, k = i * dims[0]; j < dims[0] - 1; j++, k++)
                for (int l = 0; l < nbhrs.length; l++)
                    setRange(vals[k], vals[k + nbhrs[l]], quality, length, low, d, weights[l]);

        for (int i = 0; i < quality.length; i++)
            if (length[i] > 0 && quality[i] > 0)
                quality[i] = -(float)(Math.log(quality[i] / length[i]));
             else
                quality[i] = 0;
        float[] isoVals = outIrregularField.getComponent(0).getVectorNorms().getFloatData();
        for (int i = 0; i < isoVals.length; i++)
            thrQuality[i] = quality[(int)(.5f + d * (isoVals[i] - low))];
        return DataArray.create(thrQuality, 1, "relative_threshold_quality");
    }

    public void update()
    {
        outIrregularField = null;
        outObj.clearAllGeometry();
        outGroup = null;
        ComponentValuesArray thrs = parameters.get(THRESHOLDS);
        if (inField != null && inField.getComponent(thrs.getComponentName()) != null) {
            String cName = thrs.getComponentName();
            DataArray da = inField.getComponent(cName);
            boolean onBoundary = parameters.get(BOUNDARY_ISOLINES);
            float[] thresholds = thrs.getValues();
            float time = parameters.get(TIME);
            if (da.getPreferredMaxValue() > da.getPreferredMinValue() + 1e-6f) {
                if (inField instanceof RegularField) {
                    if (((RegularField)inField).getDimNum() == 2)
                        outIrregularField = RegularFieldIsolines.create((RegularField)inField, da, time, thresholds);
                    else if (onBoundary) {
                        RegularField in = (RegularField)inField;
                        int[] dims = in.getDims();
                        RegularField slice =              SliceRegularField.sliceField(in, 0, 0, false);
                        outIrregularField =               RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds);
                        slice =                           SliceRegularField.sliceField(in, 0, dims[0] - 1, false);
                        outIrregularField =
                                MergeIrregularField.merge(outIrregularField,
                                                          RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds),
                                                          1, false);
                        slice =                           SliceRegularField.sliceField(in, 1, 0, false);
                        outIrregularField =
                                MergeIrregularField.merge(outIrregularField,
                                                          RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds),
                                                          2, false);
                        slice =                           SliceRegularField.sliceField(in, 1, dims[1] - 1, false);
                        outIrregularField =
                                MergeIrregularField.merge(outIrregularField,
                                                          RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds),
                                                          3, false);
                        slice =                           SliceRegularField.sliceField(in, 2, 0, false);
                        outIrregularField =
                                MergeIrregularField.merge(outIrregularField,
                                                          RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds),
                                                          4, false);
                        slice =                           SliceRegularField.sliceField(in, 2, dims[2] - 1, false);
                        outIrregularField =
                                MergeIrregularField.merge(outIrregularField,
                                                          RegularFieldIsolines.create(slice, slice.getComponent(cName), time, thresholds),
                                                          5, false);
                    }
                    else
                        outIrregularField = null;
                }
                else
                    outIrregularField = IrregularFieldIsolines.create((IrregularField)inField,
                                                                       da, thresholds,
                                                                       parameters.get(TIME), parameters.get(SELECTED_CELL_SETS), onBoundary);
            }
            if (outIrregularField != null && outIrregularField.getNNodes() > 2) {
                if (parameters.get(COMPUTE_UNCERTAINTY) && inField instanceof RegularField && ((RegularField)inField).getDimNum() == 2)
                    outIrregularField.addComponent(thrQuality());
                outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
                setOutputValue("outField", new VNIrregularField(outIrregularField));
                outField = outIrregularField;
                outField.setName(inField.getName());
                prepareOutputGeometry();
                irregularFieldGeometry.getCellSetGeometry(0).getDataMappingParams().removeRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getDataMappingParams().addRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getParams().getRenderingParams().removeRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getParams().getRenderingParams().addRenderEventListener(renderEventListener);
                irregularFieldGeometry.getColormapLegend(0).setThrTable(parameters.get(THRESHOLDS).getPhysicalValues(), da.getName());
                GeometryObject2DStruct isolinesStruct = new GeometryObject2DStruct(out2D);
                irregularFieldGeometry.getGeometryObj2DStruct().removeAllChildren();
                irregularFieldGeometry.getGeometryObj2DStruct().addChild(isolinesStruct);
                show();
            }
            else {
                setOutputValue("outField", null);
            }

        }
    }

    private RenderEventListener renderEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
//            updateDisplay2DParams();
        }
    };

    private void validateParamsAndSetSmart(boolean resetFully, boolean differentRegularDims)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE)
            parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_REGULAR_DIMS, inField instanceof RegularField ?
                                          ((RegularField)inField).getDimNum() : -1);
        parameters.get(THRESHOLDS).setContainerSchema(inField.getSchema());
        float[] mergedTimeSteps =
                mergedTimeSteps(inField, new String[] {"coords",
                                                       "mask",
                                                       parameters.get(THRESHOLDS).getComponentName()});
        parameters.set(META_TIME_RANGE, new float[]{mergedTimeSteps[0],
                                                    mergedTimeSteps[mergedTimeSteps.length -1]});
        parameters.set(TIME, inField.getCurrentTime());
        //validate/smart thresholds

        boolean isRegular = inField instanceof RegularField;

        if (isRegular) {
            //validate/smart crop
            int[] previousDims = parameters.get(CropUIShared.META_FIELD_DIMENSION_LENGTHS);
            int[] dims = ((RegularField) inField).getDims();
            if (dims.length == 2) {
                parameters.set(CropUIShared.META_FIELD_DIMENSION_LENGTHS, dims);
                if (resetFully)
                    CropUIShared.resetLowHighRange(parameters);
                else if (differentRegularDims) {
                    CropUIShared.rescaleAll(parameters, previousDims);
                }

                //validate/smart downsize
                if (resetFully || differentRegularDims) {
                    int[] low = parameters.get(CropUIShared.LOW);
                    int[] high = parameters.get(CropUIShared.HIGH);
                    int[] croppedDims = new int[low.length];
                    for (int i = 0; i < croppedDims.length; i++)
                        croppedDims[i] = high[i] - low[i] + 1;
                }
            }
        }
        if (inField instanceof IrregularField) {
            IrregularField irrIn = (IrregularField) inField;
            String[] cellSetNames = new String[irrIn.getNCellSets()];
            boolean[] selCellSets = new boolean[irrIn.getNCellSets()];
            int[] indices = new int[irrIn.getNCellSets()];
            for (int i = 0; i < cellSetNames.length; i++) {
                cellSetNames[i] = irrIn.getCellSet(i).getName();
                indices[i] = i;
                selCellSets[i] = true;
            }
            parameters.set(META_CELL_SET_NAMES, cellSetNames);
            parameters.set(SELECTED_CELL_SETS, selCellSets);
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            //1a. set distinction flags
            boolean isNewField = !isFromVNA() && newInField != inField;
            if (isNewField) {
                boolean isNonCompatibleField = !isFromVNA() &&
                        (inField == null ||
                        inField instanceof IrregularField && newInField instanceof RegularField ||
                        inField instanceof RegularField && newInField instanceof IrregularField);
                boolean isNewIrregularField = !isFromVNA() &&
                        inField instanceof IrregularField && newInField instanceof IrregularField && newInField != inField;
                boolean isDifferentRegularDims = !isFromVNA() &&
                        inField != null && inField instanceof RegularField && newInField instanceof RegularField &&
                        !Arrays.toString(((RegularField) inField).getDims()).equals(Arrays.toString(((RegularField) newInField).getDims()));
                inField = newInField;
                Parameters p;
                synchronized (parameters) {
                    validateParamsAndSetSmart(isNonCompatibleField || isNewIrregularField, isDifferentRegularDims);
                    //2b. clone param (local read-only copy)
                    p = parameters.getReadOnlyClone();
                }
                notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);
            }

                update();
        }
        if (getInputFirstValue("inField") == null ||
                ((VNField) getInputFirstValue("inField")).getField() == null) {
            inField = null;
            outField = null;
            return;
        }
    }

}
