/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FilePartSchema;
import java.util.Vector;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TimestepSchema extends FilePartSchema
{

    protected Vector<FilePartSchema> sections = new Vector<FilePartSchema>();
    protected float time = 0;
    protected float dt = 1;
    protected int repeat = 1;

    public int getRepeat()
    {
        return repeat;
    }

    public void setRepeat(int repeat)
    {
        this.repeat = repeat;
    }

    public float getTime()
    {
        return time;
    }

    public void setTime(float time)
    {
        this.time = time;
    }

    public float getDt()
    {
        return dt;
    }

    public void setDt(float dt)
    {
        this.dt = dt;
    }

    public Vector<FilePartSchema> getSections()
    {
        return sections;
    }

    public void setSections(Vector<FilePartSchema> sections)
    {
        this.sections = sections;
    }

    public int getNSections()
    {
        return sections.size();
    }

    public FilePartSchema getSection(int i)
    {
        if (i < 0 || i >= sections.size())
            return null;
        return sections.get(i);
    }

    public void addSection(FilePartSchema s)
    {
        sections.add(s);
    }

    @Override
    public String toString()
    {
        if (repeat < 0)
            return "indefinitely repeatable timestep";
        else if (repeat > 1)
            return "timestep " + repeat + " times";
        else
            return "timestep " + time;
    }

    public String[] getDescription(boolean t)
    {
        String[] desc = new String[sections.size() + 1];
        desc[0] = toString();
        for (int i = 0; i < sections.size(); i++)
            desc[i + 1] = sections.get(i).toString(t);
        return desc;
    }

    @Override
    public String toString(boolean t)
    {
        StringBuilder b = new StringBuilder();
        String[] desc = getDescription(t);
        for (String desc1 : desc)
            b.append(desc1).append("  ");
        return b.toString();
    }
}
