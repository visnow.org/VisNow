/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ExtendedVolumeSegmentation;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickEvent;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickListener;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.VolumeRenderer;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider.*;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.FieldDisplay3DInternalFrame;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GUI;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryUI;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.Glypher;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels.OrthosliceNumberChangedEvent;
import org.visnow.vn.lib.types.VNRegularField;
import static org.visnow.jscic.utils.CropDownUtils.*;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.denoising.AbstractAnisotropicWeightedAverageCompute;
import org.visnow.vn.lib.utils.denoising.AnisotropicWeightedAverageCompute;
import org.visnow.vn.lib.utils.denoising.AnisotropicDenoisingParams;
import static org.visnow.vn.lib.utils.denoising.AnisotropicDenoisingParams.*;
import org.visnow.vn.lib.utils.field.FieldSmoothDown;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class VolumeSegmentation extends ModuleCore
{

    public static final int FREE = 0;
    public static final int BACKGROUND = 1;
    protected FieldDisplay3DInternalFrame display3DFrame = null;
    protected VolumeRenderer volRender = new VolumeRenderer(false);
    protected GUI ui = null;
    protected DataProvider dp = new DataProvider();
    protected GeometryParams gparams = new GeometryParams();
    protected CalculableParams cparams = new CalculableParams();
    protected GeometryUI geomUI = null;
    protected Glypher glypher = new Glypher();
    protected VolumeSegmentationUI segUI = new VolumeSegmentationUI();
    protected SegmentVolume segmentVolume;
    protected RegularField inField = null;
    protected RegularField distField = null;
    protected RegularField outField = null;
    protected int[] dims = null;
    protected byte[] outData = null;
    protected int[] distData = null;
    protected boolean[] segData = null;
    protected int[] usedComponents = null;
    protected float[] weights = null;
    protected int segIndex = 0;
    protected int maxSegNumber = 1;
    protected boolean outputReady = false;
    protected ChangeListener segmentVolumeChangeListener = null;
    protected int tollerance = 100;
    protected float low = -1;
    protected float up = 256;

    /**
     * Creates a new instance of TestViewer3D
     */
    public VolumeSegmentation()
    {
        this(true);
    }

    public VolumeSegmentation(boolean expert)
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();
                display3DFrame = new FieldDisplay3DInternalFrame();
                geomUI = new GeometryUI(display3DFrame);
            }
        });
        display3DFrame.setLocation(100, 100);
        display3DFrame.setDataProvider(dp);
        geomUI.addChangeListener(display3DFrame.getManager());

        display3DFrame.setVisible(true);
        display3DFrame.getDisplay3DPanel().addChild(volRender.getOutObject());

        glypher.setParams(gparams);
        geomUI.setParams(gparams, cparams);
        display3DFrame.setGeometryParams(gparams);
        display3DFrame.getDisplay3DPanel().addChild(glypher.getGlyphsObject());
        glypher.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                display3DFrame.getDisplay3DPanel().repaint();
            }
        });

        volRender.addPick3DListener(new FieldPickListener()
        {
            @Override
            public void handlePick3D(FieldPickEvent e)
            {
                gparams.addPoint(e.getIndices());
            }
        });

        ui.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                display3DFrame.setVisible(true);
            }
        });
        setPanel(ui);

        display3DFrame.addUIPage(segUI, "Segmentation");
        if (expert) {
            display3DFrame.addUI(dp.getUI(), "Slices");
            display3DFrame.addUI(volRender.getVolRenderUI(), "3D View");
        }
        display3DFrame.setTitle("Segmentation");
        segUI.setParentModule(this);
        dp.getParams().addDataProviderParamsListener(new DataProviderParamsListener()
        {
            @Override
            public void onOrthosliceNumberChanged(OrthosliceNumberChangedEvent evt)
            {
            }

            @Override
            public void onOverlayChanged(DataProviderParamsEvent evt)
            {
                segUI.getBackgroundChangeListener().stateChanged(evt);
            }

            @Override
            public void onOverlayOpacityChanged(DataProviderParamsEvent evt)
            {
            }

            @Override
            public void onRgbComponentChanged(RgbComponentChangedEvent evt)
            {
            }

            @Override
            public void onRgbComponentWeightChanged(RgbComponentWeightChangedEvent evt)
            {
            }

            @Override
            public void onCustomPlaneChanged(CustomPlaneChangedEvent evt)
            {
            }

            @Override
            public void onIsolineThresholdChanged(IsolineThresholdChangedEvent evt)
            {
            }

            @Override
            public void onCustomOrthoPlaneChanged(CustomOrthoPlaneChangedEvent evt)
            {
            }

            @Override
            public void onColormapChanged(ColormapChangedEvent evt)
            {
            }
        });

    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    @Override
    public void onDelete()
    {
        display3DFrame.dispose();
        volRender = null;
        display3DFrame = null;
        dp = null;
    }

    private void prepareWorkObjects()
    {
        distField = new RegularField(dims);
        if (inField.getCurrentCoords() != null)
            distField.setCurrentCoords(inField.getCurrentCoords());
        else
            distField.setAffine(inField.getAffine());
        distData = new int[dims[0] * dims[1] * dims[2]];
        distField.addComponent(DataArray.create(distData, 1, "distances"));

        segUI.setInfield(inField);

        dp.setInField(inField);
        dp.setAuxField(distField);

        dp.resetCustomPlane();
        dp.updateOrthosliceImages();
        dp.updateCustomPlane();
        dp.centerSlices();
        volRender.setInField(inField);
        gparams.setInField(inField);

        outField = new RegularField(dims);
        if (inField.getCurrentCoords() != null)
            outField.setCurrentCoords(inField.getCurrentCoords());
        else
            outField.setAffine(inField.getAffine());
        outData = new byte[dims[0] * dims[1] * dims[2]];
        outField.addComponent(DataArray.create(outData, 1, "segmented data"));

        dp.setOverlayField(outField);

        segData = new boolean[dims[0] * dims[1] * dims[2]];
    }

    public FieldDisplay3DInternalFrame getDisplay3DFrame()
    {
        return display3DFrame;
    }

    public RegularField getInField()
    {
        return inField;
    }

    public void setInField(RegularField in)
    {
        if (in == null) {
            inField = in;
            return;
        }

        dims = in.getDims();
        if (dims.length != 3 || dims[0] < 2 || dims[1] < 2 || dims[2] < 2 || in.getNComponents() < 1) {
            return;
        }

        if (segUI.presmooth() || segUI.presmoothSlices()) {

            // "Presmooth by slices" parameter was deleted, since it does not
            // work in "AnisotropicDenoiser" module, nor corresponding util.
            int radiusPresmooth = 4;
            int radius = 1;
            float slope = 2.0f;
            float slope1 = 4.0f;
            int[][] components = new int[][]{{0, 0}}; // Default value. 

            RegularField anisotropyField = FieldSmoothDown.smoothDownToFloat(in, 1, (float) radiusPresmooth, VisNow.availableProcessors());

            AbstractAnisotropicWeightedAverageCompute averageCompute = new AnisotropicWeightedAverageCompute();

            AnisotropicDenoisingParams currentParams = new AnisotropicDenoisingParams(
                AVERAGE,
                radius,
                slope,
                slope1,
                components
            );

            inField = averageCompute.compute(in, anisotropyField, currentParams);
            inField.getComponent(0).setPreferredRanges(in.getComponent(0).getPreferredMinValue(), in.getComponent(0).getPreferredMaxValue(), in.getComponent(0).getPreferredPhysMinValue(), in.getComponent(0).getPreferredPhysMaxValue());
        } else {
            inField = in;
        }
        reset();
    }

    @Override
    public void onActive()
    {
        if (!outputReady) {
            if (getInputFirstValue("inField") == null || ((VNRegularField) getInputFirstValue("inField")).getField() == null) {
                dp.setInField(null);
                dp.setAuxField(null);
                volRender.setInField(null);
                gparams.setInField(null);
                setInField(null);
                outField = null;
                return;
            }
            if (inField == ((VNRegularField) getInputFirstValue("inField")).getField()) {
                return;
            }
            setInField(((VNRegularField) getInputFirstValue("inField")).getField());
        } else {
            if (outField != null) {
                ArrayList<String> names = segUI.getAllowedNames();
                String[] dataMap = new String[names.size() + 1];
                dataMap[0] = "MAP";
                for (int i = 0; i < dataMap.length - 1; i++) {
                    dataMap[i + 1] = "" + i + ":" + names.get(i);
                }
                outField.getComponent(0).setUserData(dataMap);
                extendMargins((byte[]) outField.getComponent(0).getRawArray().getData());

                outField.setAffine(inField.getAffine());
                byte[] inData = (byte[]) inField.getComponent(0).getRawArray().getData();
                byte[] visData = new byte[outData.length];
                int d = 220 / dataMap.length;
                for (int i = 0; i < visData.length; i++) {
                    visData[i] = (byte) (0xff & ((0xff & inData[i]) / 8 + 64 + d * (0xff & outData[i])));
                }
                outField.addComponent(DataArray.create(visData, 1, "vis data"));
                setOutputValue("outField", new VNRegularField(outField));
            }
            outputReady = false;
        }
    }

    private void extendMargins(byte[] data)
    {
        // along x axis
        for (int i = 0; i < dims[2]; i++) {
            for (int j = 0; j < dims[1]; j++) {
                int k = (i * dims[1] + j) * dims[0];
                data[k] = data[k + 1];
                data[k + dims[0] - 1] = data[k + dims[0] - 2];
            }
        }
        // along y axis
        for (int i = 0; i < dims[2]; i++) {
            for (int j = 0; j < dims[0]; j++) {
                int k = i * dims[1] * dims[0] + j;
                data[k] = data[k + dims[0]];
                data[k + dims[0] * (dims[1] - 1)] = data[k + dims[0] * (dims[1] - 2)];
            }
        }
        // along z axis
        for (int i = 0; i < dims[1]; i++) {
            for (int j = 0; j < dims[0]; j++) {
                int k = i * dims[0] + j;
                data[k] = data[k + dims[0] * dims[1]];
                data[k + dims[0] * dims[1] * (dims[2] - 1)] = data[k + dims[0] * dims[1] * (dims[2] - 2)];
            }
        }
    }

    public void renderVolume(int vol)
    {
        switch (vol) {
            case 0:
                if (inField != null)
                    volRender.setInField(inField);
                break;
            case 1:
                if (distField != null)
                    volRender.setInField(distField);
                break;
            case 2:
                if (outField != null)
                    volRender.setInField(outField);
                break;
            default:
                if (inField != null)
                    volRender.setInField(inField);
        }
    }

    public void setRange(float low, float up)
    {
        this.low = low;
        this.up = up;
        dp.getParams().setIsolineThresholds(new float[]{low, up});
    }

    public void clearLastPoint()
    {
        if (gparams != null) {
            gparams.clearLastPoint();
        }
    }

    public void resetSelection()
    {
        if (inField == null || segmentVolume == null) {
            return;
        }
        if (gparams != null) {
            gparams.clearPoints();
        }
    }

    public void addSelection()
    {
        updateOutField(dp.getParams().getIsolineThreshold());
        for (int i = 0; i < outData.length; i++) {
            if (segData[i]) {
                outData[i] = (byte) segIndex;
            }
        }
        outField.getComponent(0).setPreferredRange(0, maxSegNumber - 1);
        if (gparams != null) {
            gparams.clearPoints();
        }
        dp.updateOrthosliceOverlays();
        dp.updateCustomOrthoPlanesOverlays();
    }

    public void subtractSelection()
    {
        updateOutField(dp.getParams().getIsolineThreshold());
        for (int i = 0; i < outData.length; i++) {
            if (segData[i] && outData[i] == segIndex) {
                outData[i] = 0;
            }
        }
        outField.getComponent(0).setPreferredRange(0, maxSegNumber);
        if (gparams != null) {
            gparams.clearPoints();
        }
        dp.updateOrthosliceOverlays();
        dp.updateCustomOrthoPlanesOverlays();
    }

    public int[] getVolumes()
    {
        int[] volumes = new int[256];
        for (int i = 0; i < volumes.length; i++) {
            volumes[i] = 0;
        }
        for (int i = 0; i < outData.length; i++) {
            volumes[outData[i] & 0xff] += 1;
        }
        return volumes;

    }

    public void reset()
    {
        if (inField == null) {
            return;
        }

        maxSegNumber = 1;

        if (outData != null)
            for (int i = 0; i < outData.length; i++) {
                outData[i] = 0;
            }

        if (outField != null) {
            outField.getComponent(0).setPreferredRange(0, maxSegNumber);
        }

        if (gparams != null) {
            gparams.clearPoints();
        }

        dp.resetIsolines();
        dp.resetOverlays();
        prepareWorkObjects();
    }

    public void setBackgroundThresholdRange(float low, float up, boolean invert)
    {
        if (inField == null)
            return;
        float[] data = inField.getComponent(0).getRawFloatArray().getData();
        if (invert)
            for (int i = 0; i < data.length; i++) {
                if (data[i] <= low || data[i] >= up)
                    outData[i] = BACKGROUND;
                else if (outData[i] == BACKGROUND)
                    outData[i] = FREE;
            }
        else
            for (int i = 0; i < data.length; i++) {
                if (data[i] > low && data[i] < up)
                    outData[i] = BACKGROUND;
                else if (outData[i] == BACKGROUND)
                    outData[i] = FREE;
            }
        outField.getComponent(0).setPreferredRange(0, maxSegNumber);

        if (dp.getParams().isSimpleOverlay())
            dp.getParams().setSimpleOverlay(false);
        dp.updateOrthosliceOverlays();
        dp.updateCustomOrthoPlanesOverlays();
    }

    public void setBackgroundThresholdRange(float low, float up)
    {
        setBackgroundThresholdRange(low, up, false);
    }

    public void setThreshold(int threshold)
    {
        if (inField == null || segmentVolume == null)
            return;
        float t = (threshold * tollerance) / 100.f;
        dp.getParams().setIsolineThreshold(t);
    }

    private void updateOutField(float threshold)
    {
        int[] coeff
            = {
                1, 2, 1, 2, 4, 2, 1, 2, 1,
                2, 4, 2, 4, 8, 4, 2, 4, 2,
                1, 2, 1, 2, 4, 2, 1, 2, 1
            };
        int ndata = dims[0] * dims[1] * dims[2];
        int m, l;
        byte[] tData = new byte[ndata];
        m = dims[0] * dims[1];
        l = dims[0];
        int[] offsets = new int[]{
            -m - l - 1, -m - l, -m - l + 1,
            -m - 1, -m, -m + 1,
            -m + l - 1, -m + l, -m + l + 1,
            -l - 1, -l, -l + 1,
            -1, 0, 1,
            l - 1, l, l + 1,
            m - l - 1, m - l, m - l + 1,
            m - 1, m, m + 1,
            m + l - 1, m + l, m + l + 1
        };
        //threshold
        for (int i = 0; i < ndata; i++) {
            if (distData[i] < threshold) {
                tData[i] = 1;
            } else {
                tData[i] = 0;
            }
            segData[i] = false;
        }

        //smoothing out contour
        for (int i2 = 1; i2 < dims[2] - 1; i2++) {
            for (int i1 = 1; i1 < dims[1] - 1; i1++) {
                for (int i0 = 1, i = (dims[1] * i2 + i1) * dims[0] + 1; i0 < dims[0] - 1; i0++, i++) {
                    l = 0;
                    for (int j = 0; j < 27; j++) {
                        l += coeff[j] * tData[i + offsets[j]];
                    }
                    if (l > 32) {
                        segData[i] = true;
                    }
                }
            }
        }
    }

    void cropField()
    {
        int[] low = volRender.getVolRenderUI().getCropUI().getLow();
        int[] up = volRender.getVolRenderUI().getCropUI().getUp();
        int[] down = new int[dims.length];
        int[] outDims = new int[dims.length];
        for (int i = 0; i < outDims.length; i++) {
            if (low[i] < 0 || up[i] > dims[i]) {
                return;
            }
            outDims[i] = up[i] - low[i];
            down[i] = 1;
        }
        RegularField tmpField = new RegularField(outDims);
        if (inField.getCurrentCoords() != null) {
            tmpField.setCurrentCoords((FloatLargeArray)cropDownArray(inField.getCurrentCoords(), 3, dims, low, up, down));
        } else {
            float[][] outAffine = new float[4][3];
            float[][] affine = inField.getAffine();
            System.arraycopy(affine[3], 0, outAffine[3], 0, 3);
            for (int i = 0; i < outDims.length; i++) {
                for (int j = 0; j < 3; j++) {
                    outAffine[3][j] += low[i] * affine[i][j];
                    outAffine[i][j] = affine[i][j] * down[i];
                }
            }
            tmpField.setAffine(outAffine);
        }
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray dta = inField.getComponent(i);
            tmpField.addComponent(DataArray.create(cropDownArray(dta.getRawArray(), dta.getVectorLength(), dims, low, up, down), dta.getVectorLength(), dta.getName()));
        }

        //inField = tmpField;
        //prepareWorkObjects();
        setInField(tmpField);
    }

    void computeSimilarityField(boolean[] allowed,
                                ArrayList<Integer> indices, float[] weights,
                                int t)
    {
        if (segmentVolume != null && segmentVolumeChangeListener != null) {
            segmentVolume.removeChangeListener(segmentVolumeChangeListener);
        }
        this.tollerance = t;
        segmentVolume = new SimilaritySegmentVolume(inField, distField, outField);
        segmentVolumeChangeListener = new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (segmentVolume.getStatus() == SimilaritySegmentVolume.SIMILARITY_COMPUTED) //dp.updateOrthosliceImages();
                {
                    DataArray distDA = distField.getComponent(0);
                    distDA.setPreferredRange(0, segmentVolume.getLmax());
                    dp.updateOrthosliceIsolines();
                }
            }
        };
        segmentVolume.addChangeListener(segmentVolumeChangeListener);
        segmentVolume.setIndices(indices);
        segmentVolume.setTollerance((short) tollerance);
        segmentVolume.setWeights(weights);
        segmentVolume.setRange((int) low, (int) up);
        segmentVolume.computeDistance(gparams.getPoints(), allowed);
    }

    void computeDistanceField(boolean[] allowed, int component, int t)
    {
        if (segmentVolume != null && segmentVolumeChangeListener != null) {
            segmentVolume.removeChangeListener(segmentVolumeChangeListener);
        }

        segmentVolume = new RangeSegmentVolume(inField, distField, outField);
        segmentVolumeChangeListener = new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (segmentVolume.getStatus() == SimilaritySegmentVolume.SIMILARITY_COMPUTED) //dp.updateOrthosliceImages();
                {
                    DataArray distDA = distField.getComponent(0);
                    tollerance = segmentVolume.getLmax();
                    distDA.setPreferredRange(0, tollerance);
                    dp.updateOrthosliceIsolines();
                }
            }
        };
        segmentVolume.addChangeListener(segmentVolumeChangeListener);
        segmentVolume.setComponent(component);
        segmentVolume.setRange((int) low, (int) up);
        segmentVolume.computeDistance(gparams.getPoints(), allowed);
    }

    public void setSegIndex(int n)
    {
        segIndex = n + 1;
    }

    public void setMaxSegNumber(int maxSegNumber)
    {
        this.maxSegNumber = maxSegNumber;
    }

    void showDistFieldSlices(boolean showDist)
    {
        if (showDist && distField != null) {
            dp.setInField(distField);
        } else {
            dp.setInField(inField);
        }
    }

    void outputResultField()
    {
        outputReady = true;
        startAction();
    }

    public VolumeRenderer getVolRender()
    {
        return volRender;
    }

    public GeometryParams getGparams()
    {
        return gparams;
    }

    public DataProvider getDp()
    {
        return dp;
    }

    public VolumeSegmentationUI getSegUI()
    {
        return segUI;
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        onActive();
    }
}
