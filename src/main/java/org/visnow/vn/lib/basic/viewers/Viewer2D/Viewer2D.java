/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.Viewer2D;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.gui.widgets.VisNowFrame;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;
import org.visnow.vn.lib.utils.geometry2D.TransformedGeometryObject2D;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Viewer2D extends ModuleCore
{

    private VisNowFrame frame;
    private Display2DInternalFrame internalFrame;
    private GUI ui;

    /**
     * Creates a new instance of Viewer2D
     */
    public Viewer2D()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                frame = new VisNowFrame();
                frame.setTitle("Viewer2D");
                internalFrame = new Display2DInternalFrame();
                frame.add(internalFrame);

                internalFrame.setVisible(true);

                ui = new GUI();
                ui.addChangeListener(new ChangeListener()
                {

                    @Override
                    public void stateChanged(ChangeEvent e)
                    {
                        frame.setVisible(true);
                        frame.setExtendedState(Frame.NORMAL);
                    }
                });

                setPanel(ui);

                frame.setBounds(VisNow.getOptimalViewerWindowBounds(VisNow.get().getMainWindow().getGraphicsConfiguration().getBounds(), VisNow.get().getMainWindow().getBounds()));
                frame.setVisible(true);
            }
        });

    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    public Display2DInternalFrame getInternalFrame()
    {
        return this.internalFrame;
    }

    public VisNowFrame getFrame()
    {
        return this.frame;
    }

    @Override
    public boolean isViewer()
    {
        return true;
    }

    @Override
    public void onDelete()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ArrayList<TransformedGeometryObject2D> tobjList = internalFrame.getDisplayPanel().getChildren();
                for (int i = 0; i < tobjList.size(); i++) {
                    TransformedGeometryObject2D tobj = tobjList.get(i);
                    if (tobj != null) {
                        tobj.getGeometryObject2DStruct().removeChangeListener(object2DChangedListener);
                    }
                }
                internalFrame.getDisplayPanel().clearAllGeometry();
                frame.dispose();
            }
        });
    }

    @Override
    public void onActive()
    {
        if (!frame.isVisible())
            frame.setVisible(true);
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                String tmpName;
                Vector<Object> ins = getInputValues("inObject");
                GeometryObject2DStruct struct;
                Object obj;
                for (int i = 0; i < ins.size(); i++) {
                    obj = ins.get(i);
                    if (obj == null || !(obj instanceof VNGeometryObject) || ((VNGeometryObject) obj).getGeometryObject2DStruct() == null) {
                        continue;
                    }

                    struct = ((VNGeometryObject) obj).getGeometryObject2DStruct();
                    if (internalFrame.getDisplayPanel().getChildByParentModulePort(struct.getParentModulePort()) == null) {
                        TransformedGeometryObject2D trobj = new TransformedGeometryObject2D(struct);
                        String tmp = struct.getParentModulePort().substring(0, struct.getParentModulePort().indexOf("."));
                        if (!trobj.getName().contains(tmp)) {
                            trobj.setName(trobj.getName() + " (" + tmp + ")");
                        }

                        trobj.setParentModulePort(struct.getParentModulePort());
                        internalFrame.getDisplayPanel().addChild(trobj);
                    } else {
                        tmpName = internalFrame.getDisplayPanel().getChildByParentModulePort(struct.getParentModulePort()).getName();
                        internalFrame.getDisplayPanel().getChildByParentModulePort(struct.getParentModulePort()).updateWithStruct(struct);
                        String tmp2 = struct.getName() + " (" + struct.getParentModulePort().substring(0, struct.getParentModulePort().indexOf(".")) + ")";
                        if (!tmpName.equals(tmp2)) {
                            tmpName = new String(tmp2);
                        }
                        internalFrame.getDisplayPanel().getChildByParentModulePort(struct.getParentModulePort()).setName(tmpName);
                        internalFrame.getDisplayPanel().update();
                    }
                }
            }
        });
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        final TransformedGeometryObject2D tobj = internalFrame.getDisplayPanel().getChildByParentModulePort("" + link.getOutput());
        if (tobj != null) {
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    internalFrame.getDisplayPanel().removeChild(tobj);
                    tobj.getGeometryObject2DStruct().removeChangeListener(object2DChangedListener);
                }
            });
        }

    }

    @Override
    public void onInputAttach(LinkFace link)
    {
        Vector ins = link.getInput().getValues();
        if (ins == null) {
            return;
        }

        if (ins.get(ins.size() - 1) == null) {
            return;
        }
        GeometryObject2DStruct struct = ((VNGeometryObject) ins.get(ins.size() - 1)).getGeometryObject2DStruct();
        if (struct == null) {
            return;
        }
        struct.addChangeListener(object2DChangedListener);
        final TransformedGeometryObject2D obj = new TransformedGeometryObject2D(struct);
        if (!obj.getName().contains(link.getName().getOutputModule())) {
            obj.setName(obj.getName() + " (" + link.getName().getOutputModule() + ")");
        }
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                internalFrame.getDisplayPanel().addChild(obj);
                internalFrame.getDisplayPanel().reset();
            }
        });
    }

    private ChangeListener object2DChangedListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            onActive();
        }
    };

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow Viewer2D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

}
