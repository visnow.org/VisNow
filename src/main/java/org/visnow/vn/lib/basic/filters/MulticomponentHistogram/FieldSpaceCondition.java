/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class FieldSpaceCondition extends Condition
{

    public static enum SpatialComponent
    {

        NODE
        {
            @Override
            public String toString()
            {
                return "n";
            }
        },
        INDEX0
        {
            @Override
            public String toString()
            {
                return "i";
            }
        },
        INDEX1
        {
            @Override
            public String toString()
            {
                return "j";
            }
        },
        INDEX2
        {
            @Override
            public String toString()
            {
                return "k";
            }
        },
        SPACEX
        {
            @Override
            public String toString()
            {
                return "x";
            }
        },
        SPACEY
        {
            @Override
            public String toString()
            {
                return "y";
            }
        },
        SPACEZ
        {
            @Override
            public String toString()
            {
                return "z";
            }
        }
    };

    protected Field field;

    protected SpatialComponent spComponent = null;
    protected Float spValue = 0.0f;

    public FieldSpaceCondition(Field field, SpatialComponent spComponent, Operator spOperator, Float spValue)
    {
        this.field = field;
        this.spComponent = spComponent;
        this.operator = spOperator;
        this.spValue = spValue;
    }

    public SpatialComponent getSpComponent()
    {
        return spComponent;
    }

    public void setSpComponent(SpatialComponent comp)
    {
        this.spComponent = comp;
    }

    public Float getSpValue()
    {
        return spValue;
    }

    public void setSpValue(Float value)
    {
        this.spValue = value;
    }

    public boolean check(int n)
    {
        if (field instanceof RegularField) {
            RegularField rField = (RegularField) field;
            int[] dims = rField.getDims();
            int nDims = dims.length;
            int i, j, k;
            float[] p = null;
            i = j = k = 0;
            switch (nDims) {
                case 1:
                    i = n;
                    if (i < 0)
                        i = 0;
                    if (i >= dims[0])
                        i = dims[0] - 1;
                    break;
                case 2:
                    j = n / dims[0];
                    i = n - j * dims[0];
                    if (i < 0)
                        i = 0;
                    if (i >= dims[0])
                        i = dims[0] - 1;
                    if (j < 0)
                        j = 0;
                    if (j >= dims[1])
                        j = dims[1] - 1;
                    break;
                case 3:
                    k = n / (dims[0] * dims[1]);
                    j = (n - k * (dims[0] * dims[1])) / dims[0];
                    i = n - k * (dims[0] * dims[1]) - j * dims[0];
                    if (i < 0)
                        i = 0;
                    if (i >= dims[0])
                        i = dims[0] - 1;
                    if (j < 0)
                        j = 0;
                    if (j >= dims[1])
                        j = dims[1] - 1;
                    if (k < 0)
                        k = 0;
                    if (k >= dims[2])
                        k = dims[2] - 1;
                    break;
            }

            switch (spComponent) {
                case NODE:
                    int n0 = (int) (float) spValue;
                    return decide(n, n0);
                case INDEX0:
                    int i0 = (int) (float) spValue;
                    return decide(i, i0);
                case INDEX1:
                    if (nDims < 2)
                        return false;
                    int j0 = (int) (float) spValue;
                    return decide(j, j0);
                case INDEX2:
                    if (nDims < 3)
                        return false;
                    int k0 = (int) (float) spValue;
                    return decide(k, k0);
                case SPACEX:
                    if (nDims == 1) {
                        p = rField.getGridCoords(i);
                    } else if (nDims == 2) {
                        p = rField.getGridCoords(i, j);
                    } else if (nDims == 3) {
                        p = rField.getGridCoords(i, j, k);
                    }
                    return decide(p[0], spValue);
                case SPACEY:
                    if (nDims == 1) {
                        p = rField.getGridCoords(i);
                    } else if (nDims == 2) {
                        p = rField.getGridCoords(i, j);
                    } else if (nDims == 3) {
                        p = rField.getGridCoords(i, j, k);
                    }
                    if (p.length < 2)
                        return false;
                    return decide(p[1], spValue);
                case SPACEZ:
                    if (nDims == 1) {
                        p = rField.getGridCoords(i);
                    } else if (nDims == 2) {
                        p = rField.getGridCoords(i, j);
                    } else if (nDims == 3) {
                        p = rField.getGridCoords(i, j, k);
                    }
                    if (p.length < 3)
                        return false;
                    return decide(p[2], spValue);
            }
        } else if (field instanceof IrregularField) {
            float[] coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords().getData();
            float[] p = {0, 0, 0};

            p[0] = coords[3 * n];
            p[1] = coords[3 * n + 1];
            p[2] = coords[3 * n + 2];

            switch (spComponent) {
                case NODE:
                    int n0 = (int) (float) spValue;
                    return decide(n, n0);
                case SPACEX:
                    return decide(p[0], spValue);
                case SPACEY:
                    return decide(p[1], spValue);
                case SPACEZ:
                    return decide(p[2], spValue);
            }
        }
        return false;
    }

    public Field getField()
    {
        return field;
    }

}
