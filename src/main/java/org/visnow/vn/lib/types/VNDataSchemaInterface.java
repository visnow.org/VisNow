/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.types;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public interface VNDataSchemaInterface
{

    public abstract Class getVnDataType();

    public abstract boolean isEmpty();

    public abstract boolean isField();


    public abstract int getTrueNSpace();

    public abstract boolean isTime();

    public abstract boolean isLarge();
    
    public abstract int[] getDataVeclens();

    public abstract int[] getDataTypes();

    public abstract String[] getDataNames();

    public boolean hasScalarComponent();

    public boolean hasVectorComponent(int veclen);

    public abstract int getNData();

    public abstract boolean isRegular();

    public abstract int getNDims();

    public abstract int[] getDims();

    public abstract boolean isAffine();

    public abstract boolean isCoords();

    public abstract boolean isIrregular();

    public abstract boolean isCellSets();

    public abstract int getNCellSets();

    public abstract int[] getNCellData();

    public abstract int[][] getCellDataVeclens();

    public abstract int[][] getCellDataTypes();

    public abstract String[][] getCellDataNames();

    public boolean hasCellScalarComponent();

    public boolean hasCellVectorComponent(int veclen);

    public abstract String[] getCellSetNames();

    public boolean hasCellsPoint();

    public boolean hasCellsSegment();

    public boolean hasCellsTriangle();

    public boolean hasCellsQuad();

    public boolean hasCellsTetra();

    public boolean hasCellsPyramid();

    public boolean hasCellsPrism();

    public boolean hasCellsHexahedron();

    public boolean hasCells2D();

    public boolean hasCells3D();
    
    public abstract boolean isPoint();
}
