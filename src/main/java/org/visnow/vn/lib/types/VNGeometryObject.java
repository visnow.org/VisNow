/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.types;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.SignalingTransform3D;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNGeometryObject implements VNGeometryTransform

{
    GeometryObject geoObj = null;
    GeometryObject outObj = null;
    GeometryObject2DStruct geoObj2D = null;
    GeometryObject2DStruct outObj2D = null;

    /**
     * Creates a new instance of VNGeometryObject
     */
    public VNGeometryObject()
    {
    }

    public VNGeometryObject(GeometryObject inObject)
    {
        geoObj = inObject;
        outObj = geoObj;
    }

    public VNGeometryObject(GeometryObject inObject, GeometryObject2DStruct inObject2D)
    {
        geoObj = inObject;
        outObj = geoObj;
        geoObj2D = inObject2D;
        outObj2D = outObj2D;
    }

    public VNGeometryObject(GeometryObject2DStruct inObject2D)
    {
        geoObj2D = inObject2D;
        outObj2D = outObj2D;
    }

    public GeometryObject getGeometryObject()
    {
        return outObj;
    }

    public GeometryObject2DStruct getGeometryObject2DStruct()
    {
        return outObj2D;
    }

    public void show(boolean show)
    {
        if (show) {
            outObj = geoObj;
            outObj2D = outObj2D;
        }
        else {
            outObj = null;
            outObj2D = null;
        }
        if (listener != null)
            listener.stateChanged(new ChangeEvent(this));
    }

    public SignalingTransform3D getTransform()
    {
        if (geoObj == null)
            return null;
        return geoObj.getCurrentTransform();
    }

    public void setTransform(SignalingTransform3D transform)
    {
        if (geoObj != null)
            geoObj.setCurrentTransform(transform);
    }

    public SignalingTransform3D getCurrentTransform()
    {
        if (geoObj == null)
            return null;
        return geoObj.getCurrentTransform();
    }

    public void setCurrentTransform(SignalingTransform3D transform)
    {
        if (geoObj != null)
            geoObj.setCurrentTransform(transform);
    }


    /**
     * Utility field holding list of listeners.
     */
    private ChangeListener listener = null;

    /**
     * Registers listener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void setListener(ChangeListener listener)
    {
        this.listener = listener;
    }

    /**
     * clears listener
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void clearListener()
    {
        listener = null;
    }

}
