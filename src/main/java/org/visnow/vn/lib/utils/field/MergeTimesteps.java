/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import org.visnow.jscic.Field;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.FieldSchema;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArraySchema;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MergeTimesteps
{

    public enum Action {
        ACCUMULATE("accumulate"),
        MERGE("merge timelines"),
        FIRST("leave first"),
        IGNORE("ignore");

        private final String name;

        private Action(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    };

    /**
     * checks if time data for t0 and t1 are the same assuming that timeSteps
     * is an array of (possibly merged) time moments of one or more TimeData
     * @param timeSteps array of (possibly merged) time moments
     * @param t0 first time moment
     * @param t1 second time moment
     * @return true if data for t0 and t1 are equal (timeData has length 1, that is data are time independent
     * or both t0 and t1 are less than all time steps or both t0 and t1 are greater than all time steps;
     * in some specific cases of improperly merged time sequences a false negative answer can be given but both
     * version of mergedTimeSteps below provide correct data
     */
    public static boolean areTimeMomentsEquivalent(float[] timeSteps, float t0, float t1)
    {
        if (timeSteps.length < 2)
            return true;
        float min =  Float.MAX_VALUE;
        float max = -Float.MAX_VALUE;
        for (float timeStep : timeSteps) {
            if (min > timeStep) min = timeStep;
            if (max < timeStep) max = timeStep;
        }
        if (t0 < min && t1 < min)
            return true;
        return t0 > max && t1 > max;
    }

    /**
     * creates the set-theoretical sum of all timesteps arrays (of all components and pseudocomponents) in the input field
     * @param fldSchema any fieldSchema
     * @return an ordered array of all timesteps of all time dependent components, mask (if present) and coords(if present).
     * Time independent data are ignored.
     * When the field is not time dependent the {0} array is returned.
     */
    public static float[] mergedTimeSteps(FieldSchema fldSchema)
    {
        Set<Float> tmp = new LinkedHashSet<>();

        for (DataArraySchema compSch : fldSchema.getComponentSchemas())
            if (compSch.getTimeDataSchema().getTimesAsList().size() > 1)
                tmp.addAll(compSch.getTimeDataSchema().getTimesAsList());
        for (DataArraySchema compSch : fldSchema.getPseudoComponentSchemas())
            if (compSch.getTimeDataSchema().getTimesAsList().size() > 1)
                tmp.addAll(compSch.getTimeDataSchema().getTimesAsList());
        if (tmp.isEmpty())
            tmp.add(0f);
        float[] tSteps = new float[tmp.size()];
        int k = 0;
        for (Float t : tmp) {
            tSteps[k] = t;
            k += 1;
        }
        Arrays.sort(tSteps);
        return tSteps;
    }

    /**
     * creates the set-theoretical sum of tha timesteps arrays of selected components and pseudocomponents in the input field
     * @param fld any field
     * @param selectedComponents names of of selected components and  pseudocomponents
     * @return an ordered array of all timesteps of all time dependent components, mask (if present) and coords(if present).
     * Time independent data are ignored.
     * When the field is not time dependent the {0} array is returned.
     */
    public static float[] mergedTimeSteps(Field fld, String[] selectedComponents)
    {
        Set<Float> tmp = new LinkedHashSet<>();

        for (String compName : selectedComponents) {
            if (fld.getSchema().getComponentSchema(compName) == null) {
                if ((compName.toLowerCase().endsWith("coords") ||
                     compName.toLowerCase().endsWith("coordinates")) &&
                     fld.hasCoords())
                    tmp.addAll(fld.getCoords().getTimeDataSchema().getTimesAsList());
                if ((compName.toLowerCase().endsWith("mask") ||
                     compName.toLowerCase().endsWith("valid")) &&
                     fld.hasMask())
                    tmp.addAll(fld.getMask().getTimeDataSchema().getTimesAsList());
                continue;
            }
            if (fld.getSchema().getComponentSchema(compName).getTimeDataSchema().getTimesAsList().size() > 1)
                tmp.addAll(fld.getSchema().getComponentSchema(compName).getTimeDataSchema().getTimesAsList());
        }
        if (tmp.isEmpty())
            tmp.add(0f);
        float[] tSteps = new float[tmp.size()];
        int k = 0;
        for (Float t : tmp) {
            tSteps[k] = t;
            k += 1;
        }
        Arrays.sort(tSteps);
        return tSteps;
    }

    /**
     * checks if a float array is (up to the given precision) an arithmetic sequence
     * @param a float array to be checked
     * @param precision an array element a[i] is accepted when it differs from the i-th element of the arithmetic sequence less then by precision*array extent
     * @return true if (up to precision) a is an arithmetic sequence
     */
    public static boolean isArithmeticSequence(float[] a, float precision)
    {
        if (a == null || a.length == 0)
            return false;
        if (a.length == 1)
            return true;
        int k = a.length - 1;
        float d = precision * Math.abs(a[k] - a[0]);
        for (int i = 0; i < a.length; i++)
            if (Math.abs(a[i] - ((i * a[k]) + (k-i) * a[0]) / k) > d)
                return false;
        return true;
    }



    /**
     * Checks compatibility of two DataArraySchemas.
     *
     * @param s0                  DataArraySchema to be checked for compatibility
     * @param s1                  DataArraySchema to be checked for compatibility
     *
     * @return true if name, type, veclen and units of s are compatible
     */
    public static boolean weaklyCompatible(DataArraySchema s0, DataArraySchema s1)
    {
        return s0.getType()         == s1.getType() &&
               s0.getVectorLength() == s1.getVectorLength() &&
               s0.getNElements() == s1.getNElements() &&
               ((s0.getUnit() == null && s1.getUnit() == null) ||
                (s0.getUnit() != null && s1.getUnit() != null &&
                 s0.getUnit().equals(s1.getUnit()))) &&
               ((s0.getMatrixDims() == null && s1.getMatrixDims() == null) ||
                (s0.getMatrixDims() != null && s1.getMatrixDims() != null &&
                 Arrays.equals(s0.getMatrixDims(), s1.getMatrixDims()))) &&
               s0.isSymmetric() == s1.isSymmetric();
    }

    /**
     * Checks compatibility of two DataArrays.
     *
     * @param s0                  DataArray to be checked for compatibility
     * @param s1                  DataArray to be checked for compatibility
     *
     * @return true if name, type, veclen and units of s are compatible
     */
    public static boolean weaklyCompatible(DataArray s0, DataArray s1)
    {
        return weaklyCompatible(s0.getSchema(), s1.getSchema());
    }

    private static TimeData mergeTimeData(TimeData accumulated, TimeData added, Action action, float timeIncrement)
    {
        if (accumulated == null || accumulated.isEmpty())
            return added;
        if (added == null || added.isEmpty() || added.getType() != accumulated.getType())
            return accumulated;
        TimeData tmp = accumulated.cloneShallow();
        float currentT;
        switch (action) {
        case ACCUMULATE:
            currentT = accumulated.getEndTime();
            LargeArray lastAccumulated = accumulated.getValue(currentT);
            for (LargeArray value : added.getValues()) {
                for (long i = 0; i < value.length(); i++)
                    if (value.getFloat(i) != lastAccumulated.getFloat(i)) {
                        currentT += timeIncrement;
                        tmp.setValue(value.clone(), currentT);
                        lastAccumulated = value;
                        break;
                    }
            }
            break;
        case MERGE:
            for (int i = 0; i < added.getNSteps(); i++) {
                currentT = added.getTime(i);
                tmp.setValue(added.getValue(currentT).clone(), currentT);
            }
            break;
        }
        return tmp;
    }

    public static Field init(Field in, String[] names, Action[] actions)
    {
        Field accumulated = in.cloneShallow();
        accumulated.removeComponents();
        for (int i = 0; i < actions.length; i++) {
            if (names[i].equalsIgnoreCase("coordinates"))
                accumulated.setCoords(in.getCoords().cloneDeep());
            else if (names[i].equalsIgnoreCase("mask") && actions[i] != Action.IGNORE)
                accumulated.setMask(in.getMask().cloneDeep());
            else if (actions[i] != Action.IGNORE)
                accumulated.addComponent(in.getComponent(names[i]).cloneDeep());
        }
        return accumulated;
    }

    public static Field mergeTimesteps(Field accumulated, Field added, String[] names, Action[] actions, float timeIncrement)
    {
        if (added.getNNodes() != accumulated.getNNodes())
            return accumulated;
        for (int i = 0; i < actions.length; i++)
            if (actions[i] != Action.IGNORE && actions[i] != Action.FIRST) {
                if (added.getComponent(names[i]) != null)
                {
                    DataArray inDA = added.getComponent(names[i]);
                    DataArray outDA = accumulated.getComponent(inDA.getName());
                    if (outDA == null)
                        accumulated.addComponent(inDA.cloneDeep());
                    else if (weaklyCompatible(outDA, inDA))
                        outDA.setTimeData(mergeTimeData(outDA.getTimeData(), inDA.getTimeData(),
                                                        actions[i], timeIncrement));
                }
                if (names[i].equalsIgnoreCase("coordinates") && added.getCoords() != null)
                    accumulated.setCoords(mergeTimeData(accumulated.getCoords(), added.getCoords(),
                                                        actions[i], timeIncrement));
                if (names[i].equalsIgnoreCase("mask") && added.getMask() != null)
                    accumulated.setMask(mergeTimeData(accumulated.getMask(), added.getMask(),
                                                      actions[i], timeIncrement));
            }
        accumulated.setCurrentTime(accumulated.getEndTime());
        return accumulated;
    }
}
