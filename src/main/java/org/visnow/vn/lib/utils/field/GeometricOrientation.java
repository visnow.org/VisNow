/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class GeometricOrientation {

    private static final int N_THREADS = VisNow.availableProcessors();

    private static class CorrectPart implements Runnable
    {

        private final int iThread ;
        private final FloatLargeArray coords;
        private final int trueNSpace;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;

        public CorrectPart(int iThread, CellArray ca, FloatLargeArray coords, int trueNSpace)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.trueNSpace = trueNSpace;
            this.ca = ca;
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[orv.length];
                float[][] v = new float[trueNSpace][trueNSpace];
                float d = 0;
                for (int i = 0; i < orv.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < trueNSpace; i++)
                    for (int j = 0; j < trueNSpace; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) -
                                  coords.getFloat(3 * verts[0] + j);
                switch (trueNSpace) {
                case 1:
                    d = v[0][0];
                    break;
                case 2:
                    d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
                    break;
                case 3:
                    d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                        v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                    break;
                default:
                    break;
                }
                 orientations[k] = (byte)(d > 0 ? 1 : 0);
            }
        }
    }

    public static final void recomputeOrientations(CellArray ca, FloatLargeArray coords, int trueNSpace)
    {
        if (ca.getDim() != trueNSpace)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new CorrectPart(iThread, ca, coords, trueNSpace));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }

    private static class Correct2DPart implements Runnable
    {
        private final int iThread ;
        private final FloatLargeArray coords;
        private final FloatLargeArray normals;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;

        public Correct2DPart(int iThread, CellArray ca, FloatLargeArray coords, FloatLargeArray normals)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.normals = normals;
            this.ca = ca;
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[3];
                float[][] v = new float[3][3];
                float d = 0;
                for (int i = 0; i < verts.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 3; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) -
                                  coords.getFloat(3 * verts[0] + j);
                for (int j = 0; j < 3; j++)
                    v[2][j] = normals.getFloat(3 * verts[0] + j);
                d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                    v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                 orientations[k] = (byte)(d > 0 ? 1 : 0);
            }
        }
    }

    public static final void recompute2DOrientations(CellArray ca, FloatLargeArray coords, FloatLargeArray normals)
    {
        if (ca.getDim() != 2 || normals == null)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new Correct2DPart(iThread, ca, coords, normals));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }


    private static class CorrectPlanarPart implements Runnable
    {
        private final int iThread ;
        private final FloatLargeArray coords;
        private final float[] normal;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;

        public CorrectPlanarPart(int iThread, CellArray ca, FloatLargeArray coords, float[] normal)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.normal = normal;
            this.ca = ca;
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[3];
                float[][] v = new float[3][3];
                v[2] = normal;
                float d = 0;
                for (int i = 0; i < verts.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 3; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) -
                                  coords.getFloat(3 * verts[0] + j);
                d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                    v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                 orientations[k] = (byte)(d > 0 ? 1 : 0);
            }
        }
    }

    public static final void recomputePlanarOrientations(CellArray ca, FloatLargeArray coords, float[] normal)
    {
        if (ca.getDim() != 2 || normal == null)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new CorrectPlanarPart(iThread, ca, coords, normal));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }

    public static final void recomputeOrientations(IrregularField fld)
    {
        FloatLargeArray coords = fld.getCurrentCoords();
        for (CellSet cs : fld.getCellSets())
            for (CellArray ca : cs.getCellArrays()) {
                if (ca == null)
                    continue;
                if (ca.getDim() == fld.getTrueNSpace())
                    recomputeOrientations(ca, coords, fld.getTrueNSpace());
                else if (ca.getDim() == 2 && fld.getNormals() != null)
                    recompute2DOrientations(ca, coords, fld.getNormals());
            }
    }

    public static final void recomputeOrientations(IrregularField fld, float[] normal)
    {
        FloatLargeArray coords = fld.getCurrentCoords();
        for (CellSet cs : fld.getCellSets())
            for (CellArray ca : cs.getCellArrays()) {
                if (ca == null)
                    continue;
                else if (ca.getDim() == 2 && normal != null)
                    recomputePlanarOrientations(ca, coords, normal);
            }
    }

    public static final void  createBoundaryCellNormals(CellSet cellSet, float[] coords)
    {
        if (cellSet == null || cellSet.getBoundaryCellArrays() == null)
            return;
        for (CellArray ar : cellSet.getBoundaryCellArrays())
            if (ar != null && ar.getNCells() > 0 && ar.getDim() == 2)
                ar.computeCellNormals(new FloatLargeArray(coords));
    }

    public static final void createBoundaryNodeNormals(IrregularField fld)
    {
        float[] normals = new float[3 * (int)fld.getNNodes()];
        for (CellSet cellSet : fld.getCellSets()) {
            for (int m = Cell.getNProperCellTypesUpto1D(); m < Cell.getNProperCellTypesUpto2D(); m++) {
                CellArray ar = cellSet.getCellArray(CellType.getType(m));
                if (ar == null)
                    continue;
                if (ar.getCellNormals() == null)
                    ar.computeCellNormals(fld.getCurrentCoords());
                float[] cNormals = ar.getCellNormals().getData();
                for (int i = 0; i < ar.getNCells(); i++) {
                    try {
                        int[] cellNodes = ar.getNodes(i);
                        for (int j = 0; j < cellNodes.length; j++) {
                            int k = cellNodes[(int) j];
                            for (int l = 0; l < 3; l++)
                                normals[3 * k + l] += cNormals[3 * i + l];
                        }
                    } catch (Exception e) {
                    }
                }
                CellArray bar = cellSet.getBoundaryCellArray(CellType.getType(m));
                if (bar == null)
                    continue;
                cNormals = bar.getCellNormals().getData();
                for (int i = 0; i < ar.getNCells(); i++) {
                    try {
                        int[] cellNodes = ar.getNodes(i);
                        for (int j = 0; j < cellNodes.length; j++) {
                            int k = cellNodes[(int) j];
                            for (int l = 0; l < 3; l++)
                                normals[3 * k + l] += cNormals[3 * i + l];
                        }
                    } catch (Exception e) {
                    }
                }
            }
            for (int i = 0; i < fld.getNNodes(); i++) {
                double d = normals[3 * i] * normals[3 * i] + normals[3 * i + 1] * normals[3 * i + 1] + normals[3 * i + 2] * normals[3 * i + 2];
                if (d == 0)
                    normals[3 * i] = 1;
                else {
                    float r = (float) (Math.sqrt(d));
                    for (int j = 0; j < 3; j++)
                        normals[3 * i + j] /= r;
                }
            }
        }
        fld.setNormals(new FloatLargeArray(normals));
    }

}
