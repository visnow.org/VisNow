/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import static org.apache.commons.math3.util.FastMath.sqrt;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.datamaps.ColorMap;
import org.visnow.vn.geometries.geometryTemplates.Glyph;
import org.visnow.vn.geometries.geometryTemplates.ScalarGlyphTemplates;
import org.visnow.vn.geometries.geometryTemplates.VectorGlyphTemplates;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenColoringAttributes;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenMaterial;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.objects.generics.OpenTransparencyAttributes;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.engine.core.ParameterChangeListener;
import static org.apache.commons.math3.util.FastMath.abs;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedGeometryStripArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.IndexedTriangleFanArray;
import org.jogamp.java3d.IndexedTriangleStripArray;
import org.jogamp.java3d.PolygonAttributes;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class GlyphFlowCore {
    protected OpenBranchGroup outGroup = new OpenBranchGroup();
    protected OpenBranchGroup glyphGroup;
    
    protected GlyphFlowParams params;
    protected DataMappingParams dataMappingParams;
    
    protected Field inField = null;
    
    protected boolean invalid = true;
    
    protected int[] startDims;
    protected DataArray glyphDataArray;
    
    protected int[] baseGlyphIn = null;
    protected int[] frameOffsets = null;
    protected int[] glyphIndices = null;
    
    protected float[] baseCoords = null;
    protected float[] baseU = null;
    protected float[] baseV = null;
    protected float[] baseW = null;
    protected float[] dynScale = null;
    
    protected int nGlyphs, nstrip, nvert, nind, ncol;
    protected boolean isNormals = false;
    protected int[] cIndex = null;
    protected int[] pIndex = null;
    protected int[] strips = null;
    protected float[] verts = null;
    protected float[] normals = null;
    protected float[] trajectoryDynScale = null;
    protected byte[] colors = null;
    
    protected int animationType = GlyphFlowParams.ONCE;
    protected boolean animating = false;
    protected boolean renderDone = true;
    protected int nFrames = 1;
    protected int nBatches = 1;
    protected int frame = 0;
    protected int nStreamlines = 0;
    protected int dir = 0;
    
    protected Glyph gt = null;
    protected IndexedGeometryStripArray glyphs = null;
    protected OpenShape3D surfaces = new OpenShape3D();
    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenTransparencyAttributes transparencyAttributes = new OpenTransparencyAttributes();
    protected OpenLineAttributes lattr = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);
    
    protected ColorMap colorMap = null;
    protected int currentColorMode = -1;
    
    protected static final boolean[][] RESET_GEOMETRY
        = {{false, true,  true,  true,  true,  true,  true,  true, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true,  true,  true,  true,  true,  true,  true,  true, false}
        };

/**
 * Creates and updates a branch group of glyphs floating according to a special input field
 * @param inField     a trajectory field mimicking a time dependent field
 * if  inField has no user data or if user data do not begin with "streamlines" or "trajectories"
 * invalid flag is raised.
 * "trajectories" 
 * @param params parameters for glyph template, scale etc. and the mode of glyph flow
 * @param dataMappingParams a DataMappingParams object defining glyph coloring
 */
    public GlyphFlowCore(Field inField, GlyphFlowParams params, DataMappingParams dataMappingParams)
    {
        invalid = false;
        this.inField = inField;
        if (inField.getUserData() == null || inField.getUserData().length < 1) 
            invalid = true;
        String[] items = inField.getUserData()[0].split(" +");
        if (!items[0].equalsIgnoreCase("streamlines") && 
            !items[0].equalsIgnoreCase("trajectories")) 
            invalid = true;
        nStreamlines = 0;
        if (items.length > 2)
            nStreamlines = Integer.parseInt(items[2]);
        if (inField.getUserData().length > 1) {
            nStreamlines = 1;
            items = inField.getUserData()[1].split(" +");
            startDims = new int[items.length - 2];
            for (int i = 0; i < startDims.length; i++) {
                startDims[i] = Integer.parseInt(items[i + 1]);
                nStreamlines *= startDims[i];
            }
        }
        else
            startDims = new int[] {nStreamlines};
        if (nStreamlines == 0 || inField.getNNodes() % nStreamlines != 0) 
            invalid = true;
        nFrames = (int)(inField.getNNodes() / nStreamlines);
        trajectoryDynScale = new float[(int)inField.getNNodes()];
        float[] crds = inField.getCurrentCoords().getData();
        Arrays.fill(trajectoryDynScale, 1);
        for (int i = 0; i < nStreamlines; i++) {
            float[] start = new float[3];
            float[] prev = new float[3];
            float[] current = new float[3];
            boolean onStart = true;
            int lastStart = 0;
            int firstStop = nFrames;
            System.arraycopy(crds, 3 * i, start, 0, 3);
            System.arraycopy(crds, 3 * i, prev, 0, 3);
            for (int j = 1; j < nFrames; j++) {
                System.arraycopy(crds, 3 * (j * nStreamlines + i), current, 0, 3);
                if (onStart) {
                    for (int k = 0; k < 3; k++) 
                        if (current[k] != start[k]) {
                            onStart = false;
                            break;
                        }
                    if (onStart) {
                        lastStart = j;
                        continue;
                    }
                }
                boolean onStop = true;
                for (int k = 0; k < 3; k++) {
                    if (current[k] != prev[k]) {
                        onStop = false;
                        break;
                    }
                }
                if (onStop) {
                    firstStop = j - 1;
                    break;
                }
                else
                    System.arraycopy(current, 0, prev, 0, 3);
            }
            lastStart = Math.min(lastStart + 50, nFrames - 1);
            for (int j = lastStart; j >= 0; j--) 
                trajectoryDynScale[j * nStreamlines + i] = Math.max(0,1 - (lastStart - j) / (float)lastStart);
            for (int j = Math.max(0, firstStop - 50); j < nFrames; j++) 
                trajectoryDynScale[j * nStreamlines + i] = Math.max(0, (firstStop - j) / 50f);
        }
        this.params = params;
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                glyphFlowParameterChanged(name);
            }
        });
        this.dataMappingParams = dataMappingParams;
        dataMappingParams.clearRenderEventListeners();

        appearance.setTransparencyAttributes(transparencyAttributes);
        appearance.getPolygonAttributes().setBackFaceNormalFlip(true);

        OpenMaterial mat = new OpenMaterial();
        mat.setShininess(15.f);
        mat.setColorTarget(OpenMaterial.AMBIENT_AND_DIFFUSE);
        appearance.setMaterial(mat);
        PolygonAttributes pattr = new PolygonAttributes(
            PolygonAttributes.POLYGON_FILL,
            PolygonAttributes.CULL_NONE, 0.f, true);
        appearance.setPolygonAttributes(pattr);
        OpenColoringAttributes colAttrs = new OpenColoringAttributes();
        appearance.setColoringAttributes(colAttrs);
        lattr.setLineWidth(params.getLineWidth());
        appearance.setLineAttributes(lattr);
        appearance.setUserData(this);
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

    }
    
/**
 * This method is called when rendering current frame is finished. It updates glyphs position, sizes
 * and colors for the next frame
 * @param e A RenderEvent containing type of update (coordinates only, colors only etc.)
 */
    public void updateRendering(RenderEvent e)
    {
        if (inField != null) {
            int extent = e.getUpdateExtent();
            int cMode = dataMappingParams.getColorMode();
            if (currentColorMode < 0) {
                updateGeometry();
                currentColorMode = cMode;
                return;
            }
            if (extent == RenderEvent.COLORS || extent == RenderEvent.TRANSPARENCY || extent == RenderEvent.TEXTURE) {
                if (RESET_GEOMETRY[currentColorMode][cMode])
                    updateGeometry();
                else
                    updateFrame();
                currentColorMode = cMode;
                return;
            }
            if (extent == RenderEvent.COORDS)
                updateCoords();
            if (extent == RenderEvent.GEOMETRY)
                updateGeometry();
            currentColorMode = cMode;
        }
    }
/**
 * A method called when one of the parameters is changed and updating glyphs according to the new parameters
 * @param name Name of the parameter changed;
 */        
    public void glyphFlowParameterChanged(String name)
    {
        switch (name) {
        case GlyphFlowParams.COMPONENT:
            updateColors();
            break;
        case GlyphFlowParams.DOWN:
            updateColors();
            break;
        case GlyphFlowParams.TYPE:
            updateGeometry();
            break;
        case GlyphFlowParams.LOD:
            updateGeometry();
            break;
        case GlyphFlowParams.CONSTANT_SIZE:
            updateCoords();
            break;
        case GlyphFlowParams.SCALE:
            updateCoords();
            break;
        case GlyphFlowParams.LINE_WIDTH:
            break;
        case GlyphFlowParams.USE_ABS:
            updateCoords();
            break;
        case GlyphFlowParams.USE_SQRT:
            updateCoords();
            break;
        case GlyphFlowParams.N_OF_BATCHES:
            updateColors();
            break;
        case GlyphFlowParams.ANIMATION_TYPE:
            animationType = params.getAnimationType();
            dir = GlyphFlowParams.STOP;
            frame = 0;
            updateFrame();
            break;
        case GlyphFlowParams.DELAY:
            break;
        case GlyphFlowParams.ANIMATE:
            switch (params.getAnimate()) {
            case GlyphFlowParams.RESET:
                dir = GlyphFlowParams.STOP;
                frame = 0;
                updateFrame();
                break;
            case GlyphFlowParams.STOP:
                dir = GlyphFlowParams.STOP;
                break;
            case GlyphFlowParams.FORWARD:
                dir = params.getAnimate();
                new Thread(new Animation()).start();
            }
            break;
        }
    }
    
    private class Animation implements Runnable
    {
        @Override
        public synchronized void run()
        {
            if (animating)
                return;
            animating = true;
            while (dir != GlyphFlowParams.STOP) {
                renderDone = false;
                frame = frame + dir;
                if (frame >= 2 * nFrames) {
                    if (animationType == GlyphFlowParams.ONCE) {
                        dir = GlyphFlowParams.STOP;
                        frame = 0;
                        updateUIs();
                    }
                    if (animationType == GlyphFlowParams.CYCLE) 
                        frame = 0;
                }
                updateFrame();
                try {
                    while (!renderDone) {
                        wait(10);
                    }
                    wait(params.getDelay() + 1);
                } catch (InterruptedException c) {
                    System.out.println("interrupt");
                }
            }
            animating = false;
        }
    }
    
/**
 * This method sets renderDone flag allowing animation to pass to the next frame
 * @param renderDone always called with renderDone=true
 */
    public void setRenderDone(boolean renderDone)
    {
        this.renderDone = renderDone;
    }
    
    
    private void prepareLocalCoords()
    {
        if (glyphDataArray == null)
            return;
        float[] um = {0, 0, 0};
        float[] vm = {0, 0, 0};
        float[] wm = {0, 0, 0};

        //float[] p = {0, 0, 0};
        int vlen = glyphDataArray.getVectorLength();
        for (int i = 0; i < nGlyphs * nBatches; i++) {
            float[] p = glyphDataArray.getFloatElement(glyphIndices[i]);
            
            float pn = 0;
            for (int j = 0; j < p.length; j++)
                pn += p[j] * p[j];
            if (vlen == 1 || vlen > 3 || pn < 1e-20) {
                for (int j = 0; j < 3; j++)
                    baseU[3 * i + j] = baseV[3 * i + j] = baseW[3 * i + j] = 0;
                baseU[3 * i] = baseV[3 * i + 1] = baseW[3 * i + 2] = 1;
                continue;
            }
            pn = (float)sqrt(pn);
            for (int j = 0; j < p.length; j++)
                um[j] = p[j] / pn;
            if (abs(um[0]) > abs(um[1]) && abs(um[0]) > abs(um[2])) {
                vm[0] = vm[1] = 0;
                vm[2] = 1;
            } else {
                vm[1] = vm[2] = 0;
                vm[0] = 1;
            }
            wm[0] = um[1] * vm[2] - um[2] * vm[1];
            wm[1] = um[2] * vm[0] - um[0] * vm[2];
            wm[2] = um[0] * vm[1] - um[1] * vm[0];
            pn = (float) sqrt(wm[0] * wm[0] + wm[1] * wm[1] + wm[2] * wm[2]);
            for (int j = 0; j < 3; j++)
                wm[j] /= pn;
            vm[0] = um[2] * wm[1] - um[1] * wm[2];
            vm[1] = um[0] * wm[2] - um[2] * wm[0];
            vm[2] = um[1] * wm[0] - um[0] * wm[1];
            System.arraycopy(um, 0, baseW, 3 * i, 3);
            System.arraycopy(vm, 0, baseV, 3 * i, 3);
            System.arraycopy(wm, 0, baseU, 3 * i, 3);
        }
    }

    private void prepareGlyphCount()
    {
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        nBatches = params.getNBatches();
        int maxNGlyphs = 1;
        int[] down = params.getDown();
        for (int i = 0; i < Math.min(startDims.length, down.length); i++)
            maxNGlyphs *= startDims[i] / down[i] + 1;
        baseGlyphIn = new int[maxNGlyphs];
        nGlyphs = 0;
        glyphs = null;
        int l;
        switch (startDims.length) {
        case 3:
            for (int i = 0; i < startDims[2]; i += down[2])
                for (int j = 0; j < startDims[1]; j += down[1]) {
                    l = (startDims[1] * i + j) * startDims[0];
                    for (int k = 0; k < startDims[0]; k += down[0], l += down[0]) {
                        baseGlyphIn[nGlyphs] = l;
                        nGlyphs += 1;
                    }
                }
            break;
        case 2:
            for (int j = 0; j < startDims[1]; j += down[1]) {
                l = j * startDims[0];
                for (int k = 0; k < startDims[0]; k += down[0], l += down[0]) {
                    baseGlyphIn[nGlyphs] = l;
                    nGlyphs += 1;
                    }
            }
            break;
        case 1:
            for (int k = 0; k < startDims[0]; k += down[0]) {
                    baseGlyphIn[nGlyphs] = k;
                    nGlyphs += 1;
                }
        }
        frameOffsets = new int[nBatches];
        for (int i = 0; i < nBatches; i++) 
            frameOffsets[i] = -i;
        glyphIndices = new int[nBatches * nGlyphs];
        colors = new byte[3 * nGlyphs * nBatches];
        baseCoords = new float[3 * nGlyphs * nBatches];
        baseU = new float[3 * nGlyphs * nBatches];
        baseV = new float[3 * nGlyphs * nBatches];
        baseW = new float[3 * nGlyphs * nBatches];
        dynScale = new float[nGlyphs * nBatches];
    }
    
    private void updateFrame()
    {
        if (baseGlyphIn == null)
            return;
        FloatLargeArray fldCoords = inField.getCoords(0);
        for (int i = 0; i < nBatches; i++) {
            int frameOff = frame;
            if (nBatches > 1)
                frameOff -= (i * nFrames) / (nBatches -1);
            if (params.getAnimationType() == GlyphFlowParams.CONTINUOUS && frameOff > 0)
                frameOff = frameOff % nFrames; 
            frameOff = Math.max(0, Math.min(nFrames - 1, frameOff));
            float s = params.getScale();
            frameOff = nStreamlines * frameOff;
            for (int j = 0; j < nGlyphs; j++) {
                int l = nGlyphs * i + j;
                int n = glyphIndices[l] = frameOff + baseGlyphIn[j];
                for (int k = 0; k < 3; k++)
                    baseCoords[3 * l + k] = fldCoords.get(3 * n + k);
                dynScale[l] = s * trajectoryDynScale[n];
            }
            prepareLocalCoords();
        }
        updateCoords();
        colors = ColorMapper.mapColorsIndexed(inField, dataMappingParams, glyphIndices, dataMappingParams.getDefaultColor(), colors);
        glyphs.setColors(0, colors);
    }
/**
 * Can be called anytime to update completely the geometry structure
 */
    public void updateGeometry()
    {
        outGroup.removeAllChildren();
        glyphGroup = new OpenBranchGroup();
        surfaces.removeAllGeometries();
        glyphs = null;
        int cMode = dataMappingParams.getColorMode();
        generateGlyphs(cMode);
        surfaces = new OpenShape3D();
        surfaces.setAppearance(appearance);
        surfaces.addGeometry(glyphs);
        updateFrame();
        glyphGroup.addChild(surfaces);
        outGroup.addChild(glyphGroup);
    }

    private void generateGlyphs(int cMode)
    {
        if (nGlyphs * nBatches < 1)
            return;
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        int type = params.getGlyphType();
        int lod = params.getLevelOfDetail();

        if (glyphDataArray == null || glyphDataArray.getVectorLength() == 1)
            gt = ScalarGlyphTemplates.glyph(type, lod);
        else
            gt = VectorGlyphTemplates.glyph(type, lod);
        nstrip = nGlyphs * nBatches * gt.getNstrips();
        nvert = nGlyphs * nBatches * gt.getNverts();
        nind = nGlyphs * nBatches * gt.getNinds();
        ncol = nGlyphs * nBatches;
        strips = new int[nstrip];
        verts = new float[3 * nvert];
        normals = new float[3 * nvert];
        pIndex = new int[nind];
        cIndex = new int[nind];
        makeIndices();
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 1)
            prepareLocalCoords();
        if (verts == null || verts.length != 3 * nGlyphs * nBatches * gt.getNverts())
            verts = new float[3 * nGlyphs * nBatches * gt.getNverts()];

        int verticesMode = GeometryArray.COORDINATES;
        isNormals = false;
        if (gt.getType() != Glyph.LINE_STRIPS) {
            verticesMode |= GeometryArray.NORMALS;
            isNormals = true;
        }
        verticesMode |= GeometryArray.COLOR_4;

        if (isNormals && (normals == null || normals.length != 3 * nGlyphs * nBatches * gt.getNverts()))
            normals = new float[3 * nGlyphs * nBatches * gt.getNverts()];

        switch (gt.getType()) {
            case Glyph.TRIANGLE_STRIPS:
                glyphs = new IndexedTriangleStripArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.TRIANGLE_FANS:
                glyphs = new IndexedTriangleFanArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.LINE_STRIPS:
                glyphs = new IndexedLineStripArray(nvert, verticesMode, nind, strips);
                break;
        }
        glyphs.setCapability(IndexedLineStripArray.ALLOW_COUNT_READ);
        glyphs.setCapability(IndexedLineStripArray.ALLOW_FORMAT_READ);
        glyphs.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_INDEX_READ);
        glyphs.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_READ);
        glyphs.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_WRITE);
        if (cMode == DataMappingParams.UVTEXTURED) {
            glyphs.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_READ);
            glyphs.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_WRITE);
            glyphs.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_READ);
            glyphs.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_WRITE);
        } else {
            glyphs.setCapability(IndexedLineStripArray.ALLOW_COLOR_READ);
            glyphs.setCapability(IndexedLineStripArray.ALLOW_COLOR_WRITE);
            glyphs.setColorIndices(0, cIndex);
        }
        glyphs.setCoordinates(0, verts);
        glyphs.setCoordinateIndices(0, pIndex);
        if (isNormals) {
            glyphs.setCapability(IndexedLineStripArray.ALLOW_NORMAL_READ);
            glyphs.setCapability(IndexedLineStripArray.ALLOW_NORMAL_WRITE);
            glyphs.setNormals(0, normals);
            glyphs.setNormalIndices(0, pIndex);
        }
        updateCoords();
    }

    private void updateCoords()
    {
        float s, st;
        boolean useAbs = params.isUseAbs();
        boolean useSqrt = params.isUseSqrt();
        float[] tVerts = gt.getVerts();
        float[] tNorms = gt.getNormals();
        float[] p = new float[3];
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        if (glyphDataArray == null) {
            for (int i = 0, k = 0; i < nGlyphs * nBatches; i++) {
                s = dynScale[i];
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        } else if (glyphDataArray.getVectorLength() == 1)
            for (int i = 0, k = 0; i < nGlyphs * nBatches; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (params.isConstantGlyphSize())
                    s = dynScale[i];
                else {
                    s = glyphDataArray.getFloatElement(glyphIndices[i])[0];
                    if (useAbs || useSqrt)
                        s = abs(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= dynScale[i];
                }
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        else
            for (int i = 0, k = 0; i < nGlyphs * nBatches; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                System.arraycopy(baseU, 3 * i, u, 0, 3);
                System.arraycopy(baseV, 3 * i, v, 0, 3);
                System.arraycopy(baseW, 3 * i, w, 0, 3);
                float[] vs = glyphDataArray.getFloatElement(glyphIndices[i]);
                if (params.isConstantGlyphSize())
                    s = dynScale[i];
                else {
                    s = 0;
                    for (int j = 0; j < vs.length; j++)
                        s += vs[j] * vs[j];
                    s = (float) sqrt(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= dynScale[i] ;
                }
                st = s;
                if (isNormals)
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++) {
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
                            normals[k] = tNorms[3 * j] * u[l] + tNorms[3 * j + 1] * v[l] + tNorms[3 * j + 2] * w[l];
                        }
                else
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++)
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
            }
        glyphs.setCoordinates(0, verts);
        if (isNormals)
            glyphs.setNormals(0, normals);
    }
    
/**
 * This method forces update of glyph colors according to current dataMappingParams
 */
    
    public void updateColors()
    {
        prepareGlyphCount();
        currentColorMode = dataMappingParams.getColorMode();
        if (nGlyphs < 1)
            return;
        updateGeometry();
    }

    protected void makeIndices()
    {
        int istrip = 0, iind = 0, ivert = 0, icol = 0;
        for (int n = 0; n < nGlyphs * nBatches; n++) {
            for (int i = 0; i < gt.getNstrips(); i++, istrip++)
                strips[istrip] = gt.getStrips()[i];
            for (int i = 0; i < gt.getNinds(); i++, iind++) {
                pIndex[iind] = ivert + gt.getPntsIndex()[i];
                cIndex[iind] = icol;
            }
            ivert += gt.getNverts();
            icol += 1;
        }
    }
    
/**
 * Getter for current glyphs geometry
 * @return a branch group for display
 */
    public OpenBranchGroup getGeometry() {
        return outGroup;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> uiListenerList = new ArrayList<>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addUIChangeListener(ChangeListener listener)
    {
        uiListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeUIChangeListener(ChangeListener listener)
    {
        uiListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     * <p>
     */
    protected void updateUIs()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : uiListenerList)
            listener.stateChanged(e);
    }

}
