/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.vtk;

import java.io.File;
import java.nio.ByteOrder;
import org.visnow.jscic.Field;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public abstract class VTKCore
{

    String logFilePath = System.getProperty("user.home") + File.separator + ".visnow" + File.separator + "log" + File.separator + "vtklog.log";
    // TODO: load this somehow from log4j's configuration;
    File logFile = new File(logFilePath);

    /**
     * Creates a new
     * <code>VTKCore</code> object.
     */
    public VTKCore()
    {
    }

    public abstract Field readVTK(String filename, ByteOrder order);
    public abstract Field readVTK(File filename, ByteOrder order);

    public static VTKCore loadVTKLibrary()
    {
        if (VisNow.isNativeLibraryLoaded("vtk")) {
            return new VTKNativeCore();
        } else {
            return new VTKJavaCore();
        }
    }
}
