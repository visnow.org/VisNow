/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.graph3d;

import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.utils.graph3d.GenericGraph3DShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GenericGraph3DGUI extends javax.swing.JPanel
{

    protected Parameters parameters = null;
    protected double valGeomRatio = 1;

    /**
     * Creates new form GUI
     */
    public GenericGraph3DGUI()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        dataComponentUI = new org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI(false);
        zeroBaseButton = new javax.swing.JRadioButton();
        minBaseButton = new javax.swing.JRadioButton();
        globalBox = new javax.swing.JCheckBox();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        scaleSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        physScaleField = new org.visnow.vn.gui.components.FloatFormattedTextField();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Plot component and scale", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(dataComponentUI, gridBagConstraints);

        buttonGroup1.add(zeroBaseButton);
        zeroBaseButton.setSelected(true);
        zeroBaseButton.setText("zero based");
        zeroBaseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                zeroBaseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        add(zeroBaseButton, gridBagConstraints);

        buttonGroup1.add(minBaseButton);
        minBaseButton.setText("min based");
        minBaseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                minBaseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        add(minBaseButton, gridBagConstraints);

        globalBox.setText("use global (preferred) min/max");
        globalBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                globalBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        add(globalBox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("<html>graph geometric proportions<p> (flat / tall)</html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 3, 0);
        jPanel1.add(jLabel2, gridBagConstraints);

        scaleSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        scaleSlider.setGlobalMax(10);
        scaleSlider.setGlobalMin(.001);
        scaleSlider.setMax(10);
        scaleSlider.setMin(.001);
        scaleSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        scaleSlider.setValue(.3);
        scaleSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                scaleSliderUserAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(scaleSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        add(jPanel1, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("<html>graph scale (world geometry unit per component physical unit)</html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 3, 0);
        jPanel2.add(jLabel3, gridBagConstraints);

        physScaleField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                physScaleFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel2.add(physScaleField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void zeroBaseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_zeroBaseButtonActionPerformed
    {//GEN-HEADEREND:event_zeroBaseButtonActionPerformed
        parameters.set(ZERO_BASED, zeroBaseButton.isSelected());
    }//GEN-LAST:event_zeroBaseButtonActionPerformed

    private void minBaseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_minBaseButtonActionPerformed
    {//GEN-HEADEREND:event_minBaseButtonActionPerformed
        parameters.set(ZERO_BASED, zeroBaseButton.isSelected());
    }//GEN-LAST:event_minBaseButtonActionPerformed

    private void globalBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_globalBoxActionPerformed
    {//GEN-HEADEREND:event_globalBoxActionPerformed
        parameters.set(GLOBAL_RANGE, globalBox.isSelected());
    }//GEN-LAST:event_globalBoxActionPerformed

    private void scaleSliderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_scaleSliderUserAction
    {//GEN-HEADEREND:event_scaleSliderUserAction
        parameters.set(SCALE, scaleSlider.getValue().floatValue());
        physScaleField.setValue(scaleSlider.getValue().floatValue() * (float)valGeomRatio);
        parameters.set(ADJUSTING, scaleSlider.isAdjusting());
    }//GEN-LAST:event_scaleSliderUserAction

    private void physScaleFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_physScaleFieldActionPerformed
    {//GEN-HEADEREND:event_physScaleFieldActionPerformed
        float val = physScaleField.getValue();
        if (val < scaleSlider.getMin().floatValue() * valGeomRatio)
            val = (float)(scaleSlider.getMin().floatValue() * valGeomRatio);
        if (val > scaleSlider.getMax().floatValue() * valGeomRatio)
            val = (float)(scaleSlider.getMax().floatValue() * valGeomRatio);
        if (val != physScaleField.getValue())
            physScaleField.setValue(val);
        parameters.set(SCALE, (float)(val / valGeomRatio));
        scaleSlider.setValue(val / valGeomRatio);
    }//GEN-LAST:event_physScaleFieldActionPerformed

    /**
     * Set the value of parameters
     *
     * @param parameters new value of parameters
     */
    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
        dataComponentUI.setComponentFeature(parameters.get(COMPONENT));
    }

    public void setValGeomRatio(double valGeomRatio)
    {
        this.valGeomRatio = valGeomRatio;
        physScaleField.setValue(scaleSlider.getValue().floatValue() * (float)valGeomRatio);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI dataComponentUI;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JCheckBox globalBox;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton minBaseButton;
    private org.visnow.vn.gui.components.FloatFormattedTextField physScaleField;
    private org.visnow.vn.gui.widgets.ExtendedSlider scaleSlider;
    private javax.swing.JRadioButton zeroBaseButton;
    // End of variables declaration//GEN-END:variables

}
