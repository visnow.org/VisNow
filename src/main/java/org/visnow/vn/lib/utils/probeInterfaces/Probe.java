/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.probeInterfaces;

import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.lib.utils.field.subset.subvolume.IrregularFieldSplitter;

public abstract class Probe {

    abstract public void setInData(Field inField, DataMappingParams dataMappingParams);

    abstract public void setDataMappingParams(DataMappingParams dataMappingParams);

    abstract public OpenBranchGroup getGlyphGeometry();

    abstract public JPanel getGlyphGUI();

    public IrregularField getSliceField()
    {
        return null;
    }

    public IrregularField getSliceAreaField()
    {
        return null;
    }

    abstract public void hide();

    abstract public void show();

    public float[] getPlaneCenter()
    {
        return null;
    }

    public float[] getPlaneX()
    {
        return null;
    }

    public float[] getPlaneY()
    {
        return null;
    }

    public float getRX()
    {
        return Float.MAX_VALUE;
    }

    public float getRY()
    {
        return Float.MAX_VALUE;
    }

    public float getRZ()
    {
        return Float.MAX_VALUE;
    }

    public RegularField getRegularSliceField()
    {
        return null;
    }

    public RegularField getRegularSliceAreaField()
    {
        return null;
    }

    public static final float[] dirComponent(float[] coords, float[] center, float[] direction)
    {
        float[] var = new float[coords.length / 3];
        for (int i = 0; i < var.length; i++) {
            float v = 0;
            for (int j = 0; j < 3; j++)
                v += direction[j] * (coords[3 * i + j] - center[j]);
            var[i] = v;
        }
        return var;
    }

    public static final IrregularField clipOneDir(IrregularField sliceField, float[] center, float[] axis, float thr, boolean above, float[] normal)
    {
        if (sliceField == null)
            return null;
        for (CellSet cellSet : sliceField.getCellSets())
            cellSet.generateDisplayData(sliceField.getCurrentCoords());
        FloatLargeArray isoComp = new FloatLargeArray(dirComponent(sliceField.getCurrentCoords().getData(), center, axis));
        return IrregularFieldSplitter.splitField(sliceField, isoComp, 1,  thr, above, normal);
    }

    /**
     * Utility field holding list of global ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged(boolean adjusting)
    {
        if (adjusting)
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++)
            changeListenerList.get(i).stateChanged(e);
    }
}
