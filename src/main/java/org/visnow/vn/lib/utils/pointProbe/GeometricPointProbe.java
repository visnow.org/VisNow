/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.pointProbe;

import java.util.Arrays;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.geometries.objects.generics.*;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.INDEX_COORDS;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class GeometricPointProbe extends Probe 
{

    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
    }

    protected Field inField = null;
    protected RegularField regularInField = null;
    protected IrregularField irregularInField = null;
    protected int trueNSpace = 3;
    protected int[] dims;
    protected IrregularField pointField = null;
    
    protected InteractiveGlyph       glyph       = new InteractiveGlyph(POINT);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI    glyphGUI    = glyph.getComputeUI();
    protected float[]                center      = glyphParams.getCenter(); 
    
    protected DataMappingParams mapParams;
    
    
    public GeometricPointProbe()
    {
        glyph.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                updateProbe();
                show();
            }
        });
        glyphParams.setFireWhenAdjusting(true);
        glyph.setName("probeGlyph");
    }
    
    public void probePosition()
    {
        float[] ind = null; 
        pointField = new IrregularField(1);
        CellSet pointCells = new CellSet("point");
        pointCells.addCells(new CellArray(CellType.POINT, new int[]{0}, new byte[]{(byte)1}, null));
        pointField.addCellSet(pointCells);
        center      = Arrays.copyOf(glyphParams.getCenter(), 3); 
        pointField.setCurrentCoords(new FloatLargeArray(center));
        if (inField instanceof RegularField) {
            switch (regularInField.getDimNum()) {
            case 3:
               ind = regularInField.getFloatIndices(center[0], center[1], center[2]); 
               break;
            case 2:
               ind = regularInField.getFloatIndices(center[0], center[1]); 
               break;
            case 1:
               ind = new float[] {center[0]};
               break;
            }
            pointField.removeComponents();
            pointField.addComponent(DataArray.create(ind, dims.length, INDEX_COORDS));
        }
        else {
            SimplexPosition pos = inField.getFieldCoords(center);
            if (pos == null)
                return;
            FieldPosition[] nodes = new FieldPosition[] {new FieldPosition(pos)};
            pointField.removeComponents();
            for (DataArray component : inField.getComponents()) 
                pointField.addComponent(FieldPosition.interpolate(nodes, component));
        }
    }
    
    protected final void updateProbe()
    {
        probePosition();
        fireStateChanged(false);
    }
    
    @Override
    public void setInData(Field inField, DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
        this.inField = inField;
        if (inField instanceof RegularField) {
            irregularInField = null;
            regularInField = (RegularField)inField;
            dims = regularInField.getDims();
        }
        else {
            irregularInField = (IrregularField)inField;
            regularInField = null;
        }
        trueNSpace = inField.getTrueNSpace();
        
        glyph.setField(inField);
        if (trueNSpace == 2) {
            glyph.setType(PLANAR_LINE);
            glyph.setField(inField);
            glyph.getGlyph().setName("plane_point");
        }
        else {
            glyph.setType(LINE);
            glyph.setField(inField);
            glyph.getGlyph().setName("point");
        }
        if (pointField != null && mapParams != null)
            mapParams.setInData(pointField, (DataContainer)null);
    }

    @Override
    public IrregularField getSliceField() {
        return pointField;
    }

    public void setMapParams(DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
    }

    
    @Override
    public float[] getPlaneCenter()
    {
        return center;
    }
    
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return glyph;
    }
    
    public InteractiveGlyph getGlyph() {
        return glyph;
    }

    @Override
    public InteractiveGlyphGUI getGlyphGUI()
    {
        return glyphGUI;
    }
    
    @Override
    public void hide()
    {
        glyph.hide();
    }

    @Override
    public void show()
    {
         glyph.show();
    }
}
