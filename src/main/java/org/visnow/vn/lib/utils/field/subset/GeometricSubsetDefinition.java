/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset;

import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;

/**
 *
 * @author know
 */


public class GeometricSubsetDefinition {
    
    private GlyphType type = BOX;
    private float[] center  = {0, 0, 0};
    private float   radius  = 1;
    private float[] versorU = {1, 0, 0};
    private float[] versorV = {0, 1, 0};
    private float[] versorW = {0, 0, 1};
    private float   radiusU = 1;
    private float   radiusV = 1;
    private float   radiusW = 1;

    public GeometricSubsetDefinition(GlyphType type,
                                     float[] center,
                                     float   radius,
                                     float[] versorU,
                                     float[] versorV,
                                     float[] versorW,
                                     float   radiusU,
                                     float   radiusV,
                                     float   radiusW ) {
        this.type   = type;
        this.center = center;
        this.radius = radius;
        this.versorU = versorU;
        this.versorV = versorV;
        this.versorW = versorW;
        this.radiusU = radiusU;
        this.radiusV = radiusV;
        this.radiusW = radiusW;
    }

    public GlyphType getType() {
        return type;
    }

    public void setType(GlyphType type) {
        this.type = type;
    }
    
    public float[] getCenter() {
        return center;
    }

    public void setCenter(float[] center) {
        this.center = center;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float[] getVersorU() {
        return versorU;
    }

    public void setVersorU(float[] versorU) {
        this.versorU = versorU;
    }

    public float[] getVersorV() {
        return versorV;
    }

    public void setVersorV(float[] versorV) {
        this.versorV = versorV;
    }

    public float[] getVersorW() {
        return versorW;
    }

    public void setVersorW(float[] versorW) {
        this.versorW = versorW;
    }

    public float getRadiusU() {
        return radiusU;
    }

    public void setRadiusU(float radiusU) {
        this.radiusU = radiusU;
    }

    public float getRadiusV() {
        return radiusV;
    }

    public void setRadiusV(float radiusV) {
        this.radiusV = radiusV;
    }

    public float getRadiusW() {
        return radiusW;
    }

    public void setRadiusW(float radiusW) {
        this.radiusW = radiusW;
    }

    
}
