/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.ArrayList;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FldToPointFld
{
    private FldToPointFld()
    {
    }

    private static LargeArray select(LargeArray in, int vlen, long nOut, LogicLargeArray mask)
    {
        LargeArray out = LargeArrayUtils.create(in.getType(), vlen * nOut);
        for (long i = 0, iIn = 0, iOut = 0; i < in.length() / vlen; i++)
            if (mask.getBoolean(i)) {
                LargeArrayUtils.arraycopy(in, iIn, out, vlen * iOut, vlen);
                iIn += vlen;
                iOut += vlen;
            }
        return out;
    }

    private static TimeData select(TimeData in, int vlen, long nOut, LogicLargeArray mask)
    {
        TimeData out = new TimeData(in.getType());
        for (int i = 0; i < in.getNSteps(); i++)
            out.setValue(select(in.getValue(in.getTime(i)), vlen, nOut, mask), in.getTime(i));
        return out;
    }

    public static PointField convert(Field inField)
    {
        long nNodes = (int) inField.getNNodes();
        long nOutNodes = nNodes;
        LogicLargeArray mask = inField.getCurrentMask();
        boolean isMask = inField.hasMask();
        TimeData inTimeCoords = inField.getCoords();
        if (inTimeCoords == null) {
            FloatLargeArray inCoords = ((RegularField)inField).getCoordsFromAffine();
            ArrayList<Float> tSeries = new ArrayList<>();
            tSeries.add(0f);
            ArrayList<LargeArray> dSeries = new ArrayList<>();
            dSeries.add(inCoords);
            inTimeCoords = new TimeData(tSeries, dSeries, 0);
        }

        PointField outPointField = new PointField(nOutNodes);
        if (isMask) {
            outPointField.setCoords(select(inTimeCoords, 3, nOutNodes, mask));
            for (DataArray inDa : inField.getComponents()) {
                int vlen = inDa.getVectorLength();
                outPointField.addComponent(
                        DataArray.create(select(inDa.getTimeData(), vlen, nOutNodes, mask),
                                         vlen, inDa.getName()).unit(inDa.getUnit()).userData(inDa.getUserData()).
                                  preferredRanges(inDa.getPreferredMinValue(), inDa.getPreferredMaxValue(),
                                                  inDa.getPreferredPhysMinValue(), inDa.getPreferredPhysMaxValue()));
            }
        } else {
            outPointField.setCoords(inTimeCoords);
            for (DataArray inDa : inField.getComponents())
                outPointField.addComponent(inDa.cloneShallow());
        }
        outPointField.setCurrentTime(inField.getCurrentTime());
        return outPointField;
    }
}
