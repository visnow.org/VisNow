/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.slicing;

import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import static org.apache.commons.math3.util.FastMath.sqrt;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.interactiveGlyphs.GlyphGeometryParams;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.PLANE;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import org.visnow.vn.geometries.objects.IrregularFieldGeometry;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.utils.field.subset.subvolume.IrregularFieldSplitter;
import org.visnow.vn.lib.utils.field.subset.subvolume.RegularFieldSplitter;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class BasicPlanarSlice  extends Probe {

    protected boolean fastMode = false;
    protected Field inField = null;
    protected RegularField regularInField = null;
    protected IrregularField irregularInField = null;
    protected int[] dims;
    protected int nCells;
    protected float[] cellRadii;
    protected float[] cellCenters;
    protected boolean regularInFieldHasCoords = true;
    protected IrregularField sliceAreaField = null;
    protected float time = 0;

    protected FloatLargeArray coords;

    protected InteractiveGlyph       glyph       = new InteractiveGlyph(PLANE, false);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI    glyphGUI    = glyph.getComputeUI();
    protected GlyphGeometryParams    glyphGeometryParams;

    protected float[]                center = {0,0,0};
    protected float[]                coeffs = {0,0,0};
    protected float[]                u = {0,0,0};
    protected float[]                v = {0,0,0};

    protected IrregularField  sliceField;

    protected float[] centerShiftBounds = new float[] {-1, 1};

    protected PresentationParams presentationParams = new PresentationParams();
    protected IrregularFieldGeometry sliceGeometry;

    protected OpenBranchGroup probeGroup = new OpenBranchGroup();
    protected OpenBranchGroup probeParentGroup = new OpenBranchGroup();

    protected RenderingParams renderingParams;

    public BasicPlanarSlice(PresentationParams presentationParams)
    {
        glyph.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                updateSlice();
                show();
            }
        });
        sliceGeometry = new IrregularFieldGeometry(presentationParams);
        probeGroup.setName("probeGroup");
        probeParentGroup.setName("probeParentGroup");
        glyph.setName("probeGlyph");
        glyph.addGeometry(probeParentGroup);
    }

    public BasicPlanarSlice(PresentationParams presentationParams, boolean fastMode)
    {
        this(presentationParams);
        this.fastMode = fastMode;
    }

    public float[][] computeSliceUVExtents(float[] center, float[] U, float[] V)
    {
        float umin = Float.MAX_VALUE, umax = -Float.MAX_VALUE;
        float vmin = Float.MAX_VALUE, vmax = -Float.MAX_VALUE;
        float[] crds = sliceField.getCurrentCoords().getData();
        for (int i = 0; i < crds.length; i += 3) {
            float u = 0, v = 0;
            for (int j = 0; j < 3; j++) {
                u += U[j] * (crds[i + j] - center[j]);
                v += V[j] * (crds[i + j] - center[j]);
            }
            if (umin > u) umin = u;
            if (vmin > v) vmin = v;
            if (umax < u) umax = u;
            if (vmax < v) vmax = v;
        }
        return new float[][] {{umin, umax}, {vmin, vmax}, null};
    }

    protected final void updateSlice()
    {
        glyphGeometryParams = glyphParams.getGeometryParams();
        boolean positionChanged = false;
        for (int i = 0; i < 3; i++)
            if (center[i] != glyphGeometryParams.getCenter()[i] ||
                coeffs[i] != glyphGeometryParams.getW()[i] ||
                u[i] != glyphGeometryParams.getU()[i] ||
                v[i] != glyphGeometryParams.getV()[i]) {
                 positionChanged = true;
                 break;
            }
        System.arraycopy(glyphGeometryParams.getCenter(), 0, center, 0, 3);
        System.arraycopy(glyphGeometryParams.getW(),      0, coeffs, 0, 3);
        System.arraycopy(glyphGeometryParams.getU(),      0, u,      0, 3);
        System.arraycopy(glyphGeometryParams.getV(),      0, v,      0, 3);
        if (inField instanceof IrregularField)
            sliceField = IrregularFieldSplitter.sliceField(irregularInField,
                                                           dirComponent(coords.getData(), center, coeffs),
                                                           1, 0, coeffs);
        else
            sliceField = RegularFieldSplitter.sliceField(regularInField,
                                                         dirComponent(coords.getData(), center, coeffs),
                                                         1, 0, coeffs);
        if (sliceField == null)
            return;
        if (positionChanged)
            glyphParams.setMinMax(computeSliceUVExtents(center, u, v));
        sliceAreaField = clip(sliceField);
        if (sliceAreaField == null)
            return;
        fireStateChanged(glyph.isAdjusting());
    }

    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return glyph;
    }

    @Override
    public void hide()
    {
        glyph.hide();
    }

    @Override
    public void show()
    {
         updateCurrentProbeGeometry();
         glyph.show();
    }

    private class ComputeGeometryData implements Runnable {

        int nThreads = 1;
        int iThread = 0;
        FloatLargeArray coords;

        public ComputeGeometryData(int nThreads, int iThread, FloatLargeArray coords) {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.coords = coords;
        }

        @Override
        public void run() {
            float[] d = new float[7];
            int off0 = 3 * dims[0];
            int off1 = 3 * dims[1] * dims[0];
            for (int i = (iThread  * (dims[2] - 1)) / nThreads, p = i * (dims[1] - 1) * (dims[0] - 1);
                     i < ((iThread + 1) * (dims[2] - 1)) / nThreads;
                     i++)
                for (int j = 0; j < dims[1] - 1; j++)
                    for (int k = 0; k < dims[0] - 1; k++, p++) {
                        int l = 3 * ((i * dims[1] + j) * dims[0] + k);
                        Arrays.fill(d, 0);
                        try {
                            for (int m = 0; m < 3; m++) {
                            float v = coords.getFloat(l +                   m);
                            d[0]  += (coords.getFloat(l +               3 + m) - v) *
                                     (coords.getFloat(l +               3 + m) - v);
                            d[1]  += (coords.getFloat(l +        off0 +     m) - v) *
                                     (coords.getFloat(l +        off0 +     m) - v);
                            d[2]  += (coords.getFloat(l +        off0 + 3 + m) - v) *
                                     (coords.getFloat(l +        off0 + 3 + m) - v);
                            d[3]  += (coords.getFloat(l + off1 +            m) - v) *
                                     (coords.getFloat(l + off1 +            m) - v);
                            d[4]  += (coords.getFloat(l + off1 +        3 + m) - v) *
                                     (coords.getFloat(l + off1 +        3 + m) - v);
                            d[5]  += (coords.getFloat(l + off1 + off0 +     m) - v) *
                                     (coords.getFloat(l + off1 + off0 +     m) - v);
                            d[6]  += (coords.getFloat(l + off1 + off0 + 3 + m) - v) *
                                     (coords.getFloat(l + off1 + off0 + 3 + m) - v);
                            }
                            float r = 0;
                            for (int m = 0; m < d.length; m++)
                                if (r < d[m])
                                    r = d[m];
                            cellRadii[p] = (float) sqrt(r);
                        } catch (Exception e) {
                        }
                    }
        }
    }

/**
 * computes array of diameters of cells
 * Slicer uses these diameters to test if a cell can be nontrivially sliced
 */
    public void computeCellGeometryData() {
        dims = regularInField.getDims();
        if (!regularInFieldHasCoords)
            return;
        coords = regularInField.getCoords(0);
        nCells = 1;
        for (int i = 0; i < dims.length; i++)
            nCells *= dims[i] - 1;
        cellRadii = new float[nCells];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeGeometryData(nThreads, iThread, coords));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }

    protected IrregularField clip(IrregularField sliceField)
    {
        if (sliceField == null)
            return null;
        IrregularField tmp = clipOneDir(sliceField, center, u, glyphGeometryParams.getuRange()[1], false, coeffs);
        tmp =                clipOneDir(tmp,        center, u, glyphGeometryParams.getuRange()[0], true,  coeffs);
        tmp =                clipOneDir(tmp,        center, v, glyphGeometryParams.getvRange()[1], false, coeffs);
        return               clipOneDir(tmp,        center, v, glyphGeometryParams.getvRange()[0], true,  coeffs);
    }

    @Override
    public void setInData(Field inField, DataMappingParams dataMappingParams)
    {
        if (this.inField == inField)
            return;
        this.inField = inField;
        coords = null;
        if (inField instanceof RegularField) {
            regularInField = (RegularField)inField;
            dims = regularInField.getDims();
            regularInFieldHasCoords = regularInField.hasCoords();
            if (regularInFieldHasCoords) {
                computeCellGeometryData();
                coords = inField.getCoords(0);
            }
            else {
                coords = regularInField.getCoordsFromAffine();
            }
            irregularInField = null;
        }
        else {
            irregularInField = (IrregularField)inField;
            coords = inField.getCoords(0);
            regularInField = null;
        }
        glyph.setField(inField);
        glyph.getParams().setAxis(0);
        for (int i = 0; i < 3; i++)
            center[i] = coeffs[i] = u[i] = v[i] = 0;
        updateSlice();
    }

    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
    }

    public void setTime(float time)
    {
        this.time = time;
        sliceAreaField.setCurrentTime(time);
        sliceGeometry.updateColors();
    }

    private void updateCurrentProbeGeometry()
    {
        if (sliceAreaField == null)
            return;
        sliceGeometry.setField(sliceAreaField);
        if (probeGroup != null) {
            probeGroup.detach();
            probeGroup.removeAllChildren();
            probeGroup.addChild(sliceGeometry.getGeometry());
            probeParentGroup.addChild(probeGroup);
        }
    }

    @Override
    public IrregularField getSliceField()
    {
        return sliceField;
    }

    @Override
    public IrregularField getSliceAreaField()
    {
        return sliceAreaField;
    }

    public float[] getCoeffs()
    {
        return glyphGeometryParams.getW();
    }

    @Override
    public float[] getPlaneCenter()
    {
        return glyphGeometryParams.getCenter();
    }

    @Override
    public float[] getPlaneX()
    {
        return glyphGeometryParams.getU();
    }

    @Override
    public float[] getPlaneY()
    {
        return glyphGeometryParams.getV();
    }

    @Override
    public float getRX()
    {
        return  .5f * (glyphGeometryParams.getuRange()[1] - glyphGeometryParams.getuRange()[0]);
    }

    @Override
    public float getRY()
    {
        return .5f * (glyphGeometryParams.getvRange()[1] - glyphGeometryParams.getvRange()[0]);
    }

    public float[] getCenterShiftBounds()
    {
        return centerShiftBounds;
    }

    @Override
    public InteractiveGlyphGUI getGlyphGUI()
    {
        return glyphGUI;
    }

    public InteractiveGlyph getGlyph()
    {
        return glyph;
    }

    public OpenBranchGroup getProbeParentGroup()
    {
        return probeParentGroup;
    }

}
