/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.SliceUtils;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SliceRegularField
{

    private static final String[] coordNames
        = {
            "x", "y", "z"
        };

    /**
     * Creates a new instance of CropRegularField
     */
    public SliceRegularField()
    {
    }

    public static void sliceCoordsUpdate(RegularField inField, int axis, int slice, RegularField outField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis])
            return;
        int[] dims = inField.getDims();
        int[] outDims = outField.getDims();
        if (outDims == null || outDims.length != 2 || outDims[0] * outDims[1] * dims[axis] != (int) inField.getNNodes())
            return;
        if (!inField.hasCoords()) {
            float[][] inAffine = inField.getAffine();
            float[][] outAffine = new float[4][3];
            switch (axis) {
                case 0:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[0][i];
                        outAffine[0][i] = inAffine[1][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                    break;
                case 1:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[1][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                    break;
                case 2:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[2][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[1][i];
                        outAffine[2][i] = 0;
                    }
                    break;
            }
            outField.setAffine(outAffine);
        } else {
            outField.setCoords(inField.getCoords().get2DSlice(inField.getLDims(), axis, (long) slice, 3));
        }
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
    }

    /**
     * @param inField
     * @param axis
     * @param slice
     * @param copyPreferedRanges
     * @param outField
     *
     * @return false if slice is empty (empty outField should be set)
     */
    public static boolean sliceUpdate(RegularField inField, int axis, int slice, boolean copyPreferedRanges, RegularField outField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis] || outField == null) {
            return false;
        }
        long[] dims = inField.getLDims();
        long[] outDims = outField.getLDims();
        if (outDims == null || outDims.length != 2 || outDims[0] * outDims[1] * dims[axis] != inField.getNNodes())
            return false;
        LogicLargeArray validOut = null;
        if (inField.hasMask()) {
            long m = dims[0] * dims[1];
            long l = dims[0];
            LogicLargeArray mask = inField.getCurrentMask();
            switch (axis) {
                case 0:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice, dims[1], l, dims[2], m, 1);
                    break;
                case 1:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice * l, dims[0], 1, dims[2], m, 1);
                    break;
                case 2:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice * m, dims[0], 1, dims[1], l, 1);
                    break;
                default:
                    throw new IllegalArgumentException("axis has to be 0, 1, or 2");
            }
            outField.setCurrentMask(validOut);
        }
        sliceCoordsUpdate(inField, axis, slice, outField);
        for (int n = 0; n < inField.getNComponents(); n++) {
            DataArray inDataArr = inField.getComponent(n);
            DataArray outDataArr = inDataArr.get2DSlice(dims, axis, slice);
            if (copyPreferedRanges) {
                outDataArr.setPreferredRanges(inDataArr.getPreferredMinValue(), inDataArr.getPreferredMaxValue(), inDataArr.getPreferredPhysMinValue(), inDataArr.getPreferredPhysMaxValue());
            }
            outDataArr.setCurrentTime(inField.getCurrentTime());
            outField.setComponent(outDataArr, n);
        }

        return true;
    }

    public static RegularField sliceField(RegularField inField, int axis, int slice, boolean copyPreferedRanges)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis])
            return null;
        int[] dims = inField.getDims();
        int[] outDims = new int[2];
        if (axis == 0) {
            outDims[0] = dims[1];
            outDims[1] = dims[2];
        } else if (axis == 1) {
            outDims[0] = dims[0];
            outDims[1] = dims[2];
        } else {
            outDims[0] = dims[0];
            outDims[1] = dims[1];
        }
        RegularField outField = new RegularField(outDims);
        sliceUpdate(inField, axis, slice, copyPreferedRanges, outField);
        outField.setCurrentTime(inField.getCurrentTime());
        String[] coordsUnit = new String[3];
        System.arraycopy(inField.getCoordsUnits(), 0, coordsUnit, 0, 3);
        outField.setCoordsUnits(coordsUnit);
        return outField;
    }
    
    public static RegularField sliceField(RegularField inField, int axis, int slice)
    {
        return sliceField(inField, axis, slice, false);
    }
}
