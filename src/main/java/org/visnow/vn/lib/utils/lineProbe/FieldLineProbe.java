/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.lineProbe;


import java.util.ArrayList;
import java.util.Collections;
import org.apache.commons.lang3.ArrayUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.HEXAHEDRON;
import static org.visnow.jscic.cells.CellType.PRISM;
import static org.visnow.jscic.cells.CellType.PYRAMID;
import static org.visnow.jscic.cells.CellType.QUAD;
import org.visnow.jscic.cells.Hex;
import org.visnow.jscic.cells.Prism;
import org.visnow.jscic.cells.Pyramid;
import org.visnow.jscic.cells.Quad;
import org.visnow.jscic.cells.RegularHex;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class FieldLineProbe 
{

    protected IrregularField inIrregularField;
    protected RegularField inRegularField;
    protected Field inField;
    protected int[] dims;
    protected int nCellNodes;
    
    protected CellType cellType;
    protected FloatLargeArray crds;
    protected float[] p;
    protected float[] v;
    
    protected int index = 0;
    protected ArrayList<FieldPosition> nodes = new ArrayList<>();
    protected ArrayList<int[]> segments = new ArrayList<>();
    protected int nOutNodes;
    protected FieldPosition[] nodePositions = null;
    protected IrregularField outField;

    public FieldLineProbe(IrregularField inField, float[] center, float[] coeffs)
    {
        this.inField = inIrregularField = inField;
        inRegularField = null;
        crds = inField.getCoords(0);
        v = coeffs;
        p = center;
    }
    
    public FieldLineProbe(RegularField inField, float[] center, float[] coeffs)
    {
        this.inField = inRegularField = inField;
        inIrregularField = null;
        dims = inField.getDims();
        if (inField.hasCoords())
            crds = inField.getCoords(0);
        v = coeffs;
        p = center;
        switch (dims.length) {
            case 1:
                cellType = CellType.SEGMENT;
                break;
            case 2:
                cellType = CellType.QUAD;
                break;
            case 3:
                cellType = CellType.HEXAHEDRON;
                break;
        }
        nCellNodes = cellType.getNVertices();
    }

    @SuppressWarnings("unchecked")
    
    public void initCellSetSplit(CellSet trCS)
    {
        nodes.clear();
    }

    public void initCellArraySplit(CellArray ca)
    {
    }
    
    public FieldPosition processTriangle(int[] tr)
    {
        if (tr.length < 3)
            return null;
        if (tr[1] < tr[0])      {int i = tr[0]; tr[0] = tr[1]; tr[1] = i;}
        if (tr[2] < tr[0])      {int i = tr[2]; tr[2] = tr[1]; tr[1] = tr[0]; tr[0] = i;}
        else if (tr[2] < tr[1]) {int i = tr[2]; tr[2] = tr[1]; tr[1] = i;}
        float[]   q = new float[3];
        float[][] a = new float[3][3];
        float[]   b = new float[3];
        for (int i = 0; i < b.length; i++) {
            q[i]  = crds.getFloat(3 * tr[0] + i);
            b[i] = p[i] - q[i];
            a[i][0] = -v[i];
            a[i][1] = crds.getFloat(3 * tr[1] + i) - q[i];
            a[i][2] = crds.getFloat(3 * tr[2] + i) - q[i];
        }                // this is: p + t0*v = t1 * v1 + t2 * v2 + (1 - t1 - t2) * v0
                         // where v0, v1,v2 are triangle vertices
        float[] t;
        try {
            t = MatrixMath.lsolve3x3(a, b);
        } catch (IllegalArgumentException e) {
            return null; // unsolvable intersection equation 
        }
        if (t[1] <0 || t[2] < 0 || t[1] + t[2] > 1)
            return null; // intersection out of triangle
        return new FieldPosition(index, t[0], 
                                 new int[]{tr[0], tr[1], tr[2]}, 
                                 new float[]{1 - t[1] - t[2],  t[1],  t[2]});
    }
    
    public void processTetra(int[] tetra)
    {
        if (tetra.length < 4)
            return;
        int[] triangle = new int[3];
        int nIntersections = 0;
        FieldPosition[] intersections = new FieldPosition[4];
        for (int face = 0; face < 4; face++) {
            triangle[0] = face == 0 ? tetra[1] : tetra[0];
            triangle[1] = face <  2 ? tetra[2] : tetra[1];
            triangle[2] = face <  3 ? tetra[3] : tetra[2];
            intersections[nIntersections] = processTriangle(triangle);
            if (intersections[nIntersections] != null) 
                nIntersections += 1;
        }
        if (nIntersections <= 1)
            return;
        int ilow = 0, iup = 0;
        for (int i = 1; i < nIntersections; i++) {
            if (intersections[i].v < intersections[ilow].v)
                ilow = i;
            if (intersections[i].v > intersections[iup].v)
                iup = i;
        }  // intersections[ilow] - lowest intersection of line with tetra boundary, 
           // intersections[iup] - highest intersection of line with tetra boundary
        if (ilow == iup)
            return;
        segments.add(new int[] {index, index + 1});
        intersections[ilow].index = index;
        index += 1;
        nodes.add(intersections[ilow]);
        intersections[iup].index = index;
        index += 1;
        nodes.add(intersections[iup]);
    }
    
    public FieldPosition processSegment2D(int[] seg)
    {
        if (seg.length < 2)
            return null;
        if (seg[1] < seg[0])      {int i = seg[0]; seg[0] = seg[1]; seg[1] = i;}
        float[]   q = new float[2];
        float[][] a = new float[2][2];
        float[]   b = new float[2];
        for (int i = 0; i < b.length; i++) {
            q[i]  = crds.getFloat(3 * seg[0] + i);
            b[i] = p[i] - q[i];
            a[i][0] = -v[i];
            a[i][1] = crds.getFloat(3 * seg[1] + i) - q[i];
        }                // this is: p + t0*v = t1 * v1 + (1 - t1) * v0
                         // where v0, v1 are segment vertices
        float[] t;
        try {
            t = MatrixMath.lsolve2x2(a, b);
        } catch (IllegalArgumentException e) {
            return null; // unsolvable intersection equation 
        }
        if (t[1] <0 || t[1] > 1)
            return null; // intersection out of triangle
        return new FieldPosition(index, t[0], 
                                 new int[]{seg[0], seg[1]}, 
                                 new float[]{1 - t[1],  t[1]});
    }
    
    public void processTriangle2D(int[] triangle)
    {
        if (triangle.length != 3)
            return;
        int[] segment = new int[2];
        int nIntersections = 0;
        FieldPosition[] intersections = new FieldPosition[3];
        for (int face = 0; face < 3; face++) {
            segment[0] = face == 0 ? triangle[1] : triangle[0];
            segment[1] = face <  2 ? triangle[2] : triangle[1];
            intersections[nIntersections] = processSegment2D(segment);
            if (intersections[nIntersections] != null) 
                nIntersections += 1;
        }
        if (nIntersections <= 1)
            return;
        int ilow = 0, iup = 0;
        for (int i = 1; i < nIntersections; i++) {
            if (intersections[i].v < intersections[ilow].v)
                ilow = i;
            if (intersections[i].v > intersections[iup].v)
                iup = i;
        }  // intersections[ilow] - lowest intersection of line with tetra boundary, 
           // intersections[iup] - highest intersection of line with tetra boundary
        if (ilow == iup)
            return;
        segments.add(new int[] {index, index + 1});
        intersections[ilow].index = index;
        index += 1;
        nodes.add(intersections[ilow]);
        intersections[iup].index = index;
        index += 1;
        nodes.add(intersections[iup]);
    }
    
    public void processSimplex(int[] simplex)
    {
        switch (simplex.length) {
            case 4:
                processTetra(simplex);
                break;
            case 3:
                FieldPosition intersection = processTriangle(simplex);
                if (intersection != null) {
                    nodes.add(intersection);
                    index += 1;
                }
        }
    }
    
    public void processCell(int[] cell, CellType cellType)
    {
        if (cellType.isSimplex()) {
            int[] simplex = ArrayUtils.clone(cell);
            processSimplex(simplex);
        }
        else {
            int[][] cellTriangulation;
            switch (cellType) {
                case QUAD:
                    cellTriangulation = Quad.triangulationVertices(cell);
                    break;
                case PYRAMID:
                    cellTriangulation = Pyramid.triangulationVertices(cell);
                    break;
                case PRISM:
                    cellTriangulation = Prism.triangulationVertices(cell);
                    break;
                case HEXAHEDRON:
                    cellTriangulation = Hex.triangulationVertices(cell);
                    break;
                default:
                    return;
            }
            for (int[] simplex : cellTriangulation) {
                processSimplex(simplex);
            }
        }
    }
    
    public void processCell(int[] cell)
    {
        int[][] cellTriangulation = Quad.triangulationVertices(cell);
        for (int[] simplex : cellTriangulation) {
            processTriangle2D(simplex);
        }
    }
    
    public void processCell(int[] cell, boolean even)
    {
        int[][] cellTriangulation;
        switch (cellType) {
            case QUAD:
                cellTriangulation = Quad.triangulationVertices(cell);
                break;
            case HEXAHEDRON:
                cellTriangulation = RegularHex.triangulationVertices(cell, even);
                break;
            default:
                return;
        }
        for (int[] simplex : cellTriangulation) {
            processSimplex(simplex);
        }
    }
    
    public IrregularField createOutField()
    {
        if (nodes == null || nodes.isEmpty())
            return null;
        Collections.sort(nodes);
        int[] index = new int[nodes.size()];
        int[] reindex = new int[nodes.size()];
        index[0] = 0;
        reindex[nodes.get(0).index] = 0;
        float val = nodes.get(0).v;
        nOutNodes = 1;
        for (int i = 1; i < nodes.size(); i++) 
            if (nodes.get(i).v == val) {
                reindex[nodes.get(i).index] = reindex[nodes.get(i - 1).index];
                index[i] = -1;
            }
            else {
                val = nodes.get(i).v;
                reindex[nodes.get(i).index] = index[i] = nOutNodes;
                nOutNodes += 1;
            }
        outField = new IrregularField(nOutNodes);
        float[] outCrds = new float[3 * nOutNodes];
        float[] t = new float[nOutNodes];
        nodePositions = new FieldPosition[nOutNodes];
        for (int i = 0; i < index.length; i++) 
            if (index[i] > -1) {
                int l = index[i];
                FieldPosition intersection = nodePositions[l] = nodes.get(i);
                for (int j = 0; j < 3; j++) {
                    outCrds[3 * l + j] = intersection.v * v[j] + p[j];
                }
                t[l] = intersection.v;
            }
        outField.setCoords(new FloatLargeArray(outCrds), 0);
        int[] segNodes = new int[2 * segments.size()];
        for (int i = 0; i < segments.size(); i++) {
            segNodes[2 * i]     = reindex[segments.get(i)[0]];
            segNodes[2 * i + 1] = reindex[segments.get(i)[1]];
        }
        CellSet outCS = new CellSet("out");
        outCS.addCells(new CellArray(CellType.SEGMENT, segNodes, null, null));
        outCS.generateDisplayData(outField.getCoords(0));
        outField.addCellSet(outCS);
        for (DataArray inDA : inField.getComponents()) 
            if (inDA.isNumeric())
               outField.addComponent(FieldPosition.interpolate(nodePositions, inDA));
        if (inField.hasMask()) {
            LogicLargeArray inMask = inField.getCurrentMask();
            LogicLargeArray outMask = new LogicLargeArray(nOutNodes);
            for (int i = 0; i < nOutNodes; i++)
                outMask.setBoolean(i, nodePositions[i].isValid(inMask));
            outField.addMask(outMask);
        }
        outField.addComponent(DataArray.create(new FloatLargeArray(t), 1, LINE_SLICE_COORDS));
        if (inField instanceof RegularField) {
            int[] inDims = inRegularField.getDims();
            outField.addComponent(
                    DataArray.create(
                            FieldPosition.interpolateToIndices(nodePositions, inDims), 
                            inDims.length, INDEX_COORDS));
        }
        return outField;
    }

}
