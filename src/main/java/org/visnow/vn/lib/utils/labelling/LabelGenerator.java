/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.labelling;

import java.math.BigDecimal;
import org.apache.log4j.Logger;
import static org.visnow.jscic.utils.NumberUtils.*;

/**
 *
 * @author szpak
 */
public class LabelGenerator
{

    private static final Logger LOGGER = Logger.getLogger(LabelGenerator.class);

    /**
     * @return the most dense labelling
     */
    public static Labelling createLinearLabels(Number valueMin, Number valueMax, SpaceChecker spaceChecker, Formatter formatter)
    {
        if (!isSameClass(valueMin, valueMax)) throw new IllegalArgumentException("Min/max incompatible: " + valueMin.getClass() + " " + valueMax.getClass());
        else {
            BigDecimal max = toBigDecimal(valueMax);
            BigDecimal min = toBigDecimal(valueMin);
            BigDecimal range = max.subtract(min);

            if (range.compareTo(BigDecimal.ZERO) < 0)
                throw new IllegalArgumentException("min value cannot be larger than max value");
            else {
                Labelling firstLabelOnly = new Labelling(new double[]{0},
                                                         formatter.format(new Number[]{valueMin}, false),
                                                         formatter.format(new Number[]{valueMin}, true));
                Labelling noLabels = new Labelling(new double[]{}, new String[]{}, new String[]{});
                if (range.compareTo(BigDecimal.ZERO) == 0) { //precise equals
                    if (spaceChecker.isEnoughSpace(firstLabelOnly)) return firstLabelOnly;
                    else return noLabels;
                } else {
                    //case 0: just first/last labels
                    Labelling firstAndLastLabels = new Labelling(new double[]{0, 1},
                                                                 formatter.format(new Number[]{valueMin, valueMax}, false),
                                                                 formatter.format(new Number[]{valueMin, valueMax}, true));

                    if (!spaceChecker.isEnoughSpace(firstAndLastLabels))
                        if (spaceChecker.isEnoughSpace(firstLabelOnly)) return firstLabelOnly;
                        else return noLabels;
                    else {
                        //previous, more sparse labelling
                        Labelling lastGoodLabelling = firstAndLastLabels;

                        //labeling with steps in 1, 2, 5 multiplies of powers of 10.
                        int baseExponent = mostSignificantExponent(range);
                        int[] coefficientSteps = new int[]{5, 2, 1};
                        int stepIndex = 1;

                        //repeat until the most dense labelling is found
                        while (true && (isReal(valueMin) || baseExponent >= 0)) {
                            int step = coefficientSteps[stepIndex % coefficientSteps.length];
                            long[] gridRange = trimToGrid(ceil(min.divide(pow10(baseExponent))).longValue(),
                                                          floor(max.divide(pow10(baseExponent))).longValue(),
                                                          step);
                            //at least 3 labels                
                            if (gridRange != null && ((gridRange[1] - gridRange[0]) / step + 1) >= 3) {
                                double[] values = createDecimalGrid(baseExponent, gridRange[0], gridRange[1], step);
                                if (hasDuplicates(values)) return lastGoodLabelling; //avoid numeric problems on large Long
                                else {
                                    Labelling labelling = makeLabelling(values, convertToDouble(valueMin), convertToDouble(valueMax), 0, 1, ScaleType.LINEAR, isReal(valueMin), formatter);

                                    if (labelling.isUnique() && spaceChecker.isEnoughSpace(labelling))
                                        lastGoodLabelling = labelling;
                                    else return lastGoodLabelling;
                                }
                            }
                            stepIndex++;
                            if (stepIndex % coefficientSteps.length == 0) baseExponent--;
                        }
                        return lastGoodLabelling;
                    }
                }
            }
        }
    }

    public static Labelling createLogLabels(Number valueMin, Number valueMax, SpaceChecker spaceChecker, Formatter logarithmicFormatter)
    {
        if (!isSameClass(valueMin, valueMax)) throw new IllegalArgumentException("Min/max incompatible: " + valueMin.getClass() + " " + valueMax.getClass());
        else {
            BigDecimal max = toBigDecimal(valueMax);
            BigDecimal min = toBigDecimal(valueMin);
            BigDecimal range = max.subtract(min);
            if (min.compareTo(BigDecimal.ZERO) <= 0 || min.compareTo(max) > 0)
                throw new IllegalArgumentException("min value cannot be larger than max value, range must be positive");
            else {

                Labelling firstLabelOnly = new Labelling(new double[]{0},
                                                         logarithmicFormatter.format(new Number[]{valueMin}, false),
                                                         logarithmicFormatter.format(new Number[]{valueMin}, true));
                Labelling noLabels = new Labelling(new double[]{}, new String[]{}, new String[]{});
                if (range.compareTo(BigDecimal.ZERO) == 0) { //precise equals
                    if (spaceChecker.isEnoughSpace(firstLabelOnly)) return firstLabelOnly;
                    else return noLabels;
                } else {
                    //case 0: just first/last labels
                    Labelling firstAndLastLabels = new Labelling(new double[]{0, 1},
                                                                 logarithmicFormatter.format(new Number[]{valueMin, valueMax}, false),
                                                                 logarithmicFormatter.format(new Number[]{valueMin, valueMax}, true));

                    if (!spaceChecker.isEnoughSpace(firstAndLastLabels))
                        if (spaceChecker.isEnoughSpace(firstLabelOnly)) return firstLabelOnly;
                        else return noLabels;
                    else {
                        //previous, more sparse labelling
                        Labelling lastGoodLabelling = firstAndLastLabels;

                        double valueMinLog10 = Math.log10(convertToDouble(valueMin));
                        double valueMaxLog10 = Math.log10(convertToDouble(valueMax));

                        int exponentMin = ceilLog10(min);
                        int exponentMax = floorLog10(max);

                        BigDecimal powerMin = pow10(exponentMin);
                        BigDecimal powerMax = pow10(exponentMax);

                        int powersInRange = exponentMax - exponentMin + 1;

                        double[] powerPositions = new double[powersInRange];

                        for (int i = exponentMin; i <= exponentMax; i++)
                            powerPositions[i - exponentMin] = clip((i - valueMinLog10) / (valueMaxLog10 - valueMinLog10), 0, 1);

                        //case 1: powers of 10 (less .. more dense)
                        if (powersInRange >= 3) {
                            for (int powerStep = Integer.highestOneBit(powersInRange - 1) >> 1; powerStep >= 1; powerStep >>= 1) {
                                double[] values = new double[(powersInRange - 1) / powerStep + 1];
                                int vi = 0;
                                for (int i = exponentMin; i <= exponentMax; i += powerStep) //TODO: formatting
                                    values[vi++] = Math.pow(10, i);

                                Labelling labelling = makeLabelling(values, convertToDouble(valueMin), convertToDouble(valueMax), 0, 1, ScaleType.LOGARITHMIC, isReal(valueMin), logarithmicFormatter);

                                if (labelling.isUnique() && spaceChecker.isEnoughSpace(labelling)) lastGoodLabelling = labelling;
                                else return lastGoodLabelling;
                            }
                        }

                        //case 2: all powers of 10 + sublabels (1,2,5,10)
                        if (powersInRange >= 1) {
                            int prefixLabels = min.compareTo(powerMin.movePointLeft(1).multiply(TWO)) <= 0 ? 2
                                    : min.compareTo(powerMin.movePointLeft(1).multiply(FIVE)) <= 0 ? 1 : 0;
                            int suffixLabels = max.compareTo(powerMax.multiply(FIVE)) >= 0 ? 2
                                    : max.compareTo(powerMax.multiply(TWO)) >= 0 ? 1 : 0;
                            int totalLabels = (powersInRange - 1) * 3 + 1 + prefixLabels + suffixLabels;

                            double[] values = new double[totalLabels];

                            int i = prefixLabels - 3; //skip beginning

                            //all elements together (prefix, inner part, suffix)
                            for (int p = exponentMin - 1; p <= exponentMax; p++)
                                for (int c : new int[]{1, 2, 5}) {
                                    if (i >= 0 && i < totalLabels)
                                        values[i] = Math.pow(10, p) * c;
                                    i++;
                                }

                            Labelling labelling = makeLabelling(values, convertToDouble(valueMin), convertToDouble(valueMax), 0, 1, ScaleType.LOGARITHMIC, isReal(valueMin), logarithmicFormatter);

                            if (labelling.isUnique() && spaceChecker.isEnoughSpace(labelling)) lastGoodLabelling = labelling;
                            else return lastGoodLabelling;
                        }

//                        return lastGoodLabelling;
                        //case 3: all powers + linear range in the middle
                        int[] coefficientSteps = new int[]{5, 2, 1};
                        int currentStep = 2;

                        int densityExponent = 0;

                        //repeat until the most dense labelling is found
                        while (true && ((isReal(valueMin) || exponentMin - densityExponent >= 0))) {
                            long density = Math.round(Math.pow(10, densityExponent));
                            long coefficient = coefficientSteps[currentStep % coefficientSteps.length];

                            //XXX: apparently this starts with empty labelling (which should not be created ....) - especially when powersInRange == 0
                            //TODO: test it... shouldnt be the case
//                            Sublabelling labelling = new Sublabelling(0);
                            double[] values = new double[]{};

                            long prefixMin = ceil(min.divide(powerMin).multiply(toBigDecimal(density * 10))).longValue();
                            long suffixMax = floor(max.divide(powerMax).multiply(toBigDecimal(density))).longValue();

                            if (powersInRange == 0) { //"prefix" only
                                long[] prefixRange = trimToGrid(prefixMin, suffixMax, coefficient);
//                                        ceil(min.divide(powerMin).multiply(toBigDecimal(density * 10))).longValue(),
//                                                                floor(max.divide(powerMin).multiply(toBigDecimal(density * 10))).longValue(),
//                                                                coefficient);
                                if (prefixRange != null)
                                    values = createDecimalGrid(exponentMin - 1 - densityExponent, prefixRange[0], prefixRange[1], coefficient);
                            } else {
                                long[] prefixRange = trimToGrid(prefixMin, density * 10 - 1, coefficient);
//                                        (long) Math.ceil(density * 10 * valueMin / powerMin), density * 10 - 1, coefficient);
                                long[] innerRange = new long[]{density, density * 10 - coefficient};
                                long[] suffixRange = trimToGrid(density, suffixMax, coefficient);
//                                                                (int) Math.floor(density * valueMax / powerMax), coefficient);
                                int totalLabels = (int) (((powersInRange - 1) * density * 9) / coefficient);
                                if (prefixRange != null) totalLabels += (prefixRange[1] - prefixRange[0]) / coefficient + 1;
                                if (suffixRange != null) totalLabels += (suffixRange[1] - suffixRange[0]) / coefficient + 1;

                                values = new double[totalLabels];
                                int vi = 0;

                                //prefix
                                if (prefixRange != null) {
                                    double[] prefixValues = createDecimalGrid(exponentMin - 1 - densityExponent, prefixRange[0], prefixRange[1], coefficient);
                                    System.arraycopy(prefixValues, 0, values, vi, prefixValues.length);
                                    vi += prefixValues.length;
                                }
                                //inner part
                                for (int i = exponentMin; i < exponentMax; i++) {
                                    double[] innerValues = createDecimalGrid(i - densityExponent, innerRange[0], innerRange[1], coefficient);
                                    System.arraycopy(innerValues, 0, values, vi, innerValues.length);
                                    vi += innerValues.length;
                                }

                                //suffix
                                if (suffixRange != null) {
                                    double[] suffixValues = createDecimalGrid(exponentMax - densityExponent, suffixRange[0], suffixRange[1], coefficient);
                                    System.arraycopy(suffixValues, 0, values, vi, suffixValues.length);
                                    vi += suffixValues.length;
                                }
                            }

                            Labelling labelling = makeLabelling(values, convertToDouble(valueMin), convertToDouble(valueMax), 0, 1, ScaleType.LOGARITHMIC, isReal(valueMin), logarithmicFormatter);

                            if (labelling.isUnique() && spaceChecker.isEnoughSpace(labelling)) lastGoodLabelling = labelling;
                            else return lastGoodLabelling;

                            if (++currentStep % coefficientSteps.length == 0) densityExponent++;
                        }

                        return lastGoodLabelling;
                    }
                }
            }
        }
    }

    private static boolean hasDuplicates(double[] values)
    {
        for (int i = 1; i < values.length; i++)
            if (values[i] == values[i - 1]) return true;
        return false;
    }

    private static int[] findDecimalRangeCoefficients(double rangeMin, double rangeMax, int exponent, int coefficientStep)
    {
        if (coefficientStep != 1 && coefficientStep != 2 && coefficientStep != 5) throw new IllegalArgumentException("Coefficient step must be 1, 2 or 5");
        if (rangeMin >= rangeMax) throw new IllegalArgumentException("Wrong range: " + rangeMin + " " + rangeMax);

        double decimalBase = Math.pow(10, exponent) * coefficientStep;

        double firstPosition = Math.ceil(rangeMin / decimalBase) * coefficientStep;
        double lastPosition = Math.floor(rangeMax / decimalBase) * coefficientStep;

        if (lastPosition < firstPosition) return null;
        else
            return new int[]{(int) Math.round(firstPosition), (int) Math.round(lastPosition)};

    }

    private enum ScaleType
    {
        LINEAR, LOGARITHMIC;
    }

//    private static class Sublabelling
//    {
//        double[] positions;
//        String[] labels;
//        String[] labelsFull;
//
//        int positionIndex = 0;
//        int labelIndex = 0;
//
//        public Sublabelling(int capacity)
//        {
//            positions = new double[capacity];
//            labels = new String[capacity];
//        }
//
//        public void putNext(String label, String labelFull, double position)
//        {
//            labels[labelIndex] = label;
//            labelsFull[labelIndex++] = labelFull;
//            positions[positionIndex++] = position;
//        }
//
//        public void putNext(Sublabelling l)
//        {
//            for (int i = 0; i < l.positions.length; i++) {
//                labels[labelIndex] = l.labels[i];
//                labelsFull[labelIndex++] = l.labelsFull[i];
//                positions[positionIndex++] = l.positions[i];
//            }
//        }
//
//        public Sublabelling(double[] positions, String[] labels, String[] labelsFull)
//        {
//            this.positions = positions;
//            this.labels = labels;
//            this.labelsFull = labelsFull;
//        }
//    }
    private static Labelling makeLabelling(double[] values, double rangeInMin, double rangeInMax, double rangeOutMin, double rangeOutMax, ScaleType scaleType, boolean isReal, Formatter formatter)
    {
        if (isReal)
            return new Labelling(mapIntoRange(values, rangeInMin, rangeInMax, rangeOutMin, rangeOutMax, scaleType),
                                 formatter.format(box(values), false),
                                 formatter.format(box(values), true));
        else
            return new Labelling(mapIntoRange(values, rangeInMin, rangeInMax, rangeOutMin, rangeOutMax, scaleType),
                                 formatter.format(convertToLong(box(values)), false),
                                 formatter.format(convertToLong(box(values)), true));
    }

    private static double[] createDecimalGrid(int exponent, long firstCoefficient, long lastCoefficient, long coefficientStep)
    {
        if (exponent < -308 || exponent > 308) throw new IllegalArgumentException("Decimal density extends double precision: " + exponent);
        if (firstCoefficient > lastCoefficient) throw new IllegalArgumentException("Wrong labelling order: " + firstCoefficient + " > " + lastCoefficient);
        if (coefficientStep <= 0) throw new IllegalArgumentException("Only positive steps are allowed");

        int coefficientRange = (int) (lastCoefficient - firstCoefficient);
        if (coefficientRange % coefficientStep != 0)
            throw new IllegalArgumentException("Coefficient step not compatible with first/last coefficient: " + firstCoefficient + ", " + lastCoefficient + ", " + coefficientStep);

        int totalValues = (int) ((coefficientRange / coefficientStep) + 1);

        double[] values = new double[totalValues];
        int i = 0;

        for (long c = firstCoefficient; c <= lastCoefficient; c += coefficientStep)
            values[i++] = c * Math.pow(10, exponent);

        return values;
    }

//    private static Labelling createDecimalLabels(int exponent, long firstCoefficient, long lastCoefficient, long coefficientStep,
//                                                 double rangeInStart, double rangeInEnd, double rangeOutStart, double rangeOutEnd, ScaleType rangeInType,
//                                                 Formatter formatter)
//    {
//        if (exponent < -308 || exponent > 308) throw new IllegalArgumentException("Decimal density extends double precision: " + exponent);
//        if (firstCoefficient > lastCoefficient) throw new IllegalArgumentException("Wrong labelling order: " + firstCoefficient + " > " + lastCoefficient);
//        if (coefficientStep <= 0) throw new IllegalArgumentException("Only positive steps are allowed");
//
//        int coefficientRange = (int) (lastCoefficient - firstCoefficient);
//        if (coefficientRange % coefficientStep != 0)
//            throw new IllegalArgumentException("Coefficient step not compatible with first/last coefficient: " + firstCoefficient + ", " + lastCoefficient + ", " + coefficientStep);
//
//        int totalLabels = (int) ((coefficientRange / coefficientStep) + 1);
//
//        if (rangeInStart > rangeInEnd || rangeOutStart > rangeOutEnd)
//            throw new IllegalArgumentException("Incorrect in/out ranges: " + rangeInStart + " ? " + rangeInEnd + ", " + rangeOutStart + " ? " + rangeOutEnd);
//
//        if (rangeInType == ScaleType.LOGARITHMIC && rangeInStart <= 0)
//            throw new IllegalArgumentException("Incorrect logarithmic range: " + rangeInStart);
//
//        Sublabelling labelling = new Sublabelling(totalLabels);
//
//        for (long c = firstCoefficient; c <= lastCoefficient; c += coefficientStep)
//            labelling.putNext(formatter.format(exponent, c, firstCoefficient, lastCoefficient),
//                              Math.min(rangeOutEnd, Math.max(rangeOutStart, mapIntoRange(c * Math.pow(10, exponent), rangeInStart, rangeInEnd, rangeOutStart, rangeOutEnd, rangeInType))));
//
//        return labelling;
//    }
    /**
     * @return maximum subrange with endpoints at multiple of gridSpacing or null if no gridSpacing multiple exists in [rangeStart, rangeEnd].
     */
    public static long[] trimToGrid(long rangeStart, long rangeEnd, long gridSpacing)
    {
        //ceil (with step = gridSpacing)
        long start = rangeStart > 0 ? ((rangeStart - 1) / gridSpacing + 1) * gridSpacing : rangeStart / gridSpacing * gridSpacing;
        //floor (with step = gridSpacing)
        long end = rangeEnd > 0 ? rangeEnd / gridSpacing * gridSpacing : (rangeEnd - gridSpacing + 1) / gridSpacing * gridSpacing;

        if (start <= end) return new long[]{start, end};
        else return null;
    }

    private static double[] mapIntoRange(double[] values, double rangeInStart, double rangeInEnd, double rangeOutStart, double rangeOutEnd, ScaleType rangeInType)
    {
        double[] mapped = new double[values.length];
        for (int i = 0; i < values.length; i++)
            mapped[i] = mapIntoRange(values[i], rangeInStart, rangeInEnd, rangeOutStart, rangeOutEnd, rangeInType);
        return mapped;
    }

    /**
     * Maps linearly or logarithmically value from [rangeInStart, rangeInEnd] into [rangeOutStart, rangeOutEnd]. Result is always limited to [rangeOutStart,
     * rangeOutEnd].
     * <p>
     * This method assumes that in and out ranges are not empty and properly oredered (start &lt; end).
     */
    private static double mapIntoRange(double value, double rangeInStart, double rangeInEnd, double rangeOutStart, double rangeOutEnd, ScaleType rangeInType)
    {
        if (rangeInType == ScaleType.LINEAR)
            return clip((value - rangeInStart) / (rangeInEnd - rangeInStart) * (rangeOutEnd - rangeOutStart) + rangeOutStart, rangeOutStart, rangeOutEnd);
        else if (rangeInType == ScaleType.LOGARITHMIC)
            return clip((Math.log(value) - Math.log(rangeInStart)) / (Math.log(rangeInEnd) - Math.log(rangeInStart)) * (rangeOutEnd - rangeOutStart) + rangeOutStart, rangeOutStart, rangeOutEnd);
        else throw new UnsupportedOperationException("Scale not supported: " + rangeInType.name());

    }

    public interface Formatter
    {
        public String[] format(Number[] numbersAscending, boolean fullPrecision);
    }

    public interface SpaceChecker
    {
        public boolean isEnoughSpace(Labelling labelling);
    }

    public static void main(String[] args)
    {
//        System.out.println(simpleFormatter.format(0, 1000000000, 1000000000, 1000000001));
//        System.out.println(simpleFormatter.format(0, 1000000001, 1000000000, 1000000001));
    }
}
