/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class ExtractCellSets
{

    public static IrregularField extractCellSets(IrregularField in)
    {
        boolean[] vpts = new boolean[(int) in.getNNodes()];
        int[] vptinds = new int[(int) in.getNNodes()];
        for (int i = 0; i < vptinds.length; i++) {
            vptinds[i] = -1;
            vpts[i] = false;
        }
        for (CellSet s : in.getCellSets())
            if (s.isSelected()) {
                for (CellArray a : s.getCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        for (int i = 0; i < nodes.length; i++)
                            vpts[nodes[i]] = true;
                    }
                for (CellArray a : s.getBoundaryCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        for (int i = 0; i < nodes.length; i++)
                            vpts[nodes[i]] = true;
                    }
            }
        int nOut = 0;
        for (int i = 0; i < vptinds.length; i++)
            if (vpts[i]) {
                vptinds[i] = nOut;
                nOut += 1;
            }
        if (nOut == 0)
            return null;
        IrregularField out = new IrregularField(nOut);
        out.setCurrentCoords(in.getCurrentCoords() == null ? null : (FloatLargeArray)arrayCompact(in.getCurrentCoords(), 3, nOut, vpts));
        for (DataArray da : in.getComponents()) {
            TimeData inTD = da.getTimeData();
            TimeData outTD = new TimeData(da.getType());
            for (int i = 0; i < inTD.getNSteps(); i++)
                outTD.setValue(arrayCompact(inTD.getValue(inTD.getTime(i)), 
                                            da.getVectorLength(), nOut, vpts), inTD.getTime(i));
            out.addComponent(DataArray.create(outTD, da.getVectorLength(), da.getName(), da.getUnit(), da.getUserData()));
        }
            
        for (CellSet s : in.getCellSets())
            if (s.isSelected()) {
                CellSet outS = new CellSet(s.getName());
                for (DataArray component : s.getComponents()) 
                    outS.addComponent(component.cloneShallow());
                for (CellArray a : s.getCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        int[] outNodes = new int[nodes.length];
                        for (int i = 0; i < nodes.length; i++)
                            outNodes[i] = vptinds[nodes[i]];
                        CellArray outA = new CellArray(a.getType(), outNodes, a.getOrientations(), a.getDataIndices());
                        outS.setCellArray(outA);
                    }
                for (CellArray a : s.getBoundaryCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        int[] outNodes = new int[nodes.length];
                        for (int i = 0; i < nodes.length; i++)
                            outNodes[i] = vptinds[nodes[i]];
                        CellArray outA = new CellArray(a.getType(), outNodes, a.getOrientations(), a.getDataIndices());
                        outS.setBoundaryCellArray(outA);
                    }
                out.addCellSet(outS);
            }
        return out;
    }
    
    public static LargeArray arrayCompact(LargeArray data, int vlen, int outLen, boolean[] v)
    {
        long n = data.length() / vlen;
        if (n != v.length)
            return null;
        LargeArray out = LargeArrayUtils.create(data.getType(), outLen * vlen);
        for (int i = 0, j = 0; i < n; i++)
            if (v[i]) {
                LargeArrayUtils.arraycopy(data, vlen * i, out, j, vlen);
                j += vlen;
            }
        return out;
    }

    private ExtractCellSets()
    {
    }

}
