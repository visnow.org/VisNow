/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import static org.apache.commons.math3.util.FastMath.random;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author know
 */
public class TestFrame extends javax.swing.JFrame
{

    RegularField outField0, outField1, outField2;
    RegularField[] flds;
    SeedPointParams seedPointParams = new SeedPointParams();
    
    /**
     * Creates new form TestFrame
     */
    public TestFrame()
    {
        initComponents();
        seedPointsGUI1.setSeedParams(seedPointParams);
        seedPointParams.setInputField(createRegularTestField(new int[] {100,100,100}));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        GridBagConstraints gridBagConstraints;

        jPanel1 = new JPanel();
        jPanel3 = new JPanel();
        jPanel4 = new JPanel();
        iLabel = new JLabel();
        iSpinner = new JSpinner();
        jLabel = new JLabel();
        jSpinner = new JSpinner();
        kLabel = new JLabel();
        kSpinner = new JSpinner();
        jPanel5 = new JPanel();
        jLabel2 = new JLabel();
        nNodesField = new JTextField();
        jPanel6 = new JPanel();
        b1d = new JButton();
        b2d = new JButton();
        b3d = new JButton();
        irr = new JButton();
        jPanel11 = new JPanel();
        jPanel12 = new JPanel();
        nullAuxFieldButton = new JButton();
        b1d2 = new JButton();
        b2d2 = new JButton();
        b3d2 = new JButton();
        irr2 = new JButton();
        jPanel13 = new JPanel();
        jLabel4 = new JLabel();
        nNodesField2 = new JTextField();
        jPanel14 = new JPanel();
        iLabel2 = new JLabel();
        iSpinner2 = new JSpinner();
        jLabel5 = new JLabel();
        jSpinner2 = new JSpinner();
        kLabel2 = new JLabel();
        kSpinner2 = new JSpinner();
        jPanel15 = new JPanel();
        jPanel2 = new JPanel();
        jLabel6 = new JLabel();
        preferredField = new JTextField();
        jPanel16 = new JPanel();
        jLabel7 = new JLabel();
        maxField = new JTextField();
        jPanel17 = new JPanel();
        jLabel8 = new JLabel();
        minField = new JTextField();
        seedPointsGUI1 = new SeedPointsGUI();
        jPanel7 = new JPanel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new GridBagLayout());

        jPanel1.setBorder(BorderFactory.createTitledBorder("inField"));
        jPanel1.setLayout(new GridBagLayout());

        jPanel3.setLayout(new GridBagLayout());

        jPanel4.setMinimumSize(new Dimension(192, 24));
        jPanel4.setPreferredSize(new Dimension(192, 24));
        jPanel4.setLayout(new GridBagLayout());

        iLabel.setText("I:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel4.add(iLabel, gridBagConstraints);

        iSpinner.setModel(new SpinnerNumberModel(100, 50, null, 50));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel4.add(iSpinner, gridBagConstraints);

        jLabel.setText("J:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel4.add(jLabel, gridBagConstraints);

        jSpinner.setModel(new SpinnerNumberModel(200, 100, null, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel4.add(jSpinner, gridBagConstraints);

        kLabel.setText("K:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel4.add(kLabel, gridBagConstraints);

        kSpinner.setModel(new SpinnerNumberModel(100, 2, null, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        jPanel4.add(kSpinner, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.4;
        jPanel3.add(jPanel4, gridBagConstraints);

        jPanel5.setLayout(new GridLayout(1, 0));

        jLabel2.setText("nNodes");
        jPanel5.add(jLabel2);

        nNodesField.setText("1000000");
        nNodesField.setMinimumSize(new Dimension(100, 24));
        nNodesField.setName(""); // NOI18N
        nNodesField.setPreferredSize(new Dimension(53, 24));
        jPanel5.add(nNodesField);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel3.add(jPanel5, gridBagConstraints);

        jPanel6.setLayout(new GridLayout(1, 0));

        b1d.setText("1d");
        b1d.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b1dActionPerformed(evt);
            }
        });
        jPanel6.add(b1d);

        b2d.setText("2d");
        b2d.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b2dActionPerformed(evt);
            }
        });
        jPanel6.add(b2d);

        b3d.setText("3d");
        b3d.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b3dActionPerformed(evt);
            }
        });
        jPanel6.add(b3d);

        irr.setText("irreg");
        irr.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                irrActionPerformed(evt);
            }
        });
        jPanel6.add(irr);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel3.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel11.setBorder(BorderFactory.createTitledBorder("auxFIeld"));
        jPanel11.setLayout(new GridLayout(3, 0));

        jPanel12.setLayout(new GridLayout(1, 0));

        nullAuxFieldButton.setText("null");
        nullAuxFieldButton.setMargin(new Insets(2, 2, 2, 2));
        nullAuxFieldButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                nullAuxFieldButtonActionPerformed(evt);
            }
        });
        jPanel12.add(nullAuxFieldButton);

        b1d2.setText("1d");
        b1d2.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b1d2ActionPerformed(evt);
            }
        });
        jPanel12.add(b1d2);

        b2d2.setText("2d");
        b2d2.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b2d2ActionPerformed(evt);
            }
        });
        jPanel12.add(b2d2);

        b3d2.setText("3d");
        b3d2.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                b3d2ActionPerformed(evt);
            }
        });
        jPanel12.add(b3d2);

        irr2.setText("irreg");
        irr2.setMargin(new Insets(2, 2, 2, 2));
        irr2.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                irr2ActionPerformed(evt);
            }
        });
        jPanel12.add(irr2);

        jPanel11.add(jPanel12);

        jPanel13.setLayout(new GridLayout(1, 0));

        jLabel4.setText("nNodes");
        jPanel13.add(jLabel4);

        nNodesField2.setText("1000000");
        jPanel13.add(nNodesField2);

        jPanel11.add(jPanel13);

        jPanel14.setLayout(new GridBagLayout());

        iLabel2.setText("I:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel14.add(iLabel2, gridBagConstraints);

        iSpinner2.setModel(new SpinnerNumberModel(100, 50, null, 50));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel14.add(iSpinner2, gridBagConstraints);

        jLabel5.setText("J:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel14.add(jLabel5, gridBagConstraints);

        jSpinner2.setModel(new SpinnerNumberModel(200, 100, null, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel14.add(jSpinner2, gridBagConstraints);

        kLabel2.setText("K:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 7, 0, 5);
        jPanel14.add(kLabel2, gridBagConstraints);

        kSpinner2.setModel(new SpinnerNumberModel(100, 2, null, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        jPanel14.add(kSpinner2, gridBagConstraints);

        jPanel11.add(jPanel14);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel1.add(jPanel11, gridBagConstraints);

        jPanel15.setBorder(BorderFactory.createEtchedBorder());
        jPanel15.setLayout(new GridLayout(3, 0));

        jPanel2.setLayout(new GridLayout(1, 0));

        jLabel6.setText("preferred");
        jPanel2.add(jLabel6);

        preferredField.setText("10000");
        preferredField.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                preferredFieldActionPerformed(evt);
            }
        });
        jPanel2.add(preferredField);

        jPanel15.add(jPanel2);

        jPanel16.setLayout(new GridLayout(1, 0));

        jLabel7.setText("max");
        jPanel16.add(jLabel7);

        maxField.setText("10000000");
        maxField.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                maxFieldActionPerformed(evt);
            }
        });
        jPanel16.add(maxField);

        jPanel15.add(jPanel16);

        jPanel17.setLayout(new GridLayout(1, 0));

        jLabel8.setText("min");
        jPanel17.add(jLabel8);

        minField.setText("10");
        minField.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                minFieldActionPerformed(evt);
            }
        });
        jPanel17.add(minField);

        jPanel15.add(jPanel17);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jPanel15, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel1.add(seedPointsGUI1, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.1;
        getContentPane().add(jPanel1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.9;
        getContentPane().add(jPanel7, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b1dActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_b1dActionPerformed
    {//GEN-HEADEREND:event_b1dActionPerformed
        seedPointParams.setInputField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue()}));
    }//GEN-LAST:event_b1dActionPerformed

    private void b2dActionPerformed(ActionEvent evt)//GEN-FIRST:event_b2dActionPerformed
    {//GEN-HEADEREND:event_b2dActionPerformed
        seedPointParams.setInputField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue(),
            (Integer)jSpinner.getValue()}));
    }//GEN-LAST:event_b2dActionPerformed

    private void b3dActionPerformed(ActionEvent evt)//GEN-FIRST:event_b3dActionPerformed
    {//GEN-HEADEREND:event_b3dActionPerformed
        seedPointParams.setInputField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue(),
            (Integer)jSpinner.getValue(),
            (Integer)kSpinner.getValue()}));
    }//GEN-LAST:event_b3dActionPerformed

    private void irrActionPerformed(ActionEvent evt)//GEN-FIRST:event_irrActionPerformed
    {//GEN-HEADEREND:event_irrActionPerformed
        seedPointParams.setInputField(createIrregularTestField(Integer.parseInt(nNodesField.getText())));
    }//GEN-LAST:event_irrActionPerformed

    private void nullAuxFieldButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_nullAuxFieldButtonActionPerformed
    {//GEN-HEADEREND:event_nullAuxFieldButtonActionPerformed
        seedPointParams.setAuxField(null);
    }//GEN-LAST:event_nullAuxFieldButtonActionPerformed

    private void b1d2ActionPerformed(ActionEvent evt)//GEN-FIRST:event_b1d2ActionPerformed
    {//GEN-HEADEREND:event_b1d2ActionPerformed
        seedPointParams.setAuxField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue()}));
    }//GEN-LAST:event_b1d2ActionPerformed

    private void b2d2ActionPerformed(ActionEvent evt)//GEN-FIRST:event_b2d2ActionPerformed
    {//GEN-HEADEREND:event_b2d2ActionPerformed
        seedPointParams.setAuxField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue(),
            (Integer)jSpinner.getValue()}));
    }//GEN-LAST:event_b2d2ActionPerformed

    private void b3d2ActionPerformed(ActionEvent evt)//GEN-FIRST:event_b3d2ActionPerformed
    {//GEN-HEADEREND:event_b3d2ActionPerformed
        seedPointParams.setAuxField(createRegularTestField(new int[]{
            (Integer)iSpinner.getValue(),
            (Integer)jSpinner.getValue(),
            (Integer)kSpinner.getValue()}));
    }//GEN-LAST:event_b3d2ActionPerformed

    private void irr2ActionPerformed(ActionEvent evt)//GEN-FIRST:event_irr2ActionPerformed
    {//GEN-HEADEREND:event_irr2ActionPerformed
        seedPointParams.setAuxField(createIrregularTestField(Integer.parseInt(nNodesField.getText())));
    }//GEN-LAST:event_irr2ActionPerformed

    private void preferredFieldActionPerformed(ActionEvent evt)//GEN-FIRST:event_preferredFieldActionPerformed
    {//GEN-HEADEREND:event_preferredFieldActionPerformed
        setResultSizes();
    }//GEN-LAST:event_preferredFieldActionPerformed

    private void maxFieldActionPerformed(ActionEvent evt)//GEN-FIRST:event_maxFieldActionPerformed
    {//GEN-HEADEREND:event_maxFieldActionPerformed
        setResultSizes();
    }//GEN-LAST:event_maxFieldActionPerformed

    private void minFieldActionPerformed(ActionEvent evt)//GEN-FIRST:event_minFieldActionPerformed
    {//GEN-HEADEREND:event_minFieldActionPerformed
        setResultSizes();
    }//GEN-LAST:event_minFieldActionPerformed

    private void setResultSizes()
    {
        try {
            seedPointParams.getDownsizeParams().setSizes(Integer.parseInt(minField.getText()), 
                            Integer.parseInt(maxField.getText()), 
                            Integer.parseInt(preferredField.getText())); 
        } catch (Exception e) {
        }
    }

    private  RegularField createRegularTestField(int[] dims)
    {
        int nPoints = 1;
        RegularField out = new RegularField(dims);
        for (int dim : dims) 
            nPoints *= dim;
        float[] coords = new float[nPoints * 3];
        int[] data = new int[nPoints];
        for (int i = 0; i < nPoints; i++) {
            data[i] = i + 1;
        }
        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < 3; j++)
                coords[3 * i + j] = ((float) random() - .5f) / 10;
        }
        out.setCurrentCoords(new FloatLargeArray(coords));
        out.addComponent(DataArray.create(data, 1, "points"));
        return out;
    }

    private  IrregularField createIrregularTestField(long nPoints)
    {
        IrregularField out = new IrregularField(nPoints);
        FloatLargeArray coords = new FloatLargeArray(3 * nPoints);
        for (long i = 0; i < nPoints; i++) 
            for (int j = 0; j < 3; j++)
                coords.set(3 * i + j, (float)(random() - .5f) / 10);
        out.setCurrentCoords(coords);
        return out;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new TestFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected JButton b1d;
    protected JButton b1d2;
    protected JButton b2d;
    protected JButton b2d2;
    protected JButton b3d;
    protected JButton b3d2;
    protected JLabel iLabel;
    protected JLabel iLabel2;
    protected JSpinner iSpinner;
    protected JSpinner iSpinner2;
    protected JButton irr;
    protected JButton irr2;
    protected JLabel jLabel;
    protected JLabel jLabel2;
    protected JLabel jLabel4;
    protected JLabel jLabel5;
    protected JLabel jLabel6;
    protected JLabel jLabel7;
    protected JLabel jLabel8;
    protected JPanel jPanel1;
    protected JPanel jPanel11;
    protected JPanel jPanel12;
    protected JPanel jPanel13;
    protected JPanel jPanel14;
    protected JPanel jPanel15;
    protected JPanel jPanel16;
    protected JPanel jPanel17;
    protected JPanel jPanel2;
    protected JPanel jPanel3;
    protected JPanel jPanel4;
    protected JPanel jPanel5;
    protected JPanel jPanel6;
    protected JPanel jPanel7;
    protected JSpinner jSpinner;
    protected JSpinner jSpinner2;
    protected JLabel kLabel;
    protected JLabel kLabel2;
    protected JSpinner kSpinner;
    protected JSpinner kSpinner2;
    protected JTextField maxField;
    protected JTextField minField;
    protected JTextField nNodesField;
    protected JTextField nNodesField2;
    protected JButton nullAuxFieldButton;
    protected JTextField preferredField;
    protected SeedPointsGUI seedPointsGUI1;
    // End of variables declaration//GEN-END:variables
}
