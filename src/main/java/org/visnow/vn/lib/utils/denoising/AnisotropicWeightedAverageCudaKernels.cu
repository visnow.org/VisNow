/**
 * CUDA kernel function that denoises given volume.
 * The volume is first divided into chunks. Kernel,
 * by design, is responsible for denoising single chunk.
 *
 * @param inputChannel
 * @param controlChannel
 * @param outputChannel
 * @param numRows
 * @param numCols
 * @param numSlices
 * @param filterRadius
 * @param slope_squared_reciprocal
 * @param slope1_squared_reciprocal
 */
extern "C"
__global__ void
denoise3DChunks (const float * const __restrict inputChannel,
		 const float * const __restrict controlChannel,
		 float * outputChannel,
		 const int Nx, const int Ny, const int Nz,
		 const int chunk, const int chunk_length,
		 const int radius,
		 const float slope_squared_reciprocal, const float slope1_squared_reciprocal)
{
  /* Absolute positions in the volume (three-dimensional array).
   * Voxel being averaged. */
  int i0 = static_cast <int> (blockDim.x * blockIdx.x + threadIdx.x);
  int j0 = static_cast <int> (blockDim.y * blockIdx.y + threadIdx.y);
  int k0 = static_cast <int> (blockDim.z * blockIdx.z + threadIdx.z) + chunk * chunk_length;

  if (i0 >= Nx || j0 >= Ny || k0 >= Nz || k0 >= (chunk + chunk_length) * Nx * Ny) {
    return;
  }

  int idx_center = i0 + j0 * Nx + k0 * Nx * Ny;
  float controlChannelCenter = controlChannel[idx_center];

  float new_value = 0.0f;
  float sum_weights = 0.0f;

  int il = (i0 < radius) ? -i0 : -radius;
  int iu = (i0 >= Nx - radius) ? Nx - i0 - 1 : radius;
  int jl = (j0 < radius) ? -j0 : -radius;
  int ju = (j0 >= Ny - radius) ? Ny - j0 - 1 : radius;
  int kl = (k0 < radius) ? -k0 : -radius;
  int ku = (k0 >= Nz - radius) ? Nz - k0 - 1 : radius;

  int idx;
  float weight;
  float dist_squared;

  /* Indices i, j, k represent shifts in stencil. Negative values are possible. */
  for (int k = kl; k <= ku; ++k) {
    for (int j = jl; j <= ju; ++j) {
      for (int i = il; i <= iu; ++i) {
	/* Indices i, j, k (their absolute value) are expected to be small. */
	dist_squared = static_cast <float> (i * i + j * j + k * k);

	/* Data is fetched from controlChannel array, which has the same dimensions,
	 * as input image (inputChannel array). So stencil array index equals image
	 * array index. */
	idx = i0 + i + (j0 + j) * Nx + (k0 + k) * Nx * Ny;

	weight = controlChannel[idx] - controlChannelCenter;
	weight = __expf (- dist_squared * slope_squared_reciprocal - weight * weight * slope1_squared_reciprocal);
	sum_weights += weight;

	new_value += inputChannel[idx] * weight;
      }
    }
  }
  outputChannel[idx_center] = new_value / sum_weights;
}
