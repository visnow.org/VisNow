/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.events;

import java.util.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class MinimizationStepEvent extends EventObject
{

    protected static final long serialVersionUID = 3936955629015878704L;
    protected int stepNumber;
    protected double value;
    protected double gradientNorm;
    protected double avgStep;
    protected double maxStep;
    protected int nExtrapolations;
    protected int nBacksteps;
    protected int nInterpolations;
    protected String[] varNames;
    protected double[] variables;
    protected boolean forceRefresh = false;

    public MinimizationStepEvent(Object source,
                                 int stepNumber,
                                 double value, double gradientNorm,
                                 double avgStep, double maxStep,
                                 int nExtrapolations, int nBacksteps, int nInterpolations,
                                 String[] varNames, double[] variables,
                                 boolean forceRefresh)
    {
        super(source);
        this.stepNumber = stepNumber;
        this.value = value;
        this.gradientNorm = gradientNorm;
        this.avgStep = avgStep;
        this.maxStep = maxStep;
        this.nExtrapolations = nExtrapolations;
        this.nBacksteps = nBacksteps;
        this.nInterpolations = nInterpolations;
        this.varNames = varNames;
        this.variables = variables;
        this.forceRefresh = forceRefresh;
    }

    public MinimizationStepEvent(Object source,
                                 int stepNumber,
                                 double value, double gradientNorm,
                                 double avgStep, double maxStep,
                                 int nExtrapolations, int nBacksteps, int nInterpolations, boolean forceRefresh)
    {
        this(source, stepNumber,
             value, gradientNorm, avgStep, maxStep,
             nExtrapolations, nBacksteps, nInterpolations,
             null, null, forceRefresh);
    }

    public MinimizationStepEvent(Object source,
                                 int stepNumber,
                                 double value, double gradientNorm,
                                 double avgStep, double maxStep,
                                 int nExtrapolations, int nBacksteps, int nInterpolations,
                                 String[] varNames, double[] variables)
    {
        this(source, stepNumber,
             value, gradientNorm, avgStep, maxStep,
             nExtrapolations, nBacksteps, nInterpolations,
             varNames, variables, false);
    }

    public double getValue()
    {
        return value;
    }

    public double getGradientNorm()
    {
        return gradientNorm;
    }

    public int getNExtrapolations()
    {
        return nExtrapolations;
    }

    public int geNBacksteps()
    {
        return nBacksteps;
    }

    public int getNInterpolations()
    {
        return nInterpolations;
    }

    public int getStepNumber()
    {
        return stepNumber;
    }

    public double getAvgStep()
    {
        return avgStep;
    }

    public double getMaxStep()
    {
        return maxStep;
    }

    public String[] getVarNames()
    {
        return varNames;
    }

    public double[] getVariables()
    {
        return variables;
    }

    @Override
    public String toString()
    {
        return "MinimizationStepEvent step = " + stepNumber + " val = " + value + "   gradientNorm = " + gradientNorm;
    }

    public String toLongStringHeader()
    {
        return "step\tenergy\tavgGrad\tavgStep\tmaxStep\txt\tbk\tin";
    }

    public String toLongString()
    {
        return "" + stepNumber + "\t" + value + "\t" + gradientNorm + "\t" + avgStep + "\t" + maxStep + "\t" + nExtrapolations + "\t" + nBacksteps + "\t" + nInterpolations;
    }

    public String formatted()
    {
        return String.format("%4d %10.5e %10.5e %10.5f %10.5f %3d %3d%3d",
                             stepNumber, value, gradientNorm, avgStep, maxStep, nExtrapolations, nBacksteps, nInterpolations);
    }

    /**
     * @return the forceRefresh
     */
    public boolean isForceRefresh()
    {
        return forceRefresh;
    }

    /**
     * @param forceRefresh the forceRefresh to set
     */
    public void setForceRefresh(boolean forceRefresh)
    {
        this.forceRefresh = forceRefresh;
    }
}
