/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class DirectionalLargeFieldSmoothingDP implements Runnable
{
    private int[] dims;
    private int[] outDims;
    private int down = 1;
    private DoubleLargeArray inData;
    private DoubleLargeArray outData;
    private double[] kernel;
    private double[] kernelDist;
    private int direction = 0;
    private int radius = 5;
    private int nThreads = 1;
    private int iThread = 0;
    private int vlen;
    private double[] inSlice;
    private double[] outSlice;

    private void computeSmoothingKernel(double sigma, int[] dims)
    {
        radius = (int) (4 * sigma);

        if (radius >= dims[direction] / 2)
            radius = dims[direction] / 2 - 1;

        kernel = new double[2 * radius + 1];
        kernelDist = new double[2 * radius + 1];
        double s = 0;
        double rd = 4.f / (sigma * sigma);
        for (int i = 0; i <= radius; i++)
            kernel[radius + i] = kernel[radius - i] = (double) exp(-i * i * rd);
        for (int i = 0; i < kernel.length; i++)
            s += kernel[i];
        s = 1 / s;
        for (int i = 0; i < kernel.length; i++) {
            kernel[i] *= s;
            if (i == 0)
                kernelDist[i] = 0;
            else
                kernelDist[i] = kernelDist[i - 1] + kernel[i - 1];
        }
    }


    public DirectionalLargeFieldSmoothingDP(int direction, int[] dims, int down, DoubleLargeArray inData, DoubleLargeArray outData, double[] kernel, double[] kernelDist,
            int radius, int nThreads, int iThread)
    {
        this.dims = dims;
        this.inData = inData;
        if (outData != null)
            this.outData = outData;
        this.down = down;
        this.kernel = kernel;
        this.kernelDist = kernelDist;
        this.direction = direction;
        this.radius = radius;
        this.nThreads = nThreads;
        this.iThread = iThread;
        int nData = dims[0];
        for (int i = 1; i < dims.length; i++)
            nData *= dims[i];
        vlen = (int)(inData.length() / nData);
        outDims = new int[dims.length];
        System.arraycopy(dims, 0, outDims, 0, outDims.length);
        outDims[direction] /= down;
    }
    
    public DirectionalLargeFieldSmoothingDP(int direction, int[] dims, DoubleLargeArray data, double[] kernel, double[] kernelDist,
            int radius, int nThreads, int iThread)
    {
        this(direction, dims, 1, data, null, kernel, kernelDist, radius, nThreads, iThread);
    }


    public DirectionalLargeFieldSmoothingDP(int direction, int[] dims, int down, DoubleLargeArray inData, DoubleLargeArray outData, double sigma,
            int nThreads, int iThread)
    {
        this.dims = dims;
        this.inData = inData;
        if (outData != null)
            this.outData = outData;
        this.down = down;
        this.direction = direction;
        computeSmoothingKernel(down * sigma, dims);
        this.nThreads = nThreads;
        this.iThread = iThread;
        int nData = dims[0];
        for (int i = 1; i < dims.length; i++)
            nData *= dims[i];
        vlen = (int)(inData.length() / nData);
        outDims = new int[dims.length];
        System.arraycopy(dims, 0, outDims, 0, outDims.length);
        outDims[direction] = (dims[direction] + down - 1) / down;
    }

    public DirectionalLargeFieldSmoothingDP(int direction, int[] dims, int down, DoubleLargeArray inData, DoubleLargeArray outData,
            int nThreads, int iThread)
    {
        this(direction, dims, down, inData, outData, 1f, nThreads, iThread);
    }

    public DirectionalLargeFieldSmoothingDP(int direction, int[] dims, DoubleLargeArray inData, double sigma, int nThreads, int iThread)
    {
        this(direction, dims, 1, inData, null, sigma, nThreads, iThread);
    }
    
    private void smooth(int down)
    {
        int r = kernel.length / 2;
        int n = inSlice.length / vlen;
        for (int m = 0; m < vlen; m++)
            for (int i = 0, ii = 0; i < n; i += down, ii++) {
                double s = 0;
                if (i < r) {
                    int k = r - i;
                    for (int j = 0; j <= i + r; j++, k++)
                        s += inSlice[j * vlen + m] * kernel[k];
                    outSlice[ii * vlen + m] = s / kernelDist[r + i + 1];
                } else if (i < n - r) {
                    int k = i - r;
                    for (int j = 0; j < 2 * r + 1 && k < n; j++, k++)
                        s += inSlice[k * vlen + m] * kernel[j];
                    outSlice[ii * vlen + m] = s;
                } else {
                    int k = i - r;
                    for (int j = 0; k < n; j++, k++)
                        s += inSlice[k * vlen + m] * kernel[j];
                    outSlice[ii * vlen + m] = s / kernelDist[r + n - i];
                }
            }
    }

    @Override
    public void run()
    {
        long start = 0;
        int nDims = dims.length;
        int step = 1;
        long nSlices = 1;

        switch (direction) {
        case 0:
            inSlice = new double[dims[0] * vlen];
            outSlice = new double[outDims[0] * vlen];
            nSlices = 1;
            if(nDims > 1)
                nSlices = dims[1];
            if (nDims == 3)
                nSlices *= dims[2];
            for (long i = iThread; i < nSlices; i += nThreads) {
                for (int j = 0; j < inSlice.length; j++) 
                    inSlice[j] = inData.get(i * dims[0] * vlen + j);
                smooth(down);
                for (int j = 0; j < outSlice.length; j++) 
                    outData.setDouble(i * outDims[0] * vlen + j, outSlice[j]);
            }
            break;
        case 1:
            step = dims[0];
            inSlice = new double[dims[1] * vlen];
            outSlice = new double[outDims[1] * vlen];
            nSlices = dims[0];
            if (nDims == 3)
                nSlices *= dims[2];
            for (int i = iThread; i < nSlices; i += nThreads) {
                int p = i / dims[0];
                int q = i % dims[0];
                if (nDims == 3)
                    start = p * dims[0] * dims[1] + q;
                else
                    start = i;
                for (int j = 0, l = 0; j < dims[1]; j++)
                    for (int k = 0; k < vlen; k++, l++)
                        inSlice[l] = inData.getDouble((start + j * step) * vlen + k);
                smooth(down);
                if (nDims == 3)
                    start = p * dims[0] * outDims[1] + q;
                for (int j = 0, l = 0; j < outDims[1]; j++)
                    for (int k = 0; k < vlen; k++, l++)
                        try {
                            outData.setDouble((start + j * step) * vlen + k, outSlice[l]);
                        } catch (Exception e) {
                            System.out.println("1 " + start + " " + j + " " + step + " " + (start + j * step) + " " + (start + j * step) * vlen + k);
                        }
            }
            break;
        case 2:
            step = dims[0] * dims[1];
            inSlice = new double[dims[2] * vlen];
            outSlice = new double[outDims[2] * vlen];
            nSlices = dims[0] * dims[1];
            for (int i = iThread; i < nSlices; i += nThreads) {
                if (i >= nSlices)
                    continue;
                if (iThread == 0)
                    fireStatusChanged((float) i / nSlices);
                for (int j = 0, l = 0; j < dims[2]; j++)
                    for (int k = 0; k < vlen; k++, l++)
                        inSlice[l] = inData.getDouble((i + j * step) * vlen + k);
                smooth(down);
                for (int j = 0, l = 0; j < outDims[2]; j++)
                    for (int k = 0; k < vlen; k++, l++)
                        try {
                            outData.setDouble((i + j * step) * vlen + k, outSlice[l]);
                        } catch (Exception e) {
                            System.out.println("2 " + i + " " + j + " " + step + " " + (i + j * step) + " " + ((i + j * step) * vlen + k) + " " + l);
                        }

            }
            break;
        }
    }
    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }
}
