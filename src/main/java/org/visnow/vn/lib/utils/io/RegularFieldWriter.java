/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.util.Locale;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class RegularFieldWriter extends FieldWriter
{

    private final RegularField field;
    private static final Logger LOGGER = Logger.getLogger(RegularFieldWriter.class);

    /**
     * Creates a new instance of RegularFieldWriter.
     *
     * @param field the field to be written
     * @param path       a string indicating the directory and the base file name for the .vnf file
     * @param binary     true if writing binary data files and ascii string components file, false if all data will be written to an ASCII file
     * @param overwrite  if true, then existing header and data files will be overwritten
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public RegularFieldWriter(RegularField field, String path, boolean binary, boolean overwrite) throws FileSystemException, IOException
    {
        super(field, path, binary, overwrite);
        this.field = field;
    }


    public boolean writeField()
    {
        boolean status;
        try {
            headerWriter.println("#VisNow regular field");
            if (field.getName() != null && !field.getName().trim().isEmpty())
                headerWriter.print("field \"" + field.getName() + "\",");
            headerWriter.print(" dims ");
            for (int i = 0; i < field.getDims().length; i++)
                headerWriter.print(" " + field.getDims()[i]);
            if (field.getCurrentCoords() != null)
                headerWriter.print(", coords");
            if (field.hasMask())
                headerWriter.print(", mask");
            if (field.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = field.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            if (!field.getAxesNames()[0].equals("x") || !field.getAxesNames()[1].equals("y") || !field.getAxesNames()[2].equals("z"))
                headerWriter.printf("axes %s %s %s%n", field.getAxesNames()[0], field.getAxesNames()[1], field.getAxesNames()[2]);
            String unitString = VisNowFieldWriter.createUnitString(field);
            if (unitString != null)
                headerWriter.println(unitString);
            if (field.getCurrentCoords() == null) {
                float[][] af = field.getAffine();
                headerWriter.printf(Locale.US,      "origin %10.4e %10.4e %10.4e %n",    af[3][0], af[3][1], af[3][2]);
                for (int i = 0; i < 3; i++)
                    headerWriter.printf(Locale.US, "    v%d %10.4e %10.4e %10.4e %n", i, af[i][0], af[i][1], af[i][2]);
            }
            writeExtents();
            WriteContainer.writeHeader(inField, headerWriter);
            if (binary) {
                printDataFileHeader();
                status = WriteContainer.writeBinary(inField, "", headerWriter, largeContentOutput);
                if (strings) {
                    printAsciiFileHeader();
                    status = WriteContainer.writeStrings(inField, "", headerWriter, contentWriter)  && status;
                }
            }
            else {
                printAsciiFileHeader();
                status = WriteContainer.writeASCII(inField, "", headerWriter, contentWriter);
            }
        } catch (Exception e) {
            status = false;
            LOGGER.error("Error writing field", e);
        }
        closeAll();
        return status;
    }
}
