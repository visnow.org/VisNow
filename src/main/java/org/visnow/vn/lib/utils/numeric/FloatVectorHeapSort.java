/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FloatVectorHeapSort
{

    private float[] sortedItems;
    private int[] indices;
    private int len;
    private int n;
    private int veclen;
    private int left;
    private int right;
    private int largest;

    public FloatVectorHeapSort(float[] sortedItems, int[] indices, int veclen)
    {
        if (indices.length * veclen != sortedItems.length) {
            System.out.println("bad table lengths");
            return;
        }
        this.sortedItems = sortedItems;
        this.veclen = veclen;
        this.indices = indices;
        n = len = indices.length;
    }

    private void buildheap(float[] a)
    {
        n = len - 1;
        for (int i = n / 2; i >= 0; i--)
            maxheap(a, i);
    }

    private boolean gt(int i, int j)
    {
        for (int k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++)
            if (sortedItems[k0] < sortedItems[k1])
                return false;
            else if (sortedItems[k0] > sortedItems[k1])
                return true;
        return false;
    }

    private void maxheap(float[] a, int i)
    {
        left = 2 * i;
        right = 2 * i + 1;
        if (left <= n && gt(left, i))
            largest = left;
        else
            largest = i;

        if (right <= n && gt(right, largest))
            largest = right;
        if (largest != i) {
            exchange(i, largest);
            maxheap(a, largest);
        }
    }

    private void exchange(int i, int j)
    {
        float t;
        for (int k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++) {
            t = sortedItems[k0];
            sortedItems[k0] = sortedItems[k1];
            sortedItems[k1] = t;
        }

        int u = indices[i];
        indices[i] = indices[j];
        indices[j] = u;

    }

    public void sort()
    {
        buildheap(sortedItems);

        for (int i = n; i > 0; i--) {
            exchange(0, i);
            n -= 1;
            maxheap(sortedItems, 0);
        }
    }

    public static void main(String[] args)
    {
        for (int n = 1000; n <= 10000000; n *= 2) {
            float[] t = new float[2 * n];
            int[] ind = new int[n];
            for (int i = 0; i < t.length; i++)
                t[i] = n * (float) Math.random();
            for (int i = 0; i < ind.length; i++)
                ind[i] = i;
            FloatVectorHeapSort hs = new FloatVectorHeapSort(t, ind, 2);
            long s = System.currentTimeMillis();
            hs.sort();
            System.out.printf("%6d %7d%n", n, System.currentTimeMillis() - s);
            if (n == 100)
                for (int i = 0; i < n; i++)
                    System.out.printf("%8.3f %8.3f%n", t[2 * i], t[2 * i + 1]);
        }
    }
}
