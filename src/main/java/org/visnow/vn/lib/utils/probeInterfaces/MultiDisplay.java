/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.probeInterfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import org.visnow.vn.lib.utils.field.ValueRanges;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public abstract class MultiDisplay
{
    protected LocalToWindow ltw;
    protected boolean xSort = false;
    protected boolean pointerLine = true;
    protected Position probesPosition = RIGHT;

    protected int windowWidth = 500, windowHeight = 500;

    protected ArrayList<ProbeDisplay> displays = new ArrayList<>();
    protected IrregularField mergedField = null;
    protected float[] minima, maxima;
    protected float[] mMinima, mMaxima;
    protected float[] pMinima, physMinima, coeffs;
    protected int[] exponents;
    protected float startTime = 0, endTime = 0;
    protected float time = 0;
    protected String[] names;
    protected int initMargin = 10;
    protected int graphAreaWidth  = 180;
    protected int graphAreaHeight = 150;
    protected int gap = 10;
    protected float relativeFontHeight = .02f;

    abstract public void addDisplay(IrregularField field);

    public void updateTimeRange()
    {
    }

    public void removeDisplay(ProbeDisplay pickedGraph)
    {
        if (displays.isEmpty())
            return;
        if (pickedGraph == null)
            pickedGraph = displays.get(displays.size() - 1);
        displays.remove(pickedGraph);
        computeMergedField();
    }

    public void clearDisplays()
    {
        displays.clear();
        minima = null;
        maxima = null;
        mMinima = null;
        mMaxima = null;
        names = null;
        pMinima = null;
        physMinima = null;
        coeffs = null;
        exponents = null;
        mergedField = null;
    }

    public ProbeDisplay getSelection()
    {
        for (ProbeDisplay display : displays)
            if (display.isSelected())
                return display;
        return null;
    }

    public void sort()
    {
        Collections.sort(displays);
    }

    public void setProbesPosition(Position probePosition)
    {
        this.probesPosition = probePosition;
        for (ProbeDisplay display : displays)
            display.setProbesPosition(probesPosition);
    }

    public void setPointerLine(boolean pointerLine) {
        this.pointerLine = pointerLine;
        for (ProbeDisplay display : displays)
            display.setPointerLine(pointerLine);
    }

    public ArrayList<ProbeDisplay> getDisplays()
    {
        return displays;
    }

    protected void computeMergedField()
    {
        mergedField = null;
        int mergeCounter = 0;
        for (ProbeDisplay display : displays) {
            mergedField = MergeIrregularField.merge(mergedField, display.getField(), mergeCounter, false, true);
            mergeCounter += 1;
        }
    }

    public IrregularField getMergedField()
    {
        return mergedField;
    }

    protected void initValueRanges(Field fld)
    {
        int nCmp = fld.getNComponents();
        minima = new float[nCmp];
        maxima = new float[nCmp];
        Arrays.fill(minima, Float.MAX_VALUE);
        Arrays.fill(maxima, -Float.MAX_VALUE);
        mMinima = new float[nCmp];
        mMaxima = new float[nCmp];
        Arrays.fill(mMinima, Float.MAX_VALUE);
        Arrays.fill(mMaxima, -Float.MAX_VALUE);
        pMinima = new float[nCmp];
        physMinima = new float[nCmp];
        coeffs = new float[nCmp];
        exponents = new int[nCmp];
        names = new String[nCmp];
        for (int i = 0; i < nCmp; i++) {
            DataArray da = fld.getComponent(i);
            names[i] = da.getName();
        }
    }

    protected void updateValueRanges()
    {
        for (int m = 0; m < displays.size(); m++) {
            Field fld = displays.get(m).getField();
            if (m == 0)
                initValueRanges(fld);
            ValueRanges.updateValueRanges(fld, minima, maxima, mMinima, mMaxima,
                                               pMinima, physMinima, coeffs, false);
        }
        ValueRanges.updateExponentRanges(minima, maxima, mMinima, mMaxima,
                                         pMinima, physMinima, coeffs, exponents);
        updateTimeRange();
    }

    public int[] maxGraphCount()
    {
        return new int[] {
            (windowWidth  - initMargin) / (graphAreaWidth  + gap),
            (windowHeight - initMargin) / (graphAreaHeight + gap),
        };
    }

    public void setInitMargin(int initMargin)
    {
        this.initMargin = initMargin;
    }

    public void setGap(int gap)
    {
        this.gap = gap;
    }

    public void clearSelection()
    {
        for (ProbeDisplay display : displays)
            display.setSelected(false);
    }

    abstract public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height);

    public float getStartTime()
    {
        return startTime;
    }

    public float getEndTime()
    {
        return endTime;
    }

}
