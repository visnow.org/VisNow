/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.curves;

import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularMeshExtrusion
{

    static float[] ortogonalize(float[] ax, float[] v)
    {
        if (ax == null || ax.length != 3 || v == null || v.length != 3)
            return null;
        float[] w = new float[3];
        float da = 0, dv = 0, dvn = 0, s = 0;

        for (int i = 0; i < 3; i++) {
            da += ax[i] * ax[i];
            dv += v[i] * v[i];
            s += v[i] * ax[i];
        }
        s /= da;
        for (int i = 0; i < 3; i++) {
            v[i] -= s * ax[i];
            dvn += v[i] * v[i];
        }
        if (dvn < .00001)
            return null;
        dvn = (float) sqrt(dv / dvn);
        for (int i = 0; i < 3; i++)
            v[i] *= dvn;
        w[0] = ax[1] * v[2] - ax[2] * v[1];
        w[1] = ax[2] * v[0] - ax[0] * v[2];
        w[2] = ax[0] * v[1] - ax[1] * v[0];
        dvn = (float) sqrt(dv / (w[0] * w[0] + w[1] * w[1] + w[2] * w[2]));
        for (int i = 0; i < 3; i++)
            w[i] *= dvn;
        return w;
    }


    private static float[] smoothMesh(float[][] coords, int[] dims, int smoothing)
    {
        int length = dims[2];
        int sliceSize = dims[0] * dims[1];
        float[] crds = new float[3 * sliceSize * length];
        for (int iLine = 0; iLine < 5; iLine++) {
            float[] p0 = coords[iLine];
            float[] p1 = new float[3 * length];
            for (int step = 0; step < smoothing; step++) {
                for (int i = 3; i < p0.length - 3; i++)
                    p1[i] = (p0[i - 3] + 2 * p0[i] + p0[i + 3]) / 4;
                System.arraycopy(p1, 3, p0, 3, p0.length - 6);
            }
        }

        for (int slice = 0, k = 0; slice < length; slice++) {
            float[][] quad = new float[4][3];
            for (int i = 0; i < quad.length; i++)
                System.arraycopy(coords[i], 3 * slice, quad[i], 0, 3);
            float[] centre = new float[3];
            float[] diag0 = new float[3];
            float[] diag1 = new float[3];
            for (int i = 0; i < 3; i++)
                centre[i] = diag0[i] = diag1[i] = 0;
            for (float[] q : quad)
                for (int j = 0; j < 3; j++)
                    centre[j] += q[j];
            float l0 = 0, l1 = 0;
            for (int i = 0; i < diag1.length; i++) {
                diag0[i] = quad[3][i] - quad[0][i];
                l0 += diag0[i] * diag0[i];
                diag1[i] = quad[2][i] - quad[1][i];
                l1 += diag1[i] * diag1[i];
                centre[i] /= 4;
            }
            l0 = (float) sqrt(l0);
            l1 = (float) sqrt(l1);
            float s = 0;
            for (int i = 0; i < 3; i++) {
                diag0[i] /= l0;
                diag1[i] /= l1;
                s += diag0[i] * diag1[i];
            }
            l1 = 0;
            for (int i = 0; i < 3; i++) {
                diag1[i] -= s * diag0[i];
                l1 += diag1[i] * diag1[i];
            }
            l1 = (float) sqrt(l1);
            float[][] planeQuad = new float[4][2];
            for (int i = 0; i < 4; i++)
                planeQuad[i][0] = planeQuad[i][1] = 0;
            for (int i = 0; i < 3; i++) {
                diag1[i] /= l1;
                for (int j = 0; j < 4; j++) {
                    planeQuad[j][0] += (quad[j][i] - centre[i]) * diag0[i];
                    planeQuad[j][1] += (quad[j][i] - centre[i]) * diag1[i];
                }
            }
            float[] tOrigin = new float[]{(planeQuad[0][0] + planeQuad[1][1] - planeQuad[2][1] - planeQuad[3][0]) / 4,
                                          (planeQuad[0][1] - planeQuad[1][0] + planeQuad[2][0] - planeQuad[3][1]) / 4};
            float[] tV0 = new float[]{-tOrigin[0] - tOrigin[1], tOrigin[0] - tOrigin[1]};
            float[] tV1 = new float[]{tOrigin[1] - tOrigin[0], -tOrigin[0] - tOrigin[1]};
            float[] origin = new float[3], v0 = new float[3], v1 = new float[3];
            for (int i = 0; i < 3; i++) {
                origin[i] = coords[4][3 * slice + i] + tOrigin[0] * diag0[i] + tOrigin[1] * diag1[i];
                v0[i] = (tV0[0] * diag0[i] + tV0[1] * diag1[i]) / dims[0];
                v1[i] = (tV1[0] * diag0[i] + tV1[1] * diag1[i]) / dims[0];
            }
            for (int i = 0; i < dims[1]; i++)
                for (int j = 0; j < dims[0]; j++)
                    for (int l = 0; l < 3; l++, k++)
                        crds[k] = origin[l] + j * v0[l] + i * v1[l];

        }
        return crds;
    }
    
    private static void computeVertices(float[][] tmpCoords, int step, float[] path, int[][] off, float[] v, float[] w)
    {
        for (int vertex = 0; vertex < off.length; vertex++) {
            int[] vOff = off[vertex];
            for (int l = 0; l < 3; l++)
                tmpCoords[vertex][3 * step + l] = path[3 * step + l] + vOff[0] * v[l] + vOff[1] * w[l];
        }
    }

    public static RegularField extrude(float[] path, float[] init, int r, int smoothing)
    {
        if (path.length % 3 != 0 || path.length < 6)
            return null;
        int length = path.length / 3;
        int[] dims = new int[]{2 * r + 1, 2 * r + 1, length};
        RegularField out = new RegularField(dims);

        float[][] tmpCoords = new float[5][3 * length];

        int[][] off = new int[][]{{-r, -r}, {r, -r}, {-r, r}, {r, r}, {0, 0}};
        float[] v = new float[3];
        float[] ax = new float[3];
        for (int i = 0; i < 3; i++) {
            ax[i] = path[i + 3] - path[i];
            v[i] = init[i];
        }
        float[] w = ortogonalize(ax, v);
        if (w == null) {
            v[0] = init[1]; v[1] = init[2]; v[2] = init[0];
            w = ortogonalize(ax, v);
            if (w == null) {
                v[0] = init[2]; v[1] = init[0]; v[2] = init[1];
                w = ortogonalize(ax, v);
                if (w == null) {
                    System.out.println("extrude failed: could not find initial orthogonal basis");
                    return null;
                }
            }
        }
        computeVertices(tmpCoords, 0, path, off, v, w);
        
        for (int step = 1; step < length - 1; step++) {
            for (int i = 0; i < 3; i++)
                ax[i] = path[3 * step + i + 3] - path[3 * step + i - 3];
            w = ortogonalize(ax, v);
            if (w == null) {
                System.out.println("extrude failed: could not find orthogonal basis");
                return null;
            }
            computeVertices(tmpCoords, step, path, off, v, w);
        }
//        for (int i = 0; i < 3; i++)
//            ax[i] = path[3 * (length - 1) + i] - path[3 * (length - 1) + i - 3];
//        w = ortogonalize(ax, v);
//        if (w == null) {
//            System.out.println("extrude failed: could not find orthogonal basis");
//            return null;
//        }
        for (int vertex = 0; vertex < off.length; vertex++) {
            int[] vOff = off[vertex];
            for (int l = 0; l < 3; l++)
                tmpCoords[vertex][3 * (length - 1) + l] = path[3 * (length - 1) + l] + vOff[0] * v[l] + vOff[1] * w[l];
        }

        out.setCurrentCoords(new FloatLargeArray(smoothMesh(tmpCoords, dims, smoothing)));
        
        out.updatePreferredExtents(true);
        return out;
    }

    public static void main(String[] args)
    {
        float[][] quad = {{-5, -5, 1}, {6, -5, -2}, {-5, 6, 2}, {6, 5, 1}};
        float[] centre = new float[3];
        float[] diag0 = new float[3];
        float[] diag1 = new float[3];
        for (int i = 0; i < 3; i++)
            centre[i] = diag0[i] = diag1[i] = 0;
        for (int i = 0; i < quad.length; i++) {
            for (int j = 0; j < 3; j++)
                centre[j] += quad[i][j];
        }
        float l0 = 0, l1 = 0;
        for (int i = 0; i < diag1.length; i++) {
            diag0[i] = quad[3][i] - quad[0][i];
            l0 += diag0[i] * diag0[i];
            diag1[i] = quad[2][i] - quad[1][i];
            l1 += diag1[i] * diag1[i];
            centre[i] /= 4;
        }
        l0 = (float) sqrt(l0);
        l1 = (float) sqrt(l1);
        float s = 0;
        for (int i = 0; i < 3; i++) {
            diag0[i] /= l0;
            diag1[i] /= l1;
            s += diag0[i] * diag1[i];
        }
        l1 = 0;
        for (int i = 0; i < 3; i++) {
            diag1[i] -= s * diag0[i];
            l1 += diag1[i] * diag1[i];
        }
        l1 = (float) sqrt(l1);
        float[][] planeQuad = new float[4][2];
        for (int i = 0; i < 4; i++)
            planeQuad[i][0] = planeQuad[i][1] = 0;
        for (int i = 0; i < 3; i++) {
            diag1[i] /= l1;
            for (int j = 0; j < 4; j++) {
                planeQuad[j][0] += (quad[j][i] - centre[i]) * diag0[i];
                planeQuad[j][1] += (quad[j][i] - centre[i]) * diag1[i];
            }
        }
        float[] av = new float[]{(planeQuad[0][0] + planeQuad[1][1] - planeQuad[2][1] - planeQuad[3][0]) / 4,
                                 (planeQuad[0][1] - planeQuad[1][0] + planeQuad[2][0] - planeQuad[3][1]) / 4};
        for (int i = 0; i < av.length; i++)
            System.out.printf("%6.3f %6.3f %6.3f %6.3f     %6.3f %n", planeQuad[0][i], planeQuad[1][i], planeQuad[2][i], planeQuad[3][i], av[i]);
    }
}
