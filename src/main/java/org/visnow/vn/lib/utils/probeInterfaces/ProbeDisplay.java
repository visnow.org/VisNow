/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.probeInterfaces;

import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;

public abstract class ProbeDisplay implements Comparable
{
    public static enum Position {AT_POINT, TOP, RIGHT, BOTTOM, LEFT, NONE};
    
    protected IrregularField fld;
    
    protected float[] center = {0, 0, 0};
    protected float[] crds;
    protected float[] cornerCoords;
    protected int[] screenHandle = new int[2];
    protected boolean xSort = false;
    protected boolean pointerLine = true;
    protected Position probesPosition = Position.RIGHT;
    protected int[] probeOffset = {30, 60};
    protected float probeScale = .2f;
    protected int currentPosition = 0;
    protected boolean selected = false;
    protected int ixmin = 0, ixmax = 0, iymin = 0, iymax = 0;
    protected int screenX, screenY, screenW, screenH;
    protected String title = null;
    protected float titleFontSize = .02f;
    
    public abstract void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, String title);

    public abstract void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, 
                                      int width, int height, float scale, int index, 
                                      int ix, int iy, String title);
    
    public abstract void draw(J3DGraphics2D gr, LocalToWindow ltw, 
                              int x, int y, int width, int height, 
                              int fontSize, String title, int index);

    @Override
    public int compareTo(Object obj)
    {
        if (!(obj instanceof ProbeDisplay))
            return 0;
        ProbeDisplay p = (ProbeDisplay)obj;
        if (xSort)
            return p.screenHandle[0] > screenHandle[0] ? -1: 1;
        else
            return p.screenHandle[1] > screenHandle[1] ? -1: 1;
    }
    
    public IrregularField getField()
    {
        return fld;
    }
    
    public void setxSort(boolean xSort) {
        this.xSort = xSort;
    }

    public void setPointerLine(boolean pointerLine) {
        this.pointerLine = pointerLine;
    }

    public void setProbesPosition(Position probesPosition) {
        this.probesPosition = probesPosition;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public float[] getCenter()
    {
        return center;
    }

    public void setCenter(float[] center)
    {
        this.center = center;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getTitleFontSize()
    {
        return titleFontSize;
    }

    public void setTitleFontSize(float titleFontSize)
    {
        this.titleFontSize = titleFontSize;
    }
    
    public boolean isInside(int ix, int iy)
    {
        selected = screenX <= ix && ix <= screenX + screenW && screenY - screenH <= iy && iy <= screenY;
        return selected;
    }

    public int[] getScreenHandle() {
        return screenHandle;
    }

    public abstract void updateScreenCoords(LocalToWindow ltw);
    
}
