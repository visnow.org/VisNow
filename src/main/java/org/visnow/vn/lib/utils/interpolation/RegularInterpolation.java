/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.interpolation;

import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.apache.commons.math3.util.FastMath.min;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author know
 */
public class RegularInterpolation
{
    public static final int COMPUTE_OUTPUT = 1;
    public static final int COMPUTE_POSITIONS = 2;
    
    private final Field inField;
    private int trueDim;
    protected float[] inCoords = null;
    
    protected float[] recomputedCoords = null;
    protected int computeExtent = COMPUTE_OUTPUT;
    protected LogicLargeArray valid;
    protected FieldPosition[] result;
    private final int nThreads = VisNow.availableProcessors();
    
    private int[] outDims;
    private float[][] outAffine = new float[4][3];
    private float[][] outInvAffine;
    private RegularField outRegularField;
    
    
    public RegularInterpolation(Field inField,  int[] resDims, float[][] affine, int computeContent)
    {
        trueDim = inField.getTrueNSpace();
        if (trueDim > resDims.length) 
            throw new IllegalArgumentException("input true dimension = " + trueDim + 
                                               "does not match interpolation mesh dims = " + outDims.length);
        if (trueDim == 2 && (affine[0][2] != 0 || affine[1][2] != 0 || affine[3][2] != 0))
            throw new IllegalArgumentException("input true dimension is 2 and interpolation mesh is not in xy plane");
        this.inField = inField;
        this.outDims = new int[trueDim];
        System.arraycopy(resDims, 0, outDims, 0, trueDim);
        this.outAffine = affine;
        if (inField.hasCoords()) 
            inCoords = inField.getCoords(0).getData();
        else 
            inCoords = ((RegularField)inField).getCoordsFromAffine().getData();
        recomputedCoords = new float[trueDim * (int)inField.getNNodes()];
        this.computeExtent = computeContent;
        int outNNodes = 1;
        for (int i = 0; i < outDims.length; i++)
            outNNodes *= outDims[i];
        outRegularField = new RegularField(outDims);
        outRegularField.setAffine(outAffine);
        if ((computeExtent & COMPUTE_POSITIONS) != 0) {
            result = new FieldPosition[outNNodes];
            for (int i = 0; i < result.length; i++)
                result[i] = null;
        }
        outInvAffine = outRegularField.getInvAffine();

        if (inField.hasCoords()) {
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new CoordinateRecomputation(nThreads, iThread));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        valid = new LogicLargeArray(outRegularField.getNNodes(), false);
    }
    
    public void setOutAffine(float[][] outAffine)
    {
        this.outAffine = outAffine;
    }
    
    /**
     * The original field coordinates are recomputed to the index coordinates of the outfield
     */
    private class CoordinateRecomputation implements Runnable
    {
        int nThreads = 1;
        int iThread = 0;
        int nInNodes;

        public CoordinateRecomputation(int nThr, int iThr)
        {
            this.nThreads = nThr;
            this.iThread = iThr;
            nInNodes = (int) inField.getNNodes();
        }

        @Override
        public void run()
        {
            float[] v = new float[3];
            int dk = nInNodes / nThreads;
            int kstart = iThread * dk + min(iThread, nInNodes % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nInNodes % nThreads);
            for (int k = kstart; k < kend; k++) {
                for (int i = 0; i < 3; i++)
                    v[i] = inCoords[3 * k + i] - outAffine[3][i];
                for (int i = 0; i < trueDim; i++) {
                    recomputedCoords[trueDim * k + i] = 0;
                    for (int j = 0; j < trueDim; j++)
                        recomputedCoords[trueDim * k + i] += outInvAffine[j][i] * v[j];
                }
            }
        }
    }
    
    public static float[][] outAffineFromboxVerts(float[] boxVerts, int trueDim)
    {
        float[][] outAffine = new float[4][3];
        for (float[] outAffine1 : outAffine) 
            Arrays.fill(outAffine1, 0.f);
        for (int i = 0; i < trueDim; i++) {
            for (int j = 0, k = 3; j < trueDim; j++, k *= 2) 
                outAffine[j][i] = boxVerts[i + k] - boxVerts[i];
            outAffine[3][i] = boxVerts[i];
        }
        for (int i = trueDim; i < 3; i++) 
            outAffine[i][i] = 1;
        return outAffine;
    }
    
    protected ProcessSimplex processor;
    
    class UpdateRegularFieldPart implements Runnable
    {

        int from;
        int to;
        RegularField inField;

        public UpdateRegularFieldPart(long n, int nThreads, int iThread, RegularField inField)
        {
            this.inField = inField;
            from = (int)(n * iThread / nThreads);
            to = (int)(n * (iThread + 1) / nThreads);
        }

        @Override
        public void run()
        {
            if (trueDim == 3)
                for (int i = from; i < to; i++) {
                    int[] tetras = inField.getTetras(i);
                    if (tetras == null)
                        continue;
                    for (int tet = 0; tet < 5; tet++) {
                        int[] tetraVerts = new int[4];
                        System.arraycopy(tetras, 4 * tet, tetraVerts, 0, 4);
                        processor.processTetra(tetraVerts, 0);
                    }
                }
            else
                for (int i = from; i < to; i++) {
                    int[] triangles = inField.getTriangles(i);
                    if (triangles == null)
                        continue;
                    for (int tri = 0; tri < 2; tri++) {
                        int[] triVerts = new int[3];
                        System.arraycopy(triangles, 3 * tri, triVerts, 0, 3);
                        processor.processTriangle(triVerts, 0);
                    }
                }
        }
    }
    class UpdateAffineFieldPart implements Runnable
    {

        private final long from;
        private final long to;
        private final RegularField inField;
        private final int nSpace;
        private final LargeArray[][] inData;
        private final LargeArray[][] outData;
        private final int[] vlens;

        public UpdateAffineFieldPart(int nThreads, int iThread, RegularField inField, 
                                     LargeArray[][] inData, LargeArray[][] outData, int[] vlens)
        {
            this.inField = inField;
            nSpace = inField.getDimNum();
            long n = outRegularField.getNNodes();
            from = (int)(n * iThread / nThreads);
            to = (int)(n * (iThread + 1) / nThreads);
            this.inData = inData;
            this.outData = outData;
            this.vlens = vlens;
        }

        @Override
        public void run()
        {
            int[] inDims = inField.getDims();
            int boxLen = nSpace == 3 ? 8 : nSpace == 2 ? 4 : 2;
            float[][] inAffine  = inField.getAffine();
            float[][] invInAffine = inField.getInvAffine();
            float[] t = new float[nSpace];
            int[] ind = new int[nSpace];
            int[] inInd = new int[nSpace];
            long[] dataInd = new long[boxLen];
            float[] intCoeffs = new float[boxLen];
            for (long iOut = from; iOut < to; iOut++) {   // loop over outField nodes
                valid.set(iOut, true);
                long ii = iOut;
                for (int j = 0; j < nSpace; j++) {
                    ind[j] = (int)(ii % outDims[j]);
                    ii /= outDims[j];
                }                               // ind = indices of i-th node in outfield
                float[] s = new float[nSpace];
                for (int i = 0; i < nSpace; i++)
                    s[i] = outAffine[3][i] - inAffine[3][i];
                for (int i = 0; i < nSpace; i++)
                    for (int j = 0; j < nSpace; j++)
                        s[j] += ind[i] * outAffine[i][j];  
                Arrays.fill(t, 0);
                for (int i = 0; i < nSpace; i++)
                    for (int j = 0; j < nSpace; j++)
                        t[j] += invInAffine[i][j] * s[i];  // t contains coordinates of outField[iOut] in inField index space
                boolean inside = true;
                for (int i = 0; i < nSpace; i++) {
                    if (t[i] < 0 || t[i] > inDims[i]) {
                        inside = false;
                        valid.setBoolean(iOut, false);
                        break;
                    }                           // t[j] is the j-th coordinate in index coord system of inField
                    inInd[i] = (int)t[i];
                    t[i] -= inInd[i];
                }
                if (inside) {
                    for (int i = 0; i < boxLen; i++) {
                        long m = 0;
                        float c = 1;
                        for (int j = nSpace - 1; j >= 0; j--) {
                            int shift = (i >> j) & 1;
                            int l = inInd[j] + shift;
                            if (l >= inDims[j])
                                l = inDims[j] - 1;
                            m += l;
                            if (j > 0)
                                m *= inDims[j - 1];
                            c *= shift == 1 ? t[j] : (1 - t[j]);
                        }
                        dataInd[i] = m;
                        intCoeffs[i] = c;
                    }
                    if ((computeExtent & COMPUTE_POSITIONS) != 0)
                        result[(int)iOut] = new FieldPosition(0, t[0], inInd, t);
                    if ((computeExtent & COMPUTE_OUTPUT) != 0)
                    for (int iDataArray = 0; iDataArray < inData.length; iDataArray++) {
                        int vlen = vlens[iDataArray];
                        for (int iTimeStep = 0; iTimeStep < outData[iDataArray].length; iTimeStep++)
                            for (int j = 0; j < vlen; j++) {
                                float val = 0;
                                for (int k = 0; k < boxLen; k++)
                                    val += intCoeffs[k] * inData[iDataArray][iTimeStep].getFloat(vlen * dataInd[k] + j);
                                outData[iDataArray][iTimeStep].set(vlen * iOut + j, val);
                            }
                    }
                }
            }
        }
    }

    private void computePositions(RegularField inField)
    {
        int[] inDims = inField.getDims();
        long nOut = 1, nIn = 1;
        for (int i = 0; i < outDims.length; i++) {
            nIn *= (inDims[i] - 1);
            nOut *= outDims[i];
        }
        if (inField.hasCoords()) {
            processor = new ProcessSimplex(outDims, recomputedCoords, 0,  null, null, null,
                                           valid, result, computeExtent);
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new UpdateRegularFieldPart(nIn, nThreads, iThread, inField));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        else {
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new UpdateAffineFieldPart(nThreads, iThread, inField, null, null, null));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
    }
    private void update(RegularField inField)
    {
        int[] inDims = inField.getDims();
        long nOut = 1, nIn = 1;
        for (int i = 0; i < outDims.length; i++) {
            nIn *= (inDims[i] - 1);
            nOut *= outDims[i];
        }
        int nNumDArrays = 0;
        LargeArray[][] inData = null;
        LargeArray[][] outData = null;
        int[] vlens = null;
        int[] indices = null;
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric())
                    nNumDArrays += 1;
            inData = new LargeArray[nNumDArrays][];
            outData = new LargeArray[nNumDArrays][];
            indices = new int[nNumDArrays];
            vlens = new int[nNumDArrays]; 
            int jComponent = 0;
            for (int iComponent = 0; iComponent < inField.getNComponents(); iComponent++)
                if (inField.getComponent(iComponent).isNumeric()) {
                    DataArray da = inField.getComponent(iComponent);
                    LargeArrayType type = da.getType().toLargeArrayType();
                    indices[iComponent] = iComponent;
                    vlens[iComponent] = da.getVectorLength();
                    inData[iComponent] = new LargeArray[da.getNFrames()];
                    outData[iComponent] = new LargeArray[da.getNFrames()];
                    for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                        inData[jComponent][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                        outData[jComponent][iTimeStep] = LargeArrayUtils.create(type, vlens[iComponent] * nOut);
                    }
                    jComponent += 1;
                }
        }
        if (inField.hasCoords()) {
            processor = new ProcessSimplex(outDims, recomputedCoords, nNumDArrays,
                                           vlens, inData, outData,
                                           valid, result, computeExtent);
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new UpdateRegularFieldPart(nIn, nThreads, iThread, inField));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        else {
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new UpdateAffineFieldPart(nThreads, iThread, inField, inData, outData, vlens));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++) {
                DataArray data = inField.getComponent(indices[iDataArray]);
                DataArrayType type = data.getType();
                int veclen = data.getVectorLength();
                TimeData outTimeData = new TimeData(type);
                DataArray outDa = DataArray.create(type, nOut, veclen, data.getName(), 
                                                   data.getUnit(), data.getUserData());
                for (int iStep = 0; iStep < data.getNFrames(); iStep++) 
                    outTimeData.setValue(outData[iDataArray][iStep], data.getTime(iStep));
                outDa.setPreferredRanges(data.getPreferredMinValue(),     data.getPreferredMaxValue(), 
                                         data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                outDa.setTimeData(outTimeData);
                outRegularField.addComponent(outDa);
            }
            outRegularField.setMask(valid, 0);
            outRegularField.setCurrentTime(inField.getCurrentTime());
        }
    }

    class UpdateIrregularFieldPart implements Runnable
    {
        int from;
        int to;
        int[] nodes;
        int[] indices;

        public UpdateIrregularFieldPart(int n, int nThreads, int iThread, int[] nodes, int[] indices)
        {
            this.nodes = nodes;
            this.indices = indices;
            from = n * iThread / nThreads;
            to = n * (iThread + 1) / nThreads;
        }

        @Override
        public void run()
        {
            if (trueDim == 3)
                for (int tet = from; tet < to; tet++) {
                    int[] tetraVerts = new int[4];
                    System.arraycopy(nodes, 4 * tet, tetraVerts, 0, 4);
                    if (indices != null)
                        processor.processTetra(tetraVerts, indices[tet]);
                    else
                        processor.processTetra(tetraVerts, 0);
                }
            else
                for (int tri = from; tri < to; tri++) {
                    int[] triVerts = new int[4];
                    System.arraycopy(nodes, 3 * tri, triVerts, 0, 3);
                    if (indices != null)
                        processor.processTriangle(triVerts, indices[tri]);
                    else
                        processor.processTriangle(triVerts, 0);
                }
        }
    }
    
    private void computePositions(IrregularField inField)
    {
        processor = new ProcessSimplex(outDims, recomputedCoords, 0, 
                                       null, null, null, 
                                       valid, result, COMPUTE_POSITIONS);
        
        for (CellSet inCellSet : inField.getCellSets()) {
            if (trueDim == 3)
                for (int i = CellType.TETRA.getValue(); i <= CellType.HEXAHEDRON.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
            else if (trueDim == 2)
                for (int i = CellType.TRIANGLE.getValue(); i <= CellType.QUAD.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
        }
    }

    private void update(IrregularField inField)
    {
        int nNumDArrays = 0;
        int nNumNodeDataArrays = 0;
        nNumDArrays = 0;
        long nOut = 1;
        for (int i = 0; i < outDims.length; i++) 
            nOut *= outDims[i];
        LargeArray[][] inData = null;
        FloatLargeArray[][] outData = null;
        int[] vlens = null;
        int[] indices = null;
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric())
                    nNumDArrays += 1;
            nNumNodeDataArrays = nNumDArrays;
            boolean cellDataInterpolable = inField.getNCellSets() == 1 && inField.getCellSet(0).getNComponents() > 0;
            if (cellDataInterpolable) {
                CellSet cs = inField.getCellSet(0);
                for (int i = 0; i < cs.getNComponents(); i++)
                    if (cs.getComponent(i).isNumeric())
                        nNumDArrays += 1;
            }
            inData = new LargeArray[nNumDArrays][];
            outData = new FloatLargeArray[nNumDArrays][];
            indices = new int[nNumDArrays];
            vlens = new int[nNumDArrays];
            int iOutData = 0;
            for (int iComponent = 0; iComponent < inField.getNComponents(); iComponent++)
                if (inField.getComponent(iComponent).isNumeric()) {
                    DataArray da = inField.getComponent(iComponent);
                    indices[iOutData] = iComponent;
                    vlens[iOutData] = da.getVectorLength();
                    inData[iOutData] = new LargeArray[da.getNFrames()];
                    outData[iOutData] = new FloatLargeArray[da.getNFrames()];
                    for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                        inData[iOutData][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                        outData[iOutData][iTimeStep] = new FloatLargeArray(vlens[iOutData] * nOut);
                    }
                    iOutData += 1;
                }
            if (cellDataInterpolable) {
                CellSet cs = inField.getCellSet(0);
                for (int iComponent = 0; iComponent < cs.getNComponents(); iComponent++)
                    if (cs.getComponent(iComponent).isNumeric()) {
                        DataArray da = cs.getComponent(iComponent);
                        indices[iOutData] = iComponent;
                        vlens[iOutData] = da.getVectorLength();
                        inData[iOutData] = new LargeArray[da.getNFrames()];
                        outData[iOutData] = new FloatLargeArray[da.getNFrames()];
                        for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                            inData[iOutData][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                            outData[iOutData][iTimeStep] = new FloatLargeArray(vlens[iOutData] * nOut);
                            iOutData += 1;
                        }
                    }
            }
        }
        processor = new ProcessSimplex(outDims, recomputedCoords, nNumNodeDataArrays, 
                                       vlens, inData, outData, 
                                       valid, result, computeExtent);
        
        for (CellSet inCellSet : inField.getCellSets()) {
            if (trueDim == 3)
                for (int i = CellType.TETRA.getValue(); i <= CellType.HEXAHEDRON.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
            else if (trueDim == 2)
                for (int i = CellType.TRIANGLE.getValue(); i <= CellType.QUAD.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
        }
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++) {
                DataArray data = iDataArray < nNumNodeDataArrays ?
                        inField.getComponent(indices[iDataArray]) :
                        inField.getCellSet(0).getComponent(indices[iDataArray]);
                int veclen = data.getVectorLength();
                TimeData outTimeData = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                DataArray outDa = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, nOut, veclen, data.getName(), 
                                                   data.getUnit(), data.getUserData());
                for (int iStep = 0; iStep < data.getNFrames(); iStep++) 
                    outTimeData.setValue(outData[iDataArray][iStep], data.getTime(iStep));
                outDa.setPreferredRanges(data.getPreferredMinValue(),     data.getPreferredMaxValue(), 
                                         data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                outDa.setTimeData(outTimeData);
                outRegularField.addComponent(outDa);
            }
            outRegularField.setMask(valid, 0);
        }
    }
    
    public FieldPosition[] computePositions()
    {
        if (inField == null)
            return null;
        if (inField instanceof RegularField)
            computePositions((RegularField)inField);
        else
            computePositions((IrregularField)inField);
        return result;
    }
    
    public RegularField update()
    {
        if (inField == null)
            return null;
        if (inField instanceof RegularField)
            update((RegularField)inField);
        else
            update((IrregularField)inField);
        return outRegularField;
    }
    
    public static final RegularField interpolate(Field field, int[] outDims, float[][] outAffine, int computeContent)
    {
        if (field.getTrueNSpace() < 2)
            return null;
        return new RegularInterpolation(field, outDims, outAffine, computeContent).update();
    }
    
    public static final RegularField interpolate(Field field, float[][] boxAffine, int[] resolution,  int computeContent)
    {
        int[] outDims = new int[resolution.length];
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
             outAffine[3][i] = boxAffine[3][i];
             for (int j = 0; j < resolution.length; j++) {
                outDims[j] = resolution[j] + 1;
                outAffine[j][i] =boxAffine[j][i] / resolution[j];
            }
        }
        for (int j = resolution.length; j < 3; j++) 
            outAffine[j][j] = 1;
        return interpolate(field, outDims, outAffine, computeContent);
    }
    
    public static final RegularField interpolate(Field field, float[][] boxAffine, int res, int computeContent)
    {
        if (field.getTrueNSpace() < 2)
            return null;
        int nDim = field.getTrueNSpace();
        int nOut = (int)Math.min(.5e9, Math.pow(res, nDim));
        double[] r = new double[nDim];
        int[] resolution = new int[nDim];
        double vol = 1;
        for (int i = 0; i < nDim; i++) {
            r[i] = 0;
            for (int j = 0; j < nDim; j++)
                r[i] += boxAffine[i][j] * boxAffine[i][j];
            r[i] = Math.sqrt(r[i]);
            vol *= r[i];
        }
        float delta = (float) Math.pow(vol / nOut, 1. / nDim);
        for (int i = 0; i < nDim; i++) 
            resolution[i] = (int) (r[i] / delta) + 1;
        return interpolate(field, boxAffine, resolution,  computeContent);
    }
    
    public static final RegularField interpolate(Field field, int[] outDims, float[][] outAffine)
    {
        return interpolate(field, outDims, outAffine, COMPUTE_OUTPUT);
    }
    
    public static final RegularField interpolate(Field field, float[][] boxAffine, int[] resolution)
    {
        return interpolate(field,  boxAffine, resolution, COMPUTE_OUTPUT);
    }
    
    public static final RegularField interpolate(Field field, float[][] boxAffine, int res)
    {
        return interpolate(field, boxAffine, res,  COMPUTE_OUTPUT);
    }
    
    public static final FieldPosition[] computePositions(Field field, int[] outDims, float[][] outAffine)
    {
        if (field.getTrueNSpace() < 2)
            return null;
        return new RegularInterpolation(field, outDims, outAffine, COMPUTE_POSITIONS).computePositions();
    }
    
    public static final FieldPosition[] getFieldPositions(Field field, float[][] boxAffine, int[] resolution)
    {
        int[] outDims = new int[resolution.length];
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
             outAffine[3][i] = boxAffine[3][i];
             for (int j = 0; j < resolution.length; j++) {
                outDims[j] = resolution[j] + 1;
                outAffine[j][i] =boxAffine[j][i] / resolution[j];
            }
        }
        for (int j = resolution.length; j < 3; j++) 
            outAffine[j][j] = 1;
        return computePositions(field, outDims, outAffine);
    }
    
    public static final FieldPosition[] getFieldPositions(Field field, float[][] boxAffine, int res)
    {
        if (field.getTrueNSpace() < 2)
            return null;
        int nDim = field.getTrueNSpace();
        int nOut = (int)Math.min(.5e9, Math.pow(res, nDim));
        double[] r = new double[nDim];
        int[] resolution = new int[nDim];
        double vol = 1;
        for (int i = 0; i < nDim; i++) {
            r[i] = 0;
            for (int j = 0; j < nDim; j++)
                r[i] += boxAffine[i][j] * boxAffine[i][j];
            r[i] = Math.sqrt(r[i]);
            vol *= r[i];
        }
        float delta = (float) Math.pow(vol / nOut, 1. / nDim);
        for (int i = 0; i < nDim; i++) 
            resolution[i] = (int) (r[i] / delta) + 1;
        return getFieldPositions(field, boxAffine, resolution);
    }
}
