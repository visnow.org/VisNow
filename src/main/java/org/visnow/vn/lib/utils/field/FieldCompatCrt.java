/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.function.BiPredicate;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;

/**
 *
 * Field compatibility criteria organized as an enum of atomic criteria that can be combined according to actual requirements
 * by the compatible method
 *
 * @author Krzysztof S. Nowinski (VisNow.org, University of Warsaw, ICM)
 */



public enum FieldCompatCrt
{
    TYPE_EQUAL                  ((x, y) -> x.getType()        == y.getType()),
    SIZE_EQUAL                  ((x, y) -> x.getNElements()   == y.getNElements()),
    NDIMS_EQUAL                 ((x, y) -> x instanceof RegularField && y instanceof RegularField &&
                                           ((RegularField)x).getDimNum() == ((RegularField)y).getDimNum()),
    DIMS_EQUAL                  ((x, y) -> x instanceof RegularField && y instanceof RegularField &&
                                           Arrays.equals(((RegularField)x).getDims(), ((RegularField)y).getDims())),
    NCOMPONENTS_EQUAL           ((x, y) -> x.getNComponents() == y.getNComponents()),
    COMPONENTS_NAMES_COMPATIBLE ((x, y) -> {if (!NCOMPONENTS_EQUAL.compat.test(x, y))
                                                return false;
                                            for (String name: x.getComponentNames())
                                                if (y.getComponent(name) == null)
                                                    return false;
                                            return true;}),
    ;
    private final BiPredicate<Field, Field> compat;

    private FieldCompatCrt(BiPredicate<Field, Field> compat)
    {
        this.compat = compat;
    }

    /**
     * checks if two fields are compatible according to given criteria
     * @param x first field
     * @param y second field
     * @param criteria array of criteria to be checked
     * @param daCrt array of criteria to be checked for each component
     * @return true if x and y are not null and satisfy the given criteria, false otherwise
     */
    public static final boolean compatible(Field x, Field y,
                                FieldCompatCrt[] criteria, DataArrayCompatCrt[] daCrt)
    {
        if (x == null || y == null)
            return false;
        for (FieldCompatCrt cr : criteria)
            if (!cr.compat.test(x, y))
                return false;
        if (daCrt != null && daCrt.length > 0) {
            for (String name: x.getComponentNames()) {
                if (y.getComponent(name) == null ||
                    !DataArrayCompatCrt.compatible(x.getComponent(name), y.getComponent(name), daCrt))
                    return false;
            }
        }
        return true;
    }

}
