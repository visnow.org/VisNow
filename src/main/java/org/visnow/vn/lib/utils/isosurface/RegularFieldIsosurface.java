/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isosurface;

/*
 * RegularFieldIsosurfaceV1.java
 *
 * Created on August 14, 2004, 2:06 PM
 * Rewritten August 2020
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling, visnow.org)
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.jscic.utils.SliceUtils;
import org.visnow.vn.lib.utils.field.subset.subvolume.LinearInterpolation;
import org.visnow.vn.lib.utils.field.subset.subvolume.NewNode;
import org.visnow.vn.lib.utils.numeric.IntVectorHeapSort;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 * RegularFieldIsosurfaceV1.java
 *
 * Created on August 14, 2004, 2:06 PM
 * Rewritten August 2020
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling, visnow.org)
 */

public class RegularFieldIsosurface extends IsosurfaceEngine
{
    public interface SliceRemap
    {

        /**
         * Currently used in the standard isosurface module to wobble slightly values of the isosurfaced component
         * to avoid hard to resolve cases of isosurface passing exactly through a grid node;
         * Used in the SurfaceCompute class to create the boundary surface of the area of fixed map value
         * @param slice a slice of a regular field isosurfaced data component
         * @param val value determining surface to be created
         * @return modified slice
         */
        public float[] remap(float[] slice, float val);
    }

    private static final int X_AXIS = 0, Y_AXIS = 1, Z_AXIS = 2;
    private static final int[][][][] ISOSURFACE_TRIANGLES = LookUpTables.getISOSURFACE_TRIANGLES();
    private static final int[][][][] ISOSURFACE_EDGES     = LookUpTables.getISOSURFACE_EDGES();

    /**
     * indices of edge points
     *
     * <pre>
     *  v6----11----v7
     *  |\          |\
     *  | 9           10
     *  6  \        7  \
     *  |   v4----8----v5
     *  |   |       |  |
     *  |   |          |
     *  v2- | 5 - -v3  4
     *   \  2       \  |
     *    1 |        3 |
     *     \|         \|
     *     v0-----0----v1
     *
     *  </pre>
     *
     *
     * MID_POINT_INDICES[i][0] -
     * index to edgeIndices array
     * (x edge in current plane,
     *  y edge in current plane,
     *  z edge over current plane,
     *  x edge in plane over current plane,
     *  y edge in plane over current plane)
     *  MID_POINT_INDICES[i][1] - x coordinate offset
     *  MID_POINT_INDICES[i][2] - y coordinate offset
     *
     */

    private static final int[][] MID_POINT_INDICES =
    {{0, 0, 0}, {1, 0, 0}, {2, 0, 0}, {1, 1, 0},
     {2, 1, 0}, {0, 0, 1}, {2, 0, 1}, {2, 1, 1},
     {3, 0, 0}, {4, 0, 0}, {4, 1, 0}, {3, 0, 1}
    };
    /*
       centers of edges as defined by edge indices
    */
    private static final float[][] MID_POINT_COORDS =
    {{.5f, 0, 0}, {0, .5f, 0}, {0, 0, .5f}, {1, .5f, 0},
     {1, 0, .5f}, {.5f, 1, 0}, {0, 1, .5f}, {1, 1, .5f},
     {.5f, 0, 1}, {0, .5f, 1}, {1, .5f, 1}, {.5f, 1, 1}
    };

    private final RegularField inField;
    private final float[][] invAffine;
    private final FloatLargeArray coords;
    private SliceRemap sliceRemap;
    private boolean createGrid;

    public static final float getNorm(LargeArray x, int vLen, long i)
    {
        if (vLen == 1)
            return x.getFloat(i);
        else {
            float n = 0;
            for (int j = 0; j < vLen; j++)
                n += x.getFloat(vLen * i + j) * x.getFloat(vLen * i + j);
            return (float)FastMath.sqrt(n);
        }
    }

    protected class IsosurfacePart implements Runnable
    {
        private final int iThread, nThreads;
        private final long[]     lDims;
        private final int[]      low;
        private final int[]      down;
        private final int[]      reducedDims;
        private final long[]     offsets;
        private final int        vecLen;
        private final LargeArray inData;
        private final float      threshold;
        private final SliceRemap sliceRemap;

        private final ArrayList<NewNode>  nodes      = new ArrayList<>();
        private final ArrayList<float[]>  normals    = new ArrayList<>();
        private final ArrayList<float[]>  mappedNormals    = new ArrayList<>();
        private int   overlap= 0;

        private final int[][]  triangles;
        private int   nTriangles;

        private final int[][]  gridEdges;
        private int   nGridEdges;
        private final float[][] currentLayer;
        private final boolean[][]  currentLayerMask;
        private final int[][][] edgeIndices  = new int[5][][];
        private final short[][] hexCodes;
        private int   newPointIndex = 0;
        private final int firstSlice;
        private final int lastSlice;

        private final boolean isMask;
        private final LogicLargeArray valid;

/**
 *
 * @param iThread      current thread index
 * @param nThreads     number of threads
 * @param lDims        original field dimensions
 * @param low          left down back corner of isosurfaced area
 * @param down         downsize for each axis
 * @param rdims        dimension of reduced data
 * @param vecLen       vector length of isosurfaced data
 * @param inData       isosurfaced data
 * @param valid        mask array: if a cell contains at least one node with
 * false valid entry then this cell is ignored
 * @param sliceRemap
 * @param threshold    isosurface threshold
 */
        public IsosurfacePart(int iThread, int nThreads,
                              long[] lDims,
                              int[] low, int[] down, int[] rdims,
                              int vecLen, LargeArray inData, LogicLargeArray valid,
                              SliceRemap sliceRemap, float threshold) {

            this.iThread    = iThread;
            this.nThreads   = nThreads;
            this.lDims      = lDims;
            this.low        = low;
            this.down       = down;
            this.reducedDims       = rdims;
            this.offsets    = new long[] {down[0],
                                          down[1] * lDims[0],
                                          down[2] * lDims[1] * lDims[0]};
            this.vecLen     = vecLen;
            this.inData     = inData;
            this.sliceRemap = sliceRemap;
            this.threshold  = threshold;
            this.valid      = valid;
            this.isMask     = valid != null;
            currentLayer = new float[2][reducedDims[0] * reducedDims[1]];
            if (isMask)
                currentLayerMask = new boolean[2][reducedDims[0] * reducedDims[1]];
            else {
                currentLayerMask = new boolean[2][];
                currentLayerMask[0] = currentLayerMask[1] = null;
            }
            hexCodes     = new short[reducedDims[1]][reducedDims[0]];
            for (short[] hexCode : hexCodes) {
                Arrays.fill(hexCode, (short)0);
            }
            firstSlice = (iThread      * reducedDims[2]) / nThreads;
            lastSlice = iThread < nThreads - 1 ?
                        ((iThread + 1) * reducedDims[2]) / nThreads + 1 :
                        ((iThread + 1) * reducedDims[2]) / nThreads;
            triangles  = new int[lastSlice - 1 - firstSlice][];
            gridEdges  = new int[lastSlice - 1 - firstSlice][];
        }

        public ArrayList<NewNode> getNodes() {
            return nodes;
        }

        public ArrayList<float[]> getNormals() {
            return normals;
        }

        public ArrayList<float[]> getMappedNormals() {
            return mappedNormals;
        }

        public int getOverlap() {
            return overlap;
        }


        public int[][] getTriangles() {
            return triangles;
        }

        public int getnTriangles() {
            return nTriangles;
        }


        public int[][] getGridEdges() {
            return gridEdges;
        }

        public int getnGridEdges() {
            return nGridEdges;
        }


        private float[] getSlice(int slice)
        {
            float[] originalDataSlice =
                    (vecLen == 1) ?
                    SliceUtils.get2DFloatSlice(inData, lDims, Z_AXIS, low[2] + down[2] * slice, vecLen).getData():
                    SliceUtils.get2DNormSlice( inData, lDims, Z_AXIS, low[2] + down[2] * slice, vecLen).getData();
            float[] tmpSlice  = new float[reducedDims[0] * reducedDims[1]];
            for (int yIndex = 0, k = 0; yIndex < reducedDims[1]; yIndex++)
                for (int xIndex = 0, m = (int) ((yIndex * down[1] + low[1]) * lDims[0] + low[0]);
                         xIndex < reducedDims[0];
                         xIndex++, k++, m += down[0])
                    tmpSlice[k] = originalDataSlice[m];
            float[] dataSlice = sliceRemap.remap(tmpSlice, threshold);
            return dataSlice;
        }

        private boolean[] getMaskSlice(int slice)
        {
            boolean[]  originalMaskSlice = new boolean[(int)(lDims[0] * lDims[1])];
            try {
                LargeArrayUtils.arraycopy(valid, ((long)low[2] + down[2] * slice) * lDims[0] * lDims[1],
                                          originalMaskSlice, 0, lDims[0] * lDims[1]);
            } catch (Exception e) {
                return null;
            }
            boolean[] maskSlice = new boolean[reducedDims[0] * reducedDims[1]];
            for (int yIndex = 0, k = 0; yIndex < reducedDims[1]; yIndex++)
                for (int xIndex = 0, m = (int) ((yIndex * down[1] + low[1]) * lDims[0] + low[0]);
                         xIndex < reducedDims[0];
                         xIndex++, k++, m += down[0])
                    maskSlice[k] = originalMaskSlice[m];
            return maskSlice;
        }

        private float derivativeAtMidpoint(LargeArray data, int vLen, long l,
                                           int pointAxis, float t,
                                           int derivAxis, long derivAxisIndex)
        {
            float diff;
            if (derivAxis == pointAxis) {
                if (derivAxisIndex >= down[derivAxis]  &&
                   (derivAxisIndex > lDims[derivAxis] - 2 * down[derivAxis] || t < .5)) {
                    float tm = getNorm(inData, vLen, l - offsets[derivAxis]);
                    float t0 = getNorm(inData, vLen, l);
                    float tp = getNorm(inData, vLen, l + offsets[derivAxis]);
                    diff = (2 * t * (tp +tm - 2 * t0) + tp - tm) / 2;
                }
                else if (derivAxisIndex < lDims[derivAxis] - 2 * down[derivAxis] &&
                        (derivAxisIndex < down[derivAxis] || t > .5)) {
                    float tm = getNorm(inData, vLen, l);
                    float t0 = getNorm(inData, vLen, l + offsets[derivAxis]);
                    float tp = getNorm(inData, vLen, l + 2 * offsets[derivAxis]);
                    diff = (2 * (t - 1) * (tp +tm - 2 * t0) + tp - tm) / 2;
                }
                else
                diff = (getNorm(data, vLen, l + offsets[pointAxis]) - getNorm(data, vLen, l)) / down[derivAxis];
            }
            else if (derivAxisIndex >= down[derivAxis] &&
                     derivAxisIndex < lDims[derivAxis] - down[derivAxis])
                diff = ((1 - t) * (getNorm(inData, vLen, l +                      offsets[derivAxis]) -
                                   getNorm(inData, vLen, l -                      offsets[derivAxis])) +
                             t  * (getNorm(inData, vLen, l + offsets[pointAxis] + offsets[derivAxis]) -
                                   getNorm(inData, vLen, l + offsets[pointAxis] - offsets[derivAxis]))) / 2;
            else if (derivAxisIndex >= down[derivAxis])
                diff = ((1 - t) * (getNorm(inData, vLen, l) -
                                   getNorm(inData, vLen, l  -                     offsets[derivAxis])) +
                             t  * (getNorm(inData, vLen, l + offsets[pointAxis]) -
                                   getNorm(inData, vLen, l + offsets[pointAxis] - offsets[derivAxis])));
            else
                return ((1 - t) * (getNorm(inData, vLen, l +                      offsets[derivAxis]) -
                                   getNorm(inData, vLen, l)) +
                             t  * (getNorm(inData, vLen, l + offsets[pointAxis] + offsets[derivAxis]) -
                                   getNorm(inData, vLen, l + offsets[pointAxis]))) ;
            return diff / down[derivAxis];
        }

        private float[] coordDerivativeAtMidpoint(LargeArray coords, long l,
                                                 int pointAxis, float t,
                                                 int derivAxis, long derivAxisIndex)
        {
            float[] deriv = new float[3];
            for (int i = 0; i < deriv.length; i++) {
                if (derivAxis == pointAxis) {
                    if (derivAxisIndex >= down[derivAxis]  &&
                       (derivAxisIndex > lDims[derivAxis] - 2 * down[derivAxis] || t < .5)) {
                        float tm = coords.getFloat(3 * (l - offsets[derivAxis]) + i);
                        float t0 = coords.getFloat(3 * l + i);
                        float tp = coords.getFloat(3 * (l + offsets[derivAxis]) + i);
                        deriv[i] = (2 * t * (tp +tm - 2 * t0) + tp - tm) / 2;
                    }
                    else if (derivAxisIndex < lDims[derivAxis] - 2 * down[derivAxis] &&
                            (derivAxisIndex < down[derivAxis] || t > .5)) {
                        float tm = coords.getFloat(3 * l + i);
                        float t0 = coords.getFloat(3 * (l + offsets[derivAxis]) + i);
                        float tp = coords.getFloat(3 * (l + 2 * offsets[derivAxis]) + i);
                        deriv[i] = (2 * (t - 1) * (tp +tm - 2 * t0) + tp - tm) / 2;
                    }
                    else
                        deriv[i] = (coords.getFloat(3 * (l + offsets[pointAxis]) + i) -
                                    coords.getFloat(3 * l + i)) / down[derivAxis];
                }
                else if (derivAxisIndex >= down[derivAxis] && derivAxisIndex < lDims[derivAxis] - down[derivAxis])
                    deriv[i] = ((1 - t) * (coords.getFloat(3 * (l +                      offsets[derivAxis]) + i) -
                                           coords.getFloat(3 * (l -                      offsets[derivAxis]) + i)) +
                                      t * (coords.getFloat(3 * (l + offsets[pointAxis] + offsets[derivAxis]) + i) -
                                           coords.getFloat(3 * (l + offsets[pointAxis] - offsets[derivAxis]) + i))) / 2;
                else if (derivAxisIndex >= down[derivAxis])
                    deriv[i] = ((1 - t) * (coords.getFloat(3 * l                                 + i) -
                                           coords.getFloat(3 * (l -                      offsets[derivAxis]) + i)) +
                                      t * (coords.getFloat(3 * (l + offsets[pointAxis])                      + i) -
                                           coords.getFloat(3 * (l + offsets[pointAxis] - offsets[derivAxis]) + i)));
                else
                    deriv[i] = ((1 - t) * (coords.getFloat(3 * (l +                      offsets[derivAxis]) + i) -
                                           coords.getFloat(3 * l                                             + i)) +
                                      t * (coords.getFloat(3 * (l + offsets[pointAxis] + offsets[derivAxis]) + i) -
                                           coords.getFloat(3 * (l + offsets[pointAxis])                      + i)));

            }
            for (int i = 0; i < deriv.length; i++)
                deriv[i] /= down[derivAxis];
            return deriv;
        }

        private float[][] gradientAtMidpoint(long l, int pointAxis, float t,
                                           long iIndex, long jIndex, long kIndex)
        {
            float[] indexNormal = new float[] {
                           derivativeAtMidpoint(inData, vecLen, l, pointAxis, t, X_AXIS, iIndex),
                           derivativeAtMidpoint(inData, vecLen, l, pointAxis, t, Y_AXIS, jIndex),
                           derivativeAtMidpoint(inData, vecLen, l, pointAxis, t, Z_AXIS, kIndex)
                        };
            float[] realNormal;
            if (invAffine != null) {
                realNormal = new float[3];
                for (int i = 0; i < 3; i++) {
                    realNormal[i] = 0;
                    for (int j = 0; j < 3; j++)
                        realNormal[i] += invAffine[i][j] * indexNormal[j];
                }
            }
            else {
                float[][] jacobian = new float[][] {
                    coordDerivativeAtMidpoint(coords, l, pointAxis, t, X_AXIS, iIndex),
                    coordDerivativeAtMidpoint(coords, l, pointAxis, t, Y_AXIS, jIndex),
                    coordDerivativeAtMidpoint(coords, l, pointAxis, t, Z_AXIS, kIndex)
                };
                try {
                    realNormal = MatrixMath.lsolve3x3(jacobian, indexNormal);
                } catch (Exception e) {
                    realNormal = new float[] {1, 0, 0};
                }

            }
            return new float[][] {indexNormal, realNormal};
        }

    /**
     * finding isosurface  nodes and node normals on lines parallel to x axis
     * @return last used index of isosurface point
     * */
        private int xIntersectionPoints(int sliceZ,
                                        float[] slice, boolean[] maskSlice,
                                        int[][][] edgeIndices, int newPointIndex)
        {
            int[][] newNodeInd = new int[reducedDims[1]][reducedDims[0]];
            long origK = low[2] + sliceZ * down[2];
            for (int yIndex = 0; yIndex < reducedDims[1]; yIndex++)
            {
                long origJ = low[1] + yIndex * down[1];
                for (int xIndex = 0, ptIndex = yIndex * reducedDims[0];
                         xIndex < reducedDims[0] - 1;
                         xIndex++, ptIndex++) {
                    long origI = low[0] + xIndex * down[0];
                    // x, y are indices of first edge node
                    float u = slice[ptIndex];
                    float v = slice[ptIndex + 1];
                    if (u * v < 0 &&
                       (!isMask || (maskSlice[ptIndex] && maskSlice[ptIndex + 1]))) {
                        // isosurface cuts the edge between valid points
                        long l = (origK * lDims[1] + origJ) * lDims[0] + origI;
                        float t = u / (u - v);
                        nodes.add(new NewNode(l, l + offsets[0], t));
                        float[][] nvs = gradientAtMidpoint(l, X_AXIS, t, origI, origJ, origK);
                        normals.add(nvs[0]);
                        mappedNormals.add(nvs[1]);
                        newNodeInd[yIndex][xIndex] = newPointIndex;
                        newPointIndex += 1;
                    }
                    else
                        newNodeInd[yIndex][xIndex] = -1;
                }
            }
            edgeIndices[3] = newNodeInd;
            return newPointIndex;
        }

        private int yIntersectionPoints(int sliceZ,
                                        float[] slice, boolean[] maskSlice,
                                        int[][][] edgeIndices, int newPointIndex)
        {
            int[][] eInd = new int[reducedDims[1]][reducedDims[0]];
            long origK = low[2] + sliceZ * down[2];
            for (int yIndex = 0; yIndex < reducedDims[1] - 1; yIndex++) {
                long origJ = low[1] + yIndex * down[1];
                for (int xIndex = 0, ptIndex = yIndex * reducedDims[0];
                         xIndex < reducedDims[0];
                         xIndex++, ptIndex++) {
                    long origI = low[0] + xIndex * down[0];
                    // x, y are indices of first edge node
                    float u = slice[ptIndex];
                    float v = slice[ptIndex + reducedDims[0]];
                    if (u * v < 0 &&
                       (!isMask || (maskSlice[ptIndex] && maskSlice[ptIndex + reducedDims[0]]))) {
                        // isosurface cuts the edge between valid points
                        long l = (origK * lDims[1] + origJ) * lDims[0] + origI;
                        float t = u / (u - v);
                        nodes.add(new NewNode(l, l + offsets[1], t));
                        float[][] nvs = gradientAtMidpoint(l, Y_AXIS, t, origI, origJ, origK);
                        normals.add(nvs[0]);
                        mappedNormals.add(nvs[1]);
                        eInd[yIndex][xIndex] = newPointIndex;
                        newPointIndex += 1;
                    }
                    else
                        eInd[yIndex][xIndex] = -1;
                }
            }
            edgeIndices[4] = eInd;
            return newPointIndex;
        }

        private int zIntersectionPoints(int sliceZ,
                                        float[][] currentLayer, boolean[][] currentLayerMask,
                                        int[][][] edgeIndices, int newPointIndex)
        {
            int[][] eInd = new int[reducedDims[1]][reducedDims[0]];
            long origK = low[2] + sliceZ * down[2];
            for (int yIndex = 0; yIndex < reducedDims[1]; yIndex++) //finding points in z lines
            {
                long origJ = low[1] + yIndex * down[1];
                for (int xIndex = 0, ptIndex = yIndex * reducedDims[0];
                         xIndex < reducedDims[0];
                         xIndex++, ptIndex++) {
                    long origI = low[0] + xIndex * down[0];
                    float u = currentLayer[0][ptIndex];
                    float v = currentLayer[1][ptIndex];
                    if (u * v < 0 &&
                       (!isMask || (currentLayerMask[0][ptIndex] && currentLayerMask[1][ptIndex]))) {
                        // isosurface cuts the edge
                        long l = (origK * lDims[1] + origJ) * lDims[0] + origI;
                        float t = u / (u - v);
                        nodes.add(new NewNode(l, l + offsets[2],t));
                        float[][] nvs = gradientAtMidpoint(l, Z_AXIS, t, origI, origJ, origK);
                        normals.add(nvs[0]);
                        mappedNormals.add(nvs[1]);
                        eInd[yIndex][xIndex] = newPointIndex;
                        newPointIndex += 1;
                    }
                    else
                        eInd[yIndex][xIndex] = -1;
                }
            }
            edgeIndices[2] = eInd;
            return newPointIndex;
        }

        private void updateHexCodes(float[] slice, short[][]  hexCodes)
        {
            for (short[] row : hexCodes) {
                for (int j = 0; j < row.length; j++) {
                    row[j] = (short) (row[j] >> 4);
                }
            }
            for (int i = 0; i < reducedDims[1] - 1; i++) {
                short[] row = hexCodes[i];
                for (int j = 0, k = reducedDims[0] * i; j < reducedDims[0] - 1; j++, k++) {
                    if (slice[k] > 0)
                        row[j] |= 1 << 4;
                    if (slice[k + 1] > 0)
                        row[j] |= 1 << 5;
                    if (slice[k + reducedDims[0]] > 0)
                        row[j] |= 1 << 6;
                    if (slice[k + reducedDims[0] + 1] > 0)
                        row[j] |= 1 << 7;
                }
            }
        }

        @Override
        public void run() {
            nTriangles = 0;
            int startLastLayer = 0;
            currentLayer[1] = getSlice(firstSlice);
            if (isMask)
                currentLayerMask[1] = getMaskSlice(firstSlice);
            updateHexCodes(currentLayer[1], hexCodes);
            newPointIndex = xIntersectionPoints(firstSlice, currentLayer[1], currentLayerMask[1], edgeIndices, newPointIndex);
            newPointIndex = yIntersectionPoints(firstSlice, currentLayer[1], currentLayerMask[1], edgeIndices, newPointIndex);
            for (int iSlice = firstSlice; iSlice < lastSlice - 1; iSlice++) {
                ArrayList<int[]> currentTriangles = new ArrayList<>();
                ArrayList<int[]> currentEdges = new ArrayList<>();
                currentLayer[0] = currentLayer[1];
                currentLayer[1] = getSlice(iSlice + 1);
                if (isMask) {
                    currentLayerMask[0] = currentLayerMask[1];
                    currentLayerMask[1] = getMaskSlice(iSlice + 1);
                }
                edgeIndices[0]  = edgeIndices[3];
                edgeIndices[1]  = edgeIndices[4];
                updateHexCodes(currentLayer[1], hexCodes);
                newPointIndex = zIntersectionPoints(iSlice, currentLayer, currentLayerMask, edgeIndices, newPointIndex);
                if (iSlice == lastSlice - 2 && iThread < nThreads - 1)
                    startLastLayer = newPointIndex;
                newPointIndex = xIntersectionPoints(iSlice + 1, currentLayer[1], currentLayerMask[1], edgeIndices, newPointIndex);
                newPointIndex = yIntersectionPoints(iSlice + 1, currentLayer[1], currentLayerMask[1], edgeIndices, newPointIndex);

                for (int iy = 0; iy < reducedDims[1] - 1; iy++) {
                    for (int ix = 0; ix < reducedDims[0] - 1; ix++) {
                        int parity = (iSlice + iy + ix) % 2;
                        int[][] trIn = ISOSURFACE_TRIANGLES[parity][hexCodes[iy][ix]];
                        for (int[] triangleIn : trIn) {
                            int[] triangle = new int[3];
                            for (int iTriangleNode = 0; iTriangleNode < 3; iTriangleNode++) {
                                int edgeIndex = triangleIn[iTriangleNode];
                                triangle[iTriangleNode] =
                                        edgeIndices[MID_POINT_INDICES[edgeIndex][0]]
                                              [iy + MID_POINT_INDICES[edgeIndex][2]]
                                              [ix + MID_POINT_INDICES[edgeIndex][1]];
                            }
                            if (triangle[0] == -1 || triangle[1] == -1 || triangle[2] == -1)
                                continue;
                            float[] v1 = new float[3];
                            float[] v2 = new float[3];
                            for (int i = 0; i < 3; i++) {
                                v1[i] = MID_POINT_COORDS[triangleIn[1]][i] - MID_POINT_COORDS[triangleIn[0]][i];
                                v2[i] = MID_POINT_COORDS[triangleIn[2]][i] - MID_POINT_COORDS[triangleIn[0]][i];
                            }
                            float[] normal = normals.get(triangle[0]);
                            if ((v1[1] * v2[2] - v1[2] * v2[1]) * normal[0] +
                                (v1[2] * v2[0] - v1[0] * v2[2]) * normal[1] +
                                (v1[0] * v2[1] - v1[1] * v2[0]) * normal[2] > 0) {
                                int tmp = triangle[1];
                                triangle[1] = triangle[2];
                                triangle[2] = tmp;
                            }
                            currentTriangles.add(triangle);
                        }
                        if (createGrid) {
                            int[][] edgesIn = ISOSURFACE_EDGES[parity][hexCodes[iy][ix]];
                            for (int[] edgeIn : edgesIn) {
                                int[] edge = new int[2];
                                for (int iEdgeNode = 0; iEdgeNode < 2; iEdgeNode++) {
                                    int edgeIndex = edgeIn[iEdgeNode];
                                    edge[iEdgeNode] =
                                            edgeIndices[MID_POINT_INDICES[edgeIndex][0]]
                                                  [iy + MID_POINT_INDICES[edgeIndex][2]]
                                                  [ix + MID_POINT_INDICES[edgeIndex][1]];
                                }
                                if (edge[0] >= 0 && edge[1] >= 0)
                                    currentEdges.add(edge);
                            }

                        }
                    }
                }
                nTriangles += currentTriangles.size();
                triangles[iSlice - firstSlice] = new int[3 * currentTriangles.size()];
                int l = 0;
                for (int[] currentTriangle : currentTriangles) {
                    System.arraycopy(currentTriangle, 0, triangles[iSlice - firstSlice], l, 3);
                    l += 3;
                }
                if (createGrid) {
                    nGridEdges += currentEdges.size();
                    gridEdges[iSlice - firstSlice] = new int[2 * currentEdges.size()];
                    l = 0;
                    for (int[] currentEdge : currentEdges) {
                        System.arraycopy(currentEdge, 0, gridEdges[iSlice - firstSlice], l, 2);
                        l += 2;
                    }
                }
            }
            if (iThread < nThreads - 1)
                overlap = newPointIndex - startLastLayer;
            else
                overlap = 0;
        }
    }

    /**
     *
     * @param sliceRemap a single function class;
     * Currently used in the standard isosurface module to wobble slightly values of the isosurfaced component
     * to avoid hard to resolve cases of isosurface passing exactly through a grid node;
     * Used in the SurfaceCompute class to create the boundary surface of the area of fixed map value
     */
    public void setSliceRemap(SliceRemap sliceRemap) {
        this.sliceRemap = sliceRemap;
    }

    /**
     * Creates a new instance of RegularFieldIsosurfaceV1
     * @param in inpur regular field
     */
    public RegularFieldIsosurface(RegularField in)
    {
        this.inField = in;
        if (inField.hasCoords()) {
            coords = inField.getCurrentCoords();
            invAffine = null;
        }
        else {
            invAffine = inField.getInvAffine();
            coords = null;
        }
    }

    /**
     * Creates isosurface of a data component of a RegularField in at threshold value threshold
     * and interpolates selected data components on the surface/line mesh
     * Isosurface is generated from the input field cropped and downsized according to the parameters
     * <p>
     * @param params Parameters of isosurface
     * <p>
     * @param threshold
     * @return a TriangulatedField2D containing surface representation and optimized line representation of the
     *         isosurface with outData components interpolated
     */
    @Override
    public IrregularField makeIsosurface(IsosurfaceEngineParams params, float threshold)
    {
        int comp = params.getIsoComponent();
        int[] low = params.getLow();
        int[] up = params.getUp();
        int[] down = params.getDownsize();
        if (inField == null ||
            inField.getLDims().length != 3 ||
            comp < 0 ||
            comp >= inField.getNComponents() ||
            threshold < inField.getComponent(comp).getPreferredMinValue() ||
            threshold > inField.getComponent(comp).getPreferredMaxValue()||
            down[0] * down[1] * down[2] == 0)
            return null;
        long[] lDims = inField.getLDims();
        int[] rdims  = new int[3];
        for (int i = 0; i < 3; i++) {
            if (low[i] < 0 || up[i] > lDims[i] || (up[i] - low[i]) / down[0] < 2)
                return null;
            rdims[i] =  (up[i] - low[i]) / down[i];
        }
        DataArray data = inField.getComponent(comp);

        createGrid = params.isGrid();

        int nThreads = FastMath.min((up[2] - low[2]) / (50 * down[2]) + 1, VisNow.availableProcessors());

        ConcurrencyUtils.setNumberOfThreads(nThreads);
        Future<?>[] futures = new Future<?>[nThreads];
        IsosurfacePart[] isosurfaceSegments = new IsosurfacePart[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            isosurfaceSegments[iThread] =
                new IsosurfacePart(iThread, nThreads,
                                  lDims, low, down, rdims,
                                  data.getVectorLength(), data.getRawArray(params.getTime()),
                                  inField.hasMask() ? inField.getCurrentMask() : null,
                                  sliceRemap, threshold);
            futures[iThread] = ConcurrencyUtils.submit(isosurfaceSegments[iThread]);
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            VisNow.get().userMessageSend(null, "Error while computing isosurface",
                                         "See log for details.", Level.ERROR);
            return null;
        }

        long nOutNodes  = 0;
        long nTriangles = 0;
        long nTmpEdges     = 0;
        for (int i = 0; i < nThreads; i++) {
            nOutNodes +=  isosurfaceSegments[i].getNodes().size() - isosurfaceSegments[i].getOverlap();
            nTriangles += isosurfaceSegments[i].getnTriangles();
            nTmpEdges +=  isosurfaceSegments[i].getnGridEdges();
        }
        if (nOutNodes < 1 || nOutNodes > 2<<26 || nTriangles > 2<<27)
            return null;

        NewNode[] outNodes = new NewNode[(int)nOutNodes];
        FloatLargeArray outNormals = new FloatLargeArray(3 * nOutNodes, false);

        int[] outTriangles = new int[3 * (int)nTriangles];
        int[] tmpEdges = new int[2 * (int)nTmpEdges];

        for (int iThread = 0, l = 0, lt = 0, le = 0; iThread < nThreads; iThread++) {
            int off = l;
            ArrayList<NewNode> nodes =   isosurfaceSegments[iThread].getNodes();
            ArrayList<float[]> normals = isosurfaceSegments[iThread].getMappedNormals();
            for (int i = 0; i < nodes.size() - isosurfaceSegments[iThread].getOverlap(); i++, l++) {
                outNodes[l] = nodes.get(i);
                LargeArrayUtils.arraycopy(normals.get(i), 0, outNormals, 3 * l, 3);
            }
            int[][] triangles = isosurfaceSegments[iThread].getTriangles();
            for (int[] triangle : triangles) {
                for (int j = 0; j < triangle.length; j++, lt++)
                    outTriangles[lt] = off + triangle[j];
            }
            if (nTmpEdges > 0) {
                int[][] edges = isosurfaceSegments[iThread].getGridEdges();
                for (int[] edge : edges) {
                    for (int j = 0; j < edge.length; j++, le++)
                        tmpEdges[le] = off + edge[j];
                }
            }
        }

        IrregularField outField = new IrregularField(nOutNodes);
        LinearInterpolation.interpolateFieldToNewNodesSet(inField, outField,
                                                          outNodes, data.isTimeDependant());

        CellSet cs = new CellSet("isosurface");
        cs.addCells(new CellArray(CellType.TRIANGLE, outTriangles, null, null));
        if (nTmpEdges > 0) {
            int edgeArrLength = 2;
            int[] indices = new int[ (int)nTmpEdges];
            for (int i = 0; i < indices.length; i++)
                indices[i] = i;
            IntVectorHeapSort.sort(tmpEdges, indices, 2);

            for (int i = 2, l = 0; i < tmpEdges.length; i += 2)
                if (tmpEdges[i] != tmpEdges[l] || tmpEdges[i + 1] != tmpEdges[l + 1]) {
                    l += 2;
                    tmpEdges[l]     = tmpEdges[i];
                    tmpEdges[l + 1] = tmpEdges[i + 1];
                    edgeArrLength += 2;
                }
            int[] edges = new int[edgeArrLength];
            System.arraycopy(tmpEdges, 0, edges, 0, edges.length);
            cs.addCells(new CellArray(CellType.SEGMENT, edges, null, null));
        }
        outField.addCellSet(cs);

        FloatLargeArray uncertainty = null;
        if (params.isUncertainty())
            uncertainty = new FloatLargeArray(3 * nOutNodes);
        for (long i = 0; i < nOutNodes; i++) {
            float normSq = 0;
            for (int j = 0; j < 3; j++)
                normSq += outNormals.getFloat(3 * i + j) * outNormals.getFloat(3 * i + j);
            if (normSq > 0) {
                if (uncertainty != null)
                    for (int j = 0; j < 3; j++)
                        uncertainty.setFloat(3 * i + j, outNormals.getFloat(3 * i + j) / normSq);
                normSq = (float)FastMath.sqrt(normSq);
                for (int j = 0; j < 3; j++)
                    outNormals.setFloat(3 * i + j, outNormals.getFloat(3 * i + j) / normSq);
            }
            else {
                for (int j = 0; j < 3; j++)
                    outNormals.setFloat(3 * i + j, 0);
                if (uncertainty != null)
                    for (int j = 0; j < 3; j++)
                        uncertainty.setFloat(3 * i + j, 0);
            }
        }
        if (uncertainty != null)
            outField.addComponent(DataArray.create(uncertainty, 3, "uncertainty"));
        outField.setNormals(outNormals);
        return outField;
    }

}

