/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.*;
import org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.NewCell;
import org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.PreservedNodes;
import org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.Subset;
import static org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.Subset.*;
import static org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.subset;
import static org.visnow.vn.lib.utils.field.subset.subvolume.SimplexSubCell.SIMPLEX_TYPE;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class IrregularFieldSplitter extends FieldSplitter
{

    private IrregularFieldSplitter()
    {
    }
    
    private static void splitCell (CellType cellType, int[] cVerts, float[] vals, float[] cellNormals, int index, 
                                   HashMap<Long, NewNode>newNodes, ArrayList<NewCell> newCells, PreservedNodes oldNodes)
    {
        int dim = cellType.getDim();
        Cell cell = Cell.createCell(cellType, cVerts, (byte)0);
        int[][] triangulatedVerts = cell.triangulationVertices();
        for (int i = 0; i < triangulatedVerts.length; i++) {
            int[] tVerts = triangulatedVerts[i];
            float[] tVals = new float[tVerts.length];
            for (int j = 0; j < tVals.length; j++) 
                tVals[j] = vals[tVerts[j]];
            Subset type = subset(cellType, tVals);
            switch (type) {
            case VOID:
                break;
            case NONTRIVIAL:
                NewCell subCell = SimplexSubCell.cutSimplex(tVerts, tVals, 1, index, cellNormals, oldNodes);
                NewNode[] newVerts = subCell.newVerts;
                for (int j = 0; j < newVerts.length; j++) 
                    if (newVerts[j] != null) {
                        long key = newVerts[j].getHash();
                        if (newNodes.containsKey(key))
                            newVerts[j] = newNodes.get(key);
                        else
                            newNodes.put(key, newVerts[j]);
                    }
                newCells.add(subCell);   
                break;
            case WHOLE:
                newCells.add(new NewCell(SIMPLEX_TYPE[dim], tVerts, new NewNode[3], cellNormals, 0, oldNodes));
                break;
            default:
            }
        }
    }
    
    private static void sliceCell (CellType cellType, int[] cVerts, float[] vals, float[] cellNormals, int index, 
                                   HashMap<Long, NewNode>newNodes, ArrayList<NewCell> newCells, PreservedNodes oldNodes)
    {
        int dim = cellType.getDim();
        Cell cell = Cell.createCell(cellType, cVerts, (byte)0);
        int[][] triangulatedVerts = cell.triangulationVertices();
        for (int[] tVerts : triangulatedVerts) {
            float[] tVals = new float[tVerts.length];
            for (int j = 0; j < tVals.length; j++) 
                tVals[j] = vals[tVerts[j]];
            Subset type = slice(cellType, tVals);
            switch (type) {
                case VOID:
                    break;
                case NONTRIVIAL:
                    NewCell subCell = SimplexSubCell.sliceSimplex(tVerts, tVals, index, cellNormals, oldNodes);
                    if (subCell == null)
                        break;
                    NewNode[] newVerts = subCell.newVerts;
                    for (int j = 0; j < newVerts.length; j++)
                        if (newVerts[j] != null) {
                            long key = newVerts[j].getHash();
                            if (newNodes.containsKey(key))
                                newVerts[j] = newNodes.get(key);
                            else
                                newNodes.put(key, newVerts[j]);
                        }
                    newCells.add(subCell);
                    break;
                case WHOLE:
                    newCells.add(new NewCell(SIMPLEX_TYPE[dim], tVerts, new NewNode[3], cellNormals, 0, oldNodes));
                    break;
                default:
            }
        }
    }
    
    private static float[] computeThresholdingVals(IrregularField in, LargeArray thresholdedVals, int veclen, float threshold, boolean above)
    {
        return computeThresholdingVals(in, thresholdedVals.getFloatData(), veclen, threshold, above);
    }
    
    private static float[] computeThresholdingVals(IrregularField in, float[] thresholdedVals, int veclen, float threshold, boolean above)
    {
        float[] thrVals = new float[(int)in.getNNodes()];
        for (int i = 0; i < thrVals.length; i++) {
            float thresholdedVal = 0;
            if (veclen == 1)
                thresholdedVal = thresholdedVals[i];
            else {
                for (int j = 0, l = veclen * i; j < veclen; j++, l++)
                    thresholdedVal += thresholdedVals[l] * thresholdedVals[l];
                thresholdedVal = (float)Math.sqrt(thresholdedVal);
            }
            thrVals[i] = above ? thresholdedVal - threshold :
                              threshold - thresholdedVal;
        }
        return thrVals;
    }
    
    private static IrregularField createOutfield(IrregularField in, 
                                                 HashMap<CellSet, ArrayList<NewCell>> newCellsInSets,
                                                 HashMap<Long, NewNode> newNodes, 
                                                 PreservedNodes oldNodes)
    {
        int[] retainedNodeIndices = oldNodes.getIndices();
        int nOldNodes = oldNodes.getnPreservedNodes();
        
        int nOutNodes = nOldNodes;
        for (NewNode node : newNodes.values()) {
            node.setIndex(nOutNodes);
            nOutNodes += 1;
        }
        IrregularField outField = new IrregularField(nOutNodes);
        FloatLargeArray outCoords = interpolateCoords(in, nOutNodes, retainedNodeIndices, newNodes);
        outField.setCurrentCoords(outCoords);
        
        interpolateData(in, outField, nOutNodes, retainedNodeIndices, newNodes);
        
        for (CellSet cellSet : in.getCellSets()) {
            CellSet outCellSet = new CellSet(cellSet.getName());
            
            ArrayList<Cell> cellsInSet = new  ArrayList<>();
            for (NewCell newCell : newCellsInSets.get(cellSet)) {
                Cell cell = outputCell(newCell, outCoords);
                if (cell != null)
                    cellsInSet.add(cell);
            }
            Collections.sort(cellsInSet);
            int[] nOutCells = new int[Cell.getNProperCellTypes()];
            int[] iOutCells = new int[Cell.getNProperCellTypes()];
            int[][] outNodes = new int[Cell.getNProperCellTypes()][];
            byte[][] orientations = new byte[Cell.getNProperCellTypes()][];
            int[][] dataIndices = new int[Cell.getNProperCellTypes()][];
            for (int i = 0; i < cellsInSet.size(); i++) {
                Cell newCell = cellsInSet.get(i);
                if (i == 0 || !newCell.equalsIgnoreOrientation(cellsInSet.get(i - 1)))
                    nOutCells[newCell.getType().getValue()] += 1;
            }
            
            
            for (int i = 0; i < Cell.getNProperCellTypes(); i++) 
                if (nOutCells[i] != 0) {
                    int nOutCellNodes = CellType.getType(i).getNVertices();
                    outNodes[i] = new int[nOutCells[i] * nOutCellNodes];
                    orientations[i] = new byte[nOutCells[i]];
                    iOutCells[i] = 0;
                }

            for (int i = 0; i < cellsInSet.size(); i++) {
                Cell newCell = cellsInSet.get(i);
                if (i != 0 && newCell.equalsIgnoreOrientation(cellsInSet.get(i - 1)))
                    continue;
                int iCellArr = newCell.getType().getValue();
                System.arraycopy(newCell.getVertices(), 0, 
                                 outNodes[iCellArr],    iOutCells[iCellArr] * newCell.getNVertices(), 
                                 newCell.getNVertices());
                orientations[iCellArr][iOutCells[iCellArr]] = newCell.getOrientation();
                iOutCells[iCellArr] += 1;
            }
            
            for (int i = 0; i < nOutCells.length; i++) 
                if (nOutCells[i] != 0) {
                    CellArray ca = new CellArray(Cell.getProperCellTypes()[i], outNodes[i], orientations[i], dataIndices[i]);
                    outCellSet.addCells(ca);
                }
            outField.addCellSet(outCellSet);
        }
        return outField;
    }
    
    public static IrregularField splitField(IrregularField in, LargeArray thresholdedVals, int veclen, float threshold, boolean above, float[] normal)
    {
        float[] normalVector;
        HashMap<CellSet, ArrayList<NewCell>> newCellsInSets = new HashMap<>();
        HashMap<Long, NewNode> newNodes = new HashMap<>();
        PreservedNodes oldNodes = new PreservedNodes((int)in.getNNodes());
        float[] thrVals = computeThresholdingVals(in, thresholdedVals, veclen, threshold, above);
        int trueNSpace = in.getTrueNSpace();
        boolean emptyOut = true;
        for (CellSet cellSet : in.getCellSets()) {
            ArrayList<NewCell> newCellsInSet = new ArrayList<>();
            for (CellArray cellArray : cellSet.getCellArrays()) {
                if (cellArray == null || cellArray.getNCells() < 1)
                    continue;
                CellType type = cellArray.getType();
                int dim = type.getDim();
                int cellNNodes = cellArray.getNCellNodes();
                int[] nodes = cellArray.getNodes();
                float[] normals = null;
                FloatLargeArray cellArrayNormals = cellArray.getCellNormals();
                if (cellArrayNormals != null)
                    normals = cellArrayNormals.getData();
                for (int iInCell = 0; iInCell <  cellArray.getNCells(); iInCell++) {
                    Cell cell = cellArray.getCell(iInCell);
                    int[] verts = new int[cellNNodes];
                    float[] vals = new float[cellNNodes];
                    int index = 0;
                    if (cellArray.getDataIndices() != null)
                        index = cellArray.getDataIndices()[iInCell];
                    for (int i = 0; i < cellNNodes; i++) {
                        int iNode = verts[i] = nodes[cellNNodes * iInCell + i];
                        vals[i] = thrVals[iNode];
                    }
                    if (normal != null && normal.length == 3)
                        normalVector = Arrays.copyOfRange(normal, 0, 3);
                    else if (trueNSpace < 2 && dim == 2 && normals != null && normals.length == 3 * cellArray.getNCells())
                        normalVector = Arrays.copyOfRange(normals, 3 * iInCell, 3 * iInCell + 3);
                    else 
                        normalVector = new float[] {0, 0, 1};
                    switch (subset(type, vals))
                    {
                    case VOID:
                        break;
                    case NONTRIVIAL:
                        splitCell(type, verts, thrVals, normalVector, index, newNodes, newCellsInSet, oldNodes);
                        break;
                    case WHOLE:
                        newCellsInSet.add(new NewCell(type, verts, new NewNode[verts.length], normalVector, index, oldNodes));
                        break;
                    case TRIANGULATED:
                        int[][] tetsVerts = cell.triangulationVertices();
                        for (int[] tetVerts : tetsVerts) 
                            newCellsInSet.add(new NewCell(TETRA, tetVerts, new NewNode[4], normalVector, index, oldNodes));
                        break;
                    }
                }
            }
            if (!newCellsInSet.isEmpty())
                emptyOut = false;
            newCellsInSets.put(cellSet, newCellsInSet);
        }
        
        if (emptyOut)
            return null;
        return createOutfield(in, newCellsInSets, newNodes, oldNodes);
    }
    
    public static IrregularField splitField(IrregularField in, LargeArray thresholdedVals, int veclen, float threshold, boolean above)
    {
        return splitField(in, thresholdedVals, veclen, threshold, above, null);
    }
    
    public static IrregularField sliceField(IrregularField in, float[] thresholdedVals, int veclen, float threshold, float[] normalVector)
    {
        HashMap<CellSet, ArrayList<NewCell>> newCellsInSets = new HashMap<>();
        HashMap<Long, NewNode> newNodes = new HashMap<>();
        PreservedNodes oldNodes = new PreservedNodes((int)in.getNNodes());
        float[] thrVals = computeThresholdingVals(in, thresholdedVals, veclen, threshold, true);
        boolean emptyOut = true;
        for (CellSet cellSet : in.getCellSets()) {
            ArrayList<NewCell> newCellsInSet = new ArrayList<>();
            for (CellArray cellArray : cellSet.getCellArrays()) {
                if (cellArray == null || cellArray.getNCells() < 1)
                    continue;
                CellType type = cellArray.getType();
                int cellNNodes = cellArray.getNCellNodes();
                int[] nodes = cellArray.getNodes();
                for (int iInCell = 0; iInCell <  cellArray.getNCells(); iInCell++) {
                    int[] verts = new int[cellNNodes];
                    float[] vals = new float[cellNNodes];
                    int index = 0;
                    if (cellArray.getDataIndices() != null)
                        index = cellArray.getDataIndices()[iInCell];
                    for (int i = 0; i < cellNNodes; i++) {
                        int iNode = verts[i] = nodes[cellNNodes * iInCell + i];
                        vals[i] = thrVals[iNode];
                    }
                    switch (slice(type, vals))
                    {
                    case VOID:
                        break;
                    case NONTRIVIAL:
                        sliceCell(type, verts, thrVals, normalVector, index, newNodes, newCellsInSet, oldNodes);
                        break;
                    }
                }
            }
            if (!newCellsInSet.isEmpty())
                emptyOut = false;
            newCellsInSets.put(cellSet, newCellsInSet);
        }
        
        if (emptyOut)
            return null;
        return createOutfield(in, newCellsInSets, newNodes, oldNodes);
    }
    
}
