/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isosurface;

import org.visnow.jscic.IrregularField;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 * This is an abstract class for creating engines computing isosurfaces.
 *
 * The process of creation an engine for given new field and computing isosurface
 * is decoupled. This allows to precompute some data and speed up creation
 * of isosurface on parameter changes.
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public abstract class IsosurfaceEngine
{
    public static final String CELLSET_NAME = "Isosurface";
    
    public abstract IrregularField makeIsosurface(IsosurfaceEngineParams params, float threshold);

    protected transient FloatValueModificationListener statusListener = null;

    public void clearFloatValueModificationListener()
    {
        statusListener = null;
    }

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null) {
            this.statusListener = listener;
        } else {
            throw new RuntimeException("" + this + ": only one status listener can be added");
        }
    }

    protected void fireStatusChanged(float status)
    {
        if (statusListener != null) {
            FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
            statusListener.floatValueChanged(e);
        }
    }
}
