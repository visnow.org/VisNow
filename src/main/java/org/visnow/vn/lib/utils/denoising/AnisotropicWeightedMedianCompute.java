/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.denoising;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.utils.numeric.HeapSort;
import org.visnow.vn.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.engine.core.ProgressAgent;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class AnisotropicWeightedMedianCompute extends AbstractAnisotropicWeightedMedianCompute
{
    private static final Logger LOGGER = Logger.getLogger(AnisotropicWeightedMedianCompute.class);

    private int[] dims;
    private int radius;
    private int anisotropyVlen;
    private float slope, slope1;
    private float[] anisotropy;

    ProgressAgent progressAgent;

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params)
    {
        return compute(inField, anisotropyField, params, ProgressAgent.getDummyAgent());
    }

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params, ProgressAgent progressAgent)
    {
        if (inField == null)
            return null;

        this.progressAgent = progressAgent;

        radius = params.getRadius();
        slope = params.getSlope();
        slope *= slope;
        slope1 = params.getSlope1();
        slope1 *= slope1;
        dims = inField.getDims();
        int[] outDims = new int[dims.length];
        float[][] inPts = inField.getPreferredExtents();
        float[][] outPts = new float[2][3];
        int outNdata = 1;
        int vlen;
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < dims.length; i++) {
            outDims[i] = dims[i];
            outNdata *= outDims[i];
            outPts[0][i] = inPts[0][i];
            outPts[1][i] = inPts[1][i];
        }
        RegularField outField = new RegularField(outDims);
        outField.setAffine(MatrixMath.computeAffineFromExtents(outDims, outPts));
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (int nComponent = 0; nComponent < params.getComponentsNumber(); nComponent++) {
            int component = params.getComponent(nComponent);
            int anisotropyComponent = params.getAnisotropyComponent(nComponent);
            DataArray dataArr = inField.getComponent(component);
            LOGGER.info("averaging component " + component + "(" + dataArr.getName() + ")");
            if (anisotropyField == null || anisotropyComponent < 0 || anisotropyField.getComponent(anisotropyComponent) == null) {
                anisotropy = null;
            } else {
                DataArray anisotropyArr = anisotropyField.getComponent(anisotropyComponent);
                anisotropyVlen = anisotropyArr.getVectorLength();
                if (anisotropyVlen != 1 && anisotropyVlen != dims.length)
                    return null;
                anisotropy = anisotropyArr.getRawFloatArray().getData();
                LOGGER.info(" with anisotropy component " + anisotropyComponent + "(" + anisotropyArr.getName() + ")");
            }
            vlen = dataArr.getVectorLength();

            long t0 = System.currentTimeMillis();

            LargeArray inData = dataArr.getRawArray();
            LargeArray outData = LargeArrayUtils.create(dataArr.getType().toLargeArrayType(), (long) outNdata * vlen);
            for (int i = 0; i < workThreads.length; i++) {
                workThreads[i] = new Thread(new FilterArray(vlen, nThreads, i, inData, outData));
                workThreads[i].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
            outField.addComponent(DataArray.create(outData, vlen, dataArr.getName()).
                    preferredRanges(inField.getComponent(params.getComponent(nComponent)).getPreferredMinValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredMaxValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredPhysMinValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredPhysMaxValue()));
            LOGGER.info("elapsed time " + (System.currentTimeMillis() - t0) / (1000.f));
            LOGGER.info("average slice time using " + nThreads + " thread(s): " +
                    ((System.currentTimeMillis() - t0) / (1000.f * (dims[dims.length - 1] - 2 * radius + 1))));
        }
        if (outField.getNComponents() > 0)
            return outField;
        else
            return null;
    }

    @Override
    public synchronized RegularField computePresmoothing(RegularField inField, AnisotropicDenoisingParams params)
    {
        if (inField == null)
            return null;
        long t = System.currentTimeMillis();
        radius = params.getRadius();
        slope = params.getSlope();
        slope *= slope;
        slope1 = params.getSlope1();
        slope1 *= slope1;
        dims = inField.getDims();
        int[] outDims = new int[dims.length];
        float[][] inPts = inField.getExtents();
        float[][] outPts = new float[2][3];
        int outNdata = 1;
        int vlen;
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < dims.length; i++) {
            outDims[i] = dims[i];
            outNdata *= outDims[i];
            outPts[0][i] = inPts[0][i];
            outPts[1][i] = inPts[1][i];
        }
        RegularField outField = new RegularField(outDims);
        outField.setAffine(MatrixMath.computeAffineFromExtents(outDims, outPts));
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (int nComponent = 0; nComponent < params.getComponentsNumber(); nComponent++) {
            int component = params.getComponent(nComponent);
            DataArray dataArr = inField.getComponent(component);
            LOGGER.info("averaging component " + component + "(" + dataArr.getName() + ")");
            vlen = dataArr.getVectorLength();

            long t0 = System.currentTimeMillis();
            LargeArray inData = dataArr.getRawArray();
            LargeArray outData = new FloatLargeArray(outNdata * vlen);
            for (int i = 0; i < workThreads.length; i++) {
                workThreads[i] = new Thread(new FilterArray(vlen, nThreads, i, inData, outData));
                workThreads[i].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                    LOGGER.info(e.toString());
                }
            outField.addComponent(DataArray.create(outData, vlen, dataArr.getName()).
                    preferredRanges(inField.getComponent(params.getComponent(nComponent)).getPreferredMinValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredMaxValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredPhysMinValue(),
                                    inField.getComponent(params.getComponent(nComponent)).getPreferredPhysMaxValue()));
            LOGGER.info("elapsed time " + (System.currentTimeMillis() - t0) / (1000.f));
            LOGGER.info("average slice time using " + nThreads + " thread(s): " +
                    ((System.currentTimeMillis() - t0) / (1000.f * (dims[dims.length - 1] - 2 * radius + 1))));
        }
        if (outField.getNComponents() > 0)
            return outField;
        else
            return null;
    }

    class FilterArray implements Runnable
    {
        LargeArrayType type;
        int vlen;
        int nThreads;
        int iThread;
        LargeArray inData, outData;
        
        public FilterArray(int vlen, int nThreads, int iThread,
                           LargeArray inData, LargeArray outData)
        {
            type = inData.getType();
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.vlen = vlen;
            this.inData = inData;
            this.outData = outData;
        }

        @Override
        public void run()
        {
            int nSort = 1;
            for (int i = 0; i < dims.length; i++)
                nSort *= (2 * radius + 1);
            float[] r = new float[2 * nSort];
            int rr, kl, ku, jl, ju, il, iu;
            float w, sw;
            for (int v = 0; v < vlen; v++)
                switch (dims.length) {
                    case 3:
                        for (int k0 = iThread; k0 < dims[2]; k0 += nThreads) {
//                        progressAgent.increase();
                            int m = k0 * dims[0] * dims[1] * vlen + v;
                            kl = -radius;
                            if (k0 < radius)
                                kl = -k0;
                            ku = radius;
                            if (k0 >= dims[2] - radius)
                                ku = dims[2] - k0 - 1;
                            for (int j0 = 0; j0 < dims[1]; j0++) {
                                jl = -radius;
                                if (j0 < radius)
                                    jl = -j0;
                                ju = radius;
                                if (j0 >= dims[1] - radius)
                                    ju = dims[1] - j0 - 1;
                                for (int i0 = 0, n0 = (dims[1] * k0 + j0) * dims[0]; i0 < dims[0]; i0++, m += vlen, n0++) {
                                    il = -radius;
                                    if (i0 < radius)
                                        il = -i0;
                                    iu = radius;
                                    if (i0 >= dims[0] - radius)
                                        iu = dims[0] - i0 - 1;
                                    sw = 0.f;
                                    int l = 0;
                                    for (int k = kl; k <= ku; k++)
                                        for (int j = jl; j <= ju; j++)
                                            for (int i = il, n = ((k0 + k) * dims[1] + j + j0) * dims[0] + i0 + il, p = k * k + j * j; i <= iu; i++, n++, l++) {
                                                w = 0;
                                                if (anisotropy != null) {
                                                    if (anisotropyVlen > 1)
                                                        w = k * anisotropy[n0 * anisotropyVlen + 2] +
                                                                j * anisotropy[n0 * anisotropyVlen + 1] +
                                                                i * anisotropy[n0 * anisotropyVlen];
                                                    else
                                                        w = anisotropy[n0] - anisotropy[n];
                                                }
                                                w = (float) (exp(-w * w / slope1 - (p + i * i) / slope));
                                                r[2 * l] = inData.getFloat(n * vlen + v);
                                                r[2 * l + 1] = w;
                                                sw += w;
                                            }
                                    outData.setFloat(m, getMedian(r, l, sw));
                                }
                            }
                        }
                        break;
                    case 2:
                        for (int j0 = iThread; j0 < dims[1]; j0 += nThreads) {
//                        progressAgent.increase();
                            jl = -radius;
                            if (j0 < radius)
                                jl = -j0;
                            ju = radius;
                            if (j0 >= dims[1] - radius)
                                ju = dims[1] - j0 - 1;
                            for (int i0 = 0, n0 = j0 * dims[0], m = j0 * dims[0] * vlen + v; i0 < dims[0]; i0++, m += vlen, n0++) {
                                il = -radius;
                                if (i0 < radius)
                                    il = -i0;
                                iu = radius;
                                if (i0 >= dims[0] - radius)
                                    iu = dims[0] - i0 - 1;
                                sw = 0.f;
                                int l = 0;
                                for (int j = jl; j <= ju; j++)
                                    for (int i = il, n = (j + j0) * dims[0] + i0 + il, p = j * j; i <= iu; i++, n++, l++) {
                                        w = 0;
                                        if (anisotropy != null) {
                                            if (anisotropyVlen > 1)
                                                w = j * anisotropy[n0 * anisotropyVlen + 1] +
                                                        i * anisotropy[n0 * anisotropyVlen];
                                            else
                                                w = anisotropy[n0] - anisotropy[n];
                                        }
                                        w = (float) (exp(-w * w / slope1 - (p + i * i) / slope));
                                        r[2 * l] = inData.getFloat(n * vlen + v);
                                        r[2 * l + 1] = w;
                                        sw += w;
                                    }
                                outData.setFloat(m, getMedian(r, l, sw));
                            }
                        }
                        break;
                    case 1:
                        for (int i0 = iThread, m = iThread; i0 < dims[0]; i0 += nThreads, m += nThreads) {
//                        progressAgent.increase();
                            il = i0 - radius;
                            if (il < 0)
                                il = 0;
                            iu = i0 + radius;
                            if (i0 >= dims[0])
                                iu = dims[0] - 1;
                            sw = 0.f;
                            int l = 0;
                            for (int i = il; i <= iu; i++) {
                                w = 0;
                                if (anisotropy != null)
                                    w = anisotropy[i] - anisotropy[i0];
                                w = (float) (exp(w * w / slope1 - (i - i0) * (i - i0) / slope));
                                r[2 * l] = inData.getFloat(i * vlen + v);
                                r[2 * l + 1] = w;
                                sw += w;
                            }
                            outData.setFloat(m, getMedian(r, l, sw));
                        }
                        break;
                }
        }

        private float getMedian(float[] r, int n, float sw)
        {
            if (n == 0)
                return 0;
            HeapSort.sort(r, n, 2);
            float w = 0, v = 0;
            for (int l = 1; l < 2 * n; l += 2) {
                w += r[l];
                if (w > sw / 2) {
                    if (l > 1) {
                        v = (sw / 2 - v) / r[l];
                        return v * r[l - 1] + (1 - v) * r[l - 3];
                    } else
                        return r[l - 1];
                }
                v = w;
            }
            return r[2 * n - 2];
        }

    }
}
