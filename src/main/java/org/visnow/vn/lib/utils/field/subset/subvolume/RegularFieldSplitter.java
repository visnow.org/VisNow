/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.cells.Quad;
import org.visnow.jscic.cells.RegularHex;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import static org.visnow.jscic.cells.CellType.*;
import static org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.Subset.NONTRIVIAL;
import static org.visnow.vn.lib.utils.field.subset.subvolume.SimplexSubCell.SIMPLEX_TYPE;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularFieldSplitter extends FieldSplitter
{

    private static final CellType[] cellTypes = {POINT, CellType.SEGMENT, CellType.QUAD, CellType.HEXAHEDRON};

    private  RegularFieldSplitter()
    {
    }
    
    private static void sliceCell (int dim, int trueNSpace, int[] cVerts, float[] vals, boolean even,
                                   float[] fieldNormals,
                                   HashMap<Long, NewNode>newNodes, 
                                   ArrayList<NewCell> newCells, PreservedNodes oldNodes)
    {
        float[] normals  = {0, 0, 1};
        if (dim <2 || dim > 3)
            return;
        int[][] triangulatedVerts =   dim == 2 ? Quad.triangulationVertices(cVerts) :
                                                 RegularHex.triangulationVertices(cVerts, even);
        int[][] triangulatedIndices = dim == 2 ? Quad.triangulationIndices(null) :
                                                 RegularHex.triangulationIndices(even);
        for (int i = 0; i < triangulatedVerts.length; i++) {
            int[] tNodes = triangulatedIndices[i];
            int[] tVerts = triangulatedVerts[i];
            float[] tVals = new float[tVerts.length];
            for (int j = 0; j < tVals.length; j++) 
                tVals[j] = vals[tNodes[j]];
            Subset type = slice(dim == 2 ? TRIANGLE : TETRA, tVals);
            switch (type) {
            case VOID:
                break;
            case NONTRIVIAL:
                if (trueNSpace < 2)
                    normals = Arrays.copyOfRange(fieldNormals, 3 * cVerts[0], 3 * cVerts[0] + 3);
                NewCell subCell = SimplexSubCell.sliceSimplex(tVerts, tVals, 0, normals, oldNodes);
                NewNode[] newVerts = subCell.newVerts;
                for (int j = 0; j < newVerts.length; j++) 
                    if (newVerts[j] != null) {
                        long key = newVerts[j].getHash();
                        if (newNodes.containsKey(key))
                            newVerts[j] = newNodes.get(key);
                        else
                            newNodes.put(key, newVerts[j]);
                    }
                newCells.add(subCell);   
                break;
            default:
            }
        }
    }
    
    
    private static void splitCell (int dim, int trueNSpace, int[] cVerts, float[] vals, boolean even,
                                   float[] fieldNormals,
                                   HashMap<Long, NewNode>newNodes, 
                                   ArrayList<NewCell> newCells, PreservedNodes oldNodes)
    {
        float[] normals  = {0, 0, 1};
        if (dim <2 || dim > 3)
            return;
        int[][] triangulatedVerts =   dim == 2 ? Quad.triangulationVertices(cVerts) :
                                                 RegularHex.triangulationVertices(cVerts, even);
        int[][] triangulatedIndices = dim == 2 ? Quad.triangulationIndices(null) :
                                                 RegularHex.triangulationIndices(even);
        for (int i = 0; i < triangulatedVerts.length; i++) {
            int[] tNodes = triangulatedIndices[i];
            int[] tVerts = triangulatedVerts[i];
            float[] tVals = new float[tVerts.length];
            for (int j = 0; j < tVals.length; j++) 
                tVals[j] = vals[tNodes[j]];
            Subset type = subset(dim == 2 ? TRIANGLE : TETRA, tVals);
            switch (type) {
            case VOID:
                break;
            case NONTRIVIAL:
                if (trueNSpace < 2)
                    normals = Arrays.copyOfRange(fieldNormals, 3 * cVerts[0], 3 * cVerts[0] + 3);
                NewCell subCell = SimplexSubCell.cutSimplex(tVerts, tVals, 1, 0, normals, oldNodes);
                NewNode[] newVerts = subCell.newVerts;
                for (int j = 0; j < newVerts.length; j++) 
                    if (newVerts[j] != null) {
                        long key = newVerts[j].getHash();
                        if (newNodes.containsKey(key))
                            newVerts[j] = newNodes.get(key);
                        else
                            newNodes.put(key, newVerts[j]);
                    }
                newCells.add(subCell);   
                break;
            case WHOLE:
                if (trueNSpace < 2)
                    normals = Arrays.copyOfRange(fieldNormals, 3 * cVerts[0], 3 * cVerts[0] + 3);
                newCells.add(new NewCell(SIMPLEX_TYPE[dim], tVerts, new NewNode[tVerts.length], normals, 0, oldNodes));
                break;
            default:
            }
        }
    }
    
    private static IrregularField createOutfield(RegularField in, 
                                                 ArrayList<NewCell> newCells,
                                                 HashMap<Long, NewNode> newNodes, 
                                                 PreservedNodes oldNodes)
    {
        
        int[] dims = in.getDims();
        float[] normals  = {0, 0, 1};
        
        int[] retainedNodeIndices = oldNodes.getIndices();
        int nOldNodes = oldNodes.getnPreservedNodes();
        
        int nOutNodes = nOldNodes;
        for (NewNode node : newNodes.values()) {
            node.setIndex(nOutNodes);
            nOutNodes += 1;
        }
        
        IrregularField outField = new IrregularField(nOutNodes);
        FloatLargeArray coords = in.getCurrentCoords();
        FloatLargeArray outCoords;
        if (coords != null)
            outCoords = interpolateCoords(in, nOutNodes, retainedNodeIndices, newNodes);
        else {
            outCoords = new FloatLargeArray(3 * nOutNodes);
            float[][] affine = in.getAffine();
            switch (dims.length) {
                case 1:
                    for (int i0 = 0; i0 < dims[0]; i0++)
                        if (retainedNodeIndices[i0] >= 0) {
                            int iOut = 3 * retainedNodeIndices[i0];
                            for (int i = 0; i < 3; i++)
                                outCoords.setFloat(iOut + i, affine[3][i] + i0 * affine[0][i]);
                        }
                    break;
                case 2:
                    for (int i1 = 0, l = 0; i1 < dims[1]; i1++)
                        for (int i0 = 0; i0 < dims[0]; i0++, l++)
                            if (retainedNodeIndices[l] >= 0) {
                                int iOut = 3 * retainedNodeIndices[l];
                                for (int i = 0; i < 3; i++)
                                    outCoords.setFloat(iOut + i, affine[3][i] + i1 * affine[1][i] + i0 * affine[0][i]);
                            }
                    break;
                case 3:
                    for (int i2 = 0, l = 0; i2 < dims[2]; i2++)
                        for (int i1 = 0; i1 < dims[1]; i1++)
                            for (int i0 = 0; i0 < dims[0]; i0++, l++)
                                if (retainedNodeIndices[l] >= 0) {
                                    int iOut = 3 * retainedNodeIndices[l];
                                    for (int i = 0; i < 3; i++)
                                        outCoords.setFloat(iOut + i, affine[3][i] + i2 * affine[2][i] + i1 * affine[1][i] + i0 * affine[0][i]);
                                }
                    break;
            }
            for (NewNode node : newNodes.values()) {
                long k0 = node.p0;
                long k1 = node.p1;
                int k = 3 * node.getIndex();
                float r = node.ratio;
                float[] c0 = new float[3];
                float[] c1 = new float[3];
                long i00, i01, i02, i10, i11, i12;
                switch (dims.length) {
                    case 1:
                        for (int i = 0; i < 3; i++) {
                            c0[i] = affine[3][i] + k0 * affine[0][i];
                            c1[i] = affine[3][i] + k1 * affine[0][i];
                        }
                        break;
                    case 2:
                        i00 = k0 % dims[0];
                        i01 = k0 / dims[0];
                        i10 = k1 % dims[0];
                        i11 = k1 / dims[0];
                        for (int i = 0; i < 3; i++) {
                            c0[i] = affine[3][i] + i01 * affine[1][i] + i00 * affine[0][i];
                            c1[i] = affine[3][i] + i11 * affine[1][i] + i10 * affine[0][i];
                        }
                        break;
                    case 3:
                        i00 = k0 % dims[0];
                        i01 = k0 / dims[0];
                        i02 = i01 / dims[1];
                        i01 %= dims[1];
                        i10 = k1 % dims[0];
                        i11 = k1 / dims[0];
                        i12 = i11 / dims[1];
                        i11 %= dims[1];
                        for (int i = 0; i < 3; i++) {
                            c0[i] = affine[3][i] + i02 * affine[2][i] + i01 * affine[1][i] + i00 * affine[0][i];
                            c1[i] = affine[3][i] + i12 * affine[2][i] + i11 * affine[1][i] + i10 * affine[0][i];
                        }
                        break;
                }
                for (int j = 0; j < 3; j++, k++)
                    outCoords.setFloat(k, r * c0[j] + (1 - r) * c1[j]);
            }
        }
        outField.setCurrentCoords(outCoords);
        
        interpolateData(in, outField, nOutNodes, retainedNodeIndices, newNodes);
        
        ArrayList<Cell> cellsInSet = new  ArrayList<>();
        for (NewCell newCell : newCells)
            cellsInSet.add(outputCell(newCell, outCoords));
        Collections.sort(cellsInSet);
        int[] nOutCells = new int[Cell.getNProperCellTypes()];
        int[] iOutCells = new int[Cell.getNProperCellTypes()];
        int[] iOutNodes = new int[Cell.getNProperCellTypes()];
        int[][] outNodes = new int[Cell.getNProperCellTypes()][];
        byte[][] orientations = new byte[Cell.getNProperCellTypes()][];
        for (int i = 0; i < cellsInSet.size(); i++) {
            Cell newCell = cellsInSet.get(i);
            if (i == 0 || !newCell.equalsIgnoreOrientation(cellsInSet.get(i - 1)))
                nOutCells[newCell.getType().getValue()] += 1;
        }
        
        for (int i = 0; i < Cell.getNProperCellTypes(); i++) 
            if (nOutCells[i] != 0) {
                int nOutCellNodes = CellType.getType(i).getNVertices();
                outNodes[i] = new int[nOutCells[i] * nOutCellNodes];
                orientations[i] = new byte[nOutCells[i]];
                iOutCells[i] = iOutNodes[i] = 0;
            }
        
        for (int i = 0; i < cellsInSet.size(); i++) {
            Cell newCell = cellsInSet.get(i);
            if (i != 0 && newCell.equalsIgnoreOrientation(cellsInSet.get(i - 1)))
                continue;
            int iCellArr = newCell.getType().getValue();
            System.arraycopy(newCell.getVertices(), 0, 
                             outNodes[iCellArr],    iOutCells[iCellArr] * newCell.getNVertices(), 
                             newCell.getNVertices());
            orientations[iCellArr][iOutCells[iCellArr]] = newCell.getOrientation();
            iOutCells[iCellArr] += 1;
        }
        CellSet cs = new CellSet();
        for (int i = 0; i < nOutCells.length; i++) 
            if (nOutCells[i] != 0) {
                CellArray ca = new CellArray(Cell.getProperCellTypes()[i], outNodes[i], orientations[i], null);
                cs.addCells(ca);
            }
        outField.addCellSet(cs);
        return outField;
    }
    
    
    public static IrregularField sliceField(RegularField in, float[] thresholdedVals, int veclen, float threshold, float[] normal)
    {
        
        if (in.getNNodes() > Integer.MAX_VALUE / 8)
            return null;
        
        HashMap<Long, NewNode> newNodes = new HashMap<>();
        ArrayList<NewCell> newCells = new ArrayList<>();
        PreservedNodes oldNodes = new PreservedNodes((int)in.getNNodes());
        
        int[] dims = in.getDims();
        float[] fieldNormals = in.getTrueNSpace() > 0 ? null : in.getNormals().getData();
        int[] offsets = in.getCellNodeOffsets();
        int cellNNodes = offsets.length;
        float[] vals = new float[cellNNodes];
        int dim = dims.length;
        switch (dim) {
        case 1:
            break;
        case 2:
            for (int iy = 0; iy < dims[1] - 1; iy++) 
                for (int ix = 0; ix < dims[0] - 1; ix++) {
                    int iInCell = iy * dims[0] + ix;
                    int[] verts = new int[cellNNodes];
                    for (int i = 0; i < cellNNodes; i++) {
                        int iNode = verts[i] = iInCell + offsets[i];
                        float thresholdedVal = 0;
                        if (veclen == 1)
                            thresholdedVal = thresholdedVals[iNode];
                        else {
                            for (int j = 0, l = veclen * iNode; j < veclen; j++, l++)
                                thresholdedVal += thresholdedVals[l] * thresholdedVals[l];
                            thresholdedVal = (float)Math.sqrt(thresholdedVal);
                        }
                        vals[i] = thresholdedVal - threshold;
                    }
                    if (slice(QUAD, vals)  ==  NONTRIVIAL)
                        sliceCell (dim, in.getTrueNSpace(), verts, vals, false, fieldNormals, newNodes, newCells, oldNodes);
                }
            break;
        case 3: 
            for (int iz = 0; iz < dims[2] - 1; iz++) 
                for (int iy = 0; iy < dims[1] - 1; iy++) 
                    for (int ix = 0; ix < dims[0] - 1; ix++) {
                        int iInCell = (iz * dims[1] + iy) * dims[0] + ix;
                        int[] verts = new int[cellNNodes];
                        for (int i = 0; i < cellNNodes; i++) {
                            try {
                            int iNode = verts[i] = iInCell + offsets[i];
                            float thresholdedVal = 0;
                            if (veclen == 1)
                                thresholdedVal = thresholdedVals[iNode];
                            else {
                                for (int j = 0, l = veclen * iNode; j < veclen; j++, l++)
                                    thresholdedVal += thresholdedVals[l] * thresholdedVals[l];
                                thresholdedVal = (float)Math.sqrt(thresholdedVal);
                            }
                            vals[i] = thresholdedVal - threshold;
                                
                            } catch (Exception e) {
                                System.out.println("????");
                            }
                        }
                        if (slice(HEXAHEDRON, vals) == NONTRIVIAL)
                            sliceCell (dim, in.getTrueNSpace(), verts, vals, (ix + iy + iz) % 2 == 0, normal, newNodes, newCells, oldNodes);
                    }
            break;
        default:
            return null;
        }
        if (newCells.isEmpty())
            return null;
        return createOutfield(in, newCells, newNodes, oldNodes);
    }
        
    /**
     * creating a subset of a regular field containing points satisfying an inequality (geometrically exact, splitting cells when necessary)
     * @param in input regular field (triangulated in the process of splitting)
     * @param thresholdedVals a large array used as left hand side of the inequality
     * @param veclen vector length of the data in thresholdedVals (if veclen>1 pontwise vector norm is used at theleft hand side of the inequality)
     * @param threshold right hand side of the inequality
     * @param above direction of the inequality
     * @param globalNormal for 2d fields and a planar slice the vector normal to the field plane
     * @return a field containing nodes and cells satisfying the inequality with subsets of simplices  cut by the inequality criterion
     */
    public static IrregularField splitField(RegularField in, LargeArray thresholdedVals, int veclen, float threshold, boolean above, float[] globalNormal)
    {
        if (in.getNNodes() > Integer.MAX_VALUE / 8)
            return null;
        float[] normal = {0,0,1};
        HashMap<Long, NewNode> newNodes = new HashMap<>();
        ArrayList<NewCell> newCells = new ArrayList<>();
        PreservedNodes oldNodes = new PreservedNodes((int)in.getNNodes());
        
        int[] dims = in.getDims();
        float[] fieldNormals = in.getTrueNSpace() > 0 ? null : in.getNormals().getData();
        int[] offsets = in.getCellNodeOffsets();
        int cellNNodes = offsets.length;
        float[] vals = new float[cellNNodes];
        int dim = dims.length;
        switch (dim) {
        case 1:
            break;
        case 2:
            for (int iy = 0; iy < dims[1] - 1; iy++) 
                for (int ix = 0; ix < dims[0] - 1; ix++) {
                    int iInCell = iy * dims[0] + ix;
                    int[] verts = new int[cellNNodes];
                    for (int i = 0; i < cellNNodes; i++) {
                        int iNode = iInCell + offsets[i];
                        verts[i] = iNode;
                        float thresholdedVal = 0;
                        if (veclen == 1)
                            thresholdedVal = thresholdedVals.getFloat(iNode);
                        else {
                            for (int j = 0, l = veclen * iNode; j < veclen; j++, l++)
                                thresholdedVal += thresholdedVals.getFloat(l) * thresholdedVals.getFloat(l);
                            thresholdedVal = (float)Math.sqrt(thresholdedVal);
                        }
                        vals[i] = above ?thresholdedVal - threshold :
                                         threshold - thresholdedVal;
                    }
                    switch (subset(QUAD, vals)) {
                    case VOID:
                        break;
                    case NONTRIVIAL:
                        splitCell (dim, in.getTrueNSpace(), verts, vals, false, fieldNormals, newNodes, newCells, oldNodes);
                        break;
                    case WHOLE:
                        if (globalNormal != null)
                            normal = Arrays.copyOfRange(globalNormal, 0, 3);
                        else if (in.getTrueNSpace() != 2)
                            normal = Arrays.copyOfRange(fieldNormals, 3 * iInCell, 3 * iInCell + 3);
                        newCells.add(new NewCell(QUAD, verts, new NewNode[4], normal, 0, oldNodes));
                        break;
                    }
                }
            break;
        case 3: 
            for (int iz = 0; iz < dims[2] - 1; iz++) 
                for (int iy = 0; iy < dims[1] - 1; iy++) 
                    for (int ix = 0; ix < dims[0] - 1; ix++) {
                        int iInCell = (iz * dims[1] + iy) * dims[0] + ix;
                        int[] verts = new int[cellNNodes];
                        for (int i = 0; i < cellNNodes; i++) {
                            verts[i] = iInCell + offsets[i];
                            int iNode = iInCell + offsets[i];
                            verts[i] = iNode;
                            float thresholdedVal = 0;
                            if (veclen == 1)
                                thresholdedVal = thresholdedVals.getFloat(iNode);
                            else {
                                for (int j = 0, l = veclen * iNode; j < veclen; j++, l++)
                                    thresholdedVal += thresholdedVals.getFloat(l) * thresholdedVals.getFloat(l);
                                thresholdedVal = (float)Math.sqrt(thresholdedVal);
                            }
                            vals[i] = above ? thresholdedVal - threshold :
                                              threshold - thresholdedVal;
                        }
                        switch (subset(HEXAHEDRON, vals))
                        {
                        case VOID:
                            break;
                        case NONTRIVIAL:
                            splitCell (dim, in.getTrueNSpace(), verts, vals, (ix + iy + iz) % 2 == 0, fieldNormals, newNodes, newCells, oldNodes);
                            break;
                        case WHOLE:
                        case TRIANGULATED:
                            int[][] tetsVerts = RegularHex.triangulationVertices(verts, (ix + iy + iz) % 2 == 0);
                            for (int[] tetVerts : tetsVerts) 
                                newCells.add(new NewCell(TETRA, tetVerts, new NewNode[4], null, 0, oldNodes));
                            break;
                        }
                    }
            break;
        default:
            return null;
        }
        
        if (newCells.isEmpty())
            return null;
        return createOutfield(in, newCells, newNodes, oldNodes);
    }
    
    public static IrregularField splitField(RegularField in, LargeArray thresholdedVals, int veclen, float threshold, boolean above)
    {
        return splitField(in, thresholdedVals, veclen, threshold, above, null);
    }

}
