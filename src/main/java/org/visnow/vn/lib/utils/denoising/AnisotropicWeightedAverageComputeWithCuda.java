/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.denoising;

import java.io.*;

import jcuda.*;
import jcuda.driver.*;
import static jcuda.driver.JCudaDriver.*;

import org.apache.log4j.Logger;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.ConvertUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiser;

import org.visnow.vn.system.main.VisNow;

/**
 * Computation part of anisotropic denoiser implemented using NVIDIA CUDA technology.
 * Based on the algorithm by Krzysztof S. Nowiński (Warsaw University, Interdisciplinary
 * Centre for Mathematical and Computational Modelling).
 * <p>
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class AnisotropicWeightedAverageComputeWithCuda extends AbstractAnisotropicWeightedAverageCompute
{

    private static final Logger LOGGER = Logger.getLogger(AnisotropicDenoiser.class);

    private int[] dims;
    private int radius;
    private float slope, slope1;

    private int iterations;

    private float[] h_data;
    private float[] h_anisotropy;
    private float[] h_output;

    private ProgressAgent progressAgent;

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params)
    {
        return compute(inField, anisotropyField, params, ProgressAgent.getDummyAgent());
    }

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params, ProgressAgent progressAgent)
    {
        this.progressAgent = progressAgent;
        try {
            return computeMultipleKernel(inField, anisotropyField, params);
        } catch (IOException ex) {
            LOGGER.error("Could not compute the result on the GPU.");
        } catch (RuntimeException ex) {
            LOGGER.info(ex);
        }
        return null;
    }

    private RegularField computeMultipleKernel(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params) throws IOException, RuntimeException
    {
        if (inField == null)
            return null;

        radius = params.getRadius();
        slope = params.getSlope();
        slope1 = params.getSlope1();
        dims = inField.getDims();

        iterations = params.getIterations();

        if (dims.length != 3) {
            throw new RuntimeException("Only 3D fields are implemented GPU (for now)...");
        }

        int outNdata = 1;
        for (int i = 0; i < dims.length; i++) {
            outNdata *= dims[i];
        }

        RegularField outField = new RegularField(dims);
        outField.setAffine(inField.getAffine().clone());
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());

        for (int iComponent = 0; iComponent < params.getComponentsNumber(); iComponent++) {
            // Get raw float[] arrays (stored in host memory).
            h_data = getRawDataArray(inField, params.getComponent(iComponent));
            h_anisotropy = getRawAnisotropyArray(anisotropyField, params.getAnisotropyComponent(iComponent));

            String name = inField.getComponent(params.getComponent(iComponent)).getName();

            // Enable exceptions and omit all subsequent error checks.
            JCudaDriver.setExceptionsEnabled(true);

            // Initialize the driver and create a context for the first device.
            cuInit(0);

            int deviceOrdinal = VisNow.getCudaDevice();
            
            CUdevice device = new CUdevice();
            cuDeviceGet(device, deviceOrdinal);
            CUcontext context = new CUcontext();
            cuCtxCreate(context, 0, device);

            // Obtain the device name
            byte deviceNameRaw[] = new byte[1024];
            cuDeviceGetName(deviceNameRaw, deviceNameRaw.length, device);
            String deviceName = createString(deviceNameRaw);

            LOGGER.info("Using device " + deviceOrdinal + " (" + deviceName + ")...");

            // Load the PTX file
            // First: obtain the Compute Capability (determine type of computing kernel)
            int majorArray[] = {0};
            int minorArray[] = {0};
            cuDeviceComputeCapability(majorArray, minorArray, device);
            int major = majorArray[0];
            int minor = minorArray[0];
            LOGGER.info("Detected Compute Capability " + major + "." + minor + "...");
            
            String separator = File.separator;
            String ptxFileName = (major == 3 && minor == 5) ? "AnisotropicWeightedAverageCudaKernelsKepler" : "AnisotropicWeightedAverageCudaKernels";
            if (major == 3 && minor == 5)
                LOGGER.info("Using kernel optimized for Kepler microarchitecture (SM 3.5).");
            if (VisNow.get().isDevelopment())
                ptxFileName = VisNow.get().getOperatingFolder() + separator + "target" + separator + "libs" + separator + "native" + separator + "cuda" + separator + "ptxas" + separator + ptxFileName + System.getProperty("sun.arch.data.model") + ".ptx";
            else
                ptxFileName = VisNow.get().getOperatingFolder() + separator + "libs" + separator + "native" + separator + "cuda" + separator + "ptxas" + separator + ptxFileName + System.getProperty("sun.arch.data.model") + ".ptx";
            
            File ptxFile = new File(ptxFileName);
            if (!ptxFile.exists()) {
                throw new IOException("CUDA kernel file (PTX assembler): " + ptxFileName + "not found! Check your installation.");
            }

            CUmodule module = new CUmodule();
            cuModuleLoad(module, ptxFileName);

            // Obtain a function pointer to the "add" function (name of kernel).
            CUfunction function = new CUfunction();
            cuModuleGetFunction(function, module, "denoise3DChunks");

            // Allocate memory on device.
            // Copy arrays to the device.
            CUdeviceptr d_data = new CUdeviceptr();
            CUdeviceptr d_anisotropy = new CUdeviceptr();
            CUdeviceptr d_output = new CUdeviceptr();

            cuMemAlloc(d_data, outNdata * Sizeof.FLOAT);
            cuMemAlloc(d_anisotropy, outNdata * Sizeof.FLOAT);
            cuMemAlloc(d_output, outNdata * Sizeof.FLOAT);

            cuMemcpyHtoD(d_data, Pointer.to(h_data), outNdata * Sizeof.FLOAT);
            cuMemcpyHtoD(d_anisotropy, Pointer.to(h_anisotropy), outNdata * Sizeof.FLOAT);

            // Load balancing.
            int blockSizeX = 16;
            int blockSizeY = 8;
            int blockSizeZ = 8;
            if (major == 1) {
                blockSizeX = 8;
                blockSizeY = 4;
                blockSizeZ = 8;
            }

            // Compute grid size.
            // TODO Check limits.
            // int gridSizeX = (int)Math.ceil((double)outNdata / blockSizeX);
            int gridSizeX = dims[0] / blockSizeX;
            int restX = dims[0] % blockSizeX;
            if (restX != 0) {
                gridSizeX++;
            }

            int gridSizeY = dims[1] / blockSizeY;
            int restY = dims[1] % blockSizeY;
            if (restY != 0) {
                gridSizeY++;
            }

            int chunk_length = 8;
            int num_chunks = dims[2] / chunk_length;
            int rest = dims[2] % chunk_length;
            if (rest != 0) {
                num_chunks++;
            }

            int gridSizeZ = chunk_length / blockSizeZ;
            int restZ = chunk_length % blockSizeZ;
            if (restZ != 0) {
                gridSizeZ++;
            }

            // Call the kernel function.
            CUdeviceptr tmp;
            for (int i = 0; i < iterations; ++i) {
                for (int chunk = 0; chunk < num_chunks; ++chunk) {
                    LOGGER.info("[chunk " + (chunk + 1) + "/" + num_chunks + "; iteration " + (i + 1) + "/" + iterations + "] Launching kernel on thread block " + blockSizeX + " x " + blockSizeY + " x " + blockSizeZ + "; grid " + gridSizeX + " x " + gridSizeY + " x " + gridSizeZ + "...");
                    // Set up the kernel parameters: A pointer to an array
                    // of pointers which point to the actual values.
                    Pointer kernelParameters = Pointer.to(
                        Pointer.to(d_data),
                        Pointer.to(d_anisotropy),
                        Pointer.to(d_output),
                        Pointer.to(new int[]{dims[0]}),
                        Pointer.to(new int[]{dims[1]}),
                        Pointer.to(new int[]{dims[2]}),
                        Pointer.to(new int[]{chunk}),
                        Pointer.to(new int[]{chunk_length}),
                        Pointer.to(new int[]{radius}),
                        Pointer.to(new float[]{1.0f / (slope * slope)}),
                        Pointer.to(new float[]{1.0f / (slope1 * slope1)})
                    );

                    cuLaunchKernel(function,
                                   gridSizeX, gridSizeY, gridSizeZ, // Grid dimension
                                   blockSizeX, blockSizeY, blockSizeZ, // Block dimension
                                   0, null, // Shared memory size and stream
                                   kernelParameters, null // Kernel- and extra parameters
                    );
                    cuCtxSynchronize();

                    // Update progress bar.
                    double currentComponentProgress = (double) (i * num_chunks + chunk + 1) / (double) (iterations * num_chunks);
                    progressAgent.setProgress(((double) iComponent + currentComponentProgress) / (double) params.getComponentsNumber());
                }
                // Switch pointers for next iteration.
                tmp = d_anisotropy; // Keep the old pointer, so that memory can be freed.
                d_anisotropy = d_output;
                d_output = tmp;
            }
            // Switch the pointers back, so that everything points to the right thing.
            tmp = d_anisotropy;
            d_anisotropy = d_output;
            d_output = tmp;

            // Allocate host output memory and copy the device output
            // to the host.
            h_output = new float[outNdata];
            cuMemcpyDtoH(Pointer.to(h_output), d_output, outNdata * Sizeof.FLOAT);

            DataArray typedOutput = null;
            switch (inField.getComponent(params.getComponent(iComponent)).getType()) {
                case FIELD_DATA_BYTE:
                    typedOutput = DataArray.create(ConvertUtils.convertToUnsignedByteLargeArray(new FloatLargeArray(h_output), false, 1.0f, 0.0f), 1, name);
                    break;
                case FIELD_DATA_COMPLEX:
                    throw new RuntimeException("Component " + params.getComponent(iComponent) + " has unsupported data type (complex).");
                case FIELD_DATA_DOUBLE:
                    typedOutput = DataArray.create(ConvertUtils.convertToDoubleLargeArray(new FloatLargeArray(h_output)), 1, name);
                    break;
                case FIELD_DATA_FLOAT:
                    typedOutput = DataArray.create(h_output, 1, name);
                    break;
                case FIELD_DATA_INT:
                    typedOutput = DataArray.create(ConvertUtils.convertToIntLargeArray(new FloatLargeArray(h_output), false, 1.0f, 0.0f), 1, name);
                    break;
                case FIELD_DATA_LOGIC:
                    throw new RuntimeException("Component " + params.getComponent(iComponent) + " has unsupported data type (logic).");
                case FIELD_DATA_OBJECT:
                    throw new RuntimeException("Component " + params.getComponent(iComponent) + " has unsupported data type (object).");
                case FIELD_DATA_SHORT:
                    typedOutput = DataArray.create(ConvertUtils.convertToShortLargeArray(new FloatLargeArray(h_output), false, 1.0f, 0.0f), 1, name);
                    break;
                case FIELD_DATA_STRING:
                    throw new RuntimeException("Component " + params.getComponent(iComponent) + " has unsupported data type (string).");
                case FIELD_DATA_UNKNOWN:
                    throw new RuntimeException("Could not determine the component " + params.getComponent(iComponent) + " data type.");
            }
            typedOutput.setPreferredRanges(inField.getComponent(params.getComponent(iComponent)).getPreferredMinValue(), inField.getComponent(params.getComponent(iComponent)).getPreferredMaxValue(), inField.getComponent(params.getComponent(iComponent)).getPreferredPhysMinValue(), inField.getComponent(params.getComponent(iComponent)).getPreferredPhysMaxValue());
            outField.addComponent(typedOutput);

            // Clean up.
            cuMemFree(d_data);
            cuMemFree(d_anisotropy);
            cuMemFree(d_output);
        }
        return outField;
    }

    private float[] getRawDataArray(RegularField inField, int iComponent)
    {
        DataArray dataArray = inField.getComponent(iComponent);
        float[] rawArray;

        if (dataArray.getType() != DataArrayType.FIELD_DATA_FLOAT) {
            LOGGER.warn("Only fields of type \"float\" are supported on a GPU; data array will be converted to \"float\" (memory usage will rise)...");
            rawArray = dataArray.getRawFloatArray().getData();
        } else {
            rawArray = (float[]) dataArray.getRawArray().getData();
        }

        if (rawArray == null) {
            throw new RuntimeException("LargeArrays are not supported on GPUs yet...");
        }
        LOGGER.info("Using data component " + iComponent + " (" + dataArray.getName() + ").");
        return rawArray;
    }

    private float[] getRawAnisotropyArray(RegularField anisotropyField, int anisotropyComponent)
    {
        float[] rawArray = null;

        if (anisotropyComponent < 0) {
            LOGGER.info("anisotropyComponent < 0, assuming null (no anisotropy field connected)...");
        } else {
            DataArray anisotropyArray = anisotropyField.getComponent(anisotropyComponent);
            if (anisotropyArray.getVectorLength() != 1) {
                throw new RuntimeException("Only vlen = 1 anisotropy fields are supported on a GPU...");
            }

            if (anisotropyArray.getType() != DataArrayType.FIELD_DATA_FLOAT) {
                LOGGER.warn("Only fields of type \"float\" are supported on a GPU; anisotropy array will be converted to \"float\" (memory usage will rise)...");
            }

            rawArray = (float[]) anisotropyArray.getRawArray().getData();
            if (rawArray == null) {
                throw new RuntimeException("LargeArrays are not supported on GPUs yet...");
            }

            rawArray = (float[]) anisotropyArray.getRawArray().getData();
            LOGGER.info("Using anisotropy component " + anisotropyComponent + " (" + anisotropyArray.getName() + ").");
        }
        return rawArray;
    }

    /**
     * Creates a String from a zero-terminated string in a byte array
     * <p>
     * @param bytes The byte array
     * <p>
     * @return The String
     */
    private static String createString(byte bytes[])
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            char c = (char) bytes[i];
            if (c == 0) {
                break;
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
