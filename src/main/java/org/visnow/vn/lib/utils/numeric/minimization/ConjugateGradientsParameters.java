/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.minimization;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ConjugateGradientsParameters
{

    //   double gradientEpsToExit;
    //   double maxCoordMoveToExit;
    //   double linearMinimizationPrecision
    //   double[] initialStep;
    //   int[] restartSchedule;
    //   int maxLinearInterp;
    private float gradientEpsToExit = .001f;
    private float maxCoordMoveToExit = .001f;
    private float linearMinimizationPrecision = .001f;
    private int[] restartSchedule = new int[]{20, 50, 200};

    private GradValDP gradValDP;
    private GradVal gradVal;

    /**
     * Mimimization will finish if for last 5 steps all gradient components are
     * <lt> gradientEpsToExit
     *
     * @return the value of gradientEpsToExit
     */
    public float getGradientEpsToExit()
    {
        return gradientEpsToExit;
    }

    /**
     * Set the value of gradientEpsToExit
     *
     * @param gradientEpsToExit new value of gradientEpsToExit
     */
    public void setGradientEpsToExit(float gradientEpsToExit)
    {
        this.gradientEpsToExit = gradientEpsToExit;
    }

    /**
     * Mimimization will finish if for last 5 steps max coordinate change <lt>
     * maxCoordMoveToExit
     *
     * @return the value of maxCoordMoveToExit
     */
    public float getMaxCoordMoveToExit()
    {
        return maxCoordMoveToExit;
    }

    /**
     * Set the value of maxCoordMoveToExit
     *
     * @param maxCoordMoveTo exit new value of maxCoordMoveToExit
     */
    public void setMaxCoordMoveToExit(float maxCoordMoveToExit)
    {
        this.maxCoordMoveToExit = maxCoordMoveToExit;
    }

    /**
     * Get the value of linearMinimizationPrecision
     *
     * @return the value of linearMinimizationPrecision
     */
    public float getLinearMinimizationPrecision()
    {
        return linearMinimizationPrecision;
    }

    /**
     * Set the value of linearMinimizationPrecision
     *
     * @param linearMinimizationPrecision new value of
     *                                    linearMinimizationPrecision
     */
    public void setLinearMinimizationPrecision(float linearMinimizationPrecision)
    {
        this.linearMinimizationPrecision = linearMinimizationPrecision;
    }

    /**
     * Get the value of restartSchedule
     *
     * @return the value of restartSchedule
     */
    public int[] getRestartSchedule()
    {
        return restartSchedule;
    }

    /**
     * Set the value of restartSchedule
     *
     * @param restartSchedule new value of restartSchedule
     */
    public void setRestartSchedule(int[] restartSchedule)
    {
        this.restartSchedule = restartSchedule;
    }

    /**
     * Get the value of restartSchedule at specified index
     *
     * @param index
     *              <p>
     * @return the value of restartSchedule at specified index
     */
    public int getRestartSchedule(int index)
    {
        return this.restartSchedule[index];
    }

    /**
     * Set the value of restartSchedule at specified index.
     *
     * @param index
     * @param newRestartSchedule new value of restartSchedule at specified index
     */
    public void setRestartSchedule(int index, int newRestartSchedule)
    {
        this.restartSchedule[index] = newRestartSchedule;
    }

}
