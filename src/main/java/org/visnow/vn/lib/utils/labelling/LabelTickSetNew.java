/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.labelling;

import java.awt.FontMetrics;
import java.util.Arrays;

//TODO: add subgrid flag (which is if ticks are subgrid of maingrid - that may be usefull for graphs)
//but before that - just remove major ticks 
//(this is to create range/labels like: 500, 1000, 2000 for log ranges like: 700-1500 (much better then range/labels 100 ... 10000)
//                    1. zrobic metode, która dla danej liczby pikseli bedzie zwracala optymalna liczbe wartosci(nie mniejsza niz liczbe pikseli) 
//                    i skok pojedynczego ticka (musi uwzgledniac minimalna odleglosc tikow(px) i optymalna (%))
//                       Przypadki: (szerokie / wąskie etykiety, duzo / malo etykiet(czyli powerMax >> powerMin)), 
//                       szerokosc bardzo duża / średnia / mała => widoczne etykiety potęg, widoczne etykiety 2 i 5, bez tików
//                    ?, tiki przy potęgach, tiki przy potęgach i 2, 5, tiki też w środku ???)
/**
 * Possible set of labels and ticks - completely independent from space where it will be (or not) rendered.
 * This class is a representation of labels and ticks to draw at regular grid (but neither all grid positions need to be occupied by labels nor by ticks!).
 *
 * Spec:
 * TotalGridPositions is always positive (at least 1 position - this is not really a grid - may be suitable for 1px wide slider/axis)
 * TotalGridBins == TotalGridPositions - 1
 * Label positions are within 0 .. TotalGridPositions - 1
 * Label set may be empty (TotalLabelNum == 0)
 * But labels cannot be empty (at least one character)
 * Tick grids may contain 0 grid (no ticks)
 *
 *
 * Tick grid is always a sub-grid of label grid (excluding 0 case: no ticks at all, and 1 case: just one tick at the beginning)!
 *
 * @author szpak
 */
public class LabelTickSetNew
{
    private final double tolerancePx = 0.1; //tolerance in px (necessary to avoid floating point problems; without that no labels at all)
    private final double toleranceDouble = 0.0001; //tolerance to avoid floating point problems

    private final double[] labelPositions; //ascending order label positions within range [0..1]
    private final String[] labels;

    // Multiple tickgrids ordered from best ticking (maximum number of ticks) to worst one (possibly no ticks at all)
    // For each i: tickGrids[i] - ascending order tick positions within range [0..1]
    private final double[][] tickGrids;

    public LabelTickSetNew(String[] labels, double[] labelPositions, double[][] tickGrids)
    {
        this.labelPositions = labelPositions;
        this.labels = labels;
        this.tickGrids = tickGrids;

        if (labels.length != labelPositions.length)
            throw new IllegalArgumentException("Incorrect label positions");

        for (String label : labels)
            if (label.length() <= 0)
                throw new IllegalArgumentException("Empty labels are not supported");

        //TODO: calculate tolerance or specify precision
        for (double position : labelPositions)
            if (position < 0 - toleranceDouble || position > 1 + toleranceDouble)
                throw new IllegalArgumentException("Incorrect label position: " + Arrays.toString(labelPositions));

//        for (double[] grid: tickGrids) 
//            for (double position: grid) 
//                if (position < 0 || position >1)
//                    throw new IllegalArgumentException("Incorrect tick position");        
    }

    public int labelCount()
    {
        return labels.length;
    }

    public int[] getLabelPositions(int rangeMin, int rangeMax)
    {
        int[] intPositions = new int[labelPositions.length];

        int range = rangeMax - rangeMin;

        for (int i = 0; i < intPositions.length; i++)
            intPositions[i] = rangeMin + (int) Math.round(labelPositions[i] * range);

        return intPositions;
    }

    public String[] getLabels()
    {
        return labels;
    }

    private int[] recentLabelWidths = null;
    private FontMetrics recentFontMetrics = null;

    private int[] labelWidths(FontMetrics fontMetrics)
    {
        if (recentFontMetrics != fontMetrics) {
            recentFontMetrics = fontMetrics;
            recentLabelWidths = new int[labels.length];

            for (int i = 0; i < labels.length; i++)
                recentLabelWidths[i] = fontMetrics.stringWidth(labels[i]);
        }
        return recentLabelWidths;
    }

    public int labelMaxWidth(FontMetrics fontMetrics)
    {
        int max = 0;
        for (int width : labelWidths(fontMetrics)) max = Math.max(max, width);
        return max;
    }

    public int labelMinWidth(FontMetrics fontMetrics)
    {
        int min = Integer.MAX_VALUE;
        for (int width : labelWidths(fontMetrics)) min = Math.min(min, width);
        return min;
    }

    //TODO: another flag should be added here ("bounded/limited" or something like that - that will be usefull for axes 3D - we've got no limitations on sides of the range - just in the middle)
    //TODO: slightly different computations for left/right/center alignment (not really applicable to JSliders, but may be usefull in graph axes)
    //TODO: vertical case (or better vertical/horizontal/both - see axes3D)
    //TODO: if only one label fits width and label width is much larger than sliderWidth (after padding) then mark as 'not enough space'
    /**
     * Tests if passed space {@code widthPx} is large enough for this LabelTickSet.
     *
     * @param widthPx           free space (in pixels)
     * @param minimumDistancePx minimum distance between labels (in pixels)
     *
     * [Note: Following calculations need to be done in real arithmetics to avoid blinking (while resize) - this is because after rounding to integer,
     * paradoxically, distance between labels can be larger for smaller width!
     * For the same reason minimum margin should be 1px (or maybe not .. 1px margin is an additional implemented feature)]
     */
    //TODO: In VisNow Thumb has width 15px (7px each side). Good news is that slider.getWidth() gives correct answer (checked in Gimp)
    public boolean isEnoughSpace(int widthPx, FontMetrics fontMetrics, int minimumDistancePx, int maximumLabelOverflowPx)
    {
        //always enough space for no labels
        if (labels.length == 0) return true;

        //always not enough space for no width
        if (widthPx <= 0) return false;

        int minLabelWidth = labelMinWidth(fontMetrics) & 0x01;

        //eliminate obvious case (if number of labels * shortest label + minimum distance > width)
        //(this is not necessary but could speed up computations in many cases)
        if (labels.length * minLabelWidth + (labels.length - 1) * minimumDistancePx > widthPx + 2 * maximumLabelOverflowPx)
            return false;

        int[] labelWidths = labelWidths(fontMetrics);

        //first label too close
        if (labelPositions[0] * (widthPx - 1) - labelWidths[0] / 2.0 < -maximumLabelOverflowPx - tolerancePx)
            return false;

        //last label too far
        if (labelPositions[labels.length - 1] * (widthPx - 1) + labelWidths[labels.length - 1] / 2.0 > widthPx - 1 + maximumLabelOverflowPx + tolerancePx)
            return false;

        //labels overflow
        for (int i = 1; i < labels.length; i++)
            if (labelPositions[i - 1] * (widthPx - 1) + labelWidths[i - 1] / 2.0 + minimumDistancePx>
                    labelPositions[i] * (widthPx - 1) - labelWidths[i] / 2.0 + tolerancePx) return false;

        return true;
    }

    //TODO: minimum distance should be rather like 1px (add to spec)
    /**
     * Calculates best tick spacing and returns tick spacing in grid units (so if return 1 then every grid node should be painted as a tick)
     *
     * @param major           if true then major ticks are calculated, minor ticks otherwise
     * @param width           slider width in px
     * @param minimumDistance minimum distance in px between ticks (for example: if minimumDistance == 1 then there will be at least 1 px space between two
     *                        ticks);
     *                        minimumDistance should be in general smaller for minor ticks and larger for major ticks (sometimes visible minimumDistance can be smaller than needed -
     *                        and this
     *                        (apart from problems with calculating Thumb width) is caused by incorrect rounding tick position in JSlider (one can see distance 1 and 3 px in the same
     *                        set!))
     * <p>
     * @return tick spacing in grid units or 0 if no ticks should be drawn
     */
//    public int getBestTickSpacing(boolean major, int width, int minimumDistance)
//    {
//        if (major) {
//            for (int tickGrid : majorTickGrids)
//                if (tickGrid != 0 && (width - 1) / (tickGrid - 1) > minimumDistance)
//                    return (totalGridPositions - 1) / (tickGrid - 1);
//        } else {
//            for (int tickGrid : minorTickGrids)
//                if (tickGrid != 0 && (width - 1) / (tickGrid - 1) > minimumDistance)
//                    return (totalGridPositions - 1) / (tickGrid - 1);
//        }
//        return 0;
//    }

    /**
     * Returns extended info about this set.
     */
    @Override
    public String toString()
    {
//        return "Grid: " + totalGridPositions + ";  Labels: " + Arrays.toString(labels) + " at: " + Arrays.toString(labelPositions) + ";  Major tick grids: " + Arrays.toString(majorTickGrids) + ";  Minor tick grids: " + Arrays.toString(minorTickGrids);
        return "  Labels: " + Arrays.toString(labels) + " at: " + Arrays.toString(labelPositions);
    }
}
