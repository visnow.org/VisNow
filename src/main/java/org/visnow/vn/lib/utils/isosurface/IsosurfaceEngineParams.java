/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isosurface;

/**
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * updated by szpak
 */
public class IsosurfaceEngineParams
{
    final private int isoComponent;
    final private int[] downsize;
    final private int[] low;
    final private int[] up;
    final private int smoothSteps;
    final private boolean smoothing;
    final private boolean uncertainty;
    final private boolean separate;
    final private boolean grid;
    final private float time;

    public IsosurfaceEngineParams(int isoComponent, int[] downsize, int[] low, int[] up,
                                  int smoothSteps, boolean smoothing, boolean uncertainty,
                                  boolean separate, boolean grid, float time)
    {
        this.isoComponent = isoComponent;
        this.downsize = downsize;
        this.low = low;
        this.up = up;
        this.smoothSteps = smoothSteps;
        this.smoothing = smoothing;
        this.uncertainty = uncertainty;
        this.separate = separate;
        this.grid = grid;
        this.time = time;
    }

    public int getIsoComponent()
    {
        return isoComponent;
    }

    public int[] getDownsize()
    {
        return downsize;
    }

    public int[] getLow()
    {
        return low;
    }

    public int[] getUp()
    {
        return up;
    }

    public int getSmoothSteps()
    {
        return smoothSteps;
    }

    public boolean isSmoothing()
    {
        return smoothing;
    }

    public boolean isUncertainty()
    {
        return uncertainty;
    }

    public boolean isSeparate()
    {
        return separate;
    }

    public boolean isGrid()
    {
        return grid;
    }

    public float getTime()
    {
        return time;
    }
}
