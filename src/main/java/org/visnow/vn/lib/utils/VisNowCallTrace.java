/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import org.apache.log4j.Logger;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class VisNowCallTrace
{

    static Logger logger = Logger.getLogger(VisNowCallTrace.class);

    public static void trace()
    {
        //      if (VisNow.isDebug())
        try {
            StringWriter sw = new StringWriter();
            PrintWriter w = new PrintWriter(sw);
            new Exception().printStackTrace(w);
            String s = sw.toString();
            BufferedReader r = new BufferedReader(new StringReader(s));
            String line = " ";
            do {
                line = r.readLine();
                if (line == null)
                    break;
                if (line.indexOf("VisNowCallTrace") >= 0)
                    continue;
                int k = line.indexOf("visnow");
                if (k >= 0)
                    System.out.println(line.substring(k));
            } while (line != null);
            System.out.println("");
        } catch (Exception e) {
        }
    }

    public static void traceLog(int level)
    {
        if (VisNow.isDebug())
            try {
                StringWriter sw = new StringWriter();
                PrintWriter w = new PrintWriter(sw);
                new Exception().printStackTrace(w);
                String s = sw.toString();
                BufferedReader r = new BufferedReader(new StringReader(s));
                StringBuilder out = new StringBuilder();
                String line = " ";
                do {
                    line = r.readLine();
                    if (line == null)
                        break;
                    if (line.indexOf("VisNowCallTrace") >= 0)
                        continue;
                    int k = line.indexOf("visnow");
                    if (k >= 0)
                        out.append("\n");
                    out.append(line);
                } while (line != null);
                logger.debug(out.toString());
            } catch (Exception e) {
            }
    }

    private VisNowCallTrace()
    {
    }
}
