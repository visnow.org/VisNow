/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.animation;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{

    public static final int FORWARD = 1;
    public static final int STOP = 0;
    public static final int BACK = -1;

    private float d = .1f;

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("counter", ParameterType.independent, 0),
        new ParameterEgg<Integer>("min", ParameterType.independent, 0),
        new ParameterEgg<Integer>("max", ParameterType.independent, 10),
        new ParameterEgg<Float>("value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("min value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("min value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("max value", ParameterType.independent, 1.f),};

    public Params()
    {
        super(eggs);
    }

    public int getCounter()
    {
        return (Integer) getValue("counter");
    }

    public void setCounter(int counter)
    {
        setValue("counter", counter);
        fireStateChanged();
    }

    public float getValue()
    {
        return (Float) getValue("value");
    }

    public void setValue(float value)
    {
        setValue("value", value);
        fireStateChanged();
    }
}
