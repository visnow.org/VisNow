/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import org.jogamp.vecmath.Point3f;

public class Helper
{

    private float[] offset;
    private int[] size;
    private float[] coords;
    private int[] dims;
    private float[] nX, nY, nZ;
    private int x, y, z;

    public Helper(float[] offset, int[] size, float[] coords, int[] dims)
    {
        this.coords = coords;
        switch (dims.length) {
            case 1:
                this.dims = new int[]{dims[0], 0, 0};
                this.offset = new float[]{offset[0], 0, 0};
                this.size = new int[]{size[0], 1, 1};
                break;
            case 2:
                this.dims = new int[]{dims[0], dims[1], 1};
                this.offset = new float[]{offset[0], offset[1], 1};
                this.size = new int[]{size[0], size[1], 1};
                break;
            case 3:
                this.dims = dims;
                this.offset = offset;
                this.size = size;
                break;
            default:
                break;
        }
    }

    public void updateCoords(int x, int y)
    {
        int iX = dims[0];
        int iY = dims[1];
        this.x = x;
        this.y = y;
        if (coords == null) {
            nX = new float[]{
                x,
                x,
                x + 1,
                x + 1,};
            nY = new float[]{
                y,
                y + 1,
                y + 1,
                y,};
            nZ = new float[]{
                0,
                0,
                0,
                0,};
        } else {
            nX = new float[]{
                coords[3 * (y * iX + x)], // (x, y, z)
                coords[3 * ((y + 1) * iX + x)], // (x, y + 1, z)
                coords[3 * ((y + 1) * iX + x + 1)], // (x + 1, y + 1, z)
                coords[3 * (y * iX + x + 1)], // (x + 1, y, z)
            };
            nY = new float[]{
                coords[3 * (y * iX + x) + 1], // (x, y, z)
                coords[3 * ((y + 1) * iX + x) + 1], // (x, y + 1, z)
                coords[3 * ((y + 1) * iX + x + 1) + 1], // (x + 1, y + 1, z)
                coords[3 * (y * iX + x + 1) + 1], // (x + 1, y, z)
            };
            nZ = new float[]{
                coords[3 * (y * iX + x) + 2], // (x, y, z)
                coords[3 * ((y + 1) * iX + x) + 2], // (x, y + 1, z)
                coords[3 * ((y + 1) * iX + x + 1) + 2], // (x + 1, y + 1, z)
                coords[3 * (y * iX + x + 1) + 2], // (x + 1, y, z)
            };
        }
    }

    public void updateCoords(int x, int y, int z)
    {
        int iX = dims[0];
        int iY = dims[1];
        int iZ = dims[2];
        this.x = x;
        this.y = y;
        this.z = z;
        if (coords == null) {
            nX = new float[]{
                x,
                x,
                x + 1,
                x + 1,
                x,
                x,
                x + 1,
                x + 1,};
            nY = new float[]{
                y,
                y + 1,
                y + 1,
                y,
                y,
                y + 1,
                y + 1,
                y,};
            nZ = new float[]{
                z,
                z,
                z,
                z,
                z + 1,
                z + 1,
                z + 1,
                z + 1,};
        } else {
            nX = new float[]{
                coords[3 * (z * iX * iY + y * iX + x)], // (x, y, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x)], // (x, y + 1, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x + 1)], // (x + 1, y + 1, z)
                coords[3 * (z * iX * iY + y * iX + x + 1)], // (x + 1, y, z)

                coords[3 * ((z + 1) * iX * iY + y * iX + x)], // (x, y, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x)], // (x, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x + 1)], // (x + 1, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + y * iX + x + 1)], // (x + 1, y, z + 1)
            };
            nY = new float[]{
                coords[3 * (z * iX * iY + y * iX + x) + 1], // (x, y, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x) + 1], // (x, y + 1, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x + 1) + 1], // (x + 1, y + 1, z)
                coords[3 * (z * iX * iY + y * iX + x + 1) + 1], // (x + 1, y, z)

                coords[3 * ((z + 1) * iX * iY + y * iX + x) + 1], // (x, y, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x) + 1], // (x, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x + 1) + 1], // (x + 1, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + y * iX + x + 1) + 1], // (x + 1, y, z + 1)
            };
            nZ = new float[]{
                coords[3 * (z * iX * iY + y * iX + x) + 2], // (x, y, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x) + 2], // (x, y + 1, z)
                coords[3 * (z * iX * iY + (y + 1) * iX + x + 1) + 2], // (x + 1, y + 1, z)
                coords[3 * (z * iX * iY + y * iX + x + 1) + 2], // (x + 1, y, z)

                coords[3 * ((z + 1) * iX * iY + y * iX + x) + 2], // (x, y, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x) + 2], // (x, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + (y + 1) * iX + x + 1) + 2], // (x + 1, y + 1, z + 1)
                coords[3 * ((z + 1) * iX * iY + y * iX + x + 1) + 2], // (x + 1, y, z + 1)
            };
        }
    }

    public Point3f getPoint(int i, int x, int y, int z)
    {

        return new Point3f(
            offset[0] + nX[i] * size[0],
            offset[1] + nY[i] * size[1],
            offset[2] + nZ[i] * size[2]);
    }

    public float[] getPointF(int i, int x, int y, int z)
    {

        return new float[]{
            offset[0] + nX[i] * size[0],
            offset[1] + nY[i] * size[1],
            offset[2] + nZ[i] * size[2]
        };
    }

    public void putLine(Point3f[] pts, int i, int j, int k)
    {
        pts[2 * i] = getPoint(j, x, y, z);
        pts[2 * i + 1] = getPoint(k, x, y, z);
    }
}
