/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.curves;

import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author babor
 */
public class Bezier
{

    private float[][] points;

    public Bezier(float[][] points)
    {
        this.points = points;
    }

    public float[] bezier(float t)
    {
        return bezier(points, t);
    }

    public float[] bezierDerivative(float t)
    {
        return bezierDerivative(points, t);
    }

    public static float[] bezier(float[][] points, float t)
    {
        if (points == null)
            return null;

        int n = points.length - 1;
        int nDims = points[0].length;
        float[] out = new float[nDims];
        for (int d = 0; d < nDims; d++) {
            out[d] = 0.0f;
            for (int i = 0; i < n + 1; i++) {
                out[d] += points[i][d] * bernstein(n, i, t);
            }
        }
        return out;
    }

    public static float[] bezierDerivative(float[][] points, float t)
    {
        if (points == null)
            return null;

        int n = points.length - 1;
        int nDims = points[0].length;

        float[][] dPoints = new float[n][nDims];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < nDims; j++) {
                dPoints[i][j] = n * (points[i + 1][j] - points[i][j]);
            }
        }

        float[] out = new float[nDims];
        for (int d = 0; d < nDims; d++) {
            out[d] = 0.0f;
            for (int i = 0; i < n; i++) {
                out[d] += dPoints[i][d] * bernstein(n - 1, i, t);
            }
        }
        return out;
    }

    public static double bernstein(int n, int i, float u)
    {
        return binomial(n, i) * pow(u, i) * pow(1.0f - u, n - i);
    }

    public static int factorial(int n)
    {
        int ret = 1;
        for (int i = 1; i <= n; ++i)
            ret *= i;
        return ret;
    }

    public static float binomial(int n, int i)
    {
        return (float) factorial(n) / (float) (factorial(i) * factorial(n - i));
    }

}
