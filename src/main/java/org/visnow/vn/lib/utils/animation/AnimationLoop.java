/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.animation;

import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationListener;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnimationLoop implements Runnable
{

    public static final int ONCE = 0;
    public static final int CYCLE = 1;
    public static final int BOUNCE = 2;
    private Params params = new Params();
    private int counter = 0;
    private float start = 0, incr = 1, stop = 10, value = 0;
    private int firstFrame = 0;
    private int currentFrame = 0;
    private float currentTime = 0;
    private int lastFrame = 100;
    private float tmin = 0, tmax = 1, dt = .01f;
    private float tlow = 0, tup = 1;
    private int delay = 10;
    private int step = 1;
    private int mode = params.STOP;
    private int loopMode = ONCE;
    private boolean renderDone = true;
    private FrameModificationListener frameModificationListener = new FrameModificationListener()
    {
        @Override
        public void frameChanged(FrameModificationEvent e)
        {
            counter = e.getFrame();
            value = start + counter * incr;
            params.setCounter(counter);
            params.setValue(value);
        }
    };

    @Override
    public synchronized void run()
    {
        if (mode == params.STOP)
            return;
        int frame;
        int dir = mode;
        frame = firstFrame;
        animation_loop:
        while (mode != params.STOP) {
            renderDone = false;
            frame += dir * step;
            if (dir == 1 && frame > lastFrame) {
                switch (loopMode) {
                    case ONCE:
                        mode = params.STOP;
                        break animation_loop;
                    case CYCLE:
                        frame = 0;
                        break;
                    case BOUNCE:
                        dir = -dir;
                        frame = lastFrame;
                }
            } else if (dir == -1 && frame < 0) {
                switch (loopMode) {
                    case ONCE:
                        mode = params.STOP;
                        break animation_loop;
                    case CYCLE:
                        frame = lastFrame;
                        break;
                    case BOUNCE:
                        dir = -dir;
                        frame = 0;
                }
            }

            //            setFrame(frame);
            try {
                while (!renderDone)
                    wait(10);
                if (mode == params.STOP)
                    //                  stopAnimation();
                    wait(delay);
            } catch (InterruptedException c) {
                //               stopAnimation();
            }
        }
        //         stopAnimation();
    }

    public AnimationLoop()
    {
    }
}
