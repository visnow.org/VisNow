/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isosurface;

import java.io.File;
import java.io.PrintWriter;
import java.util.Locale;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class LookUpTables {
    /**
     * ISOSURFACE_TRIANGLES[parity][index][i] is a triple of vertices of i-th triangle of isosurface 
     * of a function f defined on vertices of a hexahedron. Correct orientation is ensured.
     * <pre>
     *  v6----11----v7
     *  |\          |\
     *  | 9           10
     *  6  \        7  \
     *  |   v4----8----v5
     *  |   |       |  |
     *  |   |          |
     *  v2- | 5 - -v3  4
     *   \  2       \  |
     *    1 |        3 |
     *     \|         \|
     *     v0-----0----v1
     * 
     *                    </pre>
     * Index is: sign(v0) | sign(v1)<<1 | ... | sign(v6)<<6 | sign(v7)<<7,
     * parity is (ix + iy + iz) % 2    for a hex with 0 vertex at position (ix,iy,iz) in a ragular field
     */
    private static int[][][][] ISOSURFACE_TRIANGLES = null;
    
    /**
     * for debug purposes RAW_ISOSURFACE_TRIANGLES[parity][index][i] is a triple of vertices of i-th triangle of 
     * raw isosurface that is the isosurface generated from 5 tetras - before projecting of additional vertices
     * located on diagonals of the hex faces to the hex edges.
     * of a function f defined on vertices of a hexahedron. 
     */
    private static int[][][][] RAW_ISOSURFACE_TRIANGLES = null;
    
    /**
     * ISOSURFACE_EDGES[parity][index][i] is a pair of vertices of i-th triangle of isosurface 
     * of a function f defined on vertices of a hexahedron.
     * Index is: sign(v0) | sign(v1)<<1 | ... | sign(v6)<<6 | sign(v7)<<7,
     * parity is (ix + iy + iz) % 2    for a hex with 0 vertex at position (ix,iy,iz) in a ragular field
     */
    private static int[][][][] ISOSURFACE_EDGES = null;
    
    
    
    private static int[] hexEdge(int tetra, int[] tetEdge, int[][] triang)
    {
        int[] hexEdge = new int[2];
        int[] tet = triang[tetra];
        for (int i = 0; i < tetEdge.length; i++)
            hexEdge[i] = tet[tetEdge[i]];
        return hexEdge;
    }
    
    private static void createLUTs()
    {
        createLUTs(false);
    }
    
    private static void createLUTs(boolean debug)
    {
        ISOSURFACE_TRIANGLES = new int[2][256][][];
        ISOSURFACE_EDGES = new int[2][256][][];
        
        if (debug)
            RAW_ISOSURFACE_TRIANGLES = new int[2][256][][];
        
        int hexNodes = 8;
        // vertex coords
        float[][] hexCoords = {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
                               {0, 0, 1}, {1, 0, 1}, {0, 1, 1}, {1, 1, 1}};
        // triangulation for even and odd indexed hexahedra
        int[][][] hexTriangulations = {
            {{0, 3, 5, 6},
             {1, 0, 3, 5}, {2, 0, 3, 6}, {4, 0, 5, 6}, {7, 3, 5, 6}},
            {{1, 2, 4, 7},
             {0, 1, 2, 4}, {3, 1, 2, 7}, {5, 1, 4, 7}, {6, 2, 4, 7}}};
        // edges of triangulated hexahedra
        int[][][] hexTriangulationsEdges = {
            {{0, 1}, {0, 2}, {0, 4},
             {1, 3}, {1, 5}, {2, 3}, {2, 6}, {3, 7},
             {4, 5}, {4, 6}, {5, 7}, {6, 7},
             {0, 3}, {0, 5}, {0, 6}, {3, 5}, {3, 6}, {5, 6}},
            {{0, 1}, {0, 2}, {0, 4},
             {1, 3}, {1, 5}, {2, 3}, {2, 6}, {3, 7},
             {4, 5}, {4, 6}, {5, 7}, {6, 7},
             {1, 2}, {1, 4}, {1, 7}, {2, 4}, {2, 7}, {4, 7}}};
        // triangulated isosurfaces for tetrahedron cases are indexed by code=sign(val0) | sign(val1)<<1 | sign(val2)<<2 | sign(val3)<<3
        // corresponding value is a sequence of triples of tetrahedron edges
        // triangles of the isosurface are spanned by centres of these edges
        int[][][][] tetraIsosurfaces
                = {{},
                {{{0, 1}, {0, 2}, {0, 3}}},
                {{{0, 1}, {1, 2}, {1, 3}}},
                {{{0, 2}, {1, 2}, {1, 3}}, {{0, 2}, {1, 3}, {0, 3}}},
                {{{0, 2}, {1, 2}, {2, 3}}},
                {{{0, 1}, {1, 2}, {2, 3}}, {{0, 1}, {2, 3}, {0, 3}}},
                {{{0, 1}, {1, 3}, {2, 3}}, {{0, 1}, {2, 3}, {0, 2}}},
                {{{0, 3}, {1, 3}, {2, 3}}},
                {{{0, 3}, {1, 3}, {2, 3}}},
                {{{0, 1}, {1, 3}, {2, 3}}, {{0, 1}, {2, 3}, {0, 2}}},
                {{{0, 1}, {1, 2}, {2, 3}}, {{0, 1}, {2, 3}, {0, 3}}},
                {{{0, 2}, {1, 2}, {2, 3}}},
                {{{0, 2}, {1, 2}, {1, 3}}, {{0, 2}, {1, 3}, {0, 3}}},
                {{{0, 1}, {1, 2}, {1, 3}}},
                {{{0, 1}, {0, 2}, {0, 3}}},
                {}
                };
        // coordinates of all vertices, edge centers anf face centers
        float[][] fullCoords = new float[26][3];
    
        int lastVert = 8, lastEdgeVert = 8 + 12;
        for (int i = 0; i < hexNodes; i++)
            System.arraycopy(hexCoords[i], 0, fullCoords[i], 0, 3);

        for (int parity = 0; parity < 2; parity++) {

            for (int i = 0, l = 8; i < hexTriangulationsEdges[parity].length; i++, l++) {
                int i0 = hexTriangulationsEdges[parity][i][0], i1 = hexTriangulationsEdges[parity][i][1];
                for (int j = 0; j < 3; j++)
                    fullCoords[l][j] = (hexCoords[i0][j] + hexCoords[i1][j]) / 2;
            }

            for (int isosurfaceCode = 0; isosurfaceCode < 256; isosurfaceCode++) {
                // there is 5 tetrahedra and an isosurface can have max 2 triangles in each tetra
                int[][] rawTriangles = new int[10][3];
                int[][] isoTriangles = new int[10][3];
                int nRawTriangles = 0;
                int[] indices = new int[8];
                int[] map = new int[26];

                // hexVals set so that threshold = 0 gives isosurface for the isosurfaceCode
                for (int j = 0; j < hexNodes; j++) 
                    indices[j] = (isosurfaceCode >> j) & 1;
                
                // first 8 nodes are hex vertices, then 12 hex edge centers and 6 face centers
                // computing parts of isosurface inside each tetra
                for (int tetIndex = 0; tetIndex < hexTriangulations[parity].length; tetIndex++) {
                    int index = 0;
                    for (int k = 0; k < hexTriangulations[parity][tetIndex].length; k++) {
                        int l = indices[hexTriangulations[parity][tetIndex][k]];
                        index |= l << k;
                    }
                    int[][][] tetraIso = tetraIsosurfaces[index];

                    for (int[][] triangles : tetraIso) {
                        // a triangle of a tetrahedron isosurface is spanned by the centres of three tetrahedron edges
                        // this loop converts tetra edges to indices of hex edges/diagonals
                        for (int vert = 0; vert < 3; vert++) {
                            int[] hEdge = hexEdge(tetIndex, triangles[vert], hexTriangulations[parity]);
                            for (int j = 0; j < hexTriangulationsEdges[parity].length; j++)
                                if (hEdge[0] == hexTriangulationsEdges[parity][j][0] && hEdge[1] == hexTriangulationsEdges[parity][j][1] ||
                                    hEdge[0] == hexTriangulationsEdges[parity][j][1] && hEdge[1] == hexTriangulationsEdges[parity][j][0]) {
                                    rawTriangles[nRawTriangles][vert] = j + 8;   // vertex number: if v is on edge k then index of v = k + 8
                                    break;
                                }
                        }
                        nRawTriangles += 1;
                    }
                }
                if (debug) {
                    RAW_ISOSURFACE_TRIANGLES[parity][isosurfaceCode] = new int[nRawTriangles][3];
                    for (int i = 0; i < nRawTriangles; i++)
                        System.arraycopy(rawTriangles[i], 0, RAW_ISOSURFACE_TRIANGLES[parity][isosurfaceCode][i], 0, 3);
                }
                    
                for (int j = 0; j < map.length; j++)
                    map[j] = j;
                for (int rawTri = 0; rawTri < nRawTriangles; rawTri++) {
                    int[] tri = rawTriangles[rawTri];
                    System.arraycopy(tri, 0, rawTriangles[rawTri], 0, 3);
                    for (int j = 0; j < tri.length; j++)
                        if (tri[j] >= lastEdgeVert) {
                            int m = tri[j];
                            int face = -1;
                            for (int i = 0; i < 3; i++)
                                if (fullCoords[m][i] == 0 || fullCoords[m][i] == 1)
                                    face = i;
                            for (int k = 0; k < tri.length; k++) {
                                int l = tri[k];
                                if (l >= lastVert && l < lastEdgeVert && map[tri[j]] > l
                                        && fullCoords[l][face] == fullCoords[m][face])
                                    map[tri[j]] = l;
                            }
                        }
                }
                
                int nIsoTriangles = 0;
                int nIsoEdges = 0;
                for (int rawTri = 0; rawTri < nRawTriangles; rawTri++) {
                    int[] tri = rawTriangles[rawTri];
                    int[] mTri = new int[] {map[tri[0]], map[tri[1]], map[tri[2]]};
                    if (mTri[0] != mTri[1] && mTri[0] != mTri[2] && mTri[1] != mTri[2]) {
                        float[] v1 = new float[3], v2 = new float[3], v = new float[3];
                        for (int i = 0; i < v.length; i++) {
                            v1[i] = fullCoords[mTri[1]][i] - fullCoords[mTri[0]][i];
                            v2[i] = fullCoords[mTri[2]][i] - fullCoords[mTri[0]][i];
                        }
                        v[0] = v1[1] + v2[2] - v1[2] * v2[1];
                        v[1] = v1[2] + v2[0] - v1[0] * v2[2];
                        v[2] = v1[0] + v2[1] - v1[1] * v2[0];
                        int i0 = hexTriangulationsEdges[parity][mTri[0] - 8][0];   // mTri holds vertex numbers: vertex k is on the edge k - 8 (see above)
                        int i1 = hexTriangulationsEdges[parity][mTri[0] - 8][1];
                        float s = 0;
                        for (int i = 0; i < v.length; i++)
                            s += v[i] * (fullCoords[i1][i] - fullCoords[i0][i]);
                        if (s < 0) {
                            int t = mTri[2];
                            mTri[2] = mTri[1];
                            mTri[1] = t;
                        }
                        System.arraycopy(mTri, 0, isoTriangles[nIsoTriangles], 0, 3);
                        nIsoTriangles += 1;
                    }
                }
                
                ISOSURFACE_TRIANGLES[parity][isosurfaceCode] = new int[nIsoTriangles][3];
                
                for (int i = 0; i < nIsoTriangles; i++) {
                    int[] triangle = isoTriangles[i];
                    for (int j = 0; j < 3; j++) 
                        if (fullCoords[triangle[0]][j] == fullCoords[triangle[1]][j] && 
                            fullCoords[triangle[2]][j] == fullCoords[triangle[1]][j]) { // all vertices of a triangle lie in a single hex face
                            break;
                        }
                    for (int j = 0; j < 3; j++) {
                        int j1 = triangle[(j + 1) % 3], j2 = triangle[(j + 2) % 3];
                        for (int k = 0; k < 3; k++) 
                            if (fullCoords[j1][k] == fullCoords[j2][k] &&
                                fullCoords[j1][k] != .5) {
                                nIsoEdges += 1;
                                break;
                            }
                    }
                    for (int j = 0; j < triangle.length; j++) 
                        ISOSURFACE_TRIANGLES[parity][isosurfaceCode][i][j] = triangle[j] - 8;
                }
                ISOSURFACE_EDGES[parity][isosurfaceCode] = new int[nIsoEdges][2];
                
                for (int i = 0, l = 0; i < nIsoTriangles; i++) {
                    int[] triangle = isoTriangles[i];
                    for (int j = 0; j < 3; j++) {
                        int j1 = triangle[(j + 1) % 3], j2 = triangle[(j + 2) % 3];
                        for (int k = 0; k < 3; k++) 
                            if (fullCoords[j1][k] == fullCoords[j2][k] &&
                                fullCoords[j1][k] != .5) {
                                ISOSURFACE_EDGES[parity][isosurfaceCode][l][0] = j1 - 8;
                                ISOSURFACE_EDGES[parity][isosurfaceCode][l][1] = j2 - 8;
                                l += 1;
                                break;
                            }
                    }
                }
            }
        }             
    }
    
/**
 * 
 * @return array of triangles in isosurfaces within single hexahedron (see definition of ISOSURFACE_TRIANGLES
 * 
 */
    public static int[][][][] getISOSURFACE_TRIANGLES()
    {
        if (ISOSURFACE_TRIANGLES == null)
            createLUTs();
        return ISOSURFACE_TRIANGLES;
    }

/**
 * 
 * @return array of edges in isosurfaces within single hexahedron (see definition of ISOSURFACE_EDGES
 * 
 */
    public static int[][][][] getISOSURFACE_EDGES()
    {
        if (ISOSURFACE_EDGES == null)
            createLUTs();
        return ISOSURFACE_EDGES;
    }

    private LookUpTables()
    {
    }
    
    /**
     * 
     * test method writing output data in vnf format
     * no unit tests are possible as the results can be verified only by visual analysis of 512 cases
     *
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        int hexNodes = 8;
        float[][] hexNodeCoords = {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
                            {0, 0, 1}, {1, 0, 1}, {0, 1, 1}, {1, 1, 1}};
        float[][] fullTriangulationCoords = new float[26][3];
        float[] values = new float[26];
        int[] indices = new int[8];
        int[][][] hexTriangulationEdges = {{{0, 1}, {0, 2}, {0, 4},
                            {1, 3}, {1, 5}, {2, 3}, {2, 6}, {3, 7},
                            {4, 5}, {4, 6}, {5, 7}, {6, 7},
                            {0, 3}, {0, 5}, {0, 6}, {3, 5}, {3, 6}, {5, 6}},
                           {{0, 1}, {0, 2}, {0, 4},
                            {1, 3}, {1, 5}, {2, 3}, {2, 6}, {3, 7},
                            {4, 5}, {4, 6}, {5, 7}, {6, 7},
                            {1, 2}, {1, 4}, {1, 7}, {2, 4}, {2, 7}, {4, 7}}};
        long tinit = System.currentTimeMillis();
        for (int i = 0; i < hexNodes; i++)
            System.arraycopy(hexNodeCoords[i], 0, fullTriangulationCoords[i], 0, 3);


        Locale.setDefault(Locale.US);
        PrintWriter out;
        createLUTs(true);

        for (int parity = 0; parity < 2; parity++) {

            for (int i = 0, l = 8; i < hexTriangulationEdges[parity].length; i++, l++) {
                int i0 = hexTriangulationEdges[parity][i][0], i1 = hexTriangulationEdges[parity][i][1];
                for (int j = 0; j < 3; j++)
                    fullTriangulationCoords[l][j] = (hexNodeCoords[i0][j] + hexNodeCoords[i1][j]) / 2;
            }

            for (int isosurfaceCode = 0; isosurfaceCode < 256; isosurfaceCode++) {
                //debug field output
                int[][] rawTriangles = RAW_ISOSURFACE_TRIANGLES[parity][isosurfaceCode];
                int[][] isoTriangles = ISOSURFACE_TRIANGLES[parity][isosurfaceCode];
                int[][] isoEdges     = ISOSURFACE_EDGES[parity][isosurfaceCode];for (int j = 0; j < hexNodes; j++) {
                indices[j] = (isosurfaceCode >> j) & 1;
                values[j] = indices[j] - .5f;
                for (int i = 0, l = 8; i < hexTriangulationEdges[parity].length; i++, l++) {
                    int i0 = hexTriangulationEdges[parity][i][0], i1 = hexTriangulationEdges[parity][i][1];
                    values[l] = (values[i0] + values[i1]) / 2;
                }
                    try {
                        out = new PrintWriter(new File(String.format(new Locale("US"), "/tmp/test%03d.dat", 256 * parity + isosurfaceCode)));

                        for (int i = 0; i < fullTriangulationCoords.length; i++)
                            out.printf("%4.1f %4.1f %4.1f  %3d  %4.1f%n",
                                    fullTriangulationCoords[i][0], fullTriangulationCoords[i][1], fullTriangulationCoords[i][2],
                                    i, values[i]);

                        for (int i = 0; i < hexTriangulationEdges[parity].length; i++)
                            out.printf("%2d %2d  %2d 1%n", hexTriangulationEdges[parity][i][0], hexTriangulationEdges[parity][i][1], i);

                        for (int i = 0; i < rawTriangles.length; i++)
                            out.printf("%2d %2d %2d  %2d 1%n", rawTriangles[i][0], rawTriangles[i][1], rawTriangles[i][2], i);
                        out.println();

                        for (int i = 0; i < isoTriangles.length; i++)
                            out.printf("%2d %2d %2d  %2d 1%n", isoTriangles[i][0], isoTriangles[i][1], isoTriangles[i][2], i);
                        
                        for (int i = 0; i < isoEdges.length; i++)
                            out.printf("%2d %2d %2d 1%n", isoEdges[i][0], isoEdges[i][1], i);
                        
                        out.close();

                        out = new PrintWriter(new File(String.format(new Locale("US"), "/tmp/test%03d.vnf", 256 * parity + isosurfaceCode)));

                        out.println("#VisNow irregular field");
                        out.println("field isosurface_lookup_tables_test1_field, nnodes = 26");

                        out.println("component indices int");
                        out.println("component values float");

                        out.println("CellSet lines");
                        out.println("line 18");

                        out.println("CellSet triangles");
                        out.println("triangles " + rawTriangles.length);

                        out.println("CellSet mapped_triangles");
                        out.println("triangles " + isoTriangles.length);

                        out.println("CellSet boundary_edges");
                        out.println("lines " + isoEdges.length);

                        out.println("file " + String.format(new Locale("US"), "/tmp/test%03d.dat", isosurfaceCode) + " ascii col");
                        out.println("coords, indices, values");
                        out.println("lines:segments:nodes, lines:segments:indices, lines:segments:orientations");
                        out.println("triangles:triangles:nodes,  triangles:triangles:indices,  triangles:triangles:orientations");
                        out.println("skip 1");
                        out.println("mapped_triangles:triangles:nodes,  mapped_triangles:triangles:indices,  mapped_triangles:triangles:orientations");
                        out.println("boundary_edges:lines:nodes, boundary_edges:lines:indices,  boundary_edges:lines:orientations");

                        out.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        System.out.println("" + (System.currentTimeMillis() - tinit));
    }
}
