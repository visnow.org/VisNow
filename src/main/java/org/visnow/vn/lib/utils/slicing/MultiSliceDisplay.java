/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.slicing;

import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.objects.generics.*;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
import org.visnow.vn.lib.utils.probeInterfaces.MultiDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class MultiSliceDisplay extends MultiDisplay
{
    protected OpenBranchGroup slicesGroup  = new OpenBranchGroup();
    protected OpenBranchGroup parentSlicesGroup  = new OpenBranchGroup();
    protected IrregularField mergedField = null;
    protected int resolution = 300;
    private int lastIX = -1, lastIY = -1;
    
    
    public MultiSliceDisplay(BasicPlanarSliceParams parameters)
    {
        super();
    }
    
    public MultiSliceDisplay()
    {
        super();
    }
    
    @Override
    public void addDisplay(IrregularField field)
    {
    }
    
    public void addDisplay(Field field, DataMappingParams dataMappingParams,
                           float[] planeCenter, float[] planeX, float[] planeY, 
                           float rX, float rY)
    {
        if (field != null) {
            SliceDisplay display = new SliceDisplay(field, dataMappingParams,
                                                    planeCenter, planeX, planeY, 
                                                    rX, rY, resolution);
            display.setProbesPosition(probesPosition);
            display.setPointerLine(pointerLine);
            display.updateImage(dataMappingParams);
            displays.add(display);
            startTime = field.getStartTime();
            endTime   = field.getEndTime();
        }
        computeMergedField();
        updateSlicesGroup();
    }
    
    public void setTime(float time)
    {
        for (ProbeDisplay display : displays)
            ((SliceDisplay)display).setTime(time);
    }
    
    @Override
    public void removeDisplay(ProbeDisplay pickedGraph)
    {
        super.removeDisplay(pickedGraph);
        updateSlicesGroup();
    }
    
    @Override
    public void clearDisplays()
    {
        super.clearDisplays();
        updateSlicesGroup();
    }
    
    
    public SliceDisplay pickedDisplay(int ix, int iy)
    {
        for (ProbeDisplay pDisplay : displays) {
            SliceDisplay display = (SliceDisplay)pDisplay;
            if (display.isInside(ix, iy)) 
                return display;
        }
        return null;
    }
    
    public void showTooltip(int ix, int iy, int scrX, int scrY)
    {
        if (ix == lastIX && iy == lastIY)
            return;
        lastIX = ix;
        lastIY = iy;
        try {
            for (ProbeDisplay display : displays) {
                SliceDisplay sliceDisplay = (SliceDisplay)display;
                sliceDisplay.setTooltipLocation(ix, iy);
            }
        } catch (Exception e) {
        }
    }
    
    public static RegularField repositionInterpolatedPlane(RegularField planarInterpolatedSliceField,
                                                           float[] planeCenter, 
                                                           float[] planeX, float[] planeY)
    {
        RegularField interpolatedSliceField = planarInterpolatedSliceField.cloneShallow();
        float[][] planeAf = interpolatedSliceField.getAffine();
        float[][] outInterpolatedAf = new float[4][3];
        for (int i = 0; i < 3; i++) {
            for (float[] outInterpolatedAf1 : outInterpolatedAf)
                outInterpolatedAf1[i] = 0;
            outInterpolatedAf[0][i] += planeAf[0][0] * planeX[i];
            outInterpolatedAf[1][i] += planeAf[1][1] * planeY[i];
            outInterpolatedAf[3][i] += planeCenter[i] + planeAf[3][0] * planeX[i] + 
                                                        planeAf[3][1] * planeY[i];
        }
        interpolatedSliceField.setAffine(outInterpolatedAf);
        return interpolatedSliceField;
    }
    
    protected float slice2DScale = .2f;
    protected int[] slice2DOffset = {30, 60};

    public void setSlice2DResolution(int resolution)
    {
        this.resolution = resolution;
        for (ProbeDisplay display : displays)
            ((SliceDisplay)display).setResolution(resolution);
    }
    
    public void setSlice2DScale(float slice2DScale)
    {
        this.slice2DScale = slice2DScale;
    }

    public void setPlanarSlicesPosition(Position position)
    {
        this.probesPosition = position;
    }

    public void setTitleFontSize(float s)
    {
        relativeFontHeight = s;
        for (ProbeDisplay display : displays) 
            display.setTitleFontSize(relativeFontHeight);
    }
    
    public void updateDataMaps(DataMappingParams dataMappingParams)
    { 
        for (ProbeDisplay probeDisplay : displays) 
            ((SliceDisplay) probeDisplay).updateImage(dataMappingParams);
    }
    
    protected int currentPosition = 0;
    
    @Override
    public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
    {   
        windowWidth = width;
        windowHeight = height;
        int fontSize = (int)(relativeFontHeight * height);
        ltw.update();
        int ix = 10, iy = 10 + fontSize;
        int index = 0;
        for (ProbeDisplay probeDisplay : displays) {
            SliceDisplay slice = (SliceDisplay)probeDisplay;
            slice.updateScreenCoords(ltw);
            int[] sDims = slice.getImageDims();
            float s = slice2DScale * (float)Math.sqrt((double)(width * height) / (sDims[0] * sDims[1]));
            int w = (int)(s * sDims[0]);
            int h = (int)(s * sDims[1]);
            switch (probesPosition) {
            case AT_POINT:
                int[] point = slice.getScreenHandle();
                slice.draw(gr, ltw, point[0], point[1], w, h, fontSize, String.format(" %d ", index)); 
                break;
            case RIGHT:
                slice.draw(gr, ltw, windowWidth - w - 10, iy + h, w, h, fontSize, String.format(" %d ", index));
                iy += h + 10 + fontSize;
                break;
            case LEFT:
                slice.draw(gr, ltw, 10, iy + h, w, h, fontSize, String.format(" %d ", index));
                iy += h + 10 + fontSize;
                break;
            case BOTTOM:
                slice.draw(gr, ltw, ix, (int)(height - 1.5 * fontSize), w, h, fontSize, String.format(" %d ", index));
                ix += w + 10;
                break;
            case TOP:
                slice.draw(gr, ltw, ix, h + 10, w, h, fontSize, String.format(" %d ", index));
                ix += w + 10;
                break;
            }
            index += 1;
        }
        for (ProbeDisplay probeDisplay : displays) 
            ((SliceDisplay)probeDisplay).showTooltip(gr);
        xSort = probesPosition == TOP || probesPosition == BOTTOM;
    }
    
    void updateSlicesGroup()
    {
        slicesGroup.detach();
        slicesGroup.removeAllChildren();
        for (ProbeDisplay display : displays)
            slicesGroup.addChild(((SliceDisplay)display).getSliceGroup());
        parentSlicesGroup.addChild(slicesGroup);
    }

    public OpenBranchGroup getParentSlicesGroup()
    {
        return parentSlicesGroup;
    }

}
