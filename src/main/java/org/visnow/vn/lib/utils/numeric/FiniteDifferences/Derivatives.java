/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.FiniteDifferences;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Derivatives
{

    private Derivatives()
    {
    }
    
    private abstract static class Compute implements Runnable
    {
        protected final int nThreads;
        protected final int iThread;
        protected final long[] dims;
        protected final FloatLargeArray invJacobian;
        protected final LargeArray val;
        protected final FloatLargeArray deriv; //ordered: dv0/di0, dv1/di0, dv2/di0, dv0/di1, dv1/di1, dv2/di1, dv0/di2, dv1/di2, dv2/di2 
        protected final long kstart;
        protected final long kend;

        public Compute(int nThreads, int iThread, long[] dims, FloatLargeArray invJacobian, LargeArray val, FloatLargeArray grad)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.invJacobian = invJacobian;
            this.val = val;
            this.deriv = grad;
            kstart = (iThread       * dims[dims.length - 1]) / nThreads;
            kend =   ((iThread + 1) * dims[dims.length - 1]) / nThreads;
        }
    }

    private static class Compute2DLargeGradient extends Compute
    {
        
        public Compute2DLargeGradient(int nThreads, int iThread, long[] dims, FloatLargeArray invJacobian, LargeArray val, FloatLargeArray grad)
        {
            super(nThreads, iThread, dims, invJacobian, val, grad);
        }
        

        @Override
        public void run()
        {
            long m = dims[0];
            float[] v = new float[2];
            float[] w = new float[2];
            for (long j = kstart; j < kend; j++)
                for (long k = 0, n = j * m; k < dims[0]; k++, n++) {
                    if (k == 0)
                        v[0] = val.getFloat(n + 1) - val.getFloat(n);
                    else if (k == dims[0] - 1)
                        v[0] = val.getFloat(n) - val.getFloat(n - 1);
                    else
                        v[0] = .5f * (val.getFloat(n + 1) - val.getFloat(n - 1));
                    if (j == 0)
                        v[1] = val.getFloat(n + m) - val.getFloat(n);
                    else if (j == dims[1] - 1)
                        v[1] = val.getFloat(n) - val.getFloat(n - m);
                    else
                        v[1] = .5f * (val.getFloat(n + m) - val.getFloat(n - m));
                    long l = 0;
                    if (invJacobian.length() > 4)
                        l = 4 * n;
                    for (int p = 0; p < 2; p++) {
                        w[p] = 0;
                        for (int q = 0; q < 2; q++)
                            w[p] += invJacobian.getFloat(l + 2 * p + q) * v[q];
                    }
                    LargeArrayUtils.arraycopy(w, 0, deriv, 2 * n, 2);
                }
        }
    }

    private static class Compute3DLargeGradient extends Compute
    {
        public Compute3DLargeGradient(int nThreads, int iThread, long[] dims, FloatLargeArray invJacobian, LargeArray val, FloatLargeArray grad)
        {
            super(nThreads, iThread, dims, invJacobian, val, grad);
        }
        
        @Override
        public void run()
        {
            long m = dims[0] * dims[1];
            float[] v = new float[3];
            float[] w = new float[3];
            long kstart = (iThread       * dims[2]) / nThreads;
            long kend =   ((iThread + 1) * dims[2]) / nThreads;
            for (long i = kstart; i < kend; i++)
                for (long j = 0, n = i * m; j < dims[1]; j++)
                    for (long k = 0; k < dims[0]; k++, n++) {
                        if (k == 0)
                            v[0] = val.getFloat(n + 1) - val.getFloat(n);
                        else if (k == dims[0] - 1)
                            v[0] = val.getFloat(n) - val.getFloat(n - 1);
                        else
                            v[0] = .5f * (val.getFloat(n + 1) - val.getFloat(n - 1));
                        if (j == 0)
                            v[1] = val.getFloat(n + dims[0]) - val.getFloat(n);
                        else if (j == dims[1] - 1)
                            v[1] = val.getFloat(n) - val.getFloat(n - dims[0]);
                        else
                            v[1] = .5f * (val.getFloat(n + dims[0]) - val.getFloat(n - dims[0]));
                        if (i == 0)
                            v[2] = val.getFloat(n + m) - val.getFloat(n);
                        else if (i == dims[2] - 1)
                            v[2] = val.getFloat(n) - val.getFloat(n - m);
                        else
                            v[2] = .5f * (val.getFloat(n + m) - val.getFloat(n - m));
                        long l = 0;
                        if (invJacobian.length() > 9)
                            l = 9 * n;
                        for (int p = 0; p < 3; p++) {
                            w[p] = 0;
                            for (int q = 0; q < 3; q++)
                                w[p] += invJacobian.getFloat(l + 3 * p + q) * v[q];
                        }
                        LargeArrayUtils.arraycopy(w, 0, deriv, 3 * n, 3);
                    }
        }
    }

    private static class Compute3DLargeVectorDerivatives extends Compute
    {
        public Compute3DLargeVectorDerivatives(int nThreads, int iThread, long[] dims, FloatLargeArray invJacobian, LargeArray val, FloatLargeArray deriv)
        {
            super(nThreads, iThread, dims, invJacobian, val, deriv);
        }

        @Override
        public void run()
        {
            long off1 = 3 * dims[0];
            long off2 = 3 * dims[0] * dims[1];
            float[] v = new float[3];
            float[] w = new float[3];
            for (long i = kstart; i < kend; i++) {
                for (long j = 0, n = 3 * i * off2, m = i * off2; j < dims[1]; j++)
                    for (long k = 0; k < dims[0]; k++, n += 9)
                        for (int l = 0; l < 3; l++, m++) {
                            if (k == 0)
                                v[0] = val.getFloat(m + 3) - val.getFloat(m);
                            else if (k == dims[0] - 1)
                                v[0] = val.getFloat(m) - val.getFloat(m - 3);
                            else
                                v[0] = .5f * (val.getFloat(m + 3) - val.getFloat(m - 3));
                            if (j == 0)
                                v[1] = val.getFloat(m + off1) - val.getFloat(m);
                            else if (j == dims[1] - 1)
                                v[1] = val.getFloat(m) - val.getFloat(m - off1);
                            else
                                v[1] = .5f * (val.getFloat(m + off1) - val.getFloat(m - off1));
                            if (i == 0)
                                v[2] = val.getFloat(m + off2) - val.getFloat(m);
                            else if (i == dims[2] - 1)
                                v[2] = val.getFloat(m) - val.getFloat(m - off2);
                            else
                                v[2] = .5f * (val.getFloat(m + off2) - val.getFloat(m - off2));
                            long s = 0;
                            if (invJacobian.length() > 9)
                                s = n;
                            for (int p = 0; p < 3; p++) {
                                w[p] = 0;
                                for (int q = 0; q < 3; q++)
                                    w[p] += invJacobian.getFloat(s + 3 * p + q) * v[q];
                                deriv.setFloat(n + l + 3 * p, w[p]);
                            }
                        }
            }
        }
    }

    private static class Compute2DLargeVectorDerivatives extends Compute
    {
        public Compute2DLargeVectorDerivatives(int nThreads, int iThread, long[] dims, FloatLargeArray invJacobian, LargeArray val, FloatLargeArray deriv)
        {
            super(nThreads, iThread, dims, invJacobian, val, deriv);
        }

        @Override
        public void run()
        {
            long off1 = 2 * dims[0];
            float[] v = new float[2];
            float[] w = new float[2];
            for (long j = kstart; j < kend; j++)
                for (long k = 0, n = 2 * j * off1, m = j * off1; k < dims[0]; k++, n += 4)
                    for (long l = 0; l < 2; l++, m++) {
                        if (k == 0)
                            v[0] = val.getFloat(m + 2) - val.getFloat(m);
                        else if (k == dims[0] - 1)
                            v[0] = val.getFloat(m) - val.getFloat(m - 2);
                        else
                            v[0] = .5f * (val.getFloat(m + 2) - val.getFloat(m - 2));
                        if (j == 0)
                            v[1] = val.getFloat(m + off1) - val.getFloat(m);
                        else if (j == dims[1] - 1)
                            v[1] = val.getFloat(m) - val.getFloat(m - off1);
                        else
                            v[1] = .5f * (val.getFloat(m + off1) - val.getFloat(m - off1));
                        long s = 0;
                        if (invJacobian.length() > 4)
                            s = n;
                        for (int p = 0; p < 2; p++) {
                            w[p] = 0;
                            for (int q = 0; q < 2; q++)
                                w[p] += invJacobian.getFloat(s + 2 * p + q) * v[q];
                            deriv.setFloat(n + l + 2 * p, w[p]);
                        }
                    }
        }
    }
    
    
    
    public static void computeLargeDerivatives(int nThreads, int[] idims, LargeArray vals, FloatLargeArray invJacobian, FloatLargeArray derivs)
    {
        Thread[] workThreads;
        workThreads = new Thread[nThreads];
        int dim = idims.length;
        long[] dims = new long[dim];
        long nNodes = 1;
        for (int i = 0; i < dim; i++) {
            dims[i] = idims[i];
            nNodes *= dims[i];            
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            if (vals.length() / nNodes == 1) {
                if (dim == 3)
                    workThreads[iThread] = new Thread(new Compute3DLargeGradient(nThreads, iThread, dims, invJacobian, vals, derivs));
                else if (dim == 2)
                    workThreads[iThread] = new Thread(new Compute2DLargeGradient(nThreads, iThread, dims, invJacobian, vals, derivs));
            } else if (vals.length() / nNodes == dim) {
                if (dim == 3)
                    workThreads[iThread] = new Thread(new Compute3DLargeVectorDerivatives(nThreads, iThread, dims, invJacobian, vals, derivs));
                else if (dim == 2)
                    workThreads[iThread] = new Thread(new Compute2DLargeVectorDerivatives(nThreads, iThread, dims, invJacobian, vals, derivs));
            }
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }

    public static FloatLargeArray computeLargeDerivatives(int nThreads, int[] dims, LargeArray vals, FloatLargeArray invJacobian)
    {
        FloatLargeArray outData = new FloatLargeArray(dims.length * vals.length());
        computeLargeDerivatives(nThreads, dims, vals, invJacobian, outData);
        return outData;
    }

    public static FloatLargeArray symmetrize(int dim, FloatLargeArray in)
    {
        if (dim == 1)
            return in;
        long nData = in.length() / (dim * dim);
        int d = (dim * (dim + 1)) / 2;
        FloatLargeArray h = new FloatLargeArray(nData * d);
        if (dim == 2)
            for (long k = 0, l = 0, m = 0; k < nData; k++, l += 3, m += 4) {
                h.setFloat(l, in.getFloat(m));
                h.setFloat(l + 1, .5f * (in.getFloat(m + 1) + in.getFloat(m + 2)));
                h.setFloat(l + 2, in.getFloat(m + 3));
            }
        else if (dim == 3)
            for (long k = 0, l = 0, m = 0; k < nData; k++, l += 6, m += 9) {
                h.setFloat(l, in.getFloat(m));
                h.setFloat(l + 1, .5f * (in.getFloat(m + 1) + in.getFloat(m + 3)));
                h.setFloat(l + 2, .5f * (in.getFloat(m + 2) + in.getFloat(m + 6)));
                h.setFloat(l + 3, in.getFloat(m + 4));
                h.setFloat(l + 4, .5f * (in.getFloat(m + 5) + in.getFloat(m + 7)));
                h.setFloat(l + 5, in.getFloat(m + 8));
            }
        return h;
    }
}
