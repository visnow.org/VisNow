/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isosurface;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.utils.IntDataCache;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class CellCache
{

    public static final int BUCKETS_NUMBER = 32768;

    public static final int NULL_CEILING_ARRAY = -1;
    public static final int WRONG_CEILING_ARRAY = -2;
    public static final int NULL_BUCKETS_ARRAY = -3;
    public static final int WRONG_BUCKETS_ARRAY = -4;
    
    public static final int[] TRIANGULATION_SIZE
            = {
                1, 2, 3, 6
            };

    private final DataArray data;
    private final CellSet cellSet;
    private final float[] bucketsCeilingFun;
    private final int[][] buckets;
    private transient FloatValueModificationListener statusListener = null;

    public CellCache(DataArray data, CellSet cellSet, float[] bucketsCeilingFun, int[][] buckets, FloatValueModificationListener statusListener)
    {
        this.data = data;
        this.cellSet = cellSet;
        this.bucketsCeilingFun = bucketsCeilingFun;
        this.buckets = buckets;
        this.statusListener = statusListener;
    }

    public long computeCache()
    {
        if (bucketsCeilingFun == null)
            return NULL_CEILING_ARRAY;
        if (bucketsCeilingFun.length != BUCKETS_NUMBER)
            return WRONG_CEILING_ARRAY;
        if (buckets == null)
            return NULL_BUCKETS_ARRAY;
        if (buckets.length != BUCKETS_NUMBER)
            return WRONG_BUCKETS_ARRAY;
        float dataMin = (float) data.getPreferredMinValue();
        float dataMax = (float) data.getPreferredMaxValue();
        float[] isoData;
        if (data.getVectorLength() == 1) {
            isoData = data.getRawFloatArray().getData();
        } else {
            isoData = data.getVectorNorms().getData();
        }
        float bucketFactor = BUCKETS_NUMBER / (dataMax - dataMin);

        //TODO który cellSet? Wybór w parametrach?
        //each cellArray in cellSet contains different types of cells)
        //we are interested only in 3D cell types(4-7 indexes, see class Cell)
        //count how many cells we are going to get
        int finalNCells = 0;
        for (int caInd = 4; caInd < 8; ++caInd) {
            CellArray ca = cellSet.getCellArray(CellType.getType(caInd));
            if (ca != null) {
                finalNCells += ca.getNCells() * TRIANGULATION_SIZE[caInd - 4];
            }
        }

        /* This solution uses algorithm which optimizes access to cells by data value
         * (used data structure is built once, then used for all possible threshold values)
         *
         * Cells are mapped to a 2D plane (preferredMinValue data value of a cell VS preferredMaxValue data value)
         * Plane is triangular (all cells are above diagonal).
         * Is is likely that for a single cell preferredMaxValue and preferredMinValue data values will be close,
         * then all cells should be near to diagonal of the plane.
         *
         * Search by threshold value should only consider cells that have preferredMinValue<thr && preferredMaxValue>thr,
         * which cuts out only small part of diagonal high-density region.
         *
         * 'Min value' axis is discretized into buckets, cells in buckets are not sorted
         */
        IntDataCache[] bucketsNodes = new IntDataCache[BUCKETS_NUMBER];
        int cellCacheSize = (int) max((float) finalNCells / BUCKETS_NUMBER * 0.6f, 100.0f);
        for (int i = 0; i < BUCKETS_NUMBER; ++i) {
            bucketsNodes[i] = new IntDataCache(cellCacheSize, 4);
            bucketsCeilingFun[i] = Float.MIN_VALUE;
        }

        //process cells
        int totalCells = 0;
        int cellsProcessed = 0;
        for (int caInd = 4; caInd < 8; ++caInd) {
            CellArray ca = cellSet.getCellArray(CellType.getType(caInd));
            if (ca != null) {
                CellArray triangulated = ca.getTriangulated(); //composed of Tetra cells
                int nCells = triangulated.getNCells();
                totalCells += nCells;
                int[] nodes = triangulated.getNodes(); //indexes of nodes
                assert nodes.length == nCells * 4;
                for (int cellInd = 0; cellInd < nCells; ++cellInd) { //for every tetra cell
                    float min = isoData[nodes[cellInd * 4]];
                    float max = min;
                    for (int i = 1; i < 4; i++) {
                        float t = isoData[nodes[cellInd * 4 + i]];
                        if (max < t)
                            max = t;
                        if (min > t)
                            min = t;
                    }
                    int bucketNum = (int) ((min - dataMin) * bucketFactor);
                    if (bucketNum >= BUCKETS_NUMBER) {
                        bucketNum = BUCKETS_NUMBER - 1;
                    }
                    bucketsCeilingFun[bucketNum] = max(bucketsCeilingFun[bucketNum], max);
                    bucketsNodes[bucketNum].put(nodes, cellInd * 4);
                    cellsProcessed++;
                    if (cellsProcessed % 100 == 0) {
                        fireStatusChanged(0.9f * cellsProcessed / finalNCells);
                    }
                }
            }
        }

        //sort data in buckets and get raw nodes data
        int maxBucketLength = 0;
        for (int i = 0; i < BUCKETS_NUMBER; ++i) {
            buckets[i] = bucketsNodes[i].getContigous();
            if (buckets[i].length > maxBucketLength)
                maxBucketLength = buckets[i].length;
            fireStatusChanged(0.9f + (0.1f * i / BUCKETS_NUMBER));
        }

        //compute ceiling function
        for (int i = 1; i < BUCKETS_NUMBER; ++i) {
            bucketsCeilingFun[i] = max(bucketsCeilingFun[i - 1], bucketsCeilingFun[i]);
        }
        return System.currentTimeMillis();
    }

    protected void fireStatusChanged(float status)
    {
        if (statusListener != null) {
            FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
            statusListener.floatValueChanged(e);
        }
    }
 /**
   * This solution uses algorithm which optimizes access to cells by data value
         * (used data structure is built once, then used for all possible threshold values)
         *
         * Cells are mapped to a 2D plane (preferredMinValue data value of a cell VS preferredMaxValue data value)
         * Plane is triangular (all cells are above diagonal).
         * Is is likely that for a single cell preferredMaxValue and preferredMinValue data values will be close,
         * then all cells should be near to diagonal of the plane.
         *
         * Search by threshold value should only consider cells that have preferredMinValue<thr && preferredMaxValue>thr,
         * which cuts out only small part of diagonal high-density region.
         *
         * 'Min value' axis is discretized into buckets, cells in buckets are sorted
         * 
  * @param data              DataArray  to be cached
  * @param cellSet           Cells of this set 
  * @param bucketsCeilingFun
  * @param buckets
  * @return 
  */
    public static final int computeCellCache(DataArray data, CellSet cellSet, float[] bucketsCeilingFun, int[][] buckets)
    {
        if (bucketsCeilingFun == null)
            return NULL_CEILING_ARRAY;
        if (bucketsCeilingFun.length != BUCKETS_NUMBER)
            return WRONG_CEILING_ARRAY;
        if (buckets == null)
            return NULL_BUCKETS_ARRAY;
        if (buckets.length != BUCKETS_NUMBER)
            return WRONG_BUCKETS_ARRAY;
        float dataMin = (float) data.getPreferredMinValue();
        float dataMax = (float) data.getPreferredMaxValue();
        float[] isoData;
        if (data.getVectorLength() == 1) {
            isoData = data.getRawFloatArray().getData();
        } else {
            isoData = data.getVectorNorms().getData();
        }
        float bucketFactor = BUCKETS_NUMBER / (dataMax - dataMin);
        int finalNCells = 0;
        for (int caInd = 4; caInd < 8; ++caInd) {
            CellArray ca = cellSet.getCellArray(CellType.getType(caInd));
            if (ca != null) {
                finalNCells += ca.getNCells() * TRIANGULATION_SIZE[caInd - 4];
            }
        }

        /* This solution uses algorithm which optimizes access to cells by data value
         * (used data structure is built once, then used for all possible threshold values)
         *
         * Cells are mapped to a 2D plane (preferredMinValue data value of a cell VS preferredMaxValue data value)
         * Plane is triangular (all cells are above diagonal).
         * Is is likely that for a single cell preferredMaxValue and preferredMinValue data values will be close,
         * then all cells should be near to diagonal of the plane.
         *
         * Search by threshold value should only consider cells that have preferredMinValue<thr && preferredMaxValue>thr,
         * which cuts out only small part of diagonal high-density region.
         *
         * 'Min value' axis is discretized into buckets, cells in buckets are not sorted
         */
        IntDataCache[] bucketsNodes = new IntDataCache[BUCKETS_NUMBER];
        int cellCacheSize = (int) max((float) finalNCells / BUCKETS_NUMBER * 0.6f, 100.0f);
        for (int i = 0; i < BUCKETS_NUMBER; ++i) {
            bucketsNodes[i] = new IntDataCache(cellCacheSize, 4);
            bucketsCeilingFun[i] = Float.MIN_VALUE;
        }

        for (int caInd = 4; caInd < 8; ++caInd) {
            CellArray ca = cellSet.getCellArray(CellType.getType(caInd));
            if (ca != null) {
                CellArray triangulated = ca.getTriangulated(); //composed of Tetra cells
                int nCells = triangulated.getNCells();
                int[] nodes = triangulated.getNodes(); //indexes of nodes
                assert nodes.length == nCells * 4;
                for (int cellInd = 0; cellInd < nCells; ++cellInd) { //for every tetra cell
                    float min = isoData[nodes[cellInd * 4]];
                    float max = min;
                    for (int i = 1; i < 4; i++) {
                        float t = isoData[nodes[cellInd * 4 + i]];
                        if (max < t)
                            max = t;
                        if (min > t)
                            min = t;
                    }
                    int bucketNum = (int) ((min - dataMin) * bucketFactor);
                    if (bucketNum >= BUCKETS_NUMBER) {
                        bucketNum = BUCKETS_NUMBER - 1;
                    }
                    bucketsCeilingFun[bucketNum] = max(bucketsCeilingFun[bucketNum], max);
                    bucketsNodes[bucketNum].put(nodes, cellInd * 4);
                    }
                }
            }
        //sort data in buckets and get raw nodes data
        int maxBucketLength = 0;
        for (int i = 0; i < BUCKETS_NUMBER; i++) {
            buckets[i] = bucketsNodes[i].getContigous();
            if (buckets[i].length > maxBucketLength)
                maxBucketLength = buckets[i].length;
        }

        //compute ceiling function
        for (int i = 1; i < BUCKETS_NUMBER; ++i) {
            bucketsCeilingFun[i] = max(bucketsCeilingFun[i - 1], bucketsCeilingFun[i]);
        }
        return 0;
    }
    
}
