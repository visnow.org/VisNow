/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.lineProbe;

import org.visnow.vn.lib.utils.probeInterfaces.Probe;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import org.jogamp.java3d.LineAttributes;
import static org.jogamp.java3d.LineAttributes.PATTERN_SOLID;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.geometries.objects.generics.*;
import static org.apache.commons.math3.util.FastMath.sqrt;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.abs;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.utils.CellSetGeometryUtilities;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.interactiveGlyphs.GlyphGeometryParams;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class BasicLineProbe extends Probe
{

    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
        mapParams = dataMappingParams;
    }

    public static enum SliceOperation {SLICE, BELOW, ABOVE};

    protected Field inField = null;
    protected int trueNSpace = 3;
    protected RegularField regularInField = null;
    protected IrregularField irregularInField = null;
    protected int[] dims;
    protected int nCells;
    protected float[] cellRadii;
    protected float[] cellCenters;
    protected boolean regularInFieldHasCoords = true;
    protected IrregularField sliceField = null;
    protected IrregularField sliceAreaField = null;
    protected RegularField regularSliceField = null;

    protected int lastAxis = 2;
    protected FloatLargeArray coords;

    protected InteractiveGlyph       glyph       = new InteractiveGlyph(LINE);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI    glyphGUI    = glyph.getComputeUI();
    protected GlyphGeometryParams    glyphGeometryParams;

    protected float[]                center      = glyphParams.getCenter();
    protected float[]                direction   = glyphParams.getW();
    protected float[]                orthoU      = glyphParams.getU();
    protected float[]                orthoV      = glyphParams.getV();

    protected DataMappingParams mapParams;
    protected OpenShape3D probeShape = new OpenShape3D();
    protected OpenBranchGroup  probeGroup = new OpenBranchGroup();
    protected IndexedLineArray probeLines;
    protected OpenBranchGroup probeParentGroup = new OpenBranchGroup();

    public BasicLineProbe()
    {
        glyph.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                updateSlice();
                show();
            }
        });
        probeShape.setName("probeShape");
        probeGroup.setName("probeGroup");
        probeParentGroup.setName("probeParentGroup");
        glyph.setName("probeGlyph");
        glyph.addGeometry(probeParentGroup);
    }

    private class ComputeGeometryData implements Runnable {

        int nThreads = 1;
        int iThread = 0;
        FloatLargeArray coords;

        public ComputeGeometryData(int nThreads, int iThread, FloatLargeArray coords) {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.coords = coords;
        }

        @Override
        public void run() {
            float[] crds = coords.getData();
            switch (dims.length) {
            case 3:
                int[] off3 = new int[]{                                      3,
                                                               3 * dims[0],
                                                               3 * dims[0] + 3,
                                       3 * dims[1] * dims[0],
                                       3 * dims[1] * dims[0]               + 3,
                                       3 * dims[1] * dims[0] + 3 * dims[0],
                                       3 * dims[1] * dims[0] + 3 * dims[0] + 3};
                for (int i =  (iThread      * (dims[2] - 1)) / nThreads;
                         i < ((iThread + 1) * (dims[2] - 1)) / nThreads;
                         i++) {
                    int currentCell = i * (dims[1] - 1) * (dims[0] - 1);
                    for (int j = 0; j < dims[1] - 1; j++)
                        for (int k = 0; k < dims[0] - 1; k++, currentCell++) {
                            int l = 3 * ((i * dims[1] + j) * dims[0] + k);
                            float[] v = new float[3];
                            float rCell = 0;
                            for (int m = 0; m < 3; m++)
                                v[m] = crds[l + m];
                            for (int m = 0; m < off3.length; m++) {
                                float r = 0;
                                for (int n = 0; n < 3; n++)
                                    r += (crds[l + off3[m] + n] - v[n]) *
                                         (crds[l + off3[m] + n] - v[n]);
                                if (r > rCell)
                                    rCell = r;
                            }
                            cellRadii[currentCell] = (float) sqrt(rCell);
                        }
                }
                break;
            case 2:
                int[] off2 = new int[]{              3,
                                       3 * dims[0],
                                       3 * dims[0] + 3};
                for (int j = (iThread  * (dims[1] - 1)) / nThreads;
                         j < ((iThread + 1) * (dims[1] - 1)) / nThreads;
                         j++) {
                    int currentCell = j * (dims[0] - 1);
                    for (int k = 0; k < dims[0] - 1; k++, currentCell++) {
                        int l = 3 * (j * dims[0] + k);
                        float[] v = new float[3];
                        float rCell = 0;
                        for (int m = 0; m < 3; m++)
                            v[m] = crds[l + m];
                        for (int m = 0; m < off2.length; m++) {
                            float r = 0;
                            for (int n = 0; n < 3; n++)
                                r += (crds[l + off2[m] + n] - v[n]) *
                                     (crds[l + off2[m] + n] - v[n]);
                            if (r > rCell)
                                rCell = r;
                        }
                        cellRadii[currentCell] = (float) sqrt(rCell);
                    }
                }
                break;
            }
        }
    }

/**
 * computes array of diameters of cells
 * Slicer uses these diameters to test if a cell can be nontrivially sliced
 */
    public void computeCellGeometryData() {
        dims = regularInField.getDims();
        if (!regularInFieldHasCoords)
            return;
        nCells = 1;
        for (int i = 0; i < dims.length; i++)
            nCells *= dims[i] - 1;
        cellRadii = new float[nCells];
        int nThreads = ConcurrencyUtils.getNumberOfThreads();
        Future[] futures = new Future[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            futures[iThread] = ConcurrencyUtils.submit(new ComputeGeometryData(nThreads, iThread, coords));
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException();
        }
    }

    public IrregularField probeRegularField3D()
    {
        float rhsX = orthoU[0] * center[0] + orthoU[1] * center[1] + orthoU[2] * center[2];
        float rhsY = orthoV[0] * center[0] + orthoV[1] * center[1] + orthoV[2] * center[2];
        int[] offsets = new int[] {
                                      0,                               1,
                                dims[0] + 1,                     dims[0],
            dims[0] * dims[1],           dims[0] * dims[1]           + 1,
            dims[0] * dims[1] + dims[0] + 1, dims[0] * dims[1] + dims[0],
        };

        FieldLineProbe probe = new FieldLineProbe(regularInField, center, direction);
        int[] splicedCell = new int[8];
        for (int i2 = 0, icell = 0; i2 < dims[2] - 1; i2++) {
            for (int i1 = 0; i1 < dims[1] - 1; i1++) {
                for (int i0 = 0, l = (dims[1] * i2 + i1) * dims[0]; i0 < dims[0] - 1; i0++, l++, icell++) {
                    float tX = orthoU[0] * coords.get(3 * l) +
                               orthoU[1] * coords.get(3 * l + 1) +
                               orthoU[2] * coords.get(3 * l + 2) - rhsX;
                    float tY = orthoV[0] * coords.get(3 * l) +
                               orthoV[1] * coords.get(3 * l + 1) +
                               orthoV[2] * coords.get(3 * l + 2) - rhsY;
                    if (abs(tX) <= cellRadii[icell] && abs(tY) <= cellRadii[icell]) {
                        for (int i = 0; i < offsets.length; i++)
                            splicedCell[i] = l + offsets[i];
                        probe.processCell(splicedCell, (i0 + i1 + i2) % 2 == 0);
                    }
                }
            }
        }
        sliceField = probe.createOutField();
        return sliceField;
    }

    public IrregularField probeRegularField2D()
    {
        float rhsY = orthoV[0] * center[0] + orthoV[1] * center[1] ;
        int[] offsets = new int[] {0,  1, dims[0] + 1, dims[0]};

        FieldLineProbe probe = new FieldLineProbe(regularInField, center, direction);
        int[] splicedCell = new int[4];
            for (int i1 = 0, icell = 0; i1 < dims[1] - 1; i1++) {
                for (int i0 = 0, l = i1 * dims[0]; i0 < dims[0] - 1; i0++, l++, icell++) {
                    float tY = orthoV[0] * coords.get(3 * l) +
                               orthoV[1] * coords.get(3 * l + 1) - rhsY;
                    if (abs(tY) <= cellRadii[icell]) {
                        for (int i = 0; i < offsets.length; i++)
                            splicedCell[i] = l + offsets[i];
                        probe.processCell(splicedCell);
                    }
                }
        }
        sliceField = probe.createOutField();
        return sliceField;
    }

    public IrregularField probeRegularField()
    {
        switch (regularInField.getTrueNSpace()) {
            case 3:
                return probeRegularField3D();
            case 2:
                return probeRegularField2D();
            default:
                return null;
        }
    }

    public IrregularField probeAffineField3D()
    {
        int off0 = dims[0];
        int off1 = dims[0] * dims[1];

        int index = 0;
        ArrayList<FieldPosition> nodes = new ArrayList<>();

        float[][] affine    = regularInField.getAffine();
        float[][] invAffine = regularInField.getInvAffine();

        float[] centerIndex = new float[3];
        float[] dirIndex = new float[3];
        float[] p = new float[3];
        for (int i = 0; i < 3; i++)
            p[i] = center[i] - affine[3][i];
        for (int i = 0; i < 3; i++) {
            centerIndex[i] = dirIndex[i] = 0;
            for (int j = 0; j < 3; j++) {
                dirIndex[i]    += invAffine[j][i] * direction[j];
                centerIndex[i] += invAffine[j][i] * p[j];
            }
        }
        float tLow = -Float.MAX_VALUE;
        float tUp  =  Float.MAX_VALUE;
        for (int i = 0; i < 3; i++) {
            if (dirIndex[i] > 0) {
                float t = -centerIndex[i] / dirIndex[i];
                if (t > tLow)
                    tLow = t;
                t = (dims[i] -centerIndex[i] - 1) / dirIndex[i];
                if (t < tUp)
                    tUp = t;
            }
            else if (dirIndex[i] < 0) {
                float t = -centerIndex[i] / dirIndex[i];
                if (t < tUp)
                    tUp = t;
                t = (dims[i] -centerIndex[i] - 1) / dirIndex[i];
                if (t > tLow)
                    tLow = t;
            }
            else if (centerIndex[i] < 0 || centerIndex[i] > dims[i] - 1)
                return null;
        }

        if (tLow > tUp)
            return null;
        int[] ind = new int[3];
        int[] incr = new int[3];
        float[] r = new float[3];
        for (int axis = 0; axis < 3; axis++) {
            if (dirIndex[axis] == 0)
                continue;
            for (int i = 0; i < dims[axis]; i++) {
                float t = (i - centerIndex[axis]) / dirIndex[axis];
                if (t <= tUp && t >= tLow) {
                    for (int j = 0; j < 3; j++)
                        if (j == axis){
                            ind[j] = i;
                            r[j]   = 0;
                            incr[j] = 0;
                        }
                        else {
                            float f = centerIndex[j] + t * dirIndex[j];
                            ind[j] = (int) f;
                            r[j]   = f - ind[j];
                            incr[j] = 1;
                        }
                    int n = (ind[2] * dims[1] + ind[1]) * dims[0] + ind[0];
                    switch (axis) {
                    case 0:
                        nodes.add(new FieldPosition (index, t,
                                                     new int[] {n,
                                                                n + incr[1] * off0,
                                                                n                  + incr[2] * off1,
                                                                n + incr[1] * off0 + incr[2] * off1},
                                                     new float[] {(1 - r[1]) * (1 - r[2]),
                                                                        r[1] * (1 - r[2]),
                                                                  (1 - r[1]) *      r[2],
                                                                       r[1]  *      r[2]}));
                        break;
                    case 1:
                        nodes.add(new FieldPosition(index, t,
                                                    new int[] {n,
                                                               n + incr[0],
                                                               n           + incr[2] * off1,
                                                               n + incr[0] + incr[2] * off1},
                                                    new float[] {(1 - r[0]) * (1 - r[2]),
                                                                      r[0]  * (1 - r[2]),
                                                                 (1 - r[0]) *      r[2],
                                                                      r[0]  *      r[2]}));
                        break;
                    case 2:
                        nodes.add(new FieldPosition(index, t,
                                                    new int[] {n,
                                                               n + incr[0],
                                                               n           + incr[1] * off0,
                                                               n + incr[0] + incr[1] * off0},
                                                    new float[] {(1 - r[0]) * (1 - r[1]),
                                                                      r[0]  * (1 - r[1]),
                                                                 (1 - r[0]) *      r[1],
                                                                      r[0] *       r[1]}));
                        break;
                    }
                    index += 1;
                }
            }
        }
        return createOutField(nodes);
    }

    public IrregularField probeAffineField2D()
    {
        int off0 = dims[0];

        int index = 0;
        ArrayList<FieldPosition> nodes = new ArrayList<>();

        float[][] affine    = regularInField.getAffine();
        float[][] invAffine = regularInField.getInvAffine();

        float[] centerIndex = new float[2];
        float[] dirIndex = new float[2];
        float[] p = new float[2];
        for (int i = 0; i < 2; i++)
            p[i] = center[i] - affine[3][i];
        for (int i = 0; i < 2; i++) {
            centerIndex[i] = dirIndex[i] = 0;
            for (int j = 0; j < 2; j++) {
                dirIndex[i]    += invAffine[j][i] * direction[j];
                centerIndex[i] += invAffine[j][i] * p[j];
            }
        }
        float tLow = -Float.MAX_VALUE;
        float tUp  =  Float.MAX_VALUE;
        for (int i = 0; i < 2; i++) {
            if (dirIndex[i] > 0) {
                float t = -centerIndex[i] / dirIndex[i];
                if (t > tLow)
                    tLow = t;
                t = (dims[i] -centerIndex[i] - 1) / dirIndex[i];
                if (t < tUp)
                    tUp = t;
            }
            else if (dirIndex[i] < 0) {
                float t = -centerIndex[i] / dirIndex[i];
                if (t < tUp)
                    tUp = t;
                t = (dims[i] -centerIndex[i] - 1) / dirIndex[i];
                if (t > tLow)
                    tLow = t;
            }
            else if (centerIndex[i] < 0 || centerIndex[i] > dims[i] - 1)
                return null;
        }

        if (tLow > tUp)
            return null;
        int[] ind = new int[2];
        int[] incr = new int[2];
        float[] r = new float[2];
        for (int axis = 0; axis < 2; axis++) {
            if (dirIndex[axis] == 0)
                continue;
            for (int i = 0; i < dims[axis]; i++) {
                float t = (i - centerIndex[axis]) / dirIndex[axis];
                if (t <= tUp && t >= tLow) {
                    for (int j = 0; j < trueNSpace; j++)
                        if (j == axis){
                            ind[j] = i;
                            r[j]   = 0;
                            incr[j] = 0;
                        }
                        else {
                            float f = centerIndex[j] + t * dirIndex[j];
                            ind[j] = (int) f;
                            r[j]   = f - ind[j];
                            incr[j] = 1;
                        }
                    int n = ind[1] * dims[0] + ind[0];
                    if (axis == 0)
                        nodes.add(new FieldPosition(index, t,
                                                    new   int[] {n,        n + incr[1] * off0},
                                                    new float[] {1 - r[1], r[1]}));
                    else
                        nodes.add(new FieldPosition(index, t,
                                                    new   int[] {n,        n + incr[0]},
                                                    new float[] {1 - r[0], r[0]}));
                    index += 1;
                }
            }
        }

        return createOutField(nodes);
    }

    private IrregularField createOutField(ArrayList<FieldPosition> nodes)
    {
        if (nodes.isEmpty())
            return null;
        int nOutNodes;
        IrregularField outField;
        Collections.sort(nodes);
        nOutNodes = nodes.size();
        outField = new IrregularField(nOutNodes);
        float[] outCrds = new float[3 * nOutNodes];
        Arrays.fill(outCrds, 0);
        float[] outT = new float[nOutNodes];
        FieldPosition[] nodePositions = new FieldPosition[nOutNodes];
        for (int i = 0; i < nOutNodes; i++) {
            FieldPosition intersection = nodePositions[i] = nodes.get(i);
            outT[i] = intersection.v;
            for (int j = 0; j < 3; j++)
                outCrds[3 * i + j] = intersection.v * direction[j] + center[j];
        }
        outField.setCoords(new FloatLargeArray(outCrds), 0);
        int[] segNodes = new int[2 * (nOutNodes - 1)];
        for (int i = 0; i < nOutNodes - 1; i++) {
            segNodes[2 * i]     = i;
            segNodes[2 * i + 1] = i + 1;
        }
        CellSet outCS = new CellSet("out");
        outCS.addCells(new CellArray(CellType.SEGMENT, segNodes, null, null));
        outCS.generateDisplayData(outField.getCoords(0));
        outField.addCellSet(outCS);
        for (DataArray inDA : inField.getComponents())
            if (inDA.isNumeric())
               outField.addComponent(FieldPosition.interpolate(nodePositions, inDA));
        outField.addComponent(DataArray.create(outT, 1, LINE_SLICE_COORDS));
        if (inField instanceof RegularField)
            outField.addComponent(
                    DataArray.create(
                            FieldPosition.interpolateToIndices(nodePositions, dims),
                            dims.length, INDEX_COORDS));
        sliceField = outField;
        return outField;

    }

    public IrregularField probeAffineField()
    {
        switch (regularInField.getTrueNSpace()) {
            case 3:
                return probeAffineField3D();
            case 2:
                return probeAffineField2D();
            default:
                return null;
        }
    }

    public IrregularField probeIrregularField()
    {
        float rhsV = 0, rhsU = 0;
        if (trueNSpace == 3)
            rhsU = orthoU[0] * center[0] + orthoU[1] * center[1] + orthoU[2] * center[2];
        rhsV = orthoV[0] * center[0] + orthoV[1] * center[1] + orthoV[2] * center[2];
        CellSetGeometryUtilities.addGeometryDataToCellSets(irregularInField.getCellSets(), irregularInField.getCurrentCoords());
        FieldLineProbe probe = new FieldLineProbe(irregularInField, center, direction);

        for (int nSet = 0; nSet < irregularInField.getNCellSets(); nSet++) {
            CellSet trCS = irregularInField.getCellSet(nSet);
            probe.initCellSetSplit(trCS);
            for (int iCellArray = 0; iCellArray < trCS.getCellArrays().length; iCellArray++) {
                if (trCS.getCellArray(CellType.getType(iCellArray)) == null)
                    continue;

                CellArray ca = trCS.getCellArray(CellType.getType(iCellArray));
                boolean isTriangulated = ca.isSimplicesArray();

                probe.initCellArraySplit(ca);
                int nCellNodes = ca.getNCellNodes();
                cellCenters = ca.getCellCenters();
                cellRadii = ca.getCellRadii();
                if (cellCenters.length != 3 * ca.getNCellNodes() || cellRadii.length != nCellNodes) {
                    ca.computeCellGeometryData(irregularInField.getCoords(0), true);
                    cellCenters = ca.getCellCenters();
                    cellRadii = ca.getCellRadii();
                }
                int[] nodes = ca.getNodes();
                int[] cellNodes = new int[ca.getNCellNodes()];
                switch (trueNSpace) {
                case 3:
                    for (int iCell = 0; iCell < ca.getNCells(); iCell++) {
                        float tX = orthoU[0] * cellCenters[3 * iCell] +
                                   orthoU[1] * cellCenters[3 * iCell + 1] +
                                   orthoU[2] * cellCenters[3 * iCell + 2] - rhsU;
                        float tY = orthoV[0] * cellCenters[3 * iCell] +
                                   orthoV[1] * cellCenters[3 * iCell + 1] +
                                   orthoV[2] * cellCenters[3 * iCell + 2] - rhsV;
                        if (abs(tX) <= cellRadii[iCell] && abs(tY) <= cellRadii[iCell]) {
                            for (int l = 0; l < nCellNodes; l++) {
                                cellNodes[l] = nodes[nCellNodes * iCell + l];
                            }
                            if (isTriangulated)
                                probe.processSimplex(cellNodes);
                            else
                                probe.processCell(cellNodes, ca.getType());
                        }
                    }
                    break;
                case 2:
                    for (int iCell = 0; iCell < ca.getNCells(); iCell++) {
                        float tY = orthoV[0] * cellCenters[3 * iCell] +
                                   orthoV[1] * cellCenters[3 * iCell + 1] +
                                   orthoV[2] * cellCenters[3 * iCell + 2] - rhsV;
                        if (abs(tY) <= cellRadii[iCell]) {
                            for (int l = 0; l < nCellNodes; l++) {
                                cellNodes[l] = nodes[nCellNodes * iCell + l];
                            }
                            if (isTriangulated)
                                probe.processTriangle2D(cellNodes);
                            else
                                probe.processCell(cellNodes);
                        }
                    }
                    break;
                }
            }
        }
        sliceField = probe.createOutField();
        return sliceField;
    }


    public IrregularField probeField()
    {
        sliceField = (inField instanceof IrregularField) ? probeIrregularField() :
                     ((inField.hasCoords()) ? probeRegularField(): probeAffineField());
        return sliceField;
    }

    protected IrregularField clip(IrregularField sliceField)
    {
        if (sliceField == null || glyphGeometryParams == null)
            return null;
        float[] dir = trueNSpace == 2 ? glyphGeometryParams.getU() : glyphGeometryParams.getW();
        float[] range = trueNSpace == 2 ? glyphGeometryParams.getuRange() : glyphGeometryParams.getwRange();
        IrregularField tmp = clipOneDir(sliceField, center, dir, range[1], false, dir);
        return               clipOneDir(tmp,        center, dir, range[0], true,  dir);
    }

    protected final void updateSlice()
    {
        glyphGeometryParams = glyphParams.getGeometryParams();
        probeField();
        sliceAreaField = clip(sliceField);
        fireStateChanged(glyph.isAdjusting());
    }


    @Override
    public void setInData(Field inField, DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
        this.inField = inField;
        trueNSpace = inField.getTrueNSpace();

        coords = null;
        if (inField instanceof RegularField) {
            regularInField = (RegularField)inField;
            dims = regularInField.getDims();
            regularInFieldHasCoords = regularInField.hasCoords();
            if (regularInFieldHasCoords) {
                coords = inField.getCoords(0);
                computeCellGeometryData();
            }
            irregularInField = null;
        }
        else {
            irregularInField = (IrregularField)inField;
            coords = inField.getCoords(0);
            regularInField = null;
        }
        glyph.setField(inField);
        if (trueNSpace == 2) {
            glyph.setType(PLANAR_LINE);
            glyph.setField(inField);
            direction = glyphParams.getU();
            glyph.getGlyph().setName("planar_line");
            glyph.getParams().setAxis(0);
        }
        else {
            glyph.setType(LINE);
            glyph.setField(inField);
            direction = glyphParams.getW();
            glyph.getGlyph().setName("line");
            glyph.getParams().setAxis(2);
        }
        sliceField = probeField();
        if (sliceField != null && mapParams != null)
            mapParams.setInData(sliceField, (DataContainer)null);
    }

    @Override
    public IrregularField getSliceField() {
        return sliceField;
    }

    @Override
    public IrregularField getSliceAreaField() {
        if (sliceField == null)
            return null;
        if (sliceAreaField == null)
            sliceAreaField = clip(sliceField);
        return sliceAreaField;
    }

    public void setMapParams(DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
    }

    public OpenBranchGroup getCurrentProbeGeometry()
    {
        return probeParentGroup;
    }

    public float[] getPlaneCenter()
    {
        return center;
    }

    public void updateCurrentProbeGeometry()
    {
        if (sliceAreaField == null)
            return;
        int nOutNodes = (int)sliceAreaField.getNNodes();
        int[] verts = sliceAreaField.getCellSet(0).getCellArray(CellType.SEGMENT).getNodes();
        if (probeGroup != null) {
            probeGroup.detach();
            probeGroup.removeAllChildren();
            probeLines = new IndexedLineArray(nOutNodes,
                                              GeometryArray.USE_COORD_INDEX_ONLY |
                                              GeometryArray.COORDINATES |
                                              GeometryArray.COLOR_4,
                                              verts.length);
            probeLines.setCoordinates(0, sliceAreaField.getCurrentCoords().getData());
            byte[] colors = ColorMapper.map(sliceAreaField, mapParams, null);
            for (int i = 0; i < colors.length; i += 4)
                for (int j = 0; j < 3; j++)
                    colors[i + j] = (byte)(0xff & (int)(.8 * (colors[i + j] & 0xff)));
            probeLines.setColors(0, colors);
            probeLines.setCoordinateIndices(0, verts);
            probeShape = new OpenShape3D();
            probeShape.setName("probeShape");
            probeShape.addGeometry(probeLines);
            OpenAppearance app = new OpenAppearance();
            app.setLineAttributes(new LineAttributes(4, PATTERN_SOLID, true));
            probeShape.setAppearance(app);
            probeGroup.addChild(probeShape);
            probeParentGroup.addChild(probeGroup);
        }
    }



    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return glyph;
    }

    public InteractiveGlyph getGlyph() {
        return glyph;
    }

    @Override
    public InteractiveGlyphGUI getGlyphGUI()
    {
        return glyphGUI;
    }

    @Override
    public float getRZ()
    {
        return .5f * (glyphGeometryParams.getwRange()[1] - glyphGeometryParams.getwRange()[0]);
    }

    @Override
    public void hide()
    {
        glyph.hide();
    }

    @Override
    public void show()
    {
         updateCurrentProbeGeometry();
         glyph.show();
    }
}
