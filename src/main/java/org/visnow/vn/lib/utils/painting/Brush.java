/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.painting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import org.visnow.vn.lib.utils.ImageUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Brush extends PaintingDevice
{

    protected BufferedImage outImg;

    public static final int BRUSH_ROUND = 1;
    public static final int BRUSH_SQUARE = 2;

    private int type = BRUSH_ROUND;
    private int size = 9;

    public Brush()
    {
        ui = new BrushUI();
    }

    public Brush(int type, int size)
    {
        ui = new BrushUI();
        this.type = type;
        this.size = size - 1;
    }

    public Brush(int type, int size, Color color, Color bgTranspColor, Color cursorColor)
    {
        ui = new BrushUI();
        this.type = type;
        this.size = size - 1;
        this.color = color;
        this.cursorColor = color;
        this.bgTransparencyColor = bgTranspColor;
    }

    @Override
    public BufferedImage onMouseClicked(int x, int y, int button, int nClicks, BufferedImage cursorLayer)
    {
        return null;
    }

    @Override
    public void onMousePressed(int x, int y, int button, BufferedImage cursorLayer)
    {
        outImg = new BufferedImage(cursorLayer.getWidth(), cursorLayer.getHeight(), BufferedImage.TYPE_INT_ARGB);
        paintBackground(outImg);
        if (button == MouseEvent.BUTTON1) {
            paintOn(x, y, outImg, false);
            paintOn(x, y, cursorLayer, false);
        } else if (button == MouseEvent.BUTTON3) {
            paintOn(x, y, outImg, true);
            paintOn(x, y, cursorLayer, true);
        }
    }

    @Override
    public BufferedImage onMouseReleased(int x, int y, int button, BufferedImage cursorLayer)
    {
        paintCursorOn(x, y, cursorLayer);
        return outImg;
    }

    @Override
    public void onMouseDragged(int x, int y, int button, BufferedImage cursorLayer)
    {
        if (button == MouseEvent.BUTTON1) {
            paintOn(x, y, outImg, false);
            paintOn(x, y, cursorLayer, false);
        } else if (button == MouseEvent.BUTTON3) {
            paintOn(x, y, outImg, true);
            paintOn(x, y, cursorLayer, true);
        }
    }

    @Override
    public void onMouseMoved(int x, int y, BufferedImage cursorLayer)
    {
        paintCursorOn(x, y, cursorLayer);
    }

    @Override
    protected void paintOn(int x, int y, BufferedImage layer, boolean inverse)
    {
        Graphics2D g = (Graphics2D) layer.getGraphics();
        Color oldColor = g.getColor();

        int relx, rely;
        relx = x - (int) Math.floor((float) size / 2.0f);
        rely = y - (int) Math.floor((float) size / 2.0f);

        if (inverse) {
            g.setColor(Color.BLACK);
        } else {
            g.setColor(color);
        }
        switch (type) {
            case BRUSH_ROUND:
                g.fillOval(relx, rely, size, size);
                g.drawOval(relx, rely, size, size);
                break;
            case BRUSH_SQUARE:
                g.fillRect(relx, rely, size, size);
                g.drawRect(relx, rely, size, size);
                break;
        }
        g.setColor(oldColor);
        ImageUtils.makeTransparent(layer, getBgTransparencyColor());
    }

    @Override
    protected void paintCursorOn(int x, int y, BufferedImage layer)
    {
        Graphics2D g = (Graphics2D) layer.getGraphics();
        Color oldColor = g.getColor();

        g.setColor(getBgTransparencyColor());
        g.fillRect(0, 0, layer.getWidth(), layer.getHeight());

        int relx, rely;
        relx = x - (int) Math.floor((float) size / 2.0f);
        rely = y - (int) Math.floor((float) size / 2.0f);

        g.setColor(cursorColor);
        switch (type) {
            case BRUSH_ROUND:
                g.drawOval(relx, rely, size, size);
                break;
            case BRUSH_SQUARE:
                g.drawRect(relx, rely, size, size);
                break;
        }
        g.setColor(oldColor);
        ImageUtils.makeTransparent(layer, getBgTransparencyColor());
    }

    public int getSize()
    {
        return size + 1;
    }

    public void setSize(int size)
    {
        this.size = size - 1;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

}
