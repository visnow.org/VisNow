/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.integration;

/**
 *
 * @author babor
 */
public class GeometricAreas
{

    static double parallelogramArea(double[] coords)
    {
        double det = (coords[2] - coords[0]) * (coords[5] - coords[1]) - (coords[3] - coords[1]) * (coords[4] - coords[0]);
        return (det >= 0 ? det : (-det));
    }

    static double convexQuadrilateralArea(double[] coords)
    {
        double det = 0.5 * ((coords[6] - coords[0]) * (coords[5] - coords[3]) - (coords[7] - coords[1]) * (coords[4] - coords[2]));
        return (det >= 0 ? det : (-det));
    }

}
