/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.expressions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.BinaryOperator;
import java.util.function.BiPredicate;
import org.apache.commons.math3.util.FastMath;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.DataArrayArithmetics;
import org.visnow.jscic.utils.DataArrayStatistics;
import org.visnow.jscic.utils.UnitUtils;
import org.visnow.vn.lib.basic.utilities.Clipboard.Clipboard;
import org.visnow.vn.lib.utils.MergeTimeSeries;
import static org.visnow.vn.lib.utils.expressions.Associativity.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 * based on:
 * http://www.technical-recipes.com/2011/a-mathematical-expression-parser-in-java-and-cpp
 * modified by:
 * Krzysztof Nowinski (know@icm.edu.pl, knowpl@gmail.com), University of Warsaw, ICM
 *
 */

public enum Operator {
//        symbol     label             precedence arguments       associativity tooltip

    SUM   ("+",      "+",                       3,        2,         LEFT_ASSOC,    "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) return DataArrayArithmetics.addD(args[1], args[0], ignoreUnits);
                    else                 return DataArrayArithmetics.addF(args[1], args[0], ignoreUnits);
                }
            },
        //</editor-fold>
    DIFF  ("-",      "-",                       3,        2,         LEFT_ASSOC,    "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) return DataArrayArithmetics.diffD(args[1], args[0], ignoreUnits);
                    else                 return DataArrayArithmetics.diffF(args[1], args[0], ignoreUnits);
                }
           },
        //</editor-fold>
    MULT  ("*",      "*",                       5,        2,         LEFT_ASSOC,    "<html>scalar*scalar, scalar*vector or componentwise vector*vector multiplication")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (args[0].getVectorLength() > 1 && args[1].getVectorLength() > 1 && args[0].getVectorLength() != args[1].getVectorLength())
                        throw new IllegalArgumentException("componentwise multiplication requires arguments of equal vector length");
                    else if (args[0].getVectorLength() == 1 || args[1].getVectorLength() == 1) {
                        if (doublePrecision) return DataArrayArithmetics.multD(args[1], args[0], ignoreUnits);
                        else                 return DataArrayArithmetics.multF(args[1], args[0], ignoreUnits);
                    }
                    return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, true,
                                                 (in)-> {return in[0] * in[1];},
                                                 (in)-> {return in[0] * in[1];});
                }
            },
        //</editor-fold>
    DIV   ("/",      "/",                       5,        2,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) return DataArrayArithmetics.divD(args[1], args[0], ignoreUnits);
                    else                 return DataArrayArithmetics.divF(args[1], args[0], ignoreUnits);
                }
            },
        //</editor-fold>
    POV   ("^",      "^",                      10,        2,        RIGHT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (args[0].isConstant() && args[0].getType() != DataArrayType.FIELD_DATA_COMPLEX && (ignoreUnits || args[0].isUnitless())) {
                        if (doublePrecision) return DataArrayArithmetics.powD(args[1], args[0].getDoubleElement(0)[0], ignoreUnits);
                        else                 return DataArrayArithmetics.powF(args[1], args[0].getFloatElement(0)[0], ignoreUnits);
                    } else {
                        if (doublePrecision) return DataArrayArithmetics.powD(args[1], args[0], ignoreUnits);
                        else                 return DataArrayArithmetics.powF(args[1], args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    NEG   ("~",      "-",                       8,        1,         LEFT_ASSOC,   "unary minus")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.negD(args[0]);
                    } else {
                        return DataArrayArithmetics.negF(args[0]);
                    }
                }
           },
        //</editor-fold>
    SQRT  ("sqrt",   "<html>&radic</html>",    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.sqrtD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.sqrtF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    LOG   ("log",    "log",                    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.logD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.logF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    LOG10 ("log10",  "<html>log<sub>10",       15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.log10D(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.log10F(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    EXP   ("exp",    "exp",                    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.expD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.expF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    ABS   ("abs",    "| |",                    15,        1,         LEFT_ASSOC,   "<html>absolute value for scalar components<p>norms for vector components")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.absD(args[0]);
                    } else {
                        return DataArrayArithmetics.absF(args[0]);
                    }
                }
            },
        //</editor-fold>
    SIN   ("sin",    "sin",                    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.sinD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.sinF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    COS   ("cos",    "cos",                    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.cosD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.cosF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    TAN   ("tan",    "tan",                    15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.tanD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.tanF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    ASIN  ("asin",   "asin",                   15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.asinD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.asinF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    ACOS  ("acos",   "acos",                   15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.acosD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.acosF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    ATAN  ("atan",   "atan",                   15,        1,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (doublePrecision) {
                        return DataArrayArithmetics.atanD(args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.atanF(args[0], ignoreUnits);
                    }
                }
            },
        //</editor-fold>
    ATAN2 ("atan2",  "atan2",                  15,        2,         LEFT_ASSOC,   "")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, true, (in)-> (float)Math.atan2(in[0], in[1]), (in)-> Math.atan2(in[0], in[1]));
                }
            },
        //</editor-fold>
    MIN   ("min",    "min",                    15, Integer.MAX_VALUE,LEFT_ASSOC,   "<html>arbitrary number of arguments<p>only scalar, real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, false,
                                                  (in)-> {float y  = Float.MAX_VALUE;  for (float  x : in) y = x < y ? x : y;  return y;},
                                                  (in)-> {double y = Double.MAX_VALUE; for (double x : in) y = x < y ? x : y;  return y;});
                }
            },
        //</editor-fold>
    MAX   ("max",    "max",                    15, Integer.MAX_VALUE,LEFT_ASSOC,   "<html>arbitrary number of arguments<p>only scalar, real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, false,
                                                  in -> {float y  = -Float.MAX_VALUE;  for (float  x : in) y = x > y ? x : y;  return y;},
                                                  in -> {double y = -Double.MAX_VALUE; for (double x : in) y = x > y ? x : y;  return y;});
                }
            },
        //</editor-fold>
    SIG   ("sgn",    "sgn",                    15,        1,         LEFT_ASSOC,   "only real numeric arguments")
        //<editor-fold defaultstate="collapsed">
            {
            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                    if (doublePrecision)
                        return DataArrayArithmetics.signumD(args[0], ignoreUnits);
                    else
                        return DataArrayArithmetics.signumF(args[0], ignoreUnits);
                }
            },
        //</editor-fold>
    LT    ("<",      "<",                       2,        2,         LEFT_ASSOC,   "only real numeric arguments")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateComparator(length, doublePrecision, ignoreUnits, args, (x,y) -> x < y, (x,y) -> x < y);
                }
            },
        //</editor-fold>
    GT    (">",      ">",                       2,        2,         LEFT_ASSOC,   "only real numeric arguments")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateComparator(length, doublePrecision, ignoreUnits, args, (x,y) -> x > y, (x,y) -> x > y);
                }
            },
        //</editor-fold>
    LE    ("<=",     "<html>&le</html>",        2,        2,         LEFT_ASSOC,   "only real numeric arguments")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateComparator(length, doublePrecision, ignoreUnits, args, (x,y) -> x <= y, (x,y) -> x <= y);
                }
            },
        //</editor-fold>
    GE    (">=",     "<html>&ge</html>",        2,        2,         LEFT_ASSOC,   "only real numeric arguments")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateComparator(length, doublePrecision, ignoreUnits, args, (x,y) -> x >= y, (x,y) -> x >= y);
                }
            },
        //</editor-fold>
    EQ    ("==",     "==",                      2,        2,         LEFT_ASSOC,   "strict comparison of all elements")
        //<editor-fold defaultstate="collapsed">
             {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateEquality(length, doublePrecision, ignoreUnits, args, (x,y) -> Float.compare(x, y) == 0,
                                                                                        (x,y) -> Double.compare(x, y) == 0,
                                                                                        (x,y) -> Float.compare(x[0], y[0]) == 0 && Float.compare(x[1], y[1]) == 0);
                }
            },
        //</editor-fold>
    NEQ   ("!=",     "<html>&ne</html>",        2,        2,         LEFT_ASSOC,   "strict comparison of all elements")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateEquality(length, doublePrecision, ignoreUnits, args, (x,y) -> Float.compare(x, y) != 0,
                                                                                        (x,y) -> Double.compare(x, y) != 0,
                                                                                        (x,y) -> Float.compare(x[0], y[0]) != 0 && Float.compare(x[1], y[1]) != 0);
                }
            },
        //</editor-fold>
    AVG   ("avg",    "avg",                    15, Integer.MAX_VALUE,LEFT_ASSOC,   "<html>if one argument, generates a constant component<p>" +
                                                                                   "averaging data for each timestep<p>" +
                                                                                   "else averages the arguments pointwise</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (args.length > 1)
                        return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, false,
                                                     (in)-> {float y  = 0; for (float  x : in) y += x;  return y / in.length;},
                                                     (in)-> {double y = 0; for (double x : in) y += x;  return y / in.length;});
                    double[][] avg = DataArrayStatistics.avg(args[0]);
                    ArrayList<Float> timeSeries = (ArrayList<Float>) args[0].getTimeData().getTimesAsList();
                    int size = timeSeries.size();
                    long len = args[0].getNElements();
                    int veclen = args[0].getVectorLength();
                    if (doublePrecision) {
                        ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                        if (veclen == 1) {
                            for (int i = 0; i < size; i++) {
                                timeValues.add(i, new DoubleLargeArray(len, avg[i][0], true));
                            }
                        } else {
                            for (int i = 0; i < size; i++) {
                                DoubleLargeArray la = new DoubleLargeArray(len * veclen, false);
                                for (int j = 0; j < len; j++) {
                                    for (int v = 0; v < veclen; v++) {
                                        la.setDouble(j * veclen + v, avg[i][v]);
                                    }
                                }
                                timeValues.add(i, la);
                            }
                        }
                        return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                    } else {
                        ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                        if (veclen == 1) {
                            for (int i = 0; i < size; i++) {
                                timeValues.add(i, new FloatLargeArray(len, (float) avg[i][0], true));
                            }
                        } else {
                            for (int i = 0; i < size; i++) {
                                FloatLargeArray la = new FloatLargeArray(len * veclen, false);
                                for (int j = 0; j < len; j++) {
                                    for (int v = 0; v < veclen; v++) {
                                        la.setFloat(j * veclen + v, (float) avg[i][v]);
                                    }
                                }
                                timeValues.add(i, la);
                            }
                        }
                        return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                    }
                }
            },
            //</editor-fold>
    STDDEV("stdev",  "<html>&sigma</html>",    15, Integer.MAX_VALUE,LEFT_ASSOC,   "<html>if one argument, generates a constant component<p>" +
                                                                                   "computing standard deviation of data for each timestep<p>" +
                                                                                   "else computes standard deviation of the arguments pointwise</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    if (args.length > 1)
                        return evaluateVarArgFunction(args, doublePrecision, ignoreUnits, false,
                                                     (in)-> {float y = 0; for (float  x : in) y += x;  y /= in.length;
                                                             float s = 0; for (float  x : in) s += (x - y) * (x - y);
                                                             return (float)Math.sqrt(s / (in.length - 1));},
                                                     (in)-> {double y = 0; for (double x : in) y += x;  y /= in.length;
                                                             double s = 0; for (double  x : in) s += (x - y) * (x - y);
                                                             return Math.sqrt(s / (in.length - 1));});
                    double[][] std = DataArrayStatistics.std(args[0]);
                    ArrayList<Float> timeSeries = (ArrayList<Float>) args[0].getTimeData().getTimesAsList();
                    int size = timeSeries.size();
                    long len = args[0].getNElements();
                    int veclen = args[0].getVectorLength();
                    if (doublePrecision) {
                        ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                        if (veclen == 1) {
                            for (int i = 0; i < size; i++) {
                                timeValues.add(i, new DoubleLargeArray(len, std[i][0], true));
                            }
                        } else {
                            for (int i = 0; i < size; i++) {
                                DoubleLargeArray la = new DoubleLargeArray(len * veclen, false);
                                for (int j = 0; j < len; j++) {
                                    for (int v = 0; v < veclen; v++) {
                                        la.setDouble(j * veclen + v, std[i][v]);
                                    }
                                }
                                timeValues.add(i, la);
                            }
                        }
                        return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                    } else {
                        ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                        if (veclen == 1) {
                            for (int i = 0; i < size; i++) {
                                timeValues.add(i, new FloatLargeArray(len, (float) std[i][0], true));
                            }
                        } else {
                            for (int i = 0; i < size; i++) {
                                FloatLargeArray la = new FloatLargeArray(len * veclen, false);
                                for (int j = 0; j < len; j++) {
                                    for (int v = 0; v < veclen; v++) {
                                        la.setFloat(j * veclen + v, (float) std[i][v]);
                                    }
                                }
                                timeValues.add(i, la);
                            }
                        }
                        return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                    }
                }
            },
        //</editor-fold>
    RAND  ("rand",   "rand",                   15,        0,         LEFT_ASSOC,   "<html>no argument <p>fills data array with random floats from [0, 1] range</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    LargeArray da;
                    if (doublePrecision) {
                        da = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, length);
                    } else {
                        da = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, length);
                    }
                    return DataArray.create(da, 1, "result");
                }

            },
        //</editor-fold>
    VECT  ("vect",   "vect",                   15, Integer.MAX_VALUE,LEFT_ASSOC,   "<html>arbitrary number of arguments<p>only scalar, numeric, not complex argument</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return createVector(args, doublePrecision, ignoreUnits);
                }
            },
        //</editor-fold>
    SCPROD("sc_prod","<html>&middot</html>",    6,        2,         LEFT_ASSOC,   "<html>scalar (dot) product of two vector components, <p>only real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateTwoArgVectorFunction(args, doublePrecision, ignoreUnits, VectorOperationResult.SCALAR,
                                                        (in0, in1)-> { float f = 0;  for (int i = 0; i < in0.length; i++) f += in0[i] * in1[i]; return new float[]{f};},
                                                        (in0, in1)-> { double f = 0; for (int i = 0; i < in0.length; i++) f += in0[i] * in1[i]; return new double[]{f};});
                }
            },
        //</editor-fold>
    VPROD ("v_prod", "x",                       6,        2,         LEFT_ASSOC,   "<html>vector (cross) product of two vector components, <p>only real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
                {
                    return evaluateTwoArgVectorFunction(args, doublePrecision, ignoreUnits, VectorOperationResult.VECTOR,
                                                        (in0, in1)-> { return in0.length == 2 ?
                                                                       new float[]{in0[0] * in1[1] - in0[1] * in1[0]} :
                                                                       new float[]{in0[1] * in1[2] - in0[2] * in1[1],
                                                                                   in0[2] * in1[0] - in0[0] * in1[2],
                                                                                   in0[0] * in1[1] - in0[1] * in1[0]};},
                                                        (in0, in1)-> { return in0.length == 2 ?
                                                                       new double[]{in0[0] * in1[1] - in0[1] * in1[0]} :
                                                                       new double[]{in0[1] * in1[2] - in0[2] * in1[1],
                                                                                    in0[2] * in1[0] - in0[0] * in1[2],
                                                                                    in0[0] * in1[1] - in0[1] * in1[0]};});
                }
            },
        //</editor-fold>
    MAXT   ("tmax",  "<html>max<sub>t</html>", 15,        1,         LEFT_ASSOC,   "<html>for each element computes max of values over time <p>only scalar, real numeric argument</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray [] args) throws Exception
                {
                    if (args[0] == null)
                        throw new Exception("null argument");
                    if (args[0].getVectorLength() > 1)
                        throw new Exception("can not compute maximum of vector fields");
                    return evaluateTimeStatistics(args[0], doublePrecision, ignoreUnits,
                                                 (times, in) -> {float y  = -Float.MAX_VALUE;  for (float  x : in) y = x > y ? x : y;  return y;},
                                                 (times, in) -> {double y = -Double.MAX_VALUE; for (double x : in) y = x > y ? x : y;  return y;});
                }
            },
        //</editor-fold>
    MINT   ("tmin",  "<html>min<sub>t</html>", 15,        1,         LEFT_ASSOC,   "<html>for each element computes min of values over time <p>only scalar, real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray [] args) throws Exception
                {
                    if (args[0] == null)
                        throw new Exception("null argument");
                    if (args[0].getVectorLength() > 1)
                        throw new Exception("can not compute minimum of vector fields");
                    return evaluateTimeStatistics(args[0], doublePrecision, ignoreUnits,
                                                 (times, in) -> {float y  = Float.MAX_VALUE;  for (float  x : in) y = x < y ? x : y;  return y;},
                                                 (times, in) -> {double y = Double.MAX_VALUE; for (double x : in) y = x < y ? x : y;  return y;});
                }
            },
        //</editor-fold>
    INTT   ("intt",  "<html>&int dt</html>",   15,        1,         LEFT_ASSOC,   "<html>for each element computes integral of values over time ><p>only real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray [] args)
                {
                    return evaluateTimeStatistics(args[0], doublePrecision, ignoreUnits,
                                                 (times, in) -> {int n = times.length - 1;
                                                                 float y  = in[0] * (times[1] - times[0]) / 2;
                                                                 for (int i = 1; i < n; i++)
                                                                     y += in[i] * (times[i + 1] - times[i - 1]) / 2;
                                                                 y += in[n] * (times[n] - times[n - 1]) / 2;
                                                                 return y;},
                                                 (times, in) -> {
                                                     int n = times.length - 1;
                                                                 double y  = in[0] * (times[1] - times[0]) / 2;
                                                                 for (int i = 1; i < n; i++)
                                                                     y += in[i] * (times[i + 1] - times[i - 1]) / 2;
                                                                 y += in[n] * (times[n] - times[n - 1]) / 2;
                                                                 return y;});
                }
            },
        //</editor-fold>
    AVGT   ("tavg",  "<html>avg<sub>t</html>", 15,        1,         LEFT_ASSOC,   "<html>for each element computes average of values over time ><p>only real numeric arguments</html>")
        //<editor-fold defaultstate="collapsed">
            {
                @Override
                public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray [] args) throws Exception
                {
                    if (args[0] == null)
                        throw new Exception("null argument");
                    return evaluateTimeStatistics(args[0], doublePrecision, ignoreUnits,
                                                 (times, in) -> {int n = times.length - 1;
                                                                 float y  = in[0] * (times[1] - times[0]) / 2;
                                                                 for (int i = 1; i < n; i++)
                                                                     y += in[i] * (times[i + 1] - times[i - 1]) / 2;
                                                                 y += in[n] * (times[n] - times[n - 1]) / 2;
                                                                 return y / (times[n] - times[0]);},
                                                 (times, in) -> {
                                                                 int n = times.length - 1;
                                                                 double y  = in[0] * (times[1] - times[0]) / 2;
                                                                 for (int i = 1; i < n; i++)
                                                                     y += in[i] * (times[i + 1] - times[i - 1]) / 2;
                                                                 y += in[n] * (times[n] - times[n - 1]) / 2;
                                                                 return y / (times[n] - times[0]);});
                }
            };
        //</editor-fold>

    private enum VectorOperationResult {SCALAR, VECTOR};

    public static final Map<String, Operator> operatorByName = new HashMap<>();
    static
    {
        for (Operator value : Operator.values()) {
            operatorByName.put(value.toString(), value);
        }
    }

    private final String symbol;
    private final String label;
    private final int precedence;
    private final int nArguments;
    private final Associativity associativity;
    private final String tooltip;

    abstract public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args) throws Exception;

    private Operator(String symbol, String label, int precedence, int nArguments, Associativity associativity, String tooltip) {
        this.symbol = symbol;
        this.label = label;
        this.precedence = precedence;
        this.nArguments = nArguments;
        this.associativity = associativity;
        this.tooltip = tooltip;
    }

    @Override
    public String toString()
    {
        return symbol;
    }

    public String getLabel() {
        return label;
    }

    public String getTooltip()
    {
        return tooltip;
    }

    public int getPrecedence()
    {
        return precedence;
    }

    public int getNArguments()
    {
        return nArguments;
    }

    public static int getNArguments(String token)
    {
        if (isOperator(token))
            return getOperator(token).getNArguments();
        return -1;
    }

    public Associativity getAssociativity()
    {
        return associativity;
    }

    public static boolean isAssociative(Operator op, Associativity type)
    {
        return (op.getAssociativity() == type);
    }

    public static int getPrecedence(String s)
    {
        if (isOperator(s))
            return getOperator(s).getPrecedence();
        return -1;
    }

    public static int cmpPrecedence(Operator op1, Operator op2)
    {
        return op1.getPrecedence() - op2.getPrecedence();
    }

    public static boolean isOperator(String token)
    {
        return (getOperator(token) != null);
    }

    public static Operator getOperator(String token)
    {
        return operatorByName.get(token);
    }


    public static boolean isAssociative(String token, Associativity type)
    {
        Operator op = getOperator(token);
        if (op == null)
            throw new IllegalArgumentException("Invalid token: " + token);
        return isAssociative(op, type);
    }

    public static int cmpPrecedence(String token1, String token2)
    {
        Operator op1 = getOperator(token1);
        Operator op2 = getOperator(token2);
        if (op1 == null || op2 == null)
            throw new IllegalArgumentException("Invalid tokens: " + token1 + " " + token2);
        return cmpPrecedence(op1, op2);
    }

//<editor-fold defaultstate="collapsed" desc=" FUNCTION template ">//*
/* To implement new function add to the Operator enum a new item:

     FUNCTION (name, label, precedence, nArguments, associativity, tooltip)
             parameters are:
             name or symbol identifying operator in the program
             label displayed on the operator button
             operator precedence (operators with higher precedence are executed before operators of lower precedence)
             use Integer.MAX_VALUE to indicate variable arguments function like max(x,y,z,...)
             1 means right associative operation like x^y^z evaluated as x^(y^z) 0 means left (usual) associativity
             tooltip will be attached to the operator button
     {
         @Override
         public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args) throws Exception
             obligatory method evaluating operation
             first argument is data size
             second argument switches between float and double precision
             third argument checks if data physical units will be ignored (true) or correctly processed (false)
             array of DataArrays containing arguments
             throws Exception when evaluation is not possible
         {
                 example implementation with lambdas:
            return evaluateVarArgFunction(args, doublePrecision, ignoreUnits,
                                          in -> {float y  = 0; for (float  x : in) y += x;  return y;},
                                          in -> {double y = 0; for (double x : in) y += x;  return y;});
         }

         As shown in the above example implementing multiargument summation
         an universal prototype of multiargument operation is supplied.
         The prototype takes care of checking arguments, merging time lines etc.
         One has thus supply lambdas for single and double precision operations that will be executed at each point
    },
*/

        //</editor-fold>

    private static DataArray evaluateComparator(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args,
                                                BiPredicate<Float, Float>cmpFloat, BiPredicate<Double, Double>cmpDouble)
        //<editor-fold defaultstate="collapsed">
    {
        DataArray da1 = args[0], da2 = args[1];
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type = LargeArrayType.LOGIC;
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();
        long len = da1.getNElements();
        double[] physMappingCoeffs1 = da1.getPhysicalMappingCoefficients();
        double[] physMappingCoeffs2 = da2.getPhysicalMappingCoefficients();

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Cannot apply logic operation to complex arrays");
        } else {
            String outUnit = "1";
            if (!(ignoreUnits || (da1.isUnitless() && da2.isUnitless()))) {
                if (!da1.isUnitless() && !da2.isUnitless()) {
                    if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                        throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                    }
                } else if (da1.isUnitless() && !da2.isUnitless()) {
                    da1.setUnit(da2.getUnit());
                } else if (da2.isUnitless() && !da1.isUnitless()) {
                    da2.setUnit(da1.getUnit());
                }
                if (!(da1.isUnitless() && da2.isUnitless())) {
                    outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                    if (doublePrecision) {
                        da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                        da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                    } else {
                        da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                        da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                    }
                }
            }

            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    if (doublePrecision)
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i * veclen1 + v), a.getDouble(i * veclen1 + v)));
                    else
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i * veclen1 + v), a.getFloat(i * veclen1 + v)));
                } else {
                    long k = len / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (doublePrecision)
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpDouble.test(b.getDouble(k * veclen1 + v), a.getDouble(k * veclen1 + v)));
                                else
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpFloat.test(b.getFloat(k * veclen1 + v), a.getFloat(k * veclen1 + v)));
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i * veclen1 + v), a.getDouble(i * veclen1 + v)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i * veclen1 + v), a.getFloat(i * veclen1 + v)));
                        }
                }
                dataSeries.add(res);
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    if (doublePrecision)
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen2; v++)
                                res.setBoolean(i * veclen2 + v, cmpDouble.test(b.getDouble(i * veclen2 + v), a.getDouble(i)));
                    else
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen2; v++)
                                res.setBoolean(i * veclen2 + v, cmpFloat.test(b.getFloat(i * veclen2 + v), a.getFloat(i)));
                } else {
                    long k = len / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (doublePrecision)
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen2; v++)
                                            res.setBoolean(k * veclen2 + v, cmpDouble.test(b.getDouble(k * veclen2 + v), a.getDouble(k)));
                                else
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen2; v++)
                                            res.setBoolean(k * veclen2 + v, cmpFloat.test(b.getFloat(k * veclen2 + v), a.getFloat(k)));
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen2; v++)
                                    res.setBoolean(i * veclen2 + v, cmpDouble.test(b.getDouble(i * veclen2 + v), a.getDouble(i)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen2; v++)
                                    res.setBoolean(i * veclen2 + v, cmpFloat.test(b.getFloat(i * veclen2 + v), a.getFloat(i)));
                    }
                }
                dataSeries.add(res);
            }

        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    if (doublePrecision)
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i), a.getDouble(i * veclen1 + v)));
                    else
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i), a.getFloat(i * veclen1 + v)));
                } else {
                    long k = len / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (doublePrecision)
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpDouble.test(b.getDouble(k), a.getDouble(k * veclen1 + v)));
                                else
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpFloat.test(b.getFloat(k), a.getFloat(k * veclen1 + v)));
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i), a.getDouble(i * veclen1 + v)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i), a.getFloat(i * veclen1 + v)));
                    }
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
    }
        //</editor-fold>

    private static DataArray evaluateEquality(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args,
                                              BiPredicate<Float, Float>cmpFloat, BiPredicate<Double, Double>cmpDouble,
                                              BiPredicate<float[], float[]>cmpComplex)
        //<editor-fold defaultstate="collapsed">
    {
        DataArray da1 = args[0], da2 = args[1];
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type = LargeArrayType.LOGIC;
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();
        double[] physMappingCoeffs1 = da1.getPhysicalMappingCoefficients();
        double[] physMappingCoeffs2 = da2.getPhysicalMappingCoefficients();

        long len = da1.getNElements();
        String outUnit = "1";
        if (!(ignoreUnits || (da1.isUnitless() && da2.isUnitless()))) {
            if (!da1.isUnitless() && !da2.isUnitless()) {
                if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                    throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                }
            } else if (da1.isUnitless() && !da2.isUnitless()) {
                da1.setUnit(da2.getUnit());
            } else if (da2.isUnitless() && !da1.isUnitless()) {
                da2.setUnit(da1.getUnit());
            }
            if (!(da1.isUnitless() && da2.isUnitless())) {
                outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                if (doublePrecision) {
                    da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                    da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                } else {
                    da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                    da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                }
            }
        }

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
        if (veclen1 == veclen2) {
            if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                for (Float time : timeSeries) {
                    final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                    final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i * veclen1 + v), a.getDouble(i * veclen1 + v)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i * veclen1 + v), a.getFloat(i * veclen1 + v)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (doublePrecision)
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen1; v++)
                                                res.setBoolean(k * veclen1 + v, cmpDouble.test(b.getDouble(k * veclen1 + v), a.getDouble(k * veclen1 + v)));
                                    else
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen1; v++)
                                                res.setBoolean(k * veclen1 + v, cmpFloat.test(b.getFloat(k * veclen1 + v), a.getFloat(k * veclen1 + v)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            if (doublePrecision)
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen1; v++)
                                        res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i * veclen1 + v), a.getDouble(i * veclen1 + v)));
                            else
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen1; v++)
                                        res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i * veclen1 + v), a.getFloat(i * veclen1 + v)));
                        }
                    }
                    dataSeries.add(res);
                }
            } else {
                for (Float time : timeSeries) {
                    final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getPhysicalValue(time, physMappingCoeffs1);
                    final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpComplex.test(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpComplex.test(b.getComplexFloat(k * veclen1 + v), a.getComplexFloat(k * veclen1 + v)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpComplex.test(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                        }
                    }
                    dataSeries.add(res);
                }
            }
        } else if (veclen1 == 1) {
            if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                for (Float time : timeSeries) {
                    final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                    final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen2; v++)
                                    res.setBoolean(i * veclen2 + v, cmpDouble.test(b.getDouble(i * veclen2 + v), a.getDouble(i)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen2; v++)
                                    res.setBoolean(i * veclen2 + v, cmpFloat.test(b.getFloat(i * veclen2 + v), a.getFloat(i)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (doublePrecision)
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen2; v++)
                                                res.setBoolean(k * veclen2 + v, cmpDouble.test(b.getDouble(k * veclen2 + v), a.getDouble(k)));
                                    else
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen2; v++)
                                                res.setBoolean(k * veclen2 + v, cmpFloat.test(b.getFloat(k * veclen2 + v), a.getFloat(k)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            if (doublePrecision)
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen2; v++)
                                        res.setBoolean(i * veclen2 + v, cmpDouble.test(b.getDouble(i * veclen2 + v), a.getDouble(i)));
                            else
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen2; v++)
                                        res.setBoolean(i * veclen2 + v, cmpFloat.test(b.getFloat(i * veclen2 + v), a.getFloat(i)));
                        }
                    }
                    dataSeries.add(res);
                }
            } else {
                for (Float time : timeSeries) {
                    final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getPhysicalValue(time, physMappingCoeffs1);
                    final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen2; v++)
                                res.setBoolean(i * veclen2 + v, cmpComplex.test(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen2; v++)
                                            res.setBoolean(k * veclen2 + v, cmpComplex.test(b.getComplexFloat(k * veclen2 + v), a.getComplexFloat(k)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen2; v++)
                                    res.setBoolean(i * veclen2 + v, cmpComplex.test(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                        }
                    }
                    dataSeries.add(res);
                }
            }

        } else if (veclen2 == 1) {
            if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                for (Float time : timeSeries) {
                    final LargeArray a = td1.getPhysicalValue(time, physMappingCoeffs1);
                    final LargeArray b = td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        if (doublePrecision)
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i), a.getDouble(i * veclen1 + v)));
                        else
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i), a.getFloat(i * veclen1 + v)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (doublePrecision)
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen1; v++)
                                                res.setBoolean(k * veclen1 + v, cmpDouble.test(b.getDouble(k), a.getDouble(k * veclen1 + v)));
                                    else
                                        for (long k = firstIdx; k < lastIdx; k++)
                                            for (int v = 0; v < veclen1; v++)
                                                res.setBoolean(k * veclen1 + v, cmpFloat.test(b.getFloat(k), a.getFloat(k * veclen1 + v)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            if (doublePrecision)
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen1; v++)
                                        res.setBoolean(i * veclen1 + v, cmpDouble.test(b.getDouble(i), a.getDouble(i * veclen1 + v)));
                            else
                                for (long i = 0; i < len; i++)
                                    for (int v = 0; v < veclen1; v++)
                                        res.setBoolean(i * veclen1 + v, cmpFloat.test(b.getFloat(i), a.getFloat(i * veclen1 + v)));
                        }
                    }
                    dataSeries.add(res);
                }
            } else {
                for (Float time : timeSeries) {
                    final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getPhysicalValue(time, physMappingCoeffs1);
                    final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getPhysicalValue(time, physMappingCoeffs2);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < len; i++)
                            for (int v = 0; v < veclen1; v++)
                                res.setBoolean(i * veclen1 + v, cmpComplex.test(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                    } else {
                        long k = len / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++)
                                        for (int v = 0; v < veclen1; v++)
                                            res.setBoolean(k * veclen1 + v, cmpComplex.test(b.getComplexFloat(k), a.getComplexFloat(k * veclen1 + v)));
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < len; i++)
                                for (int v = 0; v < veclen1; v++)
                                    res.setBoolean(i * veclen1 + v, cmpComplex.test(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                        }
                    }
                    dataSeries.add(res);
                }
            }
        } else {
            throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
    }
        //</editor-fold>

    private static DataArray createVector(DataArray[] dataArrays, boolean doublePrecision, boolean ignoreUnits)
        //<editor-fold defaultstate="collapsed">
    {
        if (dataArrays == null || dataArrays.length < 1)
            throw new IllegalArgumentException("no arguments");
        if (dataArrays.length == 1)
            return dataArrays[0];
        int nData = dataArrays.length;
        long nElements = dataArrays[0].getNElements();
        boolean unitless = dataArrays[0].isUnitless();
        String  unit     = dataArrays[0].getUnit();
        String  outUnit  = (!ignoreUnits && !unitless) ? UnitUtils.getDerivedUnit(unit) : null;
        for (DataArray dataArray : dataArrays) {
            if (dataArray == null ||
               !dataArray.isNumeric() || dataArray.getType() == DataArrayType.FIELD_DATA_COMPLEX ||
                dataArray.getNElements() != nElements || dataArray.getVectorLength() != 1)
                throw new IllegalArgumentException("some input is null, non-numeric, complex, not scalar or has wrong number of elements");
            if (!ignoreUnits && (unitless != dataArray.isUnitless() ||
                                !unitless && !UnitUtils.areValidAndCompatibleUnits(unit, dataArray.getUnit())))
                throw new IllegalArgumentException("Incompatible or invalid units: " + unit + " and " + dataArray.getUnit());
        }

        ArrayList<Float> timeSeries = MergeTimeSeries.mergeTimeSteps(dataArrays);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());

        double[][] physMappingCoeffs = new double[nData][];
        LargeArray[] dataVals = new LargeArray[nData];
        for (int i = 0; i < nData; i++)
            physMappingCoeffs[i] = dataArrays[i].getPhysicalMappingCoefficients();

        for (Float time : timeSeries) {
            for (int i = 0; i < nData; i++)
                dataVals[i] = dataArrays[i].getTimeData().getPhysicalValue(time, physMappingCoeffs[i]);
            LargeArray res = LargeArrayUtils.create(doublePrecision ? LargeArrayType.DOUBLE : LargeArrayType.FLOAT, nData * nElements, false);
            int nthreads = (int) FastMath.min(nElements, ConcurrencyUtils.getNumberOfThreads());
            if (doublePrecision) {
                if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nElements; i++) {
                        double[] args = new double[nData];
                        for (int v = 0; v < nData; v++)
                            args[nData - v - 1] = dataVals[v].getDouble(i);
                        LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nElements) / nthreads;
                        final long lastIdx  = ((j + 1) * nElements) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double[] args = new double[nData];
                                    for (int v = 0; v < nData; v++)
                                        args[nData - v - 1] = dataVals[v].getDouble(i);
                                    LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                                }
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nElements; i++) {
                            double[] args = new double[nData];
                            for (int v = 0; v < nData; v++)
                                args[nData - v - 1] = dataVals[v].getDouble(i);
                            LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                        }
                    }
                }
            }
            else {
                if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nElements; i++) {
                        float[] args = new float[nData];
                        for (int v = 0; v < nData; v++)
                            args[nData - v - 1] = dataVals[v].getFloat(i);
                        LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nElements) / nthreads;
                        final long lastIdx  = ((j + 1) * nElements) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    float[] args = new float[nData];
                                    for (int v = 0; v < nData; v++)
                                        args[nData - v - 1] = dataVals[v].getFloat(i);
                                    LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                                }
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nElements; i++) {
                            float[] args = new float[nData];
                            for (int v = 0; v < nData; v++)
                                args[nData - v - 1] = dataVals[v].getFloat(i);
                            LargeArrayUtils.arraycopy(args, 0, res, i * nData, nData);
                        }
                    }
                }
            }
            dataSeries.add(res);
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), nData, "vect", outUnit, null);
    }
        //</editor-fold>

    private static DataArray evaluateVarArgFunction(DataArray[] dataArrays, boolean doublePrecision, boolean ignoreUnits, boolean allowVectorArgs,
                                                    Function<float[],  Float> floatFun, Function<double[], Double>  dblFun)
        //<editor-fold defaultstate="collapsed">
    {
        if (dataArrays == null || dataArrays.length < 1)
            throw new IllegalArgumentException("no arguments");
        if (dataArrays.length == 1)
            return dataArrays[0];
        int nData = dataArrays.length;
        int vLen = dataArrays[0].getVectorLength();
        if (!allowVectorArgs && vLen > 1)
            throw new IllegalArgumentException("only scalar arguments are allowed");
        long nElements = dataArrays[0].getNElements();
        boolean unitless = dataArrays[0].isUnitless();
        String  unit     = dataArrays[0].getUnit();
        String  outUnit  = (!ignoreUnits && !unitless) ? UnitUtils.getDerivedUnit(unit) : null;
        for (DataArray dataArray : dataArrays) {
            if (dataArray == null ||
               !dataArray.isNumeric() || dataArray.getType() == DataArrayType.FIELD_DATA_COMPLEX ||
                dataArray.getNElements() != nElements || dataArray.getVectorLength() != vLen)
                throw new IllegalArgumentException("some input is null, non-numeric, complex, has wrong number of elements or vector lengths");
            if (!ignoreUnits && (unitless != dataArray.isUnitless() ||
                                !unitless && !UnitUtils.areValidAndCompatibleUnits(unit, dataArray.getUnit())))
                throw new IllegalArgumentException("Incompatible or invalid units: " + unit + " and " + dataArray.getUnit());
        }
        long nDataVals = nElements * vLen;
        ArrayList<Float> timeSeries = MergeTimeSeries.mergeTimeSteps(dataArrays);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());

        double[][] physMappingCoeffs = new double[nData][];
        LargeArray[] dataVals = new LargeArray[nData];
        for (int i = 0; i < nData; i++)
            physMappingCoeffs[i] = dataArrays[i].getPhysicalMappingCoefficients();

        for (Float time : timeSeries) {
            for (int i = 0; i < nData; i++)
                dataVals[i] = dataArrays[i].getTimeData().getPhysicalValue(time, physMappingCoeffs[i]);
            LargeArray res = LargeArrayUtils.create(doublePrecision ? LargeArrayType.DOUBLE : LargeArrayType.FLOAT, nDataVals, false);
            int nthreads = (int) FastMath.min(nDataVals, ConcurrencyUtils.getNumberOfThreads());
            if (doublePrecision) {
                if (nthreads < 2 || nDataVals < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nDataVals; i++) {
                        double[] args = new double[nData];
                        for (int v = 0; v < nData; v++)
                            args[nData - v - 1] = dataVals[v].getDouble(i);
                        res.setDouble(i, dblFun.apply(args));
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nDataVals) / nthreads;
                        final long lastIdx  = ((j + 1) * nDataVals) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double[] args = new double[nData];
                                    for (int v = 0; v < nData; v++)
                                        args[nData - v - 1] = dataVals[v].getDouble(i);
                                    res.setDouble(i, dblFun.apply(args));
                                }
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nDataVals; i++) {
                            double[] args = new double[nData];
                            for (int v = 0; v < nData; v++)
                                args[nData - v - 1] = dataVals[v].getDouble(i);
                            res.setDouble(i, dblFun.apply(args));
                        }
                    }
                }
            }
            else {
                if (nthreads < 2 || nDataVals < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nDataVals; i++) {
                        float[] args = new float[nData];
                        for (int v = 0; v < nData; v++)
                            args[nData - v - 1] = dataVals[v].getFloat(i);
                        res.setFloat(i, floatFun.apply(args));
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nDataVals) / nthreads;
                        final long lastIdx  = ((j + 1) * nDataVals) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    float[] args = new float[nData];
                                    for (int v = 0; v < nData; v++)
                                        args[nData - v - 1] = dataVals[v].getFloat(i);
                                    res.setFloat(i, floatFun.apply(args));
                                }
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nDataVals; i++) {
                            float[] args = new float[nData];
                            for (int v = 0; v < nData; v++)
                                args[nData - v - 1] = dataVals[v].getFloat(i);
                            res.setFloat(i, floatFun.apply(args));
                        }
                    }
                }
                dataSeries.add(res);
            }
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), vLen, "res", outUnit, null);
    }
        //</editor-fold>

    private static DataArray evaluateTimeStatistics(DataArray arg, boolean doublePrecision, boolean ignoreUnits,
                                                    BiFunction<float[], float[],  Float> floatFun, BiFunction<float[], double[], Double>  dblFun)
        //<editor-fold defaultstate="collapsed">
    {
        if (!arg.isTimeDependant())
            return arg;
        TimeData timeData = arg.getTimeData();
        int vLen = arg.getVectorLength();
        long nElements = vLen * arg.getNElements();
        int nData = arg.getNFrames();
        int nthreads = (int) FastMath.min(nElements, ConcurrencyUtils.getNumberOfThreads());
        boolean unitless = arg.isUnitless();
        String  unit     = arg.getUnit();
        String  outUnit  = (!ignoreUnits && !unitless) ? UnitUtils.getDerivedUnit(unit) : null;
        float[] timeSeries = timeData.getTimesAsArray();
        ArrayList<LargeArray> data = timeData.getValues();
        if (doublePrecision) {
            DoubleLargeArray res = new DoubleLargeArray(nElements);
            if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                for (long i = 0; i < nElements; i++) {
                    double[] args = new double[nData];
                    for (int v = 0; v < nData; v++)
                        args[v] = data.get(v).getDouble(i);
                    res.setDouble(i, dblFun.apply(timeSeries, args));
                }
            else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j       * nElements) / nthreads;
                    final long lastIdx  = ((j + 1) * nElements) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            for (long i = firstIdx; i < lastIdx; i++) {
                                double[] args = new double[nData];
                                for (int v = 0; v < nData; v++)
                                    args[v] = data.get(v).getDouble(i);
                                res.setDouble(i, dblFun.apply(timeSeries, args));
                            }
                        }
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    for (long i = 0; i < nElements; i++) {
                        double[] args = new double[nData];
                        for (int v = 0; v < nData; v++)
                            args[v] = data.get(v).getDouble(i);
                        res.setDouble(i, dblFun.apply(timeSeries, args));
                    }
                }
            }
            return DataArray.create(res, vLen, "out_data", outUnit, null);
        }
        else {
            FloatLargeArray res = new FloatLargeArray(nElements);
            if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                for (long i = 0; i < nElements; i++) {
                    float[] args = new float[nData];
                    for (int v = 0; v < nData; v++)
                        args[v] = data.get(v).getFloat(i);
                    res.setFloat(i, floatFun.apply(timeSeries, args));
                }
            else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j       * nElements) / nthreads;
                    final long lastIdx  = ((j + 1) * nElements) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            for (long i = firstIdx; i < lastIdx; i++) {
                                float[] args = new float[nData];
                                for (int v = 0; v < nData; v++)
                                    args[v] = data.get(v).getFloat(i);
                                res.setFloat(i, floatFun.apply(timeSeries, args));
                            }
                        }
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    for (long i = 0; i < nElements; i++) {
                        float[] args = new float[nData];
                        for (int v = 0; v < nData; v++)
                            args[v] = data.get(v).getFloat(i);
                        res.setFloat(i, floatFun.apply(timeSeries, args));
                    }
                }
            }
            return DataArray.create(res, vLen, "out_data", outUnit, null);
        }
    }
        //</editor-fold>

    private static DataArray evaluateTwoArgVectorFunction(DataArray[] dataArrays, boolean doublePrecision, boolean ignoreUnits,
                                                          VectorOperationResult resType, BinaryOperator<float[]>  floatFun, BinaryOperator<double[]>  dblFun)
        //<editor-fold defaultstate="collapsed">
    {
        if (dataArrays == null || dataArrays.length != 2)
            throw new IllegalArgumentException("bad number of arguments arguments");
        long nElements = dataArrays[0].getNElements();
        int vlen = dataArrays[0].getVectorLength();
        if (resType == VectorOperationResult.VECTOR && (vlen < 2 || vlen > 3))
            throw new IllegalArgumentException("bad argument vector length");
        int resVlen = resType == VectorOperationResult.SCALAR ? 1 : vlen == 2 ? 1 : 3;
        boolean unitless = dataArrays[0].isUnitless();
        String  unit     = dataArrays[0].getUnit();
        String  outUnit  = (!ignoreUnits && !unitless) ? UnitUtils.getDerivedUnit(unit) : null;
        for (DataArray dataArray : dataArrays) {
            if (dataArray == null ||
               !dataArray.isNumeric() || dataArray.getType() == DataArrayType.FIELD_DATA_COMPLEX ||
                dataArray.getNElements() != nElements || dataArray.getVectorLength() != vlen)
                throw new IllegalArgumentException("some input is null, non-numeric, complex, bad vector length or has wrong number of elements");
        }

        ArrayList<Float> timeSeries = MergeTimeSeries.mergeTimeSteps(dataArrays);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());

        double[][] physMappingCoeffs = new double[2][];
        LargeArray[] dataVals = new LargeArray[2];
        for (int i = 0; i < 2; i++)
            physMappingCoeffs[i] = dataArrays[i].getPhysicalMappingCoefficients();

        for (Float time : timeSeries) {
            for (int i = 0; i < 2; i++)
                dataVals[i] = dataArrays[i].getTimeData().getPhysicalValue(time, physMappingCoeffs[i]);
            LargeArray res = LargeArrayUtils.create(doublePrecision ? LargeArrayType.DOUBLE : LargeArrayType.FLOAT, nElements * resVlen, false);
            int nthreads = (int) FastMath.min(nElements, ConcurrencyUtils.getNumberOfThreads());
            if (doublePrecision) {
                if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nElements; i++) {
                        double[] arg0 = new double[vlen];
                        double[] arg1 = new double[vlen];
                        for (int j = 0; j < vlen; j++) {
                            arg0[j] = dataVals[0].getDouble(vlen * i + j);
                            arg1[j] = dataVals[1].getDouble(vlen * i + j);
                        }
                        double[] r = dblFun.apply(arg0, arg1);
                        for (int j = 0; j < resVlen; j++)
                        res.setDouble(resVlen * i + j, r[j]);
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nElements) / nthreads;
                        final long lastIdx  = ((j + 1) * nElements) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(() -> {
                            for (long i = firstIdx; i < lastIdx; i++) {
                                double[] arg0 = new double[vlen];
                                double[] arg1 = new double[vlen];
                                for (int j1 = 0; j1 < vlen; j1++) {
                                    arg0[j1] = dataVals[0].getDouble(vlen * i + j1);
                                    arg1[j1] = dataVals[1].getDouble(vlen * i + j1);
                                }
                                double[] r = dblFun.apply(arg0, arg1);
                                for (int j2 = 0; j2 < resVlen; j2++)
                                    res.setDouble(resVlen * i + j2, r[j2]);
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nElements; i++) {
                            double[] arg0 = new double[vlen];
                            double[] arg1 = new double[vlen];
                            for (int j = 0; j < vlen; j++) {
                                arg0[j] = dataVals[0].getDouble(vlen * i + j);
                                arg1[j] = dataVals[1].getDouble(vlen * i + j);
                            }
                            double[] r = dblFun.apply(arg0, arg1);
                            for (int j = 0; j < resVlen; j++)
                            res.setDouble(resVlen * i + j, r[j]);
                        }
                    }
                }
            }
            else {
                if (nthreads < 2 || nElements < ConcurrencyUtils.getConcurrentThreshold())
                    for (long i = 0; i < nElements; i++) {
                        float[] arg0 = new float[vlen];
                        float[] arg1 = new float[vlen];
                        for (int j = 0; j < vlen; j++) {
                            arg0[j] = dataVals[0].getFloat(vlen * i + j);
                            arg1[j] = dataVals[1].getFloat(vlen * i + j);
                        }
                        float[] r = floatFun.apply(arg0, arg1);
                        for (int j = 0; j < resVlen; j++)
                        res.setFloat(resVlen * i + j, r[j]);
                    }
                else {
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = (j       * nElements) / nthreads;
                        final long lastIdx  = ((j + 1) * nElements) / nthreads;
                        threads[j] = ConcurrencyUtils.submit(() -> {
                            for (long i = firstIdx; i < lastIdx; i++) {
                                float[] arg0 = new float[vlen];
                                float[] arg1 = new float[vlen];
                                for (int j1 = 0; j1 < vlen; j1++) {
                                    arg0[j1] = dataVals[0].getFloat(vlen * i + j1);
                                    arg1[j1] = dataVals[1].getFloat(vlen * i + j1);
                                }
                                float[] r = floatFun.apply(arg0, arg1);
                                for (int j2 = 0; j2 < resVlen; j2++)
                                    res.setFloat(resVlen * i + j2, r[j2]);
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < nElements; i++) {
                            float[] arg0 = new float[vlen];
                            float[] arg1 = new float[vlen];
                            for (int j = 0; j < vlen; j++) {
                                arg0[j] = dataVals[0].getFloat(vlen * i + j);
                                arg1[j] = dataVals[1].getFloat(vlen * i + j);
                            }
                            float[] r = floatFun.apply(arg0, arg1);
                            for (int j = 0; j < resVlen; j++)
                            res.setFloat(resVlen * i + j, r[j]);
                        }
                    }
                }
            }
            dataSeries.add(res);
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), resVlen,
                                resType.name(), outUnit, null);
    }
        //</editor-fold>

}
