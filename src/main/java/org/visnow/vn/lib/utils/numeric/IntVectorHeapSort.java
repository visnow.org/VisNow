/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric;

import org.visnow.jlargearrays.IntLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class IntVectorHeapSort
{

    private final int[] sortedItems;
    private final int[] indices;
    private final IntLargeArray largeSortedItems;
    private final IntLargeArray largeIndices;
    private final long largeNSortedItems;
    private final int nSortedItems;
    private final int veclen;
    private int n;
    private int left;
    private int right;
    private int largest;
    private long lN;
    private long lLeft;
    private long lRight;
    private long lLargest;

    private IntVectorHeapSort(int[] sortedItems, int[] indices, int veclen)
    {
        this.sortedItems = sortedItems;
        this.veclen = veclen;
        this.indices = indices;
        n = nSortedItems = indices.length;
        largeSortedItems = null;
        largeIndices = null;
        largeNSortedItems = 0;
    }

    private IntVectorHeapSort(IntLargeArray sortedItems, IntLargeArray indices, int veclen)
    {
        this.largeSortedItems = sortedItems;
        this.veclen = veclen;
        this.largeIndices = indices;
        lN = largeNSortedItems = indices.length();
        this.sortedItems = null;
        this.indices = null;
        nSortedItems = 0;
    }

    private void buildHeap(int[] a)
    {
        n = nSortedItems - 1;
        for (int i = n / 2; i >= 0; i--)
            maxHeap(a, i);
    }

    private void builLargeHeap(IntLargeArray a)
    {
        lN = largeNSortedItems - 1;
        for (long i = lN / 2; i >= 0; i--)
            largeMaxHeap(a, i);
    }

    private boolean gt(int i, int j)
    {
        for (int k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++) {
            if (sortedItems[k0] < sortedItems[k1])
                return false;
            else if (sortedItems[k0] > sortedItems[k1])
                return true;
        }
        return false;
    }
    
    private boolean largeGt(long i, long j)
    {
        for (long k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++) {
            if (largeSortedItems.getInt(k0) < largeSortedItems.getInt(k1))
                return false;
            else if (largeSortedItems.getInt(k0) > largeSortedItems.getInt(k1))
                return true;
        }
        return false;
    }

    private void maxHeap(int[] a, int i)
    {
        left = 2 * i;
        right = 2 * i + 1;
        //      if (left <= n && sortedItems[left] > sortedItems[i])
        if (left <= n && gt(left, i))
            largest = left;
        else
            largest = i;

        //      if (right <= n && sortedItems[right] > sortedItems[largest])
        if (right <= n && gt(right, largest))
            largest = right;
        if (largest != i) {
            exchange(i, largest);
            maxHeap(a, largest);
        }
    }
    
    private void largeMaxHeap(IntLargeArray a, long i)
    {
        lLeft = 2 * i;
        lRight = 2 * i + 1;
        //      if (left <= n && sortedItems[left] > sortedItems[i])
        if (lLeft <= lN && largeGt(lLeft, i))
            lLargest = lLeft;
        else
            lLargest = i;

        //      if (right <= n && sortedItems[right] > sortedItems[largest])
        if (lRight <= lN && largeGt(lRight, lLargest))
            lLargest = lRight;
        if (lLargest != i) {
            lExchange(i, lLargest);
            largeMaxHeap(a, lLargest);
        }
    }

    private void exchange(int i, int j)
    {
        int t;
        for (int k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++) {
            t = sortedItems[k0];
            sortedItems[k0] = sortedItems[k1];
            sortedItems[k1] = t;
        }
        t = indices[i];
        indices[i] = indices[j];
        indices[j] = t;
    }
    
    private void lExchange(long i, long j)
    {
        int t;
        for (long k = 0, k0 = veclen * i, k1 = veclen * j; k < veclen; k++, k0++, k1++) {
            t = largeSortedItems.getInt(k0);
            largeSortedItems.setInt(k0, largeSortedItems.getInt(k1));
            largeSortedItems.setInt(k1, t);
        }
        t = largeIndices.getInt(i);
        largeIndices.setInt(i, largeIndices.getInt(j));
        largeIndices.setInt(j, t);
    }

    private void sort()
    {
        buildHeap(sortedItems);
        for (int i = n; i > 0; i--) {
            exchange(0, i);
            n -= 1;
            maxHeap(sortedItems, 0);
        }
    }
    
    private void largeSort()
    {
        builLargeHeap(largeSortedItems);
        for (long i = lN; i > 0; i--) {
            lExchange(0, i);
            lN -= 1;
            largeMaxHeap(largeSortedItems, 0);
        }
    }
    
    public static void sort(int[] sortedItems, int[] indices, int veclen)
    {
        if (indices.length * veclen != sortedItems.length) {
            System.out.println("bad table lengths");
            return;
        }
        new IntVectorHeapSort(sortedItems, indices, veclen).sort();
    }
    
    public static void sort(IntLargeArray sortedItems, IntLargeArray indices, int veclen)
    {
        if (indices.length() * veclen != sortedItems.length()) {
            System.out.println("bad table lengths");
            return;
        }
        new IntVectorHeapSort(sortedItems, indices, veclen).largeSort();
    }
    
    

    public static void main(String[] args)
    {
        for (int n = 20; n <= 10000000; n *= 2) {
            int[] t = new int[2 * n];
            int[] ind = new int[n];
            for (int i = 0; i < t.length; i++)
                t[i] = (int) (n * Math.random());
            for (int i = 0; i < ind.length; i++)
                ind[i] = i;
            long s = System.currentTimeMillis();
            sort(t, ind, 2);
            System.out.printf("%8d %7d%n", n, System.currentTimeMillis() - s);
            if (n == 20)
                for (int i = 0; i < n; i++)
                    System.out.printf("%3d %3d%n", t[2 * i], t[2 * i + 1]);
        }
        for (int n = 20; n <= 10000000; n *= 2) {
            IntLargeArray t = new IntLargeArray(2 * n);
            IntLargeArray ind = new IntLargeArray(n);
            for (int i = 0; i < t.length(); i++)
                t.setInt(i, (int) (n * Math.random()));
            for (int i = 0; i < ind.length(); i++)
                ind.setInt(i, i);
            long s = System.currentTimeMillis();
            sort(t, ind, 2);
            System.out.printf("%8d %7d%n", n, System.currentTimeMillis() - s);
            if (n == 20)
                for (int i = 0; i < n; i++)
                    System.out.printf("%3d %3d%n", t.getInt(2 * i), t.getInt(2 * i + 1));
        }
    }
}
