/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author Krzysztof Nowinski (know@icm.edu.pl, knowpl@gmail.com), University of Warsaw, ICM
 */

public class MergeTimeSeries {

    /**
     * Merges timelines of an array of data arrays into one sorted sequence
     * @param args an array of data arrays
     * @return A sorted array list of float time moments - a set-theoretic sum of timelines of all arguments
     */
    public static ArrayList<Float> mergeTimeSteps(DataArray[] args)
    {
        Set<Float> tmp = new TreeSet<>();
        for (DataArray arg : args)
            tmp.addAll(arg.getTimeSeries());
        ArrayList<Float> out = new ArrayList<>();
        out.addAll(tmp);
        Collections.sort(out);
        return out;
    }

    /**
     * Merges timelines of a collection of data arrays into one sorted sequence
     * @param args a collection of data arrays
     * @return A sorted array list of float time moments - a set-theoretic sum of timelines of all arguments
     */
    public static ArrayList<Float> mergeTimeSteps(Collection<DataArray> args)
    {
        Set<Float> tmp = new TreeSet<>();
        for (DataArray arg : args)
            tmp.addAll(arg.getTimeSeries());
        ArrayList<Float> out = new ArrayList<>();
        out.addAll(tmp);
        Collections.sort(out);
        return out;
    }

    private MergeTimeSeries()
    {

    }
}
