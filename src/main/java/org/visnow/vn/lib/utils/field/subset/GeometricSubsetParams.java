/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset;

import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import static org.visnow.vn.lib.utils.field.subset.GeometricSubsetParams.Position.*;
import static org.visnow.vn.lib.utils.field.subset.GeometricSubsetParams.Depth.*;
/**
 *
 * @author know
 */


public class GeometricSubsetParams {
    
    public static enum Position {FULLY_IN, IN, BOUNDARY, OUT, FULLY_OUT, BOUNDARY_SURFACE};
    public static enum Depth {MASK, CELLS, FULL, INDEX};
    boolean geometricCrop = true;
    private GlyphType subsetGeometry  = BOX;
    private Position  subsetType      = FULLY_IN;
    private Depth     processingDepth = CELLS;
    private InteractiveGlyphParams geometryParams = null;
    private ComponentSubrange subrange = null;

    public GeometricSubsetParams(boolean geometricCrop, 
                                 InteractiveGlyphParams geometryParams, 
                                 ComponentSubrange subrange, 
                                 Position subsetType, 
                                 Depth processingDepth)
    {
        this.geometricCrop   = geometricCrop;
        this.geometryParams  = geometryParams;
        this.subrange        = subrange;
        subsetGeometry       = geometryParams.getGlyphType();
        this.subsetType      = subsetType;
        this.processingDepth = processingDepth;
    }

    public GlyphType getSubsetGeometry()
    {
        return subsetGeometry;
    }

    public Position getSubsetType()
    {
        return subsetType;
    }

    public Depth getProcessingDepth()
    {
        return processingDepth;
    }

    public InteractiveGlyphParams getGeometryParams()
    {
        return geometryParams;
    }

    public boolean isGeometricCrop()
    {
        return geometricCrop;
    }

    public ComponentSubrange getSubrange()
    {
        return subrange;
    }
    
    
}

