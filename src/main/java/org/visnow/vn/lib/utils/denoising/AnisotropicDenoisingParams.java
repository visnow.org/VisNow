/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.denoising;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class AnisotropicDenoisingParams
{

    public static final int AVERAGE = 0;
    public static final int MEDIAN = 1;

    protected int iterations = 1;
    protected int method = AVERAGE;
    protected int radius = 3;
    protected float slope = 3.f;
    protected float slope1 = 3.f;
    protected int[][] components = null;

    public AnisotropicDenoisingParams(int method, int radius, float slope, float slope1, int[][] components)
    {
        this.method = method;
        this.radius = radius;
        this.slope = slope;
        this.slope1 = slope1;
        this.components = components;
    }
    
    public AnisotropicDenoisingParams(int iterations, int method, int radius, float slope, float slope1, int[][] components)
    {
        this(method, radius, slope, slope1, components);
        this.iterations = iterations;
    }

    public int getIterations()
    {
        return iterations;
    }
    
    public int getMethod()
    {
        return method;
    }

    public int getRadius()
    {
        return radius;
    }

    public float getSlope()
    {
        return slope;
    }

    public float getSlope1()
    {
        return slope1;
    }

    public int getComponentsNumber()
    {
        return components.length;
    }

    public int getComponent(int i)
    {
        return components[i][0];
    }

    public int getAnisotropyComponent(int i)
    {
        return components[i][1];
    }

}
