/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.interpolation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class FieldPosition implements DataObjectInterface, Comparable, Serializable
{
    
    public int index;
    public final float v;
    public final int[] p;
    public final long[] lp;
    public final float[] t;

    public FieldPosition(int index, float v, int[] p, float[] t) {
        this.index = index;
        this.v = v;
        this.p = Arrays.copyOf(p, p.length);
        this.t = Arrays.copyOf(t, t.length);
        lp = new long[p.length];
        for (int i = 0; i < t.length; i++) 
            lp[i] = p[i];
    }
    
    public FieldPosition(int index, float v, long[] lp, float[] t) {
        this.index = index;
        this.v = v;
        this.lp = Arrays.copyOf(lp, lp.length);
        this.t = Arrays.copyOf(t, t.length);
        p = null;
    }
    
    public FieldPosition(SimplexPosition simplexPosition)
    {
        index = 0;
        int[] verts = simplexPosition.getVertices();
        p = Arrays.copyOf(verts, verts.length);
        t = Arrays.copyOf(simplexPosition.getCoords(), verts.length);
        lp = new long[p.length];
        for (int i = 0; i < t.length; i++) 
            lp[i] = p[i];
        v = t[0];
    }
    
    public FieldPosition(int[] dims, float[] postion)
    {
        index = 0;
        int n = 1;
        int nDim = dims.length;
        float[] pos = new float[nDim];
        int[] ind = new int[nDim];
        for (int i = 0; i < nDim; i++) {
            pos[i] = Math.max(0, Math.min(postion[i], dims[i] - 1));
            ind[i] = (int)pos[i];
            pos[i] -= ind[i];
            if (pos[i] > 0)
                n *= 2;
        }
        p = new int[n];
        lp = null;
        t = new float[n];
        v = 0;
        int off =1;
        int m = 1;
        int l = ind[nDim - 1];
        for (int i = nDim - 2; i >= 0; i--) 
            l = l * dims[i] + ind[i];        // l - init indexof the cell
        p[0] = l;
        t[0] = 1;
        for (int i = 0; i < nDim; i++) {
            if (pos[i] > 0) {
                for (int j = 0; j < m; j++) {
                    t[m + j] = pos[i] * t[j];
                    t[j]    *= (1 - pos[i]);
                    p[m + j] = p[j] + off;
                }
                m *= 2;
            }
            off *= dims[i];
        }
    }
    
    public boolean isValid(byte[] mask)
    {
        if (p == null || p.length < 1)
            return false;
        for (int i : p) 
            if (mask[i] == 0)
                return false;
        return true;
    }
    
    public boolean isValid(LogicLargeArray mask)
    {
        
        if (lp == null || lp.length < 1) {
            if (p == null || p.length < 1)
                return false;
            for (int i : p) 
                if (!mask.getBoolean(i))
                    return false;
            return true;
        }
        for (long i : lp) 
            if (!mask.getBoolean(i))
                return false;
        return true;
    }
    
    @Override
    public float toFloat()
    {
        return v;
    }
    
    public int getHash()
    {
        if (p != null)
            return Arrays.hashCode(p);
        else
            return Arrays.hashCode(lp);
    }
    
    @Override
    public int compareTo(Object o) 
    {
        return ((FieldPosition)o).v < v ? 1 : (((FieldPosition)o).v == v ? 0 : -1);
    }
    
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder(String.format("%6d %6.3f   ", index, v));
        if (p != null)
            for (int i = 0; i < t.length; i++) 
                s.append(String.format("%6d %6.3f  ",p[i], t[i]));
        else
            for (int i = 0; i < t.length; i++) 
                s.append(String.format("%10d %6.3f  ",lp[i], t[i]));
        return s.toString();
    }
    
    public static FieldPosition combine(int ind, float val, float[] weights, FieldPosition[] positions)
    {
        if (positions == null || weights == null || positions.length != weights.length)
            throw new IllegalArgumentException("Both argument arrays must be of the same legth");
        float s = 0;
        for (float w : weights) 
            s += w;
        if (s < .99999 || s > 1.00001)
            throw new IllegalArgumentException("weights must sum to 1");
        int n = 0;
        boolean intPositions = true;
        if (positions[0].p == null)
            intPositions = false;
        for (FieldPosition pos : positions) 
            n += pos.t.length;
        if (intPositions) {
            int[] pp = new int[n];
            float[] pt = new float[n];
            for (int i = 0, l = 0; i < positions.length; i++) {
                FieldPosition pos = positions[i];
                for (int j = 0; j < pos.t.length; j++, l++) {
                    pp[l] = pos.p[j];
                    pt[l] = weights[i] * pos.t[j];
                }
            } 
            return new FieldPosition(ind, val, pp, pt);
        }
        else {
            long[] lpp = new long[n];
            float[] pt = new float[n];
            for (int i = 0, l = 0; i < positions.length; i++) {
                FieldPosition pos = positions[i];
                for (int j = 0; j < pos.p.length; j++, l++) {
                    lpp[l] = pos.lp[j];
                    pt[l] = weights[i] * pos.t[j];
                }
            } 
            return new FieldPosition(ind, val, lpp, pt);
        }
    }
    
    public float[] getInterpolatedVal(LargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < t.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in.getFloat(vlen * lp[i] + j);
        return val;
    }
//    
    
    public float getInterpolatedVal(LargeArray in)
    {
        float val = 0;
        for (int i = 0; i < t.length; i++) 
            val += t[i] * in.getFloat(p[i]);
        return val;
    }
    
    private static class ComputeInterpolation implements Runnable
    {
        private final FieldPosition[] nodes;
        private final LargeArray inArray;
        private final LargeArray outArray;
        private final ByteLargeArray mask;
        private final int vlen;
        private final int from;
        private final int to;

        public ComputeInterpolation(FieldPosition[] nodes,
                                    LargeArray inArray, LargeArray outArray, ByteLargeArray mask, 
                                    int vlen, int from, int to) {
            this.nodes = nodes;
            this.inArray = inArray;
            this.outArray = outArray;
            if (mask != null && inArray.length() == vlen * mask.length())
                this.mask = mask;
            else
                this.mask = null;
            this.vlen = vlen;
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            for (int i = from, k = vlen * from; i < to; i++)
                if (nodes[i] != null)
                    for (int j = 0; j < vlen; j++, k++) { 
                        float v = 0;
                        float[] t = nodes[i].t;
                        int[] p = nodes[i].p;
                        long[] lp  = nodes[i].lp;
                        if (p != null)
                            for (int l = 0; l < t.length; l++) 
                                v += t[l] * inArray.getFloat(vlen * p[l] + j);
                        else
                            for (int l = 0; l < t.length; l++) 
                                v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                        outArray.setFloat(k, v);
                        if (mask != null)
                            mask.setByte(i, (byte)1);
                    }
                else {
                    if (mask != null)
                        mask.setByte(i, (byte)0);
                    k += vlen;
                }
        }
    }
    
    public static LargeArray interpolate(FieldPosition[] nodes, LargeArray inArray, int vlen)
    {
        if (nodes == null || nodes.length == 0)
            return null;
        int nNodes = nodes.length;
        LargeArray outArray = LargeArrayUtils.create(inArray.getType(), vlen * nNodes);
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = 
                    new Thread(new ComputeInterpolation(nodes, inArray, outArray, null, vlen, 
                                                        (iThread * nNodes) / nThreads,
                                                        ((iThread + 1) * nNodes) / nThreads));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outArray;
    }
    
    public static LargeArray interpolate(FieldPosition[] nodes, LargeArray inArray, ByteLargeArray mask, int vlen)
    {
        if (nodes == null || nodes.length == 0)
            return null;
        int nNodes = nodes.length;
        LargeArray outArray = LargeArrayUtils.create(inArray.getType(), vlen * nNodes);
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = 
                    new Thread(new ComputeInterpolation(nodes, inArray, outArray, mask, vlen, 
                                                        (iThread * nNodes) / nThreads,
                                                        ((iThread + 1) * nNodes) / nThreads));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outArray;
    }
    
    public static TimeData interpolate(FieldPosition[] nodes, TimeData inData, int vlen)
    {
        float[] inTimes = inData.getTimesAsArray();
        ArrayList<Float> outTimeSeries = new ArrayList<>();
        ArrayList<LargeArray> outDataSeries = new ArrayList<>();
        for (int i = 0; i < inTimes.length; i++) {
            outTimeSeries.add(inTimes[i]);
            outDataSeries.add(interpolate(nodes, inData.getValues().get(i), vlen));
        }        
        return new TimeData(outTimeSeries, outDataSeries, vlen);
    }
    
    public static DataArray interpolate(FieldPosition[] nodes, DataArray inData)
    {
        TimeData outTimeData = interpolate(nodes, inData.getTimeData(), inData.getVectorLength());
        return DataArray.create(outTimeData, inData.getVectorLength(), inData.getName(), inData.getUnit(), inData.getUserData()).
                                preferredRanges(inData.getPreferredMinValue(),     inData.getPreferredMaxValue(), 
                                                inData.getPreferredPhysMinValue(), inData.getPreferredPhysMaxValue());
    }
    
    public static float[] interpolateToIndices(FieldPosition[] nodes, int[] dims)
    {
        int nDims = dims.length;
        float[] ndx = new float[nDims];
        float[] indices = new float[nDims * nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            float[] t = nodes[i].t;
            int[] p = nodes[i].p;
            long[] lp  = nodes[i].lp;
            Arrays.fill(ndx, 0);
            for (int j = 0; j < t.length; j++) {
                long k = p != null ? p[j] : lp[j];
                for (int m = 0; m < nDims; m++) {
                    ndx[m] += t[j] * (k % dims[m]);
                    k /= dims[m];
                }
            }
            System.arraycopy(ndx, 0, indices, nDims * i, nDims);
        }
        return indices;
    }

}
