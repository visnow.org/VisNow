/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.graph3d;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.types.VNField;
import static org.visnow.vn.lib.utils.graph3d.GenericGraph3DShared.*;
import org.visnow.vn.lib.types.VNRegularField;
import static org.visnow.vn.lib.utils.field.GeometricOrientation.createBoundaryNodeNormals;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class AbstractGraph3D extends OutFieldVisualizationModule
{
    protected GenericGraph3DGUI genericGUI = null;

    protected Field inField;
    protected DataArray plotDataArray;
    protected boolean fromParams = false;
    protected boolean newOutFieldStructure = true;
    protected boolean invalidDataComponent = true;

    protected float scaleCoeff, scale, base;
    protected float rMin,  rMax;
    protected float phMin, phMax;

    protected int fieldPlane = -1;
    protected float[] inCoords;
    protected float[] outCoords;
    protected float[] normals = null;
    protected float[] vals;
    protected RegularField outBox = new RegularField(new int[]{2, 2, 2});
    protected float[][] extents = new float[2][3];

    public AbstractGraph3D()
    {
        outBox.addComponent(DataArray.create(new int[]{0, 1, 2, 3, 4, 5, 6, 7}, 1, "dummy"));
        parameters.addParameterChangelistener((String name) -> {
            if (name.equals(GLOBAL_RANGE_STRING))
                updateScale();
            fromParams = true;
        });
        parameters.get(COMPONENT).addChangeListener((ChangeEvent e) -> {
            fromParams = true;
            startAction();
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return GenericGraph3DShared.createDefaultParameters();
    }

    /**
     * Checks if structures of two fields are identical: both fields of the same type and the number of nodes,
     * identical dimensions in the case of regular fields and identical (up to a permutation) cell set structures
     * (corresponding nodes arrays are identical)
     * @param f0 first field to be compared
     * @param f1 second field to be compared
     * @return true if geometry structures of f0  and f1 are identical
     */
    public boolean isStructureIdentical(Field f0, Field f1)
    {
        if (f0.getType() != f1.getType() || f0.getNNodes() != f1.getNNodes())
            return false;
        switch (f0.getType()) {
        case FIELD_POINT:
            return true;
        case FIELD_REGULAR:
            int[] d0 = ((RegularField)f0).getDims();
            int[] d1 = ((RegularField)f1).getDims();
            if (d0.length != d1.length)
                return false;
            for (int i = 0; i < d0.length; i++)
                if (d0[i] != d1[i])
                    return false;
            return true;
        case FIELD_IRREGULAR:
            ArrayList<CellSet> cs0 = ((IrregularField)f0).getCellSets();
            ArrayList<CellSet> cs1 = ((IrregularField)f1).getCellSets();
            if (cs0.size() != cs1.size())
                return false;
            cellSetLoop:
            for (CellSet s0 : cs0) {
                for (CellSet s1 : cs1)
                    if (s0.getName().equals(s1.getName())) {
                        for (int i = 0; i < s0.getCellArrays().length; i++) {
                            CellArray c0 = s0.getCellArrays()[i];
                            CellArray c1 = s1.getCellArrays()[i];
                            if (c0 == null && c1 == null)
                                continue;
                            if (c0 == null && c1 != null ||
                                c1 == null && c0 != null ||
                                !Arrays.equals(c0.getNodes(), c1.getNodes()))
                                return false;
                        }
                        continue cellSetLoop;
                    }
                return true;
            }
        default:
            return false;
        }
    }

    protected void updateRanges()
    {
        if (plotDataArray == null)
            return;

        if (plotDataArray.getMaxValue() == plotDataArray.getMinValue())
            plotDataArray.recomputeStatistics();
        if (parameters.get(GLOBAL_RANGE) || plotDataArray.getMaxValue() == plotDataArray.getMinValue()) {
            rMax = (float)plotDataArray.getPreferredMaxValue();
            rMin = (float)plotDataArray.getPreferredMinValue();
        }
        else {
            rMax = (float)plotDataArray.getMaxValue();
            rMin = (float)plotDataArray.getMinValue();
        }
        phMin = (float)plotDataArray.getSchema().dataRawToPhys(rMin);
        phMax = (float)plotDataArray.getSchema().dataRawToPhys(rMax);
        scaleCoeff = inField.getDiameter() / (phMax - phMin);
        if (Float.isInfinite(scaleCoeff) || Float.isNaN(scaleCoeff))
            scaleCoeff = 1;
        if (genericGUI != null)
            genericGUI.setValGeomRatio(scaleCoeff);
        if (parameters.get(ZERO_BASED)) {
            double dataZero = plotDataArray.getSchema().dataPhysToRaw(0);
            if (rMin > dataZero)
                rMin = (float)dataZero;
            if (rMax < dataZero)
                rMax = (float)dataZero;
        }
        if (genericGUI != null)
            genericGUI.setValGeomRatio(scaleCoeff);
        updateScale();
    }

    protected void updateScale()
    {
        scale = (float)(parameters.get(SCALE) * scaleCoeff);
        base = parameters.get(ZERO_BASED) ? (float)(rMin - phMin * (rMax - rMin) / (phMax - phMin)) : phMin;
    }

    protected void createGraph3DData()
    {
        updateRanges();
        updateInputCoords();
        plotDataArray = inField.getComponent(parameters.get(COMPONENT).getComponentName());
        vals = plotDataArray.getVectorLength() == 1 ?
                       plotDataArray.getRawFloatArray().getData() :
                       plotDataArray.getVectorNorms().getData();
        fieldPlane = inField.getPlaneOrientationAxis();
        if (fieldPlane >= 0) {
            normals = new float[3 * (int)inField.getNNodes()];
            Arrays.fill(normals, 0);
            for (int i = fieldPlane; i < normals.length; i += 3)
                normals[i] = 1;
        }
        else if (!inField.hasCoords()) {
            float[][] af = ((RegularField)inField).getAffine();
            float[] norm = new float[] {
                af[0][1] * af[1][2] - af[0][2] * af[1][1],
                af[0][2] * af[1][0] - af[0][0] * af[1][2],
                af[0][0] * af[1][1] - af[0][1] * af[1][0],
            };
            float nrm = 1 / (float)Math.sqrt(norm[0] * norm[0] + norm[1] * norm[1] + norm[2] * norm[2]);
            for (int i = 0; i < norm.length; i++)
                norm[i] *= nrm;
            normals = new float[3 * (int)inField.getNNodes()];
            for (int i = 0; i < normals.length; i += 3)
                System.arraycopy(norm, 0, normals, i, 3);
        }
        else {
            if (inField.getNormals() == null && inField instanceof IrregularField)
                    createBoundaryNodeNormals((IrregularField)inField);
            normals = inField.getNormals().getData();
        }
        updateScale();
    }

    protected void updateOutBox()
    {
        outField.updatePreferredExtents();
        float[][] inExt   = outField.getExtents();
        float[][] inPhExt = inField.getPhysicalExtents();
        float[][] outPhExt = new float[2][3];
        for (int i = 0; i < 2; i++)
            System.arraycopy(inPhExt[i], 0, outPhExt[i], 0, 3);
        String[] inCoordsUnits = inField.getCoordsUnits();
        String[] coordsUnits   = new String[3];
        String[] axesNames     = inField.getAxesNames();
        if (axesNames == null || axesNames.length < 3)
            axesNames = new String[] {"x", "y", "z"};
        System.arraycopy(inCoordsUnits, 0, coordsUnits, 0, 3);
        if (fieldPlane >= 0) {
            outPhExt[0][fieldPlane] = phMin;
            outPhExt[1][fieldPlane] = phMax;
            axesNames[fieldPlane] = plotDataArray.getName();
            coordsUnits[fieldPlane] = plotDataArray.getUnit();
        }
        outBox.setAffine(MatrixMath.computeAffineFromExtents(outBox.getDims(), inExt));
        outField.setPreferredExtents(inExt, outPhExt);
        outBox.setPreferredExtents(inExt, outPhExt);
        outField.setAxesNames(axesNames);
        outBox.setAxesNames(axesNames);
        outField.setCoordsUnits(coordsUnits);
        outBox.setCoordsUnits(coordsUnits);
    }

    abstract public void createOutField();

    abstract protected void updateCoords();

    abstract protected void updateOutputData();

    abstract protected void updateRenderingParams();

    protected void updateInputCoords()
    {
        if (inField.hasCoords())
            inCoords = inField.getCurrentCoords().getData();
        else
            inCoords = ((RegularField)inField).getCoordsFromAffine().getData();
    }

    protected void additionalParamsSetting(Field in)
    {
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        Field in = ((VNField) getInputFirstValue("inField")).getField();
        if (!fromParams && in == inField)
            return;
        fromParams = false;
        if (in != null &&
            (in instanceof RegularField && ((RegularField)in).getDimNum() != 2 ||
             in instanceof IrregularField && ((IrregularField)in).hasCells3D())) {
            outField = null;
            return;
        }
        if (inField == null || !in.isDataCompatibleWith(inField)) {
            parameters.get(COMPONENT).setContainerSchema(in.getSchema());
            additionalParamsSetting(in);
        }
        newOutFieldStructure |= (outField == null || !isStructureIdentical(inField, in));
        inField = in;
        createGraph3DData();
        if (newOutFieldStructure)
            createOutField();
        else {
            createGraph3DData();
            updateRanges();
            updateScale();
            updateCoords();
        }
        updateOutputData();
        updateOutBox();
        prepareOutputGeometry();
        outObj.setExtents(outField.getExtents());
        updateRenderingParams();
        show();
        setOutputValue("outBox", new VNRegularField(outBox));
        newOutFieldStructure = fromParams = false;
    }
}
