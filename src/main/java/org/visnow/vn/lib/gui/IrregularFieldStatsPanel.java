/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui;

import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import static org.visnow.vn.lib.basic.utilities.FieldStats.FieldStatsShared.*;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.system.swing.FixedGridBagLayoutPanel;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class IrregularFieldStatsPanel extends FixedGridBagLayoutPanel
{

    Parameters params;
    String name;
    long nnodes;
    float[][] fieldExtends;
    float[][] physExtends;
    String[] componentNames;
    DataContainerSchema schema;
    long[][] histograms;

    /**
     * Creates new form RegularFieldStatsPanel
     */
    public IrregularFieldStatsPanel()
    {
        initComponents();
    }

    public void setProperties(Parameters params, int selectedComponentIndex, boolean logScale, String name, long nnodes, float[][] fieldExtends, float[][] physExtends, String[] componentNames, DataContainerSchema schema, long[][] histograms)
    {
        this.params = params;
        this.name = name;
        this.nnodes = nnodes;
        this.fieldExtends = fieldExtends;
        this.physExtends = physExtends;
        this.componentNames = componentNames;
        this.schema = schema;
        this.histograms = histograms;
        fieldNameLabel.setText(name + " " + (int) nnodes + " nodes");
        extLabel.setText(String.format("[%4.1f:%4.1f]x[%4.1f:%4.1f]x[%4.1f:%4.1f]",
                                       fieldExtends[0][0], fieldExtends[1][0],
                                       fieldExtends[0][1], fieldExtends[1][1],
                                       fieldExtends[0][2], fieldExtends[1][2]));
        physExtLabel.setText(String.format("[%4.1f:%4.1f]x[%4.1f:%4.1f]x[%4.1f:%4.1f]",
                                           physExtends[0][0], physExtends[1][0],
                                           physExtends[0][1], physExtends[1][1],
                                           physExtends[0][2], physExtends[1][2]));
        componentsBox.setModel(new javax.swing.DefaultComboBoxModel(componentNames));
        setSelectedComponentIndex(selectedComponentIndex);
        setLogScale(logScale);
        binCountCB.setSelectedItem(params.get(BIN_COUNT));
        show(selectedComponentIndex, params.get(BIN_COUNT));
    }

    private void show(int n, int binCount)
    {
        if (n >= 0 && n < componentNames.length) {
            String[] minMeanMax = EngineeringFormattingUtils.formatInContext(new double[]{schema.getComponentSchema(n).getMinValue(), schema.getComponentSchema(n).getMeanValue(), schema.getComponentSchema(n).getMaxValue()});
            vMinLabel.setText(minMeanMax[0]);
            vMeanLabel.setText(minMeanMax[1]);
            vMaxLabel.setText(minMeanMax[2]);
            vSDLabel.setText(EngineeringFormattingUtils.format(schema.getComponentSchema(n).getStandardDeviationValue()));
            //assuming histograms of length 256 
            long[] valueHistogram = histograms[n];
            int downsize = 256 / binCount;
            long[] valueHistogramDownsized = new long[binCount];
            int k = 0;
            for (int i = 0; i < binCount; i++)
                for (int j = 0; j < downsize; j++) valueHistogramDownsized[i] += valueHistogram[k++];

            histogramPlot1.setData(valueHistogramDownsized, logBox.isSelected(), schema.getComponentSchema(n).getMinValue(), schema.getComponentSchema(n).getMaxValue());
        }
    }

    public int getSelectedComponentIndex()
    {
        return componentsBox.getSelectedIndex();
    }

    public void setSelectedComponentIndex(int idx)
    {
        componentsBox.setSelectedIndex(idx);
    }

    public boolean isLogScale()
    {
        return logBox.isSelected();
    }

    public void setLogScale(boolean logScale)
    {
        logBox.setSelected(logScale);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        valLabel = new javax.swing.JLabel();
        valALabel = new javax.swing.JLabel();
        valSDLabel = new javax.swing.JLabel();
        valHistoPanel = new javax.swing.JPanel();
        histogramPlot1 = new org.visnow.vn.lib.gui.HistogramPlot();
        jLabel7 = new javax.swing.JLabel();
        vMeanLabel = new javax.swing.JLabel();
        vSDLabel = new javax.swing.JLabel();
        componentsBox = new javax.swing.JComboBox();
        logBox = new javax.swing.JCheckBox();
        valMinLabel = new javax.swing.JLabel();
        valMaxLabel = new javax.swing.JLabel();
        vMinLabel = new javax.swing.JLabel();
        vMaxLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        fieldNameLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dimsLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        extLabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        physExtLabel = new javax.swing.JLabel();
        binCountCB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        jLabel1 = new javax.swing.JLabel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));

        valLabel.setText("Values:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        add(valLabel, gridBagConstraints);

        valALabel.setText("Mean:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valALabel, gridBagConstraints);

        valSDLabel.setText("Std. dev.:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(valSDLabel, gridBagConstraints);

        valHistoPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledText")));
        valHistoPanel.setLayout(new java.awt.BorderLayout());

        histogramPlot1.setPreferredSize(new java.awt.Dimension(100, 200));
        valHistoPanel.add(histogramPlot1, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valHistoPanel, gridBagConstraints);

        jLabel7.setText("Value histogram:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 1, 0);
        add(jLabel7, gridBagConstraints);

        vMeanLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(vMeanLabel, gridBagConstraints);

        vSDLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(vSDLabel, gridBagConstraints);

        componentsBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        componentsBox.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                componentsBoxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 7, 0);
        add(componentsBox, gridBagConstraints);

        logBox.setText("Log scale");
        logBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        logBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        logBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                logBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 8, 0);
        add(logBox, gridBagConstraints);

        valMinLabel.setText("min");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valMinLabel, gridBagConstraints);

        valMaxLabel.setText("max");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valMaxLabel, gridBagConstraints);

        vMinLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(vMinLabel, gridBagConstraints);

        vMaxLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(vMaxLabel, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        jPanel1.add(fieldNameLabel, gridBagConstraints);

        jLabel2.setText("Dimensions:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel1.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(dimsLabel, gridBagConstraints);

        jLabel4.setText("Extents:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel1.add(jLabel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(extLabel, gridBagConstraints);

        jLabel6.setText("Physical extents:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel1.add(jLabel6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(physExtLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jPanel1, gridBagConstraints);

        binCountCB.setListData(new Object[]{8, 16, 32, 64, 128, 256});
        binCountCB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                binCountCBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(binCountCB, gridBagConstraints);

        jLabel1.setText("Bin count:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(jLabel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void logBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logBoxActionPerformed
        show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        params.setParameterActive(false);
        params.set(LOG_SCALE, logBox.isSelected());
        params.setParameterActive(true);
    }//GEN-LAST:event_logBoxActionPerformed

    private void componentsBoxItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_componentsBoxItemStateChanged
    {//GEN-HEADEREND:event_componentsBoxItemStateChanged
        show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        params.setParameterActive(false);
        params.set(SELECTED_COMPONENT, componentsBox.getSelectedIndex());
        params.setParameterActive(true);
    }//GEN-LAST:event_componentsBoxItemStateChanged

    private void binCountCBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_binCountCBUserChangeAction
    {//GEN-HEADEREND:event_binCountCBUserChangeAction
        if (params != null) {
            params.set(BIN_COUNT, (int) binCountCB.getSelectedItem());
            show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        }
    }//GEN-LAST:event_binCountCBUserChangeAction

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.swingwrappers.ComboBox binCountCB;
    private javax.swing.JComboBox componentsBox;
    private javax.swing.JLabel dimsLabel;
    private javax.swing.JLabel extLabel;
    private javax.swing.JLabel fieldNameLabel;
    private javax.swing.Box.Filler filler1;
    private org.visnow.vn.lib.gui.HistogramPlot histogramPlot1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JCheckBox logBox;
    private javax.swing.JLabel physExtLabel;
    private javax.swing.JLabel vMaxLabel;
    private javax.swing.JLabel vMeanLabel;
    private javax.swing.JLabel vMinLabel;
    private javax.swing.JLabel vSDLabel;
    private javax.swing.JLabel valALabel;
    private javax.swing.JPanel valHistoPanel;
    private javax.swing.JLabel valLabel;
    private javax.swing.JLabel valMaxLabel;
    private javax.swing.JLabel valMinLabel;
    private javax.swing.JLabel valSDLabel;
    // End of variables declaration//GEN-END:variables

}
