/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.value;

import java.awt.BorderLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.gui.widgets.FloatSlider;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI;

/**
 *
 * @author know
 */


public class ComponentValueUI extends ComponentFeatureUI
{
   protected FloatSlider valueSlider = new FloatSlider();
   protected ComponentValue param;

   public ComponentValueUI()
   {
      super();
      add (valueSlider, BorderLayout.CENTER);
      valueSlider.addChangeListener(new ChangeListener()
      {
         @Override
         public void stateChanged(ChangeEvent e)
         {
            valChanged();
         }
      });
   }

   public void setComponentValue(ComponentValue param)
   {
      userUpdate = false;
      this.param = param;
      super.setComponentFeature(param);
      param.setUI(this);
      valueSlider.setMinMax(param.getComponentPhysMin(),param.getComponentPhysMax());
      userUpdate = true;
   }

   public boolean isAdjusting()
   {
      return valueSlider.isAdjusting();
   }

   void setRange(float min, float max)
   {
      userUpdate = false;
      valueSlider.setMinMax(min, max);
      userUpdate = true;
   }

   void updateRange()
   {
      userUpdate = false;
      if (param != null)
         valueSlider.setMinMax(param.getComponentPhysMin(), param.getComponentPhysMax());
      userUpdate = true;
   }

   void updateValue()
   {
      userUpdate = false;
      if (param != null)
         valueSlider.setVal(param.getPhysicalValue());
      userUpdate = true;
   }

   FloatValueModificationListener listener = null;

   public void setListener(FloatValueModificationListener listener)
   {
      this.listener = listener;
   }

   public void clearListener()
   {
      listener = null;
   }

   private void valChanged()
   {
      if (listener != null && userUpdate)
         listener.floatValueChanged(new FloatValueModificationEvent(this, valueSlider.getVal(), valueSlider.isAdjusting()));
   }

   @Override
    protected void updateUIToNewComponent(boolean isNull)
    {
        valueSlider.setEnabled(!isNull);
    }


}
