/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.scale;

import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.FieldSchema;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.*;
import static org.visnow.vn.lib.gui.ComponentBasedUI.scale.ComponentScale.ScaleAlgorithm.*;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * This parameter class is designed for automatic scaling of componentSchema values to match (usually)
 * with the field geometry extents - for use in graphing functions, glyph scaling and streamlines.
 * 
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentScale extends ComponentFeature
{
    public enum ScaleAlgorithm {SCALE_BY_AVG, SCALE_BY_EXT, SCALE_BY_RMS, SCALE_BY_MAX, SCALE_BY_SIGMA};
    
    protected float value = 1;
    
    /**
     * indicates denominator in default value computation
     */
    protected  ScaleAlgorithm  scaleBy = SCALE_BY_RMS;
    /**
     * indicates the ratio of extent produced by the default scale defaultValue * componentExtent = scaleTo * geometryExtent
     */
    protected float scaleTo = .01f;
    /**
     * indicates the logarithmic range of scale range slider in UI:
     * the slider will range from default/scaleRange to default*scaleRange
     */
    protected float scaleRange = 10;
    protected float scaleLow   = 1.f/scaleRange;
    protected float scaleUp    = scaleRange;
    
    protected float componentExtent = 1;
    protected float geometryExtent  = 1;
    protected ComponentScaleUI valUI = null;
    protected float initValue = geometryExtent * scaleTo / componentExtent;
    
    protected FloatValueModificationListener uiValueChangedListener = 
           new FloatValueModificationListener()
           {
              @Override
              public void floatValueChanged(FloatValueModificationEvent e)
              {
                 if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                    return;
                 value = e.getVal();
                 fireStateChanged();
              }
           };
    
    public ComponentScale()
    {
       super();
       preferredExtentChangeBehavior = KEEP_IF_INSIDE;
    }

    public void setScaleBy(ScaleAlgorithm scaleBy)
    {
        this.scaleBy = scaleBy;
    }

    public ComponentScale scaleBy(ScaleAlgorithm scaleBy)
    {
        this.scaleBy = scaleBy;
        return this;
    }

    public void setScaleRange(float scaleRange)
    {
        this.scaleRange = scaleRange;
    }
    
    public ComponentScale scaleRange(float scaleRange)
    {
        this.scaleRange = scaleRange;
        return this;
    }

    public void setExtent(float extent)
    {
        this.geometryExtent = extent;
    }

    public void setValue(float value)
    {
        if (componentSchema == null)
            return;
        this.value = value;
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setValueFromUI(float value)
    {
        if (componentSchema == null)
            return;
        this.value = value;
        if (continuousUpdate || valUI == null || !valUI.isAdjusting())
            fireStateChanged();
    }

    public float getValue()
    {
        return value;
    }

    public float getScaleTo()
    {
        return scaleTo;
    }

    public float getScaleRange()
    {
        return scaleRange;
    }

    public void setScaleTo(float scaleTo)
    {
        this.scaleTo = scaleTo;
        updateComponentScale();
    }

    public float getInitValue()
    {
        return initValue;
    }

    public void updateComponentScale()
    {
        initValue = geometryExtent * scaleTo / componentExtent;
        if (valUI != null)
            valUI.updateRange();
        scaleLow   = initValue / scaleRange;
        scaleUp    = initValue * scaleRange;
        switch (preferredExtentChangeBehavior)
        {
        case ALWAYS_RESET:
            value = initValue; 
            break;
        case KEEP_IF_INSIDE: 
            if (value < scaleLow || value > scaleUp)
                value = initValue;
            break;
        case ALWAYS_KEEP:
            break;
        }
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    @Override
    public void setComponentSchema(String componentName)
    {
        if (fireOnUpdate) {
            super.setComponentSchema(componentName);
            if (componentSchema == null)
                return;
            switch (scaleBy) {
            case SCALE_BY_AVG:
                componentExtent = (float)componentSchema.getMeanValue();
                break;
            case SCALE_BY_EXT:
                componentExtent = (float)(componentSchema.getPreferredPhysMaxValue() - 
                                          componentSchema.getPreferredPhysMinValue());
                break;
            case SCALE_BY_MAX:
                componentExtent = (float)componentSchema.getPreferredPhysMaxValue();
                break;
            case SCALE_BY_RMS:
                componentExtent = (float)componentSchema.getMeanSquaredValue();
                break;
            case SCALE_BY_SIGMA:
                componentExtent = (float)componentSchema.getStandardDeviationValue();
                break;
            }
            updateComponentScale();
        }
    }

    @Override
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        fireOnUpdate = false;
        super.setContainerSchema(containerSchema);
        if (containerSchema instanceof FieldSchema)
        {
            float[][] extents = ((FieldSchema)containerSchema).getPreferredExtents();
            double s = 0;
            for (int i = 0; i < extents[0].length; i++)
                s += (extents[1][i] - extents[0][i]) * (extents[1][i] - extents[0][i]);
            geometryExtent = (float)Math.sqrt(s);
        }
        updateComponentScale();
        fireOnUpdate = true;
        fireStateChanged();
    }

    @Override
    public void reset()
    {
        if (componentSchema != null)
            setValue(scaleTo / geometryExtent);
        if (valUI != null)
        {
            valUI.updateRange();
            valUI.updateValue();
        }
    }

    @Override
    protected void localUpdateUI()
    {
       if (ui != null && ui instanceof ComponentScaleUI)
          valUI = (ComponentScaleUI)ui;
       valUI.setListener(uiValueChangedListener);
    }
    
    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid) 
            setValue(scaleTo / geometryExtent);
        if (valUI == null)
           return;
        SwingInstancer.swingRunLater(() -> {
            valUI.updateRange();
            valUI.updateValue();
        });

    }

}
