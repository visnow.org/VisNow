/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class HistoArea extends JPanel
{

    int[] histo = null;
    boolean logScale = false;

    @Override
    public void paint(Graphics g)
    {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        if (histo == null) {
            return;
        }
        double max = 0;
        if (logScale) {
            for (int i = 0; i < histo.length; i++) {
                if (max < log1p((double) histo[i])) {
                    max = log1p((double) histo[i]);
                }
            }
        } else {
            for (int i = 0; i < histo.length; i++) {
                if (max < histo[i]) {
                    max = histo[i];
                }
            }
        }
        max = (h - 4) / (max + .01);
        float d = (w - 4.f) / histo.length;
        g.setColor(Color.BLUE);
        if (logScale) {
            for (int i = 0; i < histo.length; i++) {
                int j = (int) (max * log1p((double) histo[i]));
                if (j > 1) {
                    g.drawLine((int) (i * d + 2), h - 2, (int) (i * d + 2), h - 2 - j);
                }
            }
        } else {
            for (int i = 0; i < histo.length; i++) {
                g.drawLine((int) (i * d + 2), h - 2, (int) (i * d + 2), (int) (h - 2 - max * histo[i]));
            }
        }
    }

    public void setHisto(long[] histogram)
    {
        histo = new int[histogram.length];
        for (int i = 0; i < histogram.length; i++) {
            histo[i] = (int) histogram[i];
        }
        repaint();
    }

    public void setLogScale(boolean logScale)
    {
        this.logScale = logScale;
        repaint();
    }
}
