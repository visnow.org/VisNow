/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.visnow.vn.gui.swingwrappers.UserActionAdapter;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import org.visnow.vn.gui.swingwrappers.UserEvent;

/**
 * JList with context menu that includes:
 * <li> remove all
 * <li> remove selected
 * <p>
 * Note: Assuming that this JList is backed by DefaultListModel.
 * <p>
 * Events: Fires userChangeAction on remove selected or remove all element (eventData contains ascending ordered indices of just removed elements).
 * <p>
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class RemovableList extends JList
{
    private JPopupMenu popupMenu;
    private JMenuItem removeSelectedMenuItem;
    private JMenuItem removeAllMenuItem;
    private ListDataListener listDataListener;
    private ListSelectionListener listSelectionListener;

    public RemovableList()
    {
        this(new Object[]{});
    }

    public RemovableList(Vector listData)
    {
        this(listData.toArray());
    }

    public RemovableList(final Object[] listData)
    {
        this(new DefaultListModel()
        {
            {
                for (Object element : listData) addElement(element);
            }
        });
    }

    public RemovableList(ListModel dataModel)
    {
        super(dataModel);
        if (!(dataModel instanceof DefaultListModel)) throw new IllegalArgumentException("Only DefaultListModel is supported here: " + dataModel);
        setMenu();

        listSelectionListener = new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                updatePopupEnabled();
            }
        };

        listDataListener = new ListDataListener()
        {
            @Override
            public void intervalAdded(ListDataEvent e)
            {
                updatePopupEnabled();
            }

            @Override
            public void intervalRemoved(ListDataEvent e)
            {
                updatePopupEnabled();
            }

            @Override
            public void contentsChanged(ListDataEvent e)
            {
                updatePopupEnabled();
            }
        };

        setPopupEnabledBinding();
        updatePopupEnabled();
    }

    private void clearPopupEnableBinding()
    {
        removeListSelectionListener(listSelectionListener);
        getModel().removeListDataListener(listDataListener);
    }

    private void setPopupEnabledBinding()
    {
        addListSelectionListener(listSelectionListener);
        getModel().addListDataListener(listDataListener);
    }

    private void updatePopupEnabled()
    {
        removeSelectedMenuItem.setEnabled(getSelectedIndices().length != 0);
        removeAllMenuItem.setEnabled(getModel().getSize() > 0);
    }

    @Override
    public void setModel(ListModel model)
    {
        if (!(model instanceof DefaultListModel)) throw new IllegalArgumentException("Only DefaultListModel is supported here: " + model);
        clearPopupEnableBinding();
        super.setModel(model);
        setPopupEnabledBinding();
        updatePopupEnabled();
    }

    @Override
    public void setListData(Object[] listData)
    {
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < listData.length; i++) model.addElement(listData[i]);
        setModel(model);
    }

    @Override
    public void setListData(Vector listData)
    {
        setListData(listData.toArray());
    }

    private void setMenu()
    {
        popupMenu = new JPopupMenu();
        removeSelectedMenuItem = new JMenuItem("Remove selected");
        removeSelectedMenuItem.setEnabled(false);
        removeAllMenuItem = new JMenuItem("Remove all");
        removeAllMenuItem.setEnabled(false);

        popupMenu.add(removeSelectedMenuItem);
        popupMenu.add(removeAllMenuItem);

        removeSelectedMenuItem.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                int[] indices = getSelectedIndices();
                removeSelected();
                fireValueChanged(indices);
            }
        });

        removeAllMenuItem.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                int[] indices = new int[getModel().getSize()];
                for (int i = 0; i < indices.length; i++) indices[i] = i;
                clearList();
                fireValueChanged(indices);
            }
        });

        setComponentPopupMenu(popupMenu);
    }

    private void removeSelected()
    {
        int[] indices = getSelectedIndices();
        for (int i = 0; i < indices.length; i++) ((DefaultListModel) getModel()).remove(indices[indices.length - 1 - i]);
    }

    private void clearList()
    {
        ((DefaultListModel) getModel()).clear();
    }

    private List<UserActionListener> userActionListeners = new ArrayList<>();

    /**
     * Notifies all UserActionListeners about userChangeAction.
     */
    private void fireValueChanged(int[] removedIndices)
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new UserEvent(this, removedIndices));
    }

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JFrame f = new JFrame();
                f.setLocationRelativeTo(null);
                f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                f.setLayout(new BorderLayout());
                final DefaultListModel defaultListModel = new DefaultListModel();
                defaultListModel.addElement("Item at first position");
                defaultListModel.addElement("Item 2");
                defaultListModel.addElement("Third item");
                defaultListModel.addElement("4");
                final RemovableList removableList = new RemovableList(new String[]{"asdf", "ff"});//;defaultListModel);
                removableList.addUserActionListener(new UserActionAdapter()
                {

                    @Override
                    public void userChangeAction(UserEvent event)
                    {
                        System.out.println(Arrays.toString((int[]) event.getEventData()));
                    }
                });

                f.add(removableList, BorderLayout.CENTER);
                f.pack();
                f.setVisible(true);
                new Thread(new Runnable()
                {

                    @Override
                    public void run()
                    {
                        System.out.println("Changing model");
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        SwingUtilities.invokeLater(new Runnable()
                        {

                            @Override
                            public void run()
                            {
                                removableList.setModel(defaultListModel);
                            }
                        });
                    }
                }).start();
            }
        });
    }
}
