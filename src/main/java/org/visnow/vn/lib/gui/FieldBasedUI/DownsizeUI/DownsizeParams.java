/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.FieldBasedUI.DownsizeUI;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.util.FastMath;

/**
 *
 * @author know
 */


public class DownsizeParams
{
    /**
     * A value for the <code> policy </code> indicating that the parameters will be reset to default values
     * when new field size (<code>nNodes, dims </code> arrive
     */
    public static final int ALWAYS_RESET = 0;
    /**
     * A value for the <code> policy </code> indicating that the parameters will be preserved as long as possible
     * when new field size (<code>nNodes, dims </code> arrive
     */
    public static final int KEEP_IF_INSIDE = 1;
    public static final int MAX_SHUFFLED_NODES = 1 << 28;

    protected int policy = KEEP_IF_INSIDE;
    /**
     * Preferred number of downsized data. In the case of regular downsize the actual number of downsized data
     * can be different but as close as possible to <code> preferredSize </code>
     */
    protected long preferredSize;
    /**
     * minimum/maximum number of downsized data. maxSize should be set to avoid e.g. drawing of 1000000 text glyphs
     */
    protected long minSize;
    protected long maxSize;
    /**
     * Size (number of nodes) of the input field
     */
    protected long nNodes;
    /**
     * dimensions of the input field (if regular)
     */
    protected int[] dims;
    /**
     * actual downsize factors for each index
     */
    protected int[] down = {1, 1, 1};
    /**
     * minimum and maximum downsize factors for each index
     */
    protected int[] minDown = {1, 1, 1};
    protected int[] maxDown = {100, 100, 100};
    /**
     * if false and dims are provided (downsizing of a regular field) downsize
     * factors for each field index will be computed; otherwise, a random selection list will be available
     */
    protected boolean random;
    /**
     * Array of shuffled/sampled indices for the random sample
     */
    protected int[] shuffledIndices = null;
    protected long[] sampledIndices = null;
    /**
     * Beginning position of the sample indices in the shuffled/sampled indices array
     */
    protected int startIndex = 0;

    protected int nDims = 3;
    protected int downFactor = 100;
    protected int minDownFactor = 1;
    protected int maxDownFactor = 1000;
    protected boolean regularAvailable = false;
    protected boolean stableRandomAvailable = false;
    /**
     * If centered is true, regularly sampled indices are centered in o,...,dims[i] interval,
     * start from 0 otherwise: {2,6,10} is a centered sample of {0,...,11}, {0,4,8} is uncentered
     */
    protected boolean centered = true;
    protected boolean adjusting = false;
    protected boolean active = true;
    protected DownsizeUI ui = null;

    /**
     * Standard constructor
     * @param minSize minimum number of downsized data
     * @param maxSize maximum number of downsized data
     * @param preferredSize preferred number of downsized data
     * @param random force random sampling if true
     * @param centered center regular field downsize if true
     */
    public DownsizeParams(long minSize, long  maxSize, long preferredSize, boolean random, boolean centered)
    {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.preferredSize = preferredSize;
        this.random = random;
        this.centered = centered;
    }

    /**
     * Convenience constructor defaulting regular downsize to the centered version
     * @param minSize minimum number of downsized data
     * @param maxSize maximum number of downsized data
     * @param preferredSize preferred number of downsized data
     * @param random  force random sampling if true
     */
    public DownsizeParams(long minSize, long  maxSize, long preferredSize, boolean random)
    {
        this(minSize, maxSize, preferredSize, random, true);
    }

    /**
     * Convenience constructor defaulting regular downsize to the centered version and setting preferred number of downsized data
     * to geometric mean of max and min size of downsized data (e.g. min=10, max=1000 results in preferred=100)
     * @param minSize minimum number of downsized data
     * @param maxSize maximum number of downsized data
     * @param random  force random sampling if true
     */
    public DownsizeParams(int minSize, int maxSize, boolean random)
    {
        this(minSize, maxSize, (int)FastMath.sqrt(maxSize * minSize), random);
    }
    /**
     * resets regular field downsize params to default (sample size near preferred size, downsize factors roughly equal for all indices)
     */
    protected void reset()
    {
        if (dims != null)
            setDefaultDown(dims);
    }

    /**
     * getter for the user interface
     * @return user interface if set, null otherwise
     */
    public DownsizeUI getUI()
    {
        return ui;
    }

    /**
     * setter for user interface
     * @param ui user interface to control parameters
     */
    public void setUI(DownsizeUI ui)
    {
        this.ui = ui;
    }

    /**
     * setter for the policy parameter controlling behavior of the parameters on field change
     * @param policy if ALWAYS_RESET default downsize values will be set each time the input field changes,
     * if KEEP_IF_INSIDE and the current parameters are acceptable for new field, leave them unchanged
     */
    public void setPolicy(int policy)
    {
        this.policy = policy;
    }

    /**
     * information about sliders adjusting status
     * @return true if one of the UI sliders is currently dragged, false otherwise
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    /**
     * setter for base field information
     * @param nodes number of field nodes (in all field types)
     * @param dims field dimensions ion the case of a regular field
     */
    public void setFieldData(long nodes, int[] dims)
    {
        boolean skip = this.nNodes == nodes;
        if (dims != null && this.dims == null || dims == null && this.dims != null)
            skip = false;
        if (dims != null && this.dims != null) {
            if (dims.length != this.dims.length)
                skip = false;
            else
                for (int i = 0; i < dims.length; i++)
                    if (dims[i] != this.dims[i])
                        skip = false;
        }
        if (skip)
            return;
        stableRandomAvailable = nodes < MAX_SHUFFLED_NODES;
        if (this.nNodes != nodes) {
            shuffledIndices = null;
            sampledIndices  = null;
        }

        if (dims != null)
            nDims =dims.length;
        else
            nDims = -1;
        this.nNodes = nodes;
            setDefaultDownsize();
        this.dims = dims;
        if (dims != null) {
            if (policy == ALWAYS_RESET)
                setDefaultDown(dims);
            else {
                nNodes = 1;
                regularAvailable = true;
                for (int dim : dims)
                    nNodes *= dim;
                boolean retain = true;
                if (down == null || down.length != dims.length)
                    retain = false;
                else
                    for (int i = 0; i < dims.length; i++)
                        if (dims[i] < 2 * down[i])
                            retain = false;
                if (retain) {
                    long nDown = nNodes;
                    for (int d : down)
                        nDown /= d;
                    if (nDown < minSize || nDown > maxSize)
                        retain = false;
                }
                if (!retain)
                    setDefaultDown(dims);
            }
        }
        regularAvailable = dims != null;
        if (ui != null)
            ui.updateFromParams();
        fireStateChanged();
    }

    void setDown(long size) {
        if (size < minSize || size > maxSize)
            return;
        down = new int[dims.length];
        nNodes = 1;
        for (int dim : dims)
            nNodes *= dim;
        double df = FastMath.pow(nNodes / (double)size, 1./dims.length);
        for (int i = 0; i < dims.length; i++)
            down[i] = (int)FastMath.max(FastMath.min(df, dims[i] / 3), 1);
        downFactor = 1;
        for (int d : down)
            downFactor *= d;
        if (ui != null)
            ui.updateSpinners();
    }

    void setDefaultDown(int[] dims) {
        down = new int[dims.length];
        nNodes = 1;
        for (int dim : dims)
            nNodes *= dim;
        double df = FastMath.pow(nNodes / (double)preferredSize, 1./dims.length);
        double dfMax = FastMath.pow(nNodes / (double)minSize, 1./dims.length);
        double dfMin = FastMath.pow(nNodes / (double)maxSize, 1./dims.length);
        for (int i = 0; i < dims.length; i++) {
            maxDown[i] = (int)FastMath.max(FastMath.min(dfMax, dims[i] / 3), 1);
            minDown[i] = (int)FastMath.max(dfMin, 1);
            if (down[i] > maxDown[i] || down[i] < minDown[i] ||
                policy == ALWAYS_RESET)
            down[i] = (int)FastMath.max(FastMath.min(df, dims[i] / 3), 1);
        }
        minDownFactor = FastMath.max(1, (int)(nNodes / maxSize));
        maxDownFactor = (int)(nNodes / minSize);
        downFactor = 1;
        for (int d : down)
            downFactor *= d;
    }

    void setDefaultDownsize()
    {
        dims = null;
        down = null;
        minDownFactor = FastMath.max(1, (int)(nNodes / maxSize));
        maxDownFactor = (int)(nNodes / minSize);
        if (downFactor > maxDownFactor ||
            downFactor < minDownFactor ||
            policy == ALWAYS_RESET)
            downFactor = FastMath.max(1, (int)(nNodes / preferredSize));
    }

    int getnDims()
    {
        return nDims;
    }

    /**
     * getter of downsize factors for regular field downsize
     * @return current downsize factors
     */
    public int[] getDown()
    {
        return down;
    }

    /**
     * getter of minimal downsize factors for regular field downsize
     * @return  minimal downsize factors for the current field dims
     */
    public int[] getMinDown()
    {
        return minDown;
    }

    /**
     * getter of maximal downsize factors for regular field downsize
     * @return  maximal downsize factors for the current field dims
     */
    public int[] getMaxDown()
    {
        return maxDown;
    }

    /**
     * setter of downsize factors for regular field downsize
     * @param down new downsize factors
     */
    public void setDown(int[] down)
    {
        this.down = down;
        if (ui != null)
            ui.updateFromParams();
    }

    /**
     * getter for preferredSize value
     * @return preferredSize value
     */
    public long getPreferredSize()
    {
        return preferredSize;
    }

    /**
     * getter for minSize value
     * @return minSize value
     */
    public long getMinSize()
    {
        return minSize;
    }

    /**
     * getter for maxSize value
     * @return maxSize value
     */
    public long getMaxSize()
    {
        return maxSize;
    }

    /**
     * setter of preferredSize
     * @param preferredSize new preferredSize value
     */
    public void setPreferredSize(long preferredSize)
    {
        this.preferredSize = preferredSize;
        if (dims != null)
            setDown(preferredSize);
        fireStateChanged();
    }

    /**
     * setter of maxSize
     * @param maxSize new maxSize value
     */
    public void setMaxSize(long maxSize)
    {
        this.maxSize = maxSize;
        if (dims != null) {
            double dfMin = FastMath.pow(nNodes / (double)maxSize, 1./dims.length);
            for (int i = 0; i < dims.length; i++)
                minDown[i] = (int)FastMath.max(dfMin, 1);
        }
    }

    /**
     * setter for sample size values
     * @param minSize new minimal sample size
     * @param maxSize new maximal sample size
     * @param preferredSize  new preferred sample size
     */
    public void setSizes(long minSize, long maxSize, long preferredSize)
    {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.preferredSize = preferredSize;
        if (dims != null)
            setDown(preferredSize);
        fireStateChanged();
    }

    @Deprecated
    public int getDownFactor()
    {
        return downFactor;
    }

    @Deprecated
    public int getMinDownFactor()
    {
        return minDownFactor;
    }

    @Deprecated
    public int getMaxDownFactor()
    {
        return maxDownFactor;
    }

    @Deprecated
    public void setDownFactor(int downFactor)
    {
        this.downFactor = downFactor;
        fireStateChanged();
    }

    /**
     * getter for value of random
     * @return true if random sampling is selected
     */
    public boolean isRandom()
    {
        return random;
    }

    /**
     * setter for value of random
     * @param random set true when regular downsize of a regular field is desired
     */
    public void setRandom(boolean random)
    {
        this.random = random;
        fireStateChanged();
    }

    /**
     * getter for shuffled indices of a small field
     * @return shuffled indices of a small field.
     */
    public int[] getShuffledIndices()
    {
        if (stableRandomAvailable && shuffledIndices == null)
            shuffledIndices = shuffle((int)nNodes);
        return shuffledIndices;
    }

    /**
     * getter for sampled indices of a large field
     * @return sampled indices of a large field.
     */
    public long[] getSampledIndices()
    {
        if (!stableRandomAvailable && sampledIndices == null)
           sampledIndices  = sample(nNodes);
        return sampledIndices;
    }

    void drawStartIndex()
    {
        if (shuffledIndices != null)
            startIndex = (int)(shuffledIndices.length * FastMath.random());
        if (sampledIndices != null)
            startIndex = (int)(sampledIndices.length * FastMath.random());
        fireStateChanged();
    }

    /**
     * getter of the starting index of the shuffled/sampled indices sequence
     * @return starting index
     */
    public int getStartIndex()
    {
        return startIndex;
    }

    /**
     * getter for user selected value of the centered boolean variable (used in regular field downsizing)
     * @return true sample indices centering is selected
     */
    public boolean isCentered()
    {
        return centered;
    }

    void setCentered(boolean centered)
    {
        this.centered = centered;
        fireStateChanged();
    }

    /**
     * writes non-transient values to an array of strings
     * @return values for reproducing parameters (up to the random shuffling/sampling of the field indices
     */
    public String[] valuesToStringArray()
    {
        if (dims != null && down != null && !random) {
            switch (down.length) {
                case 1:
                    return new String[] {"random: false",
                                         String.format("down: {%3d}",
                                                       down[0]),
                                         String.format("centered: %b", centered)};
                case 2:
                    return new String[] {"random: false",
                                         String.format("down: {%3d %3d}",
                                                        down[0], down[1]),
                                         String.format("centered: %b", centered)};
                case 3:
                    return new String[] {"random: false",
                                         String.format("down: {%3d %3d %3d}",
                                                        down[0], down[1], down[2]),
                                         String.format("centered: %b", centered)};
            }
        }
        return new String[] {String.format("random: %b", random),
                             String.format("downsize: %6d", downFactor)};
    }

    /**
     * restores non-transient parameters from an array of strings
     * @param input String array written by the valuesToStringArray() method
     */
    public void restoreFromStringArray(String[] input)
    {
        if (input == null || input.length < 2)
            return;
        try {
            String[] tokens;
            for (String in : input)
                if (in != null && !in.isEmpty()){
                    tokens = in.trim().toLowerCase().split(" *: +");
                    if (tokens[0].equalsIgnoreCase("random"))
                        random = tokens[1].charAt(0) == 't';
                    else if (tokens[0].equalsIgnoreCase("downsize"))
                        downFactor = Integer.parseInt(tokens[1]);
                    else if (tokens[0].equalsIgnoreCase("down")){
                        String t = tokens[1].replaceAll("\\{|\\}", "");
                        tokens = t.trim().split(" +");
                        down = new int[tokens.length];
                        for (int i = 0; i < down.length; i++)
                            down[i] = Integer.parseInt(tokens[i]);
                    }
                    else if (tokens[0].equalsIgnoreCase("centered"))
                        centered = tokens[1].charAt(0) == 't';
            }
            if (ui != null)
                ui.updateFromParams();
        } catch (NumberFormatException e) {
        }
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    protected void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener listener : changeListenerList)
                listener.stateChanged(e);
        }
    }

    /**
     * delivers randomly shuffled array of requested size
     * @param size length f the output array
     * @return array of shuffled <code> size </code> integers 0, ..., <code> size - 1 </code>
     */
    public static final int[] shuffle(int size)
    {
        int[] ind = new int[size];
        for (int i = 0; i < ind.length; i++)
            ind[i] = i;
        ArrayUtils.shuffle(ind);
        return ind;
    }

    /**
     * delivers randomly sampled array of longs of requested size
     * @param size length f the output array
     * @return array of sampled <code> size </code> longs 0, ..., <code> size - 1 </code>
     */
    public static final long[] sample(long size)
    {
        long[] sample = new long[(int)FastMath.min(size, MAX_SHUFFLED_NODES)];
        for (int i = 0; i < sample.length; i++)
            sample[i] = (long)((size - i) * FastMath.random());
        return sample;
    }

}
