/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.array;

import java.util.function.Predicate;
import javax.swing.event.ChangeEvent;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.ALWAYS_RESET;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.KEEP_IF_INSIDE;
import org.visnow.vn.lib.utils.Range;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentValuesArray extends ComponentFeature
{

    protected float[] physicalValues = {0};
    protected int preferredCount = 1;
    protected boolean startSingle = true;
    protected ComponentValuesArrayUI valUI = null;

    public ComponentValuesArray()
    {
        super();
    }

    public ComponentValuesArray(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                          boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull, boolean continuousUpdate,
                          PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(pseudoComponentsAllowed, preferedItemNames,
              numericsOnly, scalarsOnly, vectorsOnly, addNull, prefereNull,
              continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentValuesArray(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }
    
    
    /**
     * Standard constructor with all behavior definable
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param pseudoComponentsAllowed  true if indices and coordinates are allowed (see e.g. texture coordinates
     * @param preferedItemNames        some general component names can be preferred as initial selection
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentValuesArray(Predicate<DataArraySchema> schemaAcceptor,
                            boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this.schemaAcceptor = schemaAcceptor;
        this.preferedItemNames = preferedItemNames;
        this.pseudoComponentsAllowed = pseudoComponentsAllowed;
        this.addNull = addNull;
        this.prefereNull = prefereNull;
        this.continuousUpdate = continuousUpdate;
        this.preferredExtentChangeBehavior = preferredExtentChangeBehavior;
    }

    /**
     * Simplified constructor without preferred component names selection and without pseudocomponents
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentValuesArray(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(schemaAcceptor, false, null, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }
    
     /**
     * Simplified constructor with proper components, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentValuesArray(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(schemaAcceptor, addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }
    
     /**
     * Simplified constructor with proper components, no null component allowed, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentValuesArray(Predicate<DataArraySchema> schemaAcceptor, 
                            boolean continuousUpdate)
    {
        this(schemaAcceptor, false, false, false);
    }

      /**
     * Simplest constructor setting lambda only; sliders in subclasses will remain silent when dragged
     * @param schemaAcceptor  lambda schema -> schema acceptable containing all component selection details
     */
   public ComponentValuesArray(Predicate<DataArraySchema> schemaAcceptor)
    {
        this(schemaAcceptor, false);
    }
    

    public int getPreferredCount()
    {
        return preferredCount;
    }

    public void setPreferredCount(int preferredCount)
    {
        this.preferredCount = preferredCount;
        if (valUI != null)
            valUI.setStartSingle(preferredCount <= 1);
    }

    public ComponentValuesArray preferredCount(int preferredCount)
    {
        this.preferredCount = preferredCount;
        if (valUI != null)
            valUI.setPreferredCount(preferredCount);
        return this;
    }

    public boolean isStartSingle()
    {
        return startSingle;
    }

    public void setStartSingle(boolean startSingle)
    {
        this.startSingle = startSingle;
    }

    public ComponentValuesArray startSingle(boolean startSingle)
    {
        this.startSingle = startSingle;
        if (valUI != null)
            valUI.setStartSingle(startSingle);
        return this;
    }

    public float[] getPhysicalValues()
    {
        return physicalValues;
    }

    public void setPhysicalValues(float[] physicalValues)
    {
        if (componentSchema == null)
            return;
        this.physicalValues = physicalValues;
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setPhysicalValuesFromUI(float[] values)
    {
        if (componentSchema == null)
            return;
        physicalValues = values;
        fireStateChanged();
    }

    public float[] getValues()
    {
        if (componentSchema == null || physicalValues == null)
            return null;
        double coeff = (componentSchema.getMaxValue() -     componentSchema.getMinValue()) /
                       (componentSchema.getPhysMaxValue() - componentSchema.getPhysMinValue());
        double add   =  componentSchema.getMinValue() - coeff * componentSchema.getPhysMinValue();
        float[] values = new float[physicalValues.length];
        for (int i = 0; i < values.length; i++)
            values[i] = (float)(coeff * physicalValues[i] + add);
        return values;
    }

    @Override
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        fireOnUpdate = false;
        super.setContainerSchema(containerSchema);
        fireOnUpdate = true;
        if (componentSchema != null)
            fireStateChanged();
    }

    @Override
    public void setContainer(DataContainer container)
    {
        setContainerSchema(container.getSchema());
    }

    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            reset();
    }

    @Override
    protected void localUpdateUI()
    {
        if (ui != null && ui instanceof ComponentValuesArrayUI) {
            valUI = (ComponentValuesArrayUI) ui;
            valUI.setStartSingle(startSingle);
            valUI.setPreferredCount(preferredCount);
            valUI.setListener(
                    (ChangeEvent e) -> {
                        if (componentSchema == null || valUI == null || valUI.isAdjusting() && !continuousUpdate)
                            return;
                        physicalValues = valUI.getValues();
                        fireStateChanged();
                    });
            valUI.setStartSingle(startSingle);
        }
    }

    @Override
    public void setComponentSchema(String componentName)
    {
        super.setComponentSchema(componentName);
        updateComponentValuesArray();
    }

    public void updateComponentValuesArray()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            reset();
        switch (preferredExtentChangeBehavior)
        {
        case ALWAYS_RESET:
            reset();
            break;
        case KEEP_IF_INSIDE:
//            if (physicalValue < componentSchema.getPhysMin() ||
//                physicalValue > componentSchema.getPhysMax())
//                physicalValue = (componentSchema.getPhysMax() + componentSchema.getPhysMin()) / 2;
            break;
        case ALWAYS_KEEP:
            break;
        }
        fireStateChanged();
    }


    @Override
    public void reset()
    {
        if (componentSchema != null)
        {
            if (preferredCount == 1)
                physicalValues = new float[]{(float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2};
            else
                physicalValues = Range.createLinearRange(preferredCount, (float)componentSchema.getPreferredPhysMinValue(), (float)componentSchema.getPreferredPhysMaxValue());
            if (valUI != null)
            {
                valUI.setMinMax(componentPhysMin, componentPhysMax);
                valUI.updateValue();
            }
        }
    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "[componentArray][/componentArray]";
        StringBuilder strB = new StringBuilder("[componentArray] "+componentSchema.getName() + ":");
        for (int i = 0; i < physicalValues.length; i++)
            strB.append(String.format(" %7.3f", physicalValues[i]));
        return strB.toString() +
               " [/componentArray]";
    }

    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty() ||
           !s.startsWith("[componentArray]") || !s.endsWith("[/componentArray]"))
            return;
        String[] c = s.substring("[componentArray]".length(), s.length() - "[/componentArray]".length()).trim().split(" *:* +");
        setComponentSchema(c[0]);
        float[] vals = new float[c.length - 1];
        for (int i = 0; i < vals.length; i++)
            vals[i] = Float.parseFloat(c[i + 1]);
        setPhysicalValues(vals);
    }

}
