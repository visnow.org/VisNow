/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.scale;

import java.awt.BorderLayout;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.gui.swingwrappers.UserActionAdapter;
import org.visnow.vn.gui.swingwrappers.UserEvent;
import org.visnow.vn.gui.widgets.ExtendedSlider;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI;

/**
 *
 * @author know
 */
public class ComponentScaleUI extends ComponentFeatureUI
{
    protected ExtendedSlider valueSlider = new ExtendedSlider();
    protected ComponentScale param;

    public ComponentScaleUI()
    {
        super();
        valueSlider.setScaleType(ExtendedSlider.ScaleType.LOGARITHMIC);
        add(valueSlider, BorderLayout.CENTER);
        valueSlider.addUserActionListener(new UserActionAdapter()
        {

            @Override
            public void userChangeAction(UserEvent event)
            {
                valChanged();
            }

        });
    }

    public void setComponentScale(ComponentScale param)
    {
        userUpdate = false;
        this.param = param;
        super.setComponentFeature(param);
        param.setUI(this);
        valueSlider.setMin(param.value / param.getScaleRange());
        valueSlider.setMax(param.value * param.getScaleRange());
        userUpdate = true;
    }

    public boolean isAdjusting()
    {
        return valueSlider.isAdjusting();
    }

    void setRange(float min, float max)
    {
        userUpdate = false;
        valueSlider.setMin(min);
        valueSlider.setMax(max);
        userUpdate = true;
    }

    void updateRange()
    {
        userUpdate = false;
        if (param != null) {
            valueSlider.setMin(param.getInitValue() / param.getScaleRange());
            valueSlider.setMax(param.getInitValue() * param.getScaleRange());
        }
        userUpdate = true;
    }

    void updateValue()
    {
        userUpdate = false;
        if (param != null)
            valueSlider.setValue(param.getValue());
        userUpdate = true;
    }

    FloatValueModificationListener listener = null;

    public void setListener(FloatValueModificationListener listener)
    {
        this.listener = listener;
    }

    public void clearListener()
    {
        listener = null;
    }

    private void valChanged()
    {
        if (listener != null && userUpdate)
            listener.floatValueChanged(new FloatValueModificationEvent(this, valueSlider.getValue().floatValue(), 
                                                                             valueSlider.isAdjusting()));
    }
    
    @Override
    protected void updateUIToNewComponent(boolean isNull)
    {
        valueSlider.setEnabled(!isNull);
    }


}
