/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MultiArrayInt
{

    private int[] dims;
    private int[] dims2;
    private int[] data;

    public MultiArrayInt(int[] dims)
    {
        this.dims = dims;
        this.dims2 = new int[dims.length];
        int size = 1;
        for (int i = 0; i < dims.length; i++) {
            dims2[i] = size;
            size *= dims[i];
        }
        this.data = new int[size];
    }

    public MultiArrayInt(int[] dims, int[] data)
    {
        this.dims = dims;
        this.dims2 = new int[dims.length];
        int size = 1;
        for (int i = 0; i < dims.length; i++) {
            dims2[i] = size;
            size *= dims[i];
        }
        this.data = data;
    }

    public void set(int[] data)
    {
        this.data = data;
    }

    public void set(int value, int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        data[p] = value;
    }

    public int get(int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        return data[p];
    }

    public int getIndex(int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        return p;
    }

    public int getOffsets(int index, int... offsets)
    {
        int p = index;
        for (int i = 0; i < offsets.length; i++) {
            p += dims2[i] * offsets[i];
        }
        return data[p];
    }

    public void setOffsets(int value, int index, int... offsets)
    {
        int p = index;
        for (int i = 0; i < offsets.length; i++) {
            p += dims2[i] * offsets[i];
        }
        data[p] = value;
    }

    public int getNextIndex(int index, int dim)
    {
        return index + dims2[dim];
    }

    public int getPrevIndex(int index, int dim)
    {
        return index - dims2[dim];
    }

    public int getNext(int index, int dim)
    {
        return data[index + dims2[dim]];
    }

    public int getPrev(int index, int dim)
    {
        return data[index - dims2[dim]];
    }

    @Override
    public String toString()
    {
        String s = "";
        for (int i = 0; i < data.length; i++) {
            s = s + " " + data[i];
            for (int j = 0; j < dims.length; j++) {
                if ((i + 1) % dims2[j] == 0) {
                    s += "|";
                }
            }
        }
        return s;
    }

    public static void main(String argv[])
    {
        int[] dims = {2, 4};
        MultiArrayInt mai = new MultiArrayInt(dims);
        mai.set(9, 0, 1);
        mai.set(10, 0, 2);
        mai.set(11, 0, 3);
        mai.set(12, 1, 2);
        System.out.println(mai);
        int index = mai.getIndex(0, 2);
        System.out.println(mai.getNext(index, 0));
    }
}
