/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.colormap2d.shape;

import org.visnow.vn.lib.basic.utilities.ShapeColorMapEditor.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import javax.swing.ImageIcon;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class EllipseDrawModel extends ShapeDrawModel
{

    protected int centerX, centerY;
    protected int radiusA, radiusB;
    protected int width;
    protected Color color;
    private static Cursor cursor1, cursor2;
    private GeneralPath gp;
    private String name;

    private void updateGeneralPath()
    {
        gp = new GeneralPath();

        double CtrlVal = 0.5522847498307933;
        double pcv = 0.5 + CtrlVal * 0.5;
        double ncv = 0.5 - CtrlVal * 0.5;
        float oX = centerX - radiusA - width;
        float oY = centerY - radiusB - width;
        float oW = 2 * (radiusA + width);
        float oH = 2 * (radiusB + width);
        float iX = centerX - radiusA + width;
        float iY = centerY - radiusB + width;
        float iW = 2 * (radiusA - width);
        float iH = 2 * (radiusB - width);
        gp.moveTo(oX + oW, oY + oH / 2);
        gp.curveTo(oX + oW, oY + pcv * oH, oX + pcv * oW, oY + oH, oX + 0.5 * oW, oY + oH);
        gp.curveTo(oX + ncv * oW, oY + 1 * oH, oX, oY + pcv * oH, oX, oY + 0.5 * oH);
        if (radiusA > width) {
            gp.lineTo(iX, centerY);
            gp.curveTo(iX + 0 * iW, iY + pcv * iH, iX + ncv * iW, iY + 1 * iH, iX + 0.5 * iW, iY + 1 * iH);
            gp.curveTo(iX + pcv * iW, iY + 1 * iH, iX + 1 * iW, iY + pcv * iH, iX + 1 * iW, iY + 0.5 * iH);
        }
        gp.closePath();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
        propertyChangeSupport.firePropertyChange("showCP", null, name);
    }

    public void updatePts()
    {
        int pts2[] = {getCenterX(), 0, getCenterX() - getRadiusA(), 0, getCenterX() - getRadiusA() - getWidth(), 0, getCenterX(), getRadiusB()};
        this.pts = pts2;
        propertyChangeSupport.firePropertyChange("pts", null, pts);
    }

    public EllipseDrawModel(String name, int centerX, int centerY, int radiusA, int radiusB, int width, Color color)
    {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radiusA = radiusA;
        this.radiusB = radiusB;
        this.width = width;
        this.color = color;

        if (cursor1 == null) {
            cursor1 = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/cursor_move_horiz.gif")).getImage(),
                new Point(0, 0),
                "Crayon");
        }
        if (cursor2 == null) {
            cursor2 = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/cursor_move_vert.gif")).getImage(),
                new Point(0, 0),
                "Crayon");
        }

        this.show = true;
        this.showCP = true;
        this.showOutline = true;
        this.name = name;
        updatePts();
        updateGeneralPath();
    }

    public void paintShapeOutline(Graphics2D g2d)
    {
        if (showOutline) {
            g2d.setColor(Color.black);
            Ellipse2D mE = new Ellipse2D.Float(getCenterX() - getRadiusA(), -getRadiusB(), 2 * getRadiusA(), 2 * getRadiusB());

            g2d.setStroke(new BasicStroke(1f));
            g2d.draw(mE);
            g2d.setStroke(new BasicStroke(
                0.5f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL,
                0,
                new float[]{3},
                0));
            g2d.draw(gp);
        }
    }

    public void paintShape(Graphics2D g2d, PaintMode paintMode)
    {
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (show) {
            switch (paintMode) {
                case ALPHA:
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    g2d.setStroke(new BasicStroke(2f));
                    g2d.setColor(new Color(color.getAlpha(), color.getAlpha(), color.getAlpha(), 255));
                    g2d.fill(gp);
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    break;
                case COLOR:
                    g2d.setStroke(new BasicStroke(1f));
                    g2d.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue()));
                    g2d.fill(gp);
                    break;
                case COLOR_ALPHA:
                    g2d.setStroke(new BasicStroke(1f));
                    g2d.setColor(getColor());
                    g2d.fill(gp);
                    break;
            }
        }
    }

    public void mouseDragged(MouseEvent e)
    {
        switch (selectedCP) {
            case 0:
                setCenterX(e.getX());
                break;
            case 1:
                setRadiusA(getCenterX() - e.getX());
                break;
            case 2:
                setWidth(getCenterX() - getRadiusA() - e.getX());
                break;
            case 3:
                setRadiusB(e.getY());
                break;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {
        if (showCP) {

            ShapePanel ep = (ShapePanel) e.getSource();
            if (e.getPoint().distance(pts[0], pts[1]) < 3 ||
                e.getPoint().distance(pts[2], pts[3]) < 3 ||
                e.getPoint().distance(pts[4], pts[5]) < 3) {
                ep.setCursor(cursor1);
            } else if (e.getPoint().distance(pts[6], pts[7]) < 3) {
                ep.setCursor(cursor2);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e)
    {

        if (showCP) {
            ShapePanel ep = (ShapePanel) e.getSource();
            for (int i = 0; i < pts.length / 2; i++) {
                if (e.getPoint().distance(pts[2 * i], pts[2 * i + 1]) < 3) {
                    selectedCP = i;
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        selectedCP = -1;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("color", null, color);
    }

    public int getCenterX()
    {
        return centerX;
    }

    public void setCenterX(int centerX)
    {
        this.centerX = centerX;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("centerX", null, centerX);
    }

    public int getCenterY()
    {
        return centerY;
    }

    public void setCenterY(int centerY)
    {
        this.centerY = centerY;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("centerY", null, centerX);
    }

    public int getRadiusA()
    {
        return radiusA;
    }

    public void setRadiusA(int radiusA)
    {
        if (radiusA < 0) {
            radiusA = 0;
        }
        this.radiusA = radiusA;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("radiusA", null, centerX);
    }

    public int getRadiusB()
    {
        return radiusB;
    }

    public void setRadiusB(int radiusB)
    {
        if (radiusB < 0) {
            radiusB = 0;
        }
        this.radiusB = radiusB;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("radiusB", null, centerX);
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        if (width < 0) {
            width = 0;
        }

        this.width = width;
        updatePts();
        updateGeneralPath();
        propertyChangeSupport.firePropertyChange("width", null, centerX);
    }
}
