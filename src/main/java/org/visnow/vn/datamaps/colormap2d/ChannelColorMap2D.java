/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.colormap2d;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.DefaultColorMap;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.vn.datamaps.colormap1d.RGBChannelColorMap1D;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ChannelColorMap2D extends DefaultColorMap implements ColorMap2D
{

    private ColorMap1D yChannel;
    private ColorMap1D xChannel;
    private final PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
    {

        public void propertyChange(PropertyChangeEvent evt)
        {
            propertyChangeSupport.firePropertyChange(evt);
        }
    };

    public ChannelColorMap2D()
    {
        this.yChannel = new RGBChannelColorMap1D();
        this.yChannel.addPropertyChangeListener(propertyChangeListener);
        this.xChannel = new RGBChannelColorMap1D();
        this.xChannel.addPropertyChangeListener(propertyChangeListener);
    }

    public ChannelColorMap2D(String name)
    {
        this();
        this.name = name;
    }

    public ChannelColorMap2D(DefaultColorMap1D yChannel, DefaultColorMap1D xChannel)
    {
        this.yChannel = yChannel;
        this.yChannel.addPropertyChangeListener(propertyChangeListener);
        this.xChannel = xChannel;
        this.xChannel.addPropertyChangeListener(propertyChangeListener);
    }

    public ChannelColorMap2D(String name, DefaultColorMap1D yChannel, DefaultColorMap1D xChannel)
    {
        this(yChannel, xChannel);
        this.name = name;
    }

    public ColorMap1D getYChannel()
    {
        return yChannel;
    }

    public ColorMap1D getXChannel()
    {
        return xChannel;
    }

    public int[] getColorTable(int sampling)
    {
        int[] colorTable = new int[sampling * sampling];
        int[] xColorMap = yChannel.getRGBColorTable();
        int[] yColorMap = xChannel.getRGBColorTable();

        int jump = ColorMapManager.SAMPLING / sampling;
        for (int y = 0, m = 0; y < sampling; y += jump) {
            for (int x = 0; x < sampling; x += jump) {
                int r1 = (yColorMap[x] >> 16) & 0xFF;
                int g1 = (yColorMap[x] >> 8) & 0xFF;
                int b1 = (yColorMap[x] >> 0) & 0xFF;
                int r2 = (xColorMap[y] >> 16) & 0xFF;
                int g2 = (xColorMap[y] >> 8) & 0xFF;
                int b2 = (xColorMap[y] >> 0) & 0xFF;

                // TODO: mixing mode
                colorTable[m++] = (-1 << 24) + ((r1 + r2) / 2 << 16) + ((g1 + g2) / 2 << 8) + (b1 + b2) / 2;
            }
        }
        return colorTable;
    }

    public byte[] getRGBByteColorTable(int sampling)
    {
        byte[] colorTable = new byte[3 * sampling * sampling];
        int[] xColorMap = yChannel.getRGBColorTable();
        int[] yColorMap = xChannel.getRGBColorTable();

        int jump = ColorMapManager.SAMPLING / sampling;
        for (int y = 0, m = 0; y < sampling; y += jump) {
            for (int x = 0; x < sampling; x += jump, m++) {
                int r1 = (yColorMap[x] >> 16) & 0xFF;
                int g1 = (yColorMap[x] >> 8) & 0xFF;
                int b1 = (yColorMap[x] >> 0) & 0xFF;
                int r2 = (xColorMap[y] >> 16) & 0xFF;
                int g2 = (xColorMap[y] >> 8) & 0xFF;
                int b2 = (xColorMap[y] >> 0) & 0xFF;

                // TODO: mixing mode
                colorTable[3 * m] = (byte) (0xff & (r1 + r2) / 2);
                colorTable[3 * m + 1] = (byte) (0xff & (g1 + g2) / 2);
                colorTable[3 * m + 2] = (byte) (0xff & (b1 + b2) / 2);
            }
        }
        return colorTable;
    }

    public byte[] getARGBByteColorTable(int sampling)
    {
        byte[] colorTable = new byte[4 * sampling * sampling];
        int[] xColorMap = yChannel.getRGBColorTable();
        int[] yColorMap = xChannel.getRGBColorTable();

        int jump = ColorMapManager.SAMPLING / sampling;
        for (int y = 0, m = 0; y < sampling; y += jump) {
            for (int x = 0; x < sampling; x += jump, m++) {
                int r1 = (yColorMap[x] >> 16) & 0xFF;
                int g1 = (yColorMap[x] >> 8) & 0xFF;
                int b1 = (yColorMap[x] >> 0) & 0xFF;
                int r2 = (xColorMap[y] >> 16) & 0xFF;
                int g2 = (xColorMap[y] >> 8) & 0xFF;
                int b2 = (xColorMap[y] >> 0) & 0xFF;

                // TODO: mixing mode
                colorTable[4 * m + 1] = (byte) (0xff & (r1 + r2) / 2);
                colorTable[4 * m + 2] = (byte) (0xff & (g1 + g2) / 2);
                colorTable[4 * m + 3] = (byte) (0xff & (b1 + b2) / 2);
            }
        }
        return colorTable;
    }

    public int[] getColorTable()
    {
        return getColorTable(ColorMapManager.SAMPLING);
    }

    public BufferedImage createPreviewFromCache()
    {

        int[] colorTable = getColorTable(ColorMapManager.PREVIEW_SIZE);
        BufferedImage previewBI = new BufferedImage(ColorMapManager.PREVIEW_SIZE, ColorMapManager.PREVIEW_SIZE, BufferedImage.TYPE_INT_ARGB);
        previewBI.setRGB(0, 0, ColorMapManager.PREVIEW_SIZE, ColorMapManager.PREVIEW_SIZE, colorTable, 0, ColorMapManager.PREVIEW_SIZE);
        return previewBI;
    }

    @Override
    public int[] getRGBColorTable()
    {
        return getColorTable(ColorMapManager.SAMPLING);
    }

    @Override
    public int[] getARGBColorTable()
    {
        return getColorTable(ColorMapManager.SAMPLING);
    }

    @Override
    public byte[] getRGBByteColorTable()
    {
        return getRGBByteColorTable(ColorMapManager.SAMPLING);
    }

    @Override
    public byte[] getARGBByteColorTable()
    {
        return getARGBByteColorTable(ColorMapManager.SAMPLING);
    }

}
