/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.vn.datamaps.colormap2d.ColorMap2D;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ColorMapComboboxModel implements ComboBoxModel
{

    private int dim = 1;
    private int index = 0;

    public void setSelectedItem(Object anItem)
    {
        switch (dim) {
            case 1:
                index = ColorMapManager.getInstance().getColorMap1DIndex((DefaultColorMap1D) anItem);
                break;
            case 2:
            default:
                index = ColorMapManager.getInstance().getColorMap2DIndex((ColorMap2D) anItem);
        }
        if (index < 0)
            index = 0;
    }

    public Object getSelectedItem()
    {
        switch (dim) {
            case 1:
                return ColorMapManager.getInstance().getColorMap1D(index);
            case 2:
            default:
                return ColorMapManager.getInstance().getColorMap2D(index);
        }
    }

    public int getSize()
    {
        switch (dim) {
            case 1:
                return ColorMapManager.getInstance().getColorMap1DCount();
            case 2:
            default:
                return ColorMapManager.getInstance().getColorMap2DCount();
        }
    }

    public Object getElementAt(int index)
    {
        switch (dim) {
            case 1:
                return ColorMapManager.getInstance().getColorMap1D(index);
            case 2:
            default:
                return ColorMapManager.getInstance().getColorMap2D(index);
        }
    }

    public ColorMapComboboxModel(int dim)
    {
        this.dim = dim;
        this.listDataListeners = new Vector<ListDataListener>();
        ColorMapManager.getInstance().propertyChangeSupport.addPropertyChangeListener(new PropertyChangeListener()
        {
            public void propertyChange(PropertyChangeEvent evt)
            {
                fireListDataListeners();
            }
        });
    }

    final protected Vector<ListDataListener> listDataListeners;

    public void fireListDataListeners()
    {
        for (ListDataListener l : listDataListeners) {
            switch (dim) {
                case 1:
                    l.intervalAdded(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, ColorMapManager.getInstance().getColorMap1DCount()));
                    break;
                case 2:
                    l.intervalAdded(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, ColorMapManager.getInstance().getColorMap2DCount()));
            }
        }
    }

    public void addListDataListener(ListDataListener l)
    {
        listDataListeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        listDataListeners.remove(l);
    }
}
