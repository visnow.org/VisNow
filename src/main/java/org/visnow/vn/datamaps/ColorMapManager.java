/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps;

import java.awt.Color;
import java.beans.PropertyChangeSupport;
import java.util.Random;
import java.util.Vector;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.vn.datamaps.colormap1d.RGBChannelColorMap1D;
import org.visnow.vn.datamaps.colormap2d.ChannelColorMap2D;
import org.visnow.vn.datamaps.colormap2d.ColorMap2D;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ColorMapManager
{

    public static final int COLORMAP1D_RAINBOW = 0;
    public static final int COLORMAP1D_GRAY = 1;
    public static final int COLORMAP1D_HOT = 2;
    public static final int COLORMAP1D_BICOLOR = 3;
    public static final int COLORMAP1D_TRICOLOR = 4;
    public static final int COLORMAP1D_COOL_WARM = 5;
    public static final int COLORMAP1D_HOT_1 = 6;
    public static final int COLORMAP1D_BLUE_WHITE_RED = 7;
    public static final int COLORMAP1D_BLUE_BLACK_RED = 8;
    public static final int COLORMAP1D_CT = 9;
    public static final int COLORMAP1D_GEOGRAPHICAL_LAND = 10;
    public static final int COLORMAP1D_GEOGRAPHICAL = 11;
    public static final int COLORMAP1D_BLUE_RED_YELLOW = 12;
    public static final int COLORMAP1D_FULLRAINBOW = 13;
    public static final int COLORMAP1D_RGB = 14;
    public static final int COLORMAP1D_BLACK_RED = 15;
    public static final int COLORMAP1D_BLACK_GREEN = 16;
    public static final int COLORMAP1D_BLACK_BLUE = 17;
    public static final int MEDICAL = 18;
    public final static int PREVIEW_SIZE = 32;
    public final static int SAMPLING_TABLE = 256;
    public final static int SAMPLING = SAMPLING_TABLE - 1;
    private static ColorMapManager singleton;
    private Vector<DefaultColorMap1D> colorMaps1D;
    private Vector<ColorMap2D> colorMaps2D;
    final public PropertyChangeSupport propertyChangeSupport;

    private void initStandardColormaps1D()
    {
        // rainbow
        {
            float[] pos = {
                .0f, .25f, .5f, .75f, 1.f
            };
            Color[] colors = {
                new Color(0.f, 0.f, 1.f),
                new Color(0.f, 1.f, 1.f),
                new Color(0.f, 1.f, 0.f),
                new Color(1.f, 1.f, 0.f),
                new Color(1.f, 0.f, 0.f),};
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Rainbow", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // gray
        {
            float[] pos = {
                .0f, 1.f
            };
            Color[] colors = {
                new Color(0.f, 0.f, 0.f),
                new Color(1.f, 1.f, 1.f),};
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Gray", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // hot
        {
            Color[] colors
                = {new Color(0.42f, 0.00f, 0.00f),
                   new Color(1.00f, 0.40f, 0.11f),
                   new Color(0.98f, 0.92f, 0.50f),
                   new Color(0.91f, 0.90f, 0.90f),
                   new Color(0.61f, 0.63f, 1.00f)
                };
            float[] pos = {
                0.00f, 0.35f, 0.57f, 0.76f, 1.00f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Hot", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // gray
        {
            float[] pos = {
                .0f, 1.f
            };
            Color[] colors = {
                new Color(0.f, 0.f, 0.f),
                new Color(1.f, 1.f, 1.f),};
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("bicolor", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        {
            Color[] colors = {
                new Color(0.f, 0.f, 1.f),
                new Color(1.f, 1.f, 1.f),
                new Color(1.f, 0.f, 0.f)
            };
            float[] pos = {
                0.f, .5f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("tricolor", true, pos, colors).symmetric(true);
            colorMaps1D.addElement(cm);
        }
        // hot 1
        {
            Color[] colors = {
                new Color(0.229801f, 0.298711f, 0.753689f),
                new Color(0.229801f, 0.298711f, 0.753689f),
                new Color(0.303868f, 0.406531f, 0.844953f),
                new Color(0.383017f, 0.509422f, 0.917388f),
                new Color(0.466667f, 0.604562f, 0.968154f),
                new Color(0.552956f, 0.688930f, 0.995377f),
                new Color(0.639170f, 0.759594f, 0.998154f),
                new Color(0.722194f, 0.813947f, 0.976577f),
                new Color(0.798688f, 0.849790f, 0.931685f),
                new Color(0.865400f, 0.865415f, 0.8654f),
                new Color(0.924132f, 0.827390f, 0.774502f),
                new Color(0.958846f, 0.769772f, 0.678004f),
                new Color(0.969955f, 0.694270f, 0.57937f),
                new Color(0.958007f, 0.602838f, 0.481773f),
                new Color(0.923949f, 0.497307f, 0.387976f),
                new Color(0.869184f, 0.378317f, 0.300267f),
                new Color(0.795636f, 0.241291f, 0.220523f),
                new Color(0.705669f, 0.015549f, 0.15024f)
            };
            float[] pos = new float[17];
            for (int i = 0; i < pos.length; i++)
                pos[i] = (float) i / 16f;
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("cool-warm", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // hot 1
        {
            Color[] colors = {
                new Color(0.5f, 0.f, 0.f),
                new Color(1.f, 0.f, 0.f),
                new Color(1.f, 1.f, 0.f),
                new Color(1.f, 1.f, 1.f),
                new Color(.5f, .5f, 1.f)
            };
            float[] pos = {
                0.f, .2f, .4f, .7f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Hot 1", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // blue - white - red:
        {
            Color[] colors = {
                new Color(0.f, 0.f, 1.f),
                new Color(0.f, 0.f, .5f),
                new Color(0.f, 0.f, 0.f),
                new Color(.5f, 0.f, 0.f),
                new Color(1.f, 0.f, 0.f)
            };
            float[] pos = {
                0.f, .4f, .5f, .6f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Blue - Black - Red", true, pos, colors).symmetric(true);
            colorMaps1D.addElement(cm);
        }
        // blue - black - red
        {
            Color[] colors = {
                new Color(0.f, 0.f, 1.f),
                new Color(.5f, .5f, 1.f),
                new Color(1.f, 1.f, 1.f),
                new Color(1.f, .5f, .5f),
                new Color(1.f, 0.f, 0.f)
            };
            float[] pos = {
                0.f, .4f, .5f, .6f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Blue - White - Red", true, pos, colors).symmetric(true);
            colorMaps1D.addElement(cm);
        }
        // CT
        {

            Color[] colors = {
                new Color(  0,   0,   0),
                new Color(194, 105,  85),
                new Color(194, 105,  85),
                new Color(194, 166, 115),
                new Color(194, 166, 115),
                new Color(102,   0,   0),
                new Color(153,   0,   0),
                new Color(255, 255, 255),
            };
            float[] pos = {
                0.f, .143372f, .203787f, .302976f, .344454f, .366096f, .461677f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Medical", true, pos, colors);
            colorMaps1D.addElement(cm);
        }


                
        // geographical saturated
        {
            Color[] colors = {
                new Color(153, 182, 235),
                new Color(8, 99, 68),
                new Color(20, 119, 51),
                new Color(230, 213, 127),
                new Color(160, 71, 7),
                new Color(123, 34, 31),
                new Color(112, 110, 111),
                new Color(255, 255, 255),
                new Color(255, 255, 255)};

            float[] pos = {0f, 1/256.0f, 0.104587f, 0.282569f, 0.420183f, 0.572477f, 0.691743f, 0.858716f, 1.000000f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Geo, 0-based", true, pos, colors).
                                              defaultRange(true).defaultLow(0).defaultUp(8000);
            colorMaps1D.addElement(cm);
        }
        
        // geographical land
        {
            Color[] colors = {new Color(0.33f, 0.55f, 0.26f),
                              new Color(0.44f, 0.80f, 0.00f),
                              new Color(0.71f, 1.00f, 0.36f),
                              new Color(0.81f, 0.81f, 0.29f),
                              new Color(0.90f, 0.78f, 0.20f),
                              new Color(0.96f, 0.71f, 0.43f),
                              new Color(0.59f, 0.59f, 0.59f),
                              new Color(1.00f, 1.00f, 1.00f),};
            float[] pos = {0.00f, 0.17f, 0.31f, 0.44f, 0.53f, 0.62f, 0.77f, 1.00f};

            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Geographical - land", true, pos, colors).
                                              defaultRange(true).defaultLow(0).defaultUp(8000);
            colorMaps1D.addElement(cm);
        }
        // geographical full
        {
            Color[] colors = {
                new Color(50, 100, 200), new Color(50, 150, 255), new Color(50, 255, 255), new Color(255, 255, 255),
                new Color(85, 140, 66), new Color(113, 205, 0), new Color(180, 255, 93),
                new Color(255, 255, 0), new Color(232, 198, 0), new Color(244, 195, 110),
                new Color(151, 151, 151), new Color(255, 255, 255),};
            float[] pos = {0.f, .1f, .25f, .45f,
                           .5f, .548f, .592f, .657f, .74f, .828f, .918f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Geographical", true, pos, colors).
                    symmetric(true).defaultRange(true).defaultLow(-8000).defaultUp(8000);
            colorMaps1D.addElement(cm);
        }
        
        // cmpl blue-red-yellow
        {
            float[] pos = {
                .0f, .33f, .67f, 1.f
            };
            Color[] colors = {
                new Color(0.f, 0.f, 1.f),
                new Color(1.f, 0.f, 1.f),
                new Color(1.f, 0.f, 0.f),
                new Color(1.f, 1.f, 0.f),};
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("blue-red-yellow", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // full rainbow
        {
            float[] pos = {
                .0f, .167f, .333f, .5f, .667f, .833f, 1.f
            };
            Color[] colors = {
                new Color(1.f, 0.f, 1.f),
                new Color(0.f, 0.f, 1.f),
                new Color(0.f, 1.f, 1.f),
                new Color(0.f, 1.f, 0.f),
                new Color(1.f, 1.f, 0.f),
                new Color(1.f, 0.f, 0.f),
                new Color(1.f, 0.f, 1.f),};
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Full rainbow", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // red-green-blue
        {
            Color[] colors = {
                new Color(1.f, 0.f, 0.f),
                new Color(1.f, 0.f, 0.f),
                new Color(0.f, 1.f, 0.f),
                new Color(0.f, 1.f, 0.f),
                new Color(0.f, 0.f, 1.f),
                new Color(0.f, 0.f, 1.f)
            };
            float[] pos = {
                0.f, .33f, .34f, .66f, .67f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("RGB", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // random grayscale map
        {
            Color[] colors = new Color[256];
            float[] pos = new float[256];
            Random r = new Random(1);
            for (int i = 0; i < pos.length; i++) {
                int g = Math.round(r.nextFloat()* 255);
                colors[i] = new Color(g, g, g);
                pos[i] = (float)i / 255;
            }
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Random grayscale", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // random color map
        {
            Color[] colors = new Color[256];
            float[] pos = new float[256];
            Random r = new Random(1);
            for (int i = 0; i < pos.length; i++) {
//                int g = Math.round(r.nextFloat()* 255);
                colors[i] = new Color(Math.round(r.nextFloat()* 255), Math.round(r.nextFloat()* 255), Math.round(r.nextFloat()* 255));
                pos[i] = (float)i / 255;
            }
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Random colormap", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        {
            Color[] colors = {
                new Color(0, 0, 0), new Color(255, 0, 0)
            };
            float[] pos = {
                0.f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Black-red", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        {
            Color[] colors = {
                new Color(0, 0, 0), new Color(0, 255, 0)
            };
            float[] pos = {
                0.f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Black-green", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        {
            Color[] colors = {
                new Color(0, 0, 0), new Color(0, 0, 255)
            };
            float[] pos = {
                0.f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Black-blue", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        {
            Color[] colors = {
                new Color(0, 0, 0), new Color(0, 0, 255)
            };
            float[] pos = {
                0.f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Black-blue", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
        // medical
        {

            Color[] colors = {
                new Color(0.f, 0.f, 0.f),
                new Color(.48f, .125f, .125f),
                new Color(1.f, .7f, .3f),
                new Color(1.f, 1.f, 1.f)
            };
            float[] pos = {
                0.f, .33f, .66f, 1.f
            };
            RGBChannelColorMap1D cm = new RGBChannelColorMap1D("Medical", true, pos, colors);
            colorMaps1D.addElement(cm);
        }
    }

    private void initStandardColormaps2D()
    {
        colorMaps2D.addElement(new ChannelColorMap2D("Black & White"));
    }

    protected ColorMapManager()
    {
        this.colorMaps1D = new Vector<DefaultColorMap1D>();
        this.colorMaps2D = new Vector<ColorMap2D>();
        this.propertyChangeSupport = new PropertyChangeSupport(this);

        initStandardColormaps1D();
        initStandardColormaps2D();
    }

    public static ColorMapManager getInstance()
    {
        if (singleton == null) {
            singleton = new ColorMapManager();
        }
        return singleton;
    }

    public void registerColorMap(ColorMap cm)
    {
        if (cm instanceof DefaultColorMap1D) {
            colorMaps1D.addElement((DefaultColorMap1D) cm);
        } else if (cm instanceof ColorMap2D) {
            colorMaps2D.addElement((ColorMap2D) cm);
        }
        propertyChangeSupport.firePropertyChange("ColorMaps", null, cm);
    }

    public void unregisterColorMap(ColorMap cm)
    {
        if (cm instanceof DefaultColorMap1D) {
            colorMaps1D.removeElement((DefaultColorMap1D) cm);
        } else if (cm instanceof ColorMap2D) {
            colorMaps2D.removeElement((ColorMap2D) cm);
        }
        propertyChangeSupport.firePropertyChange("ColorMaps", null, cm);
    }

    public ColorMapComboboxModel getColorMap2DListModel()
    {
        ColorMapComboboxModel colorMapComboboxModel = new ColorMapComboboxModel(2);
        return colorMapComboboxModel;
    }

    public ColorMapComboboxModel getColorMap1DListModel()
    {
        return new ColorMapComboboxModel(1);
    }

    public DefaultColorMap1D getColorMap1D(int index)
    {
        return colorMaps1D.get(index);
    }

    public ColorMap2D getColorMap2D(int index)
    {
        return colorMaps2D.get(index);
    }

    public int getColorMap1DIndex(DefaultColorMap1D colorMap1D)
    {
        return colorMaps1D.indexOf(colorMap1D);
    }

    public int getColorMap2DIndex(ColorMap2D colorMap2D)
    {
        return colorMaps2D.indexOf(colorMap2D);
    }

    public int getColorMap1DCount()
    {
        return colorMaps1D.size();
    }

    public int getColorMap2DCount()
    {
        return colorMaps2D.size();
    }
}
