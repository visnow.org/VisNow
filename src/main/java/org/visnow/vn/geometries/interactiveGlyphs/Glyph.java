/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.IndexedLineArray;
import org.jogamp.java3d.IndexedQuadArray;
import org.jogamp.java3d.TransparencyAttributes;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.W_ROT_VIS;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenColoringAttributes;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.objects.generics.OpenTransparencyAttributes;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
abstract public class Glyph extends OpenBranchGroup
{
    protected InteractiveGlyphParams params          = null;
    protected OpenLineAttributes lineAttr            = new OpenLineAttributes();
    protected OpenColoringAttributes colorAttr       = new OpenColoringAttributes();
    protected OpenTransparencyAttributes transpAttr  = new OpenTransparencyAttributes();
    protected OpenAppearance surfApp                 = new OpenAppearance();
    protected OpenAppearance lineApp                 = new OpenAppearance();
    protected OpenShape3D lineShape                  = new OpenShape3D("lines");
    protected OpenShape3D surfShape                  = new OpenShape3D("surfaces");
    protected OpenBranchGroup  glyphGroup            = new OpenBranchGroup();
    protected IndexedLineArray glyphLines            = null;
    protected IndexedQuadArray glyphRects            = null;
    protected IndexedLineArray lines                 = null;
    protected Reper reper;
    protected GlyphType type                         = BOX;
    protected float[] glyphVerts;
    protected float[][] currentColors = InteractiveGlyphParams.LIGHT_COLORS;
    protected float[] lineColors = {0, .6f, .6f, .6f, .6f, 0, .6f, 0, .6f};
    protected int visibleWidgets = W_ROT_VIS;
    protected boolean useRadius = false;
    protected boolean showReper = true;
    
    abstract public void update();
    
    public Glyph(InteractiveGlyphParams params)
    {
        super();
        setName("glyph");
        currentColors = params.getCurrentColors();
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                lineColors[3 * i + j] = currentColors[i][j];
        this.params = params;
        lineAttr.setLineAntialiasingEnable(true);
        lineAttr.setLineWidth(1.5f);
        lineApp.setLineAttributes(lineAttr);
        colorAttr.setColor(0, 1, 0);
        lineApp.setColoringAttributes(colorAttr);
        lineShape.setAppearance(lineApp);
        transpAttr.setTransparencyMode(TransparencyAttributes.NICEST);
        transpAttr.setTransparency(.6f);
        surfApp.setTransparencyAttributes(transpAttr);
        addChild(glyphGroup);
    }
    
    public Glyph(InteractiveGlyphParams params, boolean useRadius)
    {
        this(params);
        this.useRadius = useRadius;
    }
    
    public void updateColors()
    {
        currentColors = params.getCurrentColors();
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                lineColors[3 * i + j] = currentColors[i][j];
    }

    public InteractiveGlyphParams getParams()
    {
        return params;
    }

    public GlyphType getType()
    {
        return type;
    }

    public void setShowReper(boolean showReper) {
        this.showReper = showReper;
        update();
    }
    
    public void setParams(InteractiveGlyphParams params)
    {
        this.params = params;
    }

    
}
