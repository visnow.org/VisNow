/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs.TestInteractiveGlyph;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import static org.visnow.vn.geometries.interactiveGlyphs.TestInteractiveGlyph.TestInteractiveGlyphShared.TYPE;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TestInteractiveGlyph extends OutFieldVisualizationModule
{

    
    public static InputEgg[]  inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    
    protected int mergeCounter = 0;
    protected GlyphType type;
    protected InteractiveGlyph glyph;
    protected InteractiveGlyphParams glyphParams;
    protected InteractiveGlyphGUI glyphUI;
    protected GUI computeUI = new GUI();
    protected ParameterChangeListener glyphListener = new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                System.out.print("center");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getCenter()[i]);
                System.out.println("");
                System.out.print("     u");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getU()[i]);
                System.out.println("");
                System.out.print("     v");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getV()[i]);
                System.out.println("");
                System.out.print("     w");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getW()[i]);
                System.out.println("");
            }
        };
    
    public TestInteractiveGlyph()
    {
        type        = BOX;
        glyph       = new InteractiveGlyph(type);
        glyphParams = glyph.getParams();
        glyphUI     = glyph.getComputeUI();
        glyphUI.setParams(glyphParams);
        
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                type        = parameters.get(TYPE);
                outObj.removeNode(glyph);
                glyph.setType(type);
                outObj.addNode(glyph);
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI.setParameters(parameters);
                computeUI.addUI(glyphUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        glyph.addParameterChangelistener(glyphListener);
        outObj.addNode(glyph);
    }   

    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(TYPE, BOX)
        };
    }
    
    protected FloatLargeArray fieldCoords = null;
    
    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            inField = newField;
            if (inField.hasCoords())
                fieldCoords = inField.getCoords(0);
            else
            {
                float[][] xt = inField.getExtents();
                float[] coords = new float[24];
                for (int i = 0; i < 8; i++) {
                    int l = i;
                    for (int j = 0; j < 3; j++) {
                        coords[3 * i + j] = xt[l%2][j];
                        l /= 2;
                    }
                }
                fieldCoords = new FloatLargeArray(coords);
            }
            if (0 < inField.getTrueNSpace()) {
                glyphParams.setDimension(inField.getTrueNSpace());
                computeUI.setDimension(inField.getTrueNSpace());
                if (inField.getTrueNSpace() == 3)
                    type = BOX;
                if (inField.getTrueNSpace() == 2)
                    type = RECTANGLE;
                
            }
            glyphParams.setCoords(fieldCoords);
            
            setOutputValue("outField", new VNField(inField));
            
            prepareOutputGeometry();
            outObj.addNode(glyph);
            show();
        }
    }
}
