/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.Node;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickType;

/**
 *
 * @author know
 */


public class InteractiveGlyph extends OpenBranchGroup 
        
{
    public static enum GlyphType {BOX, PLANE, LINE, SPHERE, CIRCLE, RECTANGLE, POINT, PLANAR_LINE};
    
    protected OpenBranchGroup intermediate = new OpenBranchGroup("intermediate");
    protected Node additionalGeometry =  null;
    protected Glyph glyph;
    protected InteractiveGlyphParams params = new InteractiveGlyphParams(this);
    protected InteractiveGlyphGUI computeUI = new InteractiveGlyphGUI();
    
    public InteractiveGlyph()
    {
        setName("interactiveGlyph");
        glyph = new SphereGlyph(params);
        computeUI.setParams(params);
        addChild(intermediate);
        
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                update();
            }    
        });
    }
    
    public InteractiveGlyph(GlyphType type)
    {
        switch (type) {
            case PLANE:
                glyph = new PlaneGlyph(params);
                break;
            case SPHERE:
                glyph = new SphereGlyph(params);
                break;
            case LINE:
                glyph = new LineGlyph(params);
                break;
            case RECTANGLE:
                glyph = new RectangleGlyph(params);
                break;
            case CIRCLE:
                glyph = new CircleGlyph(params);
                break;
            case POINT:
                glyph = new PointGlyph(params);
                break;
            case PLANAR_LINE:
                glyph = new PlanarLineGlyph(params);
                break;
            default:
                glyph = new BoxGlyph(params);
                break;
        }
        computeUI.setParams(params);
        intermediate.addChild(glyph);
    }
    
    public InteractiveGlyph(GlyphType type, boolean forceUseRadius)
    {
        switch (type) {
            case PLANE:
                glyph = new PlaneGlyph(params);
                break;
            case SPHERE:
                glyph = new SphereGlyph(params);
                break;
            case LINE:
                glyph = new LineGlyph(params, forceUseRadius);
                break;
            case RECTANGLE:
                glyph = new RectangleGlyph(params);
                break;
            case CIRCLE:
                glyph = new CircleGlyph(params);
                break;
            case POINT:
                glyph = new PointGlyph(params);
                break;
            case PLANAR_LINE:
                glyph = new PlanarLineGlyph(params, forceUseRadius);
                break;
            default:
                glyph = new BoxGlyph(params);
                break;
        }
        computeUI.setParams(params);
        intermediate.addChild(glyph);
    }
    
    public void setType(GlyphType type)
    {
        if (type == glyph.getType())
            return;
        if (glyph != null)
            glyph.detach();
        switch (type) {
            case PLANE:
                glyph = new PlaneGlyph(params);
                break;
            case SPHERE:
                glyph = new SphereGlyph(params);
                break;
            case LINE:
                glyph = new LineGlyph(params);
                break;
            case RECTANGLE:
                glyph = new RectangleGlyph(params);
                break;
            case CIRCLE:
                glyph = new CircleGlyph(params);
                break;
            case POINT:
                glyph = new PointGlyph(params);
                break;
            case PLANAR_LINE:
                glyph = new PlanarLineGlyph(params);
                break;
            default:
                glyph = new BoxGlyph(params);
                break;
        }
        glyph.update();
        intermediate.addChild(glyph);
        computeUI.updateWidgetVisibility();
    }
    
    public void addGeometry(Node geometry)
    {
        intermediate.addChild(geometry);
        additionalGeometry = geometry;
    }
    
    public void setColors(boolean dark)
    {
        params.darkColors(dark);
        if (glyph != null) 
            glyph.updateColors();
    }
    
    public void hide()
    {
        removeAllChildren();
    }
    
    public void show()
    {
        try {
            if (this.numChildren() < 1) {
                addChild(intermediate);
            }
            
        } catch (Exception e) {
        }
    }
    
    public boolean isAdjusting()
    {
        return params.adjusting >= 0;
    }
    
    public void setField(Field field)
    {
        params.setDimension(field.getTrueNSpace() == 2 ? 2 : 3);
        if (field.hasCoords())
            params.setCoords(field.getCoords(0));
        else
        {
            float[][] extents = field.getExtents();
            float[] llb = extents[0];
            float[] xt = new float[3];
            for (int i = 0; i < xt.length; i++)
                xt[i] = extents[1][i] - llb[i];
            float[] c = new float[24];
            for (int i = 0; i < 8; i++) {
                int[] k = {i & 1, (i >> 1) & 1, (i >> 2) & 1};
                for (int j = 0; j < 3; j++) 
                    c[3 * i + j] = llb[j] + k[j] * xt[j];
            }
            params.setCoords(new FloatLargeArray(c));
        }
    }
    
    public void update()
    {
        if (glyph != null) 
            glyph.update();
        if (params.isShow())
            show();
        else
            hide();
    }

    public Glyph getGlyph() {
        return glyph;
    }
    
    public InteractiveGlyphParams getParams() {
        return params;
    }

    public InteractiveGlyphGUI getComputeUI() {
        return computeUI;
    }
    
    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        params.addParameterChangelistener(listener);
    }

    public synchronized void clearParameterChangelisteners()
    {
        params.clearParameterChangeListeners();
    }
    
    public float[] getBoxVerts(int trueDim)
    {
        float[] boxVerts;
        float[] center = params.getCenter();
        float[][][] v = new float[2][3][3];
        for (int i = 0; i < 3; i++) {
            v[0][0][i] = params.getuRange()[0] * params.getU()[i];
            v[0][1][i] = params.getvRange()[0] * params.getV()[i];
            v[0][2][i] = params.getwRange()[0] * params.getW()[i];
            v[1][0][i] = params.getuRange()[1] * params.getU()[i];
            v[1][1][i] = params.getvRange()[1] * params.getV()[i];
            v[1][2][i] = params.getwRange()[1] * params.getW()[i];
        }
        if (trueDim == 3) {
            boxVerts = new float[24];
            for (int i = 0, m = 0; i < 2; i++) 
                for (int j = 0; j < 2; j++) 
                    for (int k = 0; k < 2; k++) 
                        for (int l = 0; l < 3; l++, m++) 
                            boxVerts[m] = center[l] + v[i][0][l] + v[j][1][l] + v[k][2][l];
        }
        else {
            boxVerts = new float[12];
            for (int i = 0, m = 0; i < 2; i++) 
                for (int j = 0; j < 2; j++)
                    for (int l = 0; l < 3; l++, m++)  
                        boxVerts[m] = center[l] + v[i][0][l] + v[j][1][l]; 
        }
        return boxVerts;       
    }
    
    public Pick3DListener getPick3DListener() {
        return pick3DListener;
    }
    
    /**
     * Pick3DListener
     * <p/>
     * No getter is required if
     * <code>parameters</code> store an object of class
     * <code>Params</code> and that object stores this Pick3DListener and overrides
     * <code>getPick3DListener()</code> method.
     */
    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            params.setCenter(e.getPoint());
        }
    };

}
