/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.LINE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class LineGlyph extends Glyph
{
    protected float[] lineColors = { 0, .5f, .5f};

    public LineGlyph(InteractiveGlyphParams params)
    {
        this(params, false);
    }

    public LineGlyph(InteractiveGlyphParams params, boolean useRadius)
    {
        super(params, useRadius);
        type = LINE;
        setName("line glyph");
        visibleWidgets = useRadius ? U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                                     U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS |
                                     SCALE_VIS   | AXES_VIS  | RADIUS_VIS
                                   : U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                                     U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS |
                                     W_RANGE_VIS |
                                     SCALE_VIS   | AXES_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {0, 0, -1,  0, 0, 1};
        lines = new IndexedLineArray(2,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 2);
        lines.setCoordinateIndices(0, new int[] { 0,  1});
        lines.setColorIndices(0, new int[]      { 0,  0});
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        lines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        lines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        lineShape.addGeometry(lines);
        glyphGroup.addChild(lineShape);
        reper = new Reper3D(params);
        addChild(reper);
    }

    @Override
    public void updateColors()
    {
        reper.updateColors(currentColors);
    }

    @Override
    public void update()
    {
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float w0 = params.wRange[0] * params.w[i];
            float w1 = params.wRange[1] * params.w[i];
            glyphVerts[     i] = c + w0;
            glyphVerts[ 3 + i] = c + w1;
        }
        lines.setCoordinates(0, glyphVerts);
        float s = params.getScale();
        reper.update(new float[]{.3f * s, .3f * s, s});
    }

}
