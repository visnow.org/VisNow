/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.PLANAR_LINE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class PlanarLineGlyph extends Glyph
{
    protected float[] lineColors = { 1, 0, 0, 0, 0, 1};


    public PlanarLineGlyph(InteractiveGlyphParams params)
    {
        super(params);
        type = PLANAR_LINE;
        setName("plaar line glyph");
        visibleWidgets = W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS |
                         U_RANGE_VIS |
                         SCALE_VIS   | X_AXIS_VIS | Y_AXIS_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1.f,    0,  0,
                                   1.f,    0,  0,
                                     0, -.3f,  0,
                                     0,  .3f,  0};
        glyphLines = new IndexedLineArray(6,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 6);
        glyphLines.setCoordinateIndices(0, new int[] {0, 1, 2, 3});
        glyphLines.setColorIndices(0, new int[] {0, 0, 1, 1});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
        lineShape.setAppearance(lineApp);
        lineShape.addGeometry(glyphLines);
        params.setAxis(0);
        glyphGroup.addChild(lineShape);
    }

    public PlanarLineGlyph(InteractiveGlyphParams params, boolean useRadius)
    {
        super(params, useRadius);
        type = PLANAR_LINE;
        setName("plaar line glyph");
        visibleWidgets = useRadius ?  W_ROT_VIS | U_TRANS_VIS | V_TRANS_VIS |
                                      SCALE_VIS   | X_AXIS_VIS | Y_AXIS_VIS | RADIUS_VIS
                                    : W_ROT_VIS | U_TRANS_VIS | V_TRANS_VIS |
                                      U_RANGE_VIS |
                                      SCALE_VIS   | X_AXIS_VIS | Y_AXIS_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1.f,    0,  0,
                                   1.f,    0,  0,
                                     0, -.3f,  0,
                                     0,  .3f,  0};
        glyphLines = new IndexedLineArray(6,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 6);
        glyphLines.setCoordinateIndices(0, new int[] {0, 1, 2, 3});
        glyphLines.setColorIndices(0, new int[] {0, 0, 1, 1});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
        lineShape.setAppearance(lineApp);
        lineShape.addGeometry(glyphLines);
        params.setAxis(0);
        glyphGroup.addChild(lineShape);
    }

    @Override
    public void update()
    {
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 3; j++)
                lineColors[3 * i + j] = currentColors[i + 1][j];
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u = params.glyphDim * params.u[i];
            float v = params.glyphDim * params.v[i];

            glyphVerts[i]      = c - u;
            glyphVerts[3 + i]  = c + u;
            glyphVerts[6 + i]  = c - .3f * v;
            glyphVerts[9 + i]  = c + .3f * v;
        }
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
    }
}
