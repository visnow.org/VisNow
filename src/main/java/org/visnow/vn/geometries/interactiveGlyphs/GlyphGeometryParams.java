/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;


/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

/**
 * container of immutable copies of interactive glyph geometry params passed to modules
 * using glyph geometry
 */
public class GlyphGeometryParams
{
    private final float[] center;  // rotation center of the glyph
    
    private final float[] u;  // current u reper versor
    private final float   shiftU;          // current center shift in the u direction
    private final float[] uMinMax;     // extreme values of the u coordinate of the field nodes
    private final float[] uRange;    // u range of the selected box
    
    private final float[] v; 
    private final float   shiftV;
    private final float[] vMinMax;
    private final float[] vRange;
    
    private final float[] w;
    private final float   shiftW;
    private final float[] wMinMax;
    private final float[] wRange;
    
    private final float   radius;
    private final float   glyphScale;

    public GlyphGeometryParams(float[] center, 
                               float[] u, float shiftU, float[] uMinMax, float[] uRange, 
                               float[] v, float shiftV, float[] vMinMax, float[] vRange, 
                               float[] w, float shiftW, float[] wMinMax, float[] wRange, 
                               float radius, float glyphScale)
    {
        this.center = center;
        this.u = u;
        this.shiftU = shiftU;
        this.uMinMax = uMinMax;
        this.uRange = uRange;
        this.v = v;
        this.shiftV = shiftV;
        this.vMinMax = vMinMax;
        this.vRange = vRange;
        this.w = w;
        this.shiftW = shiftW;
        this.wMinMax = wMinMax;
        this.wRange = wRange;
        this.radius = radius;
        this.glyphScale = glyphScale;
    }

    public float[] getCenter()
    {
        return center;
    }

    public float[] getU()
    {
        return u;
    }

    public float getShiftU()
    {
        return shiftU;
    }

    public float[] getuMinMax()
    {
        return uMinMax;
    }

    public float[] getuRange()
    {
        return uRange;
    }

    public float[] getV()
    {
        return v;
    }

    public float getShiftV()
    {
        return shiftV;
    }

    public float[] getvMinMax()
    {
        return vMinMax;
    }

    public float[] getvRange()
    {
        return vRange;
    }

    public float[] getW()
    {
        return w;
    }

    public float getShiftW()
    {
        return shiftW;
    }

    public float[] getwMinMax()
    {
        return wMinMax;
    }

    public float[] getwRange()
    {
        return wRange;
    }

    public float getRadius()
    {
        return radius;
    }

    public float getGlyphScale()
    {
        return glyphScale;
    }
    
    
}
