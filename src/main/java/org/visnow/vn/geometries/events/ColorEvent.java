/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.events;

import java.awt.Color;
import java.util.*;
import org.jogamp.vecmath.Color3f;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ColorEvent extends EventObject
{

    private Color selectedColor = null;

    /**
     * Creates a new instance of DynamicsModificationEvent
     */
    public ColorEvent(Object source, Color selectedColor)
    {
        super(source);
        this.selectedColor = selectedColor;
    }

    @Override
    public String toString()
    {
        return "ColorEvent";
    }

    public Color3f getSelectedColor3f()
    {
        float[] clr = new float[4];
        selectedColor.getComponents(clr);
        return new Color3f(clr);
    }

    public Color getSelectedColor()
    {
        return selectedColor;
    }
}
