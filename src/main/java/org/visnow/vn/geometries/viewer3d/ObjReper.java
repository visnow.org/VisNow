/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Vector3d;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;

/**
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ObjReper
    extends OpenTransformGroup
    implements ObjRotate.Listener
{

    protected Transform3D reperTransform = new Transform3D();

    public ObjReper()
    {
    }

    public ObjReper(String name)
    {
        super(name);
    }

    /**
     * Fired when scene transform has changed, e.g. scene was moved or rotated.
     * <p/>
     * @param newTransform
     */
    @Override
    public void sceneTransformChanged(Transform3D newTransform)
    {
        applyRotationFromTranslation(newTransform);
    }

    /**
     * Rotates reper according to a rotation from a transform given in parameter.
     * Used to apply to reper current rotation of a 3D scene (objRotate).
     * <p/>
     * @param newTransform A transform which rotation will be applied to reper.
     */
    protected void applyRotationFromTranslation(Transform3D newTransform)
    {

        // Remember old transform
        this.getTransform(reperTransform);
        // Compute new rotation
        Vector3d translation = new Vector3d();
        reperTransform.get(translation);
        reperTransform.setTranslation(new Vector3f(0, 0, 0));

        // now reperTransform stores only rotation
        // Apply new transform
        reperTransform.set(newTransform);
        // Restore old translation (rotation will be new);
        reperTransform.setTranslation(translation);
        this.setTransform(reperTransform);

    }


    /**
     * This method should prevent viewer from keeping huge reper in some situations, especially on
     * Mac OS X.
     * <p>
     * @see Viewer3D#kick() "kick in Viewer3D"
     */
    void refreshTransform()
    {
        Transform3D t3d = new Transform3D();
        this.getTransform(t3d);
        this.setTransform(t3d);
    }

}
