/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import org.jogamp.java3d.utils.behaviors.mouse.*;
import org.jogamp.java3d.utils.scenegraph.io.*;
import org.jogamp.java3d.utils.universe.SimpleUniverse;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.logging.Level;
import javax.swing.JFrame;
import org.jogamp.java3d.*;
import javax.swing.JInternalFrame;
import javax.swing.ToolTipManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.geometries.events.*;
import org.visnow.vn.geometries.geometryTemplates.*;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.GeometryParent;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.controls.Display3DControlsFrame;
import org.visnow.vn.geometries.viewer3d.controls.Display3DControlsPanel;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.*;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.*;
import org.visnow.vn.geometries.viewer3d.lights.*;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.utils.ImageUtils;
import org.visnow.vn.lib.utils.events.MouseRestingEvent;
import org.visnow.vn.lib.utils.events.MouseRestingListener;
import org.visnow.vn.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.MoviesManager.*;
import org.visnow.vn.geometries.viewer3d.controls.SavedTransforms;
import org.visnow.vn.geometries.viewer3d.controls.light_editor.LightsEditor;
import static org.visnow.vn.geometries.viewer3d.lights.EditableLight.LightType.*;
import org.visnow.vn.lib.utils.SwingInstancer;


/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public final class Display3DPanel
    extends javax.swing.JPanel
    implements GeometryParent, RenderingWindowInterface, CameraWindowCloseListener
{

    protected static Logger LOGGER = Logger.getLogger(Display3DPanel.class);

    protected boolean debug = VisNow.isDebug();
    // Constants for type of light to use
    protected static final int DIRECTIONAL_LIGHT = 0;
    protected static final int POINT_LIGHT = 1;
    protected static final int SPOT_LIGHT = 2;
    protected static final int TRANSFORMED_OBJECT = 0;
    protected static final int TRANSFORMED_CAMERA = 1;
    protected static final int TRANSFORMED_LIGHT = 2;
    protected static final int TRANSFORMED_CURRENT_OBJECT = 3;
    protected View view = null;
    protected static int lightType = POINT_LIGHT;
    protected SimpleUniverse universe = null;
    //
    /*
     * One of OpenBranchGroup objects below is a root for all scene objects
     */
    protected OpenBranchGroup windowRootObject = new OpenBranchGroup("objRoot");
    protected OpenBranchGroup objScene = new OpenBranchGroup("objScene");
    //
    /*
     * Transformations used to rotate, scale and translate the whole scene with
     * objects.
     */
    protected OpenTransformGroup objScale = new OpenTransformGroup("objScale");
    protected ObjRotate objRotate = new ObjRotate("objRotate");
    protected OpenTransformGroup objTranslate = new OpenTransformGroup("objTranslate");
    /**
     * For rotating the scene by mouse
     */
    protected MouseRotate mouseRotate = new MouseRotate();
    /**
     * For rotating the scene by mouse
     */
    protected double mouseRotateSensitivity = .002;

    /**
     * Reper object (3D axes in the corner of the window). Thanks to code in constructor it will be
     * automatically updated when rotating the scene.
     */
    protected ObjReper objReper = new ObjReper("objReper");
    protected OpenTransformGroup positionedReper = new OpenTransformGroup("posReper");
    protected OpenBranchGroup reperGroup = new OpenBranchGroup("reperGroup");

    /**
     * Camera
     */

    protected OpenBranchGroup cameraGroup = new OpenBranchGroup("cameraGroup");
    protected OpenBranchGroup objCamera = new OpenBranchGroup("camera");
    protected TransformCameraGroup transformObjCamera = new TransformCameraGroup("camera transform");
    protected OpenBranchGroup cameraLight = new OpenBranchGroup("camera light");
    protected Camera camera = new Camera();

    /**
     * MoviesManager
     */

    protected MoviesManager moviesManager = new MoviesManager();


    /**
     * For moving the scene by mouse
     */
    protected MouseTranslate mouseTranslate = new MouseTranslate();
    /**
     * For moving the scene by mouse
     */
    protected double mouseTranslateSensitivity = .002;
    /**
     * For zooming the scene by mouse
     */

    /**
     * For moving camera around the path
     */
    protected MouseZoom mouseZoom = new MouseZoom();
    protected double mouseScale = 1.;
    protected double externScale = 1.;
    protected double mouseWheelSensitivity = 1.02;
    protected double mouseZoomFactor = mouseZoom.getFactor();
    //
    protected Vector3d sceneCenter = new Vector3d(0., 0., 0.);

    protected SavedTransforms savedTransforms;

    /**
     * Temporary transform used locally in many methods
     */
    protected Transform3D tempTransform = new Transform3D();
    //
    //
    protected ChangeListener renderListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            boolean action = (Boolean) e.getSource();
            if (action) {
                canvas.startRenderer();
            } else {
                canvas.stopRenderer();
            }
        }
    };

    /**
     * Scene properties
     */
    protected Color3f bgColor = new Color3f(0.f, 0.f, 0.f);
    protected LinearFog myFog = new LinearFog();
    protected BoundingSphere bounds
        = new BoundingSphere(new Point3d(0., 0., 0.), 100.0);
    protected ModelClip modelClip = new ModelClip(new Vector4d[]{new Vector4d(-1, 0, 0, 1), new Vector4d(1, 0, 0, -1),
                                                                 new Vector4d(0, -1, 0, 1), new Vector4d(0, 1, 0, -1),
                                                                 new Vector4d(0, 0, -1, 1), new Vector4d(0, 0, 1, -1)},
                                                  new boolean[]{false, false, false, false, false, false});
    protected LightsEditor lightsEditor;
    protected EditableAmbientLight ambientLight = null;
    protected ArrayList<DirectionalLightObject> directionalLights;
    protected ArrayList<PointLightObject>       pointLights;
    protected Background bg = new Background();
    protected J3DGraphics2D vGraphics = null;
    //
    /**
     * Main canvas
     */
    private JCanvas3D canvas = null;
    //
    protected boolean perspective = true;
    protected StandardView lastStandardView = StandardView.TOP_VIEW;
    protected boolean renderDone = false;
    protected boolean offScreenRenderDone = false;
    protected LocalToWindow locToWin = null;
    protected BufferedImage offScreenBufferImage = null;
    
    /**
     * Object used for output view content (images and movies)
     * An off-screen canvas, used for making screenshots only.
     */
    protected Canvas3D offScreenCanvas = null;
    protected View offScreenView = null;
    protected LocalToWindow offScreenLocToWin = null;
    protected boolean storingJPEG = false;
    protected boolean storingPNG = false;
    protected boolean storingFrames = false;

    /**
     * Display control UI
     */
    private Display3DControlsFrame controlsFrame = null;
    private JFrame transientControlsFrame = null;
    protected Display3DControlsPanel controlsPanel = null;

    protected CheatSheet cheatSheet = new CheatSheet();

    protected ArrayList<Title> titles = null;
    protected RenderingParams objectDisplayParams = new RenderingParams(this);

    protected boolean standardCenterPosition = true;

    protected GeometryObject rootObject = new GeometryObject("root_object")
    {
        @Override
        public void updateExtents()
        {
            if (lockView) {
                return;
            }

            super.updateExtents();
            rootDim = 0;
            rootExtents = rootObject.getExtents();
            for (int i = 0; i < 3; i++) {
                if (rootExtents[0][i] == Float.MAX_VALUE ||
                    rootExtents[0][i] == -Float.MAX_VALUE ||
                    rootExtents[1][i] == Float.MAX_VALUE ||
                    rootExtents[1][i] == -Float.MAX_VALUE)
                    continue;
                if (rootDim < rootExtents[1][i] - rootExtents[0][i]) {
                    rootDim = rootExtents[1][i] - rootExtents[0][i];
                }
            }
            if (rootDim == 0)
                rootDim = 1;
            if (standardCenterPosition) {
                sceneCenter = new Vector3d(-(rootExtents[1][0] + rootExtents[0][0]) / 2.0,
                                           -(rootExtents[1][1] + rootExtents[0][1]) / 2.0,
                                           -(rootExtents[1][2] + rootExtents[0][2]) / 2.0);

                Transform3D tr = new Transform3D();
                tr.setTranslation(sceneCenter);
                objTranslate.setTransform(tr);
            }
            Display3DPanel.this.setScale(1.2 / rootDim);
            for (PointLightObject pointLight : pointLights)
                pointLight.updateScale(rootDim, sceneCenter);
            controlsPanel.setExtents(rootExtents);
        }
    };

    protected final ArrayList<FrameRenderedListener> frameRenderedListeners = new ArrayList<>();
    protected PickObject pickObject;
    protected boolean pick3DActive = false;
    protected ArrayList<ColorListener> bgrColorListeners = new ArrayList<>();
    protected ArrayList<LightDirectionListener> lightDirectionListeners = new ArrayList<>();
    /*
     * Stereo settings - active only if stareo view is on
     */
    protected Point3d defaultLeftEye = new Point3d();
    protected Point3d defaultRightEye = new Point3d();
    protected float eyeSeparation = .5f;
    protected Point3d leftEye = null;
    protected Point3d rightEye = null;

    protected Thread mouseObserverThread = null;
    protected boolean mouseOn = false;
    protected int timeToPopup = 5;
    protected boolean stereoActive = false;
    protected int transformedNode = TRANSFORMED_OBJECT;
    private OpenBranchGroup myFogGroup = new OpenBranchGroup();
    protected Transform3D pickTransform = new Transform3D();
    protected float[][] rootExtents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
    protected float rootDim = 1;
    private Transform3D initialCameraTransform = new Transform3D();
    protected JInternalFrame parentFrame = null;
    protected boolean moveCameraMode = false;
    protected boolean showReper = true;
    protected int reperSize = 50;
    protected GeometryObject reper = new Reper();
    protected String name;
    protected int renderCounter = 0;
    protected int swapCounter = 0;
    protected Cursor crosshairCursor;
    private Application application = null;

    private boolean showFPS = false;
    private boolean showRendererInfo = false;

    private long frameCounter = 0;
    private long frameCounterStartTime = -1;
    private long fpsMeasureTime = 1000L;
    private double currentFPS = -1;

    private boolean fullReset = false;
    private boolean lightsVisible = false;


    /**
     * Creates new form Display3DPanel
     */
    public Display3DPanel()
    {
        initComponents();
        this.setMinimumSize(new Dimension(200, 200));
        this.setPreferredSize(new Dimension(800, 600));

        GraphicsConfigTemplate3D template = new GraphicsConfigTemplate3D();
        template.setSceneAntialiasing(org.jogamp.java3d.GraphicsConfigTemplate3D.PREFERRED);

        template.setStereo(GraphicsConfigTemplate3D.UNNECESSARY);
        canvas = new JCanvas3D(template)
        {
            @Override
            public void doPostRender(Canvas3D canvas)
            {
                vGraphics = canvas.getGraphics2D();
                vGraphics.setFont(new Font("sans-serif", Font.PLAIN, 10));
                vGraphics.setColor(Color.YELLOW);
                locToWin.update();
                fireProjectionChanged(new ProjectionEvent(this, locToWin));
                draw2D(vGraphics, locToWin, this.getWidth(), this.getHeight());
                vGraphics.flush(false);
            }

            @Override
            public void doPostSwap(Canvas3D canvas)
            {
                if(frameCounterStartTime == -1)
                    frameCounterStartTime = System.currentTimeMillis();
                frameCounter++;


                if (postRenderSilent || waitForExternalTrigger) {
                    return;
                }
                if (!(storingJPEG || storingPNG || storingFrames)) {
                    fireFrameRendered();
                }
                if (storingFrames) {
                    if (storingJPEG || storingPNG) {
                        writeImage(new File(controlsPanel.getMovieCreationPanel().getCurrentFrameFileName()));
                    }
                }
            }
        };

        add("Center", canvas);
        canvas.createCanvas(this.getPreferredSize().width, this.getPreferredSize().height);

        pickObject = new PickObject(canvas.getCanvas3D(), objScene,
                                    rootObject.getGeometryObj(), objRotate);

        canvas.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                rootObject.firePickChanged(evt, locToWin);
                if (evt.getButton() == MouseEvent.BUTTON1) {
                    if (evt.isControlDown())
                        pickObject.consume2DSelectModule(evt);
                    else if (pick3DActive)
                        pickObject.consumeEmulated3DPick(evt);
                    else if (evt.getClickCount() > 1)
                        reset();
                }
                if (evt.getButton() == MouseEvent.BUTTON3) {
                    if ((evt.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0)
                        resetViewPopup.show(Display3DPanel.this, evt.getX(), evt.getY());
                    else if (getControlsFrame() != null) {
                        getControlsFrame().setBounds(evt.getXOnScreen() - 130, evt.getYOnScreen(), 260, 540);
                        getControlsFrame().setVisible(true);
                        getControlsFrame().setExtendedState(Frame.NORMAL);
                    } else if (getTransientControlsFrame() != null) {
                        getTransientControlsFrame().setBounds(evt.getXOnScreen() - 130, evt.getYOnScreen(), 260, 540);
                        getTransientControlsFrame().setVisible(true);
                        getTransientControlsFrame().setExtendedState(Frame.NORMAL);
                    } else if (application != null) {
                        application.getFrames().getGuiPanel().selectModule(name);
                    }
                }
                if (evt.getButton() == MouseEvent.BUTTON2) {
                    if ((evt.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0 &&
                        (evt.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0)
                        fullReset();
                    else
                        reset();
                }
            }

            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
                mouseOn = true;
                mouseObserverThread = new Thread(new MouseObserver());
                mouseObserverThread.start();
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt)
            {
                mouseObserverThread = null;
                mouseOn = false;
            }
        });

        canvas.addMouseMotionListener(new java.awt.event.MouseMotionListener()
        {
            @Override
            public void mouseDragged(MouseEvent e)
            {
                //              if (e.isShiftDown())
                //                 System.out.println(""+e.getX() + " " + e.getY());
            }

            @Override
            public void mouseMoved(MouseEvent e)
            {
            }
        });

        canvas.addMouseWheelListener((java.awt.event.MouseWheelEvent evt) -> {
            rescaleFromMouseWheel(evt);
        });

        canvas.addKeyListener(new java.awt.event.KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                formKeyTyped(evt);
            }

            @Override
            public void keyPressed(KeyEvent evt)
            {
                formKeyPressed(evt);
            }

            @Override
            public void keyReleased(KeyEvent evt)
            {
                formKeyReleased(evt);
            }
        });

        ToolTipManager.sharedInstance().setLightWeightPopupEnabled(true);
        universe = new SimpleUniverse(canvas.getCanvas3D());
        view = canvas.getCanvas3D().getView();
        view.setProjectionPolicy(View.PERSPECTIVE_PROJECTION);
        view.setTransparencySortingPolicy(View.TRANSPARENCY_SORT_GEOMETRY);
        view.setSceneAntialiasingEnable(true);

        objScene.addChild(rootObject.getGeometryObj());
        rootObject.setRenderingWindow(this);
        rootObject.getGeometryObj().setUserData(null);

        bg.setCapability(Background.ALLOW_COLOR_WRITE);
        bg.setCapability(Background.ALLOW_COLOR_READ);
        bg.setCapability(Background.ALLOW_IMAGE_WRITE);
        bg.setCapability(Background.ALLOW_IMAGE_READ);
        bg.setCapability(Background.ALLOW_IMAGE_SCALE_MODE_READ);
        bg.setCapability(Background.ALLOW_IMAGE_SCALE_MODE_WRITE);
        bg.setImageScaleMode(Background.SCALE_FIT_ALL);
        bg.setApplicationBounds(bounds);

        myFog.setCapability(LinearFog.ALLOW_DISTANCE_WRITE);
        myFog.setCapability(LinearFog.ALLOW_COLOR_WRITE);
        myFog.setInfluencingBounds(bounds);
        myFog.setFrontDistance(1.);
        myFog.setBackDistance(4.);
        myFogGroup.addChild(myFog);

        mouseRotate.setTransformGroup(objRotate);
        mouseRotate.setSchedulingBounds(bounds);
        mouseRotate.setFactor(mouseRotateSensitivity);

        mouseTranslate.setTransformGroup(objRotate);
        mouseTranslate.setSchedulingBounds(bounds);
        mouseTranslate.setFactor(mouseTranslateSensitivity);

        mouseZoom.setTransformGroup(objRotate);
        mouseZoom.setSchedulingBounds(bounds);

        windowRootObject.addChild(mouseRotate);
        windowRootObject.addChild(mouseTranslate);
        windowRootObject.addChild(mouseZoom);

        objTranslate.addChild(objScene);
        objScale.addChild(objTranslate);
        objRotate.addChild(bg);
        objRotate.addChild(objScale);
        windowRootObject.addChild(objRotate);

        controlsFrame = new Display3DControlsFrame(this);
        controlsPanel = controlsFrame.getControlPanel();
        lightsEditor = controlsPanel.getLightsEditor();

        ambientLight = lightsEditor.getAmbientLightObject().getLight();
        windowRootObject.addChild(ambientLight.getLight());
        directionalLights = lightsEditor.getDirectionalLightObjects();
        pointLights = lightsEditor.getPointLightObjects();
        lightsEditor.addBaseDirectionalLight();

        modelClip.setCapability(ModelClip.ALLOW_ENABLE_READ);
        modelClip.setCapability(ModelClip.ALLOW_ENABLE_WRITE);
        modelClip.setCapability(ModelClip.ALLOW_PLANE_READ);
        modelClip.setCapability(ModelClip.ALLOW_PLANE_WRITE);
        modelClip.setInfluencingBounds(bounds);
        objScene.addChild(modelClip);

        alternateTransformObj.addChild(alternateRootObj);
        alternateTransformBranch.addChild(alternateTransformObj);
        windowRootObject.addChild(alternateTransformBranch);

        universe.getViewingPlatform().setNominalViewingTransform();
        universe.addBranchGraph(windowRootObject);

        view.getPhysicalBody().getLeftEyePosition(defaultLeftEye);
        view.getPhysicalBody().getRightEyePosition(defaultRightEye);
        universe.getViewingPlatform().getViewPlatformTransform().getTransform(initialCameraTransform);

        objReper.addChild(reper.getGeometryObj());
        positionedReper.addChild(objReper);
        positionedReper.setTransform(new Transform3D(new float[]{.15f, 0, 0, -.8f,
                                                                 0, .15f, 0, .76f,
                                                                 0, 0, .15f, 0,
                                                                 0, 0, 0, 1}));
        reperGroup.addChild(positionedReper);
        OpenBranchGroup reperLightGroup = new OpenBranchGroup();
        reperLightGroup.addChild(new AmbientLight(new Color3f(.6f, .6f, .6f)));
        reperLightGroup.addChild(new EditableDirectionalLight(new Color3f(0.4f, 0.4f, 0.4f),
                                                              bounds, "light 1", true, true).getLight());
        reperGroup.addChild(reperLightGroup);
        universe.addBranchGraph(reperGroup);
        locToWin = new LocalToWindow(rootObject.getGeometryObj(), canvas.getCanvas3D());

        /**
         * The line below binds reper with scene rotation when using any method that calls
         * objRotate.setTransform(), so: keyboard (explicitly calling), mouse (Java3D is calling
         * setTransform) and some custom ways (e.g. in haptic pointer)
         */
        objRotate.addTransformListener(objReper);
        //        mouseRotate.setupCallback(objReper); // it is not needed, because Java3D calls objRotate.setTranform which fires callback to the reper

        //Add camera BGs to the scene
        objCamera.addChild(transformObjCamera);
        cameraGroup.addChild(objCamera);
        objScene.addChild(cameraGroup);
        cameraLight.addChild(camera.getLight());
        objRotate.addChild(cameraLight);
        universe.addBranchGraph(camera.getRootBG());
        objRotate.addTransformListener(camera);
        transformObjCamera.addTransformCameraGroupListener(camera);
        addPick3DListener(new Pick3DListener(PickType.POINT)
        {
            @Override
            protected void handlePick3D(Pick3DEvent e)
            {
                InputEvent inE = e.getEvt();
                if ((inE.isAltDown() || inE.isAltGraphDown()) && inE.isControlDown()) {
                    sceneCenter = new Vector3d(-e.getPoint()[0], -e.getPoint()[1], -e.getPoint()[2]);
                    Transform3D tr = new Transform3D();
                    tr.setTranslation(sceneCenter);
                    objTranslate.setTransform(tr);
                    standardCenterPosition = false;
                }
            }
        });
        setLightsVisible(false);
        topButton.addActionListener(evt ->    setView(StandardView.TOP_VIEW, fullReset));
        bottomButton.addActionListener(evt -> setView(StandardView.BOTTOM_VIEW, fullReset));
        frontButton.addActionListener(evt ->  setView(StandardView.FRONT_VIEW, fullReset));
        backButton.addActionListener(evt ->   setView(StandardView.BACK_VIEW, fullReset));
        leftButton.addActionListener(evt ->   setView(StandardView.LEFT_VIEW, fullReset));
        rightButton.addActionListener(evt ->  setView(StandardView.RIGHT_VIEW, fullReset));
    }


    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    public void setControlsPanel(Display3DControlsPanel controlsPanel)
    {
        this.controlsPanel = controlsPanel;
    }

    public void setControlsFrame(Display3DControlsFrame controlsFrame)
    {
        this.controlsFrame = controlsFrame;
    }

    private void rescaleFromMouseWheel(java.awt.event.MouseWheelEvent evt)
    {
        if (lockView)
            return;
        int notches = evt.getWheelRotation();
        if (notches < 0)
            mouseScale /= mouseWheelSensitivity;
        else
            mouseScale *= mouseWheelSensitivity;
        tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                     0., 1., 0.,
                                                     0., 0., 1.),
                                        //sceneCenter,
                                        new Vector3d(0.0, 0.0, 0.0),
                                        externScale * mouseScale);
        objScale.setTransform(tempTransform);
    }

    public PickObject getPickObject()
    {
        return pickObject;
    }

    @Override
    public void setExtents(float[][] ext)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isMoveCameraMode()
    {
        return this.moveCameraMode;
    }

    public void setMoveCameraMode(boolean setCameraMode)
    {
        if (moveCameraMode == setCameraMode) {
            return;
        }

        moveCameraMode = setCameraMode;
    }

    /**
     * @return the controlsFrame
     */
    public Display3DControlsFrame getControlsFrame()
    {
        return controlsFrame;
    }

    /**
     * @return the transientControlsFrame
     */
    public JFrame getTransientControlsFrame()
    {
        return transientControlsFrame;
    }

    /**
     * @return the windowRootObject
     */
    public OpenBranchGroup getWindowRootObject()
    {
        return windowRootObject;
    }

    /**
     * @return rotate object
     */
    public ObjRotate getRotateObject()
    {
        return objRotate;
    }

    public ObjReper getReperObject()
    {
        return objReper;
    }

    @Override
    public PickTypeListener getPickTypeListener()
    {
        return pickObject;
    }

    /**
     * @return the application
     */
    public Application getApplication()
    {
        return application;
    }

    /**
     * @param application the application to set
     */
    public void setApplication(Application application)
    {
        this.application = application;
        pickObject.setApplication(application);
    }


    /**
     *
     * @param val fov val for the camera
     */

    public void setCameraFoV(int val) {
        camera.setFoV(val);
    }

    /**
     *
     * @param lightOnOff on/off camera light
     */

    public void enableCameraLight(boolean lightOnOff) {
        camera.enableLight(lightOnOff);
    }

    /**
     * This method calculates initial position of the camera based on inverted local to window transform,
     * adds camera geometry object and sets camera light source initial position,
     */

    public void createCameraObject() {

        /*calculate initial transform to position camera*/
        Transform3D ltw = locToWin.getLocalToVworld();
        ltw.invert();
        Matrix3d rotation = new Matrix3d();
        Vector3d translation = new Vector3d();
        ltw.get(rotation, translation);
        ltw.set(rotation, translation, externScale * mouseScale);
        transformObjCamera.setTransform(ltw);

        /*refreash camera view*/
        camera.setCameraTransform(ltw);
        objRotate.getTransform(tempTransform);
        camera.sceneTransformChanged(tempTransform);

        /*show camera*/
        transformObjCamera.addChild(camera.getGeometryObject());

        /*init light position*/
        Point3d p = new Point3d(-translation.x, -translation.y, -translation.z);
        camera.getLight().setPosition((float)p.x, (float)p.y, (float)p.z);
        camera.getLight().setInfluencingBounds(new BoundingSphere(p, 10));

        /*add camera canvas and obj rotate listener to obtain static view in cameraView frame*/
        camera.addCameraWindowCloseListener(this);
        camera.show();
    }

    /**
     * Adds new key frame to the list stored in movies manager and creates new geometry objects in the scene.
     */
    public void addKeyFrame() {
        //creates new key frame
        Transform3D transform = new Transform3D();
        transformObjCamera.getTransform(transform);
        MovieFrame keyFrame = new MovieFrame(new Transform3D(transform));
        moviesManager.addKeyFrame(keyFrame);

        //add key frame marker to the scene
        addChild(keyFrame.getOutObject());

        //update GUI
        controlsPanel.updateKeyFrameList(moviesManager.getKeyFrames());
    }

    /**
     * This method adds trajectory geometry object to the scene.
     * @param isClosed if true than the starting point is also the ending point and the trajectory is closed.
     * @param nKnots number of interpolation points on every segment between key frames
     */

    public void createTrajectorie(boolean isClosed, int nKnots){
        GeometryObject trajectory = moviesManager.getTrajectoryGeometryObject();
        //remove old trajectory
        if(trajectory!=null){
            removeChild(trajectory);
        }
        trajectory = moviesManager.createTrajectorieGeometry(isClosed, nKnots);
        if(trajectory!=null){
            addChild(trajectory);
        }
    }

    /**
     * Remove selected key frames from the movies manager list and their corresponding geometry objects from the scene.
     * @param names array consisting of the keyFrames names to be removed
     */
    public void removeKeyFrames(String[] names){
        ArrayList<MovieFrame> keyFrames;
        MovieFrame keyFrame;
        for (String frameName : names) {
            keyFrames = moviesManager.getKeyFrames();
            for (int i = 0; i<keyFrames.size();i++) {
                keyFrame = keyFrames.get(i);
                if(frameName.equals(keyFrame.toString())){
                    removeChild(keyFrame.getOutObject());
                    moviesManager.removeKeyFrame(i);
                    break;
                }
            }
        }
        controlsPanel.updateKeyFrameList(moviesManager.getKeyFrames());
    }

    /**
     * enables the camera create button in gui, disables internal camera parameters in gui nad removes camera geometry from the scene.
     */

    @Override
    public void onCameraWindowClose() {
        controlsPanel.enableCameraCreateButton(true);
        transformObjCamera.removeAllChildren();
        enableCameraLight(false);
    }



    private class MouseObserver implements Runnable
    {

        int timer = 0, lastX = -1, lastY = -1;

        @Override
        public void run()
        {
            while (mouseOn && !mouseRestingListenerList.isEmpty()) {
                Point p = getMousePosition();
                if (p == null) {
                    continue;
                }
                if ((int) p.getX() != lastX || (int) p.getY() != lastY) {
                    timer = 0;
                    lastX = (int) p.getX();
                    lastY = (int) p.getY();
                } else {
                    timer += 1;
                    if (timer == timeToPopup) {
                        fireMouseResting(lastX, lastY);
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
            fireMouseResting(-1, -1);
        }
    }

    private void formKeyTyped(KeyEvent evt)
    {
        switch (evt.getKeyChar()) {
        case ' ':
            if (controlsFrame != null) {
                controlsFrame.setBounds(30, 30, 260, 540);
                controlsFrame.setVisible(true);
                controlsFrame.setExtendedState(Frame.NORMAL);
            } else if (transientControlsFrame != null) {
                transientControlsFrame.setBounds(30, 30, 260, 540);
                transientControlsFrame.setVisible(true);
                transientControlsFrame.setExtendedState(Frame.NORMAL);
            } else if (application != null) {
                application.getFrames().getGuiPanel().selectModule(name);
            }   break;
        case 'c':
            moveCamera();
            break;
        case 'o':
            moveScene();
            break;
        case 'p':
            {
                boolean p = perspective;
                controlsPanel.togglePerspective();
                setDisplayMode(!p);
                break;
            }
        case 's':
            {
                boolean p = stereoActive;
                controlsPanel.toggleStereo();
                setStereoEnable(!p);
                break;
            }
        case 'f':
            controlsPanel.fullScreenToggle();
            break;
        case '.':
            controlsPanel.togglePick3D();
            setPick3DActive(!pickObject.isActive());
            if (pickObject.isActive())
                pickObject.setExtents(rootExtents);
            break;
        case 'm':
            moveCurrentObject();
            break;
        case KeyEvent.VK_BACK_SPACE:
            if ((evt.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0)
                fullReset();
            else
                reset();
            break;
        case '?':
            cheatSheet.setBounds(getX() + getWidth(), getY(), 900, 800);
            cheatSheet.setVisible(true);
            cheatSheet.setExtendedState(Frame.NORMAL);
            break;
        default:
            break;
        }
    }

    //TODO - chnge mapping or add switch
    private static final int CAMERA_UP_KEY = KeyEvent.VK_U;
    private static final int CAMERA_DOWN_KEY = KeyEvent.VK_J;
    private static final int CAMERA_LEFT_KEY = KeyEvent.VK_H;
    private static final int CAMERA_RIGHT_KEY = KeyEvent.VK_K;
    private static final int CAMERA_FORWARD_KEY = KeyEvent.VK_O;
    private static final int CAMERA_BACKWARD_KEY = KeyEvent.VK_L;
    private boolean cameraUpKeyPressed = false;
    private boolean cameraDownKeyPressed = false;
    private boolean cameraLeftKeyPressed = false;
    private boolean cameraRightKeyPressed = false;
    private boolean cameraForwardKeyPressed = false;
    private boolean cameraBackwardKeyPressed = false;

    private void formKeyReleased(KeyEvent evt) {

        if (evt.getModifiersEx() == 0) {
            switch (evt.getKeyCode()) {
            case CAMERA_UP_KEY:
                cameraUpKeyPressed = false;
                processCameraKeys(evt);
                break;
            case CAMERA_DOWN_KEY:
                cameraDownKeyPressed = false;
                processCameraKeys(evt);
                break;
            case CAMERA_LEFT_KEY:
                cameraLeftKeyPressed = false;
                processCameraKeys(evt);
                break;
            case CAMERA_RIGHT_KEY:
                cameraRightKeyPressed = false;
                processCameraKeys(evt);
                break;
            case CAMERA_FORWARD_KEY:
                cameraForwardKeyPressed = false;
                processCameraKeys(evt);
                break;
            case CAMERA_BACKWARD_KEY:
                cameraBackwardKeyPressed = false;
                processCameraKeys(evt);
                break;
            case KeyEvent.VK_BACK_SPACE:
                reset();
                break;
            default:
                break;
            }
        } else if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)
            switch (evt.getKeyCode()) {
            case KeyEvent.VK_F:
                showFPS = !showFPS;
                forceRender();
                break;
            case KeyEvent.VK_I:
                showRendererInfo = !showRendererInfo;
                forceRender();
                break;
            }
    }

    private void formKeyPressed(KeyEvent evt)
    {
        if (lockView) {
            return;
        }

        double dAngle = PI / 144;
        double dScale = 129. / 128.;
        if (storingFrames) {
            dAngle = PI / 360;
            dScale = 513. / 512.;
        }
        if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
            dAngle = PI / 2;
        }
        if ((evt.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0) {
            dAngle = PI / 6;
        }
        objRotate.getTransform(tempTransform);
        Transform3D rot = new Transform3D();
        if (isMoveCameraMode()) {
            switch (evt.getKeyCode()) {
            case CAMERA_UP_KEY:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_KP_UP:
            case KeyEvent.VK_NUMPAD8:
                cameraUpKeyPressed = true;
                processCameraKeys(evt);
                break;
            case CAMERA_DOWN_KEY:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_KP_DOWN:
            case KeyEvent.VK_NUMPAD2:
                cameraDownKeyPressed = true;
                processCameraKeys(evt);
                break;
            case CAMERA_LEFT_KEY:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_KP_LEFT:
            case KeyEvent.VK_NUMPAD4:
                cameraLeftKeyPressed = true;
                processCameraKeys(evt);
                break;
            case CAMERA_RIGHT_KEY:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_KP_RIGHT:
            case KeyEvent.VK_NUMPAD6:
                cameraRightKeyPressed = true;
                processCameraKeys(evt);
                break;
            case CAMERA_FORWARD_KEY:
            case KeyEvent.VK_PAGE_UP:
            case KeyEvent.VK_NUMPAD9:
                cameraForwardKeyPressed = true;
                processCameraKeys(evt);
                break;
            case CAMERA_BACKWARD_KEY:
            case KeyEvent.VK_PAGE_DOWN:
            case KeyEvent.VK_NUMPAD3:
                cameraBackwardKeyPressed = true;
                processCameraKeys(evt);
                break;
            default:
                break;
            }
        } else if ((evt.getModifiers() & KeyEvent.ALT_MASK) != 0) {
            switch (evt.getKeyCode()) {
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_KP_DOWN:
            case KeyEvent.VK_NUMPAD2:
                setView(StandardView.BACK_VIEW, false);
                break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_KP_UP:
            case KeyEvent.VK_NUMPAD8:
                setView(StandardView.FRONT_VIEW, false);
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_KP_LEFT:
            case KeyEvent.VK_NUMPAD4:
                setView(StandardView.LEFT_VIEW, false);
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_KP_RIGHT:
            case KeyEvent.VK_NUMPAD6:
                setView(StandardView.RIGHT_VIEW, false);
                break;
            case KeyEvent.VK_PAGE_UP:
            case KeyEvent.VK_NUMPAD9:
                setView(StandardView.TOP_VIEW, false);
                break;
            case KeyEvent.VK_PAGE_DOWN:
            case KeyEvent.VK_NUMPAD3:
                setView(StandardView.BOTTOM_VIEW, false);
                break;
            }
        } else {
            switch (evt.getKeyCode()) {
            case KeyEvent.VK_NUMPAD5:
                if ((evt.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0)
                    fullReset();
                else
                    reset();
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_KP_DOWN:
            case KeyEvent.VK_NUMPAD2:
                rot.rotX(dAngle);
                break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_KP_UP:
            case KeyEvent.VK_NUMPAD8:
                rot.rotX(-dAngle);
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_KP_LEFT:
            case KeyEvent.VK_NUMPAD4:
                rot.rotY(-dAngle);
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_KP_RIGHT:
            case KeyEvent.VK_NUMPAD6:
                rot.rotY(dAngle);
                break;
            case KeyEvent.VK_PAGE_UP:
            case KeyEvent.VK_NUMPAD9:
                rot.rotZ(dAngle);
                break;
            case KeyEvent.VK_PAGE_DOWN:
            case KeyEvent.VK_NUMPAD3:
                rot.rotZ(-dAngle);
                break;
            }
            rot.mul(tempTransform);
            objRotate.setTransform(rot);

            switch (evt.getKeyCode()) {
                case KeyEvent.VK_HOME:
                case KeyEvent.VK_NUMPAD7:
                    mouseScale *= dScale;
                    break;
                case KeyEvent.VK_END:
                case KeyEvent.VK_NUMPAD1:
                    mouseScale /= dScale;
                    break;
            }
            tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                         0., 1., 0.,
                                                         0., 0., 1.),
                                            //sceneCenter,
                                            new Vector3d(0.0, 0.0, 0.0),
                                            externScale * mouseScale);
            objScale.setTransform(tempTransform);
        }
        locToWin.update();
        fireProjectionChanged(new ProjectionEvent(this, locToWin));
    }

    private void processCameraKeys(KeyEvent evt)
    {
        double dAngle = PI / 144;
        double dScale = 129. / 128.;
        if (storingFrames) {
            dAngle = PI / 360;
            dScale = 513. / 512.;
        }
        if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
            dAngle = PI / 2;
        }

        TransformGroup cam = universe.getViewingPlatform().getViewPlatformTransform();
        Transform3D camTr;
        Transform3D cRot;
        Transform3D tmpTr;
        Vector3d cMov;

        if (cameraUpKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cRot = new Transform3D();
            cRot.rotX(-dAngle);
            camTr.mul(cRot);
            cam.setTransform(camTr);
        }

        if (cameraDownKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cRot = new Transform3D();
            cRot.rotX(dAngle);
            camTr.mul(cRot);
            cam.setTransform(camTr);
        }

        if (cameraRightKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cRot = new Transform3D();
            cRot.rotY(-dAngle);
            camTr.mul(cRot);
            cam.setTransform(camTr);
        }

        if (cameraLeftKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cRot = new Transform3D();
            cRot.rotY(dAngle);
            camTr.mul(cRot);
            cam.setTransform(camTr);
        }

        if (cameraForwardKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cMov = new Vector3d();
            cMov.x = 0;
            cMov.y = 0;
            cMov.z = -(dScale / 50);

            tmpTr = new Transform3D();
            cam.getTransform(tmpTr);
            tmpTr.set(new Vector3d());
            tmpTr.transform(cMov);

            tmpTr.set(cMov);
            camTr.mul(tmpTr);
            cam.setTransform(camTr);
        }

        if (cameraBackwardKeyPressed) {
            camTr = new Transform3D();
            cam.getTransform(camTr);
            cMov = new Vector3d();
            cMov.x = 0;
            cMov.y = 0;
            cMov.z = (dScale / 50);

            tmpTr = new Transform3D();
            cam.getTransform(tmpTr);
            tmpTr.set(new Vector3d());
            tmpTr.transform(cMov);

            tmpTr.set(cMov);
            camTr.mul(tmpTr);
            cam.setTransform(camTr);
        }
        cameraUpKeyPressed = cameraDownKeyPressed
            = cameraRightKeyPressed = cameraLeftKeyPressed
            = cameraForwardKeyPressed = cameraBackwardKeyPressed = false;
    }

    public void setBackgroundColor(Color3f c)
    {
        bgColor = c;
        bg.setColor(c);
        myFog.setColor(bgColor);
        fireBgrColorChanged();
    }

    public void setBackgroundColor(Color c)
    {
        bgColor = ColorMapper.convertColorToColor3f(c);
        bg.setImage(null);
        bg.setColor(bgColor);
        myFog.setColor(bgColor);
        fireBgrColorChanged();
    }

    public void setBackgroundGradient(Color c0, Color c1, Color c2)
    {
        bgColor = new Color3f(c0.getColorComponents(null));
        myFog.setColor(bgColor);
        fireBgrColorChanged();
        int r0 = c0.getRed(), r1 = c1.getRed(), r2 = c2.getRed();
        int g0 = c0.getGreen(), g1 = c1.getGreen(), g2 = c2.getGreen();
        int b0 = c0.getBlue(), b1 = c1.getBlue(), b2 = c2.getBlue();
        int[] bgrData = new int[256 * 256];
        int k = 0;
        for (int i = 0; i < 128; i++) {
            float t = i / 127.f;
            int r = (int) (t * r1 + (1 - t) * r0);
            int g = (int) (t * g1 + (1 - t) * g0);
            int b = (int) (t * b1 + (1 - t) * b0);
            int c = ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
            for (int j = 0; j < 256; j++, k++) {
                bgrData[k] = c;
            }
        }
        for (int i = 0; i < 128; i++) {
            float t = i / 127.f;
            int r = (int) (t * r2 + (1 - t) * r1);
            int g = (int) (t * g2 + (1 - t) * g1);
            int b = (int) (t * b2 + (1 - t) * b1);
            int c = ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
            for (int j = 0; j < 256; j++, k++) {
                bgrData[k] = c;
            }
        }
        BufferedImage bgrImage = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);
        bgrImage.setRGB(0, 0, 256, 256, bgrData, 0, 256);
        ImageComponent2D bgrImageComponent = new ImageComponent2D(ImageComponent2D.FORMAT_RGBA, bgrImage);
        bg.setImage(bgrImageComponent);
    }

    BufferedImage baseBgrImage;

    public void setBgrImageBrightness(float t)
    {
        if (baseBgrImage == null) {
            return;
        }
        int w = baseBgrImage.getWidth();
        int h = baseBgrImage.getHeight();
        int[] bgrData = baseBgrImage.getRGB(0, 0, w, h, null, 0, w);
        for (int i = 0; i < bgrData.length; i++) {
            int imc = bgrData[i];
            int r = (int) (t * ((imc >> 16) & 0xff));
            int g = (int) (t * ((imc >> 8) & 0xff));
            int b = (int) (t * (imc & 0xff));
            bgrData[i] = ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
        }
        BufferedImage bgrImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        bgrImage.setRGB(0, 0, w, h, bgrData, 0, w);
        ImageComponent2D bgrImageComponent = new ImageComponent2D(ImageComponent2D.FORMAT_RGBA, bgrImage);
        bg.setImage(bgrImageComponent);
    }

    public void setBackgroundImage(String imageFileName)
    {
        try {
            baseBgrImage = ImageUtils.readImage(imageFileName);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        setBgrImageBrightness(1.f);
    }

    @Override
    public Color getBackgroundColor()
    {
        return ColorMapper.convertColor3fToColor(bgColor);
    }
    public void saveTransforms()
    {
        Transform3D rotTransform = new Transform3D();
        objRotate.getTransform(rotTransform);
        Transform3D trTransform = new Transform3D();
        objTranslate.getTransform(trTransform);
        Transform3D scTransform = new Transform3D();
        objScale.getTransform(scTransform);
        Transform3D univTransform = new Transform3D();
        universe.getViewingPlatform().getViewPlatformTransform().getTransform(univTransform);
        savedTransforms = new SavedTransforms(rotTransform, trTransform, scTransform, univTransform);
    }

    public void restoreTransforms()
    {
        if (savedTransforms == null)
            return;
        objRotate.setTransform(savedTransforms.rotation);
        objTranslate.setTransform(savedTransforms.translation);
        objScale.setTransform(savedTransforms.scaling);
        universe.getViewingPlatform().getViewPlatformTransform().setTransform(savedTransforms.universe);
    }

    public void setView(StandardView view, boolean resetScale, boolean resetCenter)
    {
        if (!lockView) {
            lastStandardView = view;
            if (resetScale)
                mouseScale = 1.;
            objRotate.setTransform(new Transform3D(view.getMat(), new Vector3d(0.0, 0.0, 0.0), 1));
            Transform3D tr = new Transform3D();
            standardCenterPosition = resetCenter;
            if (resetCenter)
                updateExtents();
            tr.setTranslation(sceneCenter);
            objTranslate.setTransform(tr);
            tempTransform = new Transform3D();
            tempTransform.setScale(externScale * mouseScale);
            objScale.setTransform(tempTransform);
            universe.getViewingPlatform().getViewPlatformTransform().setTransform(initialCameraTransform);
        }
    }

    public void setView(StandardView view, boolean resetCenter)
    {
       setView(view, true, resetCenter);
    }

    public void reset()
    {
        if (lockView) {
            return;
        }
        mouseScale = 1.;
        objRotate.setTransform(new Transform3D());
        Transform3D tr = new Transform3D();
        tr.setTranslation(sceneCenter);
        objTranslate.setTransform(tr);
        tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                     0., 1., 0.,
                                                     0., 0., 1.),
                                        //sceneCenter,
                                        new Vector3d(0.0, 0.0, 0.0),
                                        externScale * mouseScale);
        objScale.setTransform(tempTransform);
        universe.getViewingPlatform().getViewPlatformTransform().setTransform(initialCameraTransform);
    }

    public void fullReset()
    {
        if (lockView) {
            return;
        }
        mouseScale = 1.;
        objRotate.setTransform(new Transform3D());
        Transform3D tr = new Transform3D();
        standardCenterPosition = true;
        updateExtents();
        tr.setTranslation(sceneCenter);
        objTranslate.setTransform(tr);
        tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                     0., 1., 0.,
                                                     0., 0., 1.),
                                        //sceneCenter,
                                        new Vector3d(0.0, 0.0, 0.0),
                                        externScale * mouseScale);
        objScale.setTransform(tempTransform);
        universe.getViewingPlatform().getViewPlatformTransform().setTransform(initialCameraTransform);
        for (DirectionalLightObject directionalLight : directionalLights)
            directionalLight.reset();
        for (PointLightObject pointLight : pointLights)
            pointLight.reset();
    }

    public void setDisplayMode(boolean mode)
    {
        perspective = mode;
        if (perspective) {
            view.setProjectionPolicy(View.PERSPECTIVE_PROJECTION);
            if (offScreenView != null)
                offScreenView.setProjectionPolicy(View.PERSPECTIVE_PROJECTION);
        } else {
            view.setProjectionPolicy(View.PARALLEL_PROJECTION);
            if (offScreenView != null)
                offScreenView.setProjectionPolicy(View.PARALLEL_PROJECTION);
        }
    }

    /*
    public void cameraPositionChanged(UnitVect u, float angle)
    {

        TransformGroup camera = universe.getViewingPlatform().getViewPlatformTransform();


        double cosA = Math.cos(angle);
        double sinA = Math.sin(angle);

        Transform3D transform = new Transform3D();
        camera.getTransform(transform);
        Vector3d vector = new Vector3d();
        transform.get(vector);

        transform.set(new Matrix3d(
            cosA+u.x*u.x*(1-cosA), u.x*u.y*(1-cosA)-u.z*sinA, u.x*u.z*(1-cosA)+u.y*sinA,
            u.y*u.x*(1-cosA)+u.z*sinA, cosA+u.y*u.y*(1-cosA), u.y*u.z*(1-cosA)-u.x*sinA,
            u.z*u.x*(1-cosA)-u.y*sinA, u.z*u.y*(1-cosA)+u.x*sinA, cosA+u.z*u.z*(1-cosA)
        ), vector, externScale * mouseScale);

        camera.setTransform(transform);

        /*
        if(cameraPath!=null){
            FloatLargeArray coords;
            if(cameraPath.hasCoords())
                coords = cameraPath.getCoords();
            else
                coords = cameraPath.getCoordsFromAffine();

            for (int i = 0; i < coords.length(); i+=3) {
                Transform3D transform = new Transform3D();
                camera.getTransform(transform);
                transform.set(new Vector3d(coords.get(i), coords.get(i+1), coords.get(i+2)));
                camera.setTransform(transform);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(Display3DPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }*/

    //}

    @Override
    public void setScale(double scale)
    {
        if (lockView)
            return;

        externScale = scale;
        tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                     0., 1., 0.,
                                                     0., 0., 1.),
                                        // sceneCenter,
                                        new Vector3d(0.0, 0.0, 0.0),
                                        externScale * mouseScale);
        objScale.setTransform(tempTransform);
    }
    
    public void createOffScreen() {
        offScreenCanvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration(), true) {
            @Override
            public void postRender() {
                J3DGraphics2D vGraphics = getGraphics2D();
                vGraphics.setFont(new Font("sans-serif", Font.PLAIN, 10));
                vGraphics.setColor(Color.YELLOW);
                //offScreenCanvas.setOffScreenLocation(canvas.getLocationOnScreen().x, canvas.getLocationOnScreen().y);
                //offScreenCanvas.setLocation(canvas.getLocationOnScreen());
                offScreenLocToWin = new LocalToWindow(objScene, offScreenCanvas);
                draw2D(vGraphics, offScreenLocToWin, offScreenCanvas.getOffScreenBuffer().getWidth(),  offScreenCanvas.getOffScreenBuffer().getHeight());
                vGraphics.flush(false);
                offScreenRenderDone = true;
            }
        };
        offScreenView = offScreenCanvas.getView();
        if (offScreenView != null) {
            if (perspective) {
                offScreenView.setProjectionPolicy(View.PERSPECTIVE_PROJECTION);
            } else {
                offScreenView.setProjectionPolicy(View.PARALLEL_PROJECTION);
            }
        }
        offScreenCanvas.setOffScreenLocation(0, 0);
        Screen3D onScreen = canvas.getCanvas3D().getScreen3D();
        Screen3D offScreen = offScreenCanvas.getScreen3D();
        offScreen.setSize(onScreen.getSize());
        offScreen.setPhysicalScreenWidth(onScreen.getPhysicalScreenWidth());
        offScreen.setPhysicalScreenHeight(onScreen.getPhysicalScreenHeight());        
        setOffScreenSize(this.getWidth(),this.getHeight());
    }
    
    public void setOffScreenSize(int w, int h)
    {
        if (w * h == 0 || w < 0 || h < 0) {
            return;
        }

        if(offScreenCanvas == null)
            createOffScreen();
        
        if(offScreenCanvas.getOffScreenBuffer() != null && offScreenCanvas.getOffScreenBuffer().getWidth() == w && 
                offScreenCanvas.getOffScreenBuffer().getHeight() == h) {
            return;
        }
        
        universe.getViewer().getView().removeCanvas3D(offScreenCanvas);
        
        offScreenBufferImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        ImageComponent2D imC = new ImageComponent2D(ImageComponent2D.FORMAT_RGB, offScreenBufferImage, true, false);
        imC.setCapability(ImageComponent2D.ALLOW_IMAGE_READ);
        offScreenCanvas.setOffScreenBuffer(imC);
        
        universe.getViewer().getView().addCanvas3D(offScreenCanvas);
    }
    
    public void writeImage(File file) {
        writeImage(file, this.getWidth(), this.getHeight());
    }
    
    public void writeImage(File file, int w, int h)
    {
        if (offScreenCanvas == null)
            createOffScreen();
        
        setOffScreenSize(w, h);
        offScreenRenderDone = false;
        try {
            offScreenCanvas.renderOffScreenBuffer();
        } catch (NullPointerException c) {
            LOGGER.error("Null pointer in offC.renderOffScreenBuffer()");
            fireFrameRendered();
            return;
        }
        if (!storingFrames)
            offScreenCanvas.waitForOffScreenRendering();
        while (!offScreenRenderDone)
            try {
                wait(10);
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Display3DPanel.class.getName()).log(Level.SEVERE, null, ex);
            }

        String ext = FilenameUtils.getExtension(file.getAbsolutePath()).toLowerCase();
        if (ext == null)
            return;
        try {
            BufferedImage img = new BufferedImage(offScreenBufferImage.getWidth(), offScreenBufferImage.getHeight(), BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(offScreenBufferImage, null, null);
            switch (ext.toLowerCase()) {
                case "jpg":
                case "jpeg":
                    ImageUtils.writeImageJPEG(offScreenBufferImage, file, 1.0f);
                    break;
                case "png":
                    ImageUtils.writeImagePNG(offScreenBufferImage, file);
                    break;
                case "gif":
                    ImageUtils.writeImageGIF(offScreenBufferImage, file);
                    break;
                case "tif":
                case "tiff":
                    ImageUtils.writeImageTIFF(offScreenBufferImage, file, "None");
                    break;
                case "bmp":
                    ImageUtils.writeImageBMP(offScreenBufferImage, file);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid file extension " + ext);
            }
        } catch (IOException e) {
            LOGGER.error("I/O exception for " + file.getAbsolutePath());
        }

        fireFrameRendered();
    }

    public void writeScene(String sceneFile)
    {
        try {
            SceneGraphFileWriter scnWriter = new SceneGraphFileWriter(new File(sceneFile), universe, true, "", null);
            scnWriter.close();
        } catch (IOException | UnsupportedUniverseException e) {
            LOGGER.error("unsupported write operation");
        }
    }

    public void readScene(String sceneFile)
    {
        try {
            SceneGraphFileReader scnReader = new SceneGraphFileReader(new File(sceneFile));
            universe = scnReader.readUniverse(true);
            scnReader.close();
        } catch (IOException e) {
            LOGGER.error("unsupported read operation");
        }
    }
    
    public void setFogActive(boolean active)
    {
        if (active) {
            objRotate.addChild(myFogGroup);
        } else {
            objRotate.removeChild(myFogGroup);
        }
    }

    public void setFogRange(double front, double back)
    {
        myFog.setFrontDistance(front);
        myFog.setBackDistance(back);
    }
    public void setLightsVisible(boolean lightsVisible)
    {
        this.lightsVisible = lightsVisible;
        for (DirectionalLightObject directionalLightObject : directionalLights)
            directionalLightObject.getGlyph().setVisible(lightsVisible);
        for (PointLightObject pointLightObject : pointLights)
            pointLightObject.setVisible(lightsVisible);
    }

    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
    {

    }

    @Override
    public int getAreaWidth()
    {
        return this.getWidth();
    }

    @Override
    public int getAreaHeight()
    {
        return this.getHeight();
    }

    @Override
    public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
    {
        long dt = System.currentTimeMillis() - frameCounterStartTime;
        if(dt > fpsMeasureTime) {
            currentFPS = 1000.0*(double)frameCounter/(double)dt;
            frameCounter = 0;
            frameCounterStartTime = -1;
        }

        try {
            drawLocal2D(vGraphics, ltw, w, h);
            rootObject.draw2D(vGraphics, ltw, w, h);            
            
            Font f = vGraphics.getFont();
            Color c = vGraphics.getColor();
            if (titles != null)
                for (Title title : titles) {
                    float fh = h * title.getFontHeight();
                    if (fh < 5) {
                        fh = 5;
                    }
                    Font actualFont = title.getFont().deriveFont(fh);
                    vGraphics.setFont(actualFont);
                    FontMetrics fm = vGraphics.getFontMetrics();
                    int strWidth = fm.stringWidth(title.getTitle());
                    vGraphics.setColor(title.getColor());
                    int xPos = w / 50;
                    if (title.getHorizontalPosition() == Title.CENTER) {
                        xPos = (w - strWidth) / 2;
                    }
                    if (title.getHorizontalPosition() == Title.RIGHT) {
                        xPos = w - strWidth - getWidth() / 50;
                    }
                    vGraphics.drawString(title.getTitle(), xPos,
                                         (int) (this.getHeight() * title.getVerticalPosition()) + actualFont.getSize());
                }


            if (showFPS) {
                Font actualFont = new Font("Dialog", 0, 18);
                vGraphics.setFont(actualFont);
                vGraphics.setColor(Color.MAGENTA);
                if(currentFPS >= 0.0 && !(new Float(currentFPS)).isInfinite() )
                    vGraphics.drawString(String.format("FPS: %4.1f", currentFPS), 10, 10+actualFont.getSize());
                else
                    vGraphics.drawString("FPS: n/a", 10, 10+actualFont.getSize());
            }

            if (showRendererInfo) {
                Font actualFont = new Font("Dialog", 0, 18);
                vGraphics.setFont(actualFont);
                vGraphics.setColor(Color.YELLOW);

                Map<String, String> propertyMap = getRenderingProperties();
                float xpos = 10;
                float ystep = 10+actualFont.getSize();
                float ypos =  ystep + (showFPS?ystep:0);
                vGraphics.drawString("Renderer: "+propertyMap.get("native.renderer"), xpos, ypos+2*ystep);
                vGraphics.drawString("Vendor: "+propertyMap.get("native.vendor"), xpos, ypos);
                vGraphics.drawString("Version: "+propertyMap.get("native.version"), xpos, ypos+1*ystep);
                vGraphics.drawString("Java3D version: "+propertyMap.get("j3d.version"), xpos, ypos+3*ystep);
                vGraphics.drawString("Java3D pipeline: "+propertyMap.get("j3d.pipeline"), xpos, ypos+4*ystep);
                vGraphics.drawString("Java3D renderer: "+propertyMap.get("j3d.renderer"), xpos, ypos+5*ystep);
            }


            vGraphics.setFont(f);
            vGraphics.setColor(c);
        } catch (Exception e) {
        }
    }

    @Override
    public void addChild(GeometryObject child)
    {
        synchronized (this) {
            boolean wasRendererRunning = canvas.getCanvas3D().isRendererRunning();
            if (rootObject.getChildren().contains(child))
                return;
            if (wasRendererRunning)
                canvas.stopRenderer();
            locToWin.update();
            child.setCurrentViewer(this);
            child.setLocalToWindow(locToWin);
            child.setRenderingListener(renderListener);
            rootObject.addChild(child);
            addBgrColorListener(child.getBackgroundColorListener());
            fireBgrColorChanged();
            updateExtents();
            if (wasRendererRunning) {
//                    Thread.sleep(1);
                canvas.startRenderer();
            }
        }
    }

    @Override
    public boolean removeChild(GeometryObject child)
    {
        synchronized (this) {
            boolean success = rootObject.removeChild(child);
            if (success) {
                child.setCurrentViewer(null);
                child.setParentGeom(null);
            }
            return success;
        }
    }

    public BoundingSphere getBoundingSphere()
    {
        return bounds;
    }

    @Override
    public OpenBranchGroup getGeometryObj()
    {
        return rootObject.getGeometryObj();
    }

    @Override
    public void clearAllGeometry()
    {
        SortedSet<GeometryObject> children = rootObject.getChildren();
        for (GeometryObject child : children) {
            child.setCurrentViewer(null);
            child.setParentGeom(null);
        }
        rootObject.clearAllGeometry();
        standardCenterPosition = true;
        addChild(pickObject.getOutObject());
    }

    @Override
    public void addNode(Node node)
    {
        synchronized (this) {
            if (node instanceof OpenBranchGroup) {
                ((OpenBranchGroup) node).setCurrentViewer(this);
            } else if (node instanceof OpenTransformGroup) {
                ((OpenTransformGroup) node).setCurrentViewer(this);
            }

            objScene.addChild(node);
        }
    }


    public void setTitles(ArrayList<Title> titles)
    {
        this.titles = titles;
        this.forceRender();
    }

    public void setStoringJPEG(boolean storingJPEG)
    {
        this.storingJPEG = storingJPEG;
    }

    public void setStoringPNG(boolean storingPNG)
    {
        this.storingPNG = storingPNG;
    }

    public void setStoringFrames(boolean storingFrames)
    {
        if(storingFrames && offScreenCanvas == null)
            createOffScreen();
        
        this.storingFrames = storingFrames;
        if (!storingFrames) {
            this.storingJPEG = false;
            this.storingPNG = false;
        }
    }

    public boolean isStoringFrames()
    {
        return this.storingFrames;
    }

    public void animate(double[] params)
    {
        if (lockView) {
            return;
        }

        objRotate.getTransform(tempTransform);
        Transform3D rot = new Transform3D();
        Transform3D tmp = new Transform3D();
        tmp.rotX(params[0]);
        rot.mul(tmp);
        tmp.rotY(params[1]);
        rot.mul(tmp);
        tmp.rotZ(params[2]);
        rot.mul(tmp);
        rot.mul(tempTransform);
        objRotate.setTransform(rot);
        objTranslate.getTransform(rot);
        Vector3d trans = new Vector3d(params[3], params[4], params[5]);
        tmp = new Transform3D();
        tmp.setTranslation(trans);
        rot.mul(tmp);
        objTranslate.setTransform(rot);
        mouseScale *= params[6];
        tempTransform = new Transform3D(new Matrix3d(1., 0., 0.,
                                                     0., 1., 0.,
                                                     0., 0., 1.),
                                        //sceneCenter,
                                        new Vector3d(0.0, 0.0, 0.0),
                                        externScale * mouseScale);
        objScale.setTransform(tempTransform);
    }

    public void animate()
    {
        if (controlsPanel != null && controlsPanel.animate()) {
            animate(controlsPanel.getAnimationParams());
        } else if (storingFrames) {
        }
    }

    @Override
    public void updateExtents()
    {
        //rootObject overrides updateExtents method on declaration
        rootObject.updateExtents();
    }

    @Override
    public RenderingParams getRenderingParams()
    {
        return objectDisplayParams;
    }

    @Override
    public String toString()
    {
        return "scene";
    }

    @Override
    public void printDebugInfo()
    {
        System.out.println("current view locale has " +
            universe.getLocale().numBranchGraphs() + " active obiects");
        windowRootObject.printDebugInfo();
        objScene.printDebugInfo();
    }

    @Override
    public void setTransparency()
    {
        rootObject.setTransparency();
    }

    @Override
    public void setLineThickness()
    {
        rootObject.setLineThickness();
    }

    @Override
    public void setLineStyle()
    {
        rootObject.setLineStyle();
    }

    @Override
    public void setShininess()
    {
        rootObject.setShininess();
    }

    @Override
    public void setTransform(Transform3D transform)
    {
        if (lockView) {
            return;
        }

        objScale.setTransform(transform);
    }

    @Override
    public Canvas3D getCanvas()
    {
        return canvas.getCanvas3D();
    }

    @Override
    public SortedSet<GeometryObject> getChildren()
    {
        return rootObject.getChildren();
    }

    public void setMouseRotateSensitivity(double mouseRotateSensitivityFactor)
    {
        mouseRotate.setFactor(mouseRotateSensitivityFactor * mouseRotateSensitivity);
    }

    public void setMouseTranslateSensitivity(double mouseTranslateSensitivityFactor)
    {
        mouseTranslate.setFactor(mouseTranslateSensitivityFactor * mouseTranslateSensitivity);
        mouseZoom.setFactor(mouseTranslateSensitivityFactor * mouseTranslateSensitivity);
    }

    public void setMouseWheelSensitivity(double step)
    {
        this.mouseWheelSensitivity = step;
    }

    public void setStereoEnable(boolean stereo)
    {
        if (canvas != null) {
            canvas.setStereoEnable(stereo);
        }
    }

    public boolean getStereoAvailable()
    {
        return canvas.getCanvas3D().getStereoAvailable();
    }

    public void setStereoSeparation(float s)
    {
        if (s > 10) {
            return;
        }
        double[] defLEye = new double[3];
        double[] defREye = new double[3];
        double[] lEye = new double[3];
        double[] rEye = new double[3];
        defaultLeftEye.get(defLEye);
        defaultRightEye.get(defREye);
        for (int i = 0; i < defLEye.length; i++) {
            lEye[i] = defLEye[i] + (1. - s) * (defREye[i] - defLEye[i]) / 2;
            rEye[i] = defREye[i] + (1. - s) * (defLEye[i] - defREye[i]) / 2;
        }
        view.getPhysicalBody().setLeftEyePosition(new Point3d(lEye));
        view.getPhysicalBody().setRightEyePosition(new Point3d(rEye));
    }

    @Override
    public void setColor()
    {
    }

    /**
     * Utility field holding list of ProjectionListeners.
     */
    private transient ArrayList<ProjectionListener> projectionListenerList = new ArrayList<>();

    /**
     * Registers ProjectionListener to receive events.
     *
     * @param listener The listener to register.
     */
    @Override
    public synchronized void addProjectionListener(ProjectionListener listener)
    {
        if (listener != null && !projectionListenerList.contains(listener)) {
            projectionListenerList.add(listener);
        }
    }

    /**
     * Removes ProjectionListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    @Override
    public synchronized void removeProjectionListener(ProjectionListener listener)
    {
        if (listener != null) {
            projectionListenerList.remove(listener);
        }
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param event The event to be fired
     */
    private void fireProjectionChanged(ProjectionEvent evt)
    {
        try {
            for (ProjectionListener listener : projectionListenerList) {
                listener.projectionChanged(evt);
            }
        } catch (java.util.ConcurrentModificationException e) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException x) {
            }
        }
    }

    public void startRenderer()
    {
        canvas.startRenderer();
    }

    public void displayControls()
    {
        if (controlsFrame != null) {
            controlsFrame.setVisible(true);
        }
    }

    public void setTransformedNode(int transformedNode) {
        if (this.transformedNode == TRANSFORMED_LIGHT && transformedNode != TRANSFORMED_LIGHT)
            fireLightDirectionChanged();
        this.transformedNode = transformedNode;
    }

    public void moveCamera()
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame) {
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming camera");
        }
        setTransformedNode(TRANSFORMED_CAMERA);
        mouseRotate.setTransformGroup(universe.getViewingPlatform().getViewPlatformTransform());
        mouseTranslate.setTransformGroup(universe.getViewingPlatform().getViewPlatformTransform());
        mouseZoom.setTransformGroup(universe.getViewingPlatform().getViewPlatformTransform());
    }

    /**
     * Set the camera transform group for the java3D MouseRotate, MouseTranslate and MouseZoom objects
     */

    public void moveCameraView()
    {
        mouseRotate.setTransformGroup(transformObjCamera);
        mouseTranslate.setTransformGroup(transformObjCamera);
        mouseZoom.setTransformGroup(transformObjCamera);
    }

    public void moveScene()
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame) {
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming object");
        }
        setTransformedNode(TRANSFORMED_OBJECT);
        mouseRotate.setTransformGroup(objRotate);
        mouseTranslate.setTransformGroup(objRotate);
        mouseZoom.setTransformGroup(objRotate);
    }

    /**
     * if currently selected module (module visible in the controls panel)
     * is a visualization module and its geometry output is connected to the viewer,
     * currentModule is this currently selected module,
     * otherwise currentModule is set to null
     * currentObject is the currentModule generated object or null if currentModule is null
     */
    protected GeometryObject currentObject = null;
    protected VisualizationModule currentModule = null;

    protected void updateCurrentModule()
    {
        if (application == null) {
            currentObject = null;
            currentModule = null;
            return;
        }
        ModuleCore currentCore = application.getFrames().getGuiPanel().getCurrentCore();
        if (currentCore == null || !(currentCore instanceof VisualizationModule)) {
            currentObject = null;
            currentModule = null;
            return;
        }
        currentModule = (VisualizationModule) currentCore;
        currentObject = currentModule.getOutObject();
        if (rootObject.isAncestor(currentObject))
            return;
        currentObject = null;
        currentModule = null;
    }

    /**
     * transformingCurrentObject is an indicator for mouse transformation modes -
     * if true, the mouse transforms only the current object
     */
    protected boolean transformingCurrentObject = false;
    /**
     * An indicator for mouse transformation modes -
     * if non-null, the mouse transforms only the light with the corresponding index
     */
    /**
     * simple switch of mouse behavior to transform a selected object caused
     * the transformations to be executed in local coordinates with an unnatural
     * effect of mouse motions.
     * To overcome this, a special alternate branch is added to vWorld for tempoarry attaching
     * the current object
     */
    protected OpenBranchGroup    alternateTransformBranch = new OpenBranchGroup();
    protected OpenTransformGroup alternateTransformObj = new OpenTransformGroup();
    protected OpenBranchGroup    alternateRootObj = new OpenBranchGroup();

    private OpenTransformGroup currentTransformGroup = null;
    private OpenBranchGroup    currentTransformObject = null;
    private OpenBranchGroup    currentParentGroup = null;

    private Matrix4f invLTW = new Matrix4f();

    protected void moveCurrentObject()
    {
        Transform3D tr;
        updateCurrentModule();
        if (currentObject == null)
            return;
        transformingCurrentObject = !transformingCurrentObject;
        if (transformingCurrentObject) {
            if (parentFrame != null && parentFrame instanceof Display3DInternalFrame)
                ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming object");
            currentTransformGroup = currentObject.getTransformObj();
            currentTransformObject = (OpenBranchGroup) currentTransformGroup.getParent();
            currentParentGroup = (OpenBranchGroup) currentTransformObject.getParent();
            LocalToWindow ltw = new LocalToWindow(currentTransformGroup.getChild(0), canvas.getCanvas3D());
            //ltw.update();
            tr = ltw.getLocalToVworld();
            float[] t = new float[16];
            tr.get(t);
            Matrix4f cltwMatrix = new Matrix4f();
            tr.get(cltwMatrix);
            invLTW.invert(cltwMatrix);
            currentParentGroup.removeChild(currentTransformObject);
            currentTransformGroup.setTransform(tr);
            alternateRootObj.addChild(currentTransformObject);
            setTransformedNode(TRANSFORMED_OBJECT);
            mouseRotate.setTransformGroup(currentTransformGroup);
            mouseTranslate.setTransformGroup(currentTransformGroup);
            mouseZoom.setTransformGroup(currentTransformGroup);
        } else {
            if (currentTransformGroup == null)
                return;
            LocalToWindow ltw = new LocalToWindow(currentTransformGroup.getChild(0), canvas.getCanvas3D());
            //ltw.update();
            tr = ltw.getLocalToVworld();
            Matrix4f trMatrix = new Matrix4f();
            tr.get(trMatrix);
            Matrix4f objMatrix = new Matrix4f();
            objMatrix.mul(invLTW, trMatrix);
            alternateRootObj.removeChild(currentTransformObject);
            tr.set(objMatrix);
            currentTransformGroup.setTransform(tr);
            currentParentGroup.addChild(currentTransformObject);
            moveScene();
        }
    }

    public void addLightObject(LightObject obj)
    {
        if (obj.getLight().getType() == DIRECTIONAL) {
            windowRootObject.addChild(obj);
            directionalLights.add((DirectionalLightObject)obj);
        }
        if (obj.getLight().getType() == POINT) {
            PointLightObject pointLight = (PointLightObject)obj;
            rootObject.addNode(pointLight);
            pointLight.updateScale(rootDim, sceneCenter);
            pointLight.reset();
            pointLights.add(pointLight);
        }
        refresh();
    }

    protected PointLightObject transformedPointLightObject = null;
    protected DirectionalLightObject transformedDirectionalLightObject = null;

    private void stopDirectionalLightTransform()
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame)
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming scene");
        setTransformedNode(TRANSFORMED_OBJECT);
        mouseRotate.setTransformGroup(objRotate);
        transformedDirectionalLightObject = null;
    }

    private void stopPointLightTransform()
    {
        if (transformedPointLightObject == null)
            return;
        Transform3D lightTransform = new Transform3D();
        currentTransformGroup.getTransform(lightTransform);
        Transform3D trans = LocalToWindow.getTransform(rootObject.getGeometryObj());
        trans.invert();
        lightTransform.mul(trans,lightTransform);
        alternateRootObj.removeChild(transformedPointLightObject);
        currentTransformGroup.setTransform(lightTransform);
        rootObject.getGeometryObj().addChild(transformedPointLightObject);
        mouseZoom.setFactor(mouseZoomFactor);
        moveScene();
        transformedPointLightObject = null;
    }

    public void stopLightTransform()
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame)
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming object");
        stopDirectionalLightTransform();
        stopPointLightTransform();
    }

    public void transformLight(LightObject transformedLightObj) {
        if (transformedDirectionalLightObject != null)
            stopDirectionalLightTransform();
        if (transformedPointLightObject != null)
            stopPointLightTransform();
        if (transformedLightObj == null)
            return;
        if (transformedLightObj.getLight().getType() == DIRECTIONAL)
            transformDirectionalLight ((DirectionalLightObject)transformedLightObj);
        if (transformedLightObj.getLight().getType() == POINT)
            transformPointLight((PointLightObject)transformedLightObj);
    }

    private void transformDirectionalLight(DirectionalLightObject transformedLightObj)
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame)
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming directional light");
        setTransformedNode(TRANSFORMED_LIGHT);
        mouseRotate.setTransformGroup(transformedLightObj.getTransformGroup());
        transformedDirectionalLightObject = transformedLightObj;
    }

    private void transformPointLight(PointLightObject transformedLightObj)
    {
        if (parentFrame != null && parentFrame instanceof Display3DInternalFrame)
            ((Display3DInternalFrame) parentFrame).getTransformMenu().setText("transforming point light");
        transformedPointLightObject = transformedLightObj;
        if (transformedPointLightObject != null) {
            transformedPointLightObject.updateScale(rootDim, sceneCenter);
            currentTransformGroup  = transformedPointLightObject.getLightTransformGroup();
            currentParentGroup = (OpenBranchGroup)transformedPointLightObject.getParent();
            rootObject.getGeometryObj().removeChild(transformedPointLightObject);
            Transform3D lightTransform = new Transform3D();
            currentTransformGroup.getTransform(lightTransform);
            Transform3D trans = LocalToWindow.getTransform(rootObject.getGeometryObj());
            lightTransform.mul(trans,lightTransform);
            currentParentGroup.removeChild(transformedPointLightObject);
            currentTransformGroup.setTransform(lightTransform);
            alternateRootObj.addChild(transformedPointLightObject);
            setTransformedNode(TRANSFORMED_LIGHT);
            mouseTranslate.setTransformGroup(currentTransformGroup);
            mouseZoom.setTransformGroup(currentTransformGroup);
            mouseZoom.setFactor(.05 * mouseZoomFactor);
        }
    }

    public void resetPointLightPosition(PointLightObject obj)
    {
        obj.getLightTransformGroup().setTransform(new Transform3D());
    }

    private void initializeOnceCrosshairCursor()
    {
        if (crosshairCursor == null) {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            BufferedImage crosshairCursorImage = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
            int[] crosshairCursorRaster = new int[32 * 32];
            for (int i = 0; i < crosshairCursorRaster.length; i++) {
                crosshairCursorRaster[i] = 0;
            }
            for (int i = 0; i < 14; i++) {
                crosshairCursorRaster[15 * 32 + i] = crosshairCursorRaster[15 * 32 + 31 - i]
                    = crosshairCursorRaster[15 + 32 * i] = crosshairCursorRaster[15 + 32 * (31 - i)] = 0xffffffff;
            }
            for (int i = 0; i < 7; i++) {
                crosshairCursorRaster[14 * 32 + i] = crosshairCursorRaster[14 * 32 + 31 - i]
                    = crosshairCursorRaster[16 * 32 + i] = crosshairCursorRaster[16 * 32 + 31 - i]
                    = crosshairCursorRaster[14 + 32 * i] = crosshairCursorRaster[14 + 32 * (31 - i)]
                    = crosshairCursorRaster[16 + 32 * i] = crosshairCursorRaster[16 + 32 * (31 - i)] = 0xff888888;
            }
            crosshairCursorImage.setRGB(0, 0, 32, 32, crosshairCursorRaster, 0, 32);
            crosshairCursor = toolkit.createCustomCursor(crosshairCursorImage, new Point(15, 15), "crosshairCursor");
        }
    }

    public void setPick3DActive(boolean pick3DActive)
    {
        this.pick3DActive = pick3DActive;
        if (!pick3DActive) {
            this.removeChild(pickObject.getOutObject());
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        } else {
            this.addChild(pickObject.getOutObject());
            initializeOnceCrosshairCursor();
            setCursor(crosshairCursor);
        }

        this.pickObject.setActive(pick3DActive);
    }

    public void setCrosshairCursor(Cursor cursor)
    {
        crosshairCursor = cursor;
    }

    public Bounds getJ3DBounds()
    {
        return bounds;
    }

    public SimpleUniverse getUniverse()
    {
        return universe;
    }

    public ModelClip getModelClip()
    {
        return modelClip;
    }

    public void setClipRange(double front, double back)
    {
        view.setFrontClipDistance(front);
        view.setBackClipDistance(back);
    }

    public void setParentFrame(JInternalFrame parentFrame)
    {
        this.parentFrame = parentFrame;
    }

    public void setFoV(int fov)
    {
        if (fov < 20) {
            fov = 20;
        }
        if (fov > 160) {
            fov = 160;
        }
        view.setFieldOfView(PI * fov / 180.);
    }

    public void setReperSize(int size)
    {
        reperSize = size;
        if (size > 10) {
            resizeAxesGlyph();
            if (!showReper) {
                objReper.addChild(reper.getGeometryObj());
            }
            showReper = true;
        } else {
            objReper.removeAllChildren();
            showReper = false;
        }
    }

    private void resizeAxesGlyph()
    {
        float s = (float) reperSize / this.getWidth() + .05f;
        float y = (float) this.getHeight() / this.getWidth();
        positionedReper.setTransform(new Transform3D(new float[]{s, 0, 0, -(1 - s),
                                                                 0, s, 0, -(y - s),
                                                                 0, 0, s, 0,
                                                                 0, 0, 0, 1}));
    }

    public void clearCanvas()
    {
        try {
            canvas.stopRenderer();
            Thread.sleep(200);
            SwingInstancer.swingRunLater(() -> {
                remove(canvas);
            });
            Thread.sleep(200);
            rootObject.clearAllGeometry();
            Thread.sleep(200);
            canvas = null;
        } catch (InterruptedException e) {
        }
    }

    public void setTransientControlsFrame(JFrame transientControlsFrame)
    {
        this.transientControlsFrame = transientControlsFrame;
        this.transientControlsFrame.addWindowListener(new WindowListener()
        {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {
                transformPointLight(null);
                setLightsVisible(false);
                controlsPanel.getLightsEditor().changeEditorDisplay(false);
            }
            @Override
            public void windowClosed(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e)
            {
                if (controlsPanel != null) {
                    setLightsVisible(controlsPanel.isLightsPanelVisible());
                    controlsPanel.getLightsEditor().changeEditorDisplay(controlsPanel.isLightsPanelVisible());
                }
            }
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        resetViewPopup = new javax.swing.JPopupMenu();
        topButton = new javax.swing.JMenuItem();
        bottomButton = new javax.swing.JMenuItem();
        frontButton = new javax.swing.JMenuItem();
        backButton = new javax.swing.JMenuItem();
        leftButton = new javax.swing.JMenuItem();
        rightButton = new javax.swing.JMenuItem();

        topButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_PAGE_UP, java.awt.event.InputEvent.ALT_DOWN_MASK));
        topButton.setText("top");
        resetViewPopup.add(topButton);

        bottomButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_PAGE_DOWN, java.awt.event.InputEvent.ALT_DOWN_MASK));
        bottomButton.setText("bottom");
        resetViewPopup.add(bottomButton);

        frontButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DOWN, java.awt.event.InputEvent.ALT_DOWN_MASK));
        frontButton.setText("front");
        resetViewPopup.add(frontButton);

        backButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_UP, java.awt.event.InputEvent.ALT_DOWN_MASK));
        backButton.setText("back");
        resetViewPopup.add(backButton);

        leftButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_LEFT, java.awt.event.InputEvent.ALT_DOWN_MASK));
        leftButton.setText("left");
        resetViewPopup.add(leftButton);

        rightButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_RIGHT, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rightButton.setText("right");
        resetViewPopup.add(rightButton);

        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentResized(java.awt.event.ComponentEvent evt)
            {
                formComponentResized(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        try {
            controlsPanel.setWindowDimensions(this.getWidth(), this.getHeight());
            resizeAxesGlyph();
            fireResize();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_formComponentResized

    public static enum ReperType
    {
        AXES, MEDICAL
    };

    public void setReperType(ReperType reperType)
    {
        objReper.removeAllChildren();
        switch (reperType) {
            case AXES:
                reper = new Reper();
                break;
            case MEDICAL:
                reper = new MedicalReper();
                break;
        }
        objReper.addChild(reper.getGeometryObj());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem backButton;
    private javax.swing.JMenuItem bottomButton;
    private javax.swing.JMenuItem frontButton;
    private javax.swing.JMenuItem leftButton;
    private javax.swing.JPopupMenu resetViewPopup;
    private javax.swing.JMenuItem rightButton;
    private javax.swing.JMenuItem topButton;
    // End of variables declaration//GEN-END:variables
    private boolean postRenderSilent = false;

    public void setPostRenderSilent(boolean value)
    {
        if (storingFrames) {
            if (!value) {
                if (!waitForExternalTrigger) {
                    canvas.startRenderer();
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ex) {
                    }
                }
                this.postRenderSilent = value;
            } else {
                this.postRenderSilent = value;
                canvas.stopRenderer();
            }
        } else {
            this.postRenderSilent = value;
        }
    }

    public void forceRender()
    {
        canvas.stopRenderer();
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
        }
        waitForExternalTrigger = false;
        canvas.startRenderer();
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
        }
    }

    protected boolean waitForExternalTrigger = false;

    public void setWaitForExternalTrigger(boolean value)
    {
        this.waitForExternalTrigger = value;
    }

    public boolean isWaitingForExternalTrigger()
    {
        return waitForExternalTrigger;
    }

    public Display3DControlsPanel getControlsPanel()
    {
        return controlsPanel;
    }

    public CheatSheet getCheatSheet()
    {
        return cheatSheet;
    }

    @Override
    public void refresh()
    {
        canvas.getCanvas3D().getView().repaint();
//        forceRender();
    }

    public LocalToWindow getLocToWin()
    {
        return locToWin;
    }

    protected boolean lockView = false;

    /**
     * @return the lockView
     */
    public boolean isLockView()
    {
        return lockView;
    }

    /**
     * @param lockView the lockView to set
     */
    public void setLockView(boolean lockView)
    {
        this.lockView = lockView;
        if (this.parentFrame != null && parentFrame instanceof Display3DInternalFrame) {
            ((Display3DInternalFrame) parentFrame).setLockInfo(lockView);
        }
        if (this.controlsPanel != null) {
            controlsPanel.setLockViewButtonState(lockView);
        }

        mouseRotate.setEnable(!lockView);
        mouseTranslate.setEnable(!lockView);
        mouseZoom.setEnable(!lockView);
    }

    public JInternalFrame getParentFrame()
    {
        return parentFrame;
    }

    /**
     * This method should prevent viewer from keeping huge reper in some situations, especially on
     * Mac OS X.
     */
    public void kick()
    {
        objReper.refreshTransform();
        if (rootObject.getChildren().size() == 1)
            fullReset();
    }

    public Map<String, String> getRenderingProperties() {
        Map<String, String> map = new HashMap<>();
        if(canvas == null)
            return map;

        //get VirtualUniverse properties
        Map<String, String> vuPropertyMap = org.jogamp.java3d.VirtualUniverse.getProperties();
        //get Canvas3D properties
        Map<String, String> canvasPropertyMap = canvas.getCanvas3D().queryProperties();


        String j3dVersion = (String) vuPropertyMap.get("j3d.version");                  //e.g. 1.5.2 fcs (build4)
        String j3dPipeline = (String) vuPropertyMap.get("j3d.pipeline");                //e.g. NATIVE_OGL
        String j3dRenderer = (String) vuPropertyMap.get("j3d.renderer");                //e.g. OpenGL

        String nativeVendor = (String) canvasPropertyMap.get("native.vendor");          //e.g. NVIDIA Corporation
        String nativeVersion = (String) canvasPropertyMap.get("native.version");        //e.g. 4.5.0 NVIDIA 346.59
        String nativeRenderer = (String) canvasPropertyMap.get("native.renderer");      //e.g. Quadro 6000/PCIe/SSE2

        map.put("native.renderer", nativeRenderer);
        map.put("native.vendor", nativeVendor);
        map.put("native.version", nativeVersion);
        map.put("j3d.version", j3dVersion);
        map.put("j3d.pipeline", j3dPipeline);
        map.put("j3d.renderer", j3dRenderer);

        return map;
    }


    public String getRenderingInfoAsString() {
        Map<String, String> propertyMap = getRenderingProperties();
        StringBuilder sb = new StringBuilder();
        return sb.append(" --- 3D Renderer Info --\n-").
                  append("Renderer:        ").append(propertyMap.get("native.renderer")).append("\n").
                  append("Vendor:          ").append(propertyMap.get("native.vendor")).append("\n").
                  append("Version:         ").append(propertyMap.get("native.version")).append("\n").
                  append("Java3D version:  ").append(propertyMap.get("j3d.version")).append("\n").
                  append("Java3D pipeline: ").append(propertyMap.get("j3d.pipeline")).append("\n").
                  append("Java3D renderer: ").append(propertyMap.get("j3d.renderer")).append("\n").toString();
    }

    public String getRenderingInfoAsHTML() {
        Map<String, String> propertyMap = getRenderingProperties();
        StringBuilder sb = new StringBuilder();
        return sb.append("<html>").append("<table>").
                  append("<tr colspan=2><th>3D Renderer Info</th></tr>").
                  append("<tr><td>Renderer:</td><td>").       append(propertyMap.get("native.renderer")).append("</td></tr>").
                  append("<tr><td>Vendor:</td><td>").         append(propertyMap.get("native.vendor")).  append("</td></tr>").
                  append("<tr><td>Version:</td><td>").        append(propertyMap.get("native.version")). append("</td></tr>").
                  append("<tr><td>Java3D version:</td><td>"). append(propertyMap.get("j3d.version")).    append("</td></tr>").
                  append("<tr><td>Java3D pipeline:</td><td>").append(propertyMap.get("j3d.pipeline")).   append("</td></tr>").
                  append("<tr><td>Java3D renderer:</td><td>").append(propertyMap.get("j3d.renderer")).   append("</td></tr>").
                  append("</table>").append("</html>").toString();
    }

    @Override
    public void addFrameRenderedListener(FrameRenderedListener l)
    {
        if (l != null) {
            synchronized (frameRenderedListeners) {
                frameRenderedListeners.add(l);
            }
        }
    }

    @Override
    public void removeFrameRenderedListener(FrameRenderedListener l)
    {
        if (l != null) {
            synchronized (frameRenderedListeners) {
                frameRenderedListeners.remove(l);
            }
        }
    }

    protected void fireFrameRendered()
    {
        //        System.out.println("firing frame rendered "+System.currentTimeMillis());
        FrameRenderedEvent e = new FrameRenderedEvent(this);
        synchronized (frameRenderedListeners) {
            for (FrameRenderedListener l : frameRenderedListeners) {
                l.frameRendered(e);
            }
        }
        animate();
    }

    @Override
    public void addBgrColorListener(ColorListener l)
    {
        if (l != null) {
            bgrColorListeners.add(l);
        }
    }

    @Override
    public void removeBgrColorListener(ColorListener l)
    {
        if (l != null) {
            bgrColorListeners.remove(l);
        }
    }

    protected void fireBgrColorChanged()
    {
        ColorEvent e = new ColorEvent(this, ColorMapper.convertColor3fToColor(bgColor));
        for (ColorListener l : bgrColorListeners) {
            l.colorChoosen(e);
        }
    }

    @Override
    public void addLightDirectionListener(LightDirectionListener l)
    {
        if (l != null && !lightDirectionListeners.contains(l)) {
            lightDirectionListeners.add(l);
        }

    }

    @Override
    public void removeLightDirectionListener(LightDirectionListener l)
    {
        if (l != null) {
            lightDirectionListeners.remove(l);
        }
    }

    protected void fireLightDirectionChanged()
    {
        LightDirectionEvent e = new LightDirectionEvent(this, directionalLights.get(0).getLight().getLight());
        for (LightDirectionListener l : lightDirectionListeners) {
            l.lightChanged(e);
        }
    }

    /**
     * Forwards this request to a PickObject object.
     * @param listener a listener to be added
     */
    @Override
    public final void addPick3DListener(Pick3DListener listener)
    {
        pickObject.addPick3DListener(listener);
    }

    /**
     * removes listener
     * @param listener a listener to be removed
     */
    @Override
    public void removePick3DListener(Pick3DListener listener)
    {
        pickObject.removePick3DListener(listener);
    }

    /**
     * Utility field holding list of MouseRestingListeners.
     */
    private transient ArrayList<MouseRestingListener> mouseRestingListenerList = new ArrayList<>();

    /**
     * Registers MouseRestingListener to receive events.
     *
     * @param listener The listener to register.
     */
    @Override
    public synchronized void addMouseRestingListener(MouseRestingListener listener)
    {
        if (listener != null) {
            mouseRestingListenerList.add(listener);
        }
    }

    /**
     * Removes MouseRestingListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    @Override
    public synchronized void removeMouseRestingListener(MouseRestingListener listener)
    {
        if (listener != null) {
            mouseRestingListenerList.remove(listener);
        }
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param source object.
     * @param ix     x mouse position.
     * @param iy     y mouse position.
     *
     */
    private synchronized void fireMouseResting(int ix, int iy)
    {
        Point pt = getLocationOnScreen();
        MouseRestingEvent e = new MouseRestingEvent(this, ix, iy, ix + pt.x, pt.y + iy);
        for (MouseRestingListener listener : mouseRestingListenerList) {
            listener.mouseResting(e);
        }
    }

    /**
     * Utility field holding list of ResizeListeners.
     */
    private transient ArrayList<ResizeListener> resizeListenerList = new ArrayList<>();

    /**
     * Registers ResizeListener to receive events.
     *
     * @param listener The listener to register.
     */
    @Override
    public synchronized void addResizeListener(ResizeListener listener)
    {
        if (listener != null) {
            resizeListenerList.add(listener);
        }
    }

    /**
     * Removes ResizeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    @Override
    public synchronized void removeResizeListener(ResizeListener listener)
    {
        if (listener != null) {
            resizeListenerList.remove(listener);
        }
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param source object.
     * @param ix     x mouse position.
     * @param iy     y mouse position.
     *
     */
    private synchronized void fireResize()
    {
        Point pt = getLocationOnScreen();
        ResizeEvent e = new ResizeEvent(this, getWidth(), getHeight());
        for (ResizeListener listener : resizeListenerList) {
            listener.resized(e);
        }
    }
}
