/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Field3DDisplayPanel extends javax.swing.JPanel
{

    Display3DPanel display3DPanel = new Display3DPanel();

    /**
     * Creates new form Field3DDisplayPanel
     */
    public Field3DDisplayPanel()
    {
        initComponents();
        display3DPanel.setMinimumSize(new java.awt.Dimension(300, 200));
        display3DPanel.setPreferredSize(new java.awt.Dimension(400, 300));
        volPanel.add(display3DPanel, java.awt.BorderLayout.CENTER);
    }

    public Display3DPanel getDisplay3DPanel()
    {
        return display3DPanel;
    }

    public javax.swing.JPanel getXyPanel()
    {
        return xyPanel;
    }

    public javax.swing.JPanel getXzPanel()
    {
        return xzPanel;
    }

    public javax.swing.JPanel getYzPanel()
    {
        return yzPanel;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        xzPanel = new javax.swing.JPanel();
        yzPanel = new javax.swing.JPanel();
        xyPanel = new javax.swing.JPanel();
        volPanel = new javax.swing.JPanel();

        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(900, 800));
        setLayout(new java.awt.GridBagLayout());

        xzPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        xzPanel.setMinimumSize(new java.awt.Dimension(256, 256));
        xzPanel.setPreferredSize(new java.awt.Dimension(400, 400));
        xzPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(xzPanel, gridBagConstraints);

        yzPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        yzPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(yzPanel, gridBagConstraints);

        xyPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        xyPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.weighty = 1.0;
        add(xyPanel, gridBagConstraints);

        volPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        volPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(volPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel volPanel;
    private javax.swing.JPanel xyPanel;
    private javax.swing.JPanel xzPanel;
    private javax.swing.JPanel yzPanel;
    // End of variables declaration//GEN-END:variables

}
