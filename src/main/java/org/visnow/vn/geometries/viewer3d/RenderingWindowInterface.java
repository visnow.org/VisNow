/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import org.jogamp.java3d.Canvas3D;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.LightDirectionListener;
import org.visnow.vn.geometries.events.ProjectionListener;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickTypeListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import org.visnow.vn.lib.utils.events.MouseRestingListener;

/**
 * Interface implemented by Display3DPanel.
 * <p>
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public interface RenderingWindowInterface
{

    void addBgrColorListener(ColorListener l);

    void addFrameRenderedListener(FrameRenderedListener l);

    void addProjectionListener(ProjectionListener listener);

    void addPick3DListener(Pick3DListener listener);
    
    void addLightDirectionListener(LightDirectionListener l);
    
    void addMouseRestingListener(MouseRestingListener listener);
    
    void addResizeListener(ResizeListener listener);

    void removeBgrColorListener(ColorListener l);
    
    void removeLightDirectionListener(LightDirectionListener l);

    void removeFrameRenderedListener(FrameRenderedListener l);

    void removeProjectionListener(ProjectionListener listener);

    void removePick3DListener(Pick3DListener listener);
    
    void removeMouseRestingListener(MouseRestingListener listener);
    
    void removeResizeListener(ResizeListener listener);
    
    Canvas3D getCanvas();

    int getWidth();

    int getHeight();

    void refresh();

    PickTypeListener getPickTypeListener();
}
