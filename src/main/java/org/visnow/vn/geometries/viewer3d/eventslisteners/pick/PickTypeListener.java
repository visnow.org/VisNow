/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.pick;

/**
 * Interface for a class that listens to changes in active types of pick.
 * <p/>
 * <code>PickTypeListener</code> object collects information about active picks and forwards them to
 * the active pointer (in future: active pointers). Pointers are responsible of displaying icons and shapes
 * that depict active picks. They can also ignore types of picks, but it's not recommended.
 * <p/>
 * The only one such class is <code>PickObject</code>.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface PickTypeListener
{

    /**
     * When a pick of type
     * <code>pickType</code> was just turned on.
     */
    void onPickTurnedOn(PickType pickType);

    /**
     * When a pick of type
     * <code>pickType</code> was just turned off.
     */
    void onPickTurnedOff(PickType pickType);
}
