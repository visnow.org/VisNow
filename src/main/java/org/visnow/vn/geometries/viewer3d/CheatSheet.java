//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

/**
 *
 * @author know
 */
public class CheatSheet extends javax.swing.JFrame
{

    /**
     * Creates new form CheatSheet
     */
    public CheatSheet()
    {
        initComponents();
        mouseActions.setText(
            "<html> <table> "  +
            "<tr valign=>" +
            "<td><p><b>Linux/Windows</b></p></td>" +
            "<td><p><b>&#63743; macOS</b></p></td>" +
            "<td><p><b>Action</b></p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>LMB double click / MMB click</p></td>" +
            "<td><p>double click</p></td>" +
            "<td><p>resets scene rotation so that the x axis is horizontal, the y axis is" +
            "	vertical,  the current center is at the center of the window and" +
            "	the scale is normalized</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+LMB click</p></td>" +
            "<td><p>&#8963;+click</p></td>" +
            "<td><p>brings forward GUI of the module that created the clicked object</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+Shift+LMB click</p></td>" +
            "<td><p>&#8963;+&#8679;+click</p></td>" +
            "<td><p>if the3D annotation module is active and some object has been clicked  a" +
            "	new editable label is created at the clicked point</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+Alt+LMB click</p></td>" +
            "<td><p>&#8963;+&#8997;+click</p></td>" +
            "<td><p>if some object has been clicked the rotation and scaling center is" +
            "	set to the clicked point</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>LMB drag</p></td>" +
            "<td><p>drag</p></td>" +
            "<td><p>rotates the scene (virtual ball) around the current center</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>RMB click</p></td>" +
            "<td><p>secondary click</p></td>" +
            "<td><p>displays the 3D window controls</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+RMB click</p></td>" +
            "<td><p>&#8963;+&#8997;+secondary click</p></td>" +
            "<td><p>displays standard views menu</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>RMB drag</p></td>" +
            "<td><p>secondary drag</p></td>" +
            "<td><p>translates the scene (the scene center is also translated)</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+Alt+MMB click</p></td>" +
            "<td><p>use keyboard &#8997;+&#9003;</p></td>" +
            "<td><p>sets the center point to the geometric center of the scene and then" +
            "	resets the scene as on the MMB click</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>mouse wheel</p></td>" +
            "<td><p>scroll</p></td>" +
            "<td><p>zoom in/out the scene</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>MMB drag</p></td>" +
            "<td><p><br/></p></td>" +
            "<td><p>translates the scene in the z direction; the results are practically" +
            "	invisible (except from clipping and depth cueing) when the" +
            "	parallel projection is selected and are similar to scene scaling" +
            "	in the case of perspective projection</p></td>" +
            "</tr>" +
            "</table>" +
            "<p></p>" +
            "<p><b>Click-drag-click - a 3D space point selection</b></p>" +
            "<p>A 3D point can be picked for annotation or scene centering purpose at any place " +
            "	(inside or outside of displayed objects) using the \"click-drag-click\" trick.</p>" +
            "<p>To activate this mode, type '.' (period) in the 3D window or select the \"pick 3D active\" " +
            "	checkbox in the 3D window controls frame. The cursor changes its form to a crosshair.</p>" +
            "<p>Click the LMB in the desired point location and note a small green glyph under the cursor." +
            "	A ray (a straight line in the 3D scene) has been selected. "  +
            "<p>Rotate now the scene with the LMB drag and observe the selected ray as " +
            "	a blue line with two green glyphs at the ends."  +
            "       Locate the desired 3D point somewhere at this line "  +
            "       and Ctrl+Shift+LMB click to annotate or Ctrl+Alt+LMB click" +
            "	to set new scene center. (The window content will then snap to the starting position." +
            "	</p></html>");
        keyboardShortcuts.setText(
            "<html> <table> "  +
            "<tr valign=>" +
            "<td><p><b>Linux/Windows</b></p></td>" +
            "<td><p><b>&#63743; macOS</b></p></td>" +
            "<td><p><b>Action</b></p>" +
            "</td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Space</p></td>" +
            "<td><p>Space</p></td>" +
            "<td><p>displays the 3D window controls</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>c</p></td>" +
            "<td><p>c</p></td>" +
            "<td><p>mouse and mouse wheel will control camera position and rotation</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>o</p></td>" +
            "<td><p>o</p></td>" +
            "<td><p>mouse and mouse wheel will control scene (objects) position and rotation</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>s</p></td>" +
            "<td><p>s</p></td>" +
            "<td><p>toggles on/off stereo rendering (when available)</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>f</p></td>" +
            "<td><p>f</p></td>" +
            "<td><p>toggles full screen display on/off</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>.</p></td>" +
            "<td><p>.</p></td>" +
            "<td><p>toggles on/off the click-drag-click 3D point selection mode</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>m</p></td>" +
            "<td><p>m</p></td>" +
            "<td><p>when an object is selected by Ctrl+LMB click toggles scene  " +
            "transform mode to the selected object transform mode</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+i</p></td>" +
            "<td><p>&#8963;+i</p></td>" +
            "<td><p>toggles on/of rendering information display </p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+f</p></td>" +
            "<td><p>&#8963;+f</p></td>" +
            "<td><p>toggles on/of FPS display </p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Backspace</p><p>or Num5</p></td>" +
            "<td><p>&#9003;</p></td>" +
            "<td><p>resets scene rotation so that the x axis is horizontal, the y axis is" +
            "vertical, the current center is at the center of the window and" +
            "the scale is normalized<</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Alt+Backspace</p><p>or Alt+Num5</p></td>" +
            "<td><p>&#8997;+&#9003;</p></td>" +
            "<td><p>sets the center pooint to the geometric center of the scene and then " +
            "resets the scene as on the MMB click; use when some weird object positioning occured</p></td>" +
            "<tr valign=>" +
            "<td><p>Ctrl+c</p></td>" +
            "<td><p>&#8963;+c</p></td>" +
            "<td><p>stores current view position </p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+v</p></td>" +
            "<td><p>&#8963;+v</p></td>" +
            "<td><p>restores last stored view position </p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>?</p></td>" +
            "<td><p>?</p></td>" +
            "<td><p>displays this help (cheat sheet) page</p></td>" +
            "<tr valign=>" +
            "<td><p>cursor keys</p><p>or Num2/4/6/8</p></td>" +
            "<td><p>cursor keys</p></td>" +
            "<td><p>rotate the scene in the numpad emulated cursor direction by 1&deg; increments</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Shift+cursor keys</p><p>or Shift+Num2/4/6/8</p></td>" +
            "<td><p>&#8679;+cursor keys</p></td>" +
            "<td><p>rotate the scene in the numpad emulated cursor direction by 30&deg; increments</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+cursor keys</p><p>or Ctrl+Num2/4/6/8</p></td>" +
            "<td><p>&nbsp;</p></td>" +
            "<td><p>rotate the scene in the numpad emulated cursor direction by 90&deg; increments</p></td>" +
            "</tr>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Alt+cursor keys, </p><p>Alt+PgUp/PgDn</p><p>or Alt+Num2/4/6/8/3/9</p></td>" +
            "<td><p>&#8997;+cursor keys, </p><p>&#8997;+PgUp/PgDn</p><p>or &#8997;+Num2/4/6/8/3/9</p></td>" +
            "<td><p>resets current view </p><p>to top, bottom, front, back, left or right</p><p>respectively</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>PgUp/PgDown</p><p>or Num3/9</p></td>" +
            "<td><p>fn+up/down</p></p></td>" +
            "<td><p>rotate the scene around Z axis by 1&deg; increments</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Shift+PgUp/PgDown</p><p>or Shift+Num3/9</p></td>" +
            "<td><p>&#8679;+fn+up/down</p></td>" +
            "<td><p>rotate the scene around Z axis by 30&deg; increments</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Ctrl+PgUp/PgDown</p><p> or Ctrl+Num3/9</p></td>" +
            "<td><p>&#8963;+fn+up/down</p></td>" +
            "<td><p>rotate the scene around Z axis by 90&deg; increments</p></td>" +
            "</tr>" +
            "<tr valign=>" +
            "<td><p>Home/End</p><p>or Num1/7</p></td>" +
            "<td><p>fn+left/right</p></td>" +
            "<td><p>zoom in/out the scene</p></td>" +
            "</tr>" +
            "</table>");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        mouseActions = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        keyboardShortcuts = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(890, 910));

        jTabbedPane1.setMinimumSize(new java.awt.Dimension(800, 800));
        jTabbedPane1.setName(""); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(890, 900));
        jTabbedPane1.setRequestFocusEnabled(false);

        jPanel1.setFocusable(false);
        jPanel1.setMinimumSize(new java.awt.Dimension(300, 200));
        jPanel1.setLayout(new java.awt.BorderLayout());

        mouseActions.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        mouseActions.setText("jLabel1");
        mouseActions.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel1.add(mouseActions, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Mouse controls", jPanel1);

        jPanel2.setFocusable(false);
        jPanel2.setMinimumSize(new java.awt.Dimension(800, 800));
        jPanel2.setName(""); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(890, 900));
        jPanel2.setLayout(new java.awt.BorderLayout());

        keyboardShortcuts.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        keyboardShortcuts.setText("jLabel1");
        keyboardShortcuts.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel2.add(keyboardShortcuts, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Keyboard shortcuts", jPanel2);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CheatSheet.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CheatSheet.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CheatSheet.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CheatSheet.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new CheatSheet().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel keyboardShortcuts;
    private javax.swing.JLabel mouseActions;
    // End of variables declaration//GEN-END:variables

//HTML codes for macOS keyboard symbols
//HTML Entity     GLYPH  NAME
//&#63743;              Apple
//&#8984;         ⌘      Command, Cmd, Clover, (formerly) Apple
//&#8963;         ⌃      Control, Ctl, Ctrl
//&#8997;         ⌥      Option, Opt, (Windows) Alt
//&#8679;         ⇧      Shift
//&#8682;         ⇪      Caps lock
//&#9167;         ⏏      Eject
//&#8617;         ↩      Return, Carriage Return
//&#8629; &crarr; ↵      Return, Carriage Return
//&#9166;         ⏎      Return, Carriage Return
//&#8996;         ⌤      Enter
//&#9003;         ⌫      Delete, Backspace
//&#8998;         ⌦      Forward Delete
//&#9099;         ⎋      Escape, Esc
//&#8594; &rarr;  →      Right arrow
//&#8592; &larr;  ←      Left arrow
//&#8593; &uarr;  ↑      Up arrow
//&#8595; &darr;  ↓      Down arrow
//&#8670;         ⇞      Page Up, PgUp
//&#8671;         ⇟      Page Down, PgDn
//&#8598;         ↖      Home
//&#8600;         ↘      End
//&#8999;         ⌧      Clear
//&#8677;         ⇥      Tab, Tab Right, Horizontal Tab
//&#8676;         ⇤      Shift Tab, Tab Left, Back-tab
//&#9250;         ␢      Space, Blank
//&#9251;         ␣      Space, Blank

}
