/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.java3d.Light;
import org.jogamp.vecmath.Color3f;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
abstract public class EditableLight
{
    public static enum LightType {AMBIENT, DIRECTIONAL, POINT};
    protected static final Color3f DEFAULT_LIGHT_COLOR =  new Color3f(1, 1, 1);

    protected Light light;
    protected String lightName = "light";
    protected Color3f lightColor = new Color3f(DEFAULT_LIGHT_COLOR);
    protected boolean enabled = true;
    protected LightType type;

    public final void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        light.setEnable(enabled);
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setLightColor(Color3f lightCol)
    {
        lightCol.scale(.4f);
        lightColor = new Color3f(lightCol);
        light.setColor(lightColor);
    }

    public Color3f getLightColor()
    {
        return lightColor;
    }

    public String getLightName()
    {
        return lightName;
    }

    public LightType getType()
    {
        return type;
    }

    abstract public void reset();

}
