/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.java3d.AmbientLight;
import org.jogamp.java3d.Bounds;
import org.jogamp.java3d.Light;
import org.jogamp.vecmath.Color3f;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class EditableAmbientLight extends EditableLight
{

    protected final Color3f stdLightCol;

    public EditableAmbientLight(Color3f lightCol, Bounds bounds,
                                String name, boolean enabled)
    {
        type = LightType.AMBIENT;
        stdLightCol = lightColor == null ? new Color3f(DEFAULT_LIGHT_COLOR) : lightColor;
        this.lightColor = stdLightCol;
        light = new AmbientLight(lightColor);
        lightName = name;
        light.setCapability(Light.ALLOW_STATE_WRITE);
        light.setCapability(Light.ALLOW_COLOR_WRITE);
        setLightColor(lightCol);
        light.setInfluencingBounds(bounds);
        setEnabled(enabled);
    }

    public EditableAmbientLight(Bounds bounds, String name)
    {
        this(new Color3f(new Color3f(DEFAULT_LIGHT_COLOR)), bounds, name, true);
    }

    public AmbientLight getLight()
    {
        return (AmbientLight)light;
    }

    @Override
    public void reset()
    {
        setLightColor(new Color3f(stdLightCol));
    }
}
