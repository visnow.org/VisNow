//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;


import org.jogamp.java3d.BoundingSphere;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.AxisAngle4f;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Vector3f;

/**
 *
 * @author know
 */
public class DirectionalLightsFactory
{
    private static final Color3f[] DIRECTIONAL_LIGHTS_COLORS = new Color3f[]
            {new Color3f(0.7f, 0.7f, 0.7f),
             new Color3f(0.3f, 0.25f, 0.2f),
             new Color3f(0.23f, 0.2f, 0.25f),
             new Color3f(0.1f, 0.1f, 0.15f)};
    private static final Vector3f INIT_LIGHT_DIRECTION = new Vector3f(0, 0, -1);
    private static final Vector3f[] LIGHT_DIRECTIONS = new Vector3f[]
            {new Vector3f(.1f, .08f, -.5f),
             new Vector3f(-1.f, -0.4f, -.5f),
             new Vector3f(0.f, 0.6f, -1.f),
             new Vector3f(0.f, -0.6f, -1.f)};
    public static final DirectionalLightObject createLight(int i, BoundingSphere bounds)
    {

            if (i >= 4)
                return new DirectionalLightObject(
                           new EditableDirectionalLight(bounds,
                                                     "directional light " + i),
                           new Transform3D());
            else {
                Vector3f axis = new Vector3f();
                float angle = (float)LIGHT_DIRECTIONS[i].angle(INIT_LIGHT_DIRECTION);
                axis.cross(LIGHT_DIRECTIONS[i],INIT_LIGHT_DIRECTION);
                AxisAngle4f rotationAngle = new AxisAngle4f(axis.x, axis.y, axis.z, angle);
                Transform3D transform = new Transform3D();
                transform.setRotation(rotationAngle);
                EditableDirectionalLight light = new EditableDirectionalLight(bounds,
                                                              "directional light " + i);
                light.setLightColor(DIRECTIONAL_LIGHTS_COLORS[i]);
                return new DirectionalLightObject(light, transform);
            }
    }

}
