/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.MoviesManager;

import static java.lang.Math.PI;
import org.jogamp.java3d.BranchGroup;
import org.jogamp.java3d.PointLight;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Matrix3d;
import org.jogamp.vecmath.Vector3d;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.vn.geometries.geometryTemplates.Reper;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.viewer3d.ObjRotate;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This class holds information about Camera parameters such as geometry, fov or light.
 * Information about scene and camera object transformations are provided through listeners mechanisms and are used to recalculate camera view, so it  remains invariant.
 * 
 */


public class Camera implements ObjRotate.Listener, TransformCameraGroup.Listener{

    
    private OpenBranchGroup geometryObject;
    private CameraView cameraView;
    private PointLight light;
    private Transform3D sceneTransform;
    private Transform3D cameraTransform;
    
    /**
     * init geometry object, camera view and camera light.
     */
    public Camera(){
        Reper reper = new Reper();
        geometryObject = reper.getGeometryObj();
        
        cameraView = new CameraView();
        
        light = new PointLight();
        light.setEnable(false);
        light.setColor(new Color3f(0.8f,0.8f,0.8f));
        light.setCapability(PointLight.ALLOW_STATE_WRITE);
        light.setCapability(PointLight.ALLOW_POSITION_WRITE);
        light.setCapability(PointLight.ALLOW_ATTENUATION_WRITE);
        light.setCapability(PointLight.ALLOW_INFLUENCING_BOUNDS_WRITE);
        
        sceneTransform = new Transform3D();
        cameraTransform = new Transform3D();
    }
    
    /**
     * returns camera geometry object
     * @return 
     */

    public OpenBranchGroup getGeometryObject() {
        return geometryObject;
    }
    
    /**
     * set camera transform 
     * @param transform - new transform of the camera
     */

    public void setCameraTransform(Transform3D transform) {
        cameraTransform.set(transform);
    }

    /**
     * returns camera view branch group
     * @return 
     */
    public BranchGroup getRootBG() {
        return cameraView.getRootBG();
    }
    
    /**
     * camera transform is multiplied by the new scene transform so the camera view can remain invariant
     * @param newTransform new transform of the scene
     */

    @Override
    public void sceneTransformChanged(Transform3D newTransform) {
        sceneTransform.set(newTransform);
        Transform3D cameraViewTransform = new Transform3D();
        cameraViewTransform.mul(newTransform, cameraTransform);
        cameraView.sceneTransformChanged(cameraViewTransform);
    }
    
    /**
     * scene transform is multiplied by the new camera transform so the camera view can remain invariant
     * @param newTransform new transform of the camera
     */
    
    @Override
    public void transformCameraGroupChanged(Transform3D newTransform) {
        cameraTransform.set(newTransform);
        Transform3D cameraViewTransform = new Transform3D();
        cameraViewTransform.mul(sceneTransform, newTransform);
        cameraView.sceneTransformChanged(cameraViewTransform);
    }
    
    /**
     * validates and sets the new fov of the camera
     * @param fov 
     */
    
    public void setFoV(int fov){
        if (fov < 20) {
            fov = 20;
        }
        if (fov > 160) {
            fov = 160;
        }
        cameraView.setFieldOfView(PI * fov / 180.);
    }

    /**
     * returns camera point light
     * @return 
     */
    public PointLight getLight() {
        return light;
    }
    
    /**
     * on/off camera light
     * @param lightOnOff 
     */
    
    public void enableLight(boolean lightOnOff){
        light.setEnable(lightOnOff);
    }
    
    /**
     * show camera view
     */
    
    public void show(){
        cameraView.show();
    }
    
    /**
     * adds camera view window close listener
     * @param listener 
     */

    public void addCameraWindowCloseListener(CameraWindowCloseListener listener) {
        cameraView.addWindowCloseListener(listener);
    }
    
    /**
     * returns current camera transform
     * @return 
     */
    
    public Transform3D getCameraTransform(){
        Transform3D transform = new Transform3D();
        cameraView.getViewPlatformTransformGroup().getTransform(transform);
        return transform;
    }
    
    /**
     * This method returns camera rotation (idx - 0), position (idx - 1) and fov (idx - 2) as separate DoubleLargeArray. 
     * @return 
     */
    
    public DoubleLargeArray[] getCameraParamsAsTimeStepData(){
        Transform3D t = getCameraTransform();
        Matrix3d rot = new Matrix3d();
        Vector3d pos = new Vector3d();
        t.get(rot, pos);
        
        DoubleLargeArray rotation = new DoubleLargeArray(new double[]
        {rot.m00, rot.m01, rot.m02, 
         rot.m10, rot.m11, rot.m12, 
         rot.m20, rot.m21, rot.m22});
        DoubleLargeArray position = new DoubleLargeArray(new double[]{pos.x, pos.y, pos.z});
        DoubleLargeArray fov = new DoubleLargeArray(new double[]{cameraView.getView().getFieldOfView()});
        
        return new DoubleLargeArray[]{rotation, position, fov};
    }
}
