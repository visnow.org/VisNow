//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.geometries.viewer3d;

import org.jogamp.vecmath.Matrix3d;

/**
 *
 * @author know
 */
public enum StandardView
{
    TOP_VIEW     ("top",    new Matrix3d( 1.,  0., 0., 0., 1., 0.,  0., 0.,  1.)),
    BOTTOM_VIEW  ("bottom", new Matrix3d(-1.,  0., 0., 0., 1., 0.,  0., 0., -1.)),
    LEFT_VIEW    ("left",   new Matrix3d( 0., -1., 0., 0., 0., 1., -1., 0.,  0.)),
    RIGHT_VIEW   ("right",  new Matrix3d( 0.,  1., 0., 0., 0., 1.,  1., 0.,  0.)),
    FRONT_VIEW   ("front",  new Matrix3d(-1.,  0., 0., 0., 0., 1.,  0., 1.,  0.)),
    BACK_VIEW    ("back",   new Matrix3d( 1.,  0., 0., 0., 0., 1.,  0.,-1.,  0.));

    private final String name;
    private final Matrix3d mat;

    StandardView(String name, Matrix3d mat)
    {
        this.name = name;
        this.mat = mat;
    }

    public String getName()
    {
        return name;
    }

    public Matrix3d getMat()
    {
        return mat;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
