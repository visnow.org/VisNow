/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
import org.jogamp.java3d.utils.geometry.*;
import org.jogamp.java3d.utils.picking.behaviors.*;
import java.awt.*;
import org.jogamp.java3d.*;
import org.jogamp.vecmath.*;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenMaterial;
import static org.apache.commons.math3.util.FastMath.*;

class CameraView
{

    protected static final PhysicalBody physBody = new PhysicalBody();
    protected static final PhysicalEnvironment physEnv
        = new PhysicalEnvironment();
    protected BranchGroup rootBG = null;
    protected TransformGroup vpTG = null;
    protected ViewPlatform viewPlatform = null;
    protected View view = null;
    protected Canvas3D canvas = null;

    public CameraView()
    {

        GraphicsConfigTemplate3D gconfigTempl
            = new GraphicsConfigTemplate3D();
        GraphicsConfiguration gconfig
            = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getBestConfiguration(gconfigTempl);

        canvas = new Canvas3D(gconfig);

        view = new View();

        viewPlatform = new ViewPlatform();

        view.setPhysicalBody(physBody);
        view.setPhysicalEnvironment(physEnv);
        view.attachViewPlatform(viewPlatform);
        view.addCanvas3D(canvas);

        vpTG = new TransformGroup();
        vpTG.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        vpTG.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        vpTG.addChild(viewPlatform);

        rootBG = new BranchGroup();
        rootBG.setCapability(BranchGroup.ALLOW_DETACH);
        rootBG.addChild(vpTG);

    }

    public TransformGroup getViewPlatformTransformGroup()
    {
        return this.vpTG;
    }

    public BranchGroup getRootBG()
    {
        return this.rootBG;
    }

    public View getView()
    {
        return this.view;
    }

    public Canvas3D getCanvas3D()
    {
        return this.canvas;
    }
}

class UniverseBuilder
{

    private Locale locale = null;
    private CameraView zyCamera = null;
    private CameraView xyCamera = null;
    private CameraView xzCamera = null;

    public UniverseBuilder()
    {
    }

    public void addCameras()
    {

        VirtualUniverse universe = new VirtualUniverse();
        locale = new Locale(universe);

        zyCamera = new CameraView();
        TransformGroup vpTG = zyCamera.getViewPlatformTransformGroup();
        Transform3D xform = new Transform3D();
        Vector3f vec = new Vector3f(4.0f, 0.0f, 0.0f);
        xform.set(vec);
        Transform3D xform2 = new Transform3D();
        xform2.rotY(PI / 2.0);
        xform.mul(xform2);
        vpTG.setTransform(xform);
        View view = zyCamera.getView();
        view.setProjectionPolicy(View.PARALLEL_PROJECTION);

        xyCamera = new CameraView();
        vpTG = xyCamera.getViewPlatformTransformGroup();
        xform = new Transform3D();
        vec = new Vector3f(0.0f, 0.0f, 4.0f);
        xform.set(vec);
        vpTG.setTransform(xform);
        view = xyCamera.getView();
        view.setProjectionPolicy(View.PARALLEL_PROJECTION);

        xzCamera = new CameraView();
        vpTG = xzCamera.getViewPlatformTransformGroup();
        xform = new Transform3D();
        vec = new Vector3f(0.0f, 4.0f, 0.0f);
        xform.set(vec);
        xform2 = new Transform3D();
        xform2.rotX(-PI / 2.0);
        xform.mul(xform2);
        vpTG.setTransform(xform);
        view = xzCamera.getView();
        view.setProjectionPolicy(View.PARALLEL_PROJECTION);

        locale.addBranchGraph(zyCamera.getRootBG());
        locale.addBranchGraph(xyCamera.getRootBG());
        locale.addBranchGraph(xzCamera.getRootBG());

    }

    public void addScene()
    {

        ModelScene scene = new ModelScene();
        BranchGroup sceneBG = scene.createSceneGraph();
        addMouseBehaviors(sceneBG, zyCamera.getCanvas3D());
        addMouseBehaviors(sceneBG, xzCamera.getCanvas3D());
        addMouseBehaviors(sceneBG, xyCamera.getCanvas3D());

        locale.addBranchGraph(sceneBG);

    }

    public void addMouseBehaviors(
        BranchGroup rootBG,
        Canvas3D canvas)
    {
        BoundingSphere bounds
            = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);

        PickRotateBehavior rotbehavior
            = new PickRotateBehavior(rootBG, canvas, bounds);
        rootBG.addChild(rotbehavior);

        PickZoomBehavior zoombehavior
            = new PickZoomBehavior(rootBG, canvas, bounds);
        rootBG.addChild(zoombehavior);

        PickTranslateBehavior transbehavior
            = new PickTranslateBehavior(rootBG, canvas, bounds);
        rootBG.addChild(transbehavior);
    }

    public CameraView getZYCamera()
    {
        return this.zyCamera;
    }

    public CameraView getXYCamera()
    {
        return this.xyCamera;
    }

    public CameraView getXZCamera()
    {
        return this.xzCamera;
    }
}

class ModelScene
{

    public BranchGroup createSceneGraph()
    {

        OpenMaterial material = new OpenMaterial();
        OpenAppearance appearance = new OpenAppearance();
        appearance.getColoringAttributes().setColor(new Color3f(1, 1, 0));
        BranchGroup objRoot = new BranchGroup();
        TransformGroup objTrans = new TransformGroup();
        objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objTrans.setCapability(TransformGroup.ENABLE_PICK_REPORTING);

        objRoot.addChild(objTrans);

        objTrans.addChild(new ColorCube(0.4));
        Cone cone = new Cone();
        appearance.setMaterial(material);
        cone.setAppearance(appearance);
        objTrans.addChild(cone);
        Background background = new Background();
        background.setColor(new Color3f(.5f, .5f, .5f));
        background.setCapability(Background.ALLOW_COLOR_WRITE);
        BoundingSphere worldBounds = new BoundingSphere(new Point3d(0.0, 0.0,
                                                                    0.0), // Center
                                                        1000.0); // Extent

        background.setApplicationBounds(worldBounds);
        objRoot.addChild(background);

        return objRoot;
    }
}
