/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Title
{

    public static final int LEFT = -1;
    public static final int CENTER = 0;
    public static final int RIGHT = 1;
    private String title = "";
    private Font font = new Font("Dialog", 0, 24);
    private Color color = Color.WHITE;
    private float verticalPosition = .04f;
    private float fontHeight = .04f;
    private int horizontalPosition = CENTER;

    public Title(String title, Font font, float h, Color color, int horizontalPos, float y)
    {
        this.title = title;
        this.font = font;
        this.fontHeight = h;
        this.color = color;
        this.horizontalPosition = horizontalPos;
        this.verticalPosition = y;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Font getFont()
    {
        return font;
    }

    public void setFont(Font font)
    {
        this.font = font;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public int getHorizontalPosition()
    {
        return horizontalPosition;
    }

    public void setHorizontalPosition(int horizontalPos)
    {
        this.horizontalPosition = horizontalPos;
    }

    public float getFontHeight()
    {
        return fontHeight;
    }

    public void setFontHeight(float h)
    {
        this.fontHeight = h;
    }

    public float getVerticalPosition()
    {
        return verticalPosition;
    }

    public void setVerticalPosition(float y)
    {
        this.verticalPosition = y;
    }

}
