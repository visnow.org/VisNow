//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3d;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 *
 * @author know
 */
public class PointLightObject extends LightObject
{
    private final OpenTransformGroup lightTransformGroup = new OpenTransformGroup();
    private final LightIndicator glyph;
    private Vector3d sceneCenter = new Vector3d(0, 0, 0);
    private float radius = 1;
    private float attenuationCoefficient = 1;

    public PointLightObject(EditablePointLight light)
    {
        super(light);
        this.glyph = new LightIndicator((EditablePointLight)light);
        glyph.setVisible(false);
        lightGroup.addChild(light.getLight());
        lightGroup.addChild(glyph);
        lightTransformGroup.addChild(lightGroup);
        addChild(lightTransformGroup);
    }

    public void updateTransform(Transform3D transform)
    {
        lightTransformGroup.setTransform(transform);
    }

    public EditablePointLight getLight()
    {
        return (EditablePointLight)light;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        light.setEnabled(enabled);
        glyph.updateEnabled();
    }

    public void setLightColor(Color3f lightCol)
    {
        light.setLightColor(lightCol);
        glyph.updateLight();
    }

    public LightIndicator getGlyph()
    {
        return glyph;
    }

    public Transform3D getTransform()
    {
        Transform3D tr = new Transform3D();
        lightTransformGroup.getTransform(tr);
        return tr;
    }

    public OpenTransformGroup getLightTransformGroup()
    {
        return lightTransformGroup;
    }

    public void setVisible(boolean visible)
    {
        glyph.setVisible(visible);
    }

    public void updateScale(float radius, Vector3d sceneCenter)
    {
        this.radius = radius;
        this.sceneCenter = new Vector3d(-sceneCenter.getX(), -sceneCenter.getY(), -sceneCenter.getZ());
        glyph.updateScale(radius);
        float u = attenuationCoefficient / radius;
        ((EditablePointLight)light).setAttenuation(new Point3f(1, u, u * u));
    }

    public float getAttenuationCoefficient()
    {
        return attenuationCoefficient;
    }

    public void setAttenuationCoefficient(float attenuationCoefficient)
    {
        this.attenuationCoefficient = attenuationCoefficient;
        float u = attenuationCoefficient / radius;
        ((EditablePointLight)light).setAttenuation(new Point3f(1, u, u * u));
    }

    public void reset()
    {
        ((EditablePointLight)light).reset();
        glyph.updateLight();
        Transform3D tr = new Transform3D();
        tr.setTranslation(sceneCenter);
        lightTransformGroup.setTransform(tr);
    }
}
