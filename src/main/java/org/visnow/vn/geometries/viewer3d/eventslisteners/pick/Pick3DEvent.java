/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.pick;

import java.awt.event.MouseEvent;
import java.util.EventObject;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Matrix3d;
import org.jogamp.vecmath.Matrix3f;
import org.jogamp.vecmath.Point3d;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * An event used for notifyng modules that a mouse pick (so called "emulated 3D pick") or a "device
 * 3D pick" was performed in the viewer.
 * <p>
 * When an emulated 3D pick was performed or a device 3D pick that returns only a 3D point,
 * <code>point</code> will store the point (use
 * <code>getPoint()</code> to get it).
 * </p>
 * <p>
 * When also a plain (a direction) was picked,
 * <code>point</code> will store the point picked, while
 * <code>sensorInLocal</code> will store transform matrix in rootObj coordinates (contains rotation
 * information as well as position).
 * Using these one can reconstruct the plain that was picked by a device or rotation of a 3D cursor.
 * </p>
 * <p/>
 * @author Krzysztof S. Nowinski University of Warsaw, ICM
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 * <p/>
 * @see PickObject see sample code in PickObject class
 */
public class Pick3DEvent extends EventObject
{

    /**
     * Point that was picked by any of emulated 3D pick or device 3D pick (will never be null).
     */
    protected float[] point = null;
    /**
     * When a plain was picked,
     * <code>sensorInLocal</code> and
     * <code>point</code> will be filled.
     * They can be used to reconstruct the equation of a plain or to get vectors with directions.
     */
    protected Transform3D sensorInLocal = null;
    
    /**
     * If picking has been done with mouse, mouse event is stored; otherwise, evt is null
     */
    protected MouseEvent evt;

    /**
     *
     * @param source        object that generated Pick3DEvent
     * @param point         will be copied to the MyPoint3d object
     * @param sensorInLocal a reference will be stored
     */
    public Pick3DEvent(Object source, Point3d point, Transform3D sensorInLocal, MouseEvent evt)
    {
        super(source);

        if (point == null)
            throw new IllegalArgumentException("point cannot be null");

        this.point = new float[]{
            (float) point.x,
            (float) point.y,
            (float) point.z
        };
        this.evt = evt;
        this.sensorInLocal = sensorInLocal;
    }

    //
    //    public Pick3DEvent(Object source, float[] point) {
    //        super(source);
    //        this.point = point;
    //    }
    /**
     * Retuns the picked point. It will be always non-null.
     */
    public float[] getPoint()
    {
        return point;
    }

    public MouseEvent getEvt() {
        return evt;
    }

    /**
     * @return true when pick 3D was performed, false otherwise
     */
    public boolean isPick3D()
    {
        return sensorInLocal != null;
    }

    /**
     * Returns transform matrix in rootObj coordinates or null if it's not a pick 3D.
     * <p/>
     * @return transform matrix or null if it's not a pick 3D
     */
    public Transform3D getSensorInLocal()
    {
        return sensorInLocal;
    }

    /**
     * Returns rotation matrix or null if it's not a pick 3D.
     * <p/>
     * @return rotation matrix or null if not a pick 3D
     */
    public Matrix3f getRotation3f()
    {
        if (sensorInLocal == null)
            return null;

        Matrix3f rotation = new Matrix3f();
        sensorInLocal.getRotationScale(rotation);
        return rotation;
    }

    /**
     * Returns rotation matrix or null if it's not a pick 3D.
     * <p/>
     * @return rotation matrix or null if not a pick 3D
     */
    public Matrix3d getRotation3d()
    {
        if (sensorInLocal == null)
            return null;

        Matrix3d rotation = new Matrix3d();
        sensorInLocal.getRotationScale(rotation);
        return rotation;
    }

    /**
     * Rotates the transform 90 degrees clockwise. It is useful for conversion hoe -> knife.
     * <p/>
     * NOTE: This will change output from getRotation3f(), getRotation3d() and getSensorInLocal().
     * It will not change getPoint() as point is stored in a separate variable.
     */
    public void changeRotateXClockWise()
    {
        Transform3D rotate = new Transform3D();
        rotate.rotX(PI / 2);
        sensorInLocal.mul(sensorInLocal, rotate);
    }
}
