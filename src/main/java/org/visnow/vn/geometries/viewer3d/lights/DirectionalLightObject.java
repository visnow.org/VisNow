//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Color3f;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 *
 * @author know
 */
public class DirectionalLightObject extends LightObject
{
    private final LightIndicator glyph;
    private final Transform3D defTransform;

    public DirectionalLightObject(EditableDirectionalLight light, Transform3D transform)
    {
        super(light);
        defTransform = new Transform3D(transform);
        this.glyph = new LightIndicator((EditableDirectionalLight)light);
        lightGroup.addChild(light.getLight());
        glyph.setVisible(false);
        lightGroup.addChild(glyph);
        lightTransform.setTransform(new Transform3D(defTransform));
        lightTransform.addChild(lightGroup);
        transGroup.addChild(lightTransform);
        light.setGlyph(glyph);
        addChild(transGroup);
    }

    public final void setBiDirectional(boolean biDirectional)
    {
        ((EditableDirectionalLight)light).setBiDirectional(biDirectional);
        glyph.setLight();
    }

    public void updateTransform(Transform3D transform)
    {
        lightTransform.setTransform(transform);
    }

    public EditableDirectionalLight getLight()
    {
        return (EditableDirectionalLight)light;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        light.setEnabled(enabled);
        glyph.updateEnabled();
    }

    public void setLightColor(Color3f lightCol)
    {
        light.setLightColor(lightCol);
        glyph.updateLight();
    }

    public LightIndicator getGlyph()
    {
        return glyph;
    }

    public OpenTransformGroup getTransformGroup()
    {
        return lightTransform;
    }

    public void setState(int state)
    {
        setEnabled(state > 0);
        setBiDirectional(state == 2);
        ((EditableDirectionalLight)light).setState(state);
        glyph.updateLight();
    }

    public void setVisible(boolean visible)
    {
        glyph.setVisible(visible);
    }

    public void reset()
    {
        light.reset();
        lightTransform.setTransform(new Transform3D(defTransform));
        glyph.updateLight();
    }
}
