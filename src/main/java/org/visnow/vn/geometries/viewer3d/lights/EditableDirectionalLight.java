/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.java3d.Bounds;
import org.jogamp.java3d.DirectionalLight;
import org.jogamp.java3d.Light;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Vector3f;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class EditableDirectionalLight extends EditableLight
{
    private static final Vector3f DEF_DIRECTION = new Vector3f(0, 0, -1);
    private static final Vector3f DEF_BACK_DIRECTION = new Vector3f(0, 0, 1);

    protected final Color3f stdLightCol;
    protected Vector3f lightDirection =    new Vector3f(DEF_DIRECTION);
    protected Vector3f backDirection =     new Vector3f(DEF_BACK_DIRECTION);
    protected DirectionalLight backLight = new DirectionalLight(lightColor, backDirection);
    private LightIndicator glyph;
    protected boolean biDirectional = false;

    public EditableDirectionalLight(Color3f lightCol, Bounds bounds,
                                    String name, boolean enabled, boolean biDirectional)
    {
        type = LightType.DIRECTIONAL;
        stdLightCol = lightColor == null ? new Color3f(DEFAULT_LIGHT_COLOR) : lightColor;
        this.lightColor = stdLightCol;
        light = new DirectionalLight(lightColor, lightDirection);
        lightName = name;
        light.setCapability(Light.ALLOW_STATE_WRITE);
        light.setCapability(Light.ALLOW_COLOR_WRITE);
        light.setCapability(DirectionalLight.ALLOW_DIRECTION_WRITE);
        backLight.setCapability(Light.ALLOW_STATE_WRITE);
        backLight.setCapability(Light.ALLOW_COLOR_WRITE);
        backLight.setCapability(DirectionalLight.ALLOW_DIRECTION_WRITE);
        setLightColor(lightCol);
        light.setInfluencingBounds(bounds);
        backLight.setInfluencingBounds(bounds);
        setEnabled(enabled);
        backLight.setEnable(biDirectional && enabled);
        setBiDirectional(biDirectional);
    }

    public EditableDirectionalLight(Bounds bounds, String name)
    {
        this(new Color3f(new Color3f(DEFAULT_LIGHT_COLOR)), bounds, name, true, false);
    }

    public final void setBiDirectional(boolean biDirectional)
    {
        this.biDirectional = biDirectional;
        if (enabled)
            backLight.setEnable(biDirectional);
    }

    public final void setLightColor(Color3f lightCol)
    {
        lightColor = lightCol;
        Color3f lc = new Color3f(lightColor);
        lc.scale(.5f);
        light.setColor(lc);
        backLight.setColor(lc);
    }

    public final void setLightDirection(Vector3f lightDir)
    {
        lightDirection = lightDir;
        Vector3f backDirection = new Vector3f(lightDir);
        backDirection.scale(-1);
        ((DirectionalLight)light).setDirection(lightDirection);
        backLight.setDirection(backDirection);
    }

    public DirectionalLight getBackLight()
    {
        return backLight;
    }

    public DirectionalLight getLight()
    {
        return (DirectionalLight)light;
    }

    public boolean isBiDirectional()
    {
        return biDirectional;
    }

    public int getState()
    {
        if (enabled && biDirectional)
            return 2;
        if (enabled)
            return 1;
        return 0;
    }

    public void setState(int state)
    {
        setEnabled(state > 0);
        setBiDirectional(state == 2);
        if (glyph != null)
            glyph.updateLight();
    }

    /**
     * Get the value of glyph
     *
     * @return the value of glyph
     */
    public LightIndicator getGlyph()
    {
        return glyph;
    }

    /**
     * Set the value of glyph
     *
     * @param glyph new value of glyph
     */
    public void setGlyph(LightIndicator glyph)
    {
        this.glyph = glyph;
    }

    @Override
    public void reset()
    {
        setLightColor(new Color3f(stdLightCol));
        setLightDirection(DEF_DIRECTION);
        setBiDirectional(false);
    }
}
