/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenColoringAttributes;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class CroppedRegularFieldOutline extends OpenBranchGroup
{
    protected final int nSegments;
    protected final int nSegmentVerts;
    protected RegularField field;
    protected OpenBranchGroup geometry = new OpenBranchGroup();
    protected IndexedLineStripArray box = null;
    protected OpenLineAttributes boxLineAttr = new OpenLineAttributes();
    protected OpenColoringAttributes boxColorAttr = new OpenColoringAttributes();
    protected OpenAppearance boxApp = new OpenAppearance();
    protected OpenShape3D boxShape = new OpenShape3D();

    protected int nLineStrips = 0;
    protected int[] dims = null;
    protected int nDims = -1;
    protected int[] lastDims = null;
    protected int lastNDims = -1;
    protected int[] up = null;
    protected int[] low = null;
    protected float[] position = {0, 0, 0};
    protected float[][] af = null;
    protected boolean[] fixed = {false, false, false};
    protected float[] coords = null;
    protected int[] lineStripCounts = null;
    protected float[] boxVerts = null;
    protected int[] subsetDims = null;
    protected int nSubsetDims = 3;
    protected int lastSubsetDims = -1;
    protected float[] center;


    public CroppedRegularFieldOutline(int nSegments)
    {
        super();
        this.nSegments = nSegments;
        nSegmentVerts = nSegments + 1;
        geometry.addChild(boxShape);
        this.addChild(geometry);
    }


    public CroppedRegularFieldOutline()
    {
        this(20);
    }

    /**
     * Set the value of field
     *
     * @param inField new value of field
     * @return true if field is a 3D regular field
     */
    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null)
            return false;
        dims = inField.getDims();
        nDims = dims.length;
        if (!Arrays.equals(lastDims, dims)) {
            low = new int[nDims];
            up  = new int[nDims];
            for (int i = 0; i < nDims; i++) {
                low[i] = 0;
                up[i]  = dims[i];
                fixed[i] = false;
            }
        }
        lastDims = Arrays.copyOf(dims, nDims);
        coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        af = inField.getAffine();
        this.field = inField;
        lastSubsetDims =  nSubsetDims = nDims;
        makeOutlineBox();
        return true;
    }

    /**
     * Set the crop range
     *
     * @param low new value of lower crop extents
     * @param up new value of upper crop extents
     * @return true low and up form valid crop range
     */
    public boolean setCrop(int[] low, int[] up)
    {
        if (low == null || low.length < nDims || up == null || up.length < nDims)
            return false;
        for (int i = 0; i < nDims; i++)
            if (low[i] < 0 || up[i] <= low[i] || up[i] > dims[i])
                return false;
        this.low = low;
        this.up  = up;
        updateBoxCoords(position, fixed, low, up);
        return true;
    }

    /**
     * set the position of index space slice
     * @param pos    float coordinates of a point in the index space contained in the slice
     * @param fix    if fixed[i] is true, the outline of the slice with i-th index coordinate fixed to position[i] is created
     * @return true if parameters are correct and passed to corresponding fields
     */
    public boolean setPosition(float[] pos, boolean[] fix, int[] low, int[] up)
    {
        if (pos == null || pos.length < nDims ||
            fix == null || fix.length < nDims ||
            low == null || low.length < nDims ||
            up  == null ||  up.length < nDims)
            return false;
        System.arraycopy(fix, 0, fixed, 0, nDims);
        nSubsetDims = 0;
        for (int i = 0; i < nDims; i++) {
            this.low[i] = Math.max(0, Math.min(dims[i] - 2, low[i]));
            this.up[i]  = Math.max(this.low[i] + 2, Math.min(dims[i], up[i]));
            position[i] = Math.max(0, Math.min(dims[i] - 1, pos[i]));
            if (!fixed[i])
                nSubsetDims += 1;
        }
        if (nSubsetDims == lastSubsetDims)
            updateBoxCoords(pos, fix, low, up);
        else
            makeOutlineBox();
        lastSubsetDims =  nSubsetDims;
        return true;
    }

    private int getInd(int i, int j, int k)
    {
        return ((i * dims[1] + j) * dims[0] + k) * 3;
    }
    private int getInd( int j, int k)
    {
        return (j * dims[0] + k) * 3;
    }
    private int getInd(int k)
    {
        return 3 * k;
    }

    protected float[] computeAtPoint(float u, float v)
    {
        float[] out = new float[3];
        if (coords == null) {
            System.arraycopy(af[3], 0, out, 0, 3);
            for (int i = 0; i < 3; i++)
                out[i] += u * af[0][i] + v * af[1][i];
            return out;
        }
        int n0 = dims[0];
        int inexact = 0;
        u = Math.max(0, Math.min(dims[0] - 1, u));
        v = Math.max(0, Math.min(dims[1] - 1, v));
        int i = (int) u;
        u -= i;
        if (u != 0)
            inexact += 1;
        int j = (int) v;
        v -= j;
        if (v != 0)
            inexact += 2;
        int m = j * dims[0] + i;
        switch (inexact) {
        case 0:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = coords[3 * m + cmp];
            break;
        case 1:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = u *       coords[3 * (m + 1) + cmp] +
                           (1 - u) * coords[3 *  m +      cmp];
            break;
        case 2:
                for (int cmp = 0; cmp < 3; cmp++)
                    out[cmp] = v *       coords[3 * (m + n0) + cmp] +
                               (1 - v) * coords[3 *  m +       cmp];
        case 3:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = v *       (u *      coords[3 * (m + n0 + 1) + cmp] +
                                     (1 - u) * coords[3 * (m + n0) +     cmp]) +
                           (1 - v) * (u *      coords[3 * (m +      1) + cmp] +
                                     (1 - u) * coords[3 *  m +           cmp]);
            break;
        }
        return out;
    }

    protected float[] computeAtPoint(float u, float v, float w)
    {
        float[] out = new float[3];
        if (coords == null) {
            System.arraycopy(af[3], 0, out, 0, 3);
            for (int i = 0; i < 3; i++)
                out[i] += u * af[0][i] + v * af[1][i] + w * af[2][i];
            return out;
        }

        int n0 = dims[0];
        int n1 = dims[0] * dims[1];
        int inexact = 0;
        u = Math.max(0, Math.min(dims[0] - 1, u));
        v = Math.max(0, Math.min(dims[1] - 1, v));
        w = Math.max(0, Math.min(dims[2] - 1, w));
        int i = (int) u;
        u -= i;
        if (u != 0)
            inexact += 1;
        int j = (int) v;
        v -= j;
        if (v != 0)
            inexact += 2;
        int k = (int) w;
        w -= k;
        if (w != 0)
            inexact += 4;
        int m = (dims[1] * k + j) * dims[0] + i;
        switch (inexact) {
        case 0:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = coords[3 * m + cmp];
            break;
        case 1:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = u *       coords[3 * (m + 1) + cmp] +
                           (1 - u) * coords[3 *  m +      cmp];
            break;
        case 2:
                for (int cmp = 0; cmp < 3; cmp++)
                    out[cmp] = v *       coords[3 * (m + n0) + cmp] +
                               (1 - v) * coords[3 *  m +       cmp];
            break;
        case 3:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = v *       (u *      coords[3 * (m + n0 + 1) + cmp] +
                                     (1 - u) * coords[3 * (m + n0) +     cmp]) +
                           (1 - v) * (u *      coords[3 * (m +      1) + cmp] +
                                     (1 - u) * coords[3 *  m +           cmp]);
            break;
        case 4:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = w *       coords[3 * (m + n1) + cmp] +
                           (1 - w) * coords[3 *  m +       cmp];
            break;
        case 5:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = w *       (u *      coords[3 * (m + n1 + 1) + cmp] +
                                     (1 - u) * coords[3 * (m + n1) +     cmp]) +
                           (1 - w) * (u *      coords[3 * (m +      1) + cmp] +
                                     (1 - u) * coords[3 *  m +           cmp]);
            break;
        case 6:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = w *       (v *      coords[3 * (m + n1 + n0) + cmp] +
                                     (1 - v) * coords[3 * (m + n1) +      cmp]) +
                           (1 - w) * (v *      coords[3 * (m +      n0) + cmp] +
                                     (1 - v) * coords[3 *  m +            cmp]);
            break;
        case 7:
            for (int cmp = 0; cmp < 3; cmp++)
                out[cmp] = w *       (v *      (u *      coords[3 * (m + n1 + n0 + 1) + cmp] +
                                               (1 - u) * coords[3 * (m + n1 + n0) +     cmp]) +
                                     (1 - v) * (u *      coords[3 * (m + n1 +      1) + cmp] +
                                               (1 - u) * coords[3 * (m + n1) +          cmp])) +
                           (1 - w) * (v *      (u *      coords[3 * (m +      n0 + 1) + cmp] +
                                               (1 - u) * coords[3 * (m +      n0) +     cmp]) +
                                     (1 - v) * (u *      coords[3 * (m +           1) + cmp] +
                                               (1 - u) * coords[3 *  m +                cmp]));
            break;
        }
        return out;
    }

    private void computePointGlyphCoords()
    {
        switch (nDims) {
        case 1:
            center = computeAtPoint(position[0], 0, 0);
            break;
        case 2:
            center = computeAtPoint(position[0], position[1], 0);
            break;
        default:
            center = computeAtPoint(position[0], position[1], position[2]);
        }
        float r = 0;
        for (int i = 0; i < 3; i++)
            r += (field.getExtents()[1][i] - field.getExtents()[0][i]) *
                 (field.getExtents()[1][i] - field.getExtents()[0][i]);
        r = (float)Math.sqrt(r) /150;
        for (int i = 0; i < 3; i++)
            boxVerts[i]      = boxVerts[i + 3] =
            boxVerts[i + 6]  = boxVerts[i + 9] =
            boxVerts[i + 12] = boxVerts[i + 15] = center[i];
        boxVerts[0] -= r;
        boxVerts[3] += r;
        boxVerts[7] -= r;
        boxVerts[10] += r;
        boxVerts[14] -= r;
        boxVerts[17] += r;
    }

    public void updateBoxCoords(float[] pos, boolean[] fix, int[] low, int[] up)
    {
        int axisInd = -1;
        int d;
        switch (nSubsetDims) {
        case 0:
            computePointGlyphCoords();
            break;
        case 1:
            for (int i = 0; i < dims.length; i++)
                if (!fixed[i]) {
                    axisInd = i;
                    break;
                }
            if (coords == null)
                for (int i = 0; i < 3; i++) {
                    boxVerts[i] = boxVerts[i + 3] = af[3][i];
                    for (int j = 0; j < dims.length; j++)
                        if (j == axisInd) {
                            boxVerts[i]     += low[j]      * af[j][i];
                            boxVerts[i + 3] += (up[j] - 1) * af[j][i];
                        }
                        else {
                            boxVerts[i]     += position[j] * af[j][i];
                            boxVerts[i + 3] += position[j] * af[j][i];
                        }
                }
            else {
                float[] x = new float[nDims];
                d = up[axisInd] - low[axisInd];
                System.arraycopy(position, 0, x, 0, nDims);
                switch (nDims) {
                case 1:
                    for (int i = 0; i < nSegmentVerts; i++) {
                        int ii = Math.min(low[0] + (i * d) / nSegments, up[0] - 1);
                        System.arraycopy(coords, getInd(ii), boxVerts, 3 * i, 3);
                    }
                    break;
                case 2:
                    for (int i = 0; i < nSegmentVerts; i++) {
                        x[axisInd] = Math.min(low[axisInd] + (i * d) / nSegments, up[axisInd] - 1);
                        System.arraycopy(computeAtPoint(x[0], x[1]), 0, boxVerts, 3 * i, 3);
                    }
                    break;
                case 3:
                    for (int i = 0; i < nSegmentVerts; i++) {
                        x[axisInd] = Math.min(low[axisInd] + (i * d) / nSegments, up[axisInd] - 1);
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * i, 3);
                    }
                    break;
                }
            }
            break;
        case 2:
                for (int i = 0; i < dims.length; i++)
                if (fixed[i]) {
                    axisInd = i;
                    break;
                }
            if (coords == null)
                for (int i = 0; i < 3; i++) {
                    boxVerts[i] = boxVerts[i + 3] = boxVerts[i + 6] = boxVerts[i + 9] = af[3][i];
                    for (int j = 0, k = 0; j < dims.length; j++) {
                        if (j == axisInd) {
                            boxVerts[i]     += position[j] * af[j][i];
                            boxVerts[i + 3] += position[j] * af[j][i];
                            boxVerts[i + 6] += position[j] * af[j][i];
                            boxVerts[i + 9] += position[j] * af[j][i];

                        }
                        else {
                            if (k == 0) {
                                boxVerts[i]     += low[j]      * af[j][i];
                                boxVerts[i + 3] += (up[j] - 1) * af[j][i];
                                boxVerts[i + 6] += low[j]      * af[j][i];
                                boxVerts[i + 9] += (up[j] - 1) * af[j][i];
                            }
                            else {
                                boxVerts[i]     += low[j]      * af[j][i];
                                boxVerts[i + 3] += low[j]      * af[j][i];
                                boxVerts[i + 6] += (up[j] - 1) * af[j][i];
                                boxVerts[i + 9] += (up[j] - 1) * af[j][i];
                            }
                            k += 1;
                        }
                    }
                }
            else
                switch (nDims) {
                case 2:
                    d = up[0] - low[0];
                    // i0 axis edges
                    for (int i = 0; i < nSegmentVerts; i++) {
                        int ii = Math.min(low[0] + (i * d) / nSegments, up[0] - 1);
                        System.arraycopy(coords, getInd(low[1], ii),    boxVerts, 3 * i, 3);
                        System.arraycopy(coords, getInd(up[1] - 1, ii), boxVerts, 3 * (nSegmentVerts + i), 3);
                    }
                    d = up[1] - low[1];
                    for (int i = 0; i < nSegmentVerts; i++) {
                        int ii = Math.min(low[1] + (i * d) / nSegments, up[1] - 1);
                        System.arraycopy(coords, getInd(ii, low[0]),    boxVerts, 3 * (2 * nSegmentVerts + i), 3);
                        System.arraycopy(coords, getInd(ii, up[0] - 1), boxVerts, 3 * (3 * nSegmentVerts + i), 3);
                    }
                    break;
                case 3:
                    int ax1 = (axisInd + 1) % 3;
                    int ax2 = (axisInd + 2) % 3;
                    float[] x = new float[3];
                    x[axisInd] = position[axisInd];
                    d = up[ax1] - low[ax1];
                    for (int i = 0; i < nSegmentVerts; i++) {
                        x[ax1] = Math.min(low[ax1] + (i * d) / nSegments, up[ax1] - 1);
                        x[ax2] = low[ax2];
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * i,                      3);
                        x[ax2] = up[ax2] - 1;
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (nSegmentVerts + i),    3);
                    }
                    d = up[ax2] - low[ax2];
                    for (int i = 0; i < nSegmentVerts; i++) {
                        x[ax2] = Math.min(low[ax2] + (i * d) / nSegments, up[ax2] - 1);
                        x[ax1] = low[ax1];
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (2 * nSegmentVerts + i), 3);
                        x[ax1] = up[ax1] - 1;
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (3 * nSegmentVerts + i), 3);
                    }
                    break;
                }
            break;
        case 3:
            if (coords == null)
                for (int i = 0; i < 3; i++) {
                    boxVerts[i]      = af[3][i] + low[0]      * af[0][i] + low[1]      * af[1][i] + low[2] * af[2][i];
                    boxVerts[i + 3]  = af[3][i] + (up[0] - 1) * af[0][i] + low[1]      * af[1][i] + low[2] * af[2][i];
                    boxVerts[i + 6]  = af[3][i] + low[0]      * af[0][i] + (up[1] - 1) * af[1][i] + low[2] * af[2][i];
                    boxVerts[i + 9]  = af[3][i] + (up[0] - 1) * af[0][i] + (up[1] - 1) * af[1][i] + low[2] * af[2][i];
                    boxVerts[i + 12] = af[3][i] + low[0]      * af[0][i] + low[1]      * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 15] = af[3][i] + (up[0] - 1) * af[0][i] + low[1]      * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 18] = af[3][i] + low[0]      * af[0][i] + (up[1] - 1) * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 21] = af[3][i] + (up[0] - 1) * af[0][i] + (up[1] - 1) * af[1][i] + (up[2] - 1) * af[2][i];
                }
            else {
                float[] x = new float[3];
                for (int ax = 0; ax < 3; ax++) {
                    int ax1 = (ax + 1) % 3;
                    int ax2 = (ax + 2) % 3;
                    float dx = (up[ax] - low[ax]) / (float)nSegments;
                    for (int i = 0, l = 4 * ax * nSegmentVerts; i < nSegmentVerts; i++, l++) {
                        x[ax]  = Math.min(low[ax] + i * dx, up[ax] - 1);
                        x[ax1] = low[ax1];
                        x[ax2] = low[ax2];
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * l, 3);
                        x[ax2] = up[ax2] - 1;
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (nSegmentVerts + l), 3);
                        x[ax1] = up[ax1] - 1;
                        x[ax2] = low[ax2];
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (2 * nSegmentVerts + l), 3);
                        x[ax2] = up[ax2] - 1;
                        System.arraycopy(computeAtPoint(x[0], x[1], x[2]), 0, boxVerts, 3 * (3 * nSegmentVerts + l), 3);
                    }
                }
            }
            break;
        }
        box.setCoordinates(0, boxVerts);
    }

    private void makeOutlineBox()
    {
        boolean detach = postdetach();
        boxShape.removeAllGeometries();
        if (field == null) {
            if (detach)
                postattach();
            return;
        }
        int[] cInd = null;
        switch (nSubsetDims) {
        case 0:
            boxVerts = new float[18];
            box = new IndexedLineStripArray(6, GeometryArray.COORDINATES, 6, new int[]{2, 2, 2});
            cInd = new int[]{0, 1, 2, 3, 4, 5};
            break;
        case 1:
            if (coords == null) {
                boxVerts = new float[6];
                box = new IndexedLineStripArray(2, GeometryArray.COORDINATES, 2, new int[]{2});
                cInd = new int[]{0, 1};
            } else {
                boxVerts = new float[3 * nSegmentVerts];
                box = new IndexedLineStripArray(nSegmentVerts,
                        GeometryArray.COORDINATES,
                        nSegmentVerts,
                        new int[]{nSegmentVerts});
                cInd = new int[nSegmentVerts];
                for (int i = 0; i < cInd.length; i++)
                    cInd[i] = i;
            }
            break;
        case 2:
            if (coords == null) {
                boxVerts = new float[12];
                box = new IndexedLineStripArray(4, GeometryArray.COORDINATES, 8, new int[]{2, 2, 2, 2});
                cInd = new int[]{0, 1, 2, 3, 0, 2, 1, 3};
            } else {
                boxVerts = new float[12 * nSegmentVerts];
                box = new IndexedLineStripArray(4 * nSegmentVerts,
                        GeometryArray.COORDINATES,
                        4 * nSegmentVerts,
                        new int[]{
                            nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts
                        });
                cInd = new int[4 * nSegmentVerts];
                for (int i = 0; i < cInd.length; i++)
                    cInd[i] = i;
            }
            break;
        case 3:
            if (coords == null) {
                boxVerts = new float[24];
                box = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});
                cInd = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7};
            } else {
                boxVerts = new float[36 * nSegmentVerts];
                box = new IndexedLineStripArray(12 * nSegmentVerts,
                        GeometryArray.COORDINATES,
                        12 * nSegmentVerts,
                        new int[]{
                            nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts,
                            nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts,
                            nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts
                        });
                cInd = new int[12 * nSegmentVerts];
                for (int i = 0; i < cInd.length; i++)
                    cInd[i] = i;
            }
                break;
        }
        box.setCoordinateIndices(0, cInd);

        boxLineAttr.setLineAntialiasingEnable(true);
        boxLineAttr.setLineWidth(1.5f);
        boxApp.setLineAttributes(boxLineAttr);
        boxColorAttr.setColor(0, 1, 0);
        boxApp.setColoringAttributes(boxColorAttr);
        box.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        box.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        box.setCoordinates(0, boxVerts);
        boxShape.setAppearance(boxApp);
        updateBoxCoords(position, fixed, low, up);
        boxShape.addGeometry(box);
        if (detach)
            postattach();
    }

    public float[] getCenter()
    {
        return center;
    }

}
