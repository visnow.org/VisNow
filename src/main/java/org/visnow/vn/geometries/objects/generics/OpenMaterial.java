/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.Material;
import org.jogamp.vecmath.Color3f;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class OpenMaterial extends Material
{

    /**
     * Creates a new instance of OpenMaterial
     */
    public OpenMaterial()
    {
        setCapability(Material.ALLOW_COMPONENT_READ);
        setCapability(Material.ALLOW_COMPONENT_WRITE);
        setColorTarget(AMBIENT_AND_DIFFUSE);
        setSpecularColor(new Color3f(.2f, .2f, .2f));
    }

    public OpenMaterial(Color3f ambientColor, Color3f emissiveColor,
                        Color3f diffuseColor, Color3f specularColor,
                        float shininess, int colorTarget)
    {
        super(ambientColor, emissiveColor, diffuseColor, specularColor, shininess);
        setColorTarget(colorTarget);
        setCapability(Material.ALLOW_COMPONENT_READ);
        setCapability(Material.ALLOW_COMPONENT_WRITE);
    }

    @Override
    public OpenMaterial cloneNodeComponent(boolean forceDuplicate)
    {
        OpenMaterial openMaterial = new OpenMaterial();
        openMaterial.duplicateNodeComponent(this, forceDuplicate);
        return openMaterial;
    }
    
    public void copyValuesFrom(Material src)
    {
        Color3f ambientColor = new Color3f(), emissiveColor = new Color3f(), 
                diffuseColor = new Color3f(), specularColor = new Color3f();
        src.getAmbientColor(ambientColor);
        src.getEmissiveColor(emissiveColor);
        src.getDiffuseColor(diffuseColor);
        src.getSpecularColor(specularColor);
        setAmbientColor(ambientColor);
        setEmissiveColor(emissiveColor);
        setDiffuseColor(diffuseColor);
        setSpecularColor(specularColor);
        setColorTarget(src.getColorTarget());
        setShininess(src.getShininess());
    }
    
}
