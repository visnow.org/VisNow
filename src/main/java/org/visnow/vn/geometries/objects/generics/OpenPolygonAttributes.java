/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.PolygonAttributes;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class OpenPolygonAttributes extends PolygonAttributes
{

    public OpenPolygonAttributes()
    {
        setCapability(ALLOW_CULL_FACE_READ);
        setCapability(ALLOW_CULL_FACE_WRITE);
        setCapability(ALLOW_MODE_READ);
        setCapability(ALLOW_MODE_WRITE);
        setCapability(ALLOW_NORMAL_FLIP_READ);
        setCapability(ALLOW_NORMAL_FLIP_WRITE);
        setCapability(ALLOW_OFFSET_READ);
        setCapability(ALLOW_OFFSET_WRITE);
        setCullFace(CULL_NONE);
        setBackFaceNormalFlip(true);
    }

    public OpenPolygonAttributes(int mode, int cull, float offset, boolean flip, float offsetFactor)
    {
        super(mode, cull, offset, flip, offsetFactor);
        setCapability(ALLOW_CULL_FACE_READ);
        setCapability(ALLOW_CULL_FACE_WRITE);
        setCapability(ALLOW_MODE_READ);
        setCapability(ALLOW_MODE_WRITE);
        setCapability(ALLOW_NORMAL_FLIP_READ);
        setCapability(ALLOW_NORMAL_FLIP_WRITE);
        setCapability(ALLOW_OFFSET_READ);
        setCapability(ALLOW_OFFSET_WRITE);
        setBackFaceNormalFlip(true);
    }

    public OpenPolygonAttributes(int mode, int cull, float offset, boolean flip, float offsetFactor, boolean backFaceNormalFlip)
    {
        super(mode, cull, offset, flip, offsetFactor);
        setCapability(ALLOW_CULL_FACE_READ);
        setCapability(ALLOW_CULL_FACE_WRITE);
        setCapability(ALLOW_MODE_READ);
        setCapability(ALLOW_MODE_WRITE);
        setCapability(ALLOW_NORMAL_FLIP_READ);
        setCapability(ALLOW_NORMAL_FLIP_WRITE);
        setCapability(ALLOW_OFFSET_READ);
        setCapability(ALLOW_OFFSET_WRITE);
        setBackFaceNormalFlip(backFaceNormalFlip);
    }

    @Override
    public OpenPolygonAttributes cloneNodeComponent(boolean forceDuplicate)
    {
        OpenPolygonAttributes openPolygonAttributes = new OpenPolygonAttributes();
        openPolygonAttributes.duplicateNodeComponent(this, forceDuplicate);
        return openPolygonAttributes;
    }

    public void copyValuesFrom(PolygonAttributes src)
    {
        setBackFaceNormalFlip(src.getBackFaceNormalFlip());
        setPolygonMode(src.getPolygonMode());
        setPolygonOffset(src.getPolygonOffset());
        setCullFace(src.getCullFace());
    }
}
