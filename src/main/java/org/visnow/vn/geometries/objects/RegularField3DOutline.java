/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField3DOutline extends OpenBranchGroup
{

    protected RegularField field;
    protected OpenShape3D outlineShape = new OpenShape3D();
    protected OpenBranchGroup geometry = new OpenBranchGroup();
    protected IndexedLineStripArray box = null;

    protected int nLineStrips = 0;
    protected int[] dims = null;
    protected FloatLargeArray coords = null;
    protected int[] lineStripCounts = null;
    protected float[] boxVerts = null;

    static Logger logger = Logger.getLogger(RegularField3DOutline.class);

    public RegularField3DOutline()
    {
        super();
        geometry.addChild(outlineShape);
        this.addChild(geometry);
    }

    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3)
            return false;
        dims = inField.getDims();
        coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords();
        this.field = inField;
        makeOutlineBox();
        return true;
    }

    private void updateBoxCoords()
    {
        long[] lDims = field.getLDims();
        boolean detach = postdetach();
        if (coords == null) {
            float[][] af = field.getAffine();
            for (int i = 0; i < 3; i++) {
                boxVerts[i] = af[3][i];
                boxVerts[i + 3] = af[3][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 6] = af[3][i] + (lDims[1] - 1) * af[1][i];
                boxVerts[i + 9] = af[3][i] + (lDims[1] - 1) * af[1][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 12] = af[3][i] + (lDims[2] - 1) * af[2][i];
                boxVerts[i + 15] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 18] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[1] - 1) * af[1][i];
                boxVerts[i + 21] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[1] - 1) * af[1][i] + (lDims[0] - 1) * af[0][i];
            }
        } else {
            int k = 0;
            long l = 0, s = 3;
            
            // i0 axis edges
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1) + 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i1 axis edges
            s = 3 * lDims[0];
            l = 0;
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1) + 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i2 axis edges
            s = 3 * lDims[0] * lDims[1];
            l = 0;
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1) + 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
        }
        box.setCoordinates(0, boxVerts);
        if (detach)
            postattach();
    }

    private void makeOutlineBox()
    {
        boolean detach = postdetach();
        outlineShape.removeAllGeometries();
        if (field == null) {
            if (detach)
                postattach();
            return;
        }

        if (coords == null) {
            boxVerts = new float[24];
            box = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});

            box.setCoordinateIndices(0, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7});
        } else {
            boxVerts = new float[3 * 4 * (dims[0] + dims[1] + dims[2])];
            box = new IndexedLineStripArray(4 * (dims[0] + dims[1] + dims[2]),
                                            GeometryArray.COORDINATES,
                                            4 * (dims[0] + dims[1] + dims[2]),
                                            new int[]{
                                                dims[0], dims[0], dims[0], dims[0],
                                                dims[1], dims[1], dims[1], dims[1],
                                                dims[2], dims[2], dims[2], dims[2]
                                            });

            int[] cInd = new int[4 * (dims[0] + dims[1] + dims[2])];
            for (int i = 0; i < cInd.length; i++)
                cInd[i] = i;
            box.setCoordinateIndices(0, cInd);
        }
        box.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        updateBoxCoords();

        outlineShape.addGeometry(box);
        if (detach)
            postattach();
    }
}
