/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.vn.geometries.parameters.ColormapLegendParameters;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.Range;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.geometries.parameters.ComponentColorMap;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ColormapLegend extends Geometry2D
{

    public static final int NONE = 0;
    public static final int NORTH = 1;
    public static final int SOUTH = 2;
    public static final int EAST = 3;
    public static final int WEST = 4;
    protected float low = 0;
    protected float up = 1;
    protected float clow = 0;
    protected float cup = 1;
    protected ComponentColorMap colormap;
    protected ColormapLegendParameters params;
    protected Graphics2D gr;
    protected DecimalFormat intf;
    protected Range range;
    protected Font font;
    protected Font titleFont;
    protected int windowHeight;
    protected int windowWidth;
    protected String position;
    /**
     * An array of explicitly set values for range values (used in custom
     * isolines values and isosurface values). If thrTable is not null and
     * coloring component is equal to the thrComponent, legend is displayed
     * as series of short lines corresponding to selected threshold.
     * Otherwise, a colored rectangle is displayed
     */
    protected float[] thrTable;
    protected String thrComponent = "";
    protected boolean discreteData = false;
    protected FontMetrics fm;

    public ColormapLegend()
    {
        super();
        name = "legend";
    }

    public void setParams(ColormapLegendParameters params)
    {
        this.params = params;
    }

    public void setThrTable(float[] thrTable, String thrComponent)
    {
        this.thrTable = thrTable;
        this.thrComponent = thrComponent;
    }

    protected void drawHorizontalLegendTicks(int x, int y, int l, int w)
    {
        gr.setColor(params.getColor());
        gr.setStroke(new BasicStroke(1));
        GeneralPath ticks = new GeneralPath();
        if (!discreteData) {
            ticks.moveTo(x, y);
            ticks.lineTo(x, y + w - 1);
            ticks.lineTo(x + l, y + w - 1);
            ticks.lineTo(x + l, y);
            ticks.lineTo(x, y);
        }
        for (float t = range.getLow(); t <= up; t += range.getStep()) {
            int i = x + (int) (l * (t - low) / (up - low));
            ticks.moveTo(i, y);
            ticks.lineTo(i, y + w - 1);
        }
        gr.draw(ticks);
    }

    protected void drawHorizontalLegendRectangle(int x, int y, int l, int w)
    {
        int[] colorMapLookup;
        if (params != null && params.getColorMapLookup() != null)
            colorMapLookup = params.getColorMapLookup();
        else
            colorMapLookup = colormap.getRGBColorTable();
        if (discreteData) {
            gr.setStroke(new BasicStroke(2));
            for (int i = 0; i < thrTable.length; i++) {
                float v = thrTable[i];
                int k = (int) (255 * (v - clow) / (cup - clow));
                if (k < 0)
                    k = 0;
                if (k > 255)
                    k = 255;
                gr.setColor(new Color(colorMapLookup[k]));
                int j = x + (int) (l * (v - low) / (up - low));
                gr.drawLine(j, y, j, y + w);
            }
            gr.setColor(params.getColor());
            gr.setStroke(new BasicStroke(1));
        }
        else {
            BufferedImage img = new BufferedImage(256, 2, BufferedImage.TYPE_INT_ARGB);
            int[] pix = new int[512];
            for (int i = 0; i < 256; i++)
                pix[i] = pix[i + 256] = colorMapLookup[i];
            img.setRGB(0, 0, 256, 2, pix, 0, 256);
            gr.drawImage(img, x, y, l, w, null);
            drawHorizontalLegendTicks(x, y, l, w);
        }
    }

    protected void drawVerticalLegendTicks(int x, int y, int l, int w)
    {
        gr.setColor(params.getColor());
        gr.setStroke(new BasicStroke(1));
        GeneralPath ticks = new GeneralPath();
        if (!discreteData) {
            ticks.moveTo(x, y);
            ticks.lineTo(x, y + l - 1);
            ticks.lineTo(x + w - 1, y + l - 1);
            ticks.lineTo(x + w - 1, y);
            ticks.lineTo(x, y);
        }
        for (float t = range.getLow(); t <= up; t += range.getStep()) {
            int i = y + l - (int) (l * (t - low) / (up - low));
            ticks.moveTo(x, i);
            ticks.lineTo(x + w - 1, i);
        }
        gr.draw(ticks);
    }

    protected void drawVerticalLegendRectangle(int x, int y, int l, int w)
    {
        int[] colorMapLookup;
        if (params != null && params.getColorMapLookup() != null)
            colorMapLookup = params.getColorMapLookup();
        else
            colorMapLookup = colormap.getRGBColorTable();
        if (discreteData) {
            gr.setStroke(new BasicStroke(2));
            for (int i = 0; i < thrTable.length; i++) {
                float v = thrTable[i];
                int k = (int) (255 * (v - clow) / (cup - clow));
                if (k < 0)
                    k = 0;
                if (k > 255)
                    k = 255;
                gr.setColor(new Color(colorMapLookup[k]));
                int j = y + l - (int) (l * (v - low) / (up - low));
                gr.drawLine(x, j, x + w, j);
            }
        }
        else {
            BufferedImage img = new BufferedImage(2, 256, BufferedImage.TYPE_INT_ARGB);
            int[] pix = new int[512];
            for (int i = 0; i < 256; i++) {
                int k = pix.length - 2 - 2 * i;
                pix[k] = pix[k + 1] = colorMapLookup[i];
            }
            img.setRGB(0, 0, 2, 256, pix, 0, 2);
            gr.drawImage(img, x, y, w, l, null);
            drawVerticalLegendTicks(x, y, l, w);
        }
    }

    protected void drawHorizontalLegendLabels(int x, int y, int l)
    {
        for (float t = range.getLow(); t <= up; t += range.getStep()) {
            String label = intf.format(t);
            gr.drawString(label, x + (int) (l * (t - low) / (up - low)) - fm.stringWidth(label) / 3, y);
        }
    }

    protected void drawVerticalLegendLabels(int x, int y, int l)
    {
        for (float t = range.getLow(); t <= up; t += range.getStep())
            gr.drawString(intf.format(t), x, y + l - (int) (l * (t - low) / (up - low)));
    }

    @Override
    public void draw2D(J3DGraphics2D g, LocalToWindow ltw, int windowWidth, int windowHeight)
    {
        if (!params.isEnabled())
            return;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;      

        discreteData = thrTable != null && thrComponent != null && !thrComponent.isEmpty() &&
                       thrComponent.equals(params.getColormap().getComponentRange().getComponentName());
        int l = 100, w = 20;
        int x = 10, y = 20;
        int fontSize, titleFontSize;

        String title = params.getName();
        String unit = params.getUnit();
        if (unit != null && !unit.isEmpty() && !unit.equalsIgnoreCase("SI") && !unit.equalsIgnoreCase("1"))
            title += " (" + params.getUnit() + ")";
        colormap = params.getColormap();

        gr = (Graphics2D) g;
        if (colormap == null || params.getPosition() == NONE)
            return;
        clow = colormap.getComponentRange().getPhysicalLow();
        cup  = colormap.getComponentRange().getPhysicalUp();
        if (!discreteData) {
            low = clow;
            up = cup;
        } else {
            low = clow;
            up = cup;
            for (int i = 0; i < thrTable.length; i++) {
                if (up < thrTable[i])
                    up = thrTable[i];
                if (low > thrTable[i])
                    low = thrTable[i];
            }
        }
        if (low == up || clow == cup)
            return;
        switch (params.getPosition()) {
        case SOUTH:
            position = "SOUTH";
            x = (int) (windowWidth * params.getX());
            l = (int) (windowWidth * params.getL());
            y = (int) (windowHeight - windowHeight * params.getY());
            w = (int) (windowHeight * params.getW());
            break;
        case NORTH:
            position = "NORTH";
            x = (int) (windowWidth * params.getX());
            l = (int) (windowWidth * params.getL());
            y = (int) (windowHeight * params.getY());
            w = (int) (windowHeight * params.getW());
            break;
        case WEST:
            position = "WEST";
            x = (int) (windowWidth * params.getY());
            w = (int) (windowWidth * params.getW());
            y = (int) (windowHeight * params.getX());
            l = (int) (windowHeight * params.getL());
            break;
        case EAST:
            position = "EAST";
            x = (int) (windowWidth - windowWidth * params.getY());
            w = (int) (windowWidth * params.getW());
            y = (int) (windowHeight * params.getX());
            l = (int) (windowHeight * params.getL());
            break;
        }
        fontSize = (params.getFontSize() * windowHeight) / 1000;
        titleFontSize = (int) (1.25 * fontSize);
        font = new java.awt.Font("Dialog", Font.PLAIN, fontSize);
        fm = gr.getFontMetrics(font);
        titleFont = new java.awt.Font("Dialog", Font.PLAIN, titleFontSize);

        range = new Range((int)(2 + 10 * params.getL()), low, up);
        String fString = "####";
        int k = (int) (-log10(range.getStep())) + 1;
        if (k > 0) {
            fString = fString += ".";
            for (int i = 0; i < k; i++)
                fString = fString += "#";
        }
        if (params.getPosition() == NORTH || params.getPosition() == SOUTH) 
            range = new Range(low, up, (int) (30 * l / (float) fm.stringWidth(fString)));

        intf = new DecimalFormat(fString);
        gr.setColor(params.getColor());
        gr.setFont(font);
        if (fm == null)
            return;

        // title
        fm = gr.getFontMetrics(titleFont);
        int xOff = 0, yOff = 0;
        switch (params.getPosition()) {
        case SOUTH:
            drawHorizontalLegendRectangle(x, y - w, l, w);
            if (discreteData) {
                 drawHorizontalLegendTicks(x, y - w, l, 4);
                 gr.drawLine(x, y - w, x + l, y - w);
                 yOff = 5;
            }
            drawHorizontalLegendLabels(x, y - yOff - w - 3, l);
            gr.setFont(titleFont);
            gr.drawString(title, x + l + (int) (.6 * fm.stringWidth(fString)), y - w / 2);
            break;
        case NORTH:
            drawHorizontalLegendRectangle(x, y, l, w);
            if (discreteData) {
                 drawHorizontalLegendTicks(x, y + w, l, 4);
                 gr.drawLine(x, y + w, x + l, y + w);
                 yOff = 5;
            }
            drawHorizontalLegendLabels(x, y + w + yOff + fontSize + 3, l);
            gr.setFont(titleFont);
            gr.drawString(title, x + l + (int) (.6 * fm.stringWidth(fString)), y + w / 2);
            break;
        case WEST:
            drawVerticalLegendRectangle(x, y, l, w);
            if (discreteData) {
                 drawVerticalLegendTicks(x + w, y, l, 4);
                 gr.drawLine(x + w, y, x + w, y + l);
                 xOff = 5;
            }
            drawVerticalLegendLabels(x + w + xOff + 3, (int) (windowHeight * params.getX()), l);
            gr.setFont(titleFont);
            gr.drawString(title, x, y - (int)(1.05 * titleFontSize));
            break;
        case EAST:
            int maxLabelWidth = 0;
            for (float t = range.getLow(); t <= up; t += range.getStep()) {
                String s = (intf.format(t));
                if (fm.stringWidth(s) > maxLabelWidth)
                    maxLabelWidth = fm.stringWidth(s);
            }
            drawVerticalLegendRectangle(x - w, y, l, w);
            if (discreteData) {
                 drawVerticalLegendTicks(x - w, y, l, 4);
                 gr.drawLine(x - w, y, x - w, y + l);
                 xOff = 5;
            }
            drawVerticalLegendLabels(x - w - xOff - 3 - maxLabelWidth, y, l);
            gr.setFont(titleFont);
            gr.drawString(title, x - fm.stringWidth(title), y - (int)(1.05 * titleFontSize));
            break;
        }
    }

    public ComponentColorMap getColormap()
    {
        return colormap;
    }

    public void setColormap(ComponentColorMap colormap)
    {
        this.colormap = colormap;
    }

    public ColormapLegendParameters getParams()
    {
        return params;
    }
}


