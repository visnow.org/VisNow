/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.parameters.GraphWorldParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.text.AxisLabelItem;
import org.visnow.vn.lib.utils.Range;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GraphWorld extends DataMappedGeometryObject
{

    protected int origX = 0, endX = 200, origY = 0, endY = 200;
    protected int w, h, lastw = -1, lasth = -1;
    protected int n0 = 0, n1 = 10;
    protected float xmin = 0, xmax = 10, ymin = 0, ymax = 1;
    protected int x0, y0;
    protected float dx, dy;
    protected Range xRange, yRange;
    protected AxisLabelItem[] xLabels = null;
    protected AxisLabelItem[] yLabels = null;
    protected int frame;
    protected GraphWorldParams params;
    protected RegularField inField = null;
    protected int dim;
    protected int nComps;
    protected boolean fromInput = true;
    protected boolean fromParams = false;
    protected ArrayList<DataArray> data = new ArrayList<>();
    protected ArrayList<Color> graphColors = new ArrayList<>();
    protected String[] axDesc = null;
    protected int fontHeight = 15;

    public GraphWorld(RegularField inField, GraphWorldParams params)
    {
        dim = inField.getDims()[0];
        data.clear();
        graphColors.clear();
        DisplayedData[] disp = params.getDisplayedData();
        for (int i = 0; i < disp.length; i++) {
            DisplayedData displayedData = disp[i];
            if (displayedData.isDisplayed()) {
                data.add(inField.getComponent(displayedData.getIndex()));
                graphColors.add(displayedData.getColor());
            }
        }
        this.params = params;
        data = inField.getComponents();
        name = "graph world";
    }

    float[] toScr(float x, float y)
    {
        float[] p = new float[2];
        p[0] = x0 + (x - xmin) * dx;
        p[1] = y0 - (y - ymin) * dy;
        return p;
    }

    @Override
    public void drawLocal2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
    {
        if (renderingWindow == null ||
            params == null ||
            params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        fontHeight = (int) (height * params.getFontSize());
        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        axDesc = params.getAxesLabels();
        String xf, yf;

        int yMargin = fontHeight + 2;
        if (data == null || data.size() < 1) {
            ymin = 0;
            ymax = 1;
            return;
        }
        ymin = (float)data.get(0).getPreferredMinValue();
        ymax = (float)data.get(0).getPreferredMaxValue();
        for (int i = 1; i < data.size(); i++) {
            if (ymin > data.get(i).getPreferredMinValue())
                ymin = (float)data.get(i).getPreferredMinValue();
            if (ymax < data.get(i).getPreferredMaxValue())
                ymax = (float)data.get(i).getPreferredMaxValue();
        }
        origY = (int) (height * params.getVerticalExtents()[0] / 100.f);
        endY = (int) (height * params.getVerticalExtents()[1] / 100.f);
        h = endY - origY;
        if (h < 2 * yMargin + 100)
            h = 2 * yMargin + 100;
        yRange = new Range((h - 2 * yMargin) / (5 * fontHeight), ymin, ymax, false);
        ymax = yRange.getUp();
        ymin = yRange.getLow();
        dy = (h - 2 * yMargin) / (ymax - ymin);
        y0 = endY - yMargin;
        int yk = (int) (log10(yRange.getStep()));
        if (yk > 0)
            yf = "%" + (yk + 2) + ".0f";
        else
            yf = "%" + (4 - yk) + "." + (2 - yk) + "f";

        int xll = yk + 2;
        if (yk < 0)
            xll = 4 - yk;
        xll = fm.stringWidth(("12345678901234567890").substring(0, xll));
        int xMargin = fm.stringWidth(axDesc[1]);
        if (xll > xMargin)
            xMargin = xll;
        int xrMargin = fm.stringWidth(axDesc[0]);
        xMargin += xrMargin;

        origX = (int) (width * params.getHorizontalExtents()[0] / 100.f);
        endX = (int) (width * params.getHorizontalExtents()[1] / 100.f);
        w = endX - origX;
        xRange = new Range(max(w - xMargin, 200) / (3 * xll), 0.f, 1.f * dim, false);
        xmax = xRange.getUp();
        xmin = 0;
        dx = (w - xMargin) / xmax;
        x0 = origX + xMargin - xrMargin;

        float[] urCorner = toScr(xmax, ymax);

        if (inField == null)
            return;
        int xk = (int) (log10(xRange.getStep()));
        if (xk >= 0)
            xf = "%" + (xk + 2) + ".0f";
        else
            xf = "%" + (2 - xk) + "." + (-xk) + "f";

        GeneralPath axes = new GeneralPath();
        gr.setStroke(new BasicStroke(params.getLineWidth()));

        float[] xr = xRange.getRange();
        float[] yr = yRange.getRange();
        gr.setColor(params.getColor());
        axes.moveTo(x0, urCorner[1]);
        axes.lineTo(x0, y0);
        axes.lineTo(urCorner[0], y0);
        gr.draw(axes);

        GeneralPath ticklines = new GeneralPath();
        gr.setStroke(new BasicStroke(1.f, BasicStroke.CAP_ROUND,
                                     BasicStroke.JOIN_ROUND, 1.0f, new float[]{
            1, 3
        }, 0));
        for (float xt = xRange.getRange()[0]; xt <= xRange.getRange()[1]; xt += xRange.getStep()) {
            float xs = toScr(xt, yr[1])[0];
            ticklines.moveTo(xs, y0);
            ticklines.lineTo(xs, urCorner[1]);
            gr.drawString(String.format(xf, xt), xs - 5, y0 + 3 + fontHeight);
        }
        for (float yt = yRange.getRange()[0]; yt <= yRange.getRange()[1]; yt += yRange.getStep()) {
            float ys = toScr(xr[0], yt)[1];
            ticklines.moveTo(x0, ys);
            ticklines.lineTo(urCorner[0], ys);
            String l = String.format(yf, yt);
            gr.drawString(l, x0 - fm.stringWidth(l) - 3, ys + fontHeight / 2.f);
        }
        gr.draw(ticklines);

        fontHeight = (int) (1.5 * height * params.getFontSize());

        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        if (axDesc != null && axDesc.length >= 2) {
            gr.drawString(axDesc[0], urCorner[0] + 3, y0);
            gr.drawString(axDesc[1], x0 - 10, urCorner[1] - fontHeight);
        }

        fontHeight = (int) (2 * height * params.getFontSize());

        if (params.getTitle() != null && !params.getTitle().isEmpty()) {
            gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
            fm = gr.getFontMetrics();
            int titleWidth = fm.stringWidth(params.getTitle());
            gr.drawString(params.getTitle(), (origX + endX - titleWidth) / 2, origY - fontHeight);
        }

        fontHeight = (int) (height * params.getFontSize());

        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        for (int i = 0; i < data.size(); i++) {
            gr.setColor(graphColors.get(i));
            GeneralPath graph = new GeneralPath();
            gr.setStroke(new BasicStroke(params.getLineWidth()));
            float[] grData = data.get(i).getRawFloatArray().getData();
            float[] p = toScr(0, grData[0]);
            graph.moveTo(p[0], p[1]);
            for (int j = 0; j < grData.length; j++) {
                p = toScr(j, grData[j]);
                graph.lineTo(p[0], p[1]);
            }
            gr.draw(graph);
            if (!params.isColorLegend())
                continue;
            gr.drawString(data.get(i).getName(), urCorner[0] + 2, origY + i * (3 + fontHeight));
        }
    }
}
