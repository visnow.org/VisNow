/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.Appearance;
import org.jogamp.java3d.TextureAttributes;

/**
 *
 * @author Krzysztof S. Nowinski, Warsaw University, ICM
 *
 */
public class OpenAppearance extends Appearance
{

    /**
     * Creates a new instance of OpenAppearance
     */
    public OpenAppearance()
    {
        setCapability(Appearance.ALLOW_LINE_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_LINE_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_POINT_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_POINT_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_MATERIAL_READ);
        setCapability(Appearance.ALLOW_MATERIAL_WRITE);
        setCapability(Appearance.ALLOW_TEXTURE_READ);
        setCapability(Appearance.ALLOW_TEXTURE_WRITE);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
        setMaterial(new OpenMaterial());
        setTransparencyAttributes(new OpenTransparencyAttributes());
        setLineAttributes(new OpenLineAttributes());
        setPointAttributes(new OpenPointAttributes());
        setPolygonAttributes(new OpenPolygonAttributes());
        setColoringAttributes(new OpenColoringAttributes());
        setTextureAttributes(new TextureAttributes());
        setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_WRITE);
    }

    @Override
    public OpenAppearance cloneNodeComponent(boolean forceDuplicate)
    {
        //      getPolygonAttributes().setCapability(PolygonAttributes.);
        OpenAppearance openAppearance = new OpenAppearance();
        openAppearance.duplicateNodeComponent(this, forceDuplicate);
        return openAppearance;
    }
    
    public void copyValuesFrom(Appearance src)
    {
        ((OpenTransparencyAttributes)getTransparencyAttributes()).
                copyValuesFrom(src.getTransparencyAttributes());
        ((OpenLineAttributes)getLineAttributes()).
                copyValuesFrom(src.getLineAttributes());
        ((OpenPointAttributes)getPointAttributes()).
                copyValuesFrom(src.getPointAttributes());
        ((OpenPolygonAttributes)getPolygonAttributes()).
                copyValuesFrom(src.getPolygonAttributes());
        ((OpenColoringAttributes)getColoringAttributes()).
                copyValuesFrom(src.getColoringAttributes());
        if (getMaterial() == null || !(getMaterial() instanceof OpenMaterial))
            setMaterial(new OpenMaterial());
        if (src.getMaterial() == null || !(src.getMaterial() instanceof OpenMaterial))   
            src.setMaterial(new OpenMaterial());
        else
            ((OpenMaterial)getMaterial()).copyValuesFrom(src.getMaterial());
    }

}
