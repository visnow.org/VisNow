/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.Node;
import org.jogamp.java3d.TransformGroup;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class OpenTransformGroup extends TransformGroup implements Debuggable
{

    /**
     * Creates a new instance of OpenTransformGroup
     */
    public OpenTransformGroup()
    {
        setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        setCapability(TransformGroup.ALLOW_CHILDREN_READ);
        setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        setCapability(TransformGroup.ALLOW_LOCAL_TO_VWORLD_READ);
    }

    public OpenTransformGroup(String name)
    {
        super.setName(name);
        setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        setCapability(TransformGroup.ALLOW_CHILDREN_READ);
        setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        setCapability(TransformGroup.ALLOW_LOCAL_TO_VWORLD_READ);
    }

    @Override
    public void setUserData(Object userData)
    {
        super.setUserData(userData);
        for (int i = 0; i < numChildren(); i++)
            if (getChild(i) != null && userData != null)
                getChild(i).setUserData(getUserData());
    }

    public void printDebugInfo()
    {
        printDebugInfo(0);
    }
    
    public void printDebugInfo(int level)
    {
        for (int i = 0; i < level; i++) 
            System.out.print("  ");
        System.out.println(("" + this).replaceFirst("org.visnow.vn.geometries.", ""));
        int n = this.numChildren();
        for (int i = 0; i < n; i++) {
            Node child = this.getChild(i);
            if (child instanceof OpenBranchGroup)
                ((OpenBranchGroup) child).printDebugInfo(level + 1);
            else if (child instanceof OpenTransformGroup)
                ((OpenTransformGroup) child).printDebugInfo(level + 1);
            else if (child instanceof OpenShape3D) 
                ((OpenShape3D) child).printDebugInfo(level + 1);
        }
    }

    public Node cloneNode(boolean forceDuplicate)
    {
        OpenTransformGroup openTransformGroup = new OpenTransformGroup();
        openTransformGroup.duplicateNode(this, forceDuplicate);
        return openTransformGroup;
    }

    public void setCurrentViewer(Display3DPanel panel)
    {
        for (int i = 0; i < this.numChildren(); i++) {
            Node n = this.getChild(i);
            if (n instanceof OpenBranchGroup) {
                ((OpenBranchGroup) n).setCurrentViewer(panel);
            } else if (n instanceof OpenTransformGroup) {
                ((OpenTransformGroup) n).setCurrentViewer(panel);
            }
        }
    }

}
