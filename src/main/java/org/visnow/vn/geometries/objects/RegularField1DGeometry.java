/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.awt.Color;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.IndexedPointArray;
import org.jogamp.java3d.LineStripArray;
import org.jogamp.vecmath.Color3f;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LogicLargeArray;
import static org.visnow.vn.geometries.objects.RegularFieldGeometry.resetGeometry;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import static org.visnow.vn.geometries.objects.generics.ObjectCapabilities.*;
import org.visnow.vn.geometries.parameters.PresentationParams;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField1DGeometry extends RegularFieldGeometry
{

    private static final Logger LOGGER = Logger.getLogger(RegularField1DGeometry.class);
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenShape3D pointShape = new OpenShape3D();
    protected OpenShape3D frameShape = new OpenShape3D();
    protected LineStripArray polyline = null;
    protected LogicLargeArray lastMask = null;

    private final void init()
    {
        geometries.removeAllChildren();
        geometries.addChild(lineShape);
        geometries.addChild(pointShape);
        geometries.addChild(frameShape);
        renderEventListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (ignoreUpdate)
                    return;
                int extent = e.getUpdateExtent();
                int cMode = dataMappingParams.getColorMode();
                if (currentColorMode < 0) {
                    currentColorMode = cMode;
                    updateShapes();
                    return;
                }
                if ((extent & RenderEvent.DATA_MAP) != 0) {
                    validateColorMode();
                    if (resetGeometry[currentColorMode][cMode])
                        updateShapes();
                    else
                        updateColors();
                    currentColorMode = cMode;
                    return;
                }
                if (extent == RenderEvent.COORDS)
                    updateCoords();
                if (extent == RenderEvent.GEOMETRY)
                    updateShapes();
                currentColorMode = cMode;
            }
        };

    }

    public RegularField1DGeometry(String name)
    {
        super(name);
        init();
    }

    public RegularField1DGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        init();
    }

    @Override
    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 1)
            return false;
        coords = inField.getCurrentCoords();
        if (inField.getCurrentCoords() != null) {
            coordsChanged = this.regularField == null;
        } else {
            coordsChanged = true;
        }
        super.setField(inField);
        renderingParams.setDisplayMode(RenderingParams.EDGES);
        //normals = inField.getNormals() == null ? null : inField.getNormals().getData();
        return true;
    }

    public void generateEdges()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        nNodes = dims[0];
        lineStripCounts = new IntLargeArray(new int[]{dims[0]});
        polyline = new LineStripArray((int) nNodes, GeometryArray.COORDINATES, lineStripCounts.getData());
        setStandardCapabilities(polyline);
        if (detach)
            geometry.postattach();
    }

    public void generateColoredEdges()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        nNodes = dims[0];
        lineStripCounts = new IntLargeArray(new int[]{dims[0]});
        polyline = new LineStripArray((int) nNodes, GeometryArray.COORDINATES | GeometryArray.COLOR_4, lineStripCounts.getData());
        setStandardCapabilities(polyline);
        if (detach)
            geometry.postattach();
    }

    private void generateNodeIndices()
    {
        if (regularField.getCurrentMask() == null) {
            nNodePoints = dims[0];
            coordIndices = new IntLargeArray(nNodePoints, false);
            long k = 0;
            for (long j = 0; j < dims[0]; j++, k++) {
                coordIndices.setLong(k, j);
            }
        } else {
            LogicLargeArray mask = field.getCurrentMask();
            nNodePoints = 0;
            for (long i = 0; i < mask.length(); i++) {
                if (mask.getByte(i) == 1)
                    nNodePoints++;
            }
            coordIndices = new IntLargeArray(nNodePoints, true);
            long m = 0;
            for (long j = 0; j < dims[0]; j++) {
                if (mask.getByte(j) == 1)
                    coordIndices.setLong(m++, j);
            }
        }
    }

    public void generateColoredNodes()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new IndexedPointArray((int) nNodes,
                                        GeometryArray.COORDINATES |
                                        GeometryArray.COLOR_4 |
                                        GeometryArray.USE_COORD_INDEX_ONLY,
                                        (int) nNodePoints);
        setStandardCapabilities(nodeArr);
        nodeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    public void generateNodes()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new IndexedPointArray((int) nNodes,
                                        GeometryArray.COORDINATES |
                                        GeometryArray.USE_COORD_INDEX_ONLY,
                                        (int) nNodePoints);
        setStandardCapabilities(nodeArr);
        nodeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    @Override
    public void updateCoords(boolean force)
    {

        if (!force || regularField == null)
            return;
        boolean detach = geometry.postdetach();

        coords = regularField.getCurrentCoords();
        if (coords == null) {
            coords = regularField.getCoordsFromAffine();
        }

        if (polyline != null)
            polyline.setCoordinates(0, coords.getData());
        if (nodeArr != null)
            nodeArr.setCoordinates(0, coords.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords(FloatLargeArray newCoords)
    {
        if (regularField == null || regularField.getDims() == null || newCoords.length() != 3 * regularField.getNNodes()) {
            return;
        }
        boolean detach = geometry.postdetach();
        coords = newCoords;
        if (polyline != null)
            polyline.setCoordinates(0, coords.getData());
        if (nodeArr != null)
            nodeArr.setCoordinates(0, coords.getData());
        if (detach)
            geometry.postattach();
    }

    public void updateColors()
    {
        boolean detach = geometry.postdetach();
        if (colors == null || colors.length() != 4 * regularField.getNNodes())
            colors = new ByteLargeArray(4 * regularField.getNNodes(), true);
        ColorMapper.map(regularField, dataMappingParams, renderingParams.getDiffuseColor(), colors.getData());
        ColorMapper.mapTransparency(regularField, dataMappingParams.getTransparencyParams(), colors.getData());
        if (polyline != null)
            polyline.setColors(0, colors.getData());
        if (nodeArr != null)
            nodeArr.setColors(0, colors.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateDataMap()
    {
        boolean detach = geometry.postdetach();
        if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORED)
            updateColors();
        if (detach)
            geometry.postattach();
    }

    public void updateShapes()
    {
        updateGeometry((renderingParams.getDisplayMode() & RenderingParams.EDGES) != 0,
                       (renderingParams.getDisplayMode() & RenderingParams.NODES) != 0,
                       (renderingParams.getDisplayMode() & RenderingParams.IMAGE) != 0,
                       (renderingParams.getDisplayMode() & RenderingParams.OUTLINE_BOX) != 0);
        if (geometries.getParent() == null)
            transformedGeometries.addChild(geometries);
        if (transformedGeometries.getParent() == null)
            geometry.addChild(transformedGeometries);
    }

    public void updateData()
    {
        boolean restructure = false;
        LogicLargeArray mask = field.getCurrentMask();
        if (mask != null)
            if (lastMask == null || lastMask.length() != mask.length()) {
                restructure = true;
                lastMask = mask;
            } else {
                for (int i = 0; i < mask.length(); i++)
                    if (mask.getByte(i) != lastMask.getByte(i)) {
                        restructure = true;
                        break;
                    }
            }
        updateExtents();
        if (restructure)
            updateShapes();
        else {
            updateColors();
            updateCoords();
        }
    }

    public void updateGeometry(RegularField inField, boolean showEdges, boolean showNodes, boolean showImage, boolean showBox)
    {
        if (regularField != inField && !setField(inField))
            return;
        updateGeometry(showEdges, showNodes, showImage, showBox);
    }

    public void updateGeometry(boolean showEdges, boolean showNodes, boolean showImage, boolean showBox)
    {
        boolean detach = geometry.postdetach();
        if (lineShape != null)
            lineShape.removeAllGeometries();
        if (pointShape != null)
            pointShape.removeAllGeometries();
        if (frameShape != null)
            frameShape.removeAllGeometries();
        polyline = null;
        nodeArr = null;
        boxArr = null;

        if (showEdges) {
            switch (dataMappingParams.getColorMode()) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredEdges();
                    break;
                case DataMappingParams.UVTEXTURED:
                case DataMappingParams.UNCOLORED:
                    generateEdges();
                    break;
            }
            lineShape.addGeometry(polyline);
        }
        if (showNodes) {
            switch (dataMappingParams.getColorMode()) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredNodes();
                    break;
                case DataMappingParams.UVTEXTURED:
                case DataMappingParams.UNCOLORED:
                    generateNodes();
                    break;
            }
            pointShape.addGeometry(nodeArr);
        }
        if (showBox) {
            //updateExtents();
            float[][] ext = regularField.getExtents();
            float[] boxVerts = new float[24];
            boxArr = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });
            boxArr.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 2; k++) {
                        int m = 3 * (4 * i + 2 * j + k);
                        //                 boxVerts[m] = (extents[k][0]==Float.MAX_VALUE || extents[k][0] == -Float.MAX_VALUE)?ownExtents[k][0]:extents[k][0];
                        //                 boxVerts[m + 1] = (extents[j][1]==Float.MAX_VALUE || extents[j][1] == -Float.MAX_VALUE)?ownExtents[j][1]:extents[j][1];
                        //                 boxVerts[m + 2] = (extents[i][2]==Float.MAX_VALUE || extents[i][2] == -Float.MAX_VALUE)?ownExtents[i][2]:extents[i][2];
                        boxVerts[m] = ext[k][0];
                        boxVerts[m + 1] = ext[j][1];
                        boxVerts[m + 2] = ext[i][2];
                    }
            boxArr.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            boxArr.setCoordinates(0, boxVerts);
            frameShape.addGeometry(boxArr);
        }

        structureChanged = false;
        updateCoords();
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                updateColors();
                break;
            case DataMappingParams.UVTEXTURED:
                break;
            case DataMappingParams.UNCOLORED:
                break;
        }
        appearance = renderingParams.getAppearance();
        appearance.setUserData(this);
        lineAppearance = renderingParams.getLineAppearance();
        lineAppearance.setUserData(this);
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

        Color bgrColor = renderingParams.getBackgroundColor();
        float[] bgrColorComps = new float[3];
        bgrColor.getColorComponents(bgrColorComps);
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
            appearance.getColoringAttributes().setColor(new Color3f(bgrColorComps[0], bgrColorComps[1], bgrColorComps[2]));
        } else
            appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        lineAppearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        lineAppearance.setLineAttributes(renderingParams.getLineAppearance().getLineAttributes());
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        pointShape.setAppearance(lineAppearance);
        lineShape.setAppearance(lineAppearance);
        frameShape.setAppearance(lineAppearance);
        if (detach)
            geometry.postattach();
    }

    public OpenBranchGroup getGeometry(RegularField inField)
    {
        updateGeometry(inField);
        return geometry;
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return null;
        regularField = (RegularField) inField;
        return getGeometry((RegularField) inField);
    }

    @Override
    public void createGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return;
        updateGeometry((RegularField) inField);
    }

    public void updateGeometry(RegularField inField)
    {
        setField(inField);
        updateShapes();
    }

    @Override
    public void updateGeometry(Field inField)
    {
        if (inField instanceof RegularField)
            updateGeometry((RegularField) inField);
    }

    private void validateColorMode()
    {
        colorMode = dataMappingParams.getColorMode();
        // check if color mode and selected components combination is valid; fall back to UNCOLORED otherwise
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                if (regularField.getComponent(dataMappingParams.getColorMap0().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.RGB:
                if (regularField.getComponent(dataMappingParams.getRedParams().getDataComponentName()) != null ||
                    regularField.getComponent(dataMappingParams.getGreenParams().getDataComponentName()) != null ||
                    regularField.getComponent(dataMappingParams.getBlueParams().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.UVTEXTURED:
                break;
        }
        colorMode = DataMappingParams.UNCOLORED;
    }

    @Override
    public void updateGeometry()
    {
        if (ignoreUpdate)
            return;
        dataMappingParams.setParentObjectSize((int) nNodes);
        updateShapes();
    }
}
