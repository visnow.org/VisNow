/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.awt.Color;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.TransformParams;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
abstract public class FieldGeometry extends DataMappedGeometryObject
{

    protected Field field;

    protected OpenBranchGroup geometries = new OpenBranchGroup("field geometry");
    protected TransformParams transformParams = null;
    protected OpenTransformGroup transformedGeometries = new OpenTransformGroup("transformed field geometry");
    protected boolean ignoreUpdate = false;

    public FieldGeometry()
    {
        this("");
    }

    public FieldGeometry(String name)
    {
        this.name = name;
        debug = VisNow.isDebug();
    }

    public FieldGeometry(PresentationParams presentationParams)
    {
        this("", presentationParams);
    }

    public FieldGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams.getDataMappingParams());
        transformParams = presentationParams.getTransformParams();
        debug = VisNow.isDebug();
    }

    public void fireBgrColorChanged(Color color)
    {
        ColorEvent e = new ColorEvent(this, color);
        for (ColorListener colorListener : bgrColorListenerList)
            colorListener.colorChoosen(e);
    }

    abstract public void createGeometry(Field inField);

    abstract public OpenBranchGroup getGeometry(Field inField);

    abstract public void updateGeometry(Field inField);

    abstract public void updateCoords(boolean force);

    abstract public void updateCoords(FloatLargeArray coords);

    abstract public void updateCoords();

    abstract public void updateDataMap();

    abstract public OpenBranchGroup getGeometry();

    abstract public boolean setField(Field inField);

    abstract public Field getField();

    /**
     * @return the ignoreUpdate
     */
    public boolean isIgnoreUpdate()
    {
        return ignoreUpdate;
    }

    /**
     * @param ignoreUpdate the ignoreUpdate to set
     */
    public void setIgnoreUpdate(boolean ignoreUpdate)
    {
        this.ignoreUpdate = ignoreUpdate;
    }
}
