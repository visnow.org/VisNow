/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.awt.Color;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import java.util.SortedSet;

import org.jogamp.java3d.J3DGraphics2D;
import org.jogamp.java3d.Node;
import org.jogamp.java3d.Transform3D;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;

/**
 *
 * @author Krzysztof S. Nowiński, Warsaw University ICM
 */
public interface GeometryParent
{

    public void addChild(GeometryObject child);

    public OpenBranchGroup getGeometryObj();

    public void clearAllGeometry();

    public void addNode(Node node);

    public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h);

    public int getAreaWidth();

    public int getAreaHeight();

    public Color getBackgroundColor();

    public boolean removeChild(GeometryObject child);

    public void setScale(double s);

    public RenderingParams getRenderingParams();

    @Override
    public String toString();

    public SortedSet<GeometryObject> getChildren();

    public void revalidate();

    public void printDebugInfo();

    public void setTransparency();

    public void setShininess();

    public void setLineThickness();

    public void setLineStyle();

    public void setColor();

    public void setTransform(Transform3D transform);

    public void setExtents(float[][] ext);

    public void updateExtents();
}
