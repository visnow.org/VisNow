/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.LineAttributes;
import org.visnow.vn.lib.utils.VisNowCallTrace;

/**
 *
 * @author Krzysztof S. Nowinski, Warsaw University, ICM
 *
 */
public class OpenLineAttributes extends LineAttributes
{

    /**
     * Creates a new instance of OpenLineAttributes
     */
    public OpenLineAttributes()
    {
        setCapability(LineAttributes.ALLOW_ANTIALIASING_READ);
        setCapability(LineAttributes.ALLOW_ANTIALIASING_WRITE);
        setCapability(LineAttributes.ALLOW_PATTERN_READ);
        setCapability(LineAttributes.ALLOW_PATTERN_WRITE);
        setCapability(LineAttributes.ALLOW_WIDTH_READ);
        setCapability(LineAttributes.ALLOW_WIDTH_WRITE);
        setLineAntialiasingEnable(true);
    }

    public OpenLineAttributes(float lineWidth, int linePattern, boolean lineAntialiasing)
    {
        super(lineWidth, linePattern, lineAntialiasing);
        setCapability(LineAttributes.ALLOW_ANTIALIASING_READ);
        setCapability(LineAttributes.ALLOW_ANTIALIASING_WRITE);
        setCapability(LineAttributes.ALLOW_PATTERN_READ);
        setCapability(LineAttributes.ALLOW_PATTERN_WRITE);
        setCapability(LineAttributes.ALLOW_WIDTH_READ);
        setCapability(LineAttributes.ALLOW_WIDTH_WRITE);
    }

    @Override
    public OpenLineAttributes cloneNodeComponent(boolean forceDuplicate)
    {
        OpenLineAttributes openLineAttributes = new OpenLineAttributes();
        openLineAttributes.duplicateNodeComponent(this, forceDuplicate);
        return openLineAttributes;
    }

    public void copyValuesFrom(LineAttributes src)
    {
        setLineWidth(src.getLineWidth());
        setLinePattern(src.getLinePattern());
    }

//    @Override
//    public void setLineWidth(float lineWidth)
//    {
//        super.setLineWidth(lineWidth);
//        System.out.println("setting line width " + lineWidth);
//        VisNowCallTrace.trace();
//    }
}
