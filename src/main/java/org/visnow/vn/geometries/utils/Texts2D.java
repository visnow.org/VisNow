/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.utils;

import java.awt.Color;
import java.awt.Font;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.lib.utils.geometry2D.CXYZString;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Texts2D
{

    protected float[] textCoords;
    protected String[] texts;
    protected CXYZString[] glyphs = null;
    protected FontParams fontParams;

    public Texts2D(float[] textCoords, String[] texts, FontParams fontParams)
    {
        this.textCoords = textCoords;
        this.texts = texts;
        this.fontParams = fontParams;
        int nGlyphs = texts.length;
        Color textColor2d = fontParams.getColor();
        Font font = fontParams.getFont2D();
        glyphs = new CXYZString[nGlyphs];
        for (int i = 0; i < nGlyphs; i++)
            glyphs[i] = new CXYZString(texts[i], textColor2d,
                                       textCoords[3 * i], textCoords[3 * i + 1], textCoords[3 * i + 2],
                                       font, fontParams.getSize());
    }

    public CXYZString[] getGlyphs()
    {
        return glyphs;
    }
}
