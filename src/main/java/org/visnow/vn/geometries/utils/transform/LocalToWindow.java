/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.utils.transform;

/*
 * LocalToWindow.java
 *
 * Created on May 15, 2004, 10:51 PM
 */
import java.awt.Dimension;
import java.awt.Point;
import java.util.Vector;
import org.jogamp.java3d.Canvas3D;
import org.jogamp.java3d.Node;
import org.jogamp.java3d.RestrictedAccessException;
import org.jogamp.java3d.Transform3D;
import org.jogamp.java3d.View;
import org.jogamp.vecmath.Point2d;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Vector3d;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import org.jogamp.java3d.TransformGroup;
import org.visnow.vn.geometries.viewer3d.WrappedCanvas3D;

/**
 * Utility class for doing local->window transformations for case where
 * Canvas3D is a simple display such as a monitor. This won't work for the
 * more complex cases (i.e. a multiple canvases, head tracking, etc).
 * <p/>
 * Usage:
 * <pre>
 *    // after the canvas and node are created
 *    LocalToWindow locToWindow = LocalToWindow(node, canvas);
 *    ...
 *    // when we need to transform (canvas location and node transforms may have
 *    // changed)
 *    locToWindow.update(); // make sure transforms are up to date
 *
 *    Point3d[] localPts = &lt; some local coords to transform >
 *    Point[] windowPts = &lt; the area to put the tranformed pts >
 *    for (int i = 0; i &lt; localPts.length; i++) {
 *       locToWindow.transformPt(localPts[i], windowPts[i]);
 *    }
 * </pre>
 */
public class LocalToWindow
{

    Canvas3D canvas = null;
    Node node = null;

    // inquired/derived data
    Transform3D localToVworld = new Transform3D();
    Transform3D vworldToImagePlate = new Transform3D();
    Transform3D localToImagePlate = new Transform3D();
    Transform3D imagePlateToLocal = new Transform3D();
    Point3d eyePos = new Point3d();
    int projType;
    Point canvasScr;
    Dimension screenSize;
    double metersPerPixelX;
    double metersPerPixelY;

    // Temporaries
    Point3d localPt = new Point3d();
    Point3d imagePlatePt = new Point3d();
    Vector3d projVec = new Vector3d();
    Point2d screenPt = new Point2d();
    Point2d tempPt2d = new Point2d();

    /**
     * Creates a LocalToWindow object with no associated node or canvas.
     * The node and canvas must be set before transforming points
     */
    /**
     * Creates a new instance of LocalToWindow
     */
    public LocalToWindow()
    {
    }

    public LocalToWindow(Node node, Canvas3D canvas)
    {
        this.canvas = canvas;
        this.node = node;
        update();
    }

    /**
     * Either create LocalToWindow() just before transforming points or call
     * this method to ensure that the transforms are up to date. Note: if
     * you are transforming several points, you only need to call this method
     * once.
     */
    public final void update()
    {
        if(canvas.isOffScreen()) {
            doUpdate();
        } else {
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    doUpdate();
                }
            });
        }
    }

    private final void doUpdate() {
        if(canvas == null || node == null)
            return;

        try {
            node.getLocalToVworld(localToVworld);
        } catch (RestrictedAccessException e) {
            System.out.println("canvas secret yet");
            return;
        }
        try {
            canvas.getVworldToImagePlate(vworldToImagePlate);
        } catch (RestrictedAccessException e) {
            System.out.println("canvas secret yet");
            return;
        }

        localToImagePlate.mul(vworldToImagePlate, localToVworld);
        imagePlateToLocal.invert(localToImagePlate);
        canvas.getCenterEyeInImagePlate(eyePos);
        projType = canvas.getView().getProjectionPolicy();
        if(canvas instanceof WrappedCanvas3D)
            canvasScr = ((WrappedCanvas3D)canvas).getWrappedLocationOnScreen();
        else
            canvasScr = canvas.getLocationOnScreen();
        screenSize = canvas.getScreen3D().getSize();
        double physicalScreenWidth
                = canvas.getScreen3D().getPhysicalScreenWidth();
        double physicalScreenHeight
                = canvas.getScreen3D().getPhysicalScreenHeight();
        metersPerPixelX = physicalScreenWidth / (double) screenSize.width;
        metersPerPixelY = physicalScreenHeight / (double) screenSize.height;
    }


    /**
     * Set the node and canvas and call update()
     */
    public void update(Node node, Canvas3D canvas)
    {
        this.canvas = canvas;
        this.node = node;
        update();
    }

    public void update(Node node)
    {
        this.node = node;
        update();
    }

    /**
     * Transform the point from local coords to window coords
     */
    public float transformPt(Point3d localPt, Point2d windowPt)
    {
        if (windowPt == null || localPt == null || canvasScr == null || screenPt == null || screenSize == null || imagePlatePt == null || localToImagePlate == null) {
            return 1.0f;
        }

        localToImagePlate.transform(localPt, imagePlatePt);
        double zScale = 1.0; // default, used for PARALELL_PROJECTION
        if (projType == View.PERSPECTIVE_PROJECTION) {
            projVec.sub(imagePlatePt, eyePos);
            zScale = eyePos.z / (-projVec.z);
            screenPt.x = eyePos.x + projVec.x * zScale;
            screenPt.y = eyePos.y + projVec.y * zScale;
        } else {
            screenPt.x = imagePlatePt.x;
            screenPt.y = imagePlatePt.y;
        }
        windowPt.x = (screenPt.x / metersPerPixelX) - canvasScr.x;
        windowPt.y = screenSize.height - 1 -
            (screenPt.y / metersPerPixelY) - canvasScr.y;
        return 1.f + (float) projVec.z;
    }

    /**
     * Transform the point from local coords to window coords
     */
    public float transformPt(Point3d localPt, Point windowPt)
    {
        float depth = transformPt(localPt, tempPt2d);
        windowPt.x = (int) round(tempPt2d.x);
        windowPt.y = (int) round(tempPt2d.y);
        return depth;
    }

    public float transformPt(Point3d localPt, int[] wCoords)
    {
        float depth = transformPt(localPt, tempPt2d);
        wCoords[0] = (int) round(tempPt2d.x);
        wCoords[1] = (int) round(tempPt2d.y);
        return depth;
    }

    public float transformPt(double[] coords, int[] wCoords)
    {
        if (coords == null || coords.length < 3)
            return 0;
        float depth = transformPt(new Point3d(coords), tempPt2d);
        wCoords[0] = (int) round(tempPt2d.x);
        wCoords[1] = (int) round(tempPt2d.y);
        return depth;
    }

    public float transformPt(double[] coords, double[] wCoords)
    {
        if (coords == null || coords.length < 3)
            return 0;
        float depth = transformPt(new Point3d(coords), tempPt2d);
        wCoords[0] = tempPt2d.x;
        wCoords[1] = tempPt2d.y;
        return depth;
    }

    public float transformPt(double[] coords, float[] wCoords)
    {
        if (coords == null || coords.length < 3)
            return 0;
        float depth = transformPt(new Point3d(coords), tempPt2d);
        wCoords[0] = (float) tempPt2d.x;
        wCoords[1] = (float) tempPt2d.y;
        return depth;
    }

    public float transformPt(float[] coords, int[] wCoords)
    {
        if (coords == null || coords.length < 3)
            return 0;
        return transformPt(new double[]{coords[0], coords[1], coords[2]}, wCoords);
    }

    public float transformPt(float[] coords, float[] wCoords)
    {
        if (coords == null || coords.length < 3)
            return 0;
        return transformPt(new double[]{coords[0], coords[1], coords[2]}, wCoords);
    }

    public void reverseTransformPt(Point2d windowPt, float z, Point3d localPt)
    {
        if (windowPt == null || localPt == null || canvasScr == null || screenPt == null || screenSize == null || imagePlatePt == null || imagePlateToLocal == null)
            return;

        screenPt.x = (windowPt.x + canvasScr.x) * metersPerPixelX;
        screenPt.y = (screenSize.height - 1 - windowPt.y - canvasScr.y) * metersPerPixelY;
        double zScale = 1.0;
        if (projType == View.PERSPECTIVE_PROJECTION) {
            projVec.sub(imagePlatePt, eyePos);
            projVec.z = z;
            zScale = -eyePos.z / z;
            projVec.x = (screenPt.x - eyePos.x) / zScale;
            projVec.y = (screenPt.y - eyePos.y) / zScale;
            imagePlatePt.add(eyePos, projVec);
        } else {
            imagePlatePt.x = screenPt.x;
            imagePlatePt.y = screenPt.y;
            imagePlatePt.z = z;
        }
        imagePlateToLocal.transform(imagePlatePt, localPt);
    }

    public void reverseTransformPt(int ix, int iy, float z, float[] coords)
    {
        if (coords == null || coords.length != 3)
            return;
        reverseTransformPt(new Point2d((double) ix, (double) iy), z, localPt);
        coords[0] = (float) localPt.x;
        coords[1] = (float) localPt.y;
        coords[2] = (float) localPt.z;
    }

    public float[] reverseTransformPt(int ix, int iy, float z)
    {
        float[] coords = new float[3];
        reverseTransformPt(ix, iy, z, coords);
        return coords;
    }

    public int getDir()
    {
        double[] v = new double[16];
        localToImagePlate.get(v);
        int d = 3;
        float c = 0;
        for (int i = 0; i < 3; i++)
            if (abs(v[8 + i]) > c) {
                c = (float) abs(v[8 + i]);
                if (v[8 + i] > 0)
                    d = i + 1;
                else
                    d = -i - 1;
            }
        return d;
    }

    public int getDir(Transform3D externalTransform)
    {
        if (externalTransform == null)
            return getDir();

        double[] v = new double[16];
        Transform3D external2ImagePlate = new Transform3D();
        external2ImagePlate.set(localToImagePlate);
        external2ImagePlate.mul(externalTransform);
        external2ImagePlate.get(v);
        int d = 3;
        float c = 0;
        for (int i = 0; i < 3; i++)
            if (abs(v[8 + i]) > c) {
                c = (float) abs(v[8 + i]);
                if (v[8 + i] > 0)
                    d = i + 1;
                else
                    d = -i - 1;
            }
        return d;
    }

    public Transform3D getLocalToVworld()
    {
        return localToVworld;
    }

    public static Transform3D getTransform(Node src, Node trg)
    {
        Transform3D stw = new Transform3D();
        src.getLocalToVworld(stw);
        Transform3D wtt = new Transform3D();
        trg.getLocalToVworld(wtt);
        wtt.invert();
        stw.mul(wtt);
        return stw;
    }

    public static Transform3D getTransform(Node src)
    {
        Transform3D stw = new Transform3D();
        src.getLocalToVworld(stw);
        return stw;
    }
}
