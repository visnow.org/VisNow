/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.utils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.jogamp.java3d.*;
import org.jogamp.vecmath.Color3f;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenMaterial;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.jscic.utils.VectorMath;
import org.visnow.vn.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.parameters.VolumeShadingParams;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.SHADING_COMPONENT;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.SHADING_INTENSITY;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.SHADING_RANGE;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.SHADING_TYPE;
import org.visnow.vn.geometries.parameters.VolumeShadingParams.ShadingType;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.Params;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.ShadingType.COMPONENT_GRADIENT;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.ShadingType.TRANSPARENCY_GRADIENT;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.utils.numeric.FiniteDifferences.Derivatives;
import org.visnow.vn.system.utils.usermessage.Level;
import static org.visnow.vn.geometries.parameters.VolumeShadingParams.ShadingType.NO_SHADING;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public final class TextureVolumeRenderer extends OpenBranchGroup
{

    protected static final Logger LOGGER = Logger.getLogger(TextureVolumeRenderer.class);
    
    protected DataMappingParams dataMappingParams = null;
    protected VolumeShadingParams volumeShadingParams = null;
    protected boolean needPow2Textures;
    protected RegularField inField;
    
    protected DataArray transparancyDataArray = null;
    protected DataArray colorDataArray        = null;
    protected DataArray shadingDataArray      = null;
    
    protected String lastTransparencyDataName = null;
    protected String lastColorDataName        = null;
    protected String lastShadingDataName      = null;
    
    protected QuadArray[][] volMeshes = new QuadArray[3][2];
    protected int n2, n1, n0; //real data array dims
    protected int nn0, nn1, nn2; //data array dims scaled up to power of 2
    protected int nvert = 0;
    protected int[] dims = null;
    protected float[] coords = null;
    protected float[] texCoords = null;
    protected int[] trMap = new int[256];
    
    protected OpenShape3D tr = null;
    protected ImageComponent3D i3d = null;
    protected Texture3D tx3d = null;
    protected TransparencyAttributes ta = null;
    protected OpenAppearance ap = new OpenAppearance();
    
    protected int axis;
    protected boolean dir;
    protected int minX;
    protected Params params = null;
    
    protected int minY, minZ, maxX, maxY, maxZ;
    protected TransformGroup tg = null;
    protected TransformGroup normalVectors = null;
    protected BufferedImage[] texImages;
    protected int[][] shadedSlices;
    protected int nThreads = VisNow.availableProcessors();
    protected int[] textureImageData = null;
    protected Runtime runtime = Runtime.getRuntime();
    protected long used = (runtime.totalMemory() - runtime.freeMemory()) / 1024;
    protected boolean textureFromUV = false;
    
    protected ShadingType shadingType = ShadingType.NO_SHADING;
    protected float[] invJacobian = new float[9];
    protected float[] gradient = null;
    protected float avgGradientNorm = 1;
    protected float[] stDgradientRange = new float[] {.1f, 2};
    protected float[] gradientRange = new float[2];
    protected float shadingIntensity = .3f;
    protected int[] transparency = null;
    protected float[] lightDir = new float[] {1,0,0};
    
    protected RenderEventListener dataMappingListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            if ((e.getUpdateExtent() & RenderEvent.COLORS) != 0)
                updateTextureColors();
            if ((e.getUpdateExtent() & RenderEvent.TEXTURE) != 0)
                updateTextureFromUV();
            if ((e.getUpdateExtent() & RenderEvent.TRANSPARENCY) != 0)
                updateTextureTransparency();
        }
    };
        
    protected ParameterChangeListener volumeShadingParamsListener = new ParameterChangeListener()
    {
        @Override
        public void parameterChanged(String name)
        {
            switch (name) {
                case SHADING_TYPE:
                    setShadingType();
                    updateTexture();
                    break;
                case SHADING_COMPONENT:
                    computeComponentGradient();
                    updateTexture();
                    break;
                case SHADING_RANGE:
                    setGradientRange();
                    break;
                case SHADING_INTENSITY:
                    setShadingIntensity();
                    break;
            }
        }
    };
   
 /**
 * Basic constructor initializinggeometry features
 * @param needPow2Textures    old versions of Java3D require texture dimensions to be powers of 2
 * if needPow2Textures is true, each texture dimension is increased to the nearest power of 2
 */
    public TextureVolumeRenderer(boolean needPow2Textures)
    {
        this.needPow2Textures = needPow2Textures;
        this.setCapability(BranchGroup.ALLOW_LOCAL_TO_VWORLD_READ);
        this.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        this.setCapability(BranchGroup.ALLOW_DETACH);
        for (int i = 0; i < trMap.length; i++)
            trMap[i] = (byte) (i / 3);
        tr = new OpenShape3D();
        OpenMaterial mat = new OpenMaterial(new Color3f(0.7f, 0.7f, 0.7f),
                                            new Color3f(0.1f, 0.1f, 0.1f),
                                            new Color3f(1.0f, 1.0f, 1.0f),
                                            new Color3f(0.7f, 0.7f, 0.7f),
                                            .5f, OpenMaterial.AMBIENT_AND_DIFFUSE);
        ap.setCapability(OpenAppearance.ALLOW_TEXTURE_WRITE);
        ta = new TransparencyAttributes();
        ta.setTransparencyMode(TransparencyAttributes.BLENDED);
        ta.setCapability(TransparencyAttributes.ALLOW_VALUE_READ);
        ta.setCapability(TransparencyAttributes.ALLOW_VALUE_WRITE);
        ta.setTransparency(0.5f);
        ap.setTransparencyAttributes(ta);
        ap.setTexture(tx3d);
        ap.setMaterial(mat);
        tr.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        tr.setAppearance(ap);
    }    
    
    
 /**
 * 
 * @param dataMappingParams   DataMappingParams object specifying color map and transparency map
 * @param volumeShadingParams VolumeShadingParams object specifying component or transparency dependent shading
 * @param needPow2Textures    old versions of Java3D require texture dimensions to be powers of 2
 * if needPow2Textures is true, each texture dimension is increased to the nearest power of 2
 */
    public TextureVolumeRenderer(DataMappingParams dataMappingParams, 
                                 VolumeShadingParams volumeShadingParams, boolean needPow2Textures)
    {
        this(needPow2Textures);
        setParams(dataMappingParams, volumeShadingParams);
    }
/**
 * 
 * @param inField             field to be mapped
 * @param dataMappingParams   DataMappingParams object specifying color map and transparency map
 * @param volumeShadingParams VolumeShadingParams object specifying component or transparency dependent shading
 * @param needPow2Textures    old versions of Java3D require texture dimensions to be powers of 2
 * if needPow2Textures is true, each texture dimension is increased to the nearest power of 2
 */
    public TextureVolumeRenderer(RegularField inField, DataMappingParams dataMappingParams, 
                                 VolumeShadingParams volumeShadingParams, boolean needPow2Textures)
    {
        this(dataMappingParams, volumeShadingParams, needPow2Textures);
        setInfield(inField);
    }
    
    protected void setParams(DataMappingParams dataMappingParams, VolumeShadingParams volumeShadingParams)
    {
        this.dataMappingParams = dataMappingParams;
        this.dataMappingParams.addRenderEventListener(dataMappingListener);
        this.volumeShadingParams = volumeShadingParams;
        this.volumeShadingParams.addParameterChangelistener(volumeShadingParamsListener);
    }

   protected void setInfield(RegularField inField)
    {
        this.inField = inField;

        dims = inField.getDims();
        if (3 * inField.getNNodes() > LargeArray.getMaxSizeOf32bitArray()) {
            int[] down = new int[]{1, 1, 1};
            boolean downsample = false;
            for (int i = 0; i < dims.length; i++) {
                if (dims[i] > 600) {
                    down[i] = (int) Math.ceil(dims[i] / 600.);
                    if (down[i] > 1) {
                        downsample = true;
                    }
                }
            }
            if (downsample) {
                this.inField = inField.downsample(down);
                dims = this.inField.getDims();
            }
        } else {
            this.inField = inField;
        }
        if (3 * this.inField.getNNodes() <= LargeArray.getMaxSizeOf32bitArray()) {
            gradient = new float[3 * (int)this.inField.getNNodes()];
            transparency = new int[(int)this.inField.getNNodes()];
            float[][] iA = this.inField.getInvAffine();
            for (int i = 0, k = 0; i < 3; i++)
                for (int j = 0; j < 3; j++, k++)
                    invJacobian[k] = iA[i][j];
        }
        else {
            throw new IllegalArgumentException("Input field is too large.");
        }
        int i, k;
        nn2 = n2 = dims[2];
        nn1 = n1 = dims[1];
        nn0 = n0 = dims[0];
        if (needPow2Textures) {
            nn0 = nn1 = nn2 = 0;
            for (i = 0, k = 1; i < 20; i++, k *= 2) {
                if (k >= n0 && k < 2 * n0)
                    nn0 = k;
                if (k >= n1 && k < 2 * n1)
                    nn1 = k;
                if (k >= n2 && k < 2 * n2)
                    nn2 = k;
            }
        }
        minX = minY = minZ = 0;
        maxX = n0 - 1;
        maxY = n1 - 1;
        maxZ = n2 - 1;

        //updateMesh();
        float[][] sourceAffine = this.inField.getAffine();
        float[] matrix = new float[16];
        for (int l = 0; l < 3; l++) {
            for (int j = 0; j < 4; j++) {
                matrix[4 * l + j] = sourceAffine[j][l];
            }
        }
        for (int j = 12; j < matrix.length; j++)
            matrix[j] = 0;
        matrix[15] = 1;

        tg = new TransformGroup(new Transform3D(matrix));
        tg.addChild(tr);
        this.addChild(tg);

        //create normal vectors for flipping planes
        //cross product / norm
        float[] cross12 = VectorMath.vectorNormalize(VectorMath.crossProduct(sourceAffine[0], sourceAffine[1]), false);
        float[] cross23 = VectorMath.vectorNormalize(VectorMath.crossProduct(sourceAffine[1], sourceAffine[2]), false);
        float[] cross31 = VectorMath.vectorNormalize(VectorMath.crossProduct(sourceAffine[2], sourceAffine[0]), false);
        float[] dirs = new float[3];
        Arrays.fill(dirs, 0);
        for (int j = 0; j < 3; j++) {
            dirs[2] += cross12[j] * sourceAffine[2][j];
            dirs[1] += cross31[j] * sourceAffine[1][j];
            dirs[0] += cross23[j] * sourceAffine[0][j];
        }
        for (int j = 0; j < 3; j++)
            dirs[j] = (float) Math.signum(dirs[j]);

        float[] normalMatrix = new float[16];

        for (int j = 0; j < cross12.length; j++) {
            normalMatrix[4 * j] = dirs[0] * cross23[j];
            normalMatrix[4 * j + 1] = dirs[1] * cross31[j];
            normalMatrix[4 * j + 2] = dirs[2] * cross12[j];
            normalMatrix[4 * j + 3] = sourceAffine[3][j];
        }
        for (int j = 12; j < normalMatrix.length; j++)
            normalMatrix[j] = 0;
        normalMatrix[15] = 1;

        normalVectors = new TransformGroup(new Transform3D(normalMatrix));
        
    }

    protected void computeTransparencyGradient()
    {
        Derivatives.computeLargeDerivatives(nThreads, dims, new IntLargeArray(transparency), 
                                                            new FloatLargeArray(invJacobian), 
                                                            new FloatLargeArray(gradient));
        normalizeGradient();
    }
    
    public void computeComponentGradient()
    {
        DataArray inDa = inField.getComponent(volumeShadingParams.getShadingComponent());
        if (inDa == null)
            return;
        FloatLargeArray inData;
        if (inDa.getVectorLength() == 1)
            inData = inDa.getRawFloatArray();
        else
            inData = inDa.getVectorNorms();
        Derivatives.computeLargeDerivatives(nThreads, dims, inData, new FloatLargeArray(invJacobian), new FloatLargeArray(gradient));
        normalizeGradient();
    }
    
    protected void normalizeGradient()
    {
        double gN = 0;
        for (int i = 0; i < gradient.length; ) 
            for (int j = 0; j < 3; j++, i++) 
                gN += gradient[i] * gradient[i];
        
        if (gN == 0)
            avgGradientNorm = 1;
        else
            avgGradientNorm = (float)Math.sqrt(3 * gN / gradient.length);
    }
    
    public void updateShading()
    {
        if (volumeShadingParams == null || dataMappingParams == null ||
            volumeShadingParams.getShadingType() == NO_SHADING ||
            gradient == null)
            return;
        if (volumeShadingParams.getShadingType() == COMPONENT_GRADIENT) 
            computeComponentGradient();
        else if (volumeShadingParams.getShadingType() == TRANSPARENCY_GRADIENT)
            computeTransparencyGradient();
        updateTexture();
    }

    protected void createMeshes()
    {
        int i0, i1, i2, k;
        float[][] ptsc = new float[2][3];
        float x, y, z, tx, ty, tz;

        ptsc[0][0] = minX;
        ptsc[1][0] = maxX;
        ptsc[0][1] = minY;
        ptsc[1][1] = maxY;
        ptsc[0][2] = minZ;
        ptsc[1][2] = maxZ;
        float[][] tsc = new float[2][3];
        tsc[0][0] = minX / (nn0 - 1.f);
        tsc[1][0] = maxX / (nn0 - 1.f);
        tsc[0][1] = (minY + nn1 - n1) / (nn1 - 1.f);
        tsc[1][1] = (maxY + nn1 - n1) / (nn1 - 1.f);
        tsc[0][2] = minZ / (nn2 - 1.f);
        tsc[1][2] = maxZ / (nn2 - 1.f);

        nvert = 4 * (maxZ - minZ + 1);
        volMeshes[2][0] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        volMeshes[2][1] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        coords = new float[3 * nvert];
        texCoords = new float[3 * nvert];
        float[] tc;
        for (i2 = minZ, k = 0; i2 <= maxZ; i2++, k += 4) {
            z = i2;
            System.arraycopy(new float[] {ptsc[0][0], ptsc[0][1], z,
                                          ptsc[1][0], ptsc[0][1], z,
                                          ptsc[1][0], ptsc[1][1], z,
                                          ptsc[0][0], ptsc[1][1], z}, 0, 
                             coords, 3 * k, 12);
            tz = i2 / (nn2 - 1.f);
            System.arraycopy(new float[] {tsc[0][0], tsc[0][1], tz,
                                          tsc[1][0], tsc[0][1], tz,
                                          tsc[1][0], tsc[1][1], tz,
                                          tsc[0][0], tsc[1][1], tz}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[2][0].setCoordinates(0, coords);
        volMeshes[2][0].setTextureCoordinates(0, 0, texCoords);
        volMeshes[2][0].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[2][0].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
        for (i2 = maxZ, k = 0; i2 >= minZ; i2--, k += 4) {
            z = i2;
            System.arraycopy(new float[] {ptsc[0][0], ptsc[0][1], z,
                                          ptsc[1][0], ptsc[0][1], z,
                                          ptsc[1][0], ptsc[1][1], z,
                                          ptsc[0][0], ptsc[1][1], z}, 0, 
                             coords, 3 * k, 12);
            tz = i2 / (nn2 - 1.f);
            System.arraycopy(new float[] {tsc[0][0], tsc[0][1], tz,
                                          tsc[1][0], tsc[0][1], tz,
                                          tsc[1][0], tsc[1][1], tz,
                                          tsc[0][0], tsc[1][1], tz}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[2][1].setCoordinates(0, coords);
        volMeshes[2][1].setTextureCoordinates(0, 0, texCoords);
        volMeshes[2][1].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[2][1].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);

        nvert = 4 * (maxY - minY + 1);
        volMeshes[1][0] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        volMeshes[1][1] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        coords = new float[3 * nvert];
        texCoords = new float[3 * nvert];
        for (i1 = minY, k = 0; i1 <= maxY; i1++, k += 4) {
            y = i1;
            System.arraycopy(new float[] {ptsc[0][0], y, ptsc[0][2],
                                          ptsc[1][0], y, ptsc[0][2],
                                          ptsc[1][0], y, ptsc[1][2],
                                          ptsc[0][0], y, ptsc[1][2]}, 0, 
                             coords, 3 * k, 12);
            ty = (i1 + nn1 - n1) / (nn1 - 1.f);
            System.arraycopy(new float[] {tsc[0][0], ty, tsc[0][2],
                                          tsc[1][0], ty, tsc[0][2],
                                          tsc[1][0], ty, tsc[1][2],
                                          tsc[0][0], ty, tsc[1][2]}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[1][0].setCoordinates(0, coords);
        volMeshes[1][0].setTextureCoordinates(0, 0, texCoords);
        volMeshes[1][0].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[1][0].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
        for (i1 = maxY, k = 0; i1 >= minY; i1--, k += 4) {
            y = i1;
            System.arraycopy(new float[] {ptsc[0][0], y, ptsc[0][2],
                                          ptsc[1][0], y, ptsc[0][2],
                                          ptsc[1][0], y, ptsc[1][2],
                                          ptsc[0][0], y, ptsc[1][2]}, 0, 
                             coords, 3 * k, 12);
            ty = (i1 + nn1 - n1) / (nn1 - 1.f);
            System.arraycopy(new float[] {tsc[0][0], ty, tsc[0][2],
                                          tsc[1][0], ty, tsc[0][2],
                                          tsc[1][0], ty, tsc[1][2],
                                          tsc[0][0], ty, tsc[1][2]}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[1][1].setCoordinates(0, coords);
        volMeshes[1][1].setTextureCoordinates(0, 0, texCoords);
        volMeshes[1][1].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[1][1].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);

        nvert = 4 * (maxX - minX + 1);
        volMeshes[0][0] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        volMeshes[0][1] = new QuadArray(nvert, GeometryArray.COORDINATES |
                                        GeometryArray.TEXTURE_COORDINATE_3);
        coords = new float[3 * nvert];
        texCoords = new float[3 * nvert];
        for (i0 = minX, k = 0; i0 <= maxX; i0++, k += 4) {
            x = i0;
            System.arraycopy(new float[] {x, ptsc[0][1], ptsc[0][2],
                                          x, ptsc[1][1], ptsc[0][2],
                                          x, ptsc[1][1], ptsc[1][2],
                                          x, ptsc[0][1], ptsc[1][2]}, 0, 
                             coords, 3 * k, 12);
            tx = i0 / (nn0 - 1.f);
            System.arraycopy(new float[] {tx, tsc[0][1], tsc[0][2],
                                          tx, tsc[1][1], tsc[0][2],
                                          tx, tsc[1][1], tsc[1][2],
                                          tx, tsc[0][1], tsc[1][2]}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[0][0].setCoordinates(0, coords);
        volMeshes[0][0].setTextureCoordinates(0, 0, texCoords);
        volMeshes[0][0].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[0][0].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
        for (i0 = maxX, k = 0; i0 >= minX; i0--, k += 4) {
            x = i0;
            System.arraycopy(new float[] {x, ptsc[0][1], ptsc[0][2],
                                          x, ptsc[1][1], ptsc[0][2],
                                          x, ptsc[1][1], ptsc[1][2],
                                          x, ptsc[0][1], ptsc[1][2]}, 0, 
                             coords, 3 * k, 12);
            tx = i0 / (nn0 - 1.f);
            System.arraycopy(new float[] {tx, tsc[0][1], tsc[0][2],
                                          tx, tsc[1][1], tsc[0][2],
                                          tx, tsc[1][1], tsc[1][2],
                                          tx, tsc[0][1], tsc[1][2]}, 0, 
                             texCoords, 3 * k, 12);
        }
        if (VisNow.getOsType() == VisNow.OsType.OS_MAC)
            fixTexCoords();
        volMeshes[0][1].setCoordinates(0, coords);
        volMeshes[0][1].setTextureCoordinates(0, 0, texCoords);
        volMeshes[0][1].setCapability(GeometryArray.ALLOW_TEXCOORD_READ);
        volMeshes[0][1].setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
    }

    protected void fixTexCoords()
    {
        float FIX = 0.01f;
        if (texCoords == null)
            return;

        for (int i = 0; i < texCoords.length; i++) {
            if (texCoords[i] == 0.0f)
                texCoords[i] += FIX;
            if (texCoords[i] == 1.0f)
                texCoords[i] -= FIX;
        }
    }

    protected void checkTexImages()
    {
        if (texImages == null || texImages.length != nn2 || texImages[0] == null ||
            texImages[0].getWidth() != nn0 || texImages[0].getHeight() != nn1) {
            texImages = new BufferedImage[nn2];
            shadedSlices = new int[nn2][];
            for (int i = 0; i < texImages.length; i++) {
                texImages[i] = new BufferedImage(nn0, nn1, BufferedImage.TYPE_INT_ARGB);
                shadedSlices[i] = ((DataBufferInt) texImages[i].getRaster().getDataBuffer()).getData();
            }
        }
    }

    protected void updateObjectTexture()
    {
        i3d = new ImageComponent3D(ImageComponent3D.FORMAT_RGBA, texImages, true, true);
        tx3d = new Texture3D(Texture3D.BASE_LEVEL, Texture3D.RGBA, nn0, nn1, nn2);
        tx3d.setCapability(Texture3D.ALLOW_IMAGE_READ);
        tx3d.setCapability(Texture3D.ALLOW_IMAGE_WRITE);
        tx3d.setMagFilter(Texture3D.BASE_LEVEL_LINEAR);
        tx3d.setMinFilter(Texture3D.BASE_LEVEL_LINEAR);
        tx3d.setBoundaryModeS(Texture.CLAMP_TO_EDGE);// Texture.CLAMP_TO_BOUNDARY ??
        tx3d.setBoundaryModeT(Texture.CLAMP_TO_EDGE);
        tx3d.setBoundaryModeR(Texture.CLAMP_TO_EDGE);
        tx3d.setEnable(true);
        tx3d.setImage(0, i3d);
        tr.getAppearance().setTexture(tx3d);
        runtime.gc();
    }

    protected class MapColors implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        byte[] byteColorSlice = null;

        public MapColors(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            byteColorSlice = new byte[4 * nn0 * nn1];
        }

        @Override
        public void run()
        {
            float gl = gradientRange[0];
            float gs = gradientRange[1] - gradientRange[0];
            gs *= gs;
            for (int i = (iThread * nn2) / nThreads; i < ((iThread + 1) * nn2) / nThreads; i++) {
                int[] shadedSlice = shadedSlices[i];
                ColorMapper.map(inField, dataMappingParams, i * nn0 * nn1, (i + 1) * nn0 * nn1, 0, new Color3f(0, 0, 0), byteColorSlice);
                for (int j = 0; j < nn0 * nn1; j++)
                    shadedSlice[j] = (shadedSlice[j] & 0xff000000) |
                        ((byteColorSlice[4 * j] & 0xff) << 16) |
                        ((byteColorSlice[4 * j + 1] & 0xff) << 8) |
                        (byteColorSlice[4 * j + 2] & 0xff);
                if (shadingType != ShadingType.NO_SHADING) {
                    for (int j = 0, k = i * nn0 * nn1; j < nn0 * nn1; j++, k++) {
                        float sp = 0;
                        float gNorm = 0;
                        for (int l = 0; l < 3; l++) {
                            float g = gradient[3 * k + l];
                            sp    += lightDir[l] * g;
                            gNorm += g * g;
                        }
                        gNorm = (float)Math.sqrt(gNorm);
                        float u = gNorm - gl;
                        if (u < 0)
                            u = 0;
                        else 
                            u = u * u / ((gs + u * u) * gNorm);
                        sp = 1 + shadingIntensity * (u * sp - .25f);
                        int r = 0xff & (shadedSlice[j] >> 16);
                        int g = 0xff & (shadedSlice[j] >> 8);
                        int b = 0xff &  shadedSlice[j];
                        if (sp <= 1) {
                            r = Math.max(0, (int)(sp * r));
                            g = Math.max(0, (int)(sp * g));
                            b = Math.max(0, (int)(sp * b));
                        }
                        else {
                            r = 255 - Math.min(255, Math.max(0, (int)((255 - r) * (sp - 1))));
                            g = 255 - Math.min(255, Math.max(0, (int)((255 - g) * (sp - 1))));
                            b = 255 - Math.min(255, Math.max(0, (int)((255 - b) * (sp - 1))));
                        }
                        shadedSlice[j] = (shadedSlice[j] & 0xff000000) |
                                         ((r & 0xff) << 16) |
                                         ((g & 0xff) << 8) |
                                          (b & 0xff);
                    }
                }
                texImages[i].setRGB(0, 0, nn0, nn1, shadedSlices[i], 0, nn0);
            }
        }
    }

    public void updateTextureColors()
    {
        gradientRange[0] = avgGradientNorm * stDgradientRange[0];
        gradientRange[1] = avgGradientNorm * stDgradientRange[1];
        textureFromUV = false;
        checkTexImages();
        ConcurrencyUtils.setNumberOfThreads(nThreads);
        Future<?>[] futures = new Future<?>[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            futures[iThread] = ConcurrencyUtils.submit(new MapColors(nThreads, iThread));
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
        }
        updateObjectTexture();
    }
    
    

    protected class MapTransparency implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;

        public MapTransparency(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
        }
        
        protected void mapTransparency(int start, int[] colors)
        {
            if (colors == null || colors.length != nn0 * nn1)
                colors = new int[nn0 * nn1];
            for (int i = 0; i < colors.length; i++)
                colors[i] &= 0xffffff;
            ComponentSubrange cr = dataMappingParams.getTransparencyParams().getComponentRange();
            DataArray trData = inField.getComponent(cr.getComponentName());
            LogicLargeArray valid = inField.getCurrentMask();
            boolean isValidity = inField.hasMask();
            if (trData == null)
                for (int i = 0; i < colors.length; i++)
                    colors[i] |= 0xff << 24;
            else {
                int[] transp = dataMappingParams.getTransparencyParams().getMap();
                float low = cr.getLow();
                float up = cr.getUp();
                if (up <= low)
                    up = low + .01f;
                float d = 255 / (up - low);
                if (trData.getVectorLength() == 1)
                    switch (trData.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] bData = (byte[]) trData.getRawArray().getData();
                            for (int i = 0, l = start, m = 0; i < n1; i++, m += nn0 - n0)
                                for (int j = 0; j < n0; j++, l++, m++) {
                                    if (isValidity && !valid.getBoolean(l))
                                        continue;
                                    int k = Math.min(255, Math.max(0, (int)(d * ((0xff & bData[l]) - low))));
                                    colors[m] |= (0xff & transp[k]) << 24;
                                    transparency[l] = transp[k];
                                }
                            break;
                        case FIELD_DATA_SHORT:
                        case FIELD_DATA_INT:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            LargeArray dData = trData.getRawArray();
                            for (int i = 0, l = start, m = 0; i < n1; i++, m += nn0 - n0)
                                for (int j = 0; j < n0; j++, l++, m++) {
                                    if (isValidity && !valid.getBoolean(l))
                                        continue;
                                    int k = (int) (d * (dData.getDouble(l) - low));
                                    if (k < 0)
                                        k = 0;
                                    if (k > 255)
                                        k = 255;
                                    colors[m] |= (0xff & transp[k]) << 24;
                                    transparency[l] = transp[k];
                                }
                            break;
                    }
                else {
                    double v;
                    d = 255 / up;
                    int vl = trData.getVectorLength();
                    switch (trData.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] bData = (byte[]) trData.getRawArray().getData();
                            for (int i = 0, l = start, m = 0; i < n1; i++, m += nn0 - n0)
                                for (int j = 0; j < n0; j++, l++, m++) {
                                    if (isValidity && !valid.getBoolean(l))
                                        continue;
                                    v = 0;
                                    for (int p = 0, k = vl * l; p < vl; p++, k++)
                                        v += (0xff & bData[k]) * (0xff & bData[k]);
                                    int k = (int) (d * sqrt(v));
                                    if (k < 0)
                                        k = 0;
                                    if (k > 255)
                                        k = 255;
                                    colors[m] |= (0xff & transp[k]) << 24;
                                    transparency[l] = transp[k];
                                }
                            break;
                        case FIELD_DATA_SHORT:
                        case FIELD_DATA_INT:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            LargeArray dData = trData.getRawArray();
                            for (int i = 0, l = start, m = 0; i < n1; i++, m += nn0 - n0)
                                for (int j = 0; j < n0; j++, l++, m++) {
                                    if (isValidity && !valid.getBoolean(l))
                                        continue;
                                    v = 0;
                                    for (int p = 0, k = vl * l; p < vl; p++, k++)
                                        v += dData.getDouble(k) * dData.getDouble(k);
                                    int k = (int) (d * sqrt(v));
                                    if (k < 0)
                                        k = 0;
                                    if (k > 255)
                                        k = 255;
                                    colors[m] |= (0xff & transp[k]) << 24;
                                    transparency[l] = transp[k];
                                }
                            break;
                    }
                }
            }
        }


        @Override
        public void run()
        {
            int dk = nn2 / nThreads;
            int kstart = iThread * dk + min(iThread, nn2 % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nn2 % nThreads);
            if (kend > nn2)
                kend = nn2;
            for (int i = kstart; i < kend; i++) {
                mapTransparency(i * nn0 * nn1, shadedSlices[i]);
                texImages[i].setRGB(0, 0, nn0, nn1, shadedSlices[i], 0, nn0);
            }

        }
    }

    public void updateTextureTransparency()
    {
        checkTexImages();
        ConcurrencyUtils.setNumberOfThreads(nThreads);
        Future<?>[] futures = new Future<?>[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            futures[iThread] = ConcurrencyUtils.submit(new MapTransparency(nThreads, iThread));
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            VisNow.get().userMessageSend(null, "Error while volume rendering", "See log for details.", Level.ERROR);
            LOGGER.debug("Error while volume rendering", ex);
        }
        if (volumeShadingParams.getShadingType() == TRANSPARENCY_GRADIENT)
        {
            computeTransparencyGradient();
            if (textureFromUV)
                updateTextureFromUV();
            else
                updateTextureColors();
        }
        updateObjectTexture();
    }


    protected class MapTextureFromUV implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        byte[] byteColorSlice = null;

        public MapTextureFromUV(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            float gl = gradientRange[0];
            float gs = gradientRange[1] - gradientRange[0];
            gs *= gs;
            float sStart = Math.max(.75f, 1 - shadingIntensity);
            int dk = nn2 / nThreads;
            int kstart = iThread * dk + min(iThread, nn2 % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nn2 % nThreads);
            if (kend > nn2)
                kend = nn2;
            for (int i = kstart; i < kend; i++) {
                int[] shadedSlice = shadedSlices[i];
                TextureMapper.map(inField, dataMappingParams, textureImageData,
                                  i * nn0 * nn1, (i + 1) * nn0 * nn1, shadedSlice);
                if (shadingType != ShadingType.NO_SHADING) {
                    for (int j = 0, k = i * nn0 * nn1; j < nn0 * nn1; j++, k++) {
                        float sp = 0;
                        float gNorm = 0;
                        for (int l = 0; l < 3; l++) {
                            float g = gradient[3 * k + l];
                            sp    += lightDir[l] * g;
                            gNorm += g * g;
                        }
                        gNorm = (float)Math.sqrt(gNorm);
                        float u = gNorm - gl;
                        if (u < 0)
                            u = 0;
                        else 
                            u = u * u / ((gs + u * u) * gNorm);
                        sp = sStart + shadingIntensity * u * sp;
                        int r = 0xff & (shadedSlice[j] >> 16);
                        r = Math.max(0, Math.min(255, (int)(sp * r)));
                        int g = 0xff & (shadedSlice[j] >> 8);
                        g = Math.max(0, Math.min(255, (int)(sp * g)));
                        int b = 0xff &  shadedSlice[j];
                        b = Math.max(0, Math.min(255, (int)(sp * b)));
                        shadedSlice[j] = (shadedSlice[j] & 0xff000000) |
                                         ((r & 0xff) << 16) |
                                         ((g & 0xff) << 8) |
                                          (b & 0xff);
                    }
                }
                texImages[i].setRGB(0, 0, nn0, nn1, shadedSlice, 0, nn0);
            }
        }
    }

    public void updateTextureFromUV()
    {
        if (dataMappingParams.getTextureImage() == null ||
            inField.getComponent(dataMappingParams.getUParams().getDataComponentIndex()) == null ||
            inField.getComponent(dataMappingParams.getVParams().getDataComponentIndex()) == null)
            return;
        textureFromUV = false;
        BufferedImage textureImage = dataMappingParams.getTextureImage();
        textureImageData = textureImage.getRGB(0, 0,
                                               textureImage.getWidth(), textureImage.getHeight(),
                                               null, 0, textureImage.getWidth());
        checkTexImages();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new MapTextureFromUV(nThreads, iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads) {
            try {
                workThread.join();
            }catch (Exception e) {
            }
        }
        updateObjectTexture();
    }
    
    public void updateTexture()
    {
        checkTexImages();
        if (textureFromUV)
            updateTextureFromUV();
        else
            updateTextureColors();
        updateTextureTransparency();
        updateObjectTexture();
    }

    public int getDir()
    {
        return axis;
    }

    public void setDir(int axis, boolean dir)
    {
        this.axis = axis;
        this.dir = dir;
        try {
            if (dir)
                tr.setGeometry(volMeshes[axis][1]);
            else
                tr.setGeometry(volMeshes[axis][0]);
        } catch (Exception e) {
        }
    }

    public void updateMesh()
    {
        createMeshes();
        if (dir) {
            tr.setGeometry(volMeshes[axis][1]);
        } else {
            tr.setGeometry(volMeshes[axis][0]);
        }
    }

    public void setCrop(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
    {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.minZ = minZ;
        this.maxZ = maxZ;
        createMeshes();
        if (dir) {
            tr.setGeometry(volMeshes[axis][1]);
        } else {
            tr.setGeometry(volMeshes[axis][0]);
        }
    }

    public void setCrop(int[] low, int[] up)
    {
        this.minX = low[0];
        this.maxX = up[0];
        this.minY = low[1];
        this.maxY = up[1];
        this.minZ = low[2];
        this.maxZ = up[2];
        createMeshes();
        if (dir) {
            tr.setGeometry(volMeshes[axis][1]);
        } else {
            tr.setGeometry(volMeshes[axis][0]);
        }
    }

    public void setShadingType()
    {
        if (volumeShadingParams.getShadingType() ==  shadingType)
            return;
        shadingType = volumeShadingParams.getShadingType();
        if (shadingType == COMPONENT_GRADIENT)
            computeComponentGradient();
        else if (shadingType == TRANSPARENCY_GRADIENT)
            computeTransparencyGradient();
        checkTexImages();
        if (textureFromUV)
            updateTextureFromUV();
        else
            updateTextureColors();
        updateObjectTexture();
    }
    
    public void setLightDir(float[] lightDir)
    {
        this.lightDir = lightDir;
        if (volumeShadingParams.getShadingType() != NO_SHADING)
        {
            checkTexImages();
            if (textureFromUV)
                updateTextureFromUV();
            else
                updateTextureColors();
        }
        updateObjectTexture();
    }
    
/**
 * Setter of gradient range from volumeShadingParams:
 * gradientRange 2-element float array. gradients below gradientRange[0] are ignored
 * and gradients above gradientRange[1] provide full shading
 */
    public void setGradientRange() {
        gradientRange = volumeShadingParams.getShadingRange();
        if (volumeShadingParams.getShadingType() != NO_SHADING)
        {
            checkTexImages();
            if (textureFromUV)
                updateTextureFromUV();
            else
                updateTextureColors();
        }
        updateTexture();
    }
    

    /**
     * Setter of shadingIntensity from volumeShadingParams:
     * shadingScale a float from 0:1 range controlling shading intensity
     */
    public void setShadingIntensity()
    {
        this.shadingIntensity = volumeShadingParams.getShadingIntensity();
        if (volumeShadingParams.getShadingType() != NO_SHADING)
        {
            checkTexImages();
            if (textureFromUV)
                updateTextureFromUV();
            else
                updateTextureColors();
        }
        updateTexture();
    }
/**
 * Currently not used - may be useful for volume rendering of large datasets
 */
    protected transient FloatValueModificationListener statusListener = null;

/**
 * Removes current status notification listener
 */
    public void clearStatusListener()
    {
        statusListener = null;
    }
/**
 * Setter for status listener
 * @param listener a FloatValueModificationListener used to indicate operation progress
 */
    public void addStatusListener(FloatValueModificationListener listener)
    {
        if (statusListener == null) {
            this.statusListener = listener;
        } else {
            System.out.println("" + this + ": only one status listener can be added");
        }
    }

    protected void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null) {
            statusListener.floatValueChanged(e);
        }
    }

    public DataMappingParams getDataMappingParams() {
        return dataMappingParams;
    }

    
    public TransformGroup getTg()
    {
        return tg;
    }

    /**
     * Vectors normal to affine base of the field. Necessary for proper flipping planes for non-orthogonal fields.
     * @return Vectors normal to affine base
     */
    public TransformGroup getNormalVectors()
    {
        return normalVectors;
    }
    
    public void cleanOldListeners()
    {
        if (dataMappingParams != null)
            dataMappingParams.removeRenderEventListener(dataMappingListener);
        if (volumeShadingParams != null)
            volumeShadingParams.removeParameterChangeListener(volumeShadingParamsListener);
    }

}
