/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import javax.swing.JComponent;

/**
 *
 * A wrapper for a Swing component for hardcopy creation.
 * To print a component cmp, add a piece of code like this:
 * <p>
 * <CODE>      PrinterJob pj = PrinterJob.getPrinterJob();
 * <p>
 * PageFormat mPageFormat = pj.defaultPage();
 * <p>
 * mPageFormat = pj.pageDialog(mPageFormat);
 * <p>
 * ComponentPrintable cPr = new ComponentPrintable(cmp);
 * <p>
 * pj.setPrintable(cPr,mPageFormat);
 * <p>
 * if (pj.printDialog())
 * <p>
 * {
 * <p>
 * try
 * <p>
 * {
 * <p>
 * pj.print();
 * <p>
 * }
 * <p>
 * catch (PrinterException e)
 * <p>
 * {
 * <p>
 * JOptionPane.showMessageDialog(null,"cannot print window");
 * <p>
 * }
 * <p>
 * }
 * <p>
 * </CODE>
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ComponentPrintable implements Printable
{

    private int width, height;
    private Component mComponent;

    /**
     * Creates a new ComponentPrintable for a Component <arg>c</arg>
     */
    public ComponentPrintable(Component c)
    {
        mComponent = c;
        height = c.getSize().height;
        width = c.getSize().width;
    }

    /**
     * Prints the Component indicated in the constructor.
     * Do not call it explicitely - it is called by the corresponding Printer Job.
     * <p>
     * @return printed page status
     */
    public int print(Graphics g, PageFormat pageFormat, int pagelndex)
    {
        double s;
        s = 9. * 72. / width;
        if (s > 6.5 * 72. / height)
            s = 6.5 * 72. / height;
        if (pagelndex > 0)
            return NO_SUCH_PAGE;
        Graphics2D g2 = (Graphics2D) g;
        g2.translate(pageFormat.getImageableX(),
                     pageFormat.getImageableY());
        g2.scale(s, s);
        boolean wasBuffered = disableDoubleBuffering(mComponent);
        mComponent.paint(g2);
        restoreDoubleBuffering(mComponent, wasBuffered);
        return PAGE_EXISTS;
    }

    private boolean disableDoubleBuffering(Component c)
    {
        if (c instanceof JComponent == false)
            return false;
        JComponent jc = (JComponent) c;
        boolean wasBuffered = jc.isDoubleBuffered();
        jc.setDoubleBuffered(false);
        return wasBuffered;
    }

    private void restoreDoubleBuffering(Component c,
                                        boolean wasBuffered)
    {
        if (c instanceof JComponent)
            ((JComponent) c).setDoubleBuffered(wasBuffered);
    }
}
