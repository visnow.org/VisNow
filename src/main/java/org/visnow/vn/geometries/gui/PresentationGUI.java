/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.Vector;
import javax.swing.JFileChooser;
import org.visnow.vn.geometries.objects.SignalingTransform3D;
import javax.swing.JTree;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import org.apache.log4j.Logger;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.lib.basic.readers.GaussianCubeReader.GaussianCubeReader;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class PresentationGUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(PresentationGUI.class);

    protected PresentationParams params = null;
    protected PresentationParams currentParams = null;
    protected DefaultMutableTreeNode top = null;
    protected String lastPath = null;

    /**
     * Creates new form ColoringGUI
     */
    public PresentationGUI()
    {
        initComponents();
        if (!VisNow.isDebug()) loadSavePanel.setVisible(false);
        FileNameExtensionFilter dataFilter = new FileNameExtensionFilter("presentation params files", "prp", "PRP");
        paramsFileChooser.setFileFilter(dataFilter);
    }

    private void setChildrenParams(DefaultMutableTreeNode node, PresentationParams params)
    {
        if (params.getChildrenParams() != null &&
                !params.getChildrenParams().isEmpty())
            for (int i = 0; i < params.getChildrenParams().size(); i++) {
                PresentationParams childParams = params.getChild(i);
                DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(childParams);
                setChildrenParams(childNode, childParams);
                node.add(childNode);
            }
    }

    public void setPresentationParams(PresentationParams params)
    {
        if (params == null || this.params == params) {
            updateParams();
            return;
        }
        this.params = params;
        this.params.setGUI(this);
        updateParams();
    }

    public void updateParams()
    {
        if(params == null)
            return;
        boolean oldActive = params.isActive();
        params.setActive(false);
        updateDisplayTree();
        if (params.getChildrenParams() != null && params.getChildrenParams().size() == 1) {
            boolean oldActiveChild = params.getChild(0).isActive();
            params.getChild(0).setActive(false);
            setCurrentParams(params.getChild(0));
            params.getChild(0).setActive(oldActiveChild);
        }
        else
            setCurrentParams(params);
        inheritMapBox.setVisible(params.getParentParams() != null &&
                params.getParentParams().getChildrenParams().size() > 1);
        inheritRenderingBox.setVisible(params.getParentParams() != null &&
                params.getParentParams().getChildrenParams().size() > 1);
        params.setActive(oldActive);
    }

    private void setCurrentParams(PresentationParams params)
    {
        currentParams = params;
        currentParams.setGUI(this);
        inheritMapBox.setVisible(currentParams.getParentParams() != null);
        inheritMapBox.setSelected(currentParams.isMappingInherited());
        inheritRenderingBox.setVisible(currentParams.getParentParams() != null);
        inheritRenderingBox.setSelected(currentParams.isRenderingInherited());
        dataMappingGUI.setParams(currentParams.getDataMappingParams());
        dataMappingGUI.updateDataValuesFromParams();
        renderingGUI.setRenderingParams(currentParams.getRenderingParams());
        renderingGUI.setRegularField3D(currentParams.isContent3DEnabled());
        renderingGUI.setContent3DParams(currentParams.getContent3DParams());
        renderingGUI.updateDataValuesFromParams();
        transformGUI.setTransformParams(currentParams.getTransformParams());
    }

    public DataMappingGUI getDataMappingGUI()
    {
        return dataMappingGUI;
    }

    public RenderingGUI getRenderingGUI()
    {
        return renderingGUI;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        textureFileChooser = new javax.swing.JFileChooser();
        paramsFileChooser = new javax.swing.JFileChooser();
        cellSetPanel = new javax.swing.JScrollPane();
        paramTree = new javax.swing.JTree();
        presentationPanel = new javax.swing.JPanel();
        extendedTabbedPane = new javax.swing.JTabbedPane();
        dataMappingScrollPane = new javax.swing.JScrollPane();
        dataMappingPanel = new javax.swing.JPanel();
        dataMappingGUI = new org.visnow.vn.geometries.gui.DataMappingGUI();
        jPanel1 = new javax.swing.JPanel();
        inheritMapBox = new javax.swing.JCheckBox();
        renderingScrollPane = new javax.swing.JScrollPane();
        renderingPanel = new javax.swing.JPanel();
        renderingGUI = new org.visnow.vn.geometries.gui.RenderingGUI();
        jPanel2 = new javax.swing.JPanel();
        inheritRenderingBox = new javax.swing.JCheckBox();
        transformScrollPane = new javax.swing.JScrollPane();
        transformPanel = new javax.swing.JPanel();
        transformGUI = new org.visnow.vn.geometries.gui.TransformPanel();
        loadSavePanel = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        loadButton = new javax.swing.JButton();

        setRequestFocusEnabled(false);
        setLayout(new java.awt.GridBagLayout());

        cellSetPanel.setMaximumSize(new java.awt.Dimension(32767, 200));
        cellSetPanel.setMinimumSize(new java.awt.Dimension(200, 83));
        cellSetPanel.setPreferredSize(new java.awt.Dimension(203, 123));

        paramTree.setBackground(new java.awt.Color(153, 204, 255));
        paramTree.setMaximumSize(new java.awt.Dimension(280, 102));
        paramTree.setMinimumSize(new java.awt.Dimension(180, 72));
        paramTree.setOpaque(false);
        paramTree.setPreferredSize(new java.awt.Dimension(200, 90));
        paramTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener()
        {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt)
            {
                paramTreeValueChanged(evt);
            }
        });
        cellSetPanel.setViewportView(paramTree);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(cellSetPanel, gridBagConstraints);

        presentationPanel.setLayout(new java.awt.CardLayout());

        dataMappingScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        dataMappingPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataMappingPanel.add(dataMappingGUI, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.5;
        dataMappingPanel.add(jPanel1, gridBagConstraints);

        inheritMapBox.setText("inheriting data map");
        inheritMapBox.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inheritMapBox.setMaximumSize(new java.awt.Dimension(144, 37));
        inheritMapBox.setMinimumSize(new java.awt.Dimension(144, 27));
        inheritMapBox.setPreferredSize(new java.awt.Dimension(144, 37));
        inheritMapBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                inheritMapBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        dataMappingPanel.add(inheritMapBox, gridBagConstraints);

        dataMappingScrollPane.setViewportView(dataMappingPanel);

        extendedTabbedPane.addTab("Datamap", dataMappingScrollPane);

        renderingScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        renderingPanel.setLayout(new java.awt.GridBagLayout());

        renderingGUI.setBackground(new java.awt.Color(238, 238, 237));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        renderingPanel.add(renderingGUI, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        renderingPanel.add(jPanel2, gridBagConstraints);

        inheritRenderingBox.setText("inherit rendering");
        inheritRenderingBox.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inheritRenderingBox.setMargin(new java.awt.Insets(5, 2, 2, 2));
        inheritRenderingBox.setMaximumSize(new java.awt.Dimension(300, 37));
        inheritRenderingBox.setMinimumSize(new java.awt.Dimension(126, 31));
        inheritRenderingBox.setPreferredSize(new java.awt.Dimension(126, 37));
        inheritRenderingBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                inheritRenderingBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        renderingPanel.add(inheritRenderingBox, gridBagConstraints);

        renderingScrollPane.setViewportView(renderingPanel);

        extendedTabbedPane.addTab("Display", renderingScrollPane);

        transformScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        transformPanel.setLayout(new java.awt.BorderLayout());

        transformGUI.setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        transformPanel.add(transformGUI, java.awt.BorderLayout.NORTH);

        transformScrollPane.setViewportView(transformPanel);

        extendedTabbedPane.addTab("Transform", transformScrollPane);

        presentationPanel.add(extendedTabbedPane, "extendedUI");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(presentationPanel, gridBagConstraints);

        loadSavePanel.setLayout(new java.awt.GridLayout(1, 0));

        saveButton.setText("save");
        saveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                saveButtonActionPerformed(evt);
            }
        });
        loadSavePanel.add(saveButton);

        loadButton.setText("load");
        loadButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                loadButtonActionPerformed(evt);
            }
        });
        loadSavePanel.add(loadButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(loadSavePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void inheritRenderingBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_inheritRenderingBoxActionPerformed
    {//GEN-HEADEREND:event_inheritRenderingBoxActionPerformed
        if (inheritRenderingBox.isSelected()) {
            if (currentParams.getParentParams() != null)
                currentParams.getRenderingParams().copyValuesFrom(currentParams.getParentParams().getRenderingParams());
        }
        currentParams.setRenderingInherited(inheritRenderingBox.isSelected());
    }//GEN-LAST:event_inheritRenderingBoxActionPerformed

    private void inheritMapBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_inheritMapBoxActionPerformed
    {//GEN-HEADEREND:event_inheritMapBoxActionPerformed
        if (inheritMapBox.isSelected()) {
            if (currentParams.getParentParams() != null)
                currentParams.getDataMappingParams().copyValuesFrom(currentParams.getParentParams().getDataMappingParams());
        }
        currentParams.setMappingInherited(inheritMapBox.isSelected());
    }//GEN-LAST:event_inheritMapBoxActionPerformed

    public void setInheritRenderingBoxSelection(boolean sel)
    {
        inheritRenderingBox.setSelected(sel);
    }
       
    public void setInheritMapBoxSelection(boolean sel)
    {
        inheritMapBox.setSelected(sel);
    } 
    
    private void paramTreeValueChanged(javax.swing.event.TreeSelectionEvent evt)//GEN-FIRST:event_paramTreeValueChanged
    {//GEN-HEADEREND:event_paramTreeValueChanged
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) paramTree.getLastSelectedPathComponent();
        if (node == null) return;
        PresentationParams selectedParams = (PresentationParams) node.getUserObject();
        setCurrentParams(selectedParams);
    }//GEN-LAST:event_paramTreeValueChanged

    private void loadButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_loadButtonActionPerformed
    {//GEN-HEADEREND:event_loadButtonActionPerformed
        String fileName = null;
        if (lastPath == null)
            paramsFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(GaussianCubeReader.class)));
        else
            paramsFileChooser.setCurrentDirectory(new File(lastPath));

        int returnVal = paramsFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fileName = paramsFileChooser.getSelectedFile().getAbsolutePath();
            lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, GaussianCubeReader.class);
            Vector<String> in = new Vector<>();
            try {
                LineNumberReader r = new LineNumberReader(new FileReader(fileName));
                for (String line = r.readLine(); line != null; line = r.readLine())
                    in.add(line);
            } catch (Exception ex) {
                System.out.println("unable to restore parameters from the requested file");
            }
            String[] r = new String[in.size()];
            for (int i = 0; i < r.length; i++)
                r[i] = in.get(i);
            params.restoreValuesFrom(r);
        }
    }//GEN-LAST:event_loadButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveButtonActionPerformed
    {//GEN-HEADEREND:event_saveButtonActionPerformed
        String fileName = null;
        if (lastPath == null)
            paramsFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(GaussianCubeReader.class)));
        else
            paramsFileChooser.setCurrentDirectory(new File(lastPath));

        int returnVal = paramsFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fileName = paramsFileChooser.getSelectedFile().getAbsolutePath();
            lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, GaussianCubeReader.class);
            try {
                PrintWriter w = new PrintWriter(fileName);
                String[] p = params.valuesToStringArray();
                for (String p1 : p)
                    w.println(p1);
                w.close();
            } catch (FileNotFoundException ex) {
                System.out.println("unable to save parameters to the requested file");
            }
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    protected void updateDisplayTree()
    {
        if (top != null)
            top.removeAllChildren();
        top = new DefaultMutableTreeNode(params);
        setChildrenParams(top, params);
        paramTree = new JTree(top);
        paramTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        paramTree.setSelectionRow(0);
        paramTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener()
        {
            @Override
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt)
            {
                paramTreeValueChanged(evt);
            }
        });
        cellSetPanel.getViewport().setView(paramTree);
        cellSetPanel.setVisible(params.getChildrenParams() != null && params.getChildrenParams().size() > 1);
    }

    public void setSignalingTransform(SignalingTransform3D sigTrans)
    {
        transformGUI.setSigTrans(sigTrans);
    }
    
    public void hideTransformPanel()
    {
        extendedTabbedPane.removeTabAt(2);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane cellSetPanel;
    private org.visnow.vn.geometries.gui.DataMappingGUI dataMappingGUI;
    private javax.swing.JPanel dataMappingPanel;
    private javax.swing.JScrollPane dataMappingScrollPane;
    private javax.swing.JTabbedPane extendedTabbedPane;
    private javax.swing.JCheckBox inheritMapBox;
    private javax.swing.JCheckBox inheritRenderingBox;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton loadButton;
    private javax.swing.JPanel loadSavePanel;
    private javax.swing.JTree paramTree;
    private javax.swing.JFileChooser paramsFileChooser;
    private javax.swing.JPanel presentationPanel;
    private org.visnow.vn.geometries.gui.RenderingGUI renderingGUI;
    private javax.swing.JPanel renderingPanel;
    private javax.swing.JScrollPane renderingScrollPane;
    private javax.swing.JButton saveButton;
    private javax.swing.JFileChooser textureFileChooser;
    private org.visnow.vn.geometries.gui.TransformPanel transformGUI;
    private javax.swing.JPanel transformPanel;
    private javax.swing.JScrollPane transformScrollPane;
    // End of variables declaration//GEN-END:variables

}
