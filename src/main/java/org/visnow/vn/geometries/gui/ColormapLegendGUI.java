/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import java.awt.GridBagLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.visnow.vn.geometries.objects.ColormapLegend;
import org.visnow.vn.geometries.parameters.ColormapLegendParameters;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColormapLegendGUI extends javax.swing.JPanel
{

    protected ColormapLegendParameters params = null;
    protected int direction = ColormapLegend.WEST;
    protected GridBagLayout stdLayout;
    private JComponent[] permanentWidgets, hideableWidgets;

    /**
     * Creates new form ColormapLegendGUI
     */
    public ColormapLegendGUI()
    {
        initComponents();
        lenPosSlider.setBottomValue(.08f);
        widthSlider.setBottomValue(.01f);
        legendPositionButton.setTexts(new String[]{"left", "bottom", "right", "top"});
        permanentWidgets = new JComponent[] {
            colorEditor, edgeLabel, edgePanel, 
            fontLabel, fontPanel, fontSpinner, 
            legendPositionButton
        };

        hideableWidgets = new JComponent[] {
            widthSlider, lenPosSlider
    };
        toggleEnabled(false);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        orientationGroup = new javax.swing.ButtonGroup();
        fontPanel = new javax.swing.JPanel();
        fontLabel = new javax.swing.JLabel();
        fontSpinner = new javax.swing.JSpinner();
        colorEditor = new org.visnow.vn.gui.widgets.ColorEditor();
        edgePanel = new javax.swing.JPanel();
        edgeLabel = new javax.swing.JLabel();
        legendPositionButton = new org.visnow.vn.gui.widgets.MultistateButton();
        widthSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider();
        lenPosSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider();

        setName(""); // NOI18N
        setLayout(new java.awt.GridBagLayout());

        fontPanel.setName("fontPanel"); // NOI18N
        fontPanel.setLayout(new java.awt.GridBagLayout());

        fontLabel.setText("Font:");
        fontLabel.setName("fontLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        fontPanel.add(fontLabel, gridBagConstraints);

        fontSpinner.setModel(new javax.swing.SpinnerNumberModel(16, 10, 40, 2));
        fontSpinner.setToolTipText("<html>label font height<p>(in thousandths of window height)</html>");
        fontSpinner.setName("fontSpinner"); // NOI18N
        fontSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                fontSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        fontPanel.add(fontSpinner, gridBagConstraints);

        colorEditor.setName("colorEditor"); // NOI18N
        colorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                colorEditorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        fontPanel.add(colorEditor, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        add(fontPanel, gridBagConstraints);

        edgePanel.setName("edgePanel"); // NOI18N
        edgePanel.setLayout(new java.awt.GridBagLayout());

        edgeLabel.setText("Window edge:");
        edgeLabel.setName("edgeLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        edgePanel.add(edgeLabel, gridBagConstraints);

        legendPositionButton.setText("multistateButton1");
        legendPositionButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        legendPositionButton.setMaximumSize(new java.awt.Dimension(80, 23));
        legendPositionButton.setMinimumSize(new java.awt.Dimension(50, 23));
        legendPositionButton.setName("legendPositionButton"); // NOI18N
        legendPositionButton.setPreferredSize(new java.awt.Dimension(60, 23));
        legendPositionButton.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                legendPositionButtonStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        edgePanel.add(legendPositionButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(edgePanel, gridBagConstraints);

        widthSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "legend width and distance from window edge", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        widthSlider.setExtent(0.01F);
        widthSlider.setMaximum(0.2F);
        widthSlider.setMinimumSize(new java.awt.Dimension(60, 40));
        widthSlider.setName("widthSlider"); // NOI18N
        widthSlider.setPreferredSize(new java.awt.Dimension(100, 40));
        widthSlider.setTopValue(0.03F);
        widthSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                widthSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(widthSlider, gridBagConstraints);

        lenPosSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "legend length and position along edge", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        lenPosSlider.setExtent(0.1F);
        lenPosSlider.setMaximum(1.0F);
        lenPosSlider.setMinimumSize(new java.awt.Dimension(60, 40));
        lenPosSlider.setName("lenPosSlider"); // NOI18N
        lenPosSlider.setPreferredSize(new java.awt.Dimension(100, 40));
        lenPosSlider.setTopValue(0.5F);
        lenPosSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                lenPosSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(lenPosSlider, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void colorEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_colorEditorStateChanged
    {//GEN-HEADEREND:event_colorEditorStateChanged
        updateParams();
    }//GEN-LAST:event_colorEditorStateChanged

    private void lenPosSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_lenPosSliderStateChanged
    {//GEN-HEADEREND:event_lenPosSliderStateChanged
        updateParams();
    }//GEN-LAST:event_lenPosSliderStateChanged

    private void widthSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_widthSliderStateChanged
        updateParams();
    }//GEN-LAST:event_widthSliderStateChanged

    private void fontSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_fontSpinnerStateChanged
    {//GEN-HEADEREND:event_fontSpinnerStateChanged
        updateParams();
    }//GEN-LAST:event_fontSpinnerStateChanged

    private void legendPositionButtonStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_legendPositionButtonStateChanged
    {//GEN-HEADEREND:event_legendPositionButtonStateChanged
        updateParams();
    }//GEN-LAST:event_legendPositionButtonStateChanged

    /**
     * Sets enabled state on this widget.
     */
    @Override
    public void setEnabled(boolean enabled)
    {
        toggleEnabled(enabled);
    }
    
    private void toggleEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        for (JComponent widget : hideableWidgets) {
            widget.setEnabled(enabled);
        }
        for (JComponent widget : permanentWidgets) {
            widget.setEnabled(enabled);
        }
    }

//    private boolean fireFromHere = false;

    public void setParams(ColormapLegendParameters params)
    {
        this.params = params;
    }

    public void processEnable(boolean enabled)
    {        
        if (enabled) updateParams();
        else {
            params.setPosition(ColormapLegend.NONE);
            params.fireStateChanged();
        }  
    }

    private void updateParams()
    {
        if (params == null)// || fireFromHere)
            return;
        params.setX(lenPosSlider.getBottomValue());
        params.setL(lenPosSlider.getTopValue() - lenPosSlider.getBottomValue());
        params.setY(widthSlider.getBottomValue());
        params.setW(widthSlider.getTopValue() - widthSlider.getBottomValue());
        switch (legendPositionButton.getState()) {
            case 0:
                params.setPosition(ColormapLegend.WEST);
                break;
            case 1:
                params.setPosition(ColormapLegend.SOUTH);
                break;
            case 2:
                params.setPosition(ColormapLegend.EAST);
                break;
            case 3:
                params.setPosition(ColormapLegend.NORTH);
                break;
        }
        params.setColor(colorEditor.getColor());
        params.setFontSize((Integer) fontSpinner.getValue());
        params.fireStateChanged();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.ColorEditor colorEditor;
    private javax.swing.JLabel edgeLabel;
    private javax.swing.JPanel edgePanel;
    private javax.swing.JLabel fontLabel;
    private javax.swing.JPanel fontPanel;
    private javax.swing.JSpinner fontSpinner;
    private org.visnow.vn.gui.widgets.MultistateButton legendPositionButton;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider lenPosSlider;
    private javax.swing.ButtonGroup orientationGroup;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider widthSlider;
    // End of variables declaration//GEN-END:variables
   
        
}
