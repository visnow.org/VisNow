/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import static com.pixelmed.dicom.DicomDirectoryRecordType.image;
import org.jogamp.java3d.utils.image.TextureLoader;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jogamp.java3d.ImageComponent2D;
import org.jogamp.java3d.Texture;
import org.jogamp.java3d.Texture2D;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.apache.log4j.Logger;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.geometries.gui.DataMappingGUI;
import org.visnow.vn.geometries.gui.DataMappingGUI.MappingType;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.utils.ImageUtils;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class DataMappingParams
{
    public static final String[] colorTypes = new String[]{
        "UNCOLORED", "COLORMAPPED", "RGB", "HSV", "COLORMAPPED2D", "UVTEXTURED"
    };
    private static final Logger LOGGER = Logger.getLogger(DataMappingParams.class);
    public static final int NULL = -1;
    public static final int UNCOLORED = 0;
    public static final int COLORMAPPED = 1;
    public static final int RGB = 2;
    public static final int COLORMAPPED2D = 4;
    public static final int COLORED = 7;
    public static final int UVTEXTURED = 8;
    public static final int NO_MAP_MODIFICATION = 0;
    public static final int SAT_MAP_MODIFICATION = 1;
    public static final int VAL_MAP_MODIFICATION = 2;
    public static final int BLEND_MAP_MODIFICATION = 3;

    protected String name = "";
    protected String id = "";

    protected DataContainer nodeData = null;
    protected DataContainer cellData = null;
    protected DataContainer dataContainer = null;
    protected DataContainerSchema nodeDataSchema = null;
    protected DataContainerSchema cellDataSchema = null;
    protected DataContainerSchema dataContainerSchema = null;
    protected ComponentColorMap colorMap0 = new ComponentColorMap();
    protected ComponentColorMap colorMap1 = new ComponentColorMap();
    protected ColorComponentParams redParams = new ColorComponentParams();
    protected ColorComponentParams greenParams = new ColorComponentParams();
    protected ColorComponentParams blueParams = new ColorComponentParams();
    protected ColorComponentParams satParams = new ColorComponentParams();
    protected ColorComponentParams valParams = new ColorComponentParams();
    protected ColorComponentParams uParams = new ColorComponentParams();
    protected ColorComponentParams vParams = new ColorComponentParams();
    protected TransparencyParams transparencyParams = new TransparencyParams();

    protected ComponentColorMap[] colorMaps = {colorMap0, colorMap1};
    protected String[] colorMapNames = {"colorMap0", "colorMap1"};
    protected ColorComponentParams[] mapParams = {redParams, greenParams, blueParams,
                                                  satParams, valParams, uParams, vParams};
    protected String[] mapParamNames = {"redParams", "greenParams", "blueParams",
                                        "satParams","valParams", "uParams", "vParams"};
    protected boolean active = true;
    protected boolean lastActive = true;
    protected boolean cellDataMapped = false;
    protected int colorMapModification = NO_MAP_MODIFICATION;
    protected int colorMode = COLORMAPPED;
    protected float blendRatio = 0.5f;
    protected byte[] invalidColor = new byte[]{0, 0, 0};

    protected PresentationParams parentPresentationParams = null;
    protected boolean inherited = true;

    protected boolean adjusting = false;
    protected int parentObjectSize = 256000;
    protected int continuousColorAdjustingLimit = 100000;

    protected boolean useColormap2D = false;
    protected String textureFileName = null;
    protected BufferedImage textureImage;
    boolean textureImageFlipX = false, textureImageFlipY = false;
    protected Texture2D texture = null;
    protected int colorMap2DIndex = 0;
    protected int modeChanged = 0;
    protected int colorModeChanged = 2;
    protected ColormapLegendParameters colormapLegendParameters = new ColormapLegendParameters();

    protected DataMappingGUI gui = null;
    protected RenderEventListener colorCompChangeListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            if (!active)
                return;
            colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
                colorMapModification == NO_MAP_MODIFICATION &&
                colorMap0.getDataComponentIndex() >= 0);

            adjusting = e.isAdjusting();
            if (e.getSource() == uParams ||
                e.getSource() == vParams) {
                fireStateChanged(RenderEvent.TEXTURE);
            } else if (e.getSource() == transparencyParams) {
                fireStateChanged(RenderEvent.TRANSPARENCY);
            } else {
                fireStateChanged(RenderEvent.COLORS);
            }
        }
    };

    protected RenderEventListener parentChangeListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
//            if ((e.getSource() == parentParams) && inherited)
//                copyValuesFrom(parentParams);
        }
    };

    public DataMappingParams()
    {
        if (VisNow.get() != null)
            continuousColorAdjustingLimit = Integer.parseInt(VisNow.get().getMainConfig().
                                                    getProperty("visnow.continuousColorAdjustingLimit"));
        for (ComponentColorMap cMap: colorMaps) {
            cMap.addRenderEventListener(colorCompChangeListener);
            cMap.getComponentRange().addNull();
        }
        for (ColorComponentParams cParams : mapParams)
            cParams.setListener(colorCompChangeListener);
        if (VisNow.get() == null)
            colorMap0.setMapIndex(0);
        else
            colorMap0.setMapIndex(VisNow.get().getDefaultColorMap());
        colorMap1.getComponentRange().setPrefereNull(true);
        uParams.usePseudocomponents();
        vParams.usePseudocomponents();
        transparencyParams.addListener(colorCompChangeListener);
        colormapLegendParameters.setColorMap(colorMap0);
        colormapLegendParameters.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged(RenderEvent.COLORS);
            }
        });
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public DataContainer getNodeData()
    {
        return nodeData;
    }

    public void setNodeData(DataContainer nodeData)
    {
        dataContainer = this.nodeData = nodeData;
        dataContainerSchema = this.nodeDataSchema = nodeData.getSchema();
        updateContainer();
    }

    public void setNodeDataSchema(DataContainerSchema nodeDataSchema)
    {
        dataContainerSchema = this.nodeDataSchema = nodeDataSchema;
        updateContainer();
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                                    cellDataSchema != null && !cellDataSchema.isEmpty());
            });
    }

    public void setCellData(DataContainer cellData)
    {
        this.cellData = cellData;
        cellDataSchema = cellData.getSchema();
        if (gui != null)
            SwingInstancer.swingRunLater(() ->{
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                                    cellDataSchema != null && !cellDataSchema.isEmpty());
            });
    }

    public void setCellDataSchema(DataContainerSchema cellDataSchema)
    {
        this.cellDataSchema = cellDataSchema;
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                                    cellDataSchema != null && !cellDataSchema.isEmpty());
            });
    }

    public final boolean setInData(DataContainer nodeData, DataContainer cellData)
    {
        active = false;
        dataContainer = this.nodeData = nodeData;
        if (nodeData != null)
            dataContainerSchema = this.nodeDataSchema = nodeData.getSchema();
        this.cellData = cellData;
        if (cellData != null)
            cellDataSchema = cellData.getSchema();
        updateContainer();
        colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
                                            colorMapModification == NO_MAP_MODIFICATION &&
                                            colorMap0.getDataComponentIndex() > -1);
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                                    cellDataSchema != null && !cellDataSchema.isEmpty());
            });
        return true;
    }

    public final boolean setInData(DataContainer nodeData, DataContainerSchema cellDataSchema)
    {
        active = false;
        dataContainer = this.nodeData = nodeData;
        if (nodeData != null)
            dataContainerSchema = this.nodeDataSchema = nodeData.getSchema();
        this.cellDataSchema = cellDataSchema;
        updateContainer();
        colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
                                            colorMapModification == NO_MAP_MODIFICATION &&
                                            colorMap0.getDataComponentIndex() > -1);
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                        cellDataSchema != null && !cellDataSchema.isEmpty());
            });
        return true;
    }

    public final boolean setInDataSchemas(DataContainerSchema nodeDataSchema, DataContainerSchema cellDataSchema)
    {
        active = false;
        dataContainerSchema = this.nodeDataSchema = nodeDataSchema;
        this.cellDataSchema = cellDataSchema;
        updateContainer();
        colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
                                            colorMapModification == NO_MAP_MODIFICATION &&
                                            colorMap0.getDataComponentIndex() >= 0);
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.setNodeCellData(nodeDataSchema != null && !nodeDataSchema.isEmpty(),
                                    cellDataSchema != null && !cellDataSchema.isEmpty());
            });
        return true;
    }

    private void updateContainer() {
        for (ComponentColorMap colorMap : colorMaps) {
            colorMap.setActive(ComponentColorMap.Active.SLEEP);
            colorMap.setContainerSchema(dataContainerSchema);
            colorMap.setActive(ComponentColorMap.Active.ONETIME);
        }
        for (ColorComponentParams cParams : mapParams) {
            cParams.setActive(false);
            if (dataContainer != null)
                cParams.setDataContainer(dataContainer);
            else if (dataContainerSchema != null)
                cParams.setDataContainerSchema(dataContainerSchema);
            cParams.setActive(true);
        }
        transparencyParams.setActive(false);
        boolean volRenderPossible = dataContainer instanceof RegularField &&
                                    ((RegularField)dataContainer).getDims().length == 3 &&
                                    !((RegularField)dataContainer).hasCoords();
        transparencyParams.getComponentRange().setPrefereNull(!volRenderPossible);
        if (dataContainer != null)
            transparencyParams.setContainer(dataContainer);
        else
            transparencyParams.setContainerSchema(dataContainerSchema);
        transparencyParams.setActive(true);
        String[] cNames = new String[dataContainerSchema.getNComponents()];
        for (int i = 0; i < cNames.length; i++)
            cNames[i] = dataContainerSchema.getComponentSchema(i).getName();
        if (StringUtils.findRGB(cNames).length == 3) {
            colorMode = RGB;
            int[] col = StringUtils.findRGB(cNames);
            DataArraySchema rSchema = dataContainerSchema.getComponentSchema(col[0]);
            redParams.setDataComponent(col[0],   (float)rSchema.getPhysMinValue(), (float)rSchema.getPhysMaxValue());
            DataArraySchema gSchema = dataContainerSchema.getComponentSchema(col[1]);
            greenParams.setDataComponent(col[1], (float)gSchema.getPhysMinValue(), (float)gSchema.getPhysMaxValue());
            DataArraySchema bSchema = dataContainerSchema.getComponentSchema(col[2]);
            blueParams.setDataComponent(col[2],  (float)bSchema.getPhysMinValue(), (float)bSchema.getPhysMaxValue() );
            if (gui != null)
                SwingInstancer.swingRunLater(() -> {
                    gui.panelsUpdateMappingTypeSelection(MappingType.RGB);
                    setActive(true);
                });
        }
        else {
            for (int i = 0; i < cNames.length; i++)
                if (cNames[i].toLowerCase().contains("gray") || cNames[i].toLowerCase().contains("grey")) {
                    colorMode = COLORMAPPED;
                    colorMap0.setMapIndex(ColorMapManager.COLORMAP1D_GRAY);
                    colorMap0.getComponentRange().setComponentSchema(i);
                    colorMap0.getComponentRange().setLowUp(0, 255);
                }
            setActive(true);
        }

    }

    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add("cell data mapped: " + cellDataMapped);
        for (int i = 0; i < colorMaps.length; i++) {
            res.add(colorMapNames[i] + " {");
            String[] cm = colorMaps[i].valuesToStringArray();
            for (String s : cm)
                res.add("    " + s);
            res.add("}");
        }
        res.add("transparency {");
        String[] tr = transparencyParams.valuesToStringArray();
        for (String s : tr)
            res.add("    " + s);
        res.add("}");
        for (int i = 0; i < mapParams.length; i++) {
            if (mapParams[i].isUnmodified())
                 continue;
            res.add(mapParamNames[i] + " {");
            String[] cm = mapParams[i].valuesToStringArray();
            for (String s : cm)
                res.add("    " + s);
            res.add("}");
        }
        res.add("modification: " + colorMapModification);
        res.add("blendRatio: " + blendRatio);
        res.add("mode: " + colorMode);
        if (textureFileName != null) {
            res.add("texture file: \"" + textureFileName + "\"");
            res.add("texture flip x: " + textureImageFlipX );
            res.add("texture flip y: " + textureImageFlipY );
        }
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++)
           r[i] = res.get(i);
        return r;
    }

    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring dataMappingParams");
        setActive(false);
        itemLoop:
        for (int i = 0; i < saved.length; i++) {
            if (saved[i].trim().startsWith("cell data mapped")) {
                setCellDataMapped(saved[i].endsWith("true"));
                continue;
            }
            if (saved[i].trim().equals("transparency {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                transparencyParams.restoreValuesFrom(currentBlock);
                i += currentBlock.length + 1;
                continue;
            }
            for (int j = 0; j < colorMaps.length; j++)
                if (saved[i].trim().equals(colorMapNames[j] + " {")) {
                    String[] currentBlock = StringUtils.findBlock(saved, i);
                    colorMaps[j].restoreValuesFrom(currentBlock);
                    i += currentBlock.length + 1;
                    continue itemLoop;
            }
            for (int j = 0; j < mapParams.length; j++)
                if (saved[i].trim().equals(mapParamNames[j] + " {")) {
                    String[] currentBlock = StringUtils.findBlock(saved, i);
                    mapParams[j].restoreValuesFrom(currentBlock);
                    i += currentBlock.length + 1;
                    continue itemLoop;
            }
            if (saved[i].trim().startsWith("modification:")) {
                try {
                    colorMapModification = Integer.parseInt(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
                continue;
            }
            if (saved[i].trim().startsWith("mode")) {
                try {
                    colorMode = Integer.parseInt(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
                continue;
            }
            if (saved[i].trim().startsWith("blendRatio")) {
                try {
                    blendRatio = Float.parseFloat(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
            }
        }
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.updateDataValuesFromParams();
                active = true;
            });
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    public void copyValuesFrom(DataMappingParams src)
    {
        setActive(false);
        int change = RenderEvent.DATA_MAP;
        if (src.cellDataMapped != cellDataMapped)
            change = RenderEvent.GEOMETRY;
        setCellDataMapped(src.isCellDataMapped());
        colorMap0.copyValuesFrom(src.colorMap0);
        colorMap1.copyValuesFrom(src.colorMap1);
        redParams.copyValuesFrom(src.redParams);
        greenParams.copyValuesFrom(src.greenParams);
        blueParams.copyValuesFrom(src.blueParams);
        satParams.copyValuesFrom(src.satParams);
        valParams.copyValuesFrom(src.valParams);
        uParams.copyValuesFrom(src.uParams);
        vParams.copyValuesFrom(src.vParams);
        transparencyParams.copyValuesFrom(src.transparencyParams);
        colorMapModification = src.colorMapModification;
        colorMode = src.colorMode;
        if (gui != null)
            SwingInstancer.swingRunLater(() -> {
                gui.updateDataValuesFromParams();
            });
        active = true;
        fireStateChanged(change);
    }


    public boolean isCellDataMapped()
    {
        return cellDataMapped;
    }


    public void setCellDataMapped(boolean mapCellData)
    {
        if (cellDataMapped == mapCellData)
            return;
        setActive(false);
        if (cellDataSchema== null || cellDataSchema.isEmpty())
            cellDataMapped = false;
        else if (nodeData == null || nodeData.isEmpty())
            cellDataMapped = true;
        else
            cellDataMapped = mapCellData;
        dataContainer = cellDataMapped ? cellData : nodeData;
        dataContainerSchema  = cellDataMapped ? cellDataSchema : nodeDataSchema;
        updateContainer();
        restoreActive();
        fireStateChanged(RenderEvent.GEOMETRY);
    }


    public ColorComponentParams getBlueParams()
    {
        return blueParams;
    }


    public ComponentColorMap getColorMap0()
    {
        return colorMap0;
    }

    public Color3f getDefaultColor()
    {
        return colorMap0.defaultColor;
    }

    public ComponentColorMap getColorMap1()
    {
        return colorMap1;
    }


    public ColorComponentParams getGreenParams()
    {
        return greenParams;
    }


    public ColorComponentParams getRedParams()
    {
        return redParams;
    }


    public ColorComponentParams getValParams()
    {
        return valParams;
    }


    public ColorComponentParams getSatParams()
    {
        return satParams;
    }


    public ColorComponentParams getUParams()
    {
        return uParams;
    }


    public ColorComponentParams getVParams()
    {
        return vParams;
    }

    public boolean isInherited()
    {
        return inherited;
    }

    public void setInherited(boolean inherited)
    {
        this.inherited = inherited;
    }


    public int getColorMapModification()
    {
        return colorMapModification;
    }


    public void setColorMapModification(int colorMapModification)
    {
        this.colorMapModification = colorMapModification;
        colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
            colorMapModification == NO_MAP_MODIFICATION &&
            colorMap0.getDataComponentIndex() >= 0);
        if (colorMode == COLORMAPPED)
            fireStateChanged(RenderEvent.COLORS);
    }


    public int getColorMode()
    {
        return colorMode;
    }


    public void setColorMode(int colorMode)
    {
        this.colorMode = colorMode;
        colormapLegendParameters.setEnabled(colorMode == COLORMAPPED &&
            colorMapModification == NO_MAP_MODIFICATION &&
            colorMap0.getDataComponentIndex() >= 0);
        fireStateChanged(RenderEvent.COLORS);
    }


    public float getBlendRatio()
    {
        return blendRatio;
    }


    public void setBlendRatio(float blendRatio)
    {
        this.blendRatio = blendRatio;
        fireStateChanged(RenderEvent.COLORS);
    }

    public String getTextureFileName()
    {
        return textureFileName;
    }

    public void setTextureImageFlipX(boolean textureImageFlipX)
    {
        this.textureImageFlipX = textureImageFlipX;
        textureImage = ImageUtils.flipImageHorizontal(textureImage);
        updateTextureImage();
    }

    public void setTextureImageFlipY(boolean textureImageFlipY)
    {
        this.textureImageFlipY = textureImageFlipY;
        textureImage = ImageUtils.flipImageVertical(textureImage);
        updateTextureImage();
    }

    private void updateTextureImage()
    {
        TextureLoader tl = new TextureLoader(textureImage);
        texture = (Texture2D) (tl.getTexture());
        texture.setMinFilter(Texture.NICEST);
        texture.setMagFilter(Texture.NICEST);
        if (colorMode == UVTEXTURED)
            fireStateChanged(RenderEvent.TEXTURE);
    }

    public void setTextureFileName(String textureFileName)
    {
        this.textureFileName = textureFileName;
        try {
            textureImage = ImageUtils.readImage(textureFileName);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        if (image == null) {
            texture = null;
            textureImage = null;
            return;
        }
        updateTextureImage();
    }

    public Texture2D getTexture()
    {
        return texture;
    }


    public void setTexture(Texture2D texture)
    {
        this.texture = texture;
        if (colorMode == UVTEXTURED) {
            LOGGER.info("renderExtentChanged texture");
            fireStateChanged(RenderEvent.TEXTURE);
        }
    }

    public int getModeChanged()
    {
        return modeChanged;
    }

    public void setModeChanged(int modeChanged)
    {
        this.modeChanged = modeChanged;
    }

    public int getColorModeChanged()
    {
        return colorModeChanged;
    }

    public void setColorModeChanged(int colorModeChanged)
    {
        this.colorModeChanged = colorModeChanged;
    }


    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    public void setParentObjectSize(int parentObjectSize)
    {
        this.parentObjectSize = parentObjectSize;
    }

    public ColormapLegendParameters getColormapLegendParameters()
    {
        return colormapLegendParameters;
    }


    public void setActive(boolean active)
    {
        lastActive = active;
        this.active = active;
        for (ColorComponentParams colorParams : mapParams)
            colorParams.setActive(active);
        transparencyParams.setActive(active);
    }

    public void restoreActive()
    {
        active = lastActive;
        for (ColorComponentParams colorParams : mapParams)
            colorParams.restoreActive();
        transparencyParams.restoreActive();
    }

    /**
     * Utility field holding list of RenderEventListeners.
     */
    private transient List<RenderEventListener> renderEventListenerList = new ArrayList<>();

    public synchronized boolean hasListener(RenderEventListener listener)
    {
        return renderEventListenerList.contains(listener);
    }
    /**
     * Registers RenderEventListener to receive events.
     * <p>
     * @param listener The listener to register.
     */

    public synchronized void addRenderEventListener(RenderEventListener listener)
    {
        if (!renderEventListenerList.contains(listener))
            renderEventListenerList.add(listener);
    }

    /**
     * Removes RenderEventListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */

    public synchronized void removeRenderEventListener(RenderEventListener listener)
    {
        renderEventListenerList.remove(listener);
    }

    public void clearRenderEventListeners()
    {
        renderEventListenerList.clear();
    }

    public void fireStateChanged(int change)
    {
        if (!active)
            return;
        boolean adj = adjusting ||
        colorMap0.isAdjusting() || colorMap1.isAdjusting() ||
        satParams.isAdjusting() || valParams.isAdjusting() ||
        redParams.isAdjusting() || greenParams.isAdjusting() || blueParams.isAdjusting() ||
        transparencyParams.isAdjusting();
        if (active && (!adj || parentObjectSize < continuousColorAdjustingLimit)) {
            RenderEvent e = new RenderEvent(this, change);
            for (RenderEventListener listener : renderEventListenerList)
                listener.renderExtentChanged(e);
        }
        if (parentPresentationParams != null) {
            parentPresentationParams.spreadMappingParams();
        }
    }

    /**
     * @param useColormap2D the useColormap2D to set
     */

    public void setUseColormap2D(boolean useColormap2D)
    {
        this.useColormap2D = useColormap2D;
        if (colorMode == COLORMAPPED2D)
            fireStateChanged(RenderEvent.COLORS);
    }

    public BufferedImage getTextureImage()
    {
        return textureImage;
    }

    public void setTextureImage(BufferedImage image)
    {
        textureImage = image;
        texture = new Texture2D(Texture2D.BASE_LEVEL, Texture2D.RGB,
                                textureImage.getWidth(), textureImage.getHeight());
        texture.setImage(0, new ImageComponent2D(ImageComponent2D.FORMAT_RGB, textureImage));
        texture.setMinFilter(Texture.NICEST);
        texture.setMagFilter(Texture.NICEST);
        if (colorMode == UVTEXTURED)
            fireStateChanged(RenderEvent.TEXTURE);
    }

    public int getColorMap2DIndex()
    {
        return colorMap2DIndex;
    }


    public void setColorMap2DIndex(int colorMap2DIndex)
    {
        this.colorMap2DIndex = colorMap2DIndex;
        fireStateChanged(RenderEvent.COLORS);
    }

    public boolean isUseColormap2D()
    {
        return useColormap2D;
    }

    public byte[] getInvalidColor()
    {
        return invalidColor;
    }

    public void setInvalidColor(byte[] invalidColor)
    {
        this.invalidColor = invalidColor;
    }

    public DataMappingParams getMappingParams()
    {
        return this;
    }

    public TransparencyParams getTransparencyParams()
    {
        return transparencyParams;
    }

    public DataContainer getDataContainer()
    {
        return dataContainer;
    }

    public RenderEventListener getParentChangeListener()
    {
        return parentChangeListener;
    }

    public void setParentParams(PresentationParams parentParams)
    {
        this.parentPresentationParams = parentParams;
    }

    public void setGUI(DataMappingGUI gui)
    {
        this.gui = gui;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

}
