/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.datamaps.ColorMap;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.geometries.gui.ColormapChooser;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.utils.Range;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentColorMap implements ColorMap
{
    public static final int CONTINUOUS = 8;

    protected static enum MapType
    {
        /**
         * standard map defined by color knots editable in colormap editor
         */
        STANDARD, 
        /**
         * special map type defined by two immediately editable colors
         */
        TWOCOLOR,
        /**
         * special map type defined by three immediately editable colors
         */
        THREECOLOR
    };

    protected ComponentSubrange componentRange = new ComponentSubrange();
    protected int mapIndex = ColorMapManager.COLORMAP1D_RAINBOW;
    protected boolean mapIndexChanged = false;
    protected ColorMap1D map = ColorMapManager.getInstance().getColorMap1D(mapIndex);
    protected float brightness = 0;
    protected boolean brightnessChanged = false;
    protected int nSteps = 0;
    protected float step = 1;
    protected boolean stepChanged = false;
    protected boolean continuous = true;
    protected boolean useStepCount = true;
    protected boolean reverse = false;
    protected int[] rgbColorTable = new int[ColorMapManager.SAMPLING_TABLE];
    protected int[] argbColorTable = new int[ColorMapManager.SAMPLING_TABLE];
    protected byte[] rgbByteColorTable = new byte[3 * ColorMapManager.SAMPLING_TABLE];
    protected byte[] argbByteColorTable = new byte[4 * ColorMapManager.SAMPLING_TABLE];
    protected int[] originalARGBTable = new int[ColorMapManager.SAMPLING_TABLE];
    protected Range range = null;
    protected MapType type = MapType.STANDARD;
    protected float[] c0  = {0, 0, 0}, 
                      c1  = {1, 1, 1},
                      c00 = {0, 0, 1}, 
                      c05 = {1, 1, 1}, 
                      c10 = {1, 0, 0};
    protected boolean bicolorMapChanged = false;
    protected boolean tricolorMapChanged = false;
    protected boolean adjusting = false;
    protected Color3f defaultColor = new Color3f(.5f, .5f, .5f);
    protected boolean defaultColorChanged = false;
    protected boolean wrap = false;
    protected ColormapChooser ui = null;

    public static enum Active
    {
        SLEEP, ONETIME, CONTINUOUS
    };

    private Active active = Active.CONTINUOUS;

    public ComponentColorMap()
    {
        //TBD
        //This is a good example of our problem with inter-thread listeners
        //stateChanged can be called by another thread (non-Swing) which is an error and can cause thread locks
        //e.g. it is called by classes inheriting from ComponentFeature in fireStateChanged() method,
        //A general question is how to make thread-aware listeners?

        componentRange.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e)
            {
                ComponentSubrange componentRange = (ComponentSubrange)e.getSource();
                if (ui != null)
                {
                     SwingInstancer.swingRunLater(() -> {
                         ui.setComponentRange(componentRange);
                     });
                }
                updateColorTables();
                fireStateChanged(componentRange.isAdjusting() ? 
                                 Active.CONTINUOUS : 
                                 Active.ONETIME, RenderEvent.COLORS);
            }
        });
        updateColorTables();
    }

    public void setUI(ColormapChooser ui)
    {
        this.ui = ui;
    }
    
    public float getBrightness()
    {
        return brightness;
    }

    public void setFinalBrightness(float brightness)
    {
        brightnessChanged = true;
        this.brightness = brightness;
        updateColorTables();
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }

    public void setReverse(boolean reverse)
    {
        this.reverse = reverse;
        updateColorTables();
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }

    public void setContinuous(boolean continuous)
    {
        this.continuous = continuous;
        updateColorTables();
    }
    
    public void setStep(float step)
    {
        stepChanged = true;
        this.step = step;
        updateColorTables();
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }

    public int getMapType()
    {
        return mapIndex;
    }

    public void setMapIndex(int mapIndex)
    {
        mapIndexChanged = true;
        this.mapIndex = mapIndex;
        map = ColorMapManager.getInstance().getColorMap1D(mapIndex);
        if (map.getName().equalsIgnoreCase("bicolor"))
            type = MapType.TWOCOLOR;
        else if (map.getName().equalsIgnoreCase("tricolor"))
            type = MapType.THREECOLOR;
        else
            type = MapType.STANDARD;
        updateColorTables();
    }
    
/**
 * argbColorTable (int[]) is the primary source of data for coloring. Some Java methods 
 * prefer byte arrays or RGB data only. To improve data mapping speed, additional color tables are created and cached
 */
    protected final void updateAdditionalColorTables()
    {
        for (int i = 0; i < rgbColorTable.length; i++) {
            rgbColorTable[i] = argbColorTable[i] | (0xff << 24);
            argbByteColorTable[4 * i + 1] = rgbByteColorTable[3 * i] = (byte) (0xff & (argbColorTable[i] >> 16));
            argbByteColorTable[4 * i + 2] = rgbByteColorTable[3 * i + 1] = (byte) (0xff & (argbColorTable[i] >> 8));
            argbByteColorTable[4 * i + 3] = rgbByteColorTable[3 * i + 2] = (byte) (0xff & argbColorTable[i]);
            argbByteColorTable[4 * i] = (byte) (0xff & (argbColorTable[i] >> 24));
        }
        if (ui != null)
            SwingInstancer.swingRunLater(() -> {
                ui.updateMapImage();
        });
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }
    

    protected final void createSteppedARGB(float step)
    {
        if (componentRange == null || continuous)
            return;
        int n = argbColorTable.length;
        float cLow = componentRange.getPhysicalLow();
        float cUp = componentRange.getPhysicalUp();
        float low = step * (int)(Math.ceil(cLow/step));
        int m = (int)(Math.floor((cUp - cLow) / step));
        float toInd = n / (cUp - cLow);
//        System.out.printf("%3d %6.2f %6.2f %3d %6.2f %6.2f %3d  %6.2f%n", nSteps, cLow, cUp, n, low, d, m, toInd);
        for (int i = -1; i <= m; i++) {
            int k = Math.max(0, (int) ((i * step + low - cLow) * toInd));
            int l = Math.min((int) (((i + 1) * step + low - cLow) * toInd), n);
//            System.out.printf("%3d %3d %3d %n", i,k,l);
            for (int j = k; j < l; j++)
                argbColorTable[j] = originalARGBTable[k];
        }
    }
    
    public final void setColorTables(float[] inc0, float[] inc1)
    {
        bicolorMapChanged = true;
        if (inc0 != null)
            c0 = inc0;
        if (inc1 != null)
            c1 = inc1;
        int n = originalARGBTable.length - 1;
        float[] dc = new float[c0.length];
        for (int i = 0; i < dc.length; i++)
            dc[i] = (c1[i] - c0[i]) / n;
        for (int i = 0; i < originalARGBTable.length; i++) {
            int a = 0xff;
            int r = 0xff & (int) (255 * (c0[0] + i * dc[0]));
            int g = 0xff & (int) (255 * (c0[1] + i * dc[1]));
            int b = 0xff & (int) (255 * (c0[2] + i * dc[2]));
            originalARGBTable[i] = (a << 24) | (r << 16) | (g << 8) | b;
        }
        if (!continuous) 
            createSteppedARGB(step);
        else
            System.arraycopy(originalARGBTable, 0, argbColorTable, 0, argbColorTable.length);
        updateAdditionalColorTables();
    }

    public final void setColorTables(float[] inc00, float[] inc05, float[] inc10)
    {
        tricolorMapChanged = true;
        if (inc00 != null)
            c00 = inc00;
        if (inc05 != null)
            c05 = inc05;
        if (inc10 != null)
            c10 = inc10;
        int n = (originalARGBTable.length - 1) / 2;
        int n1 = originalARGBTable.length / 2;
        float[] dc = new float[c0.length];
        for (int i = 0; i < dc.length; i++)
            dc[i] = (c05[i] - c00[i]) / n;
        for (int i = 0; i < n1; i++) {
            int a = 0xff;
            int r = 0xff & (int) (255 * (c00[0] + i * dc[0]));
            int g = 0xff & (int) (255 * (c00[1] + i * dc[1]));
            int b = 0xff & (int) (255 * (c00[2] + i * dc[2]));
            originalARGBTable[i] = (a << 24) | (r << 16) | (g << 8) | b;
        }
        for (int i = 0; i < dc.length; i++)
            dc[i] = (c10[i] - c05[i]) / n;
        for (int i = 0; i < n1; i++) {
            int a = 0xff;
            int r = 0xff & (int) (255 * (c05[0] + i * dc[0]));
            int g = 0xff & (int) (255 * (c05[1] + i * dc[1]));
            int b = 0xff & (int) (255 * (c05[2] + i * dc[2]));
            originalARGBTable[i + n1] = (a << 24) | (r << 16) | (g << 8) | b;
        }

        if (!continuous) 
            createSteppedARGB(step);
        else
            System.arraycopy(originalARGBTable, 0, argbColorTable, 0, argbColorTable.length);
        updateAdditionalColorTables();
    }

    public final void updateColorTables()
    {
        switch (type) {
            case STANDARD:
                float br = 1 - abs(brightness);
                int[] cTable = map.getARGBColorTable();
                int n = cTable.length - 1;
                for (int i = 0; i < argbColorTable.length; i++) {
                    int j = cTable[i];
                    int a = 0xff & (j >> 24);
                    int r = 0xff & (j >> 16);
                    int g = 0xff & (j >> 8);
                    int b = 0xff & j;
                    if ((brightness < 0))
                        j = (((int) (br * r) & 0xff) << 16) |
                            (((int) (br * g) & 0xff) << 8) |
                             ((int) (br * b) & 0xff) |
                                   ((a & 0xff) << 24);
                    else
                        j = (((int) (br * r + (1 - br) * 255) & 0xff) << 16) |
                            (((int) (br * g + (1 - br) * 255) & 0xff) << 8) |
                             ((int) (br * b + (1 - br) * 255) & 0xff) |
                                    ((a & 0xff) << 24);
                    if (reverse)
                        originalARGBTable[n - i] = j;
                    else
                        originalARGBTable[i] = j;
                }
                break;
            case TWOCOLOR:
                setColorTables(null, null);
                return;
            case THREECOLOR:
                setColorTables(null, null, null);
                return;
        }
        if (!continuous) 
            createSteppedARGB(step);
        else
            System.arraycopy(originalARGBTable, 0, argbColorTable, 0, argbColorTable.length);
        updateAdditionalColorTables();
    }

    public void setActive(Active active)
    {
        this.active = active;
        if (active != Active.SLEEP)
            fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }

    public void setActiveValue(Active active)
    {
        this.active = active;
    }

    /**
     * Utility field holding list of RenderEventListeners.
     */
    private final ArrayList<RenderEventListener> renderEventListeners = new ArrayList<>();

    /**
     * Registers RenderEventListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addRenderEventListener(RenderEventListener listener)
    {
        renderEventListeners.add(listener);
    }
    
    public synchronized void clearRenderEventListeners()
    {
        renderEventListeners.clear();
    }

    /**
     * Removes RenderEventListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeRenderEventListener(RenderEventListener listener)
    {
        renderEventListeners.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param level
     * @param change
     */
    public void fireStateChanged(Active level, int change)
    {
        if (active == Active.SLEEP)
            return;
        if (active == Active.CONTINUOUS || level == Active.ONETIME) {
            RenderEvent e = new RenderEvent(this, change);
            try {
                for (RenderEventListener renderEventListener : renderEventListeners) {
                    renderEventListener.renderExtentChanged(e);
                }
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public boolean isBuildin()
    {
        return map.isBuildin();
    }

    @Override
    public String getName()
    {
        return "customized" + map.getName();
    }

    @Override
    public int[] getRGBColorTable()
    {
        return rgbColorTable;
    }

    @Override
    public int[] getARGBColorTable()
    {
        return argbColorTable;
    }

    @Override
    public byte[] getRGBByteColorTable()
    {
        return rgbByteColorTable;
    }

    @Override
    public byte[] getARGBByteColorTable()
    {
        return argbByteColorTable;
    }

    public void setMapType(MapType type)
    {
        this.type = type;
    }

    public ColorMap1D getMap()
    {
        return map;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener)
    {

    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener)
    {

    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    public boolean isReverse()
    {
        return reverse;
    }

    /**
     * @return the active
     */
    public Active getActive()
    {
        return active;
    }

    public ComponentSubrange getComponentRange()
    {
        return componentRange;
    }
    
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        componentRange.setContainerSchema(containerSchema);
    }

    public Range getRange()
    {
        return range;
    }

    public int getDataComponentIndex()
    {
        return componentRange.getComponentIndex();
    }

    public DataArraySchema getDataComponentSchema()
    {
        return componentRange.getComponentSchema();
    }
    
    public String getDataComponentName()
    {
        return componentRange.getComponentName();
    }
    
    public float getDataMin()
    {
        return componentRange.getLow();
    }
    
    public float getDataMax()
    {
        return componentRange.getUp();
    }

    public boolean isWrap()
    {
        return wrap;
    }

    public void setWrap(boolean wrap)
    {
        this.wrap = wrap;
    }

    public Color3f getDefaultColor()
    {
        return defaultColor;
    }

    public void setDefaultColor(Color3f defaultColor)
    {
        defaultColorChanged = true;
        this.defaultColor = defaultColor;
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }
    
    
    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add("componentRange {");
        String[] cm = componentRange.valuesToStringArray();
        for (String s : cm) 
            res.add("    " + s);
        res.add("}");
        if (mapIndexChanged)
            res.add("mapIndex: " + mapIndex);   
        if (brightnessChanged)
            res.add("brightness: " + brightness); 
        if (!continuous)
            res.add("continuous: " + continuous);
        if (stepChanged)
            res.add("step: " + step);   
        if (reverse)
            res.add("reverse: " + reverse); 
        if (bicolorMapChanged) {
            res.add(String.format("c0:  %6.3f %6.3f %6.3f", c0[0], c0[1], c0[2])); 
            res.add(String.format("c1:  %6.3f %6.3f %6.3f", c1[0], c1[1], c1[2])); 
        }
        if (tricolorMapChanged) {
            res.add(String.format("c00: %6.3f %6.3f %6.3f", c00[0], c00[1], c00[2])); 
            res.add(String.format("c05: %6.3f %6.3f %6.3f", c05[0], c05[1], c05[2])); 
            res.add(String.format("c10: %6.3f %6.3f %6.3f", c10[0], c10[1], c10[2])); 
        }
        if (defaultColorChanged) {
            float[] dc = new float[3];
            defaultColor.get(dc);
            res.add(String.format("defaultColor: %6.3f %6.3f %6.3f", dc[0], dc[1], dc[2])); 
        }
        if (wrap)
            res.add("wrap: " + wrap); 
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++) 
           r[i] = res.get(i);
        return r;
    }
    
    public void restoreValuesFrom(String[] saved)
    {
        float[] dc = new float[3];
        float[][] colors = new float[][]{c0, c1, c00, c05, c10, dc};
        String[] colNames = new String[]{"c0", "c1", "c00", "c05", "c10", "dc"};
        itemLoop:
        for (int i = 0; i < saved.length; i++) {
            if (saved[i].trim().equals("componentRange {")) {
                String[] savedRange = StringUtils.findBlock(saved, i);
                componentRange.restoreFromStringArray(savedRange);
                i += savedRange.length + 1;
                continue;
            }    
            if (saved[i].trim().startsWith("mapIndex")) {
                try {
                   mapIndex  = Integer.parseInt(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
                continue;
            }
            if (saved[i].trim().startsWith("brightness")) {
                try {
                    brightness  = Float.parseFloat(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
                continue;
            }
            if (saved[i].trim().startsWith("continuous")) {
                continuous = saved[i].endsWith("true");  
                continue;
            }
            if (saved[i].trim().startsWith("step")) {
                try {
                    step  = Float.parseFloat(saved[i].trim().split(" *:* +")[1]);
                } catch (Exception e) {}
                continue;
            }
            if (saved[i].trim().startsWith("reverse")) {
                reverse = saved[i].endsWith("true");  
                continue;
            }
            for (int j = 0; j < colNames.length; j++) 
                if (saved[i].trim().equals(colNames[j] + ":")) {
                    String[] v = saved[i].trim().split(" *:* +");
                    for (int k = 0; k < 3; k++) 
                        try {
                            colors[j][k] = Float.parseFloat(v[k + 1]);
                        } catch (Exception e) {} 
                    continue itemLoop;  
                }
            if (saved[i].trim().startsWith("wrap")) {
                wrap = saved[i].endsWith("true");  
            }
        }
        setMapIndex( mapIndex);
        defaultColor = new Color3f(dc);
    }
    
    public void copyValuesFrom(ComponentColorMap src)
    {
        componentRange.copyValuesFrom(src.componentRange);
        mapIndex = src.mapIndex;
        map = ColorMapManager.getInstance().getColorMap1D(mapIndex);
        brightness = src.brightness;
        nSteps = src.nSteps;
        step = src.step;
        continuous = src.continuous;
        reverse = src.reverse;
        System.arraycopy(src.rgbColorTable, 0, rgbColorTable, 0, rgbColorTable.length);
        System.arraycopy(src.argbColorTable, 0, argbColorTable, 0, argbColorTable.length);
        System.arraycopy(src.rgbByteColorTable, 0, rgbByteColorTable, 0, rgbByteColorTable.length);
        System.arraycopy(src.argbByteColorTable, 0, argbByteColorTable, 0, argbByteColorTable.length);
        System.arraycopy(src.originalARGBTable, 0, argbColorTable, 0, argbColorTable.length);
        type = src.type;
        System.arraycopy(src.c0, 0, c0, 0, 3);
        System.arraycopy(src.c1, 0, c1, 0, 3);
        System.arraycopy(src.c00, 0, c00, 0, 3);
        System.arraycopy(src.c05, 0, c05, 0, 3);
        System.arraycopy(src.c10, 0, c10, 0, 3);
        defaultColor = new Color3f(src.defaultColor);
        wrap = src.wrap;
        fireStateChanged(Active.ONETIME, RenderEvent.COLORS);
    }
    
}
