/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.awt.Color;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class PointLightParams
{

    protected Color color = Color.LIGHT_GRAY;
    protected float constantAttenuation = 1;
    protected float linearAttenuation = 1;
    protected float quadraticAttenuation = 0;

    public PointLightParams()
    {
    }

    public PointLightParams(Color col, float cnst, float lin, float quad)
    {
        color = col;
        constantAttenuation = cnst;
        linearAttenuation = lin;
        quadraticAttenuation = quad;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public float getConstantAttenuation()
    {
        return constantAttenuation;
    }

    public void setConstantAttenuation(float constantAttenuation)
    {
        this.constantAttenuation = constantAttenuation;
    }

    public float getLinearAttenuation()
    {
        return linearAttenuation;
    }

    public void setLinearAttenuation(float linearAttenuation)
    {
        this.linearAttenuation = linearAttenuation;
    }

    public float getQuadraticAttenuation()
    {
        return quadraticAttenuation;
    }

    public void setQuadraticAttenuation(float quadraticAttenuation)
    {
        this.quadraticAttenuation = quadraticAttenuation;
    }

}
