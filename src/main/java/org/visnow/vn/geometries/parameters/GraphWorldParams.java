/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.objects.DisplayedData;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class GraphWorldParams extends Parameters
{

    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<int[]>("horizontal extents", ParameterType.dependent, null),
        new ParameterEgg<int[]>("vertical extents", ParameterType.dependent, null),
        new ParameterEgg<Color>("color", ParameterType.independent, Color.WHITE),
        new ParameterEgg<Float>("font size", ParameterType.independent, .014f),
        new ParameterEgg<Float>("line width", ParameterType.independent, 2.f),
        new ParameterEgg<String[]>("axes labels", ParameterType.independent, null),
        new ParameterEgg<String>("title", ParameterType.independent, ""),
        new ParameterEgg<DisplayedData[]>("data", ParameterType.independent, null),
        new ParameterEgg<Boolean>("color legend", ParameterType.independent, false),
        new ParameterEgg<Boolean>("refresh", ParameterType.independent, false),};

    public GraphWorldParams()
    {
        super(eggs);
        setValue("horizontal extents", new int[]{30, 90});
        setValue("vertical extents", new int[]{75, 97});
        setValue("axes labels", new String[]{"x", "y"});
    }

    public int[] getHorizontalExtents()
    {
        return (int[]) getValue("horizontal extents");
    }

    public void setHorizontalExtents(int low, int up)
    {
        int[] hex = getHorizontalExtents();
        hex[0] = low;
        hex[1] = up;
        fireStateChanged();
    }

    public int[] getVerticalExtents()
    {
        return (int[]) getValue("vertical extents");
    }

    public void setVerticalExtents(int low, int up)
    {
        int[] hex = getVerticalExtents();
        hex[0] = low;
        hex[1] = up;
        fireStateChanged();
    }

    public Color getColor()
    {
        return (Color) getValue("color");
    }

    public void setColor(Color color)
    {
        setValue("color", color);
        fireStateChanged();
    }

    public DisplayedData[] getDisplayedData()
    {
        return (DisplayedData[]) getValue("data");
    }

    public void setDisplayedData(DisplayedData[] displayedData)
    {
        setValue("data", displayedData);
        fireStateChanged();
    }

    public float getFontSize()
    {
        return (Float) getValue("font size");
    }

    public void setFontSize(float size)
    {
        setValue("font size", size);
        fireStateChanged();
    }

    public float getLineWidth()
    {
        return (Float) getValue("line width");
    }

    public void setLineWidth(float width)
    {
        setValue("line width", width);
        fireStateChanged();
    }

    public String[] getAxesLabels()
    {
        return (String[]) getValue("axes labels");
    }

    public void setAxesLabels(String[] labels)
    {
        setValue("axes labels", labels);
        fireStateChanged();
    }

    public String getTitle()
    {
        return (String) getValue("title");
    }

    public void setTitle(String title)
    {
        setValue("title", title);
        fireStateChanged();
    }

    public void updateTable()
    {
        fireStateChanged();
    }

    public boolean isColorLegend()
    {
        return (Boolean) getValue("color legend");
    }

    public void setColorLegend(boolean cl)
    {
        setValue("color legend", cl);
        fireStateChanged();
    }

    public boolean isRefresh()
    {
        boolean r = (Boolean) getValue("refresh");
        setValue("refresh", false);
        return r;
    }

    public void setRefresh(boolean r)
    {
        setValue("refresh", false);
        fireStateChanged();
    }
}
