/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import static org.apache.commons.math3.util.FastMath.*;
import static org.visnow.vn.geometries.parameters.FontParams.Decoration.*;
import static org.visnow.vn.geometries.parameters.FontParams.Position.*;
import org.visnow.vn.geometries.utils.ColorMapper;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FontParams
{

    public enum Position 
    {
        RADIAL  ( 0,  0,  0,  0,  0,  0), 
        N       ( 0,  1,  0, -1,  1, -1), 
        NE      ( 1,  1,  1, -1,  1, -1), 
        E       ( 1,  0,  1,  0,  1, -1), 
        SE      ( 1, -1,  1,  1,  1,  1), 
        S       ( 0, -1,  0,  1,  1,  1), 
        SW      (-1, -1, -1,  1, -1,  -1),
        W       (-1,  0, -1,  0, -1,  -1), 
        NW      (-1,  1, -1, -1, -1,  -1),
        AT_POINT( 0,  0, -1, -1,  0,  0) ;
        private final int xShift;
        private final int yShift;
        private final int xFrameShift;
        private final int yFrameShift;
        private final int xEdgeShift;
        private final int yEdgeShift;

        private Position(int xShift, int yShift, int xFrameShift, int yFrameShift, int xEdgeShift, int yEdgeShift)
        {
            this.xShift = xShift;
            this.yShift = yShift;
            this.xFrameShift = xFrameShift;
            this.yFrameShift = yFrameShift;
            this.xEdgeShift = xEdgeShift;
            this.yEdgeShift = yEdgeShift;
        }

        public int getxShift() {
            return xShift;
        }

        public int getyShift() {
            return yShift;
        }

        public int getxFrameShift() {
            return xFrameShift;
        }

        public int getyFrameShift() {
            return yFrameShift;
        }

        public int getxEdgeShift()
        {
            return xEdgeShift;
        }

        public int getyEdgeShift()
        {
            return yEdgeShift;
        }
        
        public static Position getPosition(float x, float y)
        {
            double phi = Math.atan2(y, x) / Math.PI;
            if (phi < 0)
                phi += 2;
            int dir = (int)(4 * phi + .5);
            if (dir >= 8)
                dir = 0;
            switch (dir) {
                case 0:
                    return E;
                case 1:
                    return NE;
                case 2:
                    return N;
                case 3:
                    return NW;
                case 4:
                    return W;
                case 5:
                    return SW;
                case 6:
                    return S;
                default:
                    return SE;
            }
        }
    }
    public enum Decoration {FRAME, EDGE, NONE}
    
    private boolean threeDimensional = false;
    private float size = .02f; //size of text glyps relative to the window size (2D) or field diameter (3d)
    private float precision = 3; //precision of font model used for 3d fonts
    private float font3DSize = 1;
    private String fontName = "sans-serif";
    private int fontSize = 15; //font size in pixels for 2d labels
    private int fontType = Font.PLAIN;
    private Color color = Color.WHITE;
    private float colorCorrection = 1;
    
    private Position position = RADIAL;
    private Decoration decoration = NONE;
    private float opacity = .5f;
    private float shift = .5f;
    private Color bgColor = Color.BLACK;

    public FontParams() 
    {
        
    }
    
    public FontParams(boolean threeDimensional, float size, float precision, String fontName, int fontType, Color color) 
    {
        this.threeDimensional = threeDimensional;
        this.size = size;
        this.precision = precision;
        this.fontName = fontName;
        this.fontType = fontType;
        this.color = color;
    }

    public FontParams clone()
    {
        FontParams clone = new FontParams(threeDimensional, size, precision, fontName, fontType, color);
        clone.setPosition(position);
        clone.setDecoration(decoration);
        clone.setOpacity(opacity);
        clone.setBgColor(bgColor);
        clone.setShift(shift);
        return clone;
    }
    
    public void createFontMetrics(LocalToWindow localToWindow, int w, int h)
    {
        fontSize = max(7, (int) (h * size));
        float z = localToWindow.transformPt(new double[]{0, 0, 0}, new int[2]);
        float[] xl = localToWindow.reverseTransformPt(w / 2, (h - fontSize) / 2, z);
        float[] xu = localToWindow.reverseTransformPt(w / 2, (h + fontSize) / 2, z);
        font3DSize = (float) (sqrt((xu[0] - xl[0]) * (xu[0] - xl[0]) +
                                   (xu[1] - xl[1]) * (xu[1] - xl[1]) +
                                   (xu[2] - xl[2]) * (xu[2] - xl[2])));
    }

    /**
     * Get the value of color
     *
     * @return the value of color
     */
    public Color getColor()
    {
        return color;
    }

    public Color3f getColor3f()
    {
        return ColorMapper.convertColorToColor3f(color);
    }

    /**
     * Set the value of color
     *
     * @param color new value of color
     */
    public void setColor(Color color)
    {
        this.color = color;
        fireStateChanged();
    }

    /**
     * Get the value of bgColor
     *
     * @return the value of transparent background color
     */
    public Color getBgColor()
    {
        return bgColor;
    }

    /**
     * Set the value of bgColor
     *
     * @param bgColor new value of bgColor
     */
    public void setBgColor(Color bgColor)
    {
        this.bgColor = bgColor;
        fireStateChanged();
    }
    
    /**
     * Set the value of fontType
     *
     * @param fontType new value of fontType
     */
    public void setFontType(int fontType)
    {
        this.fontType = fontType;
        fireStateChanged();
    }

    /**
     * Get the value of font2D
     *
     * @return the value of font2D
     */
    public Font getFont2D()
    {
        return new Font(fontName, fontType, fontSize);
    }

    public String getFontName()
    {
        return fontName;
    }

    public int getFontSize()
    {
        return (int) (threeDimensional ? precision * fontSize : fontSize);
    }

    public int getFontType()
    {
        return fontType;
    }

    public float getPrecision()
    {
        return precision;
    }

    public float getFont3DSize()
    {
        return font3DSize;
    }

    /**
     * Set the value of fontName
     *
     * @param fontName new value of fontName
     */
    public void setFontName(String fontName)
    {
        this.fontName = fontName;
        fireStateChanged();
    }

    /**
     * Set the value of precision
     *
     * @param precision new value of precision
     */
    public void setPrecision(float precision)
    {
        this.precision = precision;
        fireStateChanged();
    }

    public float getSize()
    {
        return size;
    }

    /**
     * Set the value of size
     *
     * @param size new value of size
     */
    public void setSize(float size)
    {
        this.size = size;
        fireStateChanged();
    }

    /**
     * Get the value of threeDimensional
     *
     * @return the value of threeDimensional
     */
    public boolean isThreeDimensional()
    {
        return threeDimensional;
    }

    /**
     * Set the value of threeDimensional
     *
     * @param threeDimensional new value of threeDimensional
     */
    public void setThreeDimensional(boolean threeDimensional)
    {
        this.threeDimensional = threeDimensional;
        fireStateChanged();
    }

    public void setBold(boolean bold)
    {
        if (bold)
            fontType |= Font.BOLD;
        else
            fontType &= ~Font.BOLD;
        fireStateChanged();
    }

    public void setItalic(boolean italic)
    {
        if (italic)
            fontType |= Font.ITALIC;
        else
            fontType &= ~Font.ITALIC;
        fireStateChanged();
    }

    public void setValues(Font font, boolean threeDimensional, float size, float precision)
    {
        this.threeDimensional = threeDimensional;
        this.size = size;
        this.precision = precision;
    }

    public float getColorCorrection()
    {
        return colorCorrection;
    }

    public void setColorCorrection(float colorCorrection)
    {
        this.colorCorrection = colorCorrection;
        fireStateChanged();
    }
    
 
    /**
     * Get the value of position
     *
     * @return the value of position of text relative to anchor point
     */
    public Position getPosition()
    {
        return position;
    }

    /**
     * Set the value of position
     *
     * @param position new value of position
     */
    public void setPosition(Position position)
    {
        this.position = position;
        fireStateChanged();
    }
    
    /**
     * Get the value of decoration
     *
     * @return the value of decoration
     */
    public Decoration getDecoration()
    {
        return decoration;
    }

    /**
     * Set the value of decoration
     *
     * @param decoration new value of decoration
     */
    public void setDecoration(Decoration decoration)
    {
        this.decoration = decoration;
        fireStateChanged();
    }

    /**
     * Get the value of opacity
     *
     * @return the value of opacity
     */
    public float getOpacity()
    {
        return opacity;
    }

    /**
     * Set the value of opacity
     *
     * @param opacity new value of opacity
     */
    public void setOpacity(float opacity)
    {
        this.opacity = opacity;
        fireStateChanged();
    }

    /**
     * Get the value of shift
     *
     * @return the value of shift
     */
    public float getShift()
    {
        return shift;
    }

    /**
     * Set the value of shift
     *
     * @param shift new value of shift
     */
    public void setShift(float shift)
    {
        this.shift = shift;
        fireStateChanged();
    }

   
    
     /**
     * A flag indicating if state change will be forwarded to listeners.
     */
    protected boolean active = true;

    /**
     * Returns whether state change will be forwarded to listeners.
     * <p>
     * @return
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * Set value of
     * <code>active</code> and fire fireStateChanged().
     * <p/>
     * @param active
     */
    public void setActive(boolean active)
    {
        this.active = active;
        fireStateChanged();
    }

    public void setActiveValue(boolean active)
    {
        this.active = active;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    public void fireStateChanged()
    {
        if(active){
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < changeListenerList.size(); i++) {
                changeListenerList.get(i).stateChanged(e);
            }
        }
    }

}
