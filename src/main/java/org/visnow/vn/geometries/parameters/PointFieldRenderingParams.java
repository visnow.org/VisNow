/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import javax.swing.event.ChangeEvent;
import org.jogamp.vecmath.Color3f;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.PointField;
import org.visnow.jscic.dataarrays.ByteDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.geometries.gui.PointFieldRenderingGUI;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * <p>
 * Parameters controlling the selection criteria (by index, by named subset or by component values range),
 * amount of displayed (sampled) points and geometric point size
 *
 * Contains reference to the GUI
 */
public class PointFieldRenderingParams
{
    public enum Selection
    {
        INDEX    ("index"),
        SUBSET   ("subset"),
        COMPONENT("component");

         private final String name;

        private Selection(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    };

    protected PointField        inField;

    protected long              nFieldNodes       = 1000;
    protected double            displayRatio      = 1;
    protected long              nNodes            = 0;

    protected ComponentFeature  byteComponents    = new ComponentFeature();

    protected Selection         selectBy          = Selection.INDEX;
    protected long              nPreselectedNodes = 1000;
    protected long              from              = 0;
    protected long              to                = 100;
    protected boolean[]         subsetActive      = null;
    protected ComponentSubrange valRange          = new ComponentSubrange();

    protected Selection         lastSelectBy      = Selection.INDEX;
    protected long              lastFrom          = 0;
    protected long              lastTo            = 100;
    protected int               lastSubsetComponent = 0;
    protected boolean[]         lastSubsetActive  = null;
    protected int               lastComponent     = 0;
    protected float             lastLow           = 0;
    protected float             lastUp            = 100;
    protected long              lastPreselectedNodes = 1000;

    protected int[]             reverseIndices    = new int[256];
    protected String[]          subsetNames       = null;
    protected long[]            subsetSizes       = new long[0];
    protected boolean           outline           = false;

    protected String            name              = "";

    protected OpenAppearance    appearance        = new OpenAppearance();
    protected Color             color             = null;

    protected boolean           active            = true;
    protected boolean           silent            = false;

    protected PointFieldRenderingGUI gui          = null;


    /**
     * Creates a new instance of RenderingParams
     */
    public PointFieldRenderingParams()
    {
        valRange.addChangeListener((ChangeEvent e) -> {
            if (!silent)
                updatePointSelection();
            fireStateChanged(RenderEvent.GEOMETRY);
        });
        byteComponents.addChangeListener((ChangeEvent e) -> {
            if (!silent)
                setSubsetComponent();
        });
        appearance.getPointAttributes().setPointSize(1);
    }

    public PointField getInField()
    {
        return inField;
    }

    /**
     * sets data of  the displayed field
     * @param inField a point field to be displayed
     */
    public void setField(Field inField)
    {
        this.inField = (PointField)inField;
        valRange.setContainer(inField);
        nFieldNodes = inField.getNNodes();
        DataContainerSchema availableByteComponents = new DataContainerSchema("available");
        if (inField.getComponent("subsets") != null && inField.getComponent("subsets").
                                                       getType() == DataArrayType.FIELD_DATA_BYTE)
            availableByteComponents.addDataArraySchema(inField.getComponent("subsets").getSchema());
        for (DataArray component : inField.getComponents()) {
            if (component.getType() == DataArrayType.FIELD_DATA_BYTE &&
                !component.getName().equalsIgnoreCase("subsets"))
                availableByteComponents.addDataArraySchema(component.getSchema());
        }
        nNodes = (long)Math.min(1e6, nFieldNodes);
        byteComponents.setContainerSchema(availableByteComponents);
        setSubsetComponent();
        if (subsetNames != null)
            setSelectBy(Selection.SUBSET);
        else {
            setSelectBy(Selection.INDEX);
            subsetNames  = null;
            subsetSizes  = null;
            subsetActive = null;
        }
        nPreselectedNodes = inField.getNNodes();
        if (gui != null) {
            gui.updateDataValuesFromParams();
        }
    }

    private void saveSelection()
    {
        lastSelectBy         = selectBy;
        lastFrom             = from;
        lastTo               = to;
        lastSubsetComponent  = byteComponents.getComponentIndex();
        if (subsetActive != null)
            lastSubsetActive  = Arrays.copyOf(subsetActive, subsetActive.length);
        else
            lastSubsetActive  = null;
        lastComponent        = valRange.getComponentIndex();
        lastLow              = valRange.getLow();
        lastUp               = valRange.getUp();
        lastPreselectedNodes = nPreselectedNodes;
        silent = false;
    }

    private void restoreSelection()
    {
        silent = true;
        selectBy      = lastSelectBy;
        from          = lastFrom;
        to            = lastTo;
        byteComponents.setComponentSchema(lastSubsetComponent);
        if (lastSubsetActive != null)
            subsetActive  = Arrays.copyOf(lastSubsetActive, subsetActive.length);
        valRange.setComponentSchema(lastComponent);
        valRange.setLowUp(lastLow, lastUp);
        nPreselectedNodes = lastPreselectedNodes;
        silent = false;
    }

    private void updatePointSelection()
    {
        switch (selectBy) {
        case INDEX:
            nPreselectedNodes = ((to - from) * nFieldNodes) / 100;
            break;
        case SUBSET:
            nPreselectedNodes = 0;
            for (int i = 0; i < subsetActive.length; i++)
                if (subsetActive[i])
                    nPreselectedNodes += subsetSizes[i];
            break;
        case COMPONENT:
            DataArray da = inField.getComponent(valRange.getComponentName());
            int vlen = da.getVectorLength();
            nPreselectedNodes = 0;
            LargeArray data = da.getRawArray();
            float low = valRange.getLow();
            float up  = valRange.getUp();
            for (long i = 0; i < nFieldNodes; i++) {
                float val = 0;
                if (vlen == 1)
                    val = data.getFloat(i);
                else {
                    for (int j = 0; j < vlen; j++)
                        val += data.getFloat(vlen * i + j) * data.getFloat(vlen * i + j);
                    val = (float)Math.sqrt(val);
                }
                if (low <= val && val <= up)
                    nPreselectedNodes += 1;
            }
        }
        if (nPreselectedNodes > 0) {
            saveSelection();
            if (gui != null) {
                gui.setPreselectedNodes();
                gui.setWarning(false);
            }
        }
        else {
            restoreSelection();
            if (gui != null) {
                gui.updateDataValuesFromParams();
                gui.setWarning(true);
            }
        }
    }

    private void setSubsetComponent()
    {
        subsetNames  = null;
        subsetSizes  = null;
        subsetActive = null;
        Arrays.fill(reverseIndices, -1);
        DataArraySchema subsetComponentSchema = byteComponents.getComponentSchema();
        if (subsetComponentSchema == null)
            return;
        String[] sNames = new String[0];
        int nSubsets = 0;
        if (subsetComponentSchema.getUserData() != null) {
            sNames = subsetComponentSchema.getUserData();
        }
        UnsignedByteLargeArray subsets =
                inField.getComponent(subsetComponentSchema.getName()).getRawByteArray();
        long[] sizes = new long[256];
        Arrays.fill(sizes, 0L);
        for (long i = 0; i < nFieldNodes; i++)
            sizes[subsets.getInt(i)] += 1;

        for (int i = 0; i < sizes.length; i++)
            if (sizes[i] > 0)
                nSubsets += 1;
        subsetNames = new String[nSubsets];
        subsetSizes = new long[nSubsets];
        subsetActive = new boolean[nSubsets];
        Arrays.fill(subsetActive, true);
        for (int i = 0, k = 0; i < sizes.length; i++)
            if (sizes[i] > 0) {
                subsetSizes[k] = sizes[i];
                if (k < sNames.length)
                    subsetNames[k] = sNames[k];
                else
                    subsetNames[k] = "subset" + i;
                reverseIndices[i] = k;
                k += 1;
            }
        updatePointSelection();
        if (gui != null)
            gui.updateDataValuesFromParams();
        fireStateChanged(RenderEvent.GEOMETRY);
    }

/**
 * returns the data about the subset component
 * @return subset component description
 */
    public ComponentFeature getByteComponentsParam() {
        return byteComponents;
    }

/**
 * returns the the subset component
 * @return subset component
 */
    public ByteDataArray getSubsetDataArray()
    {
        String name = byteComponents.getComponentName();
        if (name == null)
            return null;
        return (ByteDataArray)inField.getComponent(name);
    }

    /**
     * Names of subsets from the data array user data or default "subset0...n"
     * @return subset names
     */
    public String[] getSubsetNames()
    {
        return subsetNames;
    }

    /**
     * Sizes of subsets
     * @return subset sizes
     */
    public long[] getSubsetSizes()
    {
        return subsetSizes;
    }

    /**
     * selection mode
     * @return selection mode
     */
    public Selection getSelectBy()
    {
        return selectBy;
    }

    /**
     * getter for indices of selected points
     * @return indices of selected points
     */
    public int[] getReverseIndices() {
        return reverseIndices;
    }

    /**
     * sets selection mode (by node index, subset data array or component value range
     * generates new point selection and fires redisplay
     * @param selectBy
     */
    public void setSelectBy(Selection selectBy)
    {
        this.selectBy = selectBy;
        updatePointSelection();
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    /**
     * getter for lower index of displayed nodes
     * @return lower index of displayed nodes
     */
    public long getFrom() {
        return from;
    }

    /**
     * getter for upper index of displayed nodes
     * @return upper index of displayed nodes
     */
    public long getTo() {
        return to;
    }

    /**
     * setter for node index range
     * @param from lower index of displayed nodes (as per cent of field size
     * @param to   upper index of displayed nodes (as per cent of field size
     * generates new point selection and fires redisplay
     */
    public void setFromTo(int from, int to) {
        this.from = from;
        this.to = to;
        updatePointSelection();
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    /**
     * getter for active subsets array
     * @return array[i] indicates if i-th set will be displayed
     */
    public boolean[] getSubsetActive()
    {
        return subsetActive;
    }

    /**
     * called by the GUI when subset selection is updated
     * generates new point selection and fires redisplay
     */
    public void updateSubsetActive()
    {
        updatePointSelection();
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    public ComponentSubrange getValRange()
    {
        return valRange;
    }

    public PointFieldRenderingGUI getGui()
    {
        return gui;
    }


    public void setGui(PointFieldRenderingGUI gui)
    {
        this.gui = gui;
        gui.updateDataValuesFromParams();
    }

    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        return s.toString();
    }

    /**
     * Getter for property color.
     * <p>
     * @return Value of property color.
     */
    public Color getColor()
    {
        if (color != null)
            return color;
        return Color.LIGHT_GRAY;
    }

    public Color3f getColor3f()
    {
        Color3f c = new Color3f();
        appearance.getColoringAttributes().getColor(c);
        return c;
    }

    /**
     *
     * @return true if the outline box is drawn
     */
    public boolean isOutline() {
        return outline;
    }

    /**
     * setter for outline box draw
     * fires redisplay
     * @param outline true if outline box is drawn
     */
    public void setOutline(boolean outline) {
        this.outline = outline;
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    /**
     * getter for number of requested nodes
     * @return requested number of displayed nodes
     */
    public long getNNodes() {
        return nNodes;
    }

    /**
     * getter for number of nodes satisfying selection criteria
     * @return  number of nodes satisfying selection criteria
     */
    public long getNPreselectedNodes()
    {
        return nPreselectedNodes;
    }

    /**
     * setting number of nodes selected by the user
     * @param nNodes
     */
    public void setNDisplayedNodes(long nNodes)
    {
        this.nNodes = nNodes;
        fireStateChanged(RenderEvent.GEOMETRY);
    }


    public OpenAppearance getAppearance()
    {
        return appearance;
    }

    /**
     * Setter for property color.
     * <p>
     * @param color New value of property color.
     */

    public void setColor(Color color)
    {
        float[] fC = new float[3];
        color.getColorComponents(fC);
        this.color = color;
        Color3f color3f = new Color3f(fC);
        appearance.getColoringAttributes().setColor(color3f);
        fireStateChanged(RenderEvent.COLORS);
    }

    /**
     * getter of the point size (in pixels)
     * @return point size (in pixels)
     */
    public float getPointSize()
    {
        return appearance.getPointAttributes().getPointSize();
    }


    public void setPointSize(float pointSize)
    {
        appearance.getLineAttributes().setLineWidth(pointSize / 10f);
        appearance.getPointAttributes().setPointSize(pointSize / 10f);
        fireStateChanged(RenderEvent.APPEARANCE);
    }

    /**
     * Utility field holding list of RenderEventListeners.
     */
    private final ArrayList<RenderEventListener> renderEventListenerList
        = new ArrayList<>();

    /**
     * Registers RenderEventListener to receive events.
     * <p>
     * @param listener The listener to register.
     */

    public synchronized void addRenderEventListener(RenderEventListener listener)
    {
        if (!renderEventListenerList.contains(listener))
            renderEventListenerList.add(listener);
    }

    /**
     * Removes RenderEventListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */

    public synchronized void removeRenderEventListener(RenderEventListener listener)
    {
        renderEventListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param change
     */
    public final void fireStateChanged(int change)
    {
        if (!active)
            return;
        RenderEvent e = new RenderEvent(this, change);
        try {
            for (RenderEventListener renderEventListener : renderEventListenerList)
                renderEventListener.renderExtentChanged(e);
        } catch (ConcurrentModificationException exc) {
        }
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
            res.add("pointSize: " + appearance.getLineAttributes().getLineWidth());

        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++)
           r[i] = res.get(i);
        return r;
    }

    public void restoreValuesFrom(String[] saved)
    {
        for (String line : saved)
            if (line.trim().startsWith("pointSize:")) {
                try {
                    appearance.getLineAttributes().setLineWidth(Float.parseFloat(line.trim().split(" *:* +")[1]));
                }catch (Exception e) {}
            }
        if (gui != null)
            gui.updateDataValuesFromParams();
    }

}
