/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColorComponentParams implements Cloneable
{
    private static final Logger LOGGER = Logger.getLogger(ColorComponentParams.class);
    private ComponentSubrange colorRange = new ComponentSubrange();
    private boolean minmaxChanged = false;
    private float colorComponentMin = 0;
    private float colorComponentMax = 1;
    private boolean wrap = false;
    private RenderEventListener listener = null;
    private boolean active = true;
    protected boolean lastActive = true;
    private boolean adjusting = false;
    
    public ColorComponentParams()
    {
        colorRange.addNull();
        colorRange.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }
    
    public ColorComponentParams(boolean usePseudocomponents)
    {
        this();
        colorRange.setPseudoComponentsAllowed(usePseudocomponents);
    }
    
    @Override
    public ColorComponentParams clone()
    {
        ColorComponentParams cloned = new ColorComponentParams(colorRange.isPseudoComponentsAllowed());
        cloned.setActive(false);
        cloned.colorComponentMin = colorComponentMin;
        cloned.colorComponentMax = colorComponentMax;
        cloned.wrap = wrap;
        cloned.colorRange = colorRange.clone();
        cloned.setActive(true);
        return cloned;
    }
    
    public void copyValuesFrom(ColorComponentParams src)
    {
        setActive(false);
        colorComponentMin = src.colorComponentMin;
        colorComponentMax = src.colorComponentMax;
        wrap = src.wrap;
        colorRange.setActive(false);
        colorRange.copyValuesFrom(src.colorRange);
        colorRange.restoreActive();
        setActive(true);
    }
    
    public String[] valuesToStringArray()
    {
        Vector<String> res = new Vector<>();
        String[] c = colorRange.valuesToStringArray();
        res.add("component range {");
        for (String c1 : c)
            res.add("    " + c1);
        res.add("}");
        if (minmaxChanged)
            res.add(String.format("range: %6.4f %6.4f", colorComponentMin, colorComponentMax));
        if (wrap)
            res.add("wrap: " + (wrap ? "true" : "false"));
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++) 
           r[i] = res.get(i);
        return r;
    }
    
    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring ColorComponentParams");
        setActive(false);
        for (String s : saved) 
            if (s.startsWith("range")) {
                try {
                    colorComponentMin = Float.parseFloat(s.split(" *:* +")[1]);
                    colorComponentMax = Float.parseFloat(s.split(" *:* +")[2]);
                } catch (Exception e) {}
                break;
            }
        for (String s : saved) 
            if (s.startsWith("wrap")) {
                wrap = s.contains("true");
                break;
            }
        colorRange.setActive(false);
        for (int i = 0; i < saved.length; i++) {
            if (saved[i].trim().equals("component range {")) {
                colorRange.restoreFromStringArray(StringUtils.findBlock(saved, i));
                break;
            }
        }
        colorRange.restoreActive();
        setActive(true);
    }
    
    public void setDataContainer (DataContainer container)
    {
        if (container == null)
            return;
        colorRange.setContainer(container);
        if (VisNow.get() != null)
            colorRange.setContinuousUpdate(container.getNElements() < 
                                           10000 * Math.pow(4, VisNow.get().getPerformance()));
    }
    
    public void setDataContainerSchema (DataContainerSchema containerSchema)
    {
        colorRange.setContainerSchema(containerSchema);
    }

    public void setDataComponent(int dataComponent, float dataMin, float dataMax)
    {
        colorRange.setComponentSchema(dataComponent);
        colorRange.setLowUp(dataMin, dataMax);
        fireStateChanged();
    }
    
    public void setDataComponent(int dataComponent)
    {
        colorRange.setComponentSchema(dataComponent);
        fireStateChanged();
    }

    public ComponentSubrange getColorRange()
    {
        return colorRange;
    }
    
    public DataArraySchema getDataComponentSchema()
    {
        return colorRange.getComponentSchema();
    }
    
    public int getDataComponentIndex()
    {
        return colorRange.getComponentIndex();
    }
    
    public String getDataComponentName()
    {
        return colorRange.getComponentName();
    }
    
    public float getDataMin()
    {
        return colorRange.getLow();
    }
    
    public float getDataMax()
    {
        return colorRange.getUp();
    }

    public boolean isWrap()
    {
        return wrap;
    }

    public void setWrap(boolean wrap)
    {
        this.wrap = wrap;
        fireStateChanged();
    }
    
    public void setListener(RenderEventListener listener)
    {
        this.listener = listener;
    }
    
    public void setPreferredNull()
    {
        colorRange.setPrefereNull(true);
    }

    public void usePseudocomponents()
    {
        colorRange.setPseudoComponentsAllowed(true);
    }
    
    public void setActive(boolean active)
    {
        lastActive = active;
        this.active = active;
    }
    
    public void restoreActive()
    {
        active = lastActive;
    }

    public void fireStateChanged()
    {
        if (active) 
            listener.renderExtentChanged(new RenderEvent(this, RenderEvent.COLORS, colorRange.isAdjusting()));
    }


    public float getColorComponentMin()
    {
        return colorComponentMin;
    }

    public void setColorComponentMin(float colorComponentMin)
    {
        minmaxChanged = true;
        this.colorComponentMin = colorComponentMin;
    }

    public float getColorComponentMax()
    {
        return colorComponentMax;
    }

    public void setColorComponentMax(float colorComponentMax)
    {
        this.colorComponentMax = colorComponentMax;
    }
    
    public void addRangeChangeListener(ChangeListener listener)
    {
        colorRange.addChangeListener(listener);
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }
    
    public boolean isAdjusting()
    {
        return adjusting || colorRange.isAdjusting();
    }

    public boolean isUnmodified()
    {
        return colorRange.isUnmodified() && !minmaxChanged;
    }

}
