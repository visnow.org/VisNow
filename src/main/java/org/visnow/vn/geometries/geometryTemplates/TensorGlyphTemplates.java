/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.geometryTemplates;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TensorGlyphTemplates
{

    public static final int MAXLOD = 20;
    protected static boolean ready = false;
    protected static Glyph[][] glyphs;
    protected static String[] glyphNames;

    @SuppressWarnings("unchecked")
    private static void createTemplates()
    {
        if (!ready) {
            try {
                Class templatesClass = TensorTemplates.class;
                Class[] templates = templatesClass.getClasses();
                glyphs = new Glyph[templates.length][MAXLOD];
                glyphNames = new String[templates.length];
                for (int i = 0; i < templates.length; i++) {
                    for (int j = 0; j < MAXLOD; j++)
                        glyphs[i][j] = (Glyph) (templates[i].getConstructor(new Class[]{
                            Integer.class
                        }).newInstance(new Object[]{
                            new Integer(j + 2)
                        }));
                    glyphNames[i] = glyphs[i][0].getName();
                }
            } catch (Exception e) {
                System.out.println("cannot find glyph templates");
                return;
            }
            ready = true;
        }
    }

    public static Glyph[][] getGlyphs()
    {
        createTemplates();
        return glyphs;
    }

    public static Glyph glyph(int type, int lod)
    {
        createTemplates();
        if (lod < 0 || lod >= MAXLOD)
            return null;
        return glyphs[type][lod];
    }

    public static String[] getGlyphNames()
    {
        createTemplates();
        return glyphNames;
    }

    private TensorGlyphTemplates()
    {
    }

}
