/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.geometryTemplates;

import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenColoringAttributes;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author know
 */


public class SimplePointGlyph extends OpenBranchGroup
{
    
    protected float[] verts = new float[18];
    protected IndexedLineStripArray glyph = new IndexedLineStripArray(6, GeometryArray.COORDINATES, 6, new int[]{2, 2, 2});
    protected int[] cInd = new int[]{0, 1, 2, 3, 4, 5};
    protected OpenBranchGroup geometry = new OpenBranchGroup();
    protected OpenLineAttributes glyphLineAttr = new OpenLineAttributes();
    protected OpenColoringAttributes glyphColorAttr = new OpenColoringAttributes();
    protected OpenAppearance glyphApp = new OpenAppearance();
    protected OpenShape3D glyphShape = new OpenShape3D();
    protected int[] dims = null;
    protected int nDims = -1;
    protected float[] position = {0, 0, 0};
    protected float[][] af = null;
    protected float[] coords = null;
    protected float[] center = {0, 0, 0};
    protected float r = .1f;

    public SimplePointGlyph()
    {
        glyph.setCoordinateIndices(0, cInd);
        
        glyphLineAttr.setLineAntialiasingEnable(true);
        glyphLineAttr.setLineWidth(1.5f);
        glyphApp.setLineAttributes(glyphLineAttr);
        glyphColorAttr.setColor(0, 1, 0);
        glyphApp.setColoringAttributes(glyphColorAttr);
        glyph.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyph.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyph.setCoordinates(0, verts);
        glyphShape.setAppearance(glyphApp);
        updateCoords();
        glyphShape.addGeometry(glyph);
    }
    
    public void setFieldGeometry(RegularField inField)
    {
        dims = inField.getDims();
        nDims = dims.length;
        af = inField.getAffine();
        coords = inField.hasCoords() ? inField.getCurrentCoords().getData() : null;
        float[][] xt = inField.getExtents();
        r = 0;
        Arrays.fill(center, 0);
        for (int i = 0; i < 3; i++) {
            r += (xt[1][i] - xt[0][i]) * (xt[1][i] - xt[0][i]);
            center[i] = .5f * (xt[1][i] + xt[0][i]);
        }
        r = (float)Math.sqrt(r) /150;
    }
    
    public void updatePosition(float[] position)
    {
        switch (nDims) {
        case 1:
            center = computeAtPoint(position[0], 0, 0);
            break;
        case 2:
            center = computeAtPoint(position[0], position[1], 0);
            break;
        default:
            center = computeAtPoint(position[0], position[1], position[2]);
        }
        updateCoords();
    }
    
    
    public void updateCenter(float[] center)
    {
        if (center == null || center.length != 3)
            return;
        this.center = center;
        updateCoords();
    }
        
    private void updateCoords()
    {
        for (int i = 0; i < 3; i++) 
            verts[i]      = verts[i + 3] = 
            verts[i + 6]  = verts[i + 9] = 
            verts[i + 12] = verts[i + 15] = center[i];
        verts[0] -= r;
        verts[3] += r;
        verts[7] -= r;
        verts[10] += r;
        verts[14] -= r;
        verts[17] += r;
    }

    protected float[] computeAtPoint(float u, float v, float w)
    {
        float[] out = new float[3];
        if (coords == null) {
            System.arraycopy(af[3], 0, out, 0, 3);
            for (int i = 0; i < 3; i++)
                out[i] += u * af[0][i] + v * af[1][i] + w * af[2][i];
            return out;
        }
            
        int n0 = dims[0];
        int n1 = dims[0] * dims[1];
        int inexact = 0;
        u = Math.max(0, Math.min(dims[0] - 1, u));
        v = Math.max(0, Math.min(dims[1] - 1, v));
        w = Math.max(0, Math.min(dims[2] - 1, w));
        int i = (int) u;
        u -= i;
        if (u != 0)
            inexact += 1;
        int j = (int) v;
        v -= j;
        if (v != 0)
            inexact += 2;
        int k = (int) w;
        w -= k;
        if (w != 0)
            inexact += 4;
        int m = (dims[1] * k + j) * dims[0] + i;    
        switch (inexact) {
        case 0:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = coords[3 * m + cmp];
            break;
        case 1:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = u *       coords[3 * (m + 1) + cmp] +
                           (1 - u) * coords[3 *  m +      cmp];
            break;
        case 2:
                for (int cmp = 0; cmp < 3; cmp++) 
                    out[cmp] = v *       coords[3 * (m + n0) + cmp] +
                               (1 - v) * coords[3 *  m +       cmp];
            break;
        case 3:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = v *       (u *      coords[3 * (m + n0 + 1) + cmp] +
                                     (1 - u) * coords[3 * (m + n0) +     cmp]) + 
                           (1 - v) * (u *      coords[3 * (m +      1) + cmp] +
                                     (1 - u) * coords[3 *  m +           cmp]);
            break;
        case 4:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = w *       coords[3 * (m + n1) + cmp] +
                           (1 - w) * coords[3 *  m +       cmp];
            break;
        case 5:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = w *       (u *      coords[3 * (m + n1 + 1) + cmp] +
                                     (1 - u) * coords[3 * (m + n1) +     cmp]) + 
                           (1 - w) * (u *      coords[3 * (m +      1) + cmp] +
                                     (1 - u) * coords[3 *  m +           cmp]);
            break;
        case 6:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = w *       (v *      coords[3 * (m + n1 + n0) + cmp] +
                                     (1 - v) * coords[3 * (m + n1) +      cmp]) + 
                           (1 - w) * (v *      coords[3 * (m +      n0) + cmp] +
                                     (1 - v) * coords[3 *  m +            cmp]);
            break;
        case 7:
            for (int cmp = 0; cmp < 3; cmp++) 
                out[cmp] = w *       (v *      (u *      coords[3 * (m + n1 + n0 + 1) + cmp] +
                                               (1 - u) * coords[3 * (m + n1 + n0) +     cmp]) +
                                     (1 - v) * (u *      coords[3 * (m + n1 +      1) + cmp] +
                                               (1 - u) * coords[3 * (m + n1) +          cmp])) +
                           (1 - w) * (v *      (u *      coords[3 * (m +      n0 + 1) + cmp] +
                                               (1 - u) * coords[3 * (m +      n0) +     cmp]) +
                                     (1 - v) * (u *      coords[3 * (m +           1) + cmp] +
                                               (1 - u) * coords[3 *  m +                cmp]));
            break;
        }  
        return out;
    }
}
