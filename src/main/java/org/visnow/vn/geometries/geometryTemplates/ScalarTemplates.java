/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.geometryTemplates;

import java.util.Arrays;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ScalarTemplates
{

    //add new glyphs here
    public static class DiamondTemplate extends Glyph
    {
        public DiamondTemplate(Integer lod)
        {
            name = "diamond";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 1;
            nverts = 6;
            ninds = 13;
            strips = new int[]{13};
            verts = new float[] {1, 0, 0, -1,  0,  0,
                                 0, 1, 0,  0, -1,  0,
                                 0, 0, 1,  0,  0, -1};
            pntsIndex = new int[] {0, 2, 1, 3, 0, 4, 2, 5, 3, 4, 1, 5, 0};
            clrsIndex = new int[ninds];
            for (int i = 0; i < clrsIndex.length; i++)
                clrsIndex[i] = 0;
        }
    }

    public static class BoxTemplate extends Glyph
    {
        public BoxTemplate(Integer lod)
        {
            name = "box";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 12;
            nverts = 8;
            ninds = 24;
            strips = new int[] {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
            verts = new float[] {-1, -1, -1, 1, -1, -1, -1, 1, -1, 1, 1, -1,
                                 -1, -1,  1, 1, -1,  1, -1, 1,  1, 1, 1,  1};
            int[] pInd = {0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7};
            pntsIndex = pInd;
            clrsIndex = new int[ninds];
            for (int i = 0; i < clrsIndex.length; i++)
                clrsIndex[i] = 0;
        }
    }

    public static class SnowFlakeTemplate extends Glyph
    {
        public SnowFlakeTemplate(Integer lod)
        {
            name = "snowflake";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 3;
            nverts = 6;
            ninds = 6;
            strips = new int[]{2, 2, 2};

            verts = new float[] {1,   0,     0, -1,   0,     0,
                                  .5f, .81f, 0, -.5f, -.81f, 0,
                                 -.5f, .81f, 0,  .5f, -.81f, 0};
            int[] pInd = {0, 1, 2, 3, 4, 5};
            pntsIndex = pInd;
            clrsIndex = new int[ninds];
            for (int i = 0; i < clrsIndex.length; i++)
                clrsIndex[i] = 0;
        }
    }

    public static class RaindropTemplate extends Glyph
    {

        /**
         * Creates a new instance of SphereTemplate
         */
        public RaindropTemplate(Integer lod)
        {
            name = "raindrop";
            type = TRIANGLE_STRIPS;
            refinable = true;
            nstrips = 1;
            nverts = 10;
            ninds = 10;
            strips = new int[]{10};
            float s = (float) sqrt(.75);
            verts = new float[]{0, 1.4f, 0,
                                s, .5f, 0, -s, .5f, 0,
                                1, 0, 0, -1, 0, 0,
                                s, -.5f, 0, -s, -.5f, 0,
                                .5f, -s, 0, -.5f, -s, 0,
                                0, -1, 0};
            normals = new float[]{0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
                                  0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1};
            pntsIndex = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            clrsIndex = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        }
    }

    public static class SphereTemplate extends Glyph
    {
        public SphereTemplate(Integer lod)
        {
            name = "sphere";
            type = TRIANGLE_STRIPS;
            refinable = true;
            nstrips = 4 * (lod - 1);
            nverts = 4 * lod * lod;
            ninds = 8 * (lod - 1) * lod;
            strips = new int[4 * (lod - 1)];

            int i, j, k, k0, l;
            float t0, t1, t2, r;

            float[] p0 = new float[]{0, 0, 1};
            float[] p1 = new float[]{1, 0, 0};
            float[] p2 = new float[]{0, 1, 0};

            float[] q0 = new float[]{0, 1, 0};
            float[] q1 = new float[]{0, 0, 1};
            float[] q2 = new float[]{-1, 0, 0};
            verts = new float[12 * lod * lod];
            normals = new float[12 * lod * lod];
            pntsIndex = new int[8 * (lod - 1) * lod];
            clrsIndex = new int[8 * (lod - 1) * lod];

            for (i = 0; i < 4 * (lod - 1); i++)
                strips[i] = 2 * lod;

            k = 0;
            k0 = k;
            for (i = 0; i < lod; i++) {
                for (j = 0; j < lod - i; j++) {
                    t0 = (float) i / (lod - 1);
                    if (lod - i - 1 > 0)
                        t2 = (1.f - t0) * (float) j / (lod - i - 1);
                    else
                        t2 = 0.f;
                    t1 = 1.f - t0 - t2;
                    r = 0.f;
                    for (l = 0; l < 3; l++) {
                        verts[3 * k + l] = t0 * p0[l] + t1 * p1[l] + t2 * p2[l];
                        r += verts[3 * k + l] * verts[3 * k + l];
                    }
                    r = (float) sqrt(r);
                    for (l = 0; l < 3; l++)
                        verts[3 * k + l] /= r;
                    k += 1;
                }
                for (j = 0; j < i; j++) {
                    t0 = (float) (lod - 1 - i) / (lod - 1);
                    t2 = (1.f - t0) * (float) (j + 1) / i;
                    t1 = 1.f - t0 - t2;
                    r = 0.f;
                    for (l = 0; l < 3; l++) {
                        verts[3 * k + l] = t0 * q0[l] + t1 * q1[l] + t2 * q2[l];
                        r += verts[3 * k + l] * verts[3 * k + l];
                    }
                    r = (float) sqrt(r);
                    for (l = 0; l < 3; l++)
                        verts[3 * k + l] /= r;
                    k += 1;
                }
            }

            for (i = 0; i < k; i++) {
                verts[3 * (i + k)] =          verts[3 * i];
                verts[3 * (i + k) + 1] =     -verts[3 * i + 2];
                verts[3 * (i + k) + 2] =      verts[3 * i + 1];
                verts[3 * (i + 2 * k)] =      verts[3 * i];
                verts[3 * (i + 2 * k) + 1] = -verts[3 * i + 1];
                verts[3 * (i + 2 * k) + 2] = -verts[3 * i + 2];
                verts[3 * (i + 3 * k)] =      verts[3 * i];
                verts[3 * (i + 3 * k) + 1] =  verts[3 * i + 2];
                verts[3 * (i + 3 * k) + 2] = -verts[3 * i + 1];
            }
            for (i = 0; i < 3 * nverts; i++)
                normals[i] = verts[i];

            l = 0;
            k = lod * lod;
            k0 = 2 * lod * (lod - 1);
            for (i = 0; i < lod - 1; i++)
                for (j = 0; j < lod; j++) {
                    pntsIndex[l] = j * lod + i;
                    pntsIndex[l + 1] = j * lod + i + 1;
                    pntsIndex[k0 + l] = k + j * lod + i;
                    pntsIndex[k0 + l + 1] = k + j * lod + i + 1;
                    pntsIndex[2 * k0 + l] = 2 * k + j * lod + i;
                    pntsIndex[2 * k0 + l + 1] = 2 * k + j * lod + i + 1;
                    pntsIndex[3 * k0 + l] = 3 * k + j * lod + i;
                    pntsIndex[3 * k0 + l + 1] = 3 * k + j * lod + i + 1;
                    l += 2;
                }

            for (i = 0; i < l; i++)
                clrsIndex[i] = 0;
        }

        public SphereTemplate()
        {
            this(3);
        }
    }

    public static class StarTemplate extends Glyph
    {
        static float[] p0 = new float[]{0, 0, 1};
        static float[] p1 = new float[]{1, 0, 0};
        static float[] p2 = new float[]{0, 1, 0};

        static float[] q0 = new float[]{0, 1, 0};
        static float[] q1 = new float[]{0, 0, 1};
        static float[] q2 = new float[]{-1, 0, 0};
        /**
         * Creates a new instance of StarTemplate
         * @param lod level of detail
         */
        public StarTemplate(Integer lod)
        {
            name = "star";
            type = LINE_STRIPS;
            refinable = true;
            nstrips = 2 * lod * lod;
            nverts  =  ninds  = 4 * lod * lod;
            strips  = new int[nstrips];
            verts =     new float[3*nverts];
            pntsIndex = new int[ninds];
            clrsIndex = new int[ninds];

            int k = 0;
            for (int i = 0; i < lod; i++) {
                for (int j = 0; j < lod - i; j++) {
                    float t0 = (float) i / (lod - 1);
                    float t2 = lod - i - 1 > 0 ?
                               (1.f - t0) * (float) j / (lod - i - 1):
                                0.f;
                    float t1 = 1.f - t0 - t2;
                    float r = 0.f;
                    for (int l = 0; l < 3; l++) {
                        verts[3 * k + l] = t0 * p0[l] + t1 * p1[l] + t2 * p2[l];
                        r += verts[3 * k + l] * verts[3 * k + l];
                    }
                    r = (float) sqrt(r);
                    for (int l = 0; l < 3; l++)
                        verts[3 * k + l] /= r;
                    k += 1;
                }
                for (int j = 0; j < i; j++) {
                    float t0 = (float) (lod - 1 - i) / (lod - 1);
                    float t2 = (1.f - t0) * (float) (j + 1) / i;
                    float t1 = 1.f - t0 - t2;
                    float r = 0.f;
                    for (int l = 0; l < 3; l++) {
                        verts[3 * k + l] = t0 * q0[l] + t1 * q1[l] + t2 * q2[l];
                        r += verts[3 * k + l] * verts[3 * k + l];
                    }
                    r = (float) sqrt(r);
                    for (int l = 0; l < 3; l++)
                        verts[3 * k + l] /= r;
                    k += 1;
                }
            }

            for (int i = 0; i < k; i++) {
                verts[3 * (i + k)] =          verts[3 * i];
                verts[3 * (i + k) + 1] =     -verts[3 * i + 2];
                verts[3 * (i + k) + 2] =      verts[3 * i + 1];
            }
            for (int i = 0; i < 2 * k; i++) {
                verts[3 * (i + 2 * k)] =     -verts[3 * i];
                verts[3 * (i + 2 * k) + 1] = -verts[3 * i + 1];
                verts[3 * (i + 2 * k) + 2] = -verts[3 * i + 2];
            }
            Arrays.fill(strips, 2);
            Arrays.fill(clrsIndex, 0);
            for (int i = 0; i < nstrips; i++)
                strips[i] = 2;
            for (int i = 0; i < 2 * k; i++) {
                pntsIndex[2 * i] = i;
                pntsIndex[2 * i + 1] = i + 2 * k;
            }
        }

        public StarTemplate()
        {
            this(3);
        }
    }

}
