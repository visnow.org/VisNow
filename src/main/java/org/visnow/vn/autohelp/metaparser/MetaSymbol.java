/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.autohelp.metaparser;

import java.util.HashMap;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MetaSymbol extends Meta
{

    public MetaSymbol(String template, HashMap<String, String> data) throws PreprocessException
    {
        super(template, data);
        preprocess();
    }

    @Override
    protected String parseTemplate()
    {
        if (data == null || data.isEmpty())
            return "";

        String key = template.substring(SYMBOL_LENGTH, template.length() - SYMBOL_LENGTH);
        key = key.trim();
        Object value = data.get(key);
        if (value == null)
            return "";
        //return "[N/A]";
        //return template;

        return value.toString();
    }

    @Override
    protected void preprocess() throws PreprocessException
    {
        if (!(template.startsWith(SYMBOL_OPEN) && template.endsWith(SYMBOL_CLOSE)))
            throw new PreprocessException(-1);
    }

    @Override
    protected int getTemplateLegth()
    {
        return template.length();
    }

}
