/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.autohelp.metaparser;

import java.util.HashMap;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class Meta
{

    public static final int SYMBOL_LENGTH = 2;
    public static final String BLOCK_OPEN = "<%";
    public static final String BLOCK_CLOSE = "%>";
    public static final String BLOCK_TERMINATE = "end";

    public static final String SYMBOL_OPEN = "<$";
    public static final String SYMBOL_CLOSE = "$>";

    protected String template = null;
    protected HashMap<String, String> data;

    public Meta() throws PreprocessException
    {
        this(null, null);
    }

    public Meta(String template, HashMap<String, String> data) throws PreprocessException
    {
        this.template = cleanComments(cleanBlockSeparators(template));
        this.data = data;
    }

    public String getTemplate()
    {
        return this.template;
    }

    public String getParsed()
    {
        if (template == null)
            return null;

        return parseTemplate();
    }

    @Override
    public String toString()
    {
        return template;
    }

    protected abstract void preprocess() throws PreprocessException;

    protected abstract String parseTemplate();

    protected abstract int getTemplateLegth();

    private String cleanBlockSeparators(String str)
    {
        if (!str.contains(BLOCK_CLOSE))
            return str;
        String out = "";
        String[] tmp = str.split(BLOCK_CLOSE);
        for (int i = 0; i < tmp.length; i++) {
            out += tmp[i].trim();
            if (i < tmp.length - 1)
                out += " " + BLOCK_CLOSE;
        }
        if (str.trim().endsWith(BLOCK_CLOSE))
            out += " " + BLOCK_CLOSE;

        tmp = out.split(BLOCK_OPEN);
        out = "";
        for (int i = 0; i < tmp.length; i++) {
            //System.out.println("tmp["+i+"]="+tmp[i]);
            out += trimLastTabsAndSpaces(tmp[i]);
            //System.out.println("trm["+i+"]="+trimFirstTabsAndSpaces(tmp[i]));
            if (i < tmp.length - 1)
                out += BLOCK_OPEN;
        }

        //System.out.println(out);
        return out;
    }

    private String cleanComments(String str)
    {
        String COMMENT_OPEN = "<!--";
        String COMMENT_CLOSE = "-->";

        if (!str.contains(COMMENT_OPEN))
            return str;
        String out = "";

        for (int i = 0; i < str.length() - COMMENT_CLOSE.length(); i++) {
            if (str.substring(i, i + COMMENT_OPEN.length()).equals(COMMENT_OPEN)) {
                //found COMMENT_OPEN
                int startContent = i;

                //look for close
                int j = i + COMMENT_OPEN.length();
                boolean foundClose = false;
                while (j < str.length() - COMMENT_CLOSE.length() + 1) {
                    if (str.substring(j, j + COMMENT_CLOSE.length()).equals(COMMENT_CLOSE)) {
                        foundClose = true;
                        break;
                    }
                    j++;
                }
                if (!foundClose) {
                    return str;
                }
                //found COMMENT_CLOSE at j
                int endContent = j + COMMENT_CLOSE.length();

                out = str.substring(0, startContent) + str.substring(endContent);
            }
        }

        return out;
    }

    private String trimFirstTabsAndSpaces(String str)
    {
        String out = new String(str);
        while (out.startsWith("\t") || out.startsWith(" ")) {
            out = out.substring(1);
        }
        return out;
    }

    private String trimLastTabsAndSpaces(String str)
    {
        String out = new String(str);
        while (out.endsWith("\t") || out.endsWith(" ")) {
            out = out.substring(0, out.length() - 1);
        }
        return out;
    }

}
