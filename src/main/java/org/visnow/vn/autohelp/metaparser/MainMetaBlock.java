/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.autohelp.metaparser;

import java.util.HashMap;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MainMetaBlock extends MetaBlock
{

    public MainMetaBlock(String template, HashMap<String, String> data) throws PreprocessException
    {
        super(null, null, template, data);
    }

    @Override
    protected String parseTemplate()
    {
        if (contentMeta.isEmpty() || contentAddress.size() != contentMeta.size())
            return template;

        String out = "";
        int N = contentMeta.size();
        int start, end, lastEnd = 0;
        Meta meta;
        String tmp;
        for (int i = 0; i < N; i++) {
            start = contentAddress.get(i);
            meta = contentMeta.get(i);
            end = start + meta.getTemplateLegth();
            tmp = template.substring(lastEnd, start);

            //if(trimLeadingSpaces(tmp).startsWith("\n")) {
            //    out += trimLeadingSpaces(tmp).substring(1); 
            //} else {
            out += tmp;
            //}         
            lastEnd = end;
            out += meta.parseTemplate();
        }
        out += template.substring(lastEnd);
        return out;
    }

    private static String trimLeadingSpaces(String str)
    {
        String tmp = new String(str);
        while (tmp.startsWith(" ")) {
            tmp = tmp.substring(1);
        }
        return tmp;
    }

}
