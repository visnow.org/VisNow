/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.messages;

import org.visnow.vn.engine.element.Element;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Message
{

    public final static int KILL = 1;

    public final static int NOTIFY = 2;
    public final static int READY = 3;
    public final static int ACTION = 4;
    public final static int INACTION = 5;
    public final static int DONE = 6;

    public final static int LINK_ADD = 8;
    public final static int LINK_REMOVE = 9;

    public final static int START_ACTION = 10;
    public final static int START_ACTION_IF_NOT_IN_QUEUE = 11;

    private Element sender;

    public Element getSender()
    {
        return sender;
    }

    private int type;

    public int getType()
    {
        return type;
    }

    //private Wave wave;
    //public Wave getWave() {return wave;}
    public Message(Element sender, int type)
    {
        this.sender = sender;
        this.type = type;
    }

    protected static String[] types = {
        "null",
        "kill",
        "notify",
        "ready",
        "action",
        "inaction",
        "done",
        "WRONG",
        "link_add",
        "link_remove",
        "start_action",
        "start_action_if_not_int_queue",
        "WRONG"
    };

    @Override
    public String toString()
    {
        return "(" + types[getType()] + ") message from [" + getSender() + "]";
    }

    @Override
    public boolean equals(Object obj)
    {
        Message o = (Message) obj;
        
        return o.sender.equals(sender) && o.type == type;
    }
    
    
}
