/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.element.Element;
import org.visnow.vn.engine.element.ElementState;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNSystemEngineException;
import org.visnow.vn.engine.exception.VNSystemEngineStateException;
import org.visnow.vn.engine.main.InputSaturation;
import org.visnow.vn.engine.main.OutputSaturation;
import org.visnow.vn.engine.main.Port;
import static org.visnow.vn.engine.main.Port.LINK_DATA_STATUS_ERROR;
import org.visnow.vn.engine.messages.Message;
import org.visnow.vn.lib.types.VNDataAcceptor;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Input extends Port
{
    private static final Logger LOGGER = Logger.getLogger(Input.class);

    private static boolean debug = false;

    protected InputSaturation saturation;
    protected Link saturationReasonLink = null;

    public InputSaturation getInputSaturation()
    {
        return saturation;
    }

    public Link getInputSaturationReasonLink()
    {
        return saturationReasonLink;
    }

    private void setInputSaturation(InputSaturation saturation, Link saturationReasonLink)
    {
        this.saturation = saturation;
        this.saturationReasonLink = saturationReasonLink;
        this.fireElementSaturationListeners();
    }

    protected InputEgg egg;

    @Override
    public boolean isInput()
    {
        return true;
    }

    @Override
    public Class getType()
    {
        return egg.getType();
    }

    @Override
    public String getDescription()
    {
        return egg.getDescription();
    }

    public boolean isTriggering()
    {
        return egg.isTriggering();
    }

    public boolean isNecessary()
    {
        return egg.isNecessary();
    }

    public int getMinConnections()
    {
        return egg.getMinConnections();
    }

    @Override
    public int getMaxConnections()
    {
        if (egg.getMaxConnections() == -1)
            return Integer.MAX_VALUE;
        return egg.getMaxConnections();
    }

    public Object getDefaultValue()
    {
        return egg.getDefaultValue();
    }

    protected Vector<VNData> datas;
    protected Vector<Object> values;

    public final void checkSaturation()
    {
        if (this.isNecessary()) {
            if (this.isLinked()) {
                boolean any = false;
                int noData = -1;
                for (Link link : this.getLinks()) {
                    if (link.getOutput().getOutputSaturation() == OutputSaturation.noData) {
                        noData = links.indexOf(link);
                        continue;
                    } else {
                        int status = this.getLinkDataStatus(link.getOutput());
                        if (status == LINK_DATA_STATUS_ERROR) {
                            setInputSaturation(InputSaturation.wrongData, link);
                            return;
                        }
                        any = true;
                    }                    
                }
                
                if(any)
                    setInputSaturation(InputSaturation.ok, null);
                else
                    setInputSaturation(InputSaturation.noData, links.get(noData));                
            } else {
                this.setInputSaturation(InputSaturation.notLinked, null);
            }
        } else {
            if (this.isLinked()) {
                boolean any = false;
                int noData = -1;
                for (Link link : this.getLinks()) {
                    if (link.getOutput().getOutputSaturation() == OutputSaturation.noData) {
                        noData = links.indexOf(link);
                        continue;
                    } else {
                        int status = this.getLinkDataStatus(link.getOutput());
                        if (status == LINK_DATA_STATUS_ERROR) {
                            setInputSaturation(InputSaturation.wrongData, link);
                            return;
                        }
                        any = true;
                    }                    
                }
                
                if(any)
                    setInputSaturation(InputSaturation.ok, null);
                else
                    setInputSaturation(InputSaturation.noData, links.get(noData));                
            } else {
                this.setInputSaturation(InputSaturation.ok, null);
            }
        }
    }

    public boolean addLink(Link link)
    {
        if (debug)
            LOGGER.debug("add link to input");
        if (links.size() >= getMaxConnections())
            return false;
        links.add(link);
        if (debug)
            LOGGER.debug("added link to input");
        try {
            this.getModuleBox().getCore().onInputAttach(link);
        } catch (Exception e) {
            Displayer.ddisplay(200909302300L, e, this,
                               "ERROR IN MODULE FUNCTION:\n" +
                               "An error has occured in the function \"onInputAttach\"" +
                               "of module \"" + name + "\".\n" +
                               "Please report this error to the module core developer."
            );
        }
        checkSaturation();
        this.getModuleBox().getElement().checkSaturation();
        this.getModuleBox().getEngine().engineSaturationCheck();
        return true;
    }

    public boolean removeLink(Link link, boolean active)
    {
        this.links.remove(link);
        if (active) {
            try {
                this.getModuleBox().getCore().onInputDetach(link);
            } catch (Exception e) {
                Displayer.ddisplay(200909302300L, e, this,
                                   "ERROR IN MODULE FUNCTION:\n" +
                                   "An error has occured in the function \"onInputDetach\"" +
                                   "of module \"" + name + "\".\n" +
                                   "Please report this error to the module core developer."
                );
            }
        }
        checkSaturation();
        this.getModuleBox().getElement().checkSaturation();
        this.getModuleBox().getEngine().engineSaturationCheck();
        return true;
    }

    public Input(InputEgg egg)
    {//, ModuleBox module) {
        super(egg.getName());//, module);
        this.egg = egg;
        this.predecessors = new Vector<Element>();
        this.action_wait = 0;
        this.links = new Vector<Link>();
        this.datas = new Vector<VNData>();
        this.values = new Vector<Object>();
        checkSaturation();
    }

    //private ModuleBox module;
    //public ModuleBox getModuleBoxEgg() {return module;}
    //<editor-fold defaultstate="collapsed" desc=" NRAD Message Propagating ">
    protected Vector<Element> predecessors;
    protected int action_wait;
    protected boolean anyoneActive;

    @Override
    protected void onNotifyMessage(Message message) throws VNSystemEngineException
    {
        //VNLogger.debugMessage(this, true, message);
        //this.getModuleBox().getEngine().writeFlow(this+" : notify");
        if (getElementState() == ElementState.passive) {
            setElementState(ElementState.notifying);
            predecessors.clear();
            action_wait = 0;
            anyoneActive = false;
            try {
                getModuleBox().getElement().putToQueue(new Message(this, Message.NOTIFY));
            } catch (InterruptedException ex) {
                throw new VNSystemEngineException(
                        200812101750L,
                        "Notify message propagation interrupted",
                        ex,
                        this,
                        Thread.currentThread());

            }
            predecessors.add(message.getSender());
            ++action_wait;
            return;
        }
        if (getElementState() == ElementState.notifying) {
            predecessors.add(message.getSender());
            ++action_wait;
            return;
        }
        if (getElementState() == ElementState.ready) {
            predecessors.add(message.getSender());
            ++action_wait;
            try {
                message.getSender().putToQueue(new Message(this, Message.READY));
            } catch (InterruptedException ex) {
                throw new VNSystemEngineException(
                        200910261503L,
                        "Notify message answer interrupted",
                        ex,
                        this,
                        Thread.currentThread());
            }
            return;
        }

        throw new VNSystemEngineStateException(
                200812101745L,
                "Wrong state of input: notifying @ " + getElementState(),
                null,
                this,
                Thread.currentThread());

    }

    @Override
    protected void onReadyMessage(Message message) throws VNSystemEngineException
    {
        //this.getModuleBox().getEngine().writeFlow(this+" : ready");
        if (getElementState() != ElementState.notifying)
            throw new VNSystemEngineStateException(
                    200812101752L,
                    "Wrong state of input: ready @ " + getElementState(),
                    null,
                    this,
                    Thread.currentThread());
        setElementState(ElementState.ready);
        try {
            for (Element element : predecessors) {
                element.putToQueue(new Message(this, Message.READY));
            }
        } catch (InterruptedException ex) {
            throw new VNSystemEngineException(
                    200812101750L,
                    "Ready message propagation interrupted",
                    ex,
                    this,
                    Thread.currentThread());
        }

    }

    @Override
    protected void onActionMessage(Message message) throws VNSystemEngineException
    {
        //this.getModuleBox().getEngine().writeFlow(this+" : action");
        if (getElementState() != ElementState.ready)
            throw new VNSystemEngineStateException(
                    200812101755L,
                    "Wrong state of input: action @ " + getElementState(),
                    null,
                    this,
                    Thread.currentThread());
        --action_wait;
        anyoneActive = true;
        if (action_wait != 0)
            return;
        setActive();
    }

    @Override
    protected void onInactionMessage(Message message) throws VNSystemEngineException
    {
        //this.getModuleBox().getEngine().writeFlow(this+" : inaction");
        if (getElementState() != ElementState.ready)
            throw new VNSystemEngineStateException(
                    200812101755L,
                    "Wrong state of input: action @ " + getElementState(),
                    null,
                    this,
                    Thread.currentThread());
        --action_wait;
        if (action_wait != 0)
            return;
        setActive();
    }

    protected void setActive() throws VNSystemEngineException
    {
        setElementState(ElementState.propagating);
        try {
            getModuleBox().getElement().putToQueue(new Message(this, (anyoneActive) ? Message.ACTION : Message.INACTION));
        } catch (InterruptedException ex) {
            throw new VNSystemEngineException(
                    200812101758L,
                    "Action message propagation interrupted",
                    null,
                    this,
                    Thread.currentThread());

        }
    }

    @Override
    protected void onDoneMessage(Message message) throws VNSystemEngineException
    {
        //        this.getModuleBox().getEngine().writeFlow(this+" : done");
        if (getElementState() != ElementState.propagating)
            throw new VNSystemEngineStateException(
                    200812101759L,
                    "Wrong state of input: ready @ " + getElementState(),
                    null,
                    this,
                    Thread.currentThread());
        setElementState(ElementState.passive);
        try {
            for (Element element : predecessors) {
                element.putToQueue(new Message(this, Message.DONE));
            }
        } catch (InterruptedException ex) {
            throw new VNSystemEngineException(
                    200812101760L,
                    "Done message propagation interrupted",
                    null,
                    this,
                    Thread.currentThread());
        }
        predecessors.clear();
        action_wait = 0;
    }

    //</editor-fold>
    @Override
    protected void onKillMessage()
    {

    }

    public VNData getFirstData()
    {
        if (links.isEmpty())
            return null;
        return links.elementAt(0).getOutput().getData();
    }

    public Vector<VNData> getDatas()
    {
        datas.clear();
        for (Link link : links)
            datas.add(link.getOutput().getData());
        return datas;
    }

    //TODO: wersje pobierajace DV i niepobierajace DV
    //TOOD: dobry opis w javadoc
    public Object getFirstValue()
    {
        if (links.isEmpty() || this.saturation != InputSaturation.ok)
            return getDefaultValue();
        return links.elementAt(0).getOutput().getValue();
    }

    public Vector<Object> getValues()
    {
        values.clear();
        if (links.isEmpty() || this.saturation != InputSaturation.ok) {
            values.add(getDefaultValue());
        } else {
            for (Link link : links)
                values.add(link.getOutput().getValue());
        }
        return values;
    }

    @Override
    public String toString()
    {
        if (getModuleBox() == null)
            return this.getName();
        return getModuleBox().getName() + ".in." + getName();
    }

    public VNDataAcceptor[] getVNDataAcceptors()
    {
        return egg.getVNDataAcceptors();
    }
}
