/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Outputs implements Iterable<Output>
{

    protected HashMap<String, Output> outputs;
    protected Vector<Output> sorted;

    public Outputs(OutputEgg[] eggs)
    {//, ModuleBox module) {
        outputs = new HashMap<String, Output>();
        sorted = new Vector<Output>();
        if (ModuleBoxFace.ACTIONPORTS) {
            //TODO!
            Output action = new Output(new OutputEgg("actionOutput", Void.class));//, module);
            //TODO!
            outputs.put("actionOutput", action);
            //TODO!
            sorted.add(action);
        }
        if (eggs != null)
            for (OutputEgg outputEgg : eggs) {
                Output output = new Output(outputEgg);//, module);
                outputs.put(outputEgg.getName(), output);
                sorted.add(output);
            }
    }

    protected void setModuleBox(ModuleBoxFace module)
    {
        for (Output output : outputs.values())
            output.setModuleBox(module);
    }

    public HashMap<String, Output> getOutputs()
    {
        return outputs;
    }

    public Vector<Output> getSortedOutputs()
    {
        return sorted;
    }

    public Output getOutput(String name)
    {
        return outputs.get(name);
    }

    public Iterator<Output> iterator()
    {
        return getOutputs().values().iterator();
    }

    //protected void addInput(String name, Output output) {
    //    outputs.put(name, output);
    //}
    //    public Outputs(ModuleBox module) {
    //        outputs = new HashMap<String, Output>();
    //        outputs.put("actionOutput", new Output(new OutputEgg("actionOutput", Void.class), module));
    //    }
}
