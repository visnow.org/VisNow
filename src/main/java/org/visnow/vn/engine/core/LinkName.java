/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LinkName
{

    private String outputModule;
    private String outputPort;
    private String inputModule;
    private String inputPort;

    public LinkName(String outputModule, String outputPort, String inputModule, String inputPort)
    {
        this.outputModule = outputModule;
        this.outputPort = outputPort;
        this.inputModule = inputModule;
        this.inputPort = inputPort;
    }

    /**
     * @return the outputModule
     */
    public String getOutputModule()
    {
        return outputModule;
    }

    /**
     * @return the outputPort
     */
    public String getOutputPort()
    {
        return outputPort;
    }

    /**
     * @return the inputModule
     */
    public String getInputModule()
    {
        return inputModule;
    }

    /**
     * @return the inputPort
     */
    public String getInputPort()
    {
        return inputPort;
    }

    @Override
    public String toString()
    {
        return "[" + outputModule + ":" + outputPort + "]->[" + inputModule + ":" + inputPort + "]";
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof LinkName) {
            LinkName name = (LinkName) o;
            if (!this.outputModule.equals(name.getOutputModule()))
                return false;
            if (!this.outputPort.equals(name.getOutputPort()))
                return false;
            if (!this.inputModule.equals(name.getInputModule()))
                return false;
            if (!this.inputPort.equals(name.getInputPort()))
                return false;
            return true;
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 29 * hash + (this.outputModule != null ? this.outputModule.hashCode() : 0);
        hash = 29 * hash + (this.outputPort != null ? this.outputPort.hashCode() : 0);
        hash = 29 * hash + (this.inputModule != null ? this.inputModule.hashCode() : 0);
        hash = 29 * hash + (this.inputPort != null ? this.inputPort.hashCode() : 0);
        return hash;
    }
}
