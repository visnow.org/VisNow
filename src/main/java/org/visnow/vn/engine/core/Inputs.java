/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Inputs implements Iterable<Input>
{

    protected HashMap<String, Input> inputs;
    protected Vector<Input> sorted;

    public HashMap<String, Input> getInputs()
    {
        return inputs;
    }

    public Vector<Input> getSortedInputs()
    {
        return sorted;
    }

    public Input getInput(String name)
    {
        return inputs.get(name);
    }

    //protected void addInput(String name, Input input) {
    //    inputs.put(name, input);
    //}
    public Inputs(InputEgg[] eggs)
    {//, ModuleBox module) {
        inputs = new HashMap<String, Input>();
        sorted = new Vector<Input>();
        if (ModuleBoxFace.ACTIONPORTS) {
            //TODO!
            Input action = new Input(new InputEgg("actionInput", Void.class, InputEgg.TRIGGERING, 0, -1));//, module);
            //TODO!
            inputs.put("actionInput", action);
            //TODO!
            sorted.add(action);
        }
        if (eggs != null)
            for (InputEgg inputEgg : eggs) {
                Input input = new Input(inputEgg);//, module);
                inputs.put(inputEgg.getName(), input);
                sorted.add(input);
            }
    }

    public void setModuleBox(ModuleBoxFace module)
    {
        for (Input input : inputs.values())
            input.setModuleBox(module);
    }

    @Override
    public Iterator<Input> iterator()
    {
        return inputs.values().iterator();
    }

}
