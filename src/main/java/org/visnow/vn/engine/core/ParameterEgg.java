/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class ParameterEgg<E>
{

    //    public static final int INDEPENDENT = 0;
    //    public static final int DEPENDENT = 1;
    //    public static final int FILENAME = 2;
    protected String name;
    protected E defValue;
    protected ParameterType type;

    private final Class valueClass;

    public String getName()
    {
        return name;
    }

    public E getDefaultValue()
    {
        return defValue;
    }

    public ParameterType getType()
    {
        return type;
    }

    public ParameterEgg(String name, ParameterType type)
    {
        this(name, type, null);
    }

    /**
     * @deprecated Reverse procedure parameters.
     * @param type
     * @param name
     */
    public ParameterEgg(ParameterType type, String name)
    {
        this(name, type, null);
    }

    /**
     * @deprecated Change procedure parameter order.
     * @param type
     * @param name
     * @param defaultValue
     */
    public ParameterEgg(ParameterType type, String name, E defaultValue)
    {
        this.type = type;
        this.name = name;
        this.defValue = defaultValue;
        valueClass = null;
    }

    public ParameterEgg(String name, ParameterType type, E defaultValue)
    {
        this.type = type;
        this.name = name;
        this.defValue = defaultValue;
        valueClass = null;
    }

    public ParameterEgg(Class<E> valueClass, String name, ParameterType type, E defaultValue)
    {
        this.type = type;
        this.name = name;
        this.defValue = defaultValue;
        this.valueClass = valueClass;
        if (valueClass == null)
            throw new NullPointerException("null valueClass is not allowed here. Use old ParameterEgg(name, type, defaultValue) instead.");
    }

    /**
     * @deprecated
     * @param name
     * @param defaultValue
     */
    public ParameterEgg(String name, E defaultValue)
    {
        this(name, ParameterType.dependent, defaultValue);
    }

    public Parameter hatch()
    {
        if (valueClass != null)
            return new Parameter<E>(valueClass, this.getName(), defValue, type);
        else
            return new Parameter<E>(this.getName(), defValue, type);
    }
}
