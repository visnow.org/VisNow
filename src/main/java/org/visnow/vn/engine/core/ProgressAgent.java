/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Progress agent which can work as a progress proxy between progress producer and progress consumer (through abstract updateProgress method).
 * This progress agent can work in two modes:
 * - setting progress using float value in range 0 ... 1
 * - setting progress by increasing current step (up to total number of steps).
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public abstract class ProgressAgent
{
    private final long totalSteps;
    protected AtomicLong currentStep = new AtomicLong(0);

    public ProgressAgent(long totalSteps)
    {
        this.totalSteps = totalSteps;
        if (totalSteps <= 0) throw new IllegalArgumentException("Total steps must be positive");
    }

    public ProgressAgent()
    {
        this.totalSteps = Long.MAX_VALUE;
    }

    public long getTotalSteps()
    {
        return totalSteps;
    }

    public void setProgress(double progress)
    {
        setProgressStep(Math.max(0l, Math.min(totalSteps, Math.round(progress * totalSteps))));
    }

    public void setProgressStep(long step)
    {
        currentStep.set(Math.max(0l, Math.min(totalSteps, step)));
        updateProgress();
    }

    public void increase()
    {
        increase(1);
    }

    /**
     * @param step positive number of steps to increase.
     */
    public void increase(long step)
    {
        currentStep.set(Math.min(totalSteps, currentStep.get() + step));
        updateProgress();
    }

    protected double getProgress()
    {
        return (double) currentStep.get() / totalSteps;
    }

    protected abstract void updateProgress();

    public static ProgressAgent getDummyAgent()
    {
        return new ProgressAgent()
        {
            @Override
            protected void updateProgress()
            {
            }
        };
    }

    /**
     * Creates sub-agent, which is part of this agent; Its {@link #totalSteps} &le; this.{@link #totalSteps}; Steps of both agents are mapped in 1:1 ratio.
     * One can use {@link #totalSteps} as a total number of steps to increase (no matter what is {@link #totalSteps} of parent agent}.
     * This is useful for multi threaded computations.
     * <p>
     * Note: this sub-agent doesn't support "going back" so every call to {@link #setProgress(double)} and {@link #setProgressStep(long)} must increase
     * {@link #totalSteps} value (or at least leave it same as before operation).
     * <p>
     * @param agentSteps positive number of steps to get from this agent.
     * <p>
     * @throws IllegalArgumentException if incorrect value is passed to {@link #setProgress(double)} and {@link #setProgressStep(long)}.
     */
    public ProgressAgent getSubAgent(long agentSteps)
    {
        if (agentSteps > this.totalSteps) throw new IllegalArgumentException("Incorrect step number: " + agentSteps + " > " + totalSteps);

        final ProgressAgent parentAgent = this;
        return new ProgressAgent(agentSteps)
        {
            long previousStep = 0;

            @Override
            protected void updateProgress()
            {
                long increaseStep = this.currentStep.get() - previousStep;
                if (increaseStep < 0) throw new IllegalArgumentException("CurrentStep decreased! Such operation in sub-agent is not supported");
                previousStep += increaseStep;
                parentAgent.increase(increaseStep);
            }
        };
    }
}
