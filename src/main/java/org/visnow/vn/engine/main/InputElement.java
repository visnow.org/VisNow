/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.main;

import org.visnow.vn.engine.exception.VNSystemEngineException;
import org.visnow.vn.engine.messages.Message;

/**
 *
 * @author gacek
 */
public class InputElement
{/*extends Port {

     @Override
     public boolean isInput() {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     public Class getType() {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onNotifyMessage(Message message) throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onReadyMessage(Message message) throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onActionMessage(Message message) throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onInactionMessage(Message message) throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onDoneMessage(Message message) throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override
     protected void onKillMessage() throws VNSystemEngineException {
     throw new UnsupportedOperationException("Not supported yet.");
     }
     */

}
