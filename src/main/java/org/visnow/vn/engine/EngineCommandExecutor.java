/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine;

import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.element.Element;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.main.DataModule;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.messages.Message;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class EngineCommandExecutor
{

    private static final Logger LOGGER = Logger.getLogger(EngineCommandExecutor.class);

    private static boolean debug = false;
    private Engine engine;

    public Engine getEngine()
    {
        return engine;
    }

    public boolean addLink(LinkName name, boolean active)
    {
        if (getEngine().getLink(name) != null) {
            return false;
        }

        ModuleBox receivingModule = getEngine().getModule(name.getInputModule());
        Link link = new Link(
                getEngine().getModule(name.getOutputModule()).getOutput(name.getOutputPort()),
                receivingModule.getInput(name.getInputPort()));
        getEngine().getLinks().put(name, link);
        return true;
    }

    public void addModule(String name, ModuleCore core)
    {
        core.setApplication(this.getEngine().getApplication());
        ModuleBox mb = new ModuleBox(getEngine(), name, core);
        getEngine().getModules().put(name, mb);
        mb.run();
    }

    public boolean deleteLink(LinkName name, boolean active)
    {
//        LOGGER.debug("Link name: " + name);
        Link link = getEngine().getLink(name);
        link.getInput().removeLink(link, active);
        link.getOutput().removeLink(link, active);
        getEngine().getLinks().remove(name);
        return true;
    }

    public void deleteModule(String name)
    {
        LOGGER.debug("");
        try {
            getEngine().getModule(name).getCore().onDelete();
            Element element = getEngine().getModule(name).getElement();
            element.putToQueue(new Message(element, Message.KILL));
        } catch (Exception e) {
            Displayer.ddisplay(200909302300L, e, this,
                               "ERROR IN MODULE FUNCTION:\n" +
                               "An error has occured in the function \"onDelete\"" +
                               "of module \"" + name + "\".\n" +
                               "Please report this error to the module core developer.");
        }
        getEngine().getModules().remove(name);
    }

    public EngineCommandExecutor(Engine engine)
    {
        this.engine = engine;
    }

    public void renameModule(String name, String newName)
    {
        ModuleBox mb = this.engine.getModule(name);
        //        for(Link link: mb) {
        //            LinkName old = link.getName();
        //            String in = old.getInputModule();
        //            String out = old.getOutputModule();
        //            if(in.equals(name)) in = newName;
        //            if(out.equals(name)) out = newName;
        //            LinkName ln = new LinkName(
        //                    out,
        //                    old.getOutputPort(),
        //                    in,
        //                    old.getInputPort()
        //                    );
        //            link.updateName(ln);
        //            engine.updateLinkName(old, ln);
        //        }
        mb.updateName(newName);
        engine.updateModuleName(name, newName);
        // also rename links
    }

    public void splitLink(LinkName linkName, String name)
    {

        DataModule dm = new DataModule(
                getEngine(),
                name,
                getEngine().getLink(linkName).getOutput().getData());
        getEngine().getModules().put(name, dm);

        Link link = getEngine().getLink(linkName);
        link.splitToOutput(dm.getOutput());

        getEngine().getLinks().remove(linkName);
        //getEngine().getModule(linkName.getOutputModule()).splitRemoveLink(linkName);
        getEngine().getLinks().put(link.getName(), link);
        //nie tworzymy nowego watku!!

    }

    public void renameLink(LinkName oldLinkName, LinkName newLinkName)
    {
        Link link = getEngine().getLink(oldLinkName);
        link.updateName(newLinkName);
        engine.updateLinkName(oldLinkName, newLinkName);
    }
}
