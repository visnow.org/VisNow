/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.library;

import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LibraryFolder implements Comparable
{

    protected String name;
    protected LibraryRoot root;
    protected Vector<LibraryFolder> folders;
    protected Vector<LibraryCore> cores;
    protected boolean sorted = false;
    protected boolean open = false;

    public String getName()
    {
        return name;
    }

    public LibraryRoot getRoot()
    {
        return root;
    }

    public Vector<LibraryFolder> getSubFolders()
    {
        if (sorted)
            Collections.sort(folders);
        return folders;
    }

    public Vector<LibraryCore> getCores()
    {
        if (sorted)
            Collections.sort(cores);
        return cores;
    }

    public Vector<LibraryCore> getAllCores()
    {
        Vector<LibraryCore> out = new Vector<LibraryCore>();
        if (folders != null) {
            for (int i = 0; i < folders.size(); i++) {
                out.addAll(folders.get(i).getAllCores());
            }
        }
        out.addAll(cores);
        return out;
    }

    public LibraryFolder(LibraryRoot root, String name, Vector<LibraryFolder> subFolders, Vector<LibraryCore> subCores, boolean sorted, boolean open)
    {
        this.root = root;
        this.name = name;
        this.folders = subFolders;
        this.cores = subCores;
        this.sorted = sorted;
        this.open = open;
    }

    @Override
    public int compareTo(Object o)
    {
        if (o == null || !(o instanceof LibraryFolder))
            return 1;

        if (o == this)
            return 0;

        return this.getName().compareTo(((LibraryFolder) o).getName());
    }

    public boolean isOpen()
    {
        return this.open;
    }

}
