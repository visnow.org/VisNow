/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.library.jar;

import java.util.HashMap;
import java.util.Vector;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.engine.library.LibraryFolder;
import org.visnow.vn.engine.library.LibraryRoot;
import org.visnow.vn.engine.library.TypesMap;
import org.visnow.vn.lib.types.VNDataAcceptor;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class JarLibraryRoot implements LibraryRoot
{

    protected String filePath;
    protected String name;
    protected LibraryFolder rootFolder;

    protected ClassLoader loader;

    protected TypesMap typesMap;

    @Override
    public TypesMap getTypesMap()
    {
        return typesMap;
    }

    @Override
    public int getType()
    {
        return JAR;
    }

    @Override
    public String getFilePath()
    {
        return filePath;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public LibraryFolder getRootFolder()
    {
        return rootFolder;
    }

    public ClassLoader getLoader()
    {
        return loader;
    }

    protected void setRootFolder(LibraryFolder root)
    {
        this.rootFolder = root;
    }

    protected void setTypesMap(TypesMap typesMap)
    {
        this.typesMap = typesMap;
    }

    public JarLibraryRoot(String name, String filePath, ClassLoader loader)
    {
        this.name = name;
        this.loader = loader;
        this.typesMap = null;
        this.filePath = filePath;
    }

    @Override
    public ModuleCore loadCore(String classPath) throws VNException
    {
        try {
            Class coreClass = loader.loadClass(classPath);
            @SuppressWarnings("unchecked")
            ModuleCore ret = (ModuleCore) coreClass
                .getConstructor()
                .newInstance();

            //.getMethod("getInstance",(java.lang.Class[])null)
            //.invoke(null,(java.lang.Object[])null);
            /* TODO: set core name */
            //ret.setLibraryInfo(this, null, null);
            ret.setLibraryInfo(this.getName(), classPath);//, core.getName());
            return ret;
        } catch (Exception ex) {
            throw new VNException(
                123456789L,
                "Cannot load module core.",
                ex,
                this,
                Thread.currentThread());
        }
    }

    @Override
    public HashMap<String, String> getInputTypes(String className)
    {
        HashMap<String, String> ret = new HashMap<String, String>();
        InputEgg[] e = ModuleCore.getInputEggs(className, loader);
        if (e == null)
            return ret;
        for (InputEgg egg : e) {
            ret.put(egg.getName(), egg.getType().getName());
        }
        return ret;
    }

    @Override
    public HashMap<String, VNDataAcceptor[]> getInputVNDataAcceptors(String className)
    {
        HashMap<String, VNDataAcceptor[]> ret = new HashMap<String, VNDataAcceptor[]>();
        InputEgg[] e = ModuleCore.getInputEggs(className, loader);
        if (e == null)
            return ret;

        for (InputEgg egg : e) {
            VNDataAcceptor[] vndas = egg.getVNDataAcceptors();
            if (vndas != null)
                ret.put(egg.getName(), vndas);
        }
        return ret;
    }

    @Override
    public Vector<LibraryCore> getAllCores()
    {
        if (this.rootFolder == null)
            return null;

        return rootFolder.getAllCores();
    }

}
