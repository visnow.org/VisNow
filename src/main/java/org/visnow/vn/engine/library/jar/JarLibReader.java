/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.library.jar;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNOuterDataException;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.engine.library.LibraryFolder;
import org.visnow.vn.engine.library.TypesMap;
import org.visnow.vn.system.config.VNPlugin;
import org.visnow.vn.system.libraries.TypeStyle;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class JarLibReader
{

    private static final Logger LOGGER = Logger.getLogger(JarLibReader.class);

    //<editor-fold defaultstate="collapsed" desc=" Interface ">
    public static JarLibraryRoot readFromJar(String path)
    {
        return readFromJar(new File(path), null);
    }

    public static JarLibraryRoot readFromJar(File file)
    {
        return readFromJar(file, null);
    }

    public static JarLibraryRoot readFromPlugin(VNPlugin plugin)
    {
        return readFromJar(new File(plugin.getJarPath()), plugin.getLoader());
    }

    public static JarLibraryRoot readFromJar(File file, ClassLoader readyLoader)
    {
        try {
            return tryReadFromJar(file, readyLoader);
        } catch (VNOuterDataException | ParserConfigurationException | SAXException | IOException ex) {
            LOGGER.error("STATIC: JarLibReader: Could not read library from jar", ex);
        } catch (Exception e) {
            LOGGER.error("STATIC: JarLibReader: Could not read library from jar", e);
        }

        return null;
    }

    //</editor-fold>
    protected static JarLibraryRoot tryReadFromJar(File file, ClassLoader readyLoader) throws IOException, ParserConfigurationException, SAXException, VNOuterDataException
    {
        if (!file.exists()) {
            LOGGER.warn("Cannot read library. File does not exist: " + file.getAbsolutePath());
            return null;
        }
        JarFile jar = new JarFile(file);
        Enumeration<JarEntry> enumeration;
        JarEntry tmpEntry;
        JarEntry typesEntry = null;
        JarEntry libraryEntry = null;
        enumeration = jar.entries();

        int ii = 2;
        while (enumeration.hasMoreElements()) {
            tmpEntry = enumeration.nextElement();
            if (tmpEntry.getName().toLowerCase().equals("types.xml")) {
                typesEntry = tmpEntry;
                --ii;
                if (ii == 0)
                    break;
            }
            if (tmpEntry.getName().toLowerCase().equals("library.xml")) {
                libraryEntry = tmpEntry;
                --ii;
                if (ii == 0)
                    break;
            }
        }

        /*
         * TODO: types
         */
        if (libraryEntry == null)
            return null;
        ///////////////////LIBRARY FILE FOUND, GENERATING LIBRARY
        TypesMap typesMap = null;
        if (typesEntry != null)
            typesMap = readTypesMap(jar, typesEntry);
        ///////////////////GENERATE CLASS LOADER
        ClassLoader loader = null;
        if (readyLoader != null) {
            loader = readyLoader;
        } else {
            loader
                    = new URLClassLoader(new URL[]{
                        file.toURI().toURL()
                    });
        }
        ///////////////////PREPARE TO PARSE XML
        InputStream is = null;
        is = jar.getInputStream(libraryEntry);
        Node main = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
        if (!main.getNodeName().equalsIgnoreCase("library"))
            throw new VNOuterDataException(200903271350L, "Main node is not a library node.", null, null, Thread.currentThread());
        NodeList list = main.getChildNodes();

        JarLibraryRoot root;
        String tmp = file.getPath();
        if (tmp.contains("\\")) {
            tmp = file.getPath().replace('\\', '/');
            tmp = "/" + tmp;
        }
        //if(file.getPath().equals(VisNow.get().getJarPath()))
        if (tmp.equals(VisNow.get().getJarPath()))
            root = new InternalLibraryRoot(
                    main.getAttributes().getNamedItem("name").getNodeValue(),
                    file.getPath(),
                    loader);
        else
            root = new JarLibraryRoot(
                    main.getAttributes().getNamedItem("name").getNodeValue(),
                    file.getPath(),
                    loader);

        boolean sorted = false;
        if (main.getAttributes().getNamedItem("autosort") != null) {
            sorted = main.getAttributes().getNamedItem("autosort").getNodeValue().equalsIgnoreCase("yes");
        }

        boolean open = false;
        if (main.getAttributes().getNamedItem("open") != null) {
            open = main.getAttributes().getNamedItem("open").getNodeValue().equalsIgnoreCase("yes");
        }

        LibraryFolder rootFolder = new LibraryFolder(
                root,
                main.getAttributes().getNamedItem("name").getNodeValue(),
                new Vector<LibraryFolder>(),
                new Vector<LibraryCore>(),
                sorted,
                open);
        root.setRootFolder(rootFolder);
        root.setTypesMap(typesMap);

        for (int i = 0; i < list.getLength(); ++i)
            if (list.item(i).getNodeName().equalsIgnoreCase("folder")) {
                rootFolder.getSubFolders().add(readFolder(root, list.item(i)));
            } else if (list.item(i).getNodeName().equalsIgnoreCase("core")) {
                LOGGER.debug(list.item(i).getAttributes().getNamedItem("package"));
                LibraryCore core = readCore(root, list.item(i));
                if (core != null) {
                    rootFolder.getCores().add(core);
                } else {
                    String str = "";
                    if (list.item(i).getAttributes().getNamedItem("name") != null &&
                            list.item(i).getAttributes().getNamedItem("class") != null) {
                        str = list.item(i).getAttributes().getNamedItem("class").getNodeValue();
                    } else if (list.item(i).getAttributes().getNamedItem("package") != null) {
                        str = list.item(i).getAttributes().getNamedItem("package").getNodeValue();
                    }
                    System.err.println("ERROR: bad entry in library.xml file: " + str);
                }
            }
        return root;

    }

    protected static TypesMap readTypesMap(JarFile jar, JarEntry typesEntry) throws IOException, ParserConfigurationException, SAXException, VNOuterDataException
    {
        InputStream is = jar.getInputStream(typesEntry);

        TypesMap out = readTypesMap(is);
        is.close();

        return out;
    }

    public static TypesMap readTypesMap(InputStream is) throws IOException, ParserConfigurationException, SAXException, VNOuterDataException
    {
        if (is == null)
            return null;

        Node main = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
        if (!main.getNodeName().equalsIgnoreCase("types"))
            throw new VNOuterDataException(200903271351L, "Main node is nod a types node.", null, null, Thread.currentThread());
        NodeList list = main.getChildNodes();

        TypesMap typesMap = new TypesMap();

        for (int i = 0; i < list.getLength(); ++i)
            if (list.item(i).getNodeName().equalsIgnoreCase("type"))
                typesMap.getStyles().put(
                        list.item(i).getAttributes().getNamedItem("classname").getNodeValue(),
                        readTypeStyle(list.item(i)));

        return typesMap;
    }

    protected static TypeStyle readTypeStyle(Node node)
    {
        String value = node.getAttributes().getNamedItem("color").getNodeValue();
        return new TypeStyle(VNSwingUtils.color(value));
    }

    //<editor-fold defaultstate="collapsed" desc=" Read Folder ">
    protected static LibraryFolder readFolder(JarLibraryRoot root, Node node)
    {
        boolean sorted = false;
        if (node.getAttributes().getNamedItem("autosort") != null) {
            sorted = node.getAttributes().getNamedItem("autosort").getNodeValue().equalsIgnoreCase("yes");
        }

        boolean open = false;
        if (node.getAttributes().getNamedItem("open") != null) {
            open = node.getAttributes().getNamedItem("open").getNodeValue().equalsIgnoreCase("yes");
        }

        LibraryFolder ret = new LibraryFolder(
                root,
                node.getAttributes().getNamedItem("name").getNodeValue(),
                new Vector<LibraryFolder>(),
                new Vector<LibraryCore>(),
                sorted,
                open
        );
        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); ++i)
            if (list.item(i).getNodeName().equalsIgnoreCase("folder"))
                ret.getSubFolders().add(readFolder(root, list.item(i)));
            else if (list.item(i).getNodeName().equalsIgnoreCase("core")) {
//                LOGGER.debug(list.item(i).getAttributes().getNamedItem("package"));
                LibraryCore lc = readCore(root, list.item(i));
                if (lc != null) {
                    ret.getCores().add(lc);
                } else {
                    throw new RuntimeException("Error reading library core. Root: " + root.getName() +
                            " Core name: " + list.item(i).getAttributes().getNamedItem("name") +
                            " Core class: " + list.item(i).getAttributes().getNamedItem("class") +
                            " Core package: " + list.item(i).getAttributes().getNamedItem("package"));
                }
            }

        return ret;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Read Core ">
    protected static LibraryCore readCore(JarLibraryRoot root, Node node)
    {
        try {
            LibraryCore core = null;
            if (node.getAttributes().getNamedItem("name") != null &&
                    node.getAttributes().getNamedItem("class") != null)
                core = new LibraryCore(
                        root,
                        node.getAttributes().getNamedItem("name").getNodeValue(),
                        node.getAttributes().getNamedItem("class").getNodeValue());
            else if (node.getAttributes().getNamedItem("package") != null)
                core = new LibraryCore(
                        root,
                        node.getAttributes().getNamedItem("package").getNodeValue());

            if (core != null && core.isValid())
                return core;
            else
                return null;
        } catch (NullPointerException ex) {
            LOGGER.error("Incorrect node: ", ex);
            return null;
        } catch (ClassNotFoundException ex) {
            LOGGER.error("ERROR in library.xml - wrong class in node: ", ex);
            return null;
        }
    }
    //</editor-fold>
    private JarLibReader()
    {
    }
}
