/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.library;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.ModuleXMLReader;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.library.jar.JarLibraryRoot;
import org.visnow.vn.lib.types.VNDataAcceptor;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LibraryCore implements Comparable
{

    private boolean valid = false;
    protected LibraryRoot root;
    protected String className;
    protected String name;
    protected String shortDescription = null;
    protected String longDescription = null;
    protected String keyWords = null;
    protected String helpTopicID = null;
    protected CoreName coreName;
    protected boolean reader = false;
    protected String readerDataType = null;
    protected boolean testData = false;

    public String getName()
    {
        return name;
    }

    public CoreName getCoreName()
    {
        return coreName;
    }

    public LibraryRoot getRoot()
    {
        return root;
    }

    public String getClassPath()
    {
        return className;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String getHelpTopicID()
    {
        return helpTopicID;
    }

    public boolean isReader()
    {
        return reader;
    }

    public boolean isTestData()
    {
        return testData;
    }

    public String getReaderDataType()
    {
        return readerDataType;
    }

    public ModuleCore generateCore() throws VNException
    {
        return root.loadCore(className);
    }

    public LibraryCore(LibraryRoot root, String name, String className) throws ClassNotFoundException
    {
        this.valid = true;
        this.root = root;
        this.name = name;
        this.className = className;
        this.coreName = new CoreName(root.getName(), className);

        if (root instanceof JarLibraryRoot) {
            Class coreClass = ((JarLibraryRoot) root).getLoader().loadClass(className);
        }
    }

    public LibraryCore(LibraryRoot root, String packageName) throws ClassNotFoundException
    {
        this.root = root;
        this.valid = false;

        String[] tmp = null;
        try {
            if (root instanceof JarLibraryRoot)
                tmp = ModuleXMLReader.getModuleInfo(packageName, ((JarLibraryRoot) root).getLoader());
            else
                tmp = ModuleXMLReader.getModuleInfo(packageName, null);
        } catch (IOException ex) {
        } catch (ParserConfigurationException ex) {
        } catch (SAXException ex) {
        } catch (URISyntaxException ex) {
        }
        if (tmp == null || tmp.length != 8)
            return;

        this.valid = true;
        this.name = tmp[0];
        this.className = tmp[1];
        this.coreName = new CoreName(root.getName(), this.className);
        this.shortDescription = tmp[2];
        this.helpTopicID = tmp[3];
        if (tmp[4] != null) {
            this.reader = true;
            this.readerDataType = tmp[4];
        }
        this.testData = (tmp[5] != null && tmp[5].equals("true"));
        this.longDescription = tmp[6];
        this.keyWords = tmp[7];

        //        if(root instanceof JarLibraryRoot) {
        //            Class coreClass = ((JarLibraryRoot)root).getLoader().loadClass(this.className);
        //        }
    }

    public HashMap<String, String> getInputTypes()
    {
        return root.getInputTypes(className);
    }

    public HashMap<String, VNDataAcceptor[]> getInputVNDataAcceptors()
    {
        return root.getInputVNDataAcceptors(className);
    }

    @Override
    public int compareTo(Object o)
    {
        if (o == null || !(o instanceof LibraryCore))
            return 1;

        if (o == this)
            return 0;

        return this.getName().compareTo(((LibraryCore) o).getName());
    }

    /**
     * @return the valid
     */
    public boolean isValid()
    {
        return valid;
    }
}
