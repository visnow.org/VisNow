/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.element;

import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNSystemEngineException;
import org.visnow.vn.engine.messages.Message;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public abstract class Element implements Runnable
{
    private static final Logger LOGGER = Logger.getLogger(Element.class);
    
    private static boolean debug = false;
    //<editor-fold defaultstate="collapsed" desc=" [VAR] ">
    private ElementState elementState;
    //private ElementSaturation elementSaturation;
    private LinkedBlockingQueue<Message> queue; //need to be fair, use SynchronousQueue(true) constructor

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Accessors ">
    /**
     * @return the elementState
     */
    public synchronized ElementState getElementState()
    {
        return elementState;
    }

    /**
     * @param elementState the elementState to set
     */
    protected synchronized void setElementState(ElementState elementState)
    {
        LOGGER.trace(this + " " + this.elementState + " " +  elementState);
        this.elementState = elementState;
    }

    //    /**
    //     * @return the elementSaturation
    //     */
    //    public ElementSaturation getElementSaturation() {
    //        return elementSaturation;
    //    }
    //
    //    /**
    //     * @param elementSaturation the elementSaturation to set
    //     */
    //    protected void setElementSaturation(ElementSaturation elementSaturation) {
    //        this.elementSaturation = elementSaturation;
    //    }
    private final Object lock = new Object();

    public void putToQueue(Message message) throws InterruptedException
    {
        LOGGER.trace(this + ": " + message);
        //synchronized(lock) {
        getQueue().put(message);
        //}
    }
    
    public boolean isInQueue(Message message) throws InterruptedException
    {
        return getQueue().contains(message);
    }
    
    public String toStringQueue() {
        return getQueue().toString();
    }
    
    protected Message takeFromQueue() throws InterruptedException
    {
        Message m = getQueue().take();
        LOGGER.trace(this + ": " + m);
        //synchronized(lock) {
        //System.out.println("THREAD ["+Thread.currentThread()+"] is waiting on the queue.");
        return m;
        //}
    }

    protected void clearQueue()
    {
        //synchronized(lock) {
        getQueue().clear();
        //}
    }

    private LinkedBlockingQueue<Message> getQueue()
    {
        return queue;
    }        

    //</editor-fold>
    protected ElementKiller killer;

    private void kill()
    {
        clearQueue();
        setElementState(ElementState.passive);
        //TODO: log
    }

    protected String name;

    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public Element(String name)
    {
        this.name = name;
        //super(name);
        elementState = ElementState.passive;
        //        elementSaturation = ElementSaturation.notSaturated;
        queue = new LinkedBlockingQueue<Message>();
    }

    @SuppressWarnings("empty-statement")
    @Override
    public void run()
    {
//        LOGGER.debug("I AM RUNNING! {"+this+"}");
        try {
            //System.out.println("waiting... "+this);
            while (nextMessage()) {
            }
        } catch (InterruptedException ex) {
            //Displayer.ddisplay(200907100810L, ex, this, "Interrupted.");
        } catch (VNSystemEngineException ex) {
            Displayer.display(200907100811L, ex, this, "Elementary exception (Ooops!).");
        } catch (OutOfMemoryError err) {
            Exception ex = new Exception("Out of memory", err);
            Displayer.ddisplay(201112051933L, ex, this, "Out of memory error.");
        }
//        LOGGER.debug("I AM NOT RUNNING! {"+this+"}");
    }

    //<editor-fold defaultstate="collapsed" desc=" Messages ">
    public boolean nextMessage() throws InterruptedException, VNSystemEngineException
    {
        //if(debug) System.out.println("................ on "+getQueue().toString());
        Message message = takeFromQueue();

        ElementLogger.logMessage(this, message);
        if (killer.isKilled()) {
            kill();
            System.out.println("ENGINE KILLED");
            return true;
        }
//        LOGGER.debug(this + ": " + message);
        //if(debug) System.out.println(message+" received by ["+this+"]");
        switch (message.getType()) {
            case Message.KILL:
                onKillMessage();
                return false;
            case Message.NOTIFY:
                onNotifyMessage(message);
                break;
            case Message.READY:
                onReadyMessage(message);
                break;
            case Message.ACTION:
                onActionMessage(message);
                break;
            case Message.INACTION:
                onInactionMessage(message);
                break;
            case Message.DONE:
                onDoneMessage(message);
                break;
            default:
                onOtherMessage(message);
                break;
        }
        return true;
    }

    protected abstract void onNotifyMessage(Message message) throws VNSystemEngineException;

    protected abstract void onReadyMessage(Message message) throws VNSystemEngineException;

    protected abstract void onActionMessage(Message message) throws VNSystemEngineException;

    protected abstract void onInactionMessage(Message message) throws VNSystemEngineException;

    protected abstract void onDoneMessage(Message message) throws VNSystemEngineException;

    protected abstract void onKillMessage() throws VNSystemEngineException;

    protected void onOtherMessage(Message message) throws VNSystemEngineException
    {
    }

    //</editor-fold>
    protected Vector<ElementSaturationListener> saturationListeners = new Vector<ElementSaturationListener>();

    public void addSaturationListener(ElementSaturationListener elementSaturationListener)
    {
        saturationListeners.add(elementSaturationListener);
    }

    protected void fireElementSaturationListeners()
    {
        for (ElementSaturationListener listener : saturationListeners) {
            listener.saturationChanged();
        }
    }

    private Vector<ElementStateListener> listeners = new Vector<ElementStateListener>();

    public void addElementStateListener(ElementStateListener listener)
    {
        listeners.add(listener);
    }

    protected void fireElementStateListeners()
    {
        for (ElementStateListener listener : listeners) {
            listener.elementStateSet(this.getElementState());
        }
    }

    //public void restart() {
    //    this.setElementState(ElementState.passive);
    //    clearQueue();
    //
    //    }
}
