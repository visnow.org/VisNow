/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.main;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 *
 * @author babor
 */
public class WindowsRamTest
{

    private static void printFullUsage()
    {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        System.out.print("\n");
        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("get") && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                    e.printStackTrace();
                }
                System.out.println("\t" + method.getName() + " = " + value);
            }
        }
    }

    private static void printTotalMemory()
    {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        long totalMemoryB = -1;
        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("getTotalPhysicalMemorySize") && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                    e.printStackTrace();
                }

                if (value instanceof Long) {
                    totalMemoryB = (Long) value;
                }
                break;
            }
        }

        if (totalMemoryB == -1) {
            System.out.println("1024");
        } else {
            totalMemoryB = totalMemoryB * 4 / 5;
            long mem32limit = 1024L * 1024L * 1024L; //limit 32-bit JVM memory to 1GB
            if (!VisNow.isJreArch64() && totalMemoryB > mem32limit) {
                totalMemoryB = mem32limit;
            }
            System.out.println("" + (totalMemoryB / (1024L * 1024L)));
        }
    }

    public static void main(String[] args)
    {
        if (args != null && args.length == 1 && args[0].equals("-full")) {
            System.out.println("\n\n\t---------------JVM Runtime Details-------------");
            System.out.println("\tAvailable processors (Cores): " + Runtime.getRuntime().availableProcessors());
            System.out.println("\tInitial Memory (-Xms)       : " + (Runtime.getRuntime().freeMemory() / (1024 * 1024)) + " MB");
            long maxMemory = Runtime.getRuntime().maxMemory();
            System.out.println("\tMaximum JVM Memory (-Xmx)   : " + (maxMemory / (1024 * 1024)) + " MB");
            System.out.println("\tTotal Used JVM Memory       : " + (Runtime.getRuntime().totalMemory() / (1024 * 1024)) + " MB");

            File[] roots = File.listRoots();
            System.out.println("\n\n\t---------------FileSystem Details-------------");
            for (File root : roots) {
                System.out.println("\n\tFileSystem Root Details: " + root.getAbsolutePath());
                System.out.println("\tTotal Space              : " + (root.getTotalSpace() / (1024 * 1024)) + " MB");
                System.out.println("\tFree Space               : " + (root.getFreeSpace() / (1024 * 1024)) + " MB");
                System.out.println("\tUsable Space             : " + (root.getUsableSpace() / (1024 * 1024)) + " MB");
            }
            System.out.println("\n\n\t---------------CPU USAGES-------------");
            printFullUsage();
        } else {
            try {
                printTotalMemory();
            } catch (Exception ex) {
                System.out.println("1024");
            }
        }

    }
}
