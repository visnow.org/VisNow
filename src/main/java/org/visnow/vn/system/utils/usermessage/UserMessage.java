/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.utils.usermessage;

import java.sql.Timestamp;
import java.util.Calendar;


/**
 * User messages are messages presented to user; this should be the first choice for textual communication TO the user.
 * So (in production) this {@code UserMessage} functionality should be used instead of visible logger lines or {@code System.out.println}.
 *
 *
 * @author szpak
 */
public class UserMessage
{

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private String applicationName;
    private String sourceName;
    //title without any html-like tags
    private String title;
    //title possibliy with html tags apart from <html> and <html/> tags
    private String details;
    private Level level;
    private final Timestamp timestamp;
    private static final String allTagsRE = "<[^<>]+>";
    private static final String[] newLineTagsRE = {"(?i)<br>", "(?i)</tr>", "(?i)<li>"};

    /**
     * Creates new user message.
     * In
     * <code>title</code> all html-like tags and newLines are removed.
     * In
     * <code>details</code> &lt;html&gt; and &lt;/html&gt; tags are removed and all system line separators are replaced with &lt;br&gt;.
     */
    public UserMessage(String applicationName, String sourceName, String title, String details, Level level)
    {
        this.applicationName = applicationName.trim();
        this.sourceName = sourceName.trim();
        this.title = title.replaceAll(allTagsRE, " ").trim(); //remove all html-like tags
        //remove beginning/ending html tags + replace all line separators to <br>
        this.details = details.replaceAll("(?i)<html>", " ").replaceAll("(?i)</html>", " ").replaceAll(LINE_SEPARATOR, "<br>").trim();
        this.level = level;
        Calendar calendar = Calendar.getInstance();
        this.timestamp = new Timestamp(calendar.getTime().getTime());
    }

    /**
     * Returns message description in long or short format. Long format contains message title and details. Short format is just a message title.
     * In long format title and details are separated with single or double newline.
     * Html and standard version is supported.
     * In html mode description is wrapped in &lt;html&gt; and &lt;/html&gt; tags and &lt;br&gt; tag is used as line separator
     *
     * @param longFormat if true then returns title and details; if false then only title is returned
     * @param htmlMode   if true then description is returned in html mode
     *
     */
    public String getDescription(boolean longFormat, boolean htmlMode)
    {
        //double newline in html mode
        String titleDetailsSep = htmlMode ? "<br><br>" : "<br>";
        String description = title + ((longFormat && !details.isEmpty()) ? titleDetailsSep + details : "");
        if (htmlMode)
            return "<html>" + description + "</html>";
        else {
            //replace newLineTagsRE tags to new line
            for (String nlTag : newLineTagsRE)
                description = description.replaceAll(nlTag, LINE_SEPARATOR);
            //TODO: unescape html entities (not only nbsp)
            //replace whitespaces
            description = description.replaceAll("&nbsp;", " ");
            //remove remaining tags 
            return description.replaceAll(allTagsRE, " ").trim();
        }
    }

    /**
     * Returns info in standard (non-html) mode which consists of sourceName and message description.
     */
    public String getInfo(boolean longFormat)
    {
        return sourceName + ": " + getDescription(longFormat, false);
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Same as {@link getInfo(false)}
     *
     * @return
     */
    @Override
    public String toString()
    {
        return getInfo(false);
    }

    /**
     * Returns message level.
     */
    public Level getLevel()
    {
        return level;
    }

    /**
     * Returns application name.
     */
    public String getApplicationName()
    {
        return applicationName;
    }

    /**
     * Returns source name.
     */
    public String getSourceName()
    {
        return sourceName;
    }

    /**
     * Getter to use for special cases. Typically {@link getDescription} should be used.
     * Returns raw title (no html/non-html processing).
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Getter to use for special cases. Typically {@link getDescription} should be used.
     * Returns raw details (no html/non-html processing).
     */
    public String getDetails()
    {
        return details;
    }
}
