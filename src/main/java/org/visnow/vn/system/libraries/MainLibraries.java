/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.libraries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.engine.library.LibraryFolder;
import org.visnow.vn.engine.library.LibraryRoot;
import org.visnow.vn.engine.library.jar.JarLibReader;
import org.visnow.vn.engine.library.jar.JarLibraryRoot;
import org.visnow.vn.lib.types.VNDataAcceptor;
import org.visnow.vn.lib.types.VNDataSchema;
import org.visnow.vn.lib.types.VNDataSchemaComparator;
import org.visnow.vn.lib.types.VNDataSchemaInterface;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class MainLibraries
{

    protected Vector<LibraryRoot> libraries;
    protected LibraryRoot internalLibrary;

    public LibraryRoot getInternalLibrary()
    {
        return internalLibrary;
    }

    public Vector<LibraryRoot> getLibraries()
    {
        return libraries;
    }

    public boolean addLibrary(LibraryRoot library)
    {
        if (library == null)
            return false;
        libraries.add(library);
        return true;
    }

    public boolean deleteLibrary(LibraryRoot library)
    {
        if (library == null)
            return false;
        libraries.remove(library);
        return true;
    }

    public void clearLibraries()
    {
        libraries.clear();
        libraries.add(internalLibrary);
    }

    public MainLibraries()
    {
        this.libraries = new Vector<LibraryRoot>();

        internalLibrary = JarLibReader.readFromJar(VisNow.get().getJarPath());
        libraries.add(internalLibrary);
    }

    public LibraryRoot getLibrary(String name)
    {
        for (LibraryRoot library : libraries)
            if (library.getName().equals(name))
                return library;
        return null;
    }

    public DefaultTreeModel getLibrariesTreeModel()
    {
        DefaultMutableTreeNode mainNode = new DefaultMutableTreeNode();
        for (LibraryRoot library : libraries) {
            mainNode.add(createNodeFromFolder(library.getRootFolder()));
        }

        return new DefaultTreeModel(mainNode);
    }

    public DefaultTreeModel getNameFilteredLibrariesTreeModel(String name)
    {
        DefaultMutableTreeNode mainNode = new DefaultMutableTreeNode();
        for (LibraryRoot library : libraries) {
            mainNode.add(createNameFilteredNodeFromFolder(library.getRootFolder(), name));
        }
        return new DefaultTreeModel(mainNode);
    }

    public DefaultTreeModel getNameAndDescriptionFilteredLibrariesTreeModel(String searchString)
    {
        DefaultMutableTreeNode mainNode = new DefaultMutableTreeNode();
        for (LibraryRoot library : libraries) {
            mainNode.add(createNameAndDescriptionFilteredNodeFromFolder(library.getRootFolder(), searchString));
        }
        return new DefaultTreeModel(mainNode);
    }

    public DefaultTreeModel getTypeFilteredLibrariesTreeModel(String classname)
    {
        return getSchemaFilteredLibrariesTreeModel(classname, null);
    }

    public DefaultTreeModel getSchemaFilteredLibrariesTreeModel(String classname, VNDataSchemaInterface[] schemas)
    {
        DefaultMutableTreeNode mainNode = new DefaultMutableTreeNode();
        for (LibraryRoot library : libraries) {
            mainNode.add(createSchemaFilteredNodeFromFolder(library.getRootFolder(), classname, schemas));
        }
        return new DefaultTreeModel(mainNode);
    }

    private MutableTreeNode createNodeFromFolder(LibraryFolder library)
    {
        DefaultMutableTreeNode mtn = new DefaultMutableTreeNode(library);
        for (LibraryFolder lib : library.getSubFolders()) {
            mtn.add(createNodeFromFolder(lib));
        }
        for (LibraryCore core : library.getCores()) {
            mtn.add(createNodeFromCore(core));
        }
        return mtn;
    }

    private MutableTreeNode createNameFilteredNodeFromFolder(LibraryFolder library, String name)
    {
        DefaultMutableTreeNode mtn = new DefaultMutableTreeNode(library);
        for (LibraryFolder lib : library.getSubFolders()) {
            MutableTreeNode tmp = createNameFilteredNodeFromFolder(lib, name);
            if (tmp.getChildCount() > 0)
                mtn.add(tmp);
        }
        for (LibraryCore core : library.getCores()) {
            MutableTreeNode tmp = createNameFilteredNodeFromCore(core, name);
            if (tmp != null)
                mtn.add(tmp);
        }
        return mtn;
    }

    private MutableTreeNode createNameAndDescriptionFilteredNodeFromFolder(LibraryFolder library, String searchString)
    {
        DefaultMutableTreeNode mtn = new DefaultMutableTreeNode(library);
        for (LibraryFolder lib : library.getSubFolders()) {
            MutableTreeNode tmp = createNameAndDescriptionFilteredNodeFromFolder(lib, searchString);
            if (tmp.getChildCount() > 0)
                mtn.add(tmp);
        }
        for (LibraryCore core : library.getCores()) {
            MutableTreeNode tmp = createNameAndDescriptionFilteredNodeFromCore(core, searchString);
            if (tmp != null)
                mtn.add(tmp);
        }
        
        return mtn;
    }

    private MutableTreeNode createSchemaFilteredNodeFromFolder(LibraryFolder library, String classname, VNDataSchemaInterface[] schemas)
    {
        DefaultMutableTreeNode mtn = new DefaultMutableTreeNode(library);
        for (LibraryFolder lib : library.getSubFolders()) {
            MutableTreeNode tmp = createSchemaFilteredNodeFromFolder(lib, classname, schemas);
            if (tmp.getChildCount() > 0)
                mtn.add(tmp);
        }
        for (LibraryCore core : library.getCores()) {
            MutableTreeNode tmp = createSchemaFilteredNodeFromCore(core, classname, schemas);
            if (tmp != null)
                mtn.add(tmp);
        }
        return mtn;
    }

    private MutableTreeNode createNodeFromCore(LibraryCore core)
    {
        return new DefaultMutableTreeNode(core);
    }

    private MutableTreeNode createNameFilteredNodeFromCore(LibraryCore core, String name)
    {
        if (core == null || name == null) {
            return null;
        }
        if (core.getName().toLowerCase().contains(name.toLowerCase()))
            return new DefaultMutableTreeNode(core);
        else
            return null;
    }

    private MutableTreeNode createNameAndDescriptionFilteredNodeFromCore(LibraryCore core, String searchString)
    {
        if (core == null || searchString == null) {
            return null;
        }
        if (core.getName().toLowerCase().contains(searchString.toLowerCase()) ||
            (core.getShortDescription() != null && core.getShortDescription().toLowerCase().contains(searchString.toLowerCase()))||
                (core.getKeyWords()!=null && core.getKeyWords().toLowerCase().contains(searchString.toLowerCase())))
            return new DefaultMutableTreeNode(core);
        else
            return null;
    }

    private MutableTreeNode createSchemaFilteredNodeFromCore(LibraryCore core, String classname, VNDataSchemaInterface[] schemas)
    {
        if (core == null)
            return null;

        DefaultMutableTreeNode ret = new DefaultMutableTreeNode(core);
        boolean anything = false;
        HashMap<String, String> str = core.getInputTypes();
        HashMap<String, VNDataAcceptor[]> vndasList = core.getInputVNDataAcceptors();
        if (str == null)
            return null;
        for (Entry<String, String> e : str.entrySet()) {
            try {
                if (Class.forName(e.getValue()).isAssignableFrom(Class.forName(classname))) {

                    VNDataAcceptor[] vndas = vndasList.get(e.getKey());
                    boolean acceptableEntry = false;
                    boolean conditionalAccept = false;
                    if (schemas == null || vndas == null || vndas.length == 0) {

                        acceptableEntry = true;
                        conditionalAccept = true;

                    } else {
                        here:
                        for (int i = 0; i < vndas.length; i++) {
                            for (int j = 0; j < schemas.length; j++) {

                                boolean tmp = VNDataSchemaComparator.isCompatible(schemas[j], vndas[i].getVNDataSchemaInterface(), vndas[i].getVNDataCompatibilityMask());
                                if (tmp) {
                                    acceptableEntry = true;
                                    break here;
                                }

                                if (schemas[j] instanceof VNDataSchema) {
                                    long schemaMask = VNDataSchemaComparator.createComparatorFromSchemaParams(((VNDataSchema) schemas[j]).getParamsList());
                                    long acceptorMask = vndas[i].getVNDataCompatibilityMask();
                                    tmp = VNDataSchemaComparator.isConditionallyCompatible(schemas[j], schemaMask, vndas[i].getVNDataSchemaInterface(), acceptorMask);
                                    if (tmp) {
                                        acceptableEntry = true;
                                        conditionalAccept = true;
                                        break here;
                                    }
                                }
                            }
                        }
                    }

                    if (acceptableEntry) {
                        anything = true;
                        DefaultMutableTreeNode node = new DefaultMutableTreeNode(e.getKey(), !conditionalAccept);
                        ret.add(node);
                    }

                }
            } catch (ClassNotFoundException ex) {
//                System.out.println("Class not found: [" + classname + "] or [" + e.getValue() + "] in core [" + core.getName() + "]");
                //Displayer.ddisplay(201002091100L, e, this, "ERROR IN LIBRARY? Class not found.");
            }
        }

        return (anything) ? ret : null;
    }

    public ArrayList<ClassLoader> getAllClassLoaders()
    {
        ArrayList<ClassLoader> out = new ArrayList<>();
        for (LibraryRoot library : libraries) {
            if (library instanceof JarLibraryRoot) {
                out.add(((JarLibraryRoot) library).getLoader());
            }
        }
        return out;
    }

}
