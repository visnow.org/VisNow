/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.framework;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.visnow.vn.engine.library.LibraryRoot;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNLibListCellRenderer implements ListCellRenderer
{

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        JPanel panel = new JPanel();
        if (isSelected)
            panel.setBackground(new Color(128, 128, 128));
        else
            panel.setBackground(new Color(255, 255, 255));

        JLabel textLabel = new JLabel("  " + ((LibraryRoot) value).getName() + "  ");
        JLabel iconLabel;
        if (textLabel.getText().equalsIgnoreCase("  internal  "))
            iconLabel = new JLabel(new ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/libraries/kalfa_intFolder48.png")));
        else
            iconLabel = new JLabel(new ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/libraries/kalfa_jarFolder48.png")));

        panel.setLayout(new GridBagLayout());
        java.awt.GridBagConstraints iconConstraints = new GridBagConstraints();
        java.awt.GridBagConstraints textConstraints = new GridBagConstraints();
        textConstraints.gridx = 0;
        textConstraints.gridy = 1;
        panel.add(iconLabel, iconConstraints);
        panel.add(textLabel, textConstraints);

        return panel;
    }
}
