/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.framework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 * User message extension used in UserMessagePanel.
 * This includes: column order, expanded flag.
 * <p>
 * @author szpak
 */
public class UserMessageRow extends UserMessage
{

    private boolean expanded = false;

    /**
     * Just a wrapper around UserMessage constructor. Constructs new UserMessageRow with the same values as in <code>userMessage</code>.
     * By default userMessageRow is collapsed (<code>expanded == false</code>).
     */
    public UserMessageRow(UserMessage userMessage)
    {
        super(userMessage.getApplicationName(), userMessage.getSourceName(), userMessage.getTitle(), userMessage.getDetails(), userMessage.getLevel());
    }

    public static enum ColumnName
    {

        LEVEL, APPLICATION, SOURCE, DESCRIPTION, TIMESTAMP
    }

    //Column order to display; 
    public static final ColumnName[] COLUMN_ORDER = {ColumnName.LEVEL, ColumnName.APPLICATION, ColumnName.SOURCE, ColumnName.DESCRIPTION, ColumnName.TIMESTAMP};

    /**
     * Returns
     * <code>columnName</code> column index according to
     * <code>COLUMN_ORDER</code>. Throws {@code IllegalArgumentException} if
     * <code>rowName</code> is not on the list.
     */
    public static int getColumnIndex(ColumnName columnName)
    {
        for (int i = 0; i < COLUMN_ORDER.length; i++)
            if (COLUMN_ORDER[i] == columnName)
                return i;
        throw new IllegalArgumentException("Column name: " + columnName + " is not specified in COLUMN_ORDER: " + Arrays.toString(COLUMN_ORDER));
    }

    /**
     * Returns column name at selected
     * <code>index</code>
     */
    public static ColumnName getColumnName(int index)
    {
        return COLUMN_ORDER[index];
    }

    //    /**
    //     * Returns this message as an array of Strings in order
    //     * <code>COLUMN_ORDER</code>
    //     *
    //     * @param longFormat true for long format
    //     */
    //    public String[] toRow(boolean longFormat) {
    //        List<String> row = new ArrayList<String>();
    //
    //        for (ColumnName columnName : COLUMN_ORDER)
    //            switch (columnName) {
    //                case LEVEL:
    //                    row.add(level.name());
    //                    break;
    //                case APPLICATION:
    //                    row.add(applicationName);
    //                    break;
    //                case SOURCE:
    //                    row.add(sourceName);
    //                    break;
    //                case DESCRIPTION:
    //                    row.add(getDescription(longFormat, true));
    //                    break;
    //            }
    //
    //        return row.toArray(new String[row.size()]);
    //    }
    /**
     * Returns fake row. This method gives table of length dependent on {@link COLUMN_ORDER} of same copy this UserMessageRow.
     * It's cell renderer that gets proper cell value from the row.
     */
    public UserMessageRow[] toFakeRow()
    {
        List<UserMessageRow> fakeRow = new ArrayList<UserMessageRow>(COLUMN_ORDER.length);
        for (int i = 0; i < COLUMN_ORDER.length; i++)
            fakeRow.add(this);

        return fakeRow.toArray(new UserMessageRow[fakeRow.size()]);
    }

    /**
     * Toggles expanded.
     */
    public void toggleExpanded()
    {
        setExpanded(!expanded);
    }

    /**
     * Returns expanded flag
     */
    public boolean isExpanded()
    {
        return expanded;
    }

    /**
     * Sets expanded flag to <code>expanded</code>
     */
    public void setExpanded(boolean expanded)
    {
        this.expanded = expanded;
    }
}
