/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

/**
 *
 * @author gacek
 */
public class FBox implements Transferable
{

    private Component component;

    public Component getComponent()
    {
        return component;
    }

    private String name;

    public String getName()
    {
        return name;
    }

    private FPlace place;

    protected FPlace getPlace()
    {
        return place;
    }

    protected void setPlace(FPlace place)
    {
        this.place = place;
    }

    public FBox(String name, Component component)
    {
        this.name = name;
        this.component = component;
    }

    public DataFlavor[] getTransferDataFlavors()
    {
        return new DataFlavor[]{FSplitUI.BoxFlavor};
    }

    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        return (flavor.equals(FSplitUI.BoxFlavor));
    }

    public Object getTransferData(DataFlavor flavor)
    {
        return this;
    }

    void detach()
    {
        place.removeBox(this);
    }

    void detachToFrame(Point p)
    {
        //MAMY! FBox box = (FBox)(dtde.getTransferable().getTransferData(FSplitUI.BoxFlavor));
        FAreaMajor boxMajor = getPlace().getParentArea().getMajor();
        place.removeBox(this);

        FAreaMajor newMajor = new FAreaMajor(false, this.getPlace().getParentArea().getMajor().getSplitSystem());
        FComponentViewer v = new FComponentViewer(newMajor, "Palette", 250, 400, true, FComponentViewer.DISPOSE_ON_CLOSE);
        newMajor.setFrame(v);
        v.setVisible(true);
        v.setLocation(p.x - 125, p.y - 200);

        newMajor.addBox(this);
        boxMajor.checkRemoval();

    }

}
