/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing;

import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

/**
 * Simple (table) cell editor for Enums (based on toString method).
 * Works well with default cell renderer (that just uses toString).
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class EnumCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private List<Enum> values;
    private Enum currentValue;
    private JLabel label = new JLabel();
    private boolean allowNulls;

    public EnumCellEditor(Enum[] values, boolean allowNulls)
    {
        super();
        this.values = Arrays.asList(values);
        label.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
        this.allowNulls = allowNulls;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, final Object value, boolean isSelected, int row, int column)
    {
        //this is to react on first (and only) click (entering edit mode)
        if (value == null && !allowNulls) throw new IllegalArgumentException("Invalid null value");
        if (value == null) {
            label.setText("");
            currentValue = null;
        } else {
            currentValue = values.get((values.indexOf(value) + 1) % values.size());
            label.setText(currentValue.toString());
        }

        //this is to force JTable to notice the event
        //immediately exit editing mode
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                if (value == null)
                    fireEditingCanceled();
                else
                    fireEditingStopped();
            }
        });
        return label;
    }

    @Override
    public Object getCellEditorValue()
    {
        return currentValue;
    }
}
