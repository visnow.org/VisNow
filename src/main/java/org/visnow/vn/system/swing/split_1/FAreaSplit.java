/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.BorderLayout;
import javax.swing.JSplitPane;

/**
 *
 * @author gacek
 */
public class FAreaSplit extends FAreaSplittable
{

    protected boolean isMajor()
    {
        return false;
    }

    protected boolean isSplit()
    {
        return true;
    }

    protected boolean isSingle()
    {
        return false;
    }

    private FAreaSplittable son = new FAreaSingle(this);
    private FAreaSplittable daughter = new FAreaSingle(this);
    private JSplitPane pane = new JSplitPane();

    public FAreaSplittable getSon()
    {
        return son;
    }

    public FAreaSplittable getDaughter()
    {
        return daughter;
    }

    public FPlace getSomePlace()
    {
        return son.getSomePlace();
    }

    //    public FAreaSplit(FArea parent) {
    //        super(parent);
    //        pane.setLeftComponent(son);
    //        pane.setRightComponent(daughter);
    //    }
    protected FAreaSplit(FArea parent, FAreaSplittable one, FAreaSplittable two, int direction)
    {
        super(parent);

        if (direction == topD || direction == bottomD) {
            pane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        } else {
            pane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        }
        pane.setDividerSize(9);
        pane.setOneTouchExpandable(true);

        if (direction == topD || direction == leftD) {
            son = two;
            daughter = one;
        } else {
            son = one;
            daughter = two;
        }

        pane.setLeftComponent(son);
        pane.setRightComponent(daughter);

        parent.replaceChild(one, this);
        one.setParentArea(this);
        two.setParentArea(this);

        getBottomLayer().add(pane, BorderLayout.CENTER);

        parent.validate();
        if (direction == topD || direction == bottomD) {
            pane.setDividerLocation(this.getHeight() / 2);
        } else {
            pane.setDividerLocation(this.getWidth() / 2);
        }

    }

    protected void replaceChild(FAreaSplittable oldChild, FAreaSplittable newChild)
    {
        int div = pane.getDividerLocation();
        if (son.equals(oldChild)) {
            son = newChild;
            pane.setLeftComponent(son);
        } else {
            daughter = newChild;
            pane.setRightComponent(daughter);
        }
        pane.setDividerLocation(div);
    }

    public void setDividerLocation(int d)
    {
        this.pane.setDividerLocation(d);
    }

    @Override
    public void addBox(FBox box, int direction)
    {
        if (direction == centerD) {
            son.addBox(box);
            return;
        }

        FAreaSingle brother = new FAreaSingle(null);
        brother.addBox(box);
        new FAreaSplit(getParentArea(), this, brother, direction);
    }

    @Override
    protected void resize()
    {
        super.resize();
        son.resize();
        daughter.resize();
        //TODO WHAT?
    }

    //protected void doDrop(FBox box, int dir) {
    //    addBox(box, dir);
    //}
    protected String writeXML(int i)
    {
        String d = "";
        for (int j = 0; j < i; ++j)
            d += "  ";
        String ret = d + "<split dir=\"";
        ret += (pane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT) ? "horizontal" : "vertical";
        ret += "\">\n";
        ret += this.getSon().writeXML(i + 1);
        ret += this.getDaughter().writeXML(i + 1);
        ret += d + "</split>\n";
        return ret;
    }
}
