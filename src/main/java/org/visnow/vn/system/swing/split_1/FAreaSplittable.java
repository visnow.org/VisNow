/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 *
 * @author gacek
 */
public abstract class FAreaSplittable extends FArea
{

    //<editor-fold defaultstate="collapsed" desc=" Directions ">
    private final static String[] direction = new String[]{
        BorderLayout.CENTER,
        BorderLayout.NORTH,
        BorderLayout.SOUTH,
        BorderLayout.WEST,
        BorderLayout.EAST
    };
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" Layout ">
    private final static Integer bottomI = new Integer(1);
    private final static Integer topI = new Integer(10);

    private JLayeredPane layers = new JLayeredPane();
    private JPanel bottomLayer = new JPanel();
    private JPanel topLayer = new JPanel();

    protected JPanel getBottomLayer()
    {
        return bottomLayer;
    }

    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    private JPanel leftPanel = new JPanel();
    private JPanel rightPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" Active Panels ">
    private JPanel[] aPanels = new JPanel[]{centerPanel, topPanel, bottomPanel, leftPanel, rightPanel};

    private int activePanel = FAreaMajor.nullD;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Structure ">
    protected FAreaMajor getMajor()
    {
        return parent.getMajor();
    }

    private FArea parent;

    public FArea getParentArea()
    {
        return parent;
    }

    public void setParentArea(FArea parent)
    {
        this.parent = parent;
    }

    //</editor-fold>
    public FAreaSplittable(FArea parent)
    {
        this.parent = parent;

        this.setLayout(new BorderLayout());
        this.add(layers, BorderLayout.CENTER);

        bottomLayer.setLayout(new BorderLayout());
        bottomLayer.setBounds(0, 0, 500, 500);
        bottomLayer.setBackground(Color.yellow);
        layers.add(bottomLayer, bottomI);

        topLayer.setLayout(new BorderLayout());
        topLayer.setBounds(0, 0, 500, 500);
        topLayer.setOpaque(false);
        layers.add(topLayer, topI);

        for (int i = FAreaMajor.centerD; i < FAreaMajor.lastD; ++i) {
            aPanels[i].setOpaque(false);
            topLayer.add(aPanels[i], direction[i]);
            if (i != FAreaMajor.centerD)
                aPanels[i].setPreferredSize(FSplitUI.DD);
        }

        layers.addComponentListener(new ComponentListener()
        {
            public void componentResized(ComponentEvent e)
            {
                resize();
            }

            public void componentMoved(ComponentEvent e)
            {
                resize();
            }

            public void componentShown(ComponentEvent e)
            {
                resize();
            }

            public void componentHidden(ComponentEvent e)
            {
            }
        });

        //DropTarget dt = new DropTarget(this,this);
    }

    //<editor-fold defaultstate="collapsed" desc=" Border Activation ">
    private void activatePanel(int dir)
    {
        if (activePanel != FAreaMajor.nullD && activePanel != dir) {
            aPanels[activePanel].setBorder(null);
        }
        activePanel = dir;
        aPanels[activePanel].setBorder(FSplitUI.border);
        repaint();
    }

    private void deactivatePanels()
    {
        activePanel = FAreaMajor.nullD;
        for (JPanel jp : aPanels)
            jp.setBorder(null);
        repaint();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Activate Border ">
    private void activateBorder(int x, int y)
    {
        if (y < FSplitUI.D) {
            activatePanel(topD);
        } else if (y > getHeight() - FSplitUI.D) {
            activatePanel(bottomD);
        } else if (x < FSplitUI.D) {
            activatePanel(leftD);
        } else if (x > getWidth() - FSplitUI.D) {
            activatePanel(rightD);
        } else {
            if (isSingle()) {
                activatePanel(centerD);
            } else {
                deactivatePanels();
            }
        }
    }

    //</editor-fold>
    public void dragEnter(DragEvent dtde)
    {
        if (!dtde.isTypeSupported(FSplitUI.BoxFlavor))
            return;
    }

    public void dragOver(DragEvent dtde)
    {
        if (!dtde.isTypeSupported(FSplitUI.BoxFlavor))
            return;
        Point p = dtde.getLocation();
        activateBorder(p.x, p.y);
    }

    public void dragExit(DragEvent dte)
    {
        deactivatePanels();
    }

    public void drop(DragEvent dtde)
    {
        if (!dtde.isTypeSupported(FSplitUI.BoxFlavor))
            return;
        int dir = activePanel;
        deactivatePanels();
        //try {
        FBox box = (FBox) (dtde.getObject(FSplitUI.BoxFlavor));
        FAreaMajor boxMajor = box.getPlace().getParentArea().getMajor();
        //FPlace boxPlace = box.getPlace();
        box.detach();
        addBox(box, dir);
        //boxPlace.validate();
        boxMajor.checkRemoval();

        //}
        validate();
    }

    //protected abstract void doDrop(FBox box, int dir);
    protected void resize()
    {
        topLayer.setBounds(0, 0, this.getWidth(), this.getHeight());
        bottomLayer.setBounds(0, 0, this.getWidth(), this.getHeight());
        //topLayer.invalidate();
        //bottomLayer.invalidate();
        topLayer.validate();
        bottomLayer.validate();
        //TODO WHAT?
    }

    protected abstract String writeXML(int d);
}
