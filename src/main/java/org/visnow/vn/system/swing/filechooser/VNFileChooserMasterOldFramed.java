/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.filechooser;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNFileChooserMasterOldFramed
{

    //private JComponentViewer frame;
    private JDialog frame;
    //private JFileChooser ch;
    //JOptionPane p;
    private VNFileChooser panel;
    private Runnable start;
    private final Object lock = new Object();

    private boolean fileChosen = false;

    //<editor-fold defaultstate="collapsed" desc=" [CONSTRUCTOR] ">
    protected VNFileChooserMasterOldFramed(VNFileChooser chooser)
    {
        panel = chooser;
        //frame = new JComponentViewer(panel, "Select file", 800, 500, true, false);
        frame = new JDialog(VisNow.get().getMainWindow(), "Select file", false);
        frame.setContentPane(panel);
        frame.setSize(800, 500);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosed(WindowEvent e)
            {
                dialogClosed(false);
            }
        });

        //        dialog = new JDialog((JFrame)null, "Select file", true);
        //        dialog.addWindowListener(new WindowAdapter() {
        //            @Override
        //            public void windowClosed(WindowEvent e) {
        //                dialogClosed(false);
        //            }
        //        });
        start = new Runnable()
        {
            public void run()
            {
                runnableRun();
            }
        };
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" start ">
    //    protected boolean showDialog() {
    //        SwingWorker sw = new SwingWorker() {
    //            @Override
    //            protected Object doInBackground() throws Exception {
    //                return showWorkerDialog();
    //            }
    //
    //        };
    //        sw.execute();
    //        try {
    //            return ((Boolean) sw.get()).booleanValue();
    //        } catch (InterruptedException ex) {
    //            return false;
    //        } catch (ExecutionException ex) {
    //            return false;
    //        }
    //    }
    protected boolean showDialog()
    {

        panel.showing();
        frame.setVisible(true);
        frame.setLocationRelativeTo(VisNow.get().getMainWindow());

        /*
         Thread t = new Thread(start);
         t.start();
         try {
         synchronized(lock){lock.wait();}
         } catch (InterruptedException ex) {
         frame.setVisible(false);
         return false;
         }
         */
        boolean ret = fileChosen;
        fileChosen = false;
        return ret;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" runnable ">
    private void runnableRun()
    {
        panel.showing();
        frame.setVisible(true);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" stop ">
    protected void dialogClosed(boolean confirmed)
    {
        fileChosen = confirmed;
        frame.setVisible(false);
        synchronized (lock) {
            lock.notifyAll();
        }
        panel.windowCancel();
    }
    //</editor-fold>

}
