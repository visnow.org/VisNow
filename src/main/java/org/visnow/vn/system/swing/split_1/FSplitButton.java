/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.JButton;

/**
 *
 * @author gacek
 */
public class FSplitButton extends JButton
{

    //private DragSource dragSource;
    private FBox box;

    //private FBox getBox() {return box;}
    public FSplitButton(String text, FBox box)
    {
        super(text);
        this.setBackground(FSplitUI.normalColor);
        //        this.dragSource = DragSource.getDefaultDragSource();
        this.box = box;

        //      this.dragSource.createDefaultDragGestureRecognizer(
        //        this,
        //      DnDConstants.ACTION_COPY_OR_MOVE,
        //    this
        //  );
        //this.getTransferHandler();
    }

    public void setActive(boolean b)
    {
        this.setBackground((b) ? FSplitUI.highColor : FSplitUI.normalColor);
    }

    public void dragDropEnd(DragEvent dsde)
    {
        this.box.getPlace().getParentArea().getMajor().getSplitSystem().setInternalTargetsActive(true);
        Point p = dsde.getLocation();
        Frame[] frames = Frame.getFrames();
        //System.out.println("OPUSZCZANIE, na punkt ("+p.x+"x"+p.y+"), przy "+frames.length+" ramkach.");
        for (Frame f : frames) {
            if (f.contains(p.x - f.getX(), p.y - f.getY())) {
                //        System.out.println("PUNKT ZAWARTY!");
                return;
            }
        }
        //System.out.println("PUNKT WOLNY!");
        box.detachToFrame(p);
    }

    public void dragGestureRecognized(DragEvent dge)
    {
        System.out.println("DGR!!");
        this.box.getPlace().getParentArea().getMajor().getSplitSystem().setInternalTargetsActive(false);
        dge.startDrag(FSplitUI.dragCursor, box, this);
        this.cancelListeners();
    }

    //<editor-fold defaultstate="collapsed" desc=" Listeners ">
    private Vector<ActionListener> listeners = new Vector<ActionListener>();

    private void cancelListeners()
    {
        while (this.getActionListeners().length > 0) {
            listeners.add(this.getActionListeners()[0]);
            this.removeActionListener(this.getActionListeners()[0]);
        }
        this.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                restoreListeners();
            }
        });
    }

    public void restoreListeners()
    {
        while (this.getActionListeners().length > 0) {
            this.removeActionListener(this.getActionListeners()[0]);
        }
        for (ActionListener al : listeners) {
            this.addActionListener(al);
        }
        listeners.clear();
    }
    //</editor-fold>

}
