/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.filechooser;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNFileChooserMaster
{

    private JDialog frame;
    private VNFileChooser panel;
    boolean fileChosen;

    //<editor-fold defaultstate="collapsed" desc=" [CONSTRUCTOR] ">
    protected VNFileChooserMaster(VNFileChooser chooser)
    {
        panel = chooser;
        frame = new JDialog(VisNow.get().getMainWindow(), "Select file", true);
        frame.setContentPane(panel);
        frame.setSize(800, 500);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosed(WindowEvent e)
            {
                dialogClosedManually();
            }

        });

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" start ">
    protected boolean showDialog(String title)
    {
        frame.setTitle(title);
        return showDialog();
    }

    protected boolean showDialog()
    {

        panel.showing();
        frame.setVisible(true);
        frame.setLocationRelativeTo(VisNow.get().getMainWindow());

        boolean ret = fileChosen;
        fileChosen = false;
        return ret;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" stop ">
    protected void dialogClosed(boolean confirmed)
    {
        fileChosen = confirmed;
        frame.setVisible(false);
        //panel.windowCancel();
    }

    private void dialogClosedManually()
    {
        fileChosen = false;
        frame.setVisible(false);
        panel.windowCancel();
    }

    //</editor-fold>
}
