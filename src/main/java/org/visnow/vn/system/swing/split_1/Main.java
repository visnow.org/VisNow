/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author gacek
 */
public class Main
{

    public static void Debug1(String text)
    {
        System.out.println(text);
    }

    private static Color[] tab = new Color[]{
        new Color(100, 000, 000),
        new Color(200, 100, 000),
        new Color(200, 200, 000),
        new Color(100, 200, 000),
        new Color(000, 200, 200),
        new Color(000, 100, 200),
        new Color(000, 000, 200)
    };

    public static void main(String[] args)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                FAreaMajor a = new FAreaMajor(true);
                FComponentViewer v1 = new FComponentViewer(a, "test", 400, 600, true, FComponentViewer.EXIT_ON_CLOSE);
                v1.setVisible(true);

                JPanel j = new JPanel();
                j.setBackground(tab[1]);
                a.getSomePlace().addBox("tab1", j);

                j = new JPanel();
                j.setBackground(tab[4]);
                a.getSomePlace().addBox("tab4", j);

                j = new JPanel();
                j.setBackground(tab[5]);
                a.getSomePlace().addBox("tab5", j);

                a = new FAreaMajor(false);
                v1 = new FComponentViewer(a, "bitest", 400, 600, true, FComponentViewer.DISPOSE_ON_CLOSE);
                a.setFrame(v1);
                v1.setVisible(true);

                j = new JPanel();
                j.setBackground(tab[0]);
                a.getSomePlace().addBox("tab0", j);

                j = new JPanel();
                j.setBackground(tab[2]);
                a.getSomePlace().addBox("tab2", j);

                j = new JPanel();
                j.setBackground(tab[3]);
                a.getSomePlace().addBox("tab3", j);

            }
        });
    }

    private Main()
    {
    }

}
