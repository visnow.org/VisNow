/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

/**
 * Static loader for UIManager icons. This class provides functionality for loading (and caching) icons in original size and resized.
 *
 * @author szpak
 */
public class UIIconLoader
{

    //map with String as a key (IconType.name() + _ + width + _ + height)
    static Map<String, Icon> icons = new HashMap<String, Icon>();

    /**
     * Returns icon for passed
     * <code>iconType</code>. This method cache loaded icons.
     *
     * @return icon or null if such icon cannot be found
     */
    public static Icon getIcon(UIManagerKey iconKey)
    {
        if (!icons.containsKey(cacheKey(iconKey))) {
            Icon icon = UIManager.getIcon(iconKey.getKey());
            //return null if not found
            if (icon == null)
                return null;
            icons.put(cacheKey(iconKey), icon);
        }
        return icons.get(cacheKey(iconKey));
    }

    /**
     * Returns icon for passed
     * <code>iconType</code> and size. This method cache loaded icons.
     *
     * @return icon or null if such icon cannot be found
     */
    public static Icon getIcon(UIManagerKey iconKey, int width, int height)
    {
        if (iconKey == null)
            throw new NullPointerException("iconKey cannot be null");

        if (!icons.containsKey(cacheKey(iconKey, width, height))) {
            Icon icon = getIcon(iconKey);
            //return null if icon not found
            if (icon == null)
                return null;
            if (icon instanceof ImageIcon) {
                Image img = ((ImageIcon) icon).getImage();
                Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
                icon = new ImageIcon(newimg);
            }
            icons.put(cacheKey(iconKey, width, height), icon);
        }
        return icons.get(cacheKey(iconKey, width, height));
    }

    /**
     * Key to get/store icons with specified size.
     */
    private static String cacheKey(UIManagerKey iconKey, int width, int height)
    {
        return iconKey.getKey() + "_" + width + "_" + height;
    }

    /**
     * Key to get/store icons without specified size.
     */
    private static String cacheKey(UIManagerKey iconKey)
    {
        //added __ to make it unique comparing to width x height
        return iconKey.getKey() + "_" + "_";
    }
}
