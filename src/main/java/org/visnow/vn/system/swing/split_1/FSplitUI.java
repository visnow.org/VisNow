/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.border.LineBorder;

/**
 *
 * @author gacek
 */
public class FSplitUI
{

    public final static Color normalColor = new Color(200, 200, 200);
    public final static Color highColor = new Color(240, 245, 255);
    public final static Cursor dragCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

    public final static int header = 25;

    public final static String BoxFlavor = "foxtrot/fbox";

    public final static int D = 50;
    public final static Dimension DD = new Dimension(D, D);

    public final static LineBorder border = new LineBorder(new Color(255, 0, 0), 4);

    private FSplitUI()
    {
    }

}
