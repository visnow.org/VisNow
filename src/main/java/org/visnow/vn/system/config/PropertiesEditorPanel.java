/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.config;

import java.awt.Rectangle;
import java.util.Hashtable;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.InfinityAction;
import org.visnow.jscic.utils.NaNAction;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.widgets.ColorMapCellRenderer;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author babor
 */
public class PropertiesEditorPanel extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(PropertiesEditorPanel.class);
    private static final Hashtable<Integer, JLabel> performanceLabels =       new Hashtable<>();
    private static final Hashtable<Integer, JLabel> graphicCapabilityLabels = new Hashtable<>();

    private String preferredMainWindowPosition = "";
    private String preferredViewer3DWindowPosition = "";

    private ColorMapCellRenderer comboBoxRenderer;

    static
    {
        performanceLabels.put(0, new JLabel("low end"));
        performanceLabels.put(2, new JLabel("standard"));
        performanceLabels.put(4, new JLabel("extreme"));
        graphicCapabilityLabels.put(0,  new JLabel("1M"));
        graphicCapabilityLabels.put(2,  new JLabel("4M"));
        graphicCapabilityLabels.put(4,  new JLabel("16M"));
        graphicCapabilityLabels.put(6,  new JLabel("64M"));
        graphicCapabilityLabels.put(8,  new JLabel("256M nodes"));
    }
    /**
     * Creates new form PropertiesEditorPanel
     */
    public PropertiesEditorPanel()
    {
        initComponents();
    }

    public void init()
    {
        JLabel lowEndLabel = new JLabel("low end");
        lowEndLabel.setFont(performanceSlider.getFont());
        performanceSlider.setLabelTable(performanceLabels);
        graphicsCapabilitySlider.setLabelTable(graphicCapabilityLabels);
        autoconnectViewerCB.setSelected(VisNow.get().getMainConfig().isAutoconnectViewer());

        startupViewer2DCB.setSelected(VisNow.get().getMainConfig().isStartupViewer2D());
        startupViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupViewer3D());
        startupFieldViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupFieldViewer3D());
        startupOrthoViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupOrthoViewer3D());
        autoconnectOrthoViewer3DCB.setEnabled(startupOrthoViewer3DCB.isSelected());

        defAppDirTF.setText(VisNow.get().getMainConfig().getDefaultApplicationsPath());
        String tmp = VisNow.get().getMainConfig().getUsableApplicationsPathType();
        if (tmp.equalsIgnoreCase("last")) {
            useAppDirCB.setSelectedItem("last");
        } else if (tmp.equalsIgnoreCase("home")) {
            useAppDirCB.setSelectedItem("home");
        } else if (tmp.equalsIgnoreCase("default")) {
            useAppDirCB.setSelectedItem("default");
        }

        defDatDirTF.setText(VisNow.get().getMainConfig().getDefaultDataPath());
        tmp = VisNow.get().getMainConfig().getUsableDataPathType();
        if (tmp.equalsIgnoreCase("last")) {
            useDatDirCB.setSelectedItem("last");
        } else if (tmp.equalsIgnoreCase("home")) {
            useDatDirCB.setSelectedItem("home");
        } else if (tmp.equalsIgnoreCase("default")) {
            useDatDirCB.setSelectedItem("default");
        }
        infCombo.setSelectedIndex(VisNow.get().getMainConfig().getInfAction());
        nanCombo.setSelectedIndex(VisNow.get().getMainConfig().getNaNAction());
        int nThreads = VisNow.availableProcessors();
        if (nThreads >= Runtime.getRuntime().availableProcessors()) {
            allThreadsRB.setSelected(true);
            threadsSP.setEnabled(false);
            threadsSP.setValue(nThreads);
        } else {
            limitedThreadsRB.setSelected(true);
            threadsSP.setEnabled(true);
            threadsSP.setValue(nThreads);
        }
        comboBoxRenderer = new ColorMapCellRenderer();
        colorMapCombo.setRenderer(comboBoxRenderer);
        colorMapCombo.setModel(ColorMapManager.getInstance().getColorMap1DListModel());
        colorMapCombo.setSelectedIndex(VisNow.get().getMainConfig().getDefaultColorMap());

        preferredMainWindowPosition = VisNow.get().getMainConfig().getMainWindowBounds();
        if (preferredMainWindowPosition == null) preferredMainWindowPosition = "";
        preferredViewer3DWindowPosition = VisNow.get().getMainConfig().getViewer3DWindowBounds();
        if (preferredViewer3DWindowPosition == null) preferredViewer3DWindowPosition = "";

        mainWindowSizeBG.clearSelection();
        viewer3DWindowSizeBG.clearSelection();
        mainWindowAutoButton.setSelected(preferredMainWindowPosition.equals(""));
        mainWindowCurrentButton.setSelected(preferredMainWindowPosition.equals(getMainWindowBounds()));
        viewer3DWindowAutoButton.setSelected(preferredViewer3DWindowPosition.equals(""));
        viewer3DWindowCurrentButton.setSelected(preferredViewer3DWindowPosition.equals(getViewer3DBounds()));

        surfaceOffsetTF.setText(""+VisNow.get().getMainConfig().getRenderingSurfaceOffset());
        outlineBox.setSelected(VisNow.get().getMainConfig().isDefaultOutline());
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        mainWindowSizeBG = new javax.swing.ButtonGroup();
        viewer3DWindowSizeBG = new javax.swing.ButtonGroup();
        hardwarePerformancePanel = new javax.swing.JPanel();
        performanceSlider = new javax.swing.JSlider();
        graphicsCapabilitySlider = new javax.swing.JSlider();
        windowSizePanel = new javax.swing.JPanel();
        mainWindowSizePanel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        mainWindowCurrentButton = new javax.swing.JToggleButton();
        mainWindowAutoButton = new javax.swing.JToggleButton();
        viewer3DWindowSizePanel = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        viewer3DWindowCurrentButton = new javax.swing.JToggleButton();
        viewer3DWindowAutoButton = new javax.swing.JToggleButton();
        viewerDefaultPanel = new javax.swing.JPanel();
        startupViewer2DCB = new javax.swing.JCheckBox();
        startupViewer3DCB = new javax.swing.JCheckBox();
        startupFieldViewer3DCB = new javax.swing.JCheckBox();
        startupOrthoViewer3DCB = new javax.swing.JCheckBox();
        viewerAutoConnectPanel = new javax.swing.JPanel();
        autoconnectViewerCB = new javax.swing.JCheckBox();
        autoconnectOrthoViewer3DCB = new javax.swing.JCheckBox();
        fileChoosersPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        defAppDirTF = new javax.swing.JTextField();
        defAppDirBB = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        useAppDirCB = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        defDatDirTF = new javax.swing.JTextField();
        defDatDirBB = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        useDatDirCB = new javax.swing.JComboBox();
        unitPanel = new javax.swing.JPanel();
        lengthUnitCombo = new javax.swing.JComboBox();
        timeUnitCombo = new javax.swing.JComboBox();
        velocityUnitCombo = new javax.swing.JComboBox();
        colormapPanel = new javax.swing.JPanel();
        colorMapCombo = new javax.swing.JComboBox();
        renderingPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        surfaceOffsetTF = new javax.swing.JTextField();
        outlineBox = new javax.swing.JCheckBox();
        multithreadingPanel = new javax.swing.JPanel();
        allThreadsRB = new javax.swing.JRadioButton();
        limitedThreadsRB = new javax.swing.JRadioButton();
        threadsSP = new javax.swing.JSpinner();
        exceptionsPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        nanCombo = new org.visnow.vn.gui.widgets.SteppedComboBox();
        infCombo = new org.visnow.vn.gui.widgets.SteppedComboBox();

        setLayout(new java.awt.GridBagLayout());

        hardwarePerformancePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Hardware performance"));
        hardwarePerformancePanel.setMinimumSize(new java.awt.Dimension(282, 120));
        hardwarePerformancePanel.setName("hardwarePerformancePanel"); // NOI18N
        hardwarePerformancePanel.setPreferredSize(new java.awt.Dimension(282, 120));
        hardwarePerformancePanel.setLayout(new java.awt.GridLayout(2, 0));

        performanceSlider.setMajorTickSpacing(2);
        performanceSlider.setMaximum(4);
        performanceSlider.setMinorTickSpacing(1);
        performanceSlider.setPaintLabels(true);
        performanceSlider.setToolTipText("<html>for low end machines only small objects will be continuously updated<p>to avoid annoying sliders' behavior</html>"); // NOI18N
        performanceSlider.setValue(1);
        performanceSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("overall performance (processor and graphics)"));
        performanceSlider.setName("performanceSlider"); // NOI18N
        hardwarePerformancePanel.add(performanceSlider);

        graphicsCapabilitySlider.setMajorTickSpacing(2);
        graphicsCapabilitySlider.setMaximum(8);
        graphicsCapabilitySlider.setMinorTickSpacing(1);
        graphicsCapabilitySlider.setPaintLabels(true);
        graphicsCapabilitySlider.setPaintTicks(true);
        graphicsCapabilitySlider.setSnapToTicks(true);
        graphicsCapabilitySlider.setToolTipText("<html>Extending graphic card capability (GL memory) kills VisNow instantly<p>\nThis slider controls a safety feature in all visualization modules:<p>\nwhen the module generates too large geometric object, <p>\na warning is displayed and no geometry is created</html>");
        graphicsCapabilitySlider.setValue(4);
        graphicsCapabilitySlider.setName("graphicsCapabilitySlider"); // NOI18N
        graphicsCapabilitySlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                graphicsCapabilitySliderStateChanged(evt);
            }
        });
        hardwarePerformancePanel.add(graphicsCapabilitySlider);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(hardwarePerformancePanel, gridBagConstraints);

        windowSizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Window position on startup"));
        windowSizePanel.setName("windowSizePanel"); // NOI18N
        windowSizePanel.setLayout(new java.awt.GridBagLayout());

        mainWindowSizePanel.setName("mainWindowSizePanel"); // NOI18N
        mainWindowSizePanel.setLayout(new java.awt.GridBagLayout());

        jLabel11.setText("Main window:");
        jLabel11.setName("jLabel11"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        mainWindowSizePanel.add(jLabel11, gridBagConstraints);

        mainWindowSizeBG.add(mainWindowCurrentButton);
        mainWindowCurrentButton.setText("Use current position");
        mainWindowCurrentButton.setName("mainWindowCurrentButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 8);
        mainWindowSizePanel.add(mainWindowCurrentButton, gridBagConstraints);

        mainWindowSizeBG.add(mainWindowAutoButton);
        mainWindowAutoButton.setText("Default");
        mainWindowAutoButton.setName("mainWindowAutoButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        mainWindowSizePanel.add(mainWindowAutoButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 16, 4, 4);
        windowSizePanel.add(mainWindowSizePanel, gridBagConstraints);

        viewer3DWindowSizePanel.setName("viewer3DWindowSizePanel"); // NOI18N
        viewer3DWindowSizePanel.setLayout(new java.awt.GridBagLayout());

        jLabel12.setText("Viewer3D window:");
        jLabel12.setName("jLabel12"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        viewer3DWindowSizePanel.add(jLabel12, gridBagConstraints);

        viewer3DWindowSizeBG.add(viewer3DWindowCurrentButton);
        viewer3DWindowCurrentButton.setText("Use current position");
        viewer3DWindowCurrentButton.setName("viewer3DWindowCurrentButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 8);
        viewer3DWindowSizePanel.add(viewer3DWindowCurrentButton, gridBagConstraints);

        viewer3DWindowSizeBG.add(viewer3DWindowAutoButton);
        viewer3DWindowAutoButton.setText("Default");
        viewer3DWindowAutoButton.setName("viewer3DWindowAutoButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        viewer3DWindowSizePanel.add(viewer3DWindowAutoButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 16, 4, 4);
        windowSizePanel.add(viewer3DWindowSizePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(windowSizePanel, gridBagConstraints);

        viewerDefaultPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Startup viewers"));
        viewerDefaultPanel.setName("viewerDefaultPanel"); // NOI18N
        viewerDefaultPanel.setLayout(new java.awt.GridLayout(2, 2));

        startupViewer2DCB.setText("Viewer 2D");
        startupViewer2DCB.setToolTipText("Initialize Viewer 2D module at VisNow startup");
        startupViewer2DCB.setName("startupViewer2DCB"); // NOI18N
        startupViewer2DCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startupViewer2DCBActionPerformed(evt);
            }
        });
        viewerDefaultPanel.add(startupViewer2DCB);

        startupViewer3DCB.setText("Viewer 3D");
        startupViewer3DCB.setToolTipText("Initialize Viewer 3D module at VisNow startup");
        startupViewer3DCB.setName("startupViewer3DCB"); // NOI18N
        startupViewer3DCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startupViewer3DCBActionPerformed(evt);
            }
        });
        viewerDefaultPanel.add(startupViewer3DCB);

        startupFieldViewer3DCB.setText("Field Viewer 3D");
        startupFieldViewer3DCB.setToolTipText("Initialize Field Viewer 3D module at VisNow startup");
        startupFieldViewer3DCB.setName("startupFieldViewer3DCB"); // NOI18N
        startupFieldViewer3DCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startupFieldViewer3DCBActionPerformed(evt);
            }
        });
        viewerDefaultPanel.add(startupFieldViewer3DCB);

        startupOrthoViewer3DCB.setText("Orthoviewer 3D");
        startupOrthoViewer3DCB.setName("startupOrthoViewer3DCB"); // NOI18N
        startupOrthoViewer3DCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startupOrthoViewer3DCBActionPerformed(evt);
            }
        });
        viewerDefaultPanel.add(startupOrthoViewer3DCB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(viewerDefaultPanel, gridBagConstraints);

        viewerAutoConnectPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Auto connect"));
        viewerAutoConnectPanel.setName("viewerAutoConnectPanel"); // NOI18N
        viewerAutoConnectPanel.setLayout(new java.awt.GridLayout(1, 0));

        buttonGroup2.add(autoconnectViewerCB);
        autoconnectViewerCB.setText("To latest Viewer 2D/3D");
        autoconnectViewerCB.setToolTipText("Automatically connect module geometry output to Viewer 3D");
        autoconnectViewerCB.setName("autoconnectViewerCB"); // NOI18N
        viewerAutoConnectPanel.add(autoconnectViewerCB);

        buttonGroup2.add(autoconnectOrthoViewer3DCB);
        autoconnectOrthoViewer3DCB.setText("To Orthoviewer 3D");
        autoconnectOrthoViewer3DCB.setName("autoconnectOrthoViewer3DCB"); // NOI18N
        viewerAutoConnectPanel.add(autoconnectOrthoViewer3DCB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(viewerAutoConnectPanel, gridBagConstraints);

        fileChoosersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("File choosers"));
        fileChoosersPanel.setName("fileChoosersPanel"); // NOI18N
        fileChoosersPanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Default application directory:");
        jLabel3.setName("jLabel3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        fileChoosersPanel.add(jLabel3, gridBagConstraints);

        defAppDirTF.setName("defAppDirTF"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 0, 0);
        fileChoosersPanel.add(defAppDirTF, gridBagConstraints);

        defAppDirBB.setText("Browse...");
        defAppDirBB.setName("defAppDirBB"); // NOI18N
        defAppDirBB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                defAppDirBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
        fileChoosersPanel.add(defAppDirBB, gridBagConstraints);

        jLabel5.setText("Start application file chooser in:");
        jLabel5.setName("jLabel5"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        fileChoosersPanel.add(jLabel5, gridBagConstraints);

        useAppDirCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default", "last", "home" }));
        useAppDirCB.setName("useAppDirCB"); // NOI18N
        useAppDirCB.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        fileChoosersPanel.add(useAppDirCB, gridBagConstraints);

        jLabel2.setText("Default data directory:");
        jLabel2.setName("jLabel2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        fileChoosersPanel.add(jLabel2, gridBagConstraints);

        defDatDirTF.setName("defDatDirTF"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fileChoosersPanel.add(defDatDirTF, gridBagConstraints);

        defDatDirBB.setText("Browse...");
        defDatDirBB.setName("defDatDirBB"); // NOI18N
        defDatDirBB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                defDatDirBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
        fileChoosersPanel.add(defDatDirBB, gridBagConstraints);

        jLabel4.setText("Start data file chooser in:");
        jLabel4.setName("jLabel4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        fileChoosersPanel.add(jLabel4, gridBagConstraints);

        useDatDirCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default", "last", "home" }));
        useDatDirCB.setName("useDatDirCB"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        fileChoosersPanel.add(useDatDirCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(fileChoosersPanel, gridBagConstraints);

        unitPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Preferred units"));
        unitPanel.setMinimumSize(new java.awt.Dimension(373, 67));
        unitPanel.setName("unitPanel"); // NOI18N
        unitPanel.setPreferredSize(new java.awt.Dimension(373, 67));
        unitPanel.setLayout(new java.awt.GridLayout(1, 0));

        lengthUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "millimeter", "centimeter", "meter", "kilometer", " " }));
        lengthUnitCombo.setSelectedIndex(2);
        lengthUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("length/distance"));
        lengthUnitCombo.setName("lengthUnitCombo"); // NOI18N
        unitPanel.add(lengthUnitCombo);

        timeUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "microsecond", "milisecond", "second", "minute", "hour", "day", "year", " " }));
        timeUnitCombo.setSelectedIndex(2);
        timeUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("time"));
        timeUnitCombo.setName("timeUnitCombo"); // NOI18N
        unitPanel.add(timeUnitCombo);

        velocityUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "cm/sec", "m/sec", "km/h", "c" }));
        velocityUnitCombo.setSelectedIndex(1);
        velocityUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("velocity"));
        velocityUnitCombo.setName("velocityUnitCombo"); // NOI18N
        unitPanel.add(velocityUnitCombo);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(unitPanel, gridBagConstraints);

        colormapPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Colormap"));
        colormapPanel.setName("colormapPanel"); // NOI18N
        colormapPanel.setLayout(new java.awt.GridBagLayout());

        colorMapCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        colorMapCombo.setMinimumSize(new java.awt.Dimension(75, 35));
        colorMapCombo.setName("colorMapCombo"); // NOI18N
        colorMapCombo.setPreferredSize(new java.awt.Dimension(75, 35));
        colorMapCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                colorMapComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        colormapPanel.add(colorMapCombo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(colormapPanel, gridBagConstraints);

        renderingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Rendering"));
        renderingPanel.setName("renderingPanel"); // NOI18N
        renderingPanel.setLayout(new java.awt.GridBagLayout());

        jLabel10.setText("<html>Surface <p>offset:</html>");
        jLabel10.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabel10.setName("jLabel10"); // NOI18N
        jLabel10.setPreferredSize(new java.awt.Dimension(50, 30));
        jLabel10.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        renderingPanel.add(jLabel10, gridBagConstraints);

        surfaceOffsetTF.setMinimumSize(new java.awt.Dimension(80, 19));
        surfaceOffsetTF.setName("surfaceOffsetTF"); // NOI18N
        surfaceOffsetTF.setPreferredSize(new java.awt.Dimension(80, 19));
        surfaceOffsetTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                surfaceOffsetTFFocusLost(evt);
            }
        });
        surfaceOffsetTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                surfaceOffsetTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 0.7;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        renderingPanel.add(surfaceOffsetTF, gridBagConstraints);

        outlineBox.setText("<html>render 3D irregular<p>fields by outline</html>"); // NOI18N
        outlineBox.setName("outlineBox"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 0.7;
        renderingPanel.add(outlineBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(renderingPanel, gridBagConstraints);

        multithreadingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Multithreading"));
        multithreadingPanel.setName("multithreadingPanel"); // NOI18N
        multithreadingPanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(allThreadsRB);
        allThreadsRB.setSelected(true);
        allThreadsRB.setText("Use all available CPU cores");
        allThreadsRB.setName("allThreadsRB"); // NOI18N
        allThreadsRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                allThreadsRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        multithreadingPanel.add(allThreadsRB, gridBagConstraints);

        buttonGroup1.add(limitedThreadsRB);
        limitedThreadsRB.setText("Limit number of CPU cores to:");
        limitedThreadsRB.setName("limitedThreadsRB"); // NOI18N
        limitedThreadsRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                limitedThreadsRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        multithreadingPanel.add(limitedThreadsRB, gridBagConstraints);

        threadsSP.setEnabled(false);
        threadsSP.setMinimumSize(new java.awt.Dimension(48, 20));
        threadsSP.setName("threadsSP"); // NOI18N
        threadsSP.setPreferredSize(new java.awt.Dimension(48, 20));
        threadsSP.setValue(Runtime.getRuntime().availableProcessors());
        threadsSP.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                threadsSPStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        multithreadingPanel.add(threadsSP, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(multithreadingPanel, gridBagConstraints);

        exceptionsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Action on exceptional values"));
        exceptionsPanel.setName("exceptionsPanel"); // NOI18N
        exceptionsPanel.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText("On NaN");
        jLabel7.setName("jLabel7"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        exceptionsPanel.add(jLabel7, gridBagConstraints);

        jLabel8.setText("On Infinity ");
        jLabel8.setName("jLabel8"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        exceptionsPanel.add(jLabel8, gridBagConstraints);

        nanCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "set 0", "set number min", "set number max", "set data min", "set data max", "throw exception" }));
        nanCombo.setMinimumSize(new java.awt.Dimension(100, 34));
        nanCombo.setName("nanCombo"); // NOI18N
        nanCombo.setPreferredSize(new java.awt.Dimension(100, 34));
        nanCombo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                nanComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        exceptionsPanel.add(nanCombo, gridBagConstraints);

        infCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "set 0", "set number extreme", "set data extreme", "throw exception" }));
        infCombo.setMinimumSize(new java.awt.Dimension(100, 34));
        infCombo.setName("infCombo"); // NOI18N
        infCombo.setPreferredSize(new java.awt.Dimension(100, 34));
        infCombo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                infComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        exceptionsPanel.add(infCombo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(exceptionsPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void defAppDirBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defAppDirBBActionPerformed
        JFileChooser chooser = new JFileChooser(VisNow.get().getMainConfig().getDefaultApplicationsPath());
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            defAppDirTF.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_defAppDirBBActionPerformed

    private void defDatDirBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defDatDirBBActionPerformed
        JFileChooser chooser = new JFileChooser(VisNow.get().getMainConfig().getDefaultDataPath());
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            defDatDirTF.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_defDatDirBBActionPerformed

    private void threadsSPStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_threadsSPStateChanged
        int n = (Integer) threadsSP.getValue();
        int N = Runtime.getRuntime().availableProcessors();
        if (n < 1) {
            threadsSP.setValue(1);
            return;
        }

        if (n > N) {
            threadsSP.setValue(N);
            return;
        }
    }//GEN-LAST:event_threadsSPStateChanged

    private void allThreadsRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allThreadsRBActionPerformed
        threadsSP.setEnabled(limitedThreadsRB.isSelected());
    }//GEN-LAST:event_allThreadsRBActionPerformed

    private void limitedThreadsRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limitedThreadsRBActionPerformed
        threadsSP.setEnabled(limitedThreadsRB.isSelected());
    }//GEN-LAST:event_limitedThreadsRBActionPerformed

    private void nanComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_nanComboActionPerformed
    {//GEN-HEADEREND:event_nanComboActionPerformed
        FloatingPointUtils.defaultNanAction = NaNAction.values()[nanCombo.getSelectedIndex()];
    }//GEN-LAST:event_nanComboActionPerformed

    private void infComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_infComboActionPerformed
    {//GEN-HEADEREND:event_infComboActionPerformed
        FloatingPointUtils.defaultInfinityAction = InfinityAction.values()[infCombo.getSelectedIndex()];
    }//GEN-LAST:event_infComboActionPerformed

    private void colorMapComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_colorMapComboItemStateChanged
    {//GEN-HEADEREND:event_colorMapComboItemStateChanged
        int k = colorMapCombo.getSelectedIndex();

    }//GEN-LAST:event_colorMapComboItemStateChanged

    private void surfaceOffsetTFActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_surfaceOffsetTFActionPerformed
    {//GEN-HEADEREND:event_surfaceOffsetTFActionPerformed
        try {
            int v = Integer.parseInt(surfaceOffsetTF.getText());
        } catch(NumberFormatException ex) {
            surfaceOffsetTF.setText(""+VisNow.get().getMainConfig().getRenderingSurfaceOffset());
        }
    }//GEN-LAST:event_surfaceOffsetTFActionPerformed

    private void surfaceOffsetTFFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_surfaceOffsetTFFocusLost
    {//GEN-HEADEREND:event_surfaceOffsetTFFocusLost
        surfaceOffsetTFActionPerformed(null);
    }//GEN-LAST:event_surfaceOffsetTFFocusLost

    private void startupOrthoViewer3DCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startupOrthoViewer3DCBActionPerformed
    {//GEN-HEADEREND:event_startupOrthoViewer3DCBActionPerformed
        autoconnectOrthoViewer3DCB.setEnabled(startupOrthoViewer3DCB.isSelected());
    }//GEN-LAST:event_startupOrthoViewer3DCBActionPerformed

    private void startupViewer3DCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startupViewer3DCBActionPerformed
    {//GEN-HEADEREND:event_startupViewer3DCBActionPerformed
        autoconnectOrthoViewer3DCB.setEnabled(startupOrthoViewer3DCB.isSelected());
    }//GEN-LAST:event_startupViewer3DCBActionPerformed

    private void startupViewer2DCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startupViewer2DCBActionPerformed
    {//GEN-HEADEREND:event_startupViewer2DCBActionPerformed
        autoconnectOrthoViewer3DCB.setEnabled(startupOrthoViewer3DCB.isSelected());
    }//GEN-LAST:event_startupViewer2DCBActionPerformed

    private void startupFieldViewer3DCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startupFieldViewer3DCBActionPerformed
    {//GEN-HEADEREND:event_startupFieldViewer3DCBActionPerformed
        autoconnectOrthoViewer3DCB.setEnabled(startupOrthoViewer3DCB.isSelected());
    }//GEN-LAST:event_startupFieldViewer3DCBActionPerformed

    private void graphicsCapabilitySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_graphicsCapabilitySliderStateChanged
    {//GEN-HEADEREND:event_graphicsCapabilitySliderStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_graphicsCapabilitySliderStateChanged

    private String getMainWindowBounds()
    {
        Rectangle bounds = VisNow.get().getMainWindow().getBounds();
        return bounds.x + "," + bounds.y + "," + bounds.width + "," + bounds.height;
    }

    private String getViewer3DBounds()
    {
        Application currentApplication = VisNow.get().getMainWindow().getApplicationsPanel().getCurrentApplication();
        if (currentApplication == null) return null;
        else {
            Viewer3D v = currentApplication.getViewer3D();
            if (v == null) return "";
            else {
                Rectangle bounds = v.getFrame().getBounds();
                return bounds.x + "," + bounds.y + "," + bounds.width + "," + bounds.height;
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton allThreadsRB;
    private javax.swing.JCheckBox autoconnectOrthoViewer3DCB;
    private javax.swing.JCheckBox autoconnectViewerCB;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox colorMapCombo;
    private javax.swing.JPanel colormapPanel;
    private javax.swing.JButton defAppDirBB;
    private javax.swing.JTextField defAppDirTF;
    private javax.swing.JButton defDatDirBB;
    private javax.swing.JTextField defDatDirTF;
    private javax.swing.JPanel exceptionsPanel;
    private javax.swing.JPanel fileChoosersPanel;
    private javax.swing.JSlider graphicsCapabilitySlider;
    private javax.swing.JPanel hardwarePerformancePanel;
    private org.visnow.vn.gui.widgets.SteppedComboBox infCombo;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JComboBox lengthUnitCombo;
    private javax.swing.JRadioButton limitedThreadsRB;
    private javax.swing.JToggleButton mainWindowAutoButton;
    private javax.swing.JToggleButton mainWindowCurrentButton;
    private javax.swing.ButtonGroup mainWindowSizeBG;
    private javax.swing.JPanel mainWindowSizePanel;
    private javax.swing.JPanel multithreadingPanel;
    private org.visnow.vn.gui.widgets.SteppedComboBox nanCombo;
    private javax.swing.JCheckBox outlineBox;
    private javax.swing.JSlider performanceSlider;
    private javax.swing.JPanel renderingPanel;
    private javax.swing.JCheckBox startupFieldViewer3DCB;
    private javax.swing.JCheckBox startupOrthoViewer3DCB;
    private javax.swing.JCheckBox startupViewer2DCB;
    private javax.swing.JCheckBox startupViewer3DCB;
    private javax.swing.JTextField surfaceOffsetTF;
    private javax.swing.JSpinner threadsSP;
    private javax.swing.JComboBox timeUnitCombo;
    private javax.swing.JPanel unitPanel;
    private javax.swing.JComboBox useAppDirCB;
    private javax.swing.JComboBox useDatDirCB;
    private javax.swing.JComboBox velocityUnitCombo;
    private javax.swing.JToggleButton viewer3DWindowAutoButton;
    private javax.swing.JToggleButton viewer3DWindowCurrentButton;
    private javax.swing.ButtonGroup viewer3DWindowSizeBG;
    private javax.swing.JPanel viewer3DWindowSizePanel;
    private javax.swing.JPanel viewerAutoConnectPanel;
    private javax.swing.JPanel viewerDefaultPanel;
    private javax.swing.JPanel windowSizePanel;
    // End of variables declaration//GEN-END:variables


    public void apply()
    {
        MainConfig config = VisNow.get().getMainConfig();
        config.setStartupViewer2D(startupViewer2DCB.isSelected());
        config.setStartupViewer3D(startupViewer3DCB.isSelected());
        config.setStartupFieldViewer3D(startupFieldViewer3DCB.isSelected());
        config.setAutoconnectViewer(autoconnectViewerCB.isSelected());
        config.setStartupOrthoViewer3D(startupOrthoViewer3DCB.isSelected());
        config.setAutoconnectOrthoViewer3D(autoconnectOrthoViewer3DCB.isSelected());
        config.setDefaultApplicationsPath(defAppDirTF.getText());
        config.setUsableApplicationsPathType((String) useAppDirCB.getSelectedItem());
        config.setDefaultDataPath(defDatDirTF.getText());
        config.setUsableDataPathType((String) useDatDirCB.getSelectedItem());
        config.setColorAdjustingLimit(1 << 4 * performanceSlider.getValue() + 13);
        config.setGraphicsPerformanceLimit(1 << 20 + graphicsCapabilitySlider.getValue());
        config.setInfAction(infCombo.getSelectedIndex());
        config.setNaNAction(nanCombo.getSelectedIndex());
        config.setDefaultLengthUnit((String) lengthUnitCombo.getSelectedItem());
        config.setDefaultTimeUnit((String) timeUnitCombo.getSelectedItem());
        config.setDefaultVelocityUnit((String) velocityUnitCombo.getSelectedItem());
        config.setNAvailableThreads(allThreadsRB.isSelected() ?
                                    Runtime.getRuntime().availableProcessors() :
                                    (Integer) threadsSP.getValue());
        config.setPerformance(performanceSlider.getValue());
        config.setDefaultColorMap(colorMapCombo.getSelectedIndex());
        config.setRenderingSurfaceOffset(Integer.parseInt(surfaceOffsetTF.getText()));
        if (mainWindowAutoButton.isSelected())
            config.setMainWindowBounds("");
        else if (mainWindowCurrentButton.isSelected())
            config.setMainWindowBounds(getMainWindowBounds());
        if (viewer3DWindowAutoButton.isSelected())
            config.setViewer3DWindowBounds("");
        else if (viewer3DWindowCurrentButton.isSelected() && getViewer3DBounds() != null)
            config.setViewer3DWindowBounds(getViewer3DBounds());
        config.setOutlineDefault(outlineBox.isSelected());
        config.saveConfig();
    }
}
