/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.config;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNOuterIOException;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class MainConfigInitializer
{

    //<editor-fold defaultstate="collapsed" desc=" Init templates ">
    public static void initTemplates(File file)
    {
        file.mkdir();
        try {
            JarFile jar = new JarFile(VisNow.get().getJarPath());
            Enumeration<JarEntry> enumeration;
            enumeration = jar.entries();

            while (enumeration.hasMoreElements()) {
                JarEntry e = enumeration.nextElement();
                if (e.getName().toLowerCase().startsWith(MainConfig.TEMPLATES) &&
                    e.getName().length() > 10) {
                    InputStream is = jar.getInputStream(e);

                    File out = new File(file.getPath() + File.separator + e.getName().substring(9));
                    out.createNewFile();
                    OutputStream os = new FileOutputStream(out);
                    //TODO: jak to przyspieszyć?
                    for (int i = is.read();; i = is.read()) {
                        if (i == -1) {
                            os.close();
                            break;
                        }
                        os.write(i);
                    }

                }
            }

        } catch (IOException ex) {
            Displayer.ddisplay(200907311200L, ex, "MainConfigInitializer.static", "Could not initialize templates.");
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Init libraries ">
    public static void initPluginsActive(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100500L,
                "Could not create plugins active file.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    static void initPluginFolders(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100500L,
                "Could not create plugin folders file.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Init recent folders ">
    public static void initRecentFolders(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write("10\n");
            writer.close();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100501L,
                "Could not create file for recent folders configuration.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Init recent applications ">
    public static void initRecentApplications(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write("10\n");
            writer.close();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100502L,
                "Could not create file for recent applications configuration.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Init favorite folders ">
    public static void initFavoriteFolders(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write("0.Home\n");
            writer.write(System.getProperty("user.home") + "\n");
            writer.write("1.Templates\n");
            writer.write(file.getParent() + File.separator + MainConfig.TEMPLATES + "\n");
            writer.close();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100503L,
                "Could not create file for favorite folders configuration.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Init properties ">
    public static void initProperties(File file) throws VNOuterIOException
    {
        Properties props = new Properties();
        props.setProperty("visnow.startupViewer3D", "true");
        props.setProperty("visnow.startupViewer2D", "false");
        props.setProperty("visnow.startupFieldViewer3D", "false");
        props.setProperty("visnow.autoconnectViewer", "true");

        props.setProperty("visnow.paths.applications.default", System.getProperty("user.home"));
        props.setProperty("visnow.paths.applications.last", System.getProperty("user.home"));
        props.setProperty("visnow.paths.applications.use", "last"); //last/default/home

        props.setProperty("visnow.paths.data.default", System.getProperty("user.home"));
        props.setProperty("visnow.paths.data.last", System.getProperty("user.home"));
        props.setProperty("visnow.paths.data.use", "last"); //last/default/home

        props.setProperty("visnow.continuousColorAdjustingLimit", "64000");
        props.setProperty("visnow..graphicsPerformanceLimit", "16000000");
        props.setProperty("visnow.rendering.surfaceOffset", "200");

        try {
            props.store(new FileOutputStream(file), null);
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100503L,
                "Could not create file for properties configuration.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    //</editor-fold>
    public static void initColorMaps(File file) throws VNOuterIOException
    {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write("<colormaps></colormaps>");
            writer.close();
        } catch (IOException ex) {
            throw new VNOuterIOException(
                200907100503L,
                "Could not create file for favorite folders configuration.",
                ex,
                "MainConfigInitializer.static",
                Thread.currentThread());
        }
    }

    private MainConfigInitializer()
    {
    }

}
