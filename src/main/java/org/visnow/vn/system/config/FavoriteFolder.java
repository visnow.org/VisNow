/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.config;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class FavoriteFolder
{

    //<editor-fold defaultstate="collapsed" desc=" [FSVAR] Icons ">
    //public final static int MIN_PUBLIC_ICON = 2;
    public final static int HOME_ICON = 0;
    public final static int TEMPLATES_ICON = 1;

    public final static Icon[] ICONS = new Icon[]{
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/home.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/template.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/gray.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/silver.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/yellow.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/orange.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/red.png")),
        new ImageIcon(FavoriteFolder.class.getResource("/org/visnow/vn/gui/icons/folders/maroon.png")),};
    //</editor-fold>

    private String path;
    private String name;
    private int iconId;

    public FavoriteFolder(String name, String path)
    {
        this(name, path, 0);
    }

    public FavoriteFolder(String name, String path, int icon)
    {
        this.name = name;
        this.path = path;
        this.iconId = icon;
    }

    /**
     * @return the path
     */
    public String getPath()
    {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path)
    {
        this.path = path;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public int getIconId()
    {
        return iconId;
    }

    /**
     * @return the icon
     */
    public Icon getIcon()
    {
        return ICONS[iconId];
    }

    /**
     * @param icon the icon to set
     */
    public void setIconId(int iconId)
    {
        this.iconId = iconId;
    }

}
