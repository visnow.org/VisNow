/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.io;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNReaderNode
{

    //    public final static String application = "application";
    //    public final static String library = "module";
    //    public final static String module = "module";
    //    public final static String params = "params";
    //    public final static String end = "end";
    //    public final static String link = "link";
    //
    //    public final static String[] tab = {};
    private String typeString;
    public VNReaderNodeType type;
    public HashMap<String, String> data = new HashMap<String, String>();

    public static VNReaderNode read(String s)
    {
        //empty, comment line or invalid
        if (s.length() == 0 || s.charAt(0) == '#' || s.indexOf('@') == -1 || s.indexOf('@') == s.length() - 1) return null;

        s = s.substring(s.indexOf('@') + 1) + "  ";

        VNReaderNode node = new VNReaderNode();

        Vector<String> vec = new Vector<String>();
        String delim = "<>";
        String whitespace = " \t";

//        s = s.substring(1) + "  ";
        String cur = "";

        while (s.length() > 0) {
            char next = s.charAt(0);
            s = s.substring(1);
            if (whitespace.indexOf(next) == -1) {
                cur = cur + next;
            } else {
                node.typeString = cur;
                break;
            }
        }

        cur = "";
        boolean inside = false;

        while (s.length() > 0) {
            char next = s.charAt(0);
            if (delim.indexOf(next) == -1) {
                if (inside || whitespace.indexOf(next) == -1)
                    cur = cur + next;
            } else if (cur.length() > 0) {
                vec.add(cur);
                inside = !inside;
                cur = "";
            }
            s = s.substring(1);
        }

        while (vec.size() > 1) {
            node.data.put(vec.get(0), vec.get(1));
            vec.remove(0);
            vec.remove(0);
        }

        for (VNReaderNodeType type : VNReaderNodeType.values()) {
            if (node.typeString.equalsIgnoreCase(type.name())) {
                node.type = type;
                return node;
            }
        }

        return null;
    }
}
