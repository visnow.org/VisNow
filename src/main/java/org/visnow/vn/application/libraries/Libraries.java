/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.libraries;

import org.visnow.vn.engine.core.CoreName;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.library.LibraryRoot;
import org.visnow.vn.engine.main.DataCore;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Libraries implements Iterable<LibraryRoot>
{

    public final static String DATA_LIBRARY = "data";
    private Application application;

    public ModuleCore generateCore(CoreName coreName) throws VNException
    {
        if (coreName.getLibraryName().equals(DATA_LIBRARY)) {
            return new DataCore(coreName.getClassName());
        }
        return libraries.get(coreName.getLibraryName()).loadCore(coreName.getClassName());
    }

    public Application getApplication()
    {
        return application;
    }

    private HashMap<String, LibraryRoot> libraries;

    public HashMap<String, LibraryRoot> getLibraries()
    {
        return libraries;
    }

    public LibraryRoot getLibrary(String name)
    {
        return libraries.get(name);
    }

    public void addLibrary(String name, LibraryRoot root)
    {
        libraries.put(name, root);
    }

    public void deleteLibrary(String name)
    {
        libraries.remove(name);
    }

    public Libraries(Application application)
    {
        this.libraries = new HashMap<String, LibraryRoot>();
        this.application = application;
        //libraries.put("internal", VisNow.get().getMainLibraries().getInternalLibrary());
        Vector<LibraryRoot> vnlibs = VisNow.get().getMainLibraries().getLibraries();
        for (int i = 0; i < vnlibs.size(); i++) {
            libraries.put(vnlibs.get(i).getName(), vnlibs.get(i));
        }

    }

    public Iterator<LibraryRoot> iterator()
    {
        return libraries.values().iterator();
    }

}
