/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area;

import java.awt.Component;
import java.awt.Point;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.vn.application.area.widgets.LinkPanel;
import org.visnow.vn.application.area.widgets.ModulePanel;
import org.visnow.vn.engine.commands.LinkDeleteCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.LinkName;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class AreaInput
{
    private static final Logger LOGGER = Logger.getLogger(AreaInput.class);
    private Area area;

    public Area getArea()
    {
        return area;
    }

    protected AreaInput(Area area)
    {
        this.area = area;
    }

    public void showLock(final boolean b)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                area.getPanel().setLocked(b);

            }
        });
    }

    public Point getModulePosition(String name)
    {
        return area.getPanel().getModulePanel(name).getLocation();
    }

    public void setModuleProgress(final String name, final float progress)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                ModulePanel panel = area.getPanel().getModulePanel(name);
                if (panel != null)
                    panel.setProgress(progress);
            }
        });
    }

    public Vector<SelectableAreaItem> getSelectedItems()
    {
        return this.getArea().getSelection();
    }

    public void addModuleWidget(final String name, final Point position)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                ModulePanel p = new ModulePanel(area.getPanel(), area.getApplication().getEngine().getModule(name));
                area.getPanel().addModulePanel(p, position);
            }
        });
    }

    public void selectModule(final String name)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                area.select(area.getPanel().getModulePanel(name));
            }
        });
    }

    public void renameModuleWidget(final String name, final String newName)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                //               
                area.getPanel().getModulePanel(name).updateName();
                area.getPanel().updateModuleName(name, newName);
            }
        });

    }

    public void deleteModuleWidget(final String name)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                area.getPanel().deleteModulePanel(name);
            }
        });

    }

    public void addLinkWidget(final LinkName name)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                //System.out.println("--------- swing link panel creation");
                LinkPanel lp = new LinkPanel(
                        area.getApplication().getEngine().getLink(name),
                        area.getPanel().getModulePanel(name.getOutputModule()).getOutputPanel(name.getOutputPort()),
                        area.getPanel().getModulePanel(name.getInputModule()).getInputPanel(name.getInputPort()));
                //System.out.println(name);
                //System.out.println("@"+Thread.currentThread());
                area.getPanel().addLinkPanel(lp);
            }
        });
    }

    public void deleteLinkWidget(final LinkName name)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                area.getPanel().deleteLinkPanel(name);
            }
        });
    }

    public void addDataWidget(String dataName, LinkName linkName, LinkName name)
    {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Component getDropComponent()
    {
        //.getScenePanel().getViewPort();
        return this.getArea().getPanel().getViewPort();
    }

    public int getDropOffsetX()
    {
        //.getScene().getScenePanel().getScrollOffsetX();
        return this.getArea().getPanel().getScrollOffsetX();

    }

    public int getDropOffsetY()
    {
        return this.getArea().getPanel().getScrollOffsetY();
    }

    //FIXME: This method is called from Swing EDT, in the same time module onActive method calls Swing methods, and both onActive and this method
    //use application.getAccess(.) to acquire the lock which leads to deadlock (in Swing EDT)
    //new Thread .start() is just a quick fix solution - should be done in all Area event handlers!
    public void requestedDeleteSelectedItems()
    {
        LOGGER.debug("");
        Vector<SelectableAreaItem> vec = area.getInput().getSelectedItems();
        if (vec.isEmpty())
            return;

        final Vector<Link> links = new Vector<Link>();
        final Vector<ModulePanel> modules = new Vector<ModulePanel>();
        for (SelectableAreaItem i : vec) {
            if (i instanceof LinkPanel)
                links.add(((LinkPanel) i).getLink());
            if (i instanceof ModulePanel)
                modules.add((ModulePanel) i);
        }

        final Area thisArea = area;

        if (area.getApplication().isLockupBusy())
            LOGGER.debug("Lockup is busy");
        else
            new Thread(new Runnable()
            {
                public void run()
                {
                    for (ModulePanel module : modules) {
                        thisArea.getApplication()
                                .getReceiver().receive(new ModuleDeleteCommand(
                                                module.getModule().getName(),
                                                module.getModule().getCore().getCoreName(),
                                                module.getLocation()));
                    }
                    for (Link link : links) {
                        thisArea.getApplication()
                                .getReceiver().receive(new LinkDeleteCommand(link.getName(), true));
                    }
                }
            }).start();
    }

    public void renameLink(LinkName oldLinkName, LinkName newLinkName)
    {
        this.getArea().getPanel().updateLinkName(oldLinkName, newLinkName);
    }
}
