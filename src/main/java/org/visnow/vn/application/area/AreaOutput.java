/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area;

import java.awt.Point;
import org.visnow.vn.application.area.widgets.LinkPanel;
import org.visnow.vn.application.area.widgets.ModulePanel;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.commands.LinkDeleteCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.commands.ModuleRenameCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.main.Port;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class AreaOutput
{

    private Area area;

    protected Area getArea()
    {
        return area;
    }

    protected AreaOutput(Area area)
    {
        this.area = area;
    }

    public void deleteModule(final String name, final CoreName coreName, final Point location)
    {
        final Area thisArea = area;
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    thisArea
                            .getApplication()
                            .getReceiver()
                            .receive(new ModuleDeleteCommand(
                                            name, coreName, location
                                    ));
                }

            }).start();
    }

    public void renameModule(final String name, final String jop)
    {
        final Area thisArea = area;
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    thisArea
                            .getApplication()
                            .getReceiver()
                            .receive(new ModuleRenameCommand(name, jop));
                }

            }).start();
    }

    public void startAction(ModuleBox module)
    {
        module.startAction();
    }

    public void selectNull()
    {
        area.selectNull();
    }

    void select(ModulePanel panel, boolean controlDown)
    {
        if (controlDown) {
            area.alterSelection(panel);
        } else {
            area.select(panel);
        }
    }

    void select(LinkPanel panel, boolean controlDown)
    {
        if (controlDown) {
            area.alterSelection(panel);
        } else {
            area.select(panel);
        }
    }

    void addLink(Port a, Port b, final boolean isActive)
    {
        final Port in, out;
        if (a.isInput()) {
            in = a;
            out = b;
        } else {
            in = b;
            out = a;
        }

        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    getArea()
                            .getApplication()
                            .getReceiver()
                            .receive(
                                    new LinkAddCommand(
                                            new LinkName(out.getModuleBox().getName(),
                                                         out.getName(),
                                                         in.getModuleBox().getName(),
                                                         in.getName()
                                            ),
                                            isActive));
                }
            }).start();
    }

    public void deleteLink(final LinkName name)
    {
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    getArea().getApplication().getReceiver().receive(new LinkDeleteCommand(name, true));
                }
            }).start();
    }
}
