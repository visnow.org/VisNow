/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area.widgets;

import java.awt.Point;
import org.visnow.vn.application.area.Quad;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LinkPanelRectangle
{

    private int fromX;
    private int fromY;
    private int toX;
    private int toY;

    private int getLx()
    {
        return (fromX < toX) ? fromX : toX;
    }

    private int getLy()
    {
        return (fromY < toY) ? fromY : toY;
    }

    private int getRx()
    {
        return (fromX > toX) ? fromX : toX;
    }

    private int getRy()
    {
        return (fromY > toY) ? fromY : toY;
    }

    public LinkPanelRectangle(int fromX, int fromY, int toX, int toY, int d)
    {
        int minX = (fromX < toX) ? fromX : toX;
        int minY = (fromY < toY) ? fromY : toY;
        int maxX = (fromX > toX) ? fromX : toX;
        int maxY = (fromY > toY) ? fromY : toY;
        this.fromX = minX - d;
        this.fromY = minY - d;
        this.toX = maxX + d;
        this.toY = maxY + d;
    }

    public boolean contains(Point p)
    {
        if (p.getX() >= fromX && p.getX() <= toX)
            if (p.getY() >= fromY && p.getY() <= toY)
                return true;
        return false;
    }

    public boolean isRectangled(Quad q)
    {
        if ((getLx() > q.getLx() && getLx() < q.getRx()) ||
            (getRx() > q.getLx() && getRx() < q.getRx()) ||
            (getLx() < q.getLx() && getRx() > q.getLx()) ||
            (getLx() < q.getRx() && getRx() > q.getRx()))
            if ((getLy() > q.getLy() && getLy() < q.getRy()) ||
                (getRy() > q.getLy() && getRy() < q.getRy()) ||
                (getLy() < q.getLy() && getRy() > q.getLy()) ||
                (getLy() < q.getRy() && getRy() > q.getRy()))
                return true;
        return false;
    }

}
