/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.frames.tabs;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JOptionPane;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.commands.ModuleAddCommand;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author gacek
 */
public class ModuleAdder implements Runnable, Transferable
{

    private int sceneX;
    private int sceneY;
    private Vector<LibraryCore> selectedCores;
    private Application application;
    private boolean forceFlag = false;

    public ModuleAdder(Vector<LibraryCore> selectedCores, Application application)
    {
        this.selectedCores = selectedCores;
        this.application = application;
    }

    public void run()
    {
        print("run: tryGetAccess()");
        boolean acq = application.tryGetAccess("Add module from library (MLibP-1)");
        if (acq) {
            rerun();
        } else {
            java.awt.EventQueue.invokeLater(new Runnable()
            {

                public void run()
                {
                    int i = JOptionPane.showConfirmDialog(
                        null,
                        "Application is busy.\n" +
                        "Add the module anyway?\n" +
                        "(It's appearance may be delayed)",
                        "Network busy",
                        JOptionPane.YES_NO_OPTION);
                    if (i != JOptionPane.YES_OPTION) {
                        return;
                    } else {
                        new Thread(new Runnable()
                        {

                            public void run()
                            {
                                tryrun();
                            }
                        }).start();
                    }
                }
            });
        }
    }

    private void tryrun()
    {
        try {
            application.getAccess("Add module from library (MLibP-0)");
        } catch (InterruptedException ex) {
            Displayer.ddisplay(201001261216L, ex, this, "Interrupted");
        }
        rerun();
    }

    private void rerun()
    {
        int dx = 0;
        int dy = 0;
        application.releaseAccess();
        for (LibraryCore core : selectedCores) {
            application.getReceiver().receive(new ModuleAddCommand(
                core.getName() + "[" + application.getEngine().nextModuleNumber() + "]",
                core.getCoreName(),
                new Point(sceneX - 15 + dx, sceneY - 18 + dy), 
                forceFlag));
            dx += 30;
            dy += 60;
        }

    }

    private boolean debug = false;

    private void print(String s)
    {
        if (!debug) {
            return;
        }
        System.out.println("MLibrariesPanelMA: \t" + s);
    }

    @Override
    public DataFlavor[] getTransferDataFlavors()
    {
        return new DataFlavor[]{MLibrariesPanel.moduleAdderFlavor};
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        return (flavor.equals(MLibrariesPanel.moduleAdderFlavor));
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
        return this;
    }

    public void setLocation(Point location)
    {
        sceneX = location.x;
        sceneY = location.y;
    }


    /**
     * @param forceFlag the forceFlag to set
     */
    public void setForceFlag(boolean forceFlag) {
        this.forceFlag = forceFlag;
    }
    
    public static void doAddModule(LibraryCore core, Application app, Point p, boolean forceFlag) {
        try {
            Vector<LibraryCore> coreToAdd = new Vector<>();
            coreToAdd.add(core);
            ModuleAdder ma = new ModuleAdder(coreToAdd, app);
            ma.setForceFlag(forceFlag);
            if(p == null) {
                ma.setLocation(app.getNextModuleLocation());
            } else {
                ma.setLocation(p);                            
            }
            new Thread(ma).start();
        } catch (Exception ex) {
            VisNow.get().userMessageSend(null, "Error", "Error while creating module "+core.getName(), org.visnow.vn.system.utils.usermessage.Level.ERROR);
        }
        
    }
}
