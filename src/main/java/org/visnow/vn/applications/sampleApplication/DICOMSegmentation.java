
package org.visnow.vn.applications.sampleApplication;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import org.visnow.vn.applications.ApplicationTemplate;
import org.visnow.vn.lib.basic.filters.VolumeSegmentation.VolumeSegmentation;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.FieldDisplay3DInternalFrame;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author know
 */
public class DICOMSegmentation extends ApplicationTemplate implements ComponentListener
{
    protected static VolumeSegmentation volumeSegmentation;
    protected FieldDisplay3DInternalFrame segFrame = null;
    /**
     * Creates new form DICOMSegmentation
     */
    public DICOMSegmentation()
    {
        super();
        newModule("DICOM reader",         "org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOM");
        newModule("anisotropic denoiser", "org.visnow.vn.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiser");  
        
        volumeSegmentation = new VolumeSegmentation(true, true);
        initModule("volume segmentation", volumeSegmentation);
        segFrame = volumeSegmentation.getDisplay3DFrame();
        segFrame.setSize(mainPanel.getWidth(), mainPanel.getHeight());
        mainPanel.add(segFrame,BorderLayout.CENTER);
        
        newModule("writer",               "org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriter");
        
        addLink("DICOM reader",         "outField", "anisotropic denoiser", "inField");
        addLink("anisotropic denoiser", "outField", "volume segmentation",  "inField");
        addLink("volume segmentation",  "outField", "writer", "inField");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.mainBlocking(args, false);
        DICOMSegmentation segmentation = new DICOMSegmentation();
        segmentation.setVisible(true);
    }

    @Override
    public void componentResized(ComponentEvent e)
    {
        if (segFrame != null)
            segFrame.setSize(mainPanel.getWidth(), mainPanel.getHeight());
    }

    @Override
    public void componentMoved(ComponentEvent e) {}

    @Override
    public void componentShown(ComponentEvent e) {}

    @Override
    public void componentHidden(ComponentEvent e) {}
}
